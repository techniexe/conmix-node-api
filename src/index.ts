import "reflect-metadata"
import { app } from "./app"
import { logger } from "./logger"
const port = parseInt(process.env.PORT || "3033", 10)
const host = process.env.HOST || "0.0.0.0"

app.listen(port, host, (err: Error) => {
  if (err) throw err
  logger.info("Server running on port", port)
})
