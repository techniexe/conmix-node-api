import * as bodyParser from "body-parser"
import { NextFunction, Response, Request } from "express"
import * as morgan from "morgan"
import { logger } from "./logger"
import * as cors from "cors"
import * as helmet from "helmet"
import * as fileUpload from "express-fileupload"
import { ErrorRequestHandler } from "express-serve-static-core"
import { InversifyExpressServer } from "inversify-express-utils"
import { vendorContainer } from "./vendor/vendor.container"
import { commonContainer } from "./common/common.container"
import { adminContainer } from "./admin/admin.container"
import { JSONParseError } from "./utilities/customError"
import { reconnectDb } from "./utilities/db"
import { buyerContainer } from "./buyer/buyer.container"

const errorHandler: ErrorRequestHandler = (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if ((err as JSONParseError) instanceof SyntaxError && err.status === 400) {
    err.httpStatusCode = 400
    logger.info("JSON error:", err.message)
    err.message = `Invalid JSON string in request.`
  }
  if (!err.httpStatusCode) {
    err.httpStatusCode = 500
  }
  if (!res.headersSent) {
    res.status(err.httpStatusCode).json({ error: { message: err.message } })
  }

  if (err.message === "Topology was destroyed") {
    console.log(`Mongodb Error: Topology was destroyed. Trying to Reconnect`)
    reconnectDb()
  }

  logger.fatal(
    {
      errorMsg: err.message,
      uri: req.url,
      body: req.body,
      params: req.params,
      headers: req.headers,
      stack: err.stack,
    },
    `${err.httpStatusCode}Error`
  )
  res.status(err.httpStatusCode).end(err.message)
}

const morganReqHandler = morgan((tokens, req, res) => {
  logger.info({
    method: tokens.method(req, res),
    url: tokens.url(req, res),
    status: tokens.status(req, res),
    res: tokens.res(req, res, "content-length"),
    responseTime: tokens["response-time"](req, res),
    remoteAddress: tokens["remote-addr"](req, res),
    userAgent: tokens["user-agent"](req, res),
  })
  return ""
})
/** Buyer server config */
const buyerServer = new InversifyExpressServer(buyerContainer, undefined, {
  rootPath: "/v1/buyer",
})
buyerServer.setConfig((expressApp) => {
  expressApp.use(helmet())
  expressApp.use(cors())
  expressApp.use(bodyParser.json())
  expressApp.use(bodyParser.urlencoded({ extended: true }))
  expressApp.use(morganReqHandler)
  expressApp.use(
    fileUpload({
      parseNested: true,
      // debug: true,
    })
  )
})
/** Vendor server config */
const vendorServer = new InversifyExpressServer(
  vendorContainer,
  undefined,
  {
    rootPath: "/v1/vendor",
  },
  buyerServer.build()
)

vendorServer.setConfig((expressApp) => {
  expressApp.use(bodyParser.json())
  // expressApp.use(helmet())
  // expressApp.use(cors())
  // expressApp.use(bodyParser.json())
  // expressApp.use(bodyParser.urlencoded({ extended: true }))
  // expressApp.use(morganReqHandler)
  // expressApp.use(
  //   fileUpload({
  //     parseNested: true,
  //     // debug: true,
  //   })
  // )
})

// vendorServer.setConfig((expressApp) => {
//   expressApp.use(bodyParser.json())
// })
/** Admin server */
const adminServer = new InversifyExpressServer(
  adminContainer,
  undefined,
  {
    rootPath: "/v1/admin",
  },
  vendorServer.build()
)

adminServer.setConfig((expressApp) => {
  expressApp.use(bodyParser.json())
})
// const logisticsServer = new InversifyExpressServer(
//   logisticsContainer,
//   undefined,
//   {
//     rootPath: "/v1/logistics",
//   },
//   adminServer.build()
// )

// logisticsServer.setConfig((expressApp) => {
//   expressApp.use(bodyParser.json())
// })

/** Public server config */
const commonServer = new InversifyExpressServer(
  commonContainer,
  undefined,
  undefined,
  adminServer.build()
)
commonServer.setConfig((expressApp) => {
  expressApp.use(bodyParser.json())
})
commonServer.setErrorConfig((eApp) => {
  eApp.use((req, res, next) => {
    res.sendStatus(404)
  })
  eApp.use(errorHandler)
})
export const app = commonServer.build()
