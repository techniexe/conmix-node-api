import * as Mongoose from "mongoose"

export interface IWishlistDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  product_id: Mongoose.Types.ObjectId
  quantity: number
  created_at: Date
}

const mongooseWishlistSchema = new Mongoose.Schema(
  {
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      index: true,
    },
    product_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "product",
      index: true,
    },
    quantity: Number,
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "wishlist", versionKey: false }
).index(
  {
    user_id: 1,
    product_id: 1,
  },
  { unique: true }
)

export const WishlistModel: Mongoose.Model<IWishlistDoc> = Mongoose.model<
  IWishlistDoc
>("wishlist", mongooseWishlistSchema)
