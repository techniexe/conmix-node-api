import * as Mongoose from "mongoose"
export interface IUniqueIdDoc extends Mongoose.Document {
  _id: string // as collection name
  last_num: number
}

const mongooseUniqueidSchema = new Mongoose.Schema(
  {
    _id: String,
    last_num: Number,
  },
  { collection: "unique_id", versionKey: false }
)

export const UniqueidModel: Mongoose.Model<IUniqueIdDoc> = Mongoose.model<
  IUniqueIdDoc
>("unique_id", mongooseUniqueidSchema)
