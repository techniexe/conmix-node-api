import * as Mongoose from "mongoose"

export interface IConcreteGradeDoc extends Mongoose.Document {
  name: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_deleted: boolean
  deleted_at: Date
  deleted_by: Mongoose.Types.ObjectId
  cg_date: Date
}

const mongooseConcreteGradeSchema = new Mongoose.Schema(
  {
    name: {type: String, unique: true},
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    is_deleted: { type: Boolean, default: false },
    deleted_at: Date,
    deleted_by: Mongoose.Schema.Types.ObjectId,
    cg_date: { type: Date, default: Date.now },
  },
  { collection: "concrete_grade", versionKey: false }
).index({
  name: "text",
})

export const ConcreteGradeModel: Mongoose.Model<IConcreteGradeDoc> = Mongoose.model<
  IConcreteGradeDoc
>("concrete_grade", mongooseConcreteGradeSchema)
