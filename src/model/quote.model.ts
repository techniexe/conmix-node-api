import * as Mongoose from "mongoose"
import { ILocation } from "../utilities/common.interfaces"
import { OrderItemQuoteStatus } from "./order_item.model"

export enum QuoteStatus {
  OPEN = "OPEN",
  CLOSED = "CLOSED",
  ACCEPTED = "ACCEPTED",
}
export interface IQuoteDoc extends Mongoose.Document {
  quote_display_id: string
  quote_group_id: string
  logistics_user_id: Mongoose.Types.ObjectId
  buyer_user_id: Mongoose.Types.ObjectId
  supplier_user_id: Mongoose.Types.ObjectId
  product_id: Mongoose.Types.ObjectId
  product_sub_category_id: Mongoose.Types.ObjectId
  product_category_id: Mongoose.Types.ObjectId
  order_id: Mongoose.Types.ObjectId
  order_item_id: Mongoose.Types.ObjectId
  supplier_order_id: Mongoose.Types.ObjectId
  vehicle_id?: Mongoose.Types.ObjectId
  vehicle_sub_category_id: Mongoose.Types.ObjectId
  pickup_sub_city_id?: Mongoose.Types.ObjectId
  delivery_sub_city_id?: Mongoose.Types.ObjectId
  pickup_location: ILocation
  delivery_location: ILocation
  quantity: number
  quantity_unit: string
  distance: number
  trip_cost: number
  avg_trip_cost: number
  max_quote_amount: number
  quoted_amount?: number
  status: OrderItemQuoteStatus // open/closed
  created_at: Date
  quoted_at?: Date
  max_quoted_at?: Date
}

const mongooseQuoteSchema = new Mongoose.Schema(
  {
    quote_display_id: { type: String, index: true },
    quote_group_id: { type: String, index: true },
    logistics_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "logistics_user",
      index: true,
    },
    buyer_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      index: true,
    },
    supplier_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "supplier",
      index: true,
    },
    product_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "product",
      index: true,
    },
    product_category_id: Mongoose.Schema.Types.ObjectId,
    product_sub_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "product_sub_category",
      index: true,
    },
    order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "order",
      index: true,
    },
    order_item_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "order_item",
      index: true,
    },
    supplier_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "supplier_order",
      index: true,
    },
    vehicle_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "vehicle",
      index: true,
    },
    vehicle_sub_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "vehicle_sub_category",
    },
    pickup_sub_city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "sub_city",
    },
    delivery_sub_city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "sub_city",
    },
    pickup_location: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    delivery_location: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    quantity: Number,
    quantity_unit: String,
    distance: Number,
    trip_cost: Number,
    avg_trip_cost: Number,
    max_quote_amount: Number,
    quoted_amount: Number,
    status: {
      type: String,
      enum: Object.values(OrderItemQuoteStatus),
      default: OrderItemQuoteStatus.NOT_ASSIGNED,
      index: true,
    },
    created_at: { type: Date, index: true, default: Date.now },
    quoted_at: Date,
    max_quoted_at: Date,
  },
  { collection: "quote", versionKey: false }
)
export const QuoteModel: Mongoose.Model<IQuoteDoc> = Mongoose.model<IQuoteDoc>(
  "quote",
  mongooseQuoteSchema
)
