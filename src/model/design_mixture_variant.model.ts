import * as Mongoose from "mongoose"

export interface IDesignMixVariantDoc extends Mongoose.Document {
  design_mix_id: string
  grade_id: string
  product_name: string
  cement_brand_id: string
  cement_quantity: number
  sand_source_id: string
  sand_quantity: number
  aggregate_source_id: string
  aggregate1_sub_category_id: string
  aggregate1_quantity: number
  aggregate2_sub_category_id: string
  aggregate2_quantity: number
  fly_ash_source_id: string
  fly_ash_quantity: number
  ad_mixture_brand_id: string
  ad_mixture_quantity: number
  water_quantity: number
  selling_price: number
  is_available: boolean
  description: string
  is_custom: boolean
  address_id: Mongoose.Types.ObjectId
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  vendor_id: Mongoose.Types.ObjectId
  cement_grade_id: Mongoose.Types.ObjectId
  ad_mixture_category_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  variant_date: Date
  [key: string]: any
}

const mongooseDesignMixVariantSchema = new Mongoose.Schema(
  {
    design_mix_id: Mongoose.Schema.Types.ObjectId,
    grade_id: Mongoose.Schema.Types.ObjectId,
    product_name: { type: String, index: true },
    cement_brand_id: Mongoose.Schema.Types.ObjectId,
    cement_quantity: Number,
    sand_source_id: Mongoose.Schema.Types.ObjectId,
    sand_quantity: Number,
    aggregate_source_id: Mongoose.Schema.Types.ObjectId,
    aggregate1_sub_category_id: Mongoose.Schema.Types.ObjectId,
    aggregate1_quantity: Number,
    aggregate2_sub_category_id: Mongoose.Schema.Types.ObjectId,
    aggregate2_quantity: Number,
    fly_ash_source_id: Mongoose.Schema.Types.ObjectId,
    fly_ash_quantity: Number,
    ad_mixture_brand_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture_quantity: Number,
    water_quantity: Number,
    selling_price: Number,
    is_available: { type: Boolean, default: true },
    description: String,
    is_custom: Boolean,
    address_id: Mongoose.Schema.Types.ObjectId,
    pickup_location: {
      type: { type: String, default: "Point" },
      coordinates: [Number, Number],
    },
    vendor_id: Mongoose.Schema.Types.ObjectId,
    cement_grade_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture_category_id: Mongoose.Schema.Types.ObjectId,
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    created_at: { type: Date, default: Date.now, index: true },
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    variant_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "design_mix_variant", versionKey: false }
)
  .index(
    {
      product_name: 1,
      vendor_id: 1,
    },
    { unique: true }
  )
  .index({
    pickup_location: "2dsphere",
  })
  .index({
    product_name: "text",
  })

export const DesignMixVariantModel: Mongoose.Model<IDesignMixVariantDoc> = Mongoose.model<
  IDesignMixVariantDoc
>("design_mix_variant", mongooseDesignMixVariantSchema)
