import * as Mongoose from "mongoose"
import { SmsLanguage } from "../utilities/config"

export interface ILogisticsUserDoc extends Mongoose.Document {
  user_id: string
  full_name: string
  mobile_number?: string
  mobile_verified: boolean
  email?: string
  email_verified?: boolean
  landline_number?: string
  company_type: string
  signup_type: string //facebook/google/mobile
  password?: string
  pan_number?: string
  pancard_image_url?: string
  gst_number?: string
  gst_certification_image_url: string
  verified_by_admin: boolean
  verified_at?: Date
  cst?: string
  annual_turnover?: number
  trade_capacity?: string
  number_of_employee?: number
  company_name?: string
  company_certification_number?: string
  company_certification_image_url?: string
  hse_number?: string
  ehs_as_per_iso?: string
  ohsas_as_per_iso?: string
  working_with_other_ecommerce?: boolean
  language?: string
  created_at: Date
  fax_number?: string
  website_URL?: string
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode: number
  // account_type: string // personal/company
}

const mongooseLogisticsSchema = new Mongoose.Schema(
  {
    user_id: String,
    full_name: String,
    mobile_number: String,
    mobile_verified: { type: Boolean, default: false },
    email: String,
    email_verified: { type: Boolean, default: false },
    landline_number: String,
    // account_type: String,
    company_type: String,
    signup_type: String,
    password: { type: String, index: true },
    pan_number: { type: String, unique: true },
    pancard_image_url: String,
    gst_number: String,
    gst_certification_image_url: String,
    verified_by_admin: { type: Boolean, index: true, default: false },
    verified_at: Date,
    cst: String,
    annual_turnover: Number,
    trade_capacity: String,
    number_of_employee: Number,
    company_name: String,
    company_certification_number: String,
    company_certification_image_url: String,
    hse_number: String,
    ehs_as_per_iso: String,
    ohsas_as_per_iso: String,
    working_with_other_ecommerce: Boolean,
    language: {
      type: String,
      enum: Object.values(SmsLanguage),
      default: SmsLanguage[0],
    },
    created_at: { type: Date, default: Date.now, index: true },
    fax_number: String,
    website_URL: String,
    pickup_location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
  },
  { collection: "logistics_user", versionKey: false }
)
  .index(
    { mobile_number: 1 },
    {
      unique: true,
      partialFilterExpression: { mobile_number: { $exists: true } },
    }
  )
  .index(
    { email: 1 },
    {
      unique: true,
      partialFilterExpression: { email: { $exists: true } },
    }
  )

export const LogisticsUserModel: Mongoose.Model<ILogisticsUserDoc> = Mongoose.model<
  ILogisticsUserDoc
>("logistics_user", mongooseLogisticsSchema)
