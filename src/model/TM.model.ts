import * as Mongoose from "mongoose"

export interface ITMCategoryDoc extends Mongoose.Document {
  category_name: string
  is_active: boolean
  created_at: Date
  updated_at: Date
  updated_by: string
}

export interface ITMSubCategoryDoc extends Mongoose.Document {
  TM_category_id: Mongoose.Types.ObjectId
  sub_category_name: string
  load_capacity: number
  weight_unit_code: string
  weight_unit: string // Metric tonne
  is_active: boolean
  created_at: string
}

export interface ITMDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  TM_category_id: Mongoose.Types.ObjectId
  TM_sub_category_id: Mongoose.Types.ObjectId
  TM_rc_number: string
  per_Cu_mtr_km: number
  min_trip_price: number
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  address_id: string
  delivery_range: number
  manufacturer_name: string
  TM_model: string
  manufacture_year: number
  is_gps_enabled: boolean
  is_insurance_active: boolean
  rc_book_image_url: string
  insurance_image_url: string
  TM_image_url: string
  is_active: boolean
  driver1_id: Mongoose.Types.ObjectId
  driver2_id: Mongoose.Types.ObjectId
  driver1_pic: string
  created_at: Date
  updated_at: Date
  min_order: number
  min_delivery_range: number
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  TM_date: Date
  [k: string]: any
}

const mongooseTMCategorySchema = new Mongoose.Schema(
  {
    category_name: {
      type: String,
      index: true,
      unique: true,
    },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "TM_category", versionKey: false }
)

const mongooseTMSubCategorySchema = new Mongoose.Schema(
  {
    TM_category: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "TM_category",
      index: true,
    },
    sub_category_name: {
      type: String,
      index: true,
    },
    TM_category_id: Mongoose.Schema.Types.ObjectId,
    load_capacity: Number,
    weight_unit_code: { type: String, default: "Cu.mtr" },
    weight_unit: { type: String, default: "cubic metre" },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "TM_sub_category", versionKey: false }
).index(
  {
    TM_category_id: 1,
    sub_category_name: 1,
  },
  { unique: true }
)

const mongooseTMSchema = new Mongoose.Schema(
  {
    user_id: Mongoose.Schema.Types.ObjectId,
    TM_category_id: Mongoose.Schema.Types.ObjectId,
    TM_sub_category_id: Mongoose.Schema.Types.ObjectId,
    TM_rc_number: { type: String, index: true, unique: true },
    per_Cu_mtr_km: Number,
    min_trip_price: Number,
    pickup_location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    address_id: Mongoose.Schema.Types.ObjectId,
    delivery_range: { type: Number, index: true },
    manufacturer_name: String,
    TM_model: String,
    manufacture_year: Number,
    is_gps_enabled: Boolean,
    is_insurance_active: Boolean,
    rc_book_image_url: String,
    insurance_image_url: String,
    TM_image_url: String,
    is_active: { type: Boolean, index: true, default: true },
    driver1_id: Mongoose.Schema.Types.ObjectId,
    driver2_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    min_order: Number,
    min_delivery_range: Number,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    TM_date: { type: Date, default: Date.now, index: true },
  },
  {
    collection: "TM",
    versionKey: false,
  }
).index({
  pickup_location: "2dsphere",
})

export const TMCategoryModel: Mongoose.Model<ITMCategoryDoc> = Mongoose.model<
  ITMCategoryDoc
>("TM_category", mongooseTMCategorySchema)

export const TMSubCategoryModel: Mongoose.Model<ITMSubCategoryDoc> = Mongoose.model<
  ITMSubCategoryDoc
>("TM_sub_category", mongooseTMSubCategorySchema)

export const TMModel: Mongoose.Model<ITMDoc> = Mongoose.model(
  "TM",
  mongooseTMSchema
)
