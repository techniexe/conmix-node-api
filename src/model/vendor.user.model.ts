import * as Mongoose from "mongoose"
import { SmsLanguage } from "../utilities/config"
import { AddressType } from "./address.model"

export interface IVendorUserDoc extends Mongoose.Document {
  user_id: string
  full_name: string
  mobile_number: string
  mobile_verified?: boolean
  email?: string
  email_verified?: boolean
  landline_number?: string
  // account_type: string // personal/company
  company_type: string
  signup_type: string //facebook/google/mobile
  password: string
  pan_number?: string
  pancard_image_url?: string
  gst_number?: string
  gst_certification_image_url: string
  verified_by_admin: boolean
  verified_at?: Date
  cst?: string
  annual_turnover?: number
  trade_capacity?: string
  number_of_employee?: number
  company_name: string
  company_certification_number?: string
  company_certification_image_url?: string
  hse_number?: string
  ehs_as_per_iso?: string
  ohsas_as_per_iso?: string
  working_with_other_ecommerce?: boolean
  language?: string
  region_served: [Mongoose.Types.ObjectId]
  created_at: Date
  rating_sum: number
  rating_count: number
  rating: number
  notification_count: number
  notification_opted?: boolean
  unseen_message_count: number
  current_year: string
  no_of_plants: number
  plant_capacity_per_hour: number
  no_of_operation_hour: number
  new_order_taken: boolean
  master_vendor_id: Mongoose.Types.ObjectId
  is_accepted: boolean
  is_blocked: boolean
  rejected_order: number
  plant_address_id: Mongoose.Types.ObjectId
  business_name: string
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode?: number
  address_type: AddressType //warehouse/factory/quarry/kapchi/home/office/
  region_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  billing_address_id: Mongoose.Types.ObjectId
  is_verified: boolean
  city_name: string
  state_name: string
  plant_cnt: number
  vendor_date: Date
  // region_name: [string]
  [k: string]: any
}

const mongooseVendorSchema = new Mongoose.Schema(
  {
    user_id: String,
    full_name: String,
    mobile_number: String,
    mobile_verified: { type: Boolean, default: false },
    email: String,
    email_verified: { type: Boolean, default: false },
    landline_number: String,
    // account_type: String,
    company_type: String,
    signup_type: String,
    password: { type: String, index: true },
    // pan_number: { type: String, unique: true },
    pan_number: { type: String },
    pancard_image_url: String,
    gst_number: String,
    gst_certification_image_url: String,
    verified_by_admin: { type: Boolean, index: true, default: false },
    verified_at: Date,
    cst: String,
    annual_turnover: Number,
    trade_capacity: String,
    number_of_employee: Number,
    company_name: String,
    company_certification_number: String,
    company_certification_image_url: String,
    hse_number: String,
    ehs_as_per_iso: String,
    ohsas_as_per_iso: String,
    working_with_other_ecommerce: Boolean,
    language: {
      type: String,
      enum: Object.values(SmsLanguage),
      default: SmsLanguage[0],
    },
    region_served: [Mongoose.Schema.Types.ObjectId],
    region_name: [String],
    created_at: { type: Date, default: Date.now, index: true },
    rating_sum: { type: Number, default: 0 },
    rating_count: { type: Number, default: 0 },
    rating: Number,
    notification_count: Number,
    notification_opted: Boolean,
    unseen_message_count: Number,
    current_year: String,
    no_of_plants: Number,
    plant_capacity_per_hour: Number,
    no_of_operation_hour: Number,
    new_order_taken: Boolean,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    is_accepted: { type: Boolean, default: true, index: true },
    is_blocked: { type: Boolean, default: false, index: true },
    rejected_order: Number,
    plant_address_id: Mongoose.Schema.Types.ObjectId,
    business_name: String,
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
    address_type: {
      type: String,
      index: true,
      enum: Object.values(AddressType),
    },
    region_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    billing_address_id: Mongoose.Schema.Types.ObjectId,
    city_name: String,
    state_name: String,
    plant_cnt: Number,
    vendor_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "vendor", versionKey: false }
)
.index({
  company_name: "text",
})

mongooseVendorSchema.index(
  { mobile_number: 1 },
  {
    unique: true,
    partialFilterExpression: { mobile_number: { $exists: true } },
  }
)
mongooseVendorSchema.index(
  { email: 1 },
  {
    unique: true,
    partialFilterExpression: { email: { $exists: true } },
  }
)

export const VendorUserModel: Mongoose.Model<IVendorUserDoc> = Mongoose.model<
  IVendorUserDoc
>("vendor", mongooseVendorSchema)
