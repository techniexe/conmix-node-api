import * as Mongoose from "mongoose"
export enum TransactionStatus {
  SUCCESS = "success",
  FAILED = "failed",
}

export interface ITransactionDoc extends Mongoose.Document {
  gateway_transaction_id: string
  user_id: Mongoose.Types.ObjectId
  order_id: Mongoose.Types.ObjectId
  total_amount: number
  currency_code: string
  payment_gateway: string
  transaction_status: TransactionStatus
  created_at: Date
  processed_at: Date
}

export interface ITransactionLogDoc extends Mongoose.Document {
  transaction_id: Mongoose.Types.ObjectId
  order_id: Mongoose.Types.ObjectId
  payment_gateway: string
  transaction_text: string
  operation: string
  created_at: Date
}

const mongooseTranasctionSchema = new Mongoose.Schema(
  {
    gateway_transaction_id: String,
    user_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    order_id: { type: String, index: true },
    total_amount: Number,
    currency_code: String,
    payment_gateway: String,
    transaction_status: {
      type: String,
      enum: Object.values(TransactionStatus),
    },
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "transaction", versionKey: false }
)

const mongooseTransactionLogSchema = new Mongoose.Schema(
  {
    transaction_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "transaction",
    },
    payment_gateway: { type: String, index: true },
    order_id: {
      type: String,
      ref: "order",
    },
    transaction_text: String,
    operation: String,
    created_at: { type: Date, index: true, default: Date.now },
  },
  { collection: "transaction_logs", versionKey: false }
)

export const TransactionModel: Mongoose.Model<ITransactionDoc> = Mongoose.model<
  ITransactionDoc
>("transaction", mongooseTranasctionSchema)

export const TransactionLogModel: Mongoose.Model<
  ITransactionLogDoc
> = Mongoose.model<ITransactionLogDoc>(
  "transaction_log",
  mongooseTransactionLogSchema
)
