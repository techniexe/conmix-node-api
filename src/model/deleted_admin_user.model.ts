import * as Mongoose from "mongoose"

export interface IAdminUserDoc extends Mongoose.Document {
  full_name: string
  mobile_number: string
  email: string
  admin_type: string // superAdmin, Admin,  buyer_admin, supplier_admin, logistics_admin (sales, support, account_manager)
  identity_image_url?: string
  password?: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_deleted: boolean
  deleted_at: Date
  deleted_by: Mongoose.Types.ObjectId
}

const mongooseDeletedAdminUserSchema = new Mongoose.Schema(
  {
    full_name: String,
    mobile_number: String,
    email: String,
    admin_type: String,
    identity_image_url: String,
    password: { type: String, index: true },
    created_by: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    created_at: { type: Date, default: Date.now },
    deleted_at: { type: Date, default: Date.now },
    is_deleted: { type: Boolean, default: true },
    deleted_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "deleted_admin", versionKey: false }
)
mongooseDeletedAdminUserSchema.index(
  { mobile_number: 1 },
  {
    unique: true,
    partialFilterExpression: { mobile_number: { $exists: true } },
  }
)

mongooseDeletedAdminUserSchema.index(
  { email: 1 },
  {
    unique: true,
    partialFilterExpression: { email: { $exists: true } },
  }
)

export const DeletedAdminUserModel: Mongoose.Model<IAdminUserDoc> = Mongoose.model<
  IAdminUserDoc
>("deleted_admin", mongooseDeletedAdminUserSchema)
