import * as Mongoose from "mongoose"
import { UserType } from "../utilities/config"

export interface ICouponDoc extends Mongoose.Document {
  code: string
  discount_type: string
  discount_value: number
  min_order: number
  max_discount: number
  max_usage: number
  remaining_usage?: number
  unique_use: boolean
  start_date: Date
  end_date: Date
  //supplier_ids: Mongoose.Types.ObjectId[]
  buyer_ids: Mongoose.Types.ObjectId[]
  info: string
  tnc: string
  created_by_id: Mongoose.Types.ObjectId
  updated_by_id?: Mongoose.Types.ObjectId
  created_at: Date
  updated_at?: Date
  is_deleted: boolean
  is_active: boolean
  deleted_by_id?: Mongoose.Types.ObjectId
  deleted_at?: Date
  created_by_type: UserType
  deleted_by_type: UserType
  updated_by_type: UserType
}

const couponMongooseSchema = new Mongoose.Schema(
  {
    code: { type: String, index: true, unique: true },
    discount_type: String,
    discount_value: Number,
    min_order: Number,
    max_discount: Number,
    max_usage: { type: Number, default: -1 },
    remaining_usage: { type: Number, default: -1 },
    unique_use: { type: Boolean, default: false },
    start_date: Date,
    end_date: Date,
    // supplier_ids: {
    //   type: [Mongoose.Schema.Types.ObjectId],
    //   ref: "supplier_user",
    //   index: true,
    // },
    buyer_ids: {
      type: [Mongoose.Schema.Types.ObjectId],
      ref: "buyer_user",
      index: true,
    },
    info: String,
    tnc: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    is_deleted: { type: Boolean, default: false, index: true },
    is_active: { type: Boolean, default: false, index: true },
    deleted_by_id: Mongoose.Schema.Types.ObjectId,
    deleted_at: Date,
    created_by_type: {
      type: String,
      enum: Object.values(UserType),
    },
    deleted_by_type: {
      type: String,
      enum: Object.values(UserType),
    },
    updated_by_type: {
      type: String,
      enum: Object.values(UserType),
    },
  },
  { collection: "coupon", versionKey: false }
)

export const CouponModel: Mongoose.Model<ICouponDoc> = Mongoose.model<
  ICouponDoc
>("coupon", couponMongooseSchema)
