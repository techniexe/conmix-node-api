import * as Mongoose from "mongoose"
import { BillStatus } from "../utilities/config"

// export enum BillStatus {
//   ACCEPTED = "ACCEPTED",
//   PENDING = "PENDING",
//   REJECTED = "REJECTED",
// }
export enum PaymentStatus {
  UNPAID = "UNPAID",
  PAID = "PAID",
}

// export enum DeliveryStatus {
//   DELIVERED = "DELIVERED",
//   PICKUP = "PICKUP",
//   INPROGRESS = "INPROGRESS",
//   CHECKOUT = "CHECKOUT",
//   DELAY = "DELAY",
//   TRUCK_ASSIGNED = "TRUCK_ASSIGNED",
//   ACCEPTED = "ACCEPTED",
//   REJECTED = "REJECTED",
// }

export enum LogisticsOrderStatus {
  // PROCESSING = "PROCESSING",
  // PROCESSED = "PROCESSED",
  // SHIPPED = "SHIPPED",
  // DELIVERED = "DELIVERED",
  // CANCELLED = "CANCELLED",
  // FAILED = "FAILED",
  // REFUNDED = "REFUNDED",
  // PICKUP = "PICKUP",
  // INPROGRESS = "INPROGRESS",
  // CHECKOUT = "CHECKOUT",
  // DELAY = "DELAY",
  // TRUCK_ASSIGNED = "TRUCK_ASSIGNED",
  // ACCEPTED = "ACCEPTED",
  // REJECTED = "REJECTED",

  RECEIVED = "RECEIVED",
  TM_ASSIGNED = "TM_ASSIGNED",
  PICKUP = "PICKUP",
  DELAYED = "DELAYED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
}

// export enum LogisticsOrderItemStatus {
//   PROCESSING = "PROCESSING",
//   PROCESSED = "PROCESSED",
//   SHIPPED = "SHIPPED",
//   DELIVERED = "DELIVERED",
//   CANCELLED = "CANCELLED",
//   FAILED = "FAILED",
//   REFUNDED = "REFUNDED",
//   PICKUP = "PICKUP",
//   INPROGRESS = "INPROGRESS",
//   CHECKOUT = "CHECKOUT",
//   DELAY = "DELAY",
//   TRUCK_ASSIGNED = "TRUCK_ASSIGNED",
//   ACCEPTED = "ACCEPTED",
//   REJECTED = "REJECTED",
// }

export interface IBill extends Mongoose.Document {
  url?: string
  uploaded_at?: Date
  status?: BillStatus
  verified_at?: Date
  reject_reason?: string
}

export interface ILogisticsOrderDoc extends Mongoose.Document {
  _id: string
  user_id: Mongoose.Types.ObjectId
  buyer_id: Mongoose.Types.ObjectId
  supplier_id: Mongoose.Types.ObjectId
  product_id: Mongoose.Types.ObjectId
  buyer_order_id: Mongoose.Types.ObjectId
  buyer_order_item_id: Mongoose.Types.ObjectId
  vehicle_id: Mongoose.Types.ObjectId
  // vehicle_category_id: Mongoose.Types.ObjectId
  vehicle_sub_category_id: Mongoose.Types.ObjectId
  delivery_status: LogisticsOrderStatus
  quantity: number
  pickup_quantity?: number
  royality_quantity?: number
  delivery_quantity?: number
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  igst_amount?: number
  cgst_amount?: number
  sgst_amount?: number
  total_amount: number
  created_at: Date
  bills: IBill[]
  creditNotes: IBill[]
  debitNotes: IBill[]
  admin_remarks?: string
  logistics_payment_status: PaymentStatus
  supplier_pickup_signature?: string
  driver_pickup_signature?: string
  buyer_drop_signature?: string
  driver_drop_signature?: string

  checkedout_at?: Date
  pickedup_at?: Date
  delivered_at?: Date
  reasonForDelay: string
  delayTime: string
  delayed_at?: Date
  cancelled_at?: Date
  royalty_pass_image_url: string
  way_slip_image_url: string
  challan_image_url: string
  quote_id: Mongoose.Types.ObjectId
  quote_display_id: string
}

const mongooseBillsSchema = new Mongoose.Schema({
  url: String,
  uploaded_at: Date,
  status: {
    type: String,
    enum: Object.values(BillStatus),
    default: BillStatus.PENDING,
  },
  verified_at: Date,
  reject_reason: String,
})

const mongooseLogisticsOrderSchema = new Mongoose.Schema(
  {
    _id: String,
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
      ref: "logistics_user",
    },
    buyer_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
      ref: "buyer",
    },
    supplier_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
      ref: "supplier",
    },
    product_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
      ref: "product",
    },
    buyer_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    buyer_order_item_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    vehicle_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
      ref: "vehicle",
    },
    //  vehicle_category_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    vehicle_sub_category_id: { type: Mongoose.Schema.Types.ObjectId },
    delivery_status: { type: String, index: true },
    quantity: Number,
    pickup_quantity: Number,
    royality_quantity: Number,
    delivery_quantity: Number,
    igst_rate: Number,
    cgst_rate: Number,
    sgst_rate: Number,
    igst_amount: Number,
    cgst_amount: Number,
    sgst_amount: Number,
    total_amount: Number,
    created_at: { type: Date, index: true, default: Date.now },
    bills: [mongooseBillsSchema],
    creditNotes: [mongooseBillsSchema],
    debitNotes: [mongooseBillsSchema],
    admin_remarks: String,
    logistics_payment_status: {
      type: String,
      enum: Object.values(PaymentStatus),
      default: PaymentStatus.UNPAID,
    },
    supplier_pickup_signature: String,
    driver_pickup_signature: String,
    buyer_drop_signature: String,
    driver_drop_signature: String,
    checkedout_at: Date,
    pickedup_at: Date,
    delivered_at: Date,
    cancelled_at: Date,
    reasonForDelay: String,
    delayTime: String,
    delayed_at: Date,
    royalty_pass_image_url: String,
    way_slip_image_url: String,
    challan_image_url: String,
    quote_id: Mongoose.Schema.Types.ObjectId,
    quote_display_id: String,
  },
  { collection: "logistics_order", versionKey: false }
)

export const LogisticsOrderModel: Mongoose.Model<ILogisticsOrderDoc> = Mongoose.model(
  "logistics_order",
  mongooseLogisticsOrderSchema
)
