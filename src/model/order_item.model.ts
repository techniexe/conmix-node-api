import { PaymentStatus } from "./../utilities/config"
import * as Mongoose from "mongoose"
import { OrderItemStatus } from "../utilities/config"
import { AddressType } from "./address.model"

// interface IVehicles extends Mongoose.Document {
//   vehicle_sub_category_id: Mongoose.Types.ObjectId
//   weight_unit_code: string
//   load_quantity: number
//   trip_price: number
//   count: number
// }

export enum OrderItemQuoteStatus {
  // NOTPLACED = "NOTPLACED",
  // PLACING = "PLACING",
  // PLACED = "PLACED",

  NOT_ASSIGNED = "NOT_ASSIGNED",
  UNDER_BIDDING = "UNDER_BIDDING",
  ASSIGNED = "ASSIGNED",
}

export interface IOrderItemDoc extends Mongoose.Document {
  display_item_id: string
  design_mix_id: string
  concrete_grade_id: Mongoose.Types.ObjectId
  admix_brand_id: Mongoose.Types.ObjectId
  admix_cat_id: Mongoose.Types.ObjectId
  fly_ash_source_id: Mongoose.Types.ObjectId
  aggregate2_sub_cat_id: Mongoose.Types.ObjectId
  agg_source_id: Mongoose.Types.ObjectId
  aggregate1_sub_cat_id: Mongoose.Types.ObjectId
  sand_source_id: Mongoose.Types.ObjectId
  cement_brand_id: Mongoose.Types.ObjectId
  cement_grade_id: Mongoose.Types.ObjectId
  cement_quantity: number
  sand_quantity: number
  aggregate1_quantity: number
  aggregate2_quantity: number
  fly_ash_quantity: number
  admix_quantity: number
  water_quantity: number
  vendor_id: Mongoose.Types.ObjectId
  buyer_id: Mongoose.Types.ObjectId
  order_id: Mongoose.Types.ObjectId
  quantity: number
  cement_per_kg_rate: number
  sand_per_kg_rate: number
  aggregate1_per_kg_rate: number
  aggregate2_per_kg_rate: number
  fly_ash_per_kg_rate: number
  admix_per_kg_rate: number
  water_per_ltr_rate: number
  cement_price: number
  sand_price: number
  aggreagte1_price: number
  aggreagte2_price: number
  fly_ash_price: number
  admix_price: number
  water_price: number
  with_TM: boolean
  with_CP: boolean
  delivery_date: Date
  end_date: Date
  distance: number
  unit_price: number
  selling_price: number
  margin_price: number
  TM_price: number
  CP_price: number
  total_CP_price: number
  gst_type: string // IGST/ SGST,CGST
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  igst_price: number
  cgst_price: number
  sgst_price: number
  gst_price: number
  item_status: string
  remaining_quantity: number
  temp_quantity: number
  pickup_quantity: number
  part_quantity: number
  revised_quantity: number
  // quantity_unit: string
  //unit_price: number
  // base_amount: number
  // selling_price: number
  // margin_rate: number
  // margin_price: number
  // gst_type: string // IGST/ SGST,CGST
  // igst_rate: number
  // cgst_rate: number
  // sgst_rate: number
  //igst_amount?: number
  //sgst_amount?: number
  //cgst_amount?: number
  //logistics_amount: number

  // pickup_location: ILocation
  // dvlDistance: number
  // item_status: string
  admin_remarks: string
  created_at: Date
  created_by_id: string
  created_by_type: string
  payment_status: PaymentStatus
  address_id: Mongoose.Types.ObjectId
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode?: number
  location: {
    type: string
    coordinates: [number, number]
  }
  address_type: AddressType //warehouse/factory/quarry/kapchi/home/office/
  business_name: string
  city_name: string
  state_name: string
  total_amount: number
  pickedup_at: Date
  TM_no?: string
  driver_name?: string
  driver_mobile?: string
  TM_operator_name?: string
  TM_operator_mobile_no?: string
  selling_price_With_Margin: any
  unit_price_With_Margin: number
  refund_id: string
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  ad_mixture1_brand_id: Mongoose.Types.ObjectId
  ad_mixture1_category_id: Mongoose.Types.ObjectId
  ad_mixture2_brand_id: Mongoose.Types.ObjectId
  ad_mixture2_category_id: Mongoose.Types.ObjectId
  // cancelled_at?: Date
  // requiredVehicles: IVehicles[]
  // item_quote_status: OrderItemQuoteStatus
  // delivered_at: Date
  // processed_at?: Date
  //assigned_at: Date
  //pickedup_at: Date
  //delayed_at: Date
}
const mongooseOrderItemSchema = new Mongoose.Schema(
  {
    display_item_id: { type: String, index: true, unique: true },
    design_mix_id: { type: Mongoose.Schema.Types.ObjectId },
    concrete_grade_id: Mongoose.Schema.Types.ObjectId,
    admix_brand_id: Mongoose.Schema.Types.ObjectId,
    admix_cat_id: Mongoose.Schema.Types.ObjectId,
    fly_ash_source_id: Mongoose.Schema.Types.ObjectId,
    aggregate2_sub_cat_id: Mongoose.Schema.Types.ObjectId,
    agg_source_id: Mongoose.Schema.Types.ObjectId,
    aggregate1_sub_cat_id: Mongoose.Schema.Types.ObjectId,
    sand_source_id: Mongoose.Schema.Types.ObjectId,
    cement_brand_id: Mongoose.Schema.Types.ObjectId,
    cement_grade_id: Mongoose.Schema.Types.ObjectId,
    cement_quantity: Number,
    sand_quantity: Number,
    aggregate1_quantity: Number,
    aggregate2_quantity: Number,
    fly_ash_quantity: Number,
    admix_quantity: Number,
    water_quantity: Number,
    vendor_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    buyer_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    order_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    quantity: Number,
    cement_per_kg_rate: Number,
    sand_per_kg_rate: Number,
    aggregate1_per_kg_rate: Number,
    aggregate2_per_kg_rate: Number,
    fly_ash_per_kg_rate: Number,
    admix_per_kg_rate: Number,
    water_per_ltr_rate: Number,
    cement_price: Number,
    sand_price: Number,
    aggreagte1_price: Number,
    aggreagte2_price: Number,
    fly_ash_price: Number,
    admix_price: Number,
    water_price: Number,
    with_TM: Boolean,
    with_CP: Boolean,
    delivery_date: Date,
    end_date: Date,
    distance: Number,
    unit_price: Number,
    selling_price: Number,
    margin_price: Number,
    selling_price_With_Margin: Number,
    unit_price_With_Margin: Number,
    TM_price: Number,
    CP_price: Number,
    total_CP_price: Number,
    gst_type: String,
    igst_rate: Number,
    cgst_rate: Number,
    sgst_rate: Number,
    igst_price: Number,
    cgst_price: Number,
    sgst_price: Number,
    gst_price: Number,
    item_status: { type: String, default: OrderItemStatus.PROCESSING },
    address_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    address_type: {
      type: String,
      index: true,
      enum: Object.values(AddressType),
    },
    business_name: String,
    city_name: String,
    state_name: String,
    remaining_quantity: Number,
    temp_quantity: Number,
    pickup_quantity: { type: Number, default: 0 },
    part_quantity: Number,
    revised_quantity: Number,
    //quantity_unit: { type: String, default: "MT" },
    // unit_price: Number,
    //base_amount: Number,
    //  selling_price: Number,
    // margin_price: Number,
    //margin_rate: Number,
    //logistics_amount: Number,
    //total_amount: Number,
    //pickup_location: {
    //   type: { type: String, default: "Point" },
    //   coordinates: { type: [Number], default: [0, 0] },
    // },
    // dvlDistance: Number,
    admin_remarks: String,
    created_at: { type: Date, index: true, default: Date.now },
    created_by_id: String,
    created_by_type: String,
    cancelled_at: Date,
    payment_status: {
      type: String,
      enum: Object.values(PaymentStatus),
      index: true,
      default: PaymentStatus.UNPAID,
    },
    total_amount: Number,
    pickedup_at: Date,
    TM_no: String,
    driver_name: String,
    driver_mobile: String,
    TM_operator_name: String,
    TM_operator_mobile_no: String,
    refund_id: String,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture1_brand_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture1_category_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture2_brand_id: Mongoose.Schema.Types.ObjectId,
    ad_mixture2_category_id: Mongoose.Schema.Types.ObjectId,
    // requiredVehicles: [
    //   {
    //     vehicle_sub_category_id: Mongoose.Schema.Types.ObjectId,
    //     weight_unit_code: String,
    //     load_quantity: Number,
    //     trip_price: Number,
    //     count: Number,
    //   },
    // ],
    // item_quote_status: {
    //   type: String,
    //   index: true,
    //   enum: Object.values(OrderItemQuoteStatus),
    //   default: OrderItemQuoteStatus.NOT_ASSIGNED,
    // },
    // delivered_at: Date,
    // processed_at: Date,
    // assigned_at: Date,
    // pickedup_at: Date,
    // delayed_at: Date,
  },
  { collection: "order_item", versionKey: false }
)
export const OrderItemModel: Mongoose.Model<IOrderItemDoc> = Mongoose.model<
  IOrderItemDoc
>("order_item", mongooseOrderItemSchema)

OrderItemModel.init()
