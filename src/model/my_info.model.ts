import * as Mongoose from "mongoose"

export interface IMyInfokDoc extends Mongoose.Document {
  first_name: string
  last_name: string
  number: number
  [k: string]: any
}

const mongooseMyInfoSchema = new Mongoose.Schema(
  {
    first_name: String,
    last_name: String,
    number: Number,
  },
  { collection: "my_info", versionKey: false }
)

export const MyInfoModel: Mongoose.Model<IMyInfokDoc> = Mongoose.model<
  IMyInfokDoc
>("my_info", mongooseMyInfoSchema)
