import * as Mongoose from "mongoose"
import "../utilities/db"

export enum emailVerificationType {
  PROFILE = "profile",
  CONTACT = "contact",
  SITE = "site",
  REGISTER = "register",
}
export interface IAuthDoc extends Mongoose.Document {
  identifier: string
  code: string
  user_type: string
  created_at: Date
  user_id: Mongoose.Types.ObjectId
}

export interface IAuthEmailDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  email: string
  auth_token: string
  verification_type: emailVerificationType
  created_at: string
}
const mongooseAuthSchema = new Mongoose.Schema(
  {
    identifier: String,
    code: String,
    user_type: String,
    created_at: { type: Date, default: Date.now },
    user_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
  },
  { collection: "auth_codes", versionKey: false }
)
  .index(
    {
      identifier: 1,
      user_type: 1,
      verification_type: 1,
    },
    { unique: true }
  )
  .index({ created_at: 1 }, { expireAfterSeconds: 300 }) //1 Hr

const mongooseEmailAuthSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/supplier/logistics
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    email: String,
    auth_token: { type: String, index: true, unique: true },
    verification_type: {
      type: String,
      enum: Object.values(emailVerificationType),
    },
    created_at: { type: Date, default: Date.now },
  },
  { collection: "email_auth_tokens", versionKey: false }
)
  .index(
    {
      "user.user_type": 1,
      verification_type: 1,
      email: 1,
    },
    { unique: true }
  )
  .index({ created_at: 1 }, { expireAfterSeconds: 86400 }) // 24 Hours

export const AuthModel: Mongoose.Model<IAuthDoc> = Mongoose.model(
  "auth_codes",
  mongooseAuthSchema
)

export const AuthEmailModel: Mongoose.Model<IAuthEmailDoc> = Mongoose.model(
  "email_auth_tokens",
  mongooseEmailAuthSchema
)
