import * as Mongoose from "mongoose"

export interface IAggregateSourceRateDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  sub_cat_id: Mongoose.Types.ObjectId
  per_kg_rate: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
}

const MongooseAggregateSourceRateSchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    sub_cat_id: Mongoose.Schema.Types.ObjectId,
    per_kg_rate: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "aggregate_source_rate", versionKey: false }
)

export const AggregateSourceRateModel: Mongoose.Model<IAggregateSourceRateDoc> = Mongoose.model<
  IAggregateSourceRateDoc
>("aggregate_source_rate", MongooseAggregateSourceRateSchema)
