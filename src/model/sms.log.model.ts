import * as Mongoose from "mongoose"

export interface ISmsLogDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  text: string
  to_mobileNumber: string
  sent_at: Date
  send_status: string
  send_error?: string
}

const mongooseSmsLogDoc = new Mongoose.Schema(
  {
    user: {
      user_type: String,
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    text: String,
    to_mobileNumber: String,
    sent_at: { type: Date, default: Date.now, index: true },
    send_status: String,
    send_error: String,
  },
  { collection: "sms_log", versionKey: false }
)

export const SmsLogModel: Mongoose.Model<ISmsLogDoc> = Mongoose.model<
  ISmsLogDoc
>("sms_log", mongooseSmsLogDoc)
