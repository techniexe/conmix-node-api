import * as Mongoose from "mongoose"

export interface IDesignMixDoc extends Mongoose.Document {
  grade_id: Mongoose.Types.ObjectId
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
  address_id: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  design_mix_date: Date
  [k: string]: any
}

const MongooseDesignMixSchema = new Mongoose.Schema(
  {
    grade_id: Mongoose.Schema.Types.ObjectId,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    address_id: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    design_mix_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "design_mixture", versionKey: false }
).index(
  {
    grade_id: 1,
    vendor_id: 1,
  },
  { unique: true }
)

export const DesignMixtureModel: Mongoose.Model<IDesignMixDoc> = Mongoose.model<
  IDesignMixDoc
>("design_mixture", MongooseDesignMixSchema)
