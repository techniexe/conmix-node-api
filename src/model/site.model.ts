import * as Mongoose from "mongoose"

export interface ISiteDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  site_name: string
  company_name: string
  address_line1: string
  address_line2: string
  country_id: number
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  sub_city_id: Mongoose.Types.ObjectId
  pincode: number
  location: {
    type: string
    coordinates: [number, number]
  }
  person_name: string
  title?: string
  mobile_number_verified?: boolean
  email_verified?: boolean
  email: string
  mobile_number: string
  alt_mobile_number?: string
  alt_mobile_number_verified?: boolean
  whatsapp_number?: string
  whatsapp_number_verified?: boolean
  landline_number?: string
  profile_pic?: string
  is_deleted: boolean
  deleted_at?: Date 
  billing_address_id: Mongoose.Types.ObjectId
}

const mongooseSiteSchema = new Mongoose.Schema(
  {
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      index: true,
      alias: "userDetails",
      autopopulate: { select: "full_name , _id" },
    },
    site_name: String,
    company_name: String,
    address_line1: String,
    address_line2: String,
    country_id: Number,
    state_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "state",
    },
    city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "city",
    },
    sub_city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "sub_city",
    },
    pincode: Number,
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    person_name: String,
    title: String,
    email: String,
    email_verified: { type: Boolean, default: false },
    mobile_number_verified: { type: Boolean, default: false },
    mobile_number: String,
    alt_mobile_number: String,
    alt_mobile_number_verified: { type: Boolean, default: false },
    whatsapp_number: String,
    whatsapp_number_verified: { type: Boolean, default: false },
    landline_number: String,
    profile_pic: String,
    created_at: { type: Date, default: Date.now, index: true },
    is_deleted: { type: Boolean, default: false, index: true },
    deleted_at: Date,
    billing_address_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "site", versionKey: false }
)
  .index(
    {
      user_id: 1,
      site_name: 1,
    },
    { unique: true }
  )
  .index({ location: "2dsphere" })

export const SiteModel: Mongoose.Model<ISiteDoc> = Mongoose.model<ISiteDoc>(
  "site",
  mongooseSiteSchema
)
