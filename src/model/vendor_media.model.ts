import * as Mongoose from "mongoose"

export interface IVendorMedia extends Mongoose.Document {
  vendor_id: Mongoose.Types.ObjectId
  media_type: string
  media_url: string
  type: string
  created_at: Date
  created_by_type: string
}

const mongooseVendorMediaSchema = new Mongoose.Schema(
  {
    vendor_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    media_type: { type: String },
    media_url: { type: String },
    type: String,
    created_at: { type: Date, default: Date.now },
    created_by_type: String,
  },
  { collection: "vendor_media", versionKey: false }
)

export const VendorMediaModel: Mongoose.Model<IVendorMedia> = Mongoose.model<
  IVendorMedia
>("vendor_media", mongooseVendorMediaSchema)
