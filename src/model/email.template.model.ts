import * as Mongoose from "mongoose"

export interface IEmailTemplateDoc extends Mongoose.Document {
  template_type: string // welcome_user/new_order/new_invoice/payment_received/item_dispatched/item_delivered
  html: string
  subject: string
  cc?: string
  bcc?: string
  from_email: string
  from_name?: string
  headers?: [{ header_name: string; header_value: string }]
  attachments?: [
    {
      attachment_url: string
      attachment_name: string
      attachment_type: string
      size: number
    }
  ]
  created_at: Date
  updated_at?: Date
  [k: string]: any
}

const mongooseEmailTemplateSchema = new Mongoose.Schema(
  {
    template_type: { type: String, index: true, unique: true },
    html: String,
    subject: String,
    cc: String,
    bcc: String,
    from_email: String,
    from_name: String,
    headers: [{ header_name: String, header_value: String }],
    attachments: [
      {
        attachment_url: String,
        attachment_name: String,
        attachment_type: String,
        size: Number,
      },
    ],
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
  },
  { collection: "email_template", versionKey: false }
)

export const EmailTemplateModel: Mongoose.Model<IEmailTemplateDoc> = Mongoose.model<
  IEmailTemplateDoc
>("email_template", mongooseEmailTemplateSchema)
