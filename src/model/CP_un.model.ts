import * as Mongoose from "mongoose"

export interface ICP_unDoc extends Mongoose.Document {
  CPId: Mongoose.Types.ObjectId
  unavailable_at: Date
  start_time: Date
  end_time: Date
  vendor_id: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
}

const MongooseCP_unSchema = new Mongoose.Schema(
  {
    CPId: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    unavailable_at: Date,
    start_time: Date,
    end_time: Date,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
  },
  { collection: "CP_un", versionKey: false }
)

export const CP_unModel: Mongoose.Model<ICP_unDoc> = Mongoose.model<ICP_unDoc>(
  "CP_un",
  MongooseCP_unSchema
)
