import * as Mongoose from "mongoose"

export interface IVehicleRateDoc extends Mongoose.Document {
  state_id: Mongoose.Types.ObjectId
  state_name: string
  per_metric_ton_per_km_rate: number
  region_type: string
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
}

const MongooseVehicleRateSchema = new Mongoose.Schema(
  {
    state_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    state_name: String,
    per_metric_ton_per_km_rate: Number,
    region_type: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
  },
  { collection: "vehicle_rate", versionKey: false }
)

export const VehicleRateModel: Mongoose.Model<IVehicleRateDoc> = Mongoose.model<
  IVehicleRateDoc
>("vehicle_rate", MongooseVehicleRateSchema)
