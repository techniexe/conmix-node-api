import * as Mongoose from "mongoose"

export interface IConcretePumpCategoryDoc extends Mongoose.Document {
  category_name: string
  is_active: boolean
  created_at: Date
  updated_at: Date
  updated_by: string
}

export interface IConcretePumpDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  concrete_pump_category_id: Mongoose.Types.ObjectId
  company_name: string
  concrete_pump_model: string
  manufacture_year: number
  pipe_connection: number
  concrete_pump_capacity: number
  is_Operator: boolean
  operator_id: Mongoose.Types.ObjectId
  is_helper: boolean
  transportation_charge: number
  concrete_pump_price: number
  concrete_pump_image_url: string
  serial_number: string
  is_active: boolean
  created_at: Date
  updated_at: Date
  address_id: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  CP_date: Date
}

const mongooseConcretePumpCategorySchema = new Mongoose.Schema(
  {
    category_name: {
      type: String,
      index: true,
      unique: true,
    },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "concrete_pump_category", versionKey: false }
)

const mongooseConcretePumpSchema = new Mongoose.Schema(
  {
    user_id: Mongoose.Schema.Types.ObjectId,
    concrete_pump_category_id: Mongoose.Schema.Types.ObjectId,
    company_name: String,
    concrete_pump_model: String,
    manufacture_year: Number,
    pipe_connection: Number,
    concrete_pump_capacity: Number,
    is_Operator: Boolean,
    operator_id: Mongoose.Schema.Types.ObjectId,
    is_helper: Boolean,
    transportation_charge: Number,
    concrete_pump_price: Number,
    concrete_pump_image_url: String,
    serial_number: { type: String, unique: true },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    address_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    CP_date: { type: Date, default: Date.now, index: true },
  },
  {
    collection: "concrete_pump",
    versionKey: false,
  }
)

export const ConcretePumpCategoryModel: Mongoose.Model<IConcretePumpCategoryDoc> = Mongoose.model<
  IConcretePumpCategoryDoc
>("concrete_pump_category", mongooseConcretePumpCategorySchema)

export const ConcretePumpModel: Mongoose.Model<IConcretePumpDoc> = Mongoose.model(
  "concrete_pump",
  mongooseConcretePumpSchema
)
