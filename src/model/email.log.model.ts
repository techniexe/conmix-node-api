import * as Mongoose from "mongoose"

export interface IEmailLogDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  to_email: string
  to_name?: string
  from_email: string
  from_name?: string
  html: string
  subject: string
  cc?: string
  bcc?: string
  headers?: [{ header_name: string; header_value: string }]
  sent_at: Date
  send_status: string
  send_error?: string
}

const mongooseEmailLogDoc = new Mongoose.Schema(
  {
    user: {
      user_type: String,
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    to_email: String,
    to_name: String,
    from_email: String,
    from_name: String,
    html: String,
    subject: String,
    cc: String,
    bcc: String,
    headers: [{ header_name: String, header_value: String }],
    sent_at: { type: Date, default: Date.now, index: true },
    send_status: String,
    send_error: String,
  },
  { collection: "email_log", versionKey: false }
)

export const EmailLogModel: Mongoose.Model<IEmailLogDoc> = Mongoose.model<
  IEmailLogDoc
>("email_log", mongooseEmailLogDoc)
