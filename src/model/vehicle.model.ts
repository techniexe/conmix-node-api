import * as Mongoose from "mongoose"
import { InvalidInput } from "../utilities/customError"
export interface IVehicleCategoryDoc extends Mongoose.Document {
  category_name: string
  is_active: boolean
  created_at: Date
  updated_at: Date
  updated_by: string
}

export interface IVehicleSubCategoryDoc extends Mongoose.Document {
  vehicle_category_id: Mongoose.Types.ObjectId
  sub_category_name: string
  min_load_capacity: number
  max_load_capacity: number
  weight_unit_code: string
  weight_unit: string // Metric tonne
  is_active: boolean
  created_at: string
}

export interface IVehicleDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  vehicle_category_id: Mongoose.Types.ObjectId
  vehicle_sub_category_id: Mongoose.Types.ObjectId
  vehicle_rc_number: string
  per_metric_ton_per_km_rate: number
  min_trip_price: number
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode: number
  delivery_range: number
  manufacturer_name: string
  vehicle_model: string
  manufacture_year: number
  is_gps_enabled: boolean
  is_insurance_active: boolean
  rc_book_image_url: string
  insurance_image_url: string
  vehicle_image_url: string
  is_active: boolean
  driver1_id: Mongoose.Types.ObjectId
  driver2_id: Mongoose.Types.ObjectId
  // driver_name: string
  // driver_mobile_number: string
  // driver_alt_mobile_number: string
  // driver_whatsapp_number: string
  // driver_pic: string
  // driver1_name: string
  // driver1_mobile_number: string
  // driver1_alt_mobile_number: string
  // driver1_whatsapp_number: string
  driver1_pic: string
  created_at: Date
  updated_at: Date
  min_order: number
  min_delivery_range: number
}

const mongooseVehicleSchema = new Mongoose.Schema(
  {
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "logistics_user",
      index: true,
    },
    vehicle_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "vehicle_category",
      index: true,
    },
    vehicle_sub_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "vehicle_sub_category",
      index: true,
    },
    vehicle_rc_number: { type: String, index: true },
    per_metric_ton_per_km_rate: Number,
    min_trip_price: Number,
    pickup_location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
    delivery_range: { type: Number, index: true },
    manufacturer_name: String,
    vehicle_model: String,
    manufacture_year: Number,
    is_gps_enabled: Boolean,
    is_insurance_active: Boolean,
    rc_book_image_url: String,
    insurance_image_url: String,
    vehicle_image_url: String,
    is_active: { type: Boolean, index: true, default: true },
    driver1_id: Mongoose.Schema.Types.ObjectId,
    driver2_id: Mongoose.Schema.Types.ObjectId,
    // driver_name: String,
    // driver_mobile_number: String,
    // driver_alt_mobile_number: String,
    // driver_whatsapp_number: String,
    // driver_pic: String,
    // driver1_name: String,
    // driver1_mobile_number: String,
    // driver1_alt_mobile_number: String,
    // driver1_whatsapp_number: String,
    // driver1_pic: String,
    created_at: { type: Date, default: Date.now, index: true },
    min_order: Number,
    min_delivery_range: Number,
  },
  {
    collection: "vehicle",
    versionKey: false,
  }
).index({
  pickup_location: "2dsphere",
})

const mongooseVehicleCategorySchema = new Mongoose.Schema(
  {
    category_name: {
      type: String,
      index: true,
      unique: true,
    },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "vehicle_category", versionKey: false }
)

const mongooseVehicleSubCategorySchema = new Mongoose.Schema(
  {
    vehicle_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "vehicle_category",
      index: true,
    },
    sub_category_name: {
      type: String,
      index: true,
    },
    min_load_capacity: Number,
    max_load_capacity: Number,
    weight_unit_code: { type: String, default: "MT" },
    weight_unit: { type: String, default: "Metric Tonne" },
    is_active: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "vehicle_sub_category", versionKey: false }
).index(
  {
    vehicle_category_id: 1,
    sub_category_name: 1,
  },
  { unique: true }
)

export const VehicleModel: Mongoose.Model<IVehicleDoc> = Mongoose.model(
  "vehicle",
  mongooseVehicleSchema
)

export const VehicleCategoryeModel: Mongoose.Model<IVehicleCategoryDoc> = Mongoose.model<
  IVehicleCategoryDoc
>("vehicle_category", mongooseVehicleCategorySchema)

export const VehicleSubCategoryModel: Mongoose.Model<IVehicleSubCategoryDoc> = Mongoose.model<
  IVehicleSubCategoryDoc
>("vehicle_sub_category", mongooseVehicleSubCategorySchema)

mongooseVehicleSubCategorySchema.pre<IVehicleSubCategoryDoc>(
  "save",
  async function (next: Mongoose.HookErrorCallback) {
    const catData = await VehicleCategoryeModel.findById(
      this.vehicle_category_id
    )
    if (catData === null) {
      const err1 = new InvalidInput(`No such category exists.`)
      err1.httpStatusCode = 400
      return next(err1)
    }
    return next()
  }
)
mongooseVehicleSchema.pre<IVehicleDoc>("save", async function (
  next: Mongoose.HookNextFunction
) {
  console.log(this.vehicle_sub_category_id)
  console.log(this.vehicle_category_id)
  const catCheck = await VehicleSubCategoryModel.findOne({
    _id: this.vehicle_sub_category_id,
    vehicle_category_id: this.vehicle_category_id,
  })
  if (catCheck === null) {
    return next(
      new InvalidInput(
        `Vehicle Category and Vehicle Sub Category doesn't exists `,
        400
      )
    )
  }
  next()
})
