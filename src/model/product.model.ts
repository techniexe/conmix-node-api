import * as Mongoose from "mongoose"
import { ContactModel } from "./contact.model"
import { InvalidInput } from "../utilities/customError"
import { isNullOrUndefined } from "../utilities/type-guards"

interface IProductMedia {
  image_url: string
  thumbnail_url: string
  thumbnail_low_url: string
}

export interface IProductDoc extends Mongoose.Document {
  name: string
  media: IProductMedia[]
  user_id: Mongoose.Types.ObjectId
  category_id: Mongoose.Types.ObjectId
  sub_category_id: Mongoose.Types.ObjectId
  /** available quantity */
  quantity: number
  minimum_order: number
  unit_price: number
  pickup_address_id: Mongoose.Types.ObjectId
  self_logistics: boolean
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  contact_person_id: Mongoose.Types.ObjectId
  verified_by_admin: boolean
  verified_at?: Date
  created_at: Date
  //is_deleted: boolean
  is_available: boolean
  //deleted_at?: Date
  rating_sum: number
  rating_count: number
  rating: number
  region_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
  stock_updated_at: Date
}

const productMedia = new Mongoose.Schema({
  image_url: { type: String },
  thumbnail_url: { type: String },
  thumbnail_low_url: { type: String },
})

const mongooseProductSchema = new Mongoose.Schema(
  {
    name: String,
    media: [productMedia],
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "supplier",
      index: true,
    },
    category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "aggregate_sand_category",
      index: true,
    },
    sub_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "product_sub_category",
      index: true,
    },
    /** available quantity */
    quantity: Number, // available quantity
    minimum_order: Number,
    unit_price: Number,
    pickup_address_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "address",
      index: true,
    },
    pickup_location: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    self_logistics: { type: Boolean, default: false },
    contact_person_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "contact",
    },
    verified_by_admin: { type: Boolean, default: false, index: true },
    verified_at: Date,
    created_at: { type: Date, default: Date.now, index: true },
    //is_deleted: { type: Boolean, default: false, index: true },
    is_available: { type: Boolean, default: true, index: true },
    //deleted_at: Date,
    rating_sum: { type: Number, default: 0 },
    rating_count: { type: Number, default: 0 },
    rating: Number,
    region_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    stock_updated_at: Date,
  },
  { collection: "product", versionKey: false }
)
  .index(
    {
      sub_category_id: 1,
      pickup_address_id: 1,
    },
    { unique: true }
  )
  .index({
    pickup_location: "2dsphere",
  })
  .index({
    name: "text",
  })

mongooseProductSchema.pre<IProductDoc>("save", async function (
  next: Mongoose.HookErrorCallback
) {
  const conatctpersonCheck = await ContactModel.findOne({
    _id: this.contact_person_id,
    "user.user_id": this.user_id,
    "user.user_type": "supplier",
  })
  if (conatctpersonCheck === null) {
    const err1 = new InvalidInput(`No such conatct person exists.`)
    err1.httpStatusCode = 400
    return next(err1)
  }
  return next()
})

mongooseProductSchema.pre<any>("updateOne", async function (
  next: Mongoose.HookNextFunction
) {
  const updtVal = this.getUpdate()
  if (isNullOrUndefined(updtVal.$set)) {
    return next()
  }

  const query = this.getQuery()
  const setVal = updtVal.$set

  if (!isNullOrUndefined(setVal.contact_person_id)) {
    const conatctpersonCheck = await ContactModel.findOne({
      _id: setVal.contact_person_id,
      user_id: query.user_id,
    })
    if (conatctpersonCheck === null) {
      const err1 = new InvalidInput(`No such product exists.`)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }
  return next()
})

export const ProductModel: Mongoose.Model<IProductDoc> = Mongoose.model<
  IProductDoc
>("product", mongooseProductSchema)
