import * as Mongoose from "mongoose"

export interface IBuyerUserDoc extends Mongoose.Document {
  user_id: string
  account_type: string // builder/contractor
  company_type: string
  company_name: string
  full_name: string
  mobile_number: string
  mobile_verified?: boolean
  email: string
  email_verified?: boolean
  signup_type: string //facebook/google/mobile
  password: string
  gst_number?: string
  pan_number?: string
  created_at: Date
  identity_image_url?: string
  landline_number?: string
  notification_count: number
  notification_opted?: boolean
  unseen_message_count: number
  pay_method_id: string
  buyer_date: Date
}

const mongooseBuyerUserSchema = new Mongoose.Schema(
  {
    user_id: String,
    account_type: String,
    company_type: String,
    company_name: String,
    full_name: String,
    mobile_number: String,
    mobile_verified: { type: Boolean, default: false },
    email: String,
    email_verified: { type: Boolean, default: false },
    signup_type: String,
    password: { type: String, index: true },
    gst_number: String,
    pan_number: String,
    created_at: { type: Date, default: Date.now },
    identity_image_url: String,
    landline_number: String,
    notification_count: { type: Number, default: 0 },
    notification_opted: { type: Boolean, default: true },
    unseen_message_count: { type: Number, default: 0 },
    pay_method_id: String,
    buyer_date: { type: Date, default: Date.now },
  },
  { collection: "buyer", versionKey: false }
)
.index({
  full_name: "text",
})
.index({
  company_name: "text",
})
  .index(
    { mobile_number: 1 },
    {
      unique: true,
      partialFilterExpression: { mobile_number: { $exists: true } },
    }
  )
  .index(
    { email: 1 },
    {
      unique: true,
      partialFilterExpression: { email: { $exists: true } },
    }
  )

export const BuyerUserModel: Mongoose.Model<IBuyerUserDoc> = Mongoose.model<
  IBuyerUserDoc
>("buyer", mongooseBuyerUserSchema)
