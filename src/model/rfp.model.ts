import * as Mongoose from "mongoose"
import { ProposalStatus } from "../utilities/config"
export interface IRfpDoc extends Mongoose.Document {
  // category_id: Mongoose.Types.ObjectId
  // sub_category_id: Mongoose.Types.ObjectId
  // delivery_address: string
  // quantity?: number
  // message?: string
  // user_id?: Mongoose.Types.ObjectId
  // email?: string
  // mobile_number?: string
  // full_name?: string
  // created_at: Date
  // notified_at: Date
  user_id: Mongoose.Types.ObjectId
  contact_person_id: Mongoose.Types.ObjectId
  category_name: string
  sub_category_name?: string
  quantity: number
  pickup_address_id: Mongoose.Types.ObjectId
  description?: string
  status: string
  requested_at: Date
  requested_by_id: Mongoose.Types.ObjectId
}

const mongooseRfpSchema = new Mongoose.Schema(
  {
    // category_id: {
    //   type: Mongoose.Schema.Types.ObjectId,
    //   ref: "aggregate_sand_category",
    //   index: true,
    //   required: true,
    // },
    // sub_category_id: {
    //   type: Mongoose.Schema.Types.ObjectId,
    //   ref: "product_sub_category",
    //   index: true,
    //   required: true,
    // },
    // delivery_address: String,
    // quantity: Number,
    // message: String,
    // user_id: Mongoose.Schema.Types.ObjectId,
    // email: String,
    // mobile_number: String,
    // full_name: String,
    // created_at: { type: Date, default: Date.now },
    // notified_at: Date,

    user_id: Mongoose.Schema.Types.ObjectId,
    contact_person_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "contact",
    },
    category_name: String,
    sub_category_name: String,
    quantity: Number, // available quantity
    pickup_address_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "address",
      index: true,
    },
    description: String,
    status: { type: String, default: ProposalStatus.Unverified },
    requested_by_id: Mongoose.Schema.Types.ObjectId,
    requested_at: { type: Date, default: Date.now },
  },
  { versionKey: false, collection: "rfp" }
).index({
  description: "text",
})

export const RfpModel: Mongoose.Model<IRfpDoc> = Mongoose.model<IRfpDoc>(
  "rfp",
  mongooseRfpSchema
)
