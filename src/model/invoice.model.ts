import * as Mongoose from "mongoose"
import { IAggregateSandSubCategoryDoc } from "./aggregate_sand_category.model"
import { IStateDoc, ICityDoc, ISubCityDoc } from "./region.model"
import { PaymentStatus, GstTypes } from "../utilities/config"

export interface IInvoiceItems {
  product_id: Mongoose.Types.ObjectId
  supplier_user_id: Mongoose.Types.ObjectId
  category_name: string
  sub_category_id: string
  productSubCategory: IAggregateSandSubCategoryDoc
  quantity: number
  quantity_unit: string
  unit_price: number
  gst_type: string // IGST/ SGST,CGST
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  igst_amount?: number
  sgst_amount?: number
  cgst_amount?: number
  logistics_price: number
  total_price: number
}

export interface IInvoiceDoc extends Mongoose.Document {
  _id: string
  order_id: string
  user_id: Mongoose.Types.ObjectId
  items: [IInvoiceItems]
  total_price: number
  payment_mode: string
  payment_status: PaymentStatus
  site_id?: string
  site_name?: string
  address_line1: string
  address_line2?: string
  state_id: Mongoose.Types.ObjectId
  stateDetails: IStateDoc
  city_id: Mongoose.Types.ObjectId
  cityDetails: ICityDoc
  sub_city_id?: Mongoose.Types.ObjectId
  subCityDetails: ISubCityDoc
  pincode?: string
  person_name?: string
  title?: string
  email?: string
  mobile_number?: string
  alt_mobile_number?: string
  whatsapp_number?: string
  landline_number?: string
  created_at: Date
  paid_at?: Date
}

const mongooseInvoiceItemsSchema = new Mongoose.Schema(
  {
    category_name: String,
    product_id: Mongoose.Schema.Types.ObjectId,
    supplier_user_id: Mongoose.Schema.Types.ObjectId,
    sub_category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "product_sub_category",
      index: true,
    },
    quantity: Number,
    quantity_unit: String,
    gst_type: { type: String, enum: Object.values(GstTypes) },
    unit_price: Number,
    igst_rate: Number,
    cgst_rate: Number,
    sgst_rate: Number,
    igst_amount: Number,
    sgst_amount: Number,
    cgst_amount: Number,
    logistics_price: Number,
    total_price: Number,
  },
  { versionKey: false }
)

const mongooseInvoiceSchema = new Mongoose.Schema(
  {
    _id: String,
    order_id: String,
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      index: true,
    },
    items: [mongooseInvoiceItemsSchema],
    total_price: Number,
    payment_mode: String,
    payment_status: { type: String, enum: Object.values(PaymentStatus) },
    site_id: { type: Mongoose.Schema.Types.ObjectId, ref: "site", index: true },
    address_line1: String,
    address_line2: String,
    state_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "state",
      index: true,
    },
    city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "city",
    },
    sub_city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "sub_city",
      index: true,
    },
    pincode: Number,
    person_name: String,
    title: String,
    email: String,
    mobile_number: String,
    alt_mobile_number: String,
    whatsapp_number: String,
    landline_number: String,
    created_at: { type: Date, default: Date.now, index: true },
    paid_at: Date,
  },
  { collection: "invoice", versionKey: false }
)

export const InvoiceModel: Mongoose.Model<IInvoiceDoc> = Mongoose.model<
  IInvoiceDoc
>("invoice", mongooseInvoiceSchema)
