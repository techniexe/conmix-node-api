import * as Mongoose from "mongoose"

export interface ISandSourceDoc extends Mongoose.Document {
  region_id: Mongoose.Types.ObjectId
  sand_source_name: string
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id?: Mongoose.Types.ObjectId
  updated_at?: Date
  sand_source_date: Date
}
const mongooseSandSourceSchema = new Mongoose.Schema(
  {
    region_id: Mongoose.Schema.Types.ObjectId,
    sand_source_name: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    sand_source_date: { type: Date, default: Date.now },
  },
  { collection: "sand_source", versionKey: false }
).index(
  {
    region_id: 1,
    sand_source_name: 1,
  },
  { unique: true }
)

export const SandSourceModel: Mongoose.Model<ISandSourceDoc> = Mongoose.model(
  "sand_source",
  mongooseSandSourceSchema
)
