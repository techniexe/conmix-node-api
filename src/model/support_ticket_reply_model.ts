import * as Mongoose from "mongoose"
import { IAttachement, attachments } from "./support_ticket.model"

export interface ISupportTicketReplyDoc extends Mongoose.Document {
  ticket_id: string
  reply_by_id: Mongoose.Types.ObjectId
  reply_by_type: string
  comment: string
  attachments: IAttachement[]
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_by_type: string
  master_vendor_id: Mongoose.Types.ObjectId
}

const mongooseSupportTicketReplySchema = new Mongoose.Schema(
  {
    ticket_id: String,
    reply_by_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    reply_by_type: String,
    comment: String,
    attachments: [attachments],
    created_at: { type: Date, default: Date.now },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_by_type: String,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "support_ticket_reply", versionKey: false }
)

export const SupportTicketReplyModel: Mongoose.Model<
  ISupportTicketReplyDoc
> = Mongoose.model<ISupportTicketReplyDoc>(
  "support_ticket_reply",
  mongooseSupportTicketReplySchema
)
