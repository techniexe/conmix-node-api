import * as Mongoose from "mongoose"

export interface IReviewDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  // sub_category_id: Mongoose.Types.ObjectId
  // product_id: Mongoose.Types.ObjectId
  vendor_id: Mongoose.Types.ObjectId
  review_text: string
  rating: number
  status: string
  created_at: Date
  modirated_at: Date
}

const MongooseReviewSchema = new Mongoose.Schema(
  {
    user_id: Mongoose.Schema.Types.ObjectId,
    // sub_category_id: {
    //   type: Mongoose.Schema.Types.ObjectId,
    //   ref: "product_sub_category",
    //   index: true,
    // },
    // product_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    vendor_id: Mongoose.Schema.Types.ObjectId,
    review_text: { type: String, index: true },
    rating: { type: Number, index: true },
    status: { type: String, default: "unpublished", index: true },
    created_at: { type: Date, default: Date.now, index: true },
    modirated_at: { type: Date, index: true },
  },
  { collection: "vendor_review", versionKey: false }
)

MongooseReviewSchema.index({
  review_text: "text",
})

export const ReviewModel: Mongoose.Model<IReviewDoc> = Mongoose.model<
  IReviewDoc
>("vendor_review", MongooseReviewSchema)
