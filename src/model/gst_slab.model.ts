import * as Mongoose from "mongoose"

export interface IGstSlabDoc extends Mongoose.Document {
  title: string
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  hsn_code: string
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
  [k: string]: any
}

const MongooseGstSlabSchema = new Mongoose.Schema(
  {
    title: String,
    igst_rate: Number,
    cgst_rate: Number,
    sgst_rate: Number,
    hsn_code: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
  },
  { collection: "gst_slab", versionKey: false }
)

export const GstSlabModel: Mongoose.Model<IGstSlabDoc> = Mongoose.model<
  IGstSlabDoc
>("gst_slab", MongooseGstSlabSchema)
