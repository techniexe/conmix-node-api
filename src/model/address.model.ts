import * as Mongoose from "mongoose"
import { CityModel, SubCityModel, ISubCityDoc } from "./region.model"
import { isNullOrUndefined } from "../utilities/type-guards"
import { UnexpectedInput } from "../utilities/customError"
const autopopulate = require("mongoose-autopopulate")
import { mongooseAlias } from "../utilities/mongoose-alias"

export enum AddressType {
  WAREHOUSE = "warehouse",
  FACTORY = "factory",
  QUARRY = "quarry",
  HOME = "home",
  OFFICE = "office",
}
export interface IAddressDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  sub_city_id?: Mongoose.Types.ObjectId
  subCityDetails: ISubCityDoc
  pincode?: number
  location: {
    type: string
    coordinates: [number, number]
  }
  address_type: AddressType //warehouse/factory/quarry/kapchi/home/office/
  business_name: string
  region_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at?: Date
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  billing_address_id: Mongoose.Types.ObjectId
  is_verified: boolean
  is_accepted: boolean
  is_blocked: boolean
  plant_address_date: Date

  [k: string]: any
}

const mongooseAddressSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/vendor/
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    line1: String,
    line2: String,
    state_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "state",
      alias: "stateDetails",
      autopopulate: true,
      required: true,
    },
    city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "city",
      alias: "cityDetails",
      autopopulate: true,
      required: true,
    },
    sub_city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "sub_city",
      alias: "subCityDetails",
      autopopulate: true,
    },
    pincode: { type: Number, index: true },
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    address_type: {
      type: String,
      index: true,
      enum: Object.values(AddressType),
    },
    business_name: String,
    region_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    billing_address_id: Mongoose.Schema.Types.ObjectId,
    is_verified: { type: Boolean, default: false, index: true },
    is_accepted: { type: Boolean, default: true, index: true },
    is_blocked: { type: Boolean, default: false, index: true },
    plant_address_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "address", versionKey: false }
)
  .plugin(autopopulate)
  .plugin(mongooseAlias)
  .index(
    {
      "user.user_id": 1,
      line1: 1,
      line2: 1,
      sub_city_id: 1,
      address_type: 1,
    },
    {
      unique: true,
      partialFilterExpression: { line2: { $exists: true } },
    }
  )
  
  .index({
    "user.user_id": 1,
  })
  .index({
    location: "2dsphere",
  })
  .index({
    business_name: "text",
  })

mongooseAddressSchema.pre<IAddressDoc>("save", async function (
  next: Mongoose.HookErrorCallback
) {
  const cityCheck = await CityModel.findById(this.city_id)
  if (cityCheck === null) {
    const err = new UnexpectedInput(`No such city exists.`)
    err.httpStatusCode = 400
    return next(err)
  }

  if (!isNullOrUndefined(this.sub_city_id)) {
    const subCityCheck = await SubCityModel.findOne({
      _id: this.sub_city_id,
      city_id: this.city_id,
    })
    if (subCityCheck === null) {
      const err = new UnexpectedInput(
        `no Sub city in city "${cityCheck.city_name}" exists.`
      )
      err.httpStatusCode = 400
      return next(err)
    }
  }
  return next()
})

mongooseAddressSchema.pre<any>("updateOne", async function (
  next: Mongoose.HookErrorCallback
) {
  const setItems = this.getUpdate().$set

  if (isNullOrUndefined(setItems)) {
    return next()
  }
  this.update({}, { updated_at: Date.now() })
  // this.updated_at = Date.now()
  if (!isNullOrUndefined(setItems.city_id)) {
    const cityCheck = await CityModel.findById(setItems.city_id)
    if (cityCheck === null) {
      const err = new UnexpectedInput(`No such city exists.`)
      err.httpStatusCode = 400
      return next(err)
    }
  }
  if (!isNullOrUndefined(setItems.sub_city_id)) {
    if (!isNullOrUndefined(setItems.sub_city_id)) {
      const subCityCheck = await SubCityModel.findOne({
        _id: setItems.sub_city_id,
        city_id: setItems.city_id,
      })
      if (subCityCheck === null) {
        const err = new UnexpectedInput(`no Sub city exists.`)
        err.httpStatusCode = 400
        return next(err)
      }
    }
  }

  return next()
})

export const AddressModel: Mongoose.Model<IAddressDoc> = Mongoose.model<
  IAddressDoc
>("address", mongooseAddressSchema)
