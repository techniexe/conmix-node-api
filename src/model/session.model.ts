import * as Mongoose from "mongoose"
import "../utilities/db"

const mongooseSessionSchema = new Mongoose.Schema(
  {
    token: { type: String, index: true, unique: true },
    platform: String,
    created_at: { type: String, default: Date.now },
  },
  { collection: "access_tokens" }
)

export const SessionModel = Mongoose.model(
  "access_tokens",
  mongooseSessionSchema
)
