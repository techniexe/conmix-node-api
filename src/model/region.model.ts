import * as Mongoose from "mongoose"
import { InvalidInput } from "../utilities/customError"

export interface ICountryDoc extends Mongoose.Document {
  country_id: number
  country_name: string
  country_code: string
  created_at: Date
}

export interface IStateDoc extends Mongoose.Document {
  state_name: string
  country_id: number
  country_code: string
  country_name: string
  created_at: Date
}

export interface ICityDoc extends Mongoose.Document {
  state_id: Mongoose.Types.ObjectId
  city_name: string
  created_at: Date
  state_name: string
  country_id: number
  country_code: string
  country_name: string
}

export interface ISubCityDoc extends Mongoose.Document {
  sub_city_name: string
  city_id: Mongoose.Types.ObjectId
  cityDetails: ICityDoc
  location: {
    type: string
    coordinates: [number, number]
  }
  created_at: Date
}
const mongooseCountrySchema = new Mongoose.Schema(
  {
    country_id: Number,
    country_name: String,
    country_code: String,
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "country", versionKey: false }
)
  .index({ country_id: 1 }, { unique: true })
  .index({ country_name: 1 }, { unique: true })
  .index({ country_code: 1 }, { unique: true })

const mongooseStateSchema = new Mongoose.Schema(
  {
    state_name: { type: String, required: true },
    country_id: { type: Number, required: true },
    country_code: String,
    country_name: String,
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "state", versionKey: false }
)
  .index(
    {
      state_name: 1,
      country_id: 1,
    },
    { unique: true }
  )
  .index({
    state_name: "text",
  })

const mongooseCitySchema = new Mongoose.Schema(
  {
    city_name: { type: String, required: true },
    state_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "state",
      required: true,
    },
    created_at: { type: Date, default: Date.now, index: true },
    state_name: String,
    country_id: { type: Number, index: true },
    country_code: String,
    country_name: String,
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
  },
  { collection: "city", versionKey: false }
)
  .index(
    {
      city_name: 1,
      state_id: 1,
    },
    { unique: true }
  )
  .index({
    city_name: "text",
  })

const mongooseSubCitySchema = new Mongoose.Schema(
  {
    sub_city_name: { type: String, required: true },
    city_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "city",
      required: true,
      alias: "cityDetails",
      autopopulate: true,
    },
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "sub_city", versionKey: false }
).index(
  {
    sub_city_name: 1,
    city_id: 1,
  },
  { unique: true }
)

export const CountryModel: Mongoose.Model<ICountryDoc> = Mongoose.model<
  ICountryDoc
>("country", mongooseCountrySchema)

export const StateModel: Mongoose.Model<IStateDoc> = Mongoose.model<IStateDoc>(
  "state",
  mongooseStateSchema
)

export const CityModel: Mongoose.Model<ICityDoc> = Mongoose.model<ICityDoc>(
  "city",
  mongooseCitySchema
)

export const SubCityModel: Mongoose.Model<ISubCityDoc> = Mongoose.model<
  ISubCityDoc
>("sub_city", mongooseSubCitySchema)

mongooseStateSchema.pre<IStateDoc>("save", async function (
  next: Mongoose.HookErrorCallback
) {
  const countryCheck = await CountryModel.findOne({
    country_id: this.country_id,
  })
  if (countryCheck === null) {
    return next(new InvalidInput(`No such country exists.`, 400))
  }
  return next()
})

mongooseCitySchema.pre<ICityDoc>("save", async function (
  next: Mongoose.HookErrorCallback
) {
  const stateCheck = await StateModel.findById(this.state_id)
  if (stateCheck === null) {
    return next(new InvalidInput(`No such state exists.`, 400))
  }
  return next()
})

mongooseSubCitySchema.pre<ISubCityDoc>("save", async function (
  next: Mongoose.HookErrorCallback
) {
  const cityCheck = await CityModel.findById(this.city_id)
  if (cityCheck === null) {
    return next(new InvalidInput(`No such city exists.`, 400))
  }
  return next()
})
