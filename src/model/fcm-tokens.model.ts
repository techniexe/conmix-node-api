import * as Mongoose from "mongoose"

export interface IFcmTokensDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  token: string
  apn_token?: string
  device_id: string
  platform: string
  created_at: Date
  updated_at?: Date
  socket_id: string
  socket_screen_user_id?: Mongoose.Types.ObjectId
  socket_screen_room_id?: Mongoose.Types.ObjectId
  socket_last_ping: Date
}

const fcmTokensSchema = new Mongoose.Schema(
  {
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "users",
      index: true,
    },
    token: String,
    apn_token: String,
    device_id: { type: String, index: true },
    platform: String,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: { type: Date, index: true },
    socket_id: String,
    socket_screen_user_id: Mongoose.Schema.Types.ObjectId,
    socket_screen_room_id: Mongoose.Schema.Types.ObjectId,
    socket_last_ping: Date,
  },
  { collection: "fcm_tokens", versionKey: false }
).index(
  {
    device_id: 1,
    user_id: 1,
  },
  { unique: true }
)

export const FcmTokensModel: Mongoose.Model<IFcmTokensDoc> = Mongoose.model<
  IFcmTokensDoc
>("fcm_tokens", fcmTokensSchema)
