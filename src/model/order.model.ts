import * as Mongoose from "mongoose"
import { PaymentStatus } from "../utilities/config"

export enum OrderStatus {
  // PROCESSING = "PROCESSING",
  // PROCESSED = "PROCESSED",
  // SHIPPED = "SHIPPED",
  // DELIVERED = "DELIVERED",
  // CANCELLED = "CANCELLED",
  // FAILED = "FAILED",
  // REFUNDED = "REFUNDED",

  PLACED = "PLACED",
  PROCESSING = "PROCESSING",
  CONFIRMED = "CONFIRMED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
  LAPSED = "LAPSED"
}

export interface IOrderDoc extends Mongoose.Document {
  display_id: string
  user_id: Mongoose.Types.ObjectId
  order_status: OrderStatus
  payment_status: PaymentStatus
  //order_quote_status: OrderQuoteStatus
  //base_amount: number
  //logistics_amount: number
  payment_mode: string
  selling_price: number
  margin_price: number
  gst_amount: Number
  TM_price: Number
  CP_price: Number
  total_CP_price: Number
  // coupon_code_id?: Mongoose.Types.ObjectId
  coupon_amount?: number
  cgst_price: number
  sgst_price: number
  igst_price: number
  gst_price: number
  total_amount: number
  payment_attempt: number
  delivery_location: {
    type: string
    coordinates: number[]
  }
  created_at: Date
  paid_at?: Date
  delivered_at?: Date
  site_id?: Mongoose.Types.ObjectId
  billing_address_id: Mongoose.Types.ObjectId
  site_name?: string
  address_line1: string
  address_line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  sub_city_id?: Mongoose.Types.ObjectId
  pincode?: string
  contact_person_id?: Mongoose.Types.ObjectId
  contact_person_name?: string
  contact_person_title?: string
  contact_person_email?: string
  contact_person_mobile_number?: string
  contact_person_alt_mobile_number?: string
  contact_person_whatsapp_number?: string
  contact_person_landline_number?: string
  admin_remarks?: string
  client_remarks?: string
  cancelled_at?: Date
  processed_at?: Date
  gateway_transaction_id?: string
  unique_id: string
  fee: number
  tax: number
  selling_price_With_Margin: number
  billing_address_full_name: string
  billing_address_company_name: string
  billing_address_line1: string
  billing_address_line2: string
  billing_address_state_id: Mongoose.Types.ObjectId
  billing_address_city_id: Mongoose.Types.ObjectId
  billing_address_pincode: string
  billing_address_gst_number: string
  coupon_per: number
  amount: number
  one_day_notification_sent: boolean
  seven_day_notification_sent: boolean
  order_date: Date

  [k: string]: any
}

const mongooseOrderSchema = new Mongoose.Schema(
  {
    display_id: { type: String, index: true, unique: true },
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      index: true,
    },
    order_status: {
      type: String,
      enum: Object.values(OrderStatus),
      index: true,
      default: OrderStatus.PROCESSING,
    },
    payment_status: {
      type: String,
      enum: Object.values(PaymentStatus),
      index: true,
      default: PaymentStatus.UNPAID,
    },
    selling_price: Number,
    margin_price: Number,
    base_amount: Number,
    TM_price: Number,
    CP_price: Number,
    total_CP_price: Number,
    //logistics_amount: Number,
    payment_mode: String,
    gst_amount: Number,
    // coupon_code_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    coupon_amount: Number,
    cgst_price: Number,
    sgst_price: Number,
    igst_price: Number,
    gst_price: Number,
    total_amount: Number,
    payment_attempt: { type: Number, default: 0 },
    delivery_location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    created_at: { type: Date, default: Date.now, index: true },
    paid_at: Date,
    delivered_at: Date,
    site_id: Mongoose.Schema.Types.ObjectId,
    billing_address_id: Mongoose.Schema.Types.ObjectId,
    site_name: String,
    address_line1: String,
    address_line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    sub_city_id: Mongoose.Schema.Types.ObjectId,
    pincode: String,
    contact_person_id: Mongoose.Schema.Types.ObjectId,
    contact_person_name: String,
    contact_person_title: String,
    contact_person_email: String,
    contact_person_mobile_number: String,
    contact_person_alt_mobile_number: String,
    contact_person_whatsapp_number: String,
    contact_person_landline_number: String,
    admin_remarks: String,
    client_remarks: String,
    cancelled_at: Date,
    processed_at: Date,
    gateway_transaction_id: String,
    unique_id: String,
    fee: Number,
    tax: Number,
    selling_price_With_Margin: Number,
    billing_address_full_name: String,
    billing_address_company_name: String,
    billing_address_line1: String,
    billing_address_line2: String,
    billing_address_state_id: Mongoose.Schema.Types.ObjectId,
    billing_address_city_id: Mongoose.Schema.Types.ObjectId,
    billing_address_pincode: String,
    billing_address_gst_number: String,
    coupon_per: Number,
    amount: Number,
    one_day_notification_sent: Boolean,
    seven_day_notification_sent: Boolean,
    order_date: { type: Date, default: Date.now, index: true },

  },
  { collection: "order", versionKey: false }
)

export const OrderModel: Mongoose.Model<IOrderDoc> = Mongoose.model<IOrderDoc>(
  "order",
  mongooseOrderSchema
)

OrderModel.init()
