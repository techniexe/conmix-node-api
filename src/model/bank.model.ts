import * as Mongoose from "mongoose"

export interface IBankInfoDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  bank_name: string
  company_id?: Mongoose.Types.ObjectId
  account_holder_name: string
  account_number: string
  ifsc: string
  account_type: string //current/saving
  cancelled_cheque_image_url: string
  is_default: boolean
  created_at: Date
}

const mongooseBankInfoSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/vendor/
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    bank_name: String,
    account_holder_name: String,
    account_number: String,
    ifsc: String,
    account_type: String,
    cancelled_cheque_image_url: String,
    is_default: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "bank_info", versionKey: false }
)
  .index(
    {
      user_id: 1,
      company_id: 1,
      account_number: 1,
    },
    { unique: true }
  )
  .index({
    account_holder_name: "text",
    account_number: "text",
  })

export const BankInfoModel: Mongoose.Model<IBankInfoDoc> = Mongoose.model<
  IBankInfoDoc
>("bank_info", mongooseBankInfoSchema)
