import * as Mongoose from "mongoose"

export interface IPayMethodDoc extends Mongoose.Document {
  name: string
  created_at: Date
  created_by: Mongoose.Types.ObjectId
  is_active: boolean
  is_deleted: boolean
  deleted_at?: Date
  deleted_by: Mongoose.Types.ObjectId
}

const mongoosePayMethodSchema = new Mongoose.Schema(
  {
    name: String,
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    is_active: { type: Boolean, default: true, index: true },
    is_deleted: { type: Boolean, default: false, index: true },
    deleted_at: Date,
    deleted_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "pay_method", versionKey: false }
).index({
  name: "text",
})

export const PayMethodModel: Mongoose.Model<IPayMethodDoc> = Mongoose.model<
  IPayMethodDoc
>("pay_method", mongoosePayMethodSchema)
