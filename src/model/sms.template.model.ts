import * as Mongoose from "mongoose"
import {
  SmsTemplateTypes,
  SmsLanguage,
  SmsEncodings,
} from "../utilities/config"

export interface ISmsTemplateDoc extends Mongoose.Document {
  template_type: string
  text: string
  encoding: string
  sms_language: string
  created_at: Date
  updated_at?: Date
}

const mongooseSmsTemplateSchema = new Mongoose.Schema(
  {
    template_type: {
      type: String,
      enum: SmsTemplateTypes,
      index: true,
    },
    text: String,
    encoding: {
      type: String,
      index: true,
      enum: SmsEncodings,
      default: SmsEncodings[0],
    },
    sms_language: {
      type: String,
      enum: SmsLanguage,
      index: true,
      default: SmsLanguage[0],
    },
    created_at: { type: Date, index: true, default: Date.now },
    updated_at: Date,
  },

  { collection: "sms_template", versionKey: false }
).index(
  {
    template_type: 1,
    sms_language: 1,
  },
  { unique: true }
)

export const SmsTemplateModel: Mongoose.Model<ISmsTemplateDoc> = Mongoose.model<
  ISmsTemplateDoc
>("sms_template", mongooseSmsTemplateSchema)
