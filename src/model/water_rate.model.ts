import * as Mongoose from "mongoose"
export interface IWaterRateDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  per_ltr_rate: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
}

const mongooseWaterRateSchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    per_ltr_rate: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "water_rate", versionKey: false }
)

export const WaterRateModel: Mongoose.Model<IWaterRateDoc> = Mongoose.model<
  IWaterRateDoc
>("water_rate", mongooseWaterRateSchema)
