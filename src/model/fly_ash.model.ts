import * as Mongoose from "mongoose"

export interface IFlyAshSourceDoc extends Mongoose.Document {
  region_id: Mongoose.Types.ObjectId
  fly_ash_source_name: string
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id?: Mongoose.Types.ObjectId
  updated_at?: Date
  fly_Ash_date: Date
}
const mongooseFlyAshSourceSchema = new Mongoose.Schema(
  {
    region_id: Mongoose.Schema.Types.ObjectId,
    fly_ash_source_name: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    fly_Ash_date:  { type: Date, default: Date.now },
  },
  { collection: "fly_ash_source", versionKey: false }
).index(
  {
    region_id: 1,
    fly_ash_source_name: 1,
  },
  { unique: true }
)

export const FlyAshSourceModel: Mongoose.Model<IFlyAshSourceDoc> = Mongoose.model(
  "fly_ash_source",
  mongooseFlyAshSourceSchema
)
