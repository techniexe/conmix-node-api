import * as Mongoose from "mongoose"

export interface IAggregateSourceQuantityDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  sub_cat_id: Mongoose.Types.ObjectId
  quantity: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  agg_date: Date
}

const MongooseAggregateSourceQuantitySchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    sub_cat_id: Mongoose.Schema.Types.ObjectId,
    quantity: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    agg_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "aggregate_source_quantity", versionKey: false }
)

export const AggregateSourceQuantityModel: Mongoose.Model<IAggregateSourceQuantityDoc> = Mongoose.model<
  IAggregateSourceQuantityDoc
>("aggregate_source_quantity", MongooseAggregateSourceQuantitySchema)
