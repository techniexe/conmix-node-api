import * as Mongoose from "mongoose"

export interface ISandSourceQuantityDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  quantity: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  sand_date: Date

}

const MongooseSandSourceQuantitySchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    quantity: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    sand_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "sand_source_quantity", versionKey: false }
)

export const SandSourceQuantityModel: Mongoose.Model<ISandSourceQuantityDoc> = Mongoose.model<
  ISandSourceQuantityDoc
>("sand_source_quantity", MongooseSandSourceQuantitySchema)
