import * as Mongoose from "mongoose"

export interface IContactDoc extends Mongoose.Document {
  user: {
    user_type: string //collection name: buyer/supplier/
    user_id: Mongoose.Types.ObjectId
  }
  person_name: string
  title: string
  mobile_number_verified?: boolean
  email_verified?: boolean
  email: string
  mobile_number: string
  alt_mobile_number: string
  alt_mobile_number_verified?: boolean
  whatsapp_number: string
  whatsapp_number_verified?: boolean
  landline_number: string
  //profile_pic: string
  created_at: Date
}

const mongooseContactSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/supplier/
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    person_name: String,
    title: String,
    email: String,
    email_verified: { type: Boolean, default: false },
    mobile_number_verified: { type: Boolean, default: false },
    mobile_number: String,
    alt_mobile_number: String,
    alt_mobile_number_verified: { type: Boolean, default: false },
    whatsapp_number: String,
    whatsapp_number_verified: { type: Boolean, default: false },
    landline_number: String,
    //profile_pic: String,
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "contact", versionKey: false }
)
  .index({
    "user.user_id": 1,
  })
  .index({
    person_name: "text",
    title: "text",
    email: "text",
  })

export const ContactModel: Mongoose.Model<IContactDoc> = Mongoose.model<
  IContactDoc
>("contact", mongooseContactSchema)
