import * as Mongoose from "mongoose"
import { SupportTicketStatus } from "../utilities/config"

export interface IAttachement {
  type: string
  url: string
}

export const attachments = new Mongoose.Schema({
  type: { type: String },
  url: { type: String },
})

export interface ISupportTicketDoc extends Mongoose.Document {
  ticket_id: string
  question_type: string
  severity: string
  subject: string
  description: string
  attachments: IAttachement[]
  support_ticket_status: SupportTicketStatus
  order_id?: string
  client_id: Mongoose.Types.ObjectId
  client_type: string
  created_at: Date
  created_by_id: Mongoose.Types.ObjectId
  created_by_type: string
  updated_by_id: Mongoose.Types.ObjectId
  updated_by_type: string
  master_vendor_id: Mongoose.Types.ObjectId
  ticket_date: Date
}

const mongooseSupportTicketSchema = new Mongoose.Schema(
  {
    ticket_id: { type: String, index: true, unique: true },
    question_type: String,
    severity: String,
    subject: String,
    description: String,
    attachments: [attachments],
    support_ticket_status: {
      type: String,
      enum: Object.keys(SupportTicketStatus),
      default: SupportTicketStatus.OPEN,
    },
    order_id: String,
    client_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    client_type: String,
    created_at: { type: Date, default: Date.now },
    created_by_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    created_by_type: String,
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_by_type: String,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    ticket_date : { type: Date, default: Date.now },
  },
  { collection: "support_ticket", versionKey: false }
)

export const SupportTicketModel: Mongoose.Model<ISupportTicketDoc> = Mongoose.model<
  ISupportTicketDoc
>("support_ticket", mongooseSupportTicketSchema)
