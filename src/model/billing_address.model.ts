import * as Mongoose from "mongoose"

export interface IBillingAddressDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  company_name: string
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode?: number
  gst_number?: string
  gst_image_url: string
  created_at: Date
  updated_at?: Date
  is_deleted: boolean
  deleted_at?: Date 
  billing_address_date?: Date
  [k: string]: any
}

const mongooseBillingAddressSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/vendor/
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    company_name: String,
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
    gst_number: String,
    gst_image_url: String,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    is_deleted: { type: Boolean, default: false, index: true },
    deleted_at: Date,
    billing_address_date: Date
  },
  { collection: "billing_address", versionKey: false }
)
export const BillingAddressModel: Mongoose.Model<IBillingAddressDoc> = Mongoose.model<
IBillingAddressDoc
>("billing_address", mongooseBillingAddressSchema)
