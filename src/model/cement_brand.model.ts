import * as Mongoose from "mongoose"

export interface ICementBrandDoc extends Mongoose.Document {
  name: string
  image_url?: string
  main_image_url?: string
  thumbnail_url?: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_deleted: boolean
  deleted_by: Mongoose.Types.ObjectId
  deleted_at: Date
  cement_brand_date: Date
}

const mongooseCementBrandSchema = new Mongoose.Schema(
  {
    name: String,
    image_url: String,
    main_image_url: String,
    thumbnail_url: String,
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    is_deleted: Boolean,
    deleted_by: Mongoose.Schema.Types.ObjectId,
    deleted_at: Date,
    cement_brand_date: { type: Date, default: Date.now },
  },
  { collection: "cement_brand", versionKey: false }
).index({
  name: "text",
})

export const CementBrandModel: Mongoose.Model<ICementBrandDoc> = Mongoose.model<
  ICementBrandDoc
>("cement_brand", mongooseCementBrandSchema)
