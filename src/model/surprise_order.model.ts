// import { TrackStatus } from "./order_track.model"
import * as Mongoose from "mongoose"
import { BillStatus } from "../utilities/config"
import { AddressType } from "./address.model"

export enum SupplierOrderStatus {
  REJECTED = "REJECTED",
  RECEIVED = "RECEIVED",
  ACCEPTED = "ACCEPTED",
  TM_ASSIGNED = "TM_ASSIGNED",
  PICKUP = "PICKUP",
  DELAYED = "DELAYED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
}

export interface IBill extends Mongoose.Document {
  url?: string
  uploaded_at?: Date
  status?: BillStatus
  verified_at?: Date
  reject_reason?: string
}

export interface ISurpriseOrderDoc extends Mongoose.Document {
  _id: string
  user_id: Mongoose.Types.ObjectId
  design_mix_id: Mongoose.Types.ObjectId
  vendor_order_id: string
  buyer_id: Mongoose.Types.ObjectId
  buyer_order_id: Mongoose.Types.ObjectId
  buyer_order_display_id: string
  buyer_order_item_id: Mongoose.Types.ObjectId
  buyer_order_display_item_id: string
  quantity: number
  vehicle_id: Mongoose.Types.ObjectId
  logistics_user_id: Mongoose.Types.ObjectId
  vehicle_category_id: Mongoose.Types.ObjectId
  vehicle_sub_category_id: Mongoose.Types.ObjectId
  concrete_grade_id: Mongoose.Types.ObjectId
  admix_brand_id: Mongoose.Types.ObjectId
  admix_cat_id: Mongoose.Types.ObjectId
  fly_ash_source_id: Mongoose.Types.ObjectId
  aggregate2_sub_cat_id: Mongoose.Types.ObjectId
  agg_source_id: Mongoose.Types.ObjectId
  aggregate1_sub_cat_id: Mongoose.Types.ObjectId
  sand_source_id: Mongoose.Types.ObjectId
  cement_brand_id: Mongoose.Types.ObjectId
  cement_grade_id: Mongoose.Types.ObjectId
  cement_quantity: number
  sand_quantity: number
  aggregate1_quantity: number
  aggregate2_quantity: number
  fly_ash_quantity: number
  admix_quantity: number
  water_quantity: number
  cement_per_kg_rate: number
  sand_per_kg_rate: number
  aggregate1_per_kg_rate: number
  aggregate2_per_kg_rate: number
  fly_ash_per_kg_rate: number
  admix_per_kg_rate: number
  water_per_ltr_rate: number
  cement_price: number
  sand_price: number
  aggreagte1_price: number
  aggreagte2_price: number
  fly_ash_price: number
  admix_price: number
  water_price: number
  with_TM: boolean
  with_CP: boolean
  delivery_date: Date
  end_date: Date
  distance: number
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  gst_type: string // IGST/SGST,CGST
  cgst_price: number
  sgst_price: number
  gst_price: number
  item_status: string
  selling_price: number
  margin_price: number
  TM_price: number
  CP_price: number
  total_CP_price: number
  order_status: SupplierOrderStatus
  payment_status: string
  address_id: Mongoose.Types.ObjectId
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode?: number
  location: {
    type: string
    coordinates: [number, number]
  }
  address_type: AddressType //warehouse/factory/quarry/kapchi/home/office/
  business_name: string
  city_name: string
  state_name: string
  unit_price: number
  base_amount: number
  margin_rate: number
  margin_amount: number
  total_amount: number
  total_amount_without_margin: number
  created_at: Date
  bills: IBill[]
  creditNotes: IBill[]
  debitNotes: IBill[]
  cancelled_at?: Date
  dispatched_at?: Date
  pickedup_at?: Date
  delivered_at?: Date
  // delivery_status: TrackStatus
  reasonForDelay: string
  delayTime: string
  delayed_at?: Date
  max_accepted_at?: Date
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  [k: string]: any
}

const mongooseBillsSchema = new Mongoose.Schema({
  url: String,
  uploaded_at: Date,
  status: {
    type: String,
    enum: Object.values(BillStatus),
    default: BillStatus.PENDING,
  },
  verified_at: Date,
  reject_reason: String,
})

const surpriseOrderSchema = new Mongoose.Schema(
  {
    _id: String,
    user_id: Mongoose.Schema.Types.ObjectId,
    design_mix_id: Mongoose.Schema.Types.ObjectId,
    vendor_order_id: String,
    buyer_id: Mongoose.Schema.Types.ObjectId,
    buyer_order_id: Mongoose.Schema.Types.ObjectId,
    buyer_order_display_id: String,
    buyer_order_item_id: Mongoose.Schema.Types.ObjectId,
    buyer_order_display_item_id: String,
    quantity: Number,
    concrete_grade_id: Mongoose.Schema.Types.ObjectId,
    admix_brand_id: Mongoose.Schema.Types.ObjectId,
    admix_cat_id: Mongoose.Schema.Types.ObjectId,
    fly_ash_source_id: Mongoose.Schema.Types.ObjectId,
    aggregate2_sub_cat_id: Mongoose.Schema.Types.ObjectId,
    agg_source_id: Mongoose.Schema.Types.ObjectId,
    aggregate1_sub_cat_id: Mongoose.Schema.Types.ObjectId,
    sand_source_id: Mongoose.Schema.Types.ObjectId,
    cement_brand_id: Mongoose.Schema.Types.ObjectId,
    cement_grade_id: Mongoose.Schema.Types.ObjectId,
    cement_quantity: Number,
    sand_quantity: Number,
    aggregate1_quantity: Number,
    aggregate2_quantity: Number,
    fly_ash_quantity: Number,
    admix_quantity: Number,
    water_quantity: Number,
    cement_per_kg_rate: Number,
    sand_per_kg_rate: Number,
    aggregate1_per_kg_rate: Number,
    aggregate2_per_kg_rate: Number,
    fly_ash_per_kg_rate: Number,
    admix_per_kg_rate: Number,
    water_per_ltr_rate: Number,
    cement_price: Number,
    sand_price: Number,
    aggreagte1_price: Number,
    aggreagte2_price: Number,
    fly_ash_price: Number,
    admix_price: Number,
    water_price: Number,
    with_TM: Boolean,
    with_CP: Boolean,
    delivery_date: Date,
    end_date: Date,
    distance: Number,
    igst_rate: Number,
    cgst_rate: Number,
    sgst_rate: Number,
    gst_type: String, // IGST/SGST,CGST
    cgst_price: Number,
    sgst_price: Number,
    gst_price: Number,
    item_status: String,
    selling_price: Number,
    margin_price: Number,
    TM_price: Number,
    CP_price: Number,
    total_CP_price: Number,
    order_status: {
      type: String,
      enum: Object.values(SupplierOrderStatus),
      index: true,
      default: SupplierOrderStatus.RECEIVED,
    },
    payment_status: String,
    address_id: Mongoose.Schema.Types.ObjectId,
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: Number,
    location: {
      type: { type: String, enum: ["Point"], default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    address_type: {
      type: String,
      index: true,
      enum: Object.values(AddressType),
    },
    business_name: String,
    city_name: String,
    state_name: String,
    unit_price: Number,
    base_amount: Number,
    margin_rate: Number,
    margin_amount: Number,
    total_amount: Number,
    total_amount_without_margin: Number,
    created_at: { type: Date, default: Date.now, index: true },
    bills: [mongooseBillsSchema],
    creditNotes: [mongooseBillsSchema],
    debitNotes: [mongooseBillsSchema],
    cancelled_at: Date,
    dispatched_at: Date,
    pickedup_at: Date,
    delivered_at: Date,
    // TRACKING
    // delivery_status: { type: String, index: true },
    reasonForDelay: String,
    delayTime: String,
    delayed_at: Date,
    max_accepted_at: Date,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "surprise_order", versionKey: false }
)
export const SurpriseOrderModel: Mongoose.Model<ISurpriseOrderDoc> = Mongoose.model<
  ISurpriseOrderDoc
>("surprise_order", surpriseOrderSchema)
