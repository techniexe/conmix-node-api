import * as Mongoose from "mongoose"

export enum TrackStatus {
  RECEIVED = "RECEIVED",
  TM_ASSIGNED = "TM_ASSIGNED",
  CP_ASSIGNED = "CP_ASSIGNED",
  PICKUP = "PICKUP",
  DELAYED = "DELAYED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
  REJECTED = "REJECTED",
}

export interface IOrderTrackDoc extends Mongoose.Document {
  buyer_order_id: Mongoose.Types.ObjectId
  order_item_id: Mongoose.Types.ObjectId
  TM_id: Mongoose.Types.ObjectId
  vendor_order_id: string
  vendor_user_id: Mongoose.Types.ObjectId
  remaining_quantity: number
  pickup_quantity: number
  royality_quantity: number
  reasonForDelay: string
  delayTime: string
  created_at: Date
  assigned_at: Date
  pickedup_at: Date
  cancelled_at: Date
  delivered_at: Date
  delayed_at: Date
  royalty_pass_image_url: string
  way_slip_image_url: string
  challan_image_url: string
  invoice_image_url: string
  supplier_pickup_signature?: string
  driver_pickup_signature?: string
  buyer_drop_signature?: string
  driver_drop_signature?: string
  qube_test_report_7days?: string
  qube_test_report_28days?: string
  event_status: string
  category_name?: string
  driver_name?: string
  driver_mobile_number?: string
  start_time: Date
  end_time: Date
  order_item_part_id: string
  invoice_url: string
  vendor_invoice_url: string
  TM_rc_number?: string
  TM_category_name: string
  TM_sub_category_name: string
  TM_driver1_name: string
  TM_driver2_name: string
  TM_driver1_mobile_number: string
  TM_driver2_mobile_number: string 
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId

  [k: string]: any
}

const mongooseOrderTrackSchema = new Mongoose.Schema(
  {
    buyer_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    order_item_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    TM_id: Mongoose.Schema.Types.ObjectId,
    vendor_order_id: String,
    vendor_user_id: Mongoose.Schema.Types.ObjectId,
    remaining_quantity: Number,
    pickup_quantity: Number,
    royality_quantity: Number,
    reasonForDelay: String,
    delayTime: String,
    created_at: { type: Date, default: Date.now },
    assigned_at: Date,
    pickedup_at: Date,
    cancelled_at: Date,
    delivered_at: Date,
    delayed_at: Date,
    royalty_pass_image_url: String,
    way_slip_image_url: String,
    challan_image_url: String,
    invoice_image_url: String,
    supplier_pickup_signature: String,
    driver_pickup_signature: String,
    buyer_drop_signature: String,
    driver_drop_signature: String,
    qube_test_report_7days: String,
    qube_test_report_28days: String,
    event_status: String,
    category_name: String,
    driver_name: String,
    driver_mobile_number: String,
    start_time: Date,
    end_time: Date,
    order_item_part_id: Mongoose.Schema.Types.ObjectId,
    invoice_url: String,
    vendor_invoice_url: String,
    TM_rc_number: String,
    TM_category_name: String,
    TM_sub_category_name: String,
    TM_driver1_name: String,
    TM_driver2_name: String,
    TM_driver1_mobile_number: String,
    TM_driver2_mobile_number: String,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "order_track", versionKey: false }
)

export const OrderTrackModel: Mongoose.Model<IOrderTrackDoc> = Mongoose.model<
  IOrderTrackDoc
>("order_track", mongooseOrderTrackSchema)
