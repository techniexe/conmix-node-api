import * as Mongoose from "mongoose"

export interface IAdmixtureCategoryDoc extends Mongoose.Document {
  category_name: string
  brand_id: Mongoose.Types.ObjectId
  created_by: Mongoose.Types.ObjectId
  admixture_type: string
  created_at: Date
  admix_cat_date: Date
}

const mongooseAdmixtureCategorySchema = new Mongoose.Schema(
  {
    category_name: String,
    brand_id: Mongoose.Schema.Types.ObjectId,
    created_by: Mongoose.Schema.Types.ObjectId,
    admixture_type: String,
    created_at: { type: Date, default: Date.now },
    admix_cat_date: { type: Date, default: Date.now },
  },
  { collection: "admixture_category", versionKey: false }
).index({
  category_name: "text",
})

export const AdmixtureCategoryModel: Mongoose.Model<IAdmixtureCategoryDoc> = Mongoose.model<
  IAdmixtureCategoryDoc
>("admixture_category", mongooseAdmixtureCategorySchema)
