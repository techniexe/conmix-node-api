import * as Mongoose from "mongoose"

export interface IOperatorInfoDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  operator_name: string
  operator_mobile_number: string
  operator_alt_mobile_number: string
  operator_whatsapp_number: string
  operator_pic: string
  created_at: Date
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  operator_date: Date
}

const mongooseOperatorInfoSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: vendor
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    operator_name: String,
    operator_mobile_number: String,
    operator_alt_mobile_number: String,
    operator_whatsapp_number: String,
    operator_pic: String,
    created_at: { type: Date, default: Date.now, index: true },
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    operator_date: { type: Date, default: Date.now, index: true },
  },
  { collection: "operator_info", versionKey: false }
).index(
  {
    "user.user_id": 1,
    operator_name: 1,
  },
  { unique: true }
)

export const OperatorInfoModel: Mongoose.Model<IOperatorInfoDoc> = Mongoose.model<
  IOperatorInfoDoc
>("operator_info", mongooseOperatorInfoSchema)
