import * as Mongoose from "mongoose"

export interface IFlyAshSourceRateDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  source_id: Mongoose.Types.ObjectId
  per_kg_rate: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
}

const MongooseFlyAshSourceRateSchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    source_id: Mongoose.Schema.Types.ObjectId,
    per_kg_rate: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "fly_ash_source_rate", versionKey: false }
)

export const FlyAshSourceRateModel: Mongoose.Model<IFlyAshSourceRateDoc> = Mongoose.model<
  IFlyAshSourceRateDoc
>("fly_ash_source_rate", MongooseFlyAshSourceRateSchema)
