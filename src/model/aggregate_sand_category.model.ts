import * as Mongoose from "mongoose"
import { InvalidInput } from "../utilities/customError"

export interface IAggregateSandCategoryDoc extends Mongoose.Document {
  category_name: string
  //sequence: number
  image_url?: string
  main_image_url?: string
  thumbnail_url?: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  agg_sand_cat_date: Date
  [k: string]: any
}

export interface IAggregateSandSubCategoryDoc extends Mongoose.Document {
  sub_category_name: any
  category_id: Mongoose.Types.ObjectId
  //sequence: number
  image_url?: string
  thumbnail_url?: string
  // quantity_unit_code: string
  // quantity_units: string
  // igst_rate: number
  // sgst_rate: number
  // cgst_rate: number
  selling_unit: [string]
  margin_rate_id: Mongoose.Types.ObjectId
  min_quantity: {
    pcs: number
    nos: number
    kg: number
    MT: number
    sqmeter: number
    qtl: number
  }
  gst_slab_id: Mongoose.Types.ObjectId
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  agg_sand_sub_cat_date: Date
}

const mongooseAggregateSandCategorySchema = new Mongoose.Schema(
  {
    category_name: String,
    sequence: { type: Number, default: 0, index: true },
    image_url: String,
    main_image_url: String,
    thumbnail_url: String,
    created_by: { type: Mongoose.Schema.Types.ObjectId, ref: "category" },
    created_at: { type: Date, default: Date.now },
    agg_sand_cat_date: { type: Date, default: Date.now },
  },
  { collection: "aggregate_sand_category", versionKey: false }
).index({
  category_name: "text",
})

const mongooseAggregateSubCategorySchema = new Mongoose.Schema(
  {
    sub_category_name: String,
    category_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    sequence: { type: Number, default: 0, index: true },
    image_url: String,
    thumbnail_url: String,
    // quantity_unit_code: { type: String, default: "MT" },
    // quantity_units: { type: String, default: "Metric Tonne" },
    // igst_rate: Number,
    // sgst_rate: Number,
    // cgst_rate: Number,
    margin_rate_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "margin_rate",
      index: true,
    },
    min_quantity: {
      pcs: Number,
      nos: Number,
      kg: Number,
      MT: Number,
      sqmeter: Number,
      qtl: Number,
    },
    selling_unit: [String],
    gst_slab_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "gst_slab",
      index: true,
    },
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    agg_sand_sub_cat_date: { type: Date, default: Date.now },
  },
  { collection: "aggregate_sand_sub_category", versionKey: false }
).index({
  sub_category_name: "text",
})

export const AggregateSandCategoryModel: Mongoose.Model<IAggregateSandCategoryDoc> = Mongoose.model<
  IAggregateSandCategoryDoc
>("aggregate_sand_category", mongooseAggregateSandCategorySchema)

export const AggregateSandSubCategoryModel: Mongoose.Model<IAggregateSandSubCategoryDoc> = Mongoose.model<
  IAggregateSandSubCategoryDoc
>("aggregate_sand_sub_category", mongooseAggregateSubCategorySchema)

mongooseAggregateSubCategorySchema.pre<IAggregateSandSubCategoryDoc>(
  "save",
  async function (next: Mongoose.HookErrorCallback) {
    const catCheck = await AggregateSandCategoryModel.findById(this.category_id)
    if (catCheck === null) {
      const err1 = new InvalidInput(`No such category exists.`)
      err1.httpStatusCode = 400
      return next(err1)
    }
    const subCatCheck = await AggregateSandSubCategoryModel.findOne({
      sub_category_name: this.sub_category_name,
      category_id: this.category_id,
    })
    if (subCatCheck !== null) {
      const err1 = new InvalidInput(
        `Sub Category "${this.sub_category_name}" on Category "${catCheck.category_name}" already exists.`
      )
      err1.httpStatusCode = 400
      return next(err1)
    }
    return next()
  }
)
