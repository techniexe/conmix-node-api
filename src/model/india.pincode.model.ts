import * as Mongoose from "mongoose"

export interface IIndiaPinCodeDoc extends Mongoose.Document {
  pincode: number
  office_name: string
  office_type: string
  region_name: string
  circle_name: string
  taluk: string
  district_name: string
  state_name: string
  created_at: Date
}

const mongoosePinCodeSchema = new Mongoose.Schema(
  {
    pincode: Number,
    office_name: String,
    office_type: String,
    region_name: String,
    circle_name: String,
    taluk: String,
    district_name: String,
    state_name: String,
    created_at: { type: Date, default: Date.now, index: true },
  },
  { collection: "india_pincode", versionKey: false }
)

export const IndiaPostModel: Mongoose.Model<IIndiaPinCodeDoc> = Mongoose.model(
  "india_pincode",
  mongoosePinCodeSchema
)
