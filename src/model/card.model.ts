import * as Mongoose from "mongoose"

export interface ICardDoc extends Mongoose.Document {
  user_id: string
  card_number: string
  card_name: string
  expiry_month: string
  expiry_year: number
}

const mongooseCardSchema = new Mongoose.Schema(
  {
    user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "buyer",
      alias: "userDetails",
      autopopulate: { select: "full_name , _id" },
    },
    card_number: String,
    card_name: String,
    expiry_month: String,
    expiry_year: Number,
  },
  { collection: "card", versionKey: false }
).index(
  {
    user_id: 1,
    card_number: 1,
  },
  { unique: true }
)

export const CardModel: Mongoose.Model<ICardDoc> = Mongoose.model<ICardDoc>(
  "card",
  mongooseCardSchema
)
