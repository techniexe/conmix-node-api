import * as Mongoose from "mongoose"

export interface IAdmixtureOSDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  brand_id: Mongoose.Types.ObjectId
  category_id: Mongoose.Types.ObjectId
  quantity: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
}

const MongooseAdmixtureOSSchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    brand_id: Mongoose.Schema.Types.ObjectId,
    category_id: Mongoose.Schema.Types.ObjectId,
    quantity: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "admix_os", versionKey: false }
)

export const AdmixtureOSModel: Mongoose.Model<IAdmixtureOSDoc> = Mongoose.model<
IAdmixtureOSDoc
>("admix_os", MongooseAdmixtureOSSchema)
