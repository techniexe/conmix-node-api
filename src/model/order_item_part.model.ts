import * as Mongoose from "mongoose"

export interface IOrderItemPartDoc extends Mongoose.Document {
  buyer_id: Mongoose.Types.ObjectId
  buyer_order_id: Mongoose.Types.ObjectId
  order_item_id: Mongoose.Types.ObjectId
  assigned_date: Date
  assigned_quantity: number
  temp_quantity: number
  start_time: Date
  end_time: Date
  created_at: Date
  vendor_id: Mongoose.Types.ObjectId
  [k: string]: any

  //   TM_id: Mongoose.Types.ObjectId
  //   vendor_order_id: string
  //   vendor_user_id: Mongoose.Types.ObjectId
  //   remaining_quantity: number
  //   pickup_quantity: number
  //   royality_quantity: number
  //   reasonForDelay: string
  //   delayTime: string
  //   created_at: Date
  //   assigned_at: Date
  //   pickedup_at: Date
  //   cancelled_at: Date
  //   delivered_at: Date
  //   delayed_at: Date
  //   royalty_pass_image_url: string
  //   way_slip_image_url: string
  //   challan_image_url: string
  //   invoice_image_url: string
  //   supplier_pickup_signature?: string
  //   driver_pickup_signature?: string
  //   buyer_drop_signature?: string
  //   driver_drop_signature?: string
  //   event_status: string
  //   TM_rc_number?: string
  //   category_name?: string
  //   driver_name?: string
  //   driver_mobile_number?: string
}

const mongooseOrderItemPartSchema = new Mongoose.Schema(
  {
    buyer_id: Mongoose.Schema.Types.ObjectId,
    buyer_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    order_item_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    assigned_date: Date,
    assigned_quantity: Number,
    temp_quantity: Number,
    start_time: Date,
    end_time: Date,
    created_at: { type: Date, index: true, default: Date.now },
    vendor_id: Mongoose.Schema.Types.ObjectId,
    // TM_id: Mongoose.Schema.Types.ObjectId,
    // vendor_order_id: String,
    // vendor_user_id: Mongoose.Schema.Types.ObjectId,
    // remaining_quantity: Number,
    // pickup_quantity: Number,
    // royality_quantity: Number,
    // reasonForDelay: String,
    // delayTime: String,
    // created_at: { type: Date, default: Date.now },
    // assigned_at: Date,
    // pickedup_at: Date,
    // cancelled_at: Date,
    // delivered_at: Date,
    // delayed_at: Date,
    // royalty_pass_image_url: String,
    // way_slip_image_url: String,
    // challan_image_url: String,
    // invoice_image_url: String,
    // supplier_pickup_signature: String,
    // driver_pickup_signature: String,
    // buyer_drop_signature: String,
    // driver_drop_signature: String,
    // event_status: String,
    // TM_rc_number: String,
    // category_name: String,
    // driver_name: String,
    // driver_mobile_number: String,
  },
  { collection: "order_item_part", versionKey: false }
)

export const OrderItemPartModel: Mongoose.Model<IOrderItemPartDoc> = Mongoose.model<
  IOrderItemPartDoc
>("order_item_part", mongooseOrderItemPartSchema)
