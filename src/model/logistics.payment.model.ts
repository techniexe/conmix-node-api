import * as Mongoose from "mongoose"

export interface ILogisticsPaymentDoc extends Mongoose.Document {
  logistics_order_id: Mongoose.Types.ObjectId
  logistics_user_id: Mongoose.Types.ObjectId
  total_amount: number
  bank_id: Mongoose.Types.ObjectId
  account_holder_name: string
  account_number: number
  ifsc: string
  account_type: string //current/saving
  created_at: Date
  payment_status: string
}

const mongooseLogisticsPaymentSchema = new Mongoose.Schema(
  {
    logistics_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "logistics_order",
      index: true,
    },
    logistics_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "logistics_user",
      index: true,
    },

    total_amount: Number,
    bank_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "bank_info",
      index: true,
    },
    account_holder_name: String,
    account_number: Number,
    ifsc: String,
    account_type: String, //current/saving
    created_at: Date,
    payment_status: String,
  },
  { collection: "logistics_payment", versionKey: false }
)

export const SupplierPaymentModel: Mongoose.Model<
  ILogisticsPaymentDoc
> = Mongoose.model<ILogisticsPaymentDoc>(
  "logistics_payment",
  mongooseLogisticsPaymentSchema
)
