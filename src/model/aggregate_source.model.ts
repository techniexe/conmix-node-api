import * as Mongoose from "mongoose"

export interface IAggregateSourceDoc extends Mongoose.Document {
  region_id: Mongoose.Types.ObjectId
  aggregate_source_name: string
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id?: Mongoose.Types.ObjectId
  updated_at?: Date
  agg_source_date: Date
}
const mongooseAggregateSourceSchema = new Mongoose.Schema(
  {
    region_id: Mongoose.Schema.Types.ObjectId,
    aggregate_source_name: String,
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    agg_source_date: { type: Date, default: Date.now },
  },
  { collection: "aggregate_source", versionKey: false }
).index(
  {
    region_id: 1,
    aggregate_source_name: 1,
  },
  { unique: true }
)

export const AggregateSourceModel: Mongoose.Model<IAggregateSourceDoc> = Mongoose.model(
  "aggregate_source",
  mongooseAggregateSourceSchema
)
