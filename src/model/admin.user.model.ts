import * as Mongoose from "mongoose"


export enum AdminRoles {
  superAdmin = "superAdmin",
  admin_manager = "admin_manager",
  admin_marketing = "admin_marketing",
  admin_sales = "admin_sales",
  admin_accounts = "admin_accounts",
  admin_customer_care = "admin_customer_care",
  admin_grievances =  "admin_grievances",

  // PLACED = "PLACED",
  // PROCESSING = "PROCESSING",
  // CONFIRMED = "CONFIRMED",
  // DELIVERED = "DELIVERED",
  // CANCELLED = "CANCELLED",
  // LAPSED = "LAPSED"
}

export interface IAdminUserDoc extends Mongoose.Document {
  full_name: string
  mobile_number: string
  mobile_verified?: boolean
  email: string
  email_verified?: boolean
  admin_type: string // superAdmin, Admin,  buyer_admin, supplier_admin, logistics_admin (sales, support, account_manager)
  identity_image_url?: string
  password: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_active: boolean
  is_deleted: boolean
  deleted_at: Date
  deleted_by: Mongoose.Types.ObjectId
  notification_count: number
  notification_opted?: boolean
  unseen_message_count: number
}

const mongooseAdminUserSchema = new Mongoose.Schema(
  {
    full_name: String,
    mobile_number: String,
    mobile_verified: { type: Boolean, default: false },
    email: String,
    email_verified: { type: Boolean, default: false },
    admin_type: String,
    identity_image_url: String,
    password: { type: String, index: true },
    created_by: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    created_at: { type: Date, default: Date.now },
    deleted_at: Date,
    is_active: { type: Boolean, default: true },
    is_deleted: { type: Boolean, default: false },
    deleted_by: Mongoose.Schema.Types.ObjectId,
    notification_count: Number,
    notification_opted: Boolean,
    unseen_message_count: Number,
  },
  { collection: "admin", versionKey: false }
)
mongooseAdminUserSchema.index(
  { mobile_number: 1 },
  {
    unique: true,
    partialFilterExpression: { mobile_number: { $exists: true } },
  }
)

mongooseAdminUserSchema.index(
  { email: 1 },
  {
    unique: true,
    partialFilterExpression: { email: { $exists: true } },
  }
)

export const AdminUserModel: Mongoose.Model<IAdminUserDoc> = Mongoose.model<
  IAdminUserDoc
>("admin", mongooseAdminUserSchema)
