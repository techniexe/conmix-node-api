import * as Mongoose from "mongoose"
export interface IVendorAdMixtureSettingDoc extends Mongoose.Document {
  vendor_id: Mongoose.Types.ObjectId
  settings: [
    {
      min_km: number
      max_km: number
      ad_mixture1_brand_id: Mongoose.Types.ObjectId
      ad_mixture1_category_id: Mongoose.Types.ObjectId
      ad_mixture2_brand_id: Mongoose.Types.ObjectId
      ad_mixture2_category_id: Mongoose.Types.ObjectId
      price: number
      ad_mixture1_brand_name: string
      ad_mixture2_brand_name: string
      ad_mixture1_category_name: string
      ad_mixture2_category_name: string
      [k: string]: any
    }
  ]
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  [k: string]: any
}

const mongooseVendorAdMixtureSettingSchema = new Mongoose.Schema(
  {
    vendor_id: Mongoose.Schema.Types.ObjectId,
    settings: [
      {
        min_km: Number,
        max_km: Number,
        ad_mixture1_brand_id: Mongoose.Schema.Types.ObjectId,
        ad_mixture1_category_id: Mongoose.Schema.Types.ObjectId,
        ad_mixture2_brand_id: Mongoose.Schema.Types.ObjectId,
        ad_mixture2_category_id: Mongoose.Schema.Types.ObjectId,
        price: Number,
        ad_mixture1_brand_name: String,
        ad_mixture2_brand_name: String,
        ad_mixture1_category_name: String,
        ad_mixture2_category_name: String,
      },
    ],
    created_at: { type: Date, default: Date.now },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "vendor_ad_mix_setting", versionKey: false }
)

export const VendorAdMixtureSettingModel: Mongoose.Model<IVendorAdMixtureSettingDoc> = Mongoose.model<
  IVendorAdMixtureSettingDoc
>("vendor_ad_mix_setting", mongooseVendorAdMixtureSettingSchema)
