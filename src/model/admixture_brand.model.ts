import * as Mongoose from "mongoose"

export interface IAdmixBrandDoc extends Mongoose.Document {
  name: string
  image_url?: string
  main_image_url?: string
  thumbnail_url?: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_deleted: boolean
  deleted_by: Mongoose.Types.ObjectId
  deleted_at: Date
  admix_brand_date: Date
}

const mongooseAdmixBrandSchema = new Mongoose.Schema(
  {
    name: String,
    image_url: String,
    main_image_url: String,
    thumbnail_url: String,
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    is_deleted: Boolean,
    deleted_by: Mongoose.Schema.Types.ObjectId,
    deleted_at: Date,
    admix_brand_date: { type: Date, default: Date.now },
  },
  { collection: "admixture_brand", versionKey: false }
).index({
  name: "text",
})

export const AdmixtureBrandModel: Mongoose.Model<IAdmixBrandDoc> = Mongoose.model<
  IAdmixBrandDoc
>("admixture_brand", mongooseAdmixBrandSchema)
