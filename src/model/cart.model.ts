import * as Mongoose from "mongoose"

export interface ICustomMix {
  concrete_grade_id: Mongoose.Types.ObjectId
  final_admix_brand_id: Mongoose.Types.ObjectId
  final_admix_cat_id: Mongoose.Types.ObjectId
  admix_quantity: number
  admix_per_kg_rate: number
  final_fly_ash_source_id: Mongoose.Types.ObjectId
  fly_ash_per_kg_rate: number
  fly_ash_quantity: number
  final_aggregate2_sub_cat_id: Mongoose.Types.ObjectId
  aggregate2_quantity: number
  aggregate2_per_kg_rate: number
  final_agg_source_id: Mongoose.Types.ObjectId
  final_aggregate1_sub_cat_id: Mongoose.Types.ObjectId
  aggregate1_quantity: number
  aggregate1_per_kg_rate: number
  final_sand_source_id: Mongoose.Types.ObjectId
  sand_quantity: number
  sand_per_kg_rate: number
  final_cement_brand_id: Mongoose.Types.ObjectId
  final_cement_grade_id: Mongoose.Types.ObjectId
  cement_per_kg_rate: number
  cement_quantity: number
  water_quantity: number
  water_per_ltr_rate: number
  vendor_id: Mongoose.Types.ObjectId
  address_id: Mongoose.Types.ObjectId
  TM_no?: string
  driver_name?: string
  driver_mobile?: string
  TM_operator_name?: string
  TM_operator_mobile_no?: string
}

export interface ICartItems extends Mongoose.Document {
  design_mix_id: Mongoose.Types.ObjectId
  custom_mix: ICustomMix
  vendor_id: string
  quantity: number
  state_id: Mongoose.Types.ObjectId
  added_at: Date
  with_CP: boolean
  with_TM: boolean
  delivery_date: Date
  end_date: Date
  TM_no: string
  driver_name: string
  driver_mobile: string
  TM_operator_name: string
  TM_operator_mobile_no: string
}
export interface ICartDoc extends Mongoose.Document {
  items: ICartItems[]
  user_id: Mongoose.Types.ObjectId
  site_id?: Mongoose.Types.ObjectId
  delivery_location: {
    type: string
    coordinates: number[]
  }
  billing_address_id: Mongoose.Types.ObjectId
  unique_id: number
  coupon_code_id?: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  coupon_per: number
  amount: number
}
const mongooseCartItemSchema = new Mongoose.Schema({
  design_mix_id: {
    type: Mongoose.Schema.Types.ObjectId,
    index: true,
  },
  custom_mix: Object,
  vendor_id: Mongoose.Schema.Types.ObjectId,
  quantity: Number,
  added_at: { type: Date, default: Date.now, index: true },
  with_CP: Boolean,
  with_TM: Boolean,
  delivery_date: Date,
  end_date: Date,
  state_id: Mongoose.Schema.Types.ObjectId,
  TM_no: String,
  driver_name: String,
  driver_mobile: String,
  TM_operator_name: String,
  TM_operator_mobile_no: String,
})
const mongooseCartSchema = new Mongoose.Schema(
  {
    items: [mongooseCartItemSchema],
    user_id: Mongoose.Schema.Types.ObjectId,
    site_id: Mongoose.Schema.Types.ObjectId,
    delivery_location: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number], default: [0, 0] },
    },
    billing_address_id: Mongoose.Schema.Types.ObjectId,
    unique_id: Number,
    coupon_code_id: { type: Mongoose.Schema.Types.ObjectId },
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: { type: Date, default: Date.now, index: true },
    coupon_per: Number,
    amount: Number
  },
  { collection: "cart", versionKey: false }
).index(
  {
    user_id: 1,
  },
  {
    unique: true,
    partialFilterExpression: { user_id: { $exists: true } },
  }
)
export const CartModel: Mongoose.Model<ICartDoc> = Mongoose.model<ICartDoc>(
  "cart",
  mongooseCartSchema
)
