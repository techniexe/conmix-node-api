import * as Mongoose from "mongoose"

export enum TrackStatus {
  RECEIVED = "RECEIVED",
  TM_ASSIGNED = "TM_ASSIGNED",
  CP_ASSIGNED = "CP_ASSIGNED",
  PICKUP = "PICKUP",
  DELAYED = "DELAYED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
  REJECTED = "REJECTED",
}

export interface ICPOrderTrackDoc extends Mongoose.Document {
  buyer_order_id: Mongoose.Types.ObjectId
  order_item_id: Mongoose.Types.ObjectId
  CP_id: Mongoose.Types.ObjectId
  vendor_order_id: string
  vendor_user_id: Mongoose.Types.ObjectId
  reasonForDelay: string
  delayTime: string
  created_at: Date
  assigned_at: Date
  pickedup_at: Date
  cancelled_at: Date
  delivered_at: Date
  delayed_at: Date
  challan_image_url: string
  supplier_pickup_signature?: string
  driver_pickup_signature?: string
  buyer_drop_signature?: string
  driver_drop_signature?: string
  qube_test_report_7days?: string
  qube_test_report_28days?: string
  event_status: string
  start_time: Date
  end_time: Date
  order_item_part_id: string
  is_already_delivered: boolean
  CP_serial_number: string
  CP_category_name: string
  CP_operator_name: string
  CP_operator_mobile_number: string
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  [k: string]: any
}

const mongooseCPOrderTrackSchema = new Mongoose.Schema(
  {
    buyer_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    order_item_id: { type: Mongoose.Schema.Types.ObjectId, index: true },
    CP_id: Mongoose.Schema.Types.ObjectId,
    vendor_order_id: String,
    vendor_user_id: Mongoose.Schema.Types.ObjectId,
    reasonForDelay: String,
    delayTime: String,
    created_at: { type: Date, default: Date.now },
    assigned_at: Date,
    pickedup_at: Date,
    cancelled_at: Date,
    delivered_at: Date,
    delayed_at: Date,
    challan_image_url: String,
    supplier_pickup_signature: String,
    driver_pickup_signature: String,
    buyer_drop_signature: String,
    driver_drop_signature: String,
    qube_test_report_7days: String,
    qube_test_report_28days: String,
    event_status: String,
    start_time: Date,
    end_time: Date,
    order_item_part_id: Mongoose.Schema.Types.ObjectId,
    is_already_delivered: Boolean,
    CP_serial_number: String,
    CP_category_name: String,
    CP_operator_name: String,
    CP_operator_mobile_number: String,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "CP_order_track", versionKey: false }
)

export const CPOrderTrackModel: Mongoose.Model<ICPOrderTrackDoc> = Mongoose.model<
  ICPOrderTrackDoc
>("CP_order_track", mongooseCPOrderTrackSchema)
