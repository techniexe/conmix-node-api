import * as Mongoose from "mongoose"

export enum AdminNotificationType {
  orderPlaced = 1,
  orderAccept,
  orderRejected,
  orderConfirm,
  TMAssigned,
  orderPickup,
  orderItemReject,
  orderReject,
  orderDelay,
  partiallyProductDelivered,
  fullyProductDelivered,
  orderDelivered,
  partiallyOrderCancelled,
  orderCancelled,
  orderShortClose,
  CPAssigned,
  CPDelay,
  createSupportTicketByBuyer,
  createSupportTicketByVendor,
  replyOfSupportTicketByBuyer,
  replyOfSupportTicketByVendor,
  vendorRegister,
  reminderForVendorVerification
}

export enum ClientNotificationType {
  //Buyer
  orderPlaced = 1,
  orderProcess,
  orderAccept,
  assignQty,
  orderRejected,
  orderConfirm,
  TMAssigned,
  orderPickup,
  orderItemReject,
  orderReject,
  orderDelay,
  partiallyProductDelivered,
  fullyProductDelivered,
  orderDelivered,
  partiallyOrderCancelled,
  orderCancelled,
  orderShortClose,
  CPAssigned,
  reminderBeforSevenDayForClientAssignedQty,
  reminderBeforOneDayForClientAssignedQty,
  CPPickup,
  CPDelivered,
  orderItemLapsed,
  orderLapsed,
  CPDelay,
  ReplyOfSupportTicketByAdmin,
  ResolveSupportTicketByAdmin
}

export enum VendorNotificationType {
  //Supplier
  orderPlaced = 1,
  orderRejected,
  TMAssigned,
  orderPickup,
  orderItemReject,
  orderDelay,
  partiallyProductDelivered,
  orderDelivered,
  orderCancelled,
  orderShortClose,
  CPAssigned,
  CPPickup,
  CPDelivered,
  assignQtyByBuyer,
  CPDelay,
  orderItemLapsed,
  DebitNoteAccept,
  DebitNoteReject,
  ReplyOfSupportTicketByAdmin,
  ResolveSupportTicketByAdmin
}

export interface INotificationDoc extends Mongoose.Document {
  to_user_id: Mongoose.Types.ObjectId
  notification_type: number
  from_user_type?: string
  to_user_type?: string
  order_id?: Mongoose.Types.ObjectId
  item_id: Mongoose.Types.ObjectId
  order_item_id?: string
  vendor_id: Mongoose.Types.ObjectId
  vendor_order_id?: string
  vehicle_id: Mongoose.Types.ObjectId
  site_id: Mongoose.Types.ObjectId
  logistics_order_id: string
  created_at: Date
  seen_on?: Date
  revised_quantity: number
  assigned_quantity: number
  client_id: Mongoose.Types.ObjectId
  ticket_id: Mongoose.Types.ObjectId
}
const mongooseNotificationSchema = new Mongoose.Schema(
  {
    to_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    from_user_id: {
      type: Mongoose.Schema.Types.ObjectId,
    },
    notification_type: Number,
    from_user_type: String,
    to_user_type: String,
    order_id: Mongoose.Schema.Types.ObjectId,
    item_id: Mongoose.Schema.Types.ObjectId,
    order_item_id: String,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    vendor_order_id: String,
    vehicle_id: Mongoose.Schema.Types.ObjectId,
    site_id: Mongoose.Schema.Types.ObjectId,
    logistics_order_id: String,
    created_at: { type: Date, default: Date.now, index: true },
    seen_on: Date,
    revised_quantity: Number,
    assigned_quantity: Number,
    client_id: Mongoose.Schema.Types.ObjectId,
    ticket_id: Mongoose.Schema.Types.ObjectId,
  },
  {
    collection: "notifications",
    versionKey: false,
  }
)

export const NotificationModel: Mongoose.Model<INotificationDoc> = Mongoose.model<
  INotificationDoc
>("notifications", mongooseNotificationSchema)
