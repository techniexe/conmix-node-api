import * as Mongoose from "mongoose"

interface ISlab {
  upto: number
  rate: number
}

export interface IMarginRateDoc extends Mongoose.Document {
  title: string
  slab: ISlab[]
  created_by_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
}

const slabSchema = new Mongoose.Schema({
  upto: { type: Number },
  rate: { type: Number },
})

const MongooseMarginRateSchema = new Mongoose.Schema(
  {
    title: String,
    slab: [slabSchema],
    created_by_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
  },
  { collection: "margin_rate", versionKey: false }
)

export const MarginRateModel: Mongoose.Model<IMarginRateDoc> = Mongoose.model<
  IMarginRateDoc
>("margin_rate", MongooseMarginRateSchema)
