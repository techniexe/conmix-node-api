import * as Mongoose from "mongoose"

export interface IDriverInfoDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  driver_name: string
  driver_mobile_number: string
  driver_alt_mobile_number: string
  driver_whatsapp_number: string
  driver_pic: string
  created_at: Date
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  driver_date?: Date
}

const mongooseDriverInfoSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: logistics
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    driver_name: String,
    driver_mobile_number: String,
    driver_alt_mobile_number: String,
    driver_whatsapp_number: String,
    driver_pic: String,
    created_at: { type: Date, default: Date.now, index: true },
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    driver_date: Date
  },
  { collection: "driver_info", versionKey: false }
).index(
  {
    "user.user_id": 1,
    driver_name: 1,
  },
  { unique: true }
)

export const DriverInfoModel: Mongoose.Model<IDriverInfoDoc> = Mongoose.model<
  IDriverInfoDoc
>("driver_info", mongooseDriverInfoSchema)
