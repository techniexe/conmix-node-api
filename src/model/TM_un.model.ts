import * as Mongoose from "mongoose"

export interface ITM_unDoc extends Mongoose.Document {
  TMId: Mongoose.Types.ObjectId
  unavailable_at: Date
  start_time: Date
  end_time: Date
  vendor_id: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_by_id: Mongoose.Types.ObjectId
  updated_at: Date
}

const MongooseTM_unSchema = new Mongoose.Schema(
  {
    TMId: {
      type: Mongoose.Schema.Types.ObjectId,
      index: true,
    },
    unavailable_at: Date,
    start_time: Date,
    end_time: Date,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_by_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
  },
  { collection: "TM_un", versionKey: false }
)

export const TM_unModel: Mongoose.Model<ITM_unDoc> = Mongoose.model<ITM_unDoc>(
  "TM_un",
  MongooseTM_unSchema
)
