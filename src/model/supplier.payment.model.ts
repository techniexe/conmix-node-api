import * as Mongoose from "mongoose"

export interface ISupplierPaymentDoc extends Mongoose.Document {
  supplier_order_id: Mongoose.Types.ObjectId
  supplier_id: Mongoose.Types.ObjectId
  total_amount: number
  bank_id: Mongoose.Types.ObjectId
  account_holder_name: string
  account_number: number
  ifsc: string
  account_type: string //current/saving
  created_at: Date
  payment_status: string
}

const mongooseSupplierPaymentSchema = new Mongoose.Schema(
  {
    supplier_order_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "supplier_order",
      index: true,
    },
    supplier_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "supplier",
      index: true,
    },

    total_amount: Number,
    bank_id: {
      type: Mongoose.Schema.Types.ObjectId,
      ref: "bank_info",
      index: true,
    },
    account_holder_name: String,
    account_number: Number,
    ifsc: String,
    account_type: String, //current/saving
    created_at: Date,
    payment_status: String,
  },
  { collection: "supplier_payment", versionKey: false }
)

export const SupplierPaymentModel: Mongoose.Model<
  ISupplierPaymentDoc
> = Mongoose.model<ISupplierPaymentDoc>(
  "supplier_payment",
  mongooseSupplierPaymentSchema
)
