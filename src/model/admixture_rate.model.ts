import * as Mongoose from "mongoose"

export interface IAdmixtureRateDoc extends Mongoose.Document {
  address_id: Mongoose.Types.ObjectId
  brand_id: Mongoose.Types.ObjectId
  category_id: Mongoose.Types.ObjectId
  per_kg_rate: number
  vendor_id: Mongoose.Types.ObjectId
  created_at: Date
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  sub_vendor_id: Mongoose.Types.ObjectId
  master_vendor_id: Mongoose.Types.ObjectId
}

const MongooseAdmixtureRateSchema = new Mongoose.Schema(
  {
    address_id: Mongoose.Schema.Types.ObjectId,
    brand_id: Mongoose.Schema.Types.ObjectId,
    category_id: Mongoose.Schema.Types.ObjectId,
    per_kg_rate: Number,
    vendor_id: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    sub_vendor_id: Mongoose.Schema.Types.ObjectId,
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "admixture_rate", versionKey: false }
)

export const AdmixtureRateModel: Mongoose.Model<IAdmixtureRateDoc> = Mongoose.model<
  IAdmixtureRateDoc
>("admixture_rate", MongooseAdmixtureRateSchema)
