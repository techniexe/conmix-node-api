import * as Mongoose from "mongoose"
export interface IVendorSettingDoc extends Mongoose.Document {
  with_TM: boolean
  TM_price: number
  with_CP: boolean
  CP_price: number
  is_customize_design_mix: boolean
  created_at: Date
  vendor_id: Mongoose.Types.ObjectId
  updated_at: Date
  updated_by: Mongoose.Types.ObjectId
  new_order_taken: boolean
  master_vendor_id:Mongoose.Types.ObjectId
}

const mongooseVendorSettingSchema = new Mongoose.Schema(
  {
    with_TM: { type: Boolean, index: true, default: false },
    TM_price: Number,
    with_CP: { type: Boolean, index: true, default: false },
    CP_price: Number,
    is_customize_design_mix: { type: Boolean, index: true, default: true },
    created_at: { type: Date, default: Date.now },
    vendor_id: Mongoose.Schema.Types.ObjectId,
    updated_at: Date,
    updated_by: Mongoose.Schema.Types.ObjectId,
    new_order_taken: { type: Boolean, default: true },
    master_vendor_id: Mongoose.Schema.Types.ObjectId,
  },
  { collection: "vendor_setting", versionKey: false }
)

export const VendorSettingModel: Mongoose.Model<IVendorSettingDoc> = Mongoose.model<
  IVendorSettingDoc
>("vendor_setting", mongooseVendorSettingSchema)
