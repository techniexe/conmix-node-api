import * as Mongoose from "mongoose"

export interface ICementGradeDoc extends Mongoose.Document {
  name: string
  created_by: Mongoose.Types.ObjectId
  created_at: Date
  is_deleted: boolean
  deleted_at: Date
  deleted_by: Mongoose.Types.ObjectId
  cement_grade_date: Date
}

const mongooseCementGradeSchema = new Mongoose.Schema(
  {
    name: {type: String, unique: true},
    created_by: Mongoose.Schema.Types.ObjectId,
    created_at: { type: Date, default: Date.now },
    is_deleted: { type: Boolean, default: false },
    deleted_at: Date,
    deleted_by: Mongoose.Schema.Types.ObjectId,
    cement_grade_date: { type: Date, default: Date.now },
  },
  { collection: "cement_grade", versionKey: false }
).index({
  name: "text",
})

export const CementGradeModel: Mongoose.Model<ICementGradeDoc> = Mongoose.model<
  ICementGradeDoc
>("cement_grade", mongooseCementGradeSchema)
