import * as Mongoose from "mongoose"

export interface IBuyerBillingAddressDoc extends Mongoose.Document {
  user: {
    user_type: string
    user_id: Mongoose.Types.ObjectId
  }
  full_name: string
  company_name: string
  line1: string
  line2?: string
  state_id: Mongoose.Types.ObjectId
  city_id: Mongoose.Types.ObjectId
  pincode?: number
  gst_number?: string
  created_at: Date
  updated_at?: Date
  is_deleted: boolean
  deleted_at?: Date 
  [k: string]: any
}

const mongooseBuyerBillingAddressSchema = new Mongoose.Schema(
  {
    user: {
      user_type: String, //collection name: buyer/vendor/
      user_id: {
        type: Mongoose.Schema.Types.ObjectId,
        refPath: "user.user_type",
      },
    },
    full_name: String,
    company_name: String,
    line1: String,
    line2: String,
    state_id: Mongoose.Schema.Types.ObjectId,
    city_id: Mongoose.Schema.Types.ObjectId,
    pincode: { type: Number, index: true },
    gst_number: String,
    created_at: { type: Date, default: Date.now, index: true },
    updated_at: Date,
    is_deleted: { type: Boolean, default: false, index: true },
    deleted_at: Date,
  },
  { collection: "buyer_billing_address", versionKey: false }
)
export const BuyerBillingAddressModel: Mongoose.Model<IBuyerBillingAddressDoc> = Mongoose.model<
  IBuyerBillingAddressDoc
>("buyer_billing_address", mongooseBuyerBillingAddressSchema)
