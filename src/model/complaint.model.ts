import * as Mongoose from "mongoose"

export interface IComplaintDoc extends Mongoose.Document {
  user_id: Mongoose.Types.ObjectId
  order_id: Mongoose.Types.ObjectId
  complaint_text: string
  category_id?: Mongoose.Types.ObjectId
  sub_category_id?: Mongoose.Types.ObjectId
  read_by_admin: boolean
  created_at: Date
  read_unread_at: Date
}

const MongooseComplaintSchema = new Mongoose.Schema(
  {
    user_id: Mongoose.Schema.Types.ObjectId,
    order_id: Mongoose.Schema.Types.ObjectId,
    complaint_text: String,
    category_id: Mongoose.Schema.Types.ObjectId,
    sub_category_id: Mongoose.Schema.Types.ObjectId,
    read_by_admin: { type: Boolean, index: true, default: false },
    created_at: { type: Date, default: Date.now, index: true },
    read_unread_at: Date,
  },
  { collection: "complaint", versionKey: false }
)

MongooseComplaintSchema.index({
  complaint_text: "text",
})

export const ComplaintModel: Mongoose.Model<IComplaintDoc> = Mongoose.model<
  IComplaintDoc
>("complaint", MongooseComplaintSchema)
