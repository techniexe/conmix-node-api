import { injectable } from "inversify"
import { controller, all } from "inversify-express-utils"
import { NextFunction, Request, Response } from "express"

@injectable()
@controller("*")
export class PnfController {
  @all("*")
  async pnf(req: Request, res: Response, next: NextFunction) {
    res.status(404).send("Page not found")
  }
}
