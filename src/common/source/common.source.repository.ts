import { injectable } from "inversify"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { SandSourceModel } from "../../model/sand_source.model"

@injectable()
export class SourceRepository {
  async sourceList() {
    const query: { [k: string]: any } = {}
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          //"region._id": 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await AggregateSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async aggregateSourceList() {
    const query: { [k: string]: any } = {}

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          aggregate_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await AggregateSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async flyAshsourceList() {
    const query: { [k: string]: any } = {}
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          fly_ash_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await FlyAshSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async sandSourceList() {
    const query: { [k: string]: any } = {}

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          sand_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await SandSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }
}
