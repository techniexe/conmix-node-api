import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { Response } from "express"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { SourceRepository } from "./common.source.repository"

@injectable()
@controller("/v1/source")
export class SourceController {
  constructor(
    @inject(CommonTypes.SourceRepository)
    private sourceRepo: SourceRepository
  ) {}

  @httpGet("/")
  async sourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sourceRepo.sourceList()
    res.json({ data })
  }

  @httpGet("/aggregate")
  async aggregateSourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sourceRepo.aggregateSourceList()
    res.json({ data })
  }

  @httpGet("/fly_ash")
  async flyAshSourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sourceRepo.flyAshsourceList()
    res.json({ data })
  }

  @httpGet("/sand")
  async sandSourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sourceRepo.sandSourceList()
    res.json({ data })
  }
}
