import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { ReviewRepository } from "./common.review.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  GetVendorreviewSchema,
  IGetVendorreview,
} from "./common.review.req-schema"
import { CommonTypes } from "../common.types"

@controller("/v1/review")
@injectable()
export class ReviewController {
  constructor(
    @inject(CommonTypes.ReviewRepository)
    private reviewRepo: ReviewRepository
  ) {}

  //Get vendor review.
  @httpGet("/getVendorReview/:vendor_id", validate(GetVendorreviewSchema))
  async getVendorReview(
    req: IGetVendorreview,
    res: Response,
    next: NextFunction
  ) {
    const { vendor_id } = req.params
    const { before, after } = req.query
    try {
      const data = await this.reviewRepo.getVendorReview(
        vendor_id,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
