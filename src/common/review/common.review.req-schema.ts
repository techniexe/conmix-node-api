import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAddVendorReview {
  user_id?: string
  //sub_category_id?: string | ObjectID
  vendor_id: string
  review_text: string
  rating?: number
  status: string
}

export interface IAddVendorReviewRequest extends IAuthenticatedRequest {
  body: IAddVendorReview
}

export const addVendorReviewSchema: IRequestSchema = {
  body: Joi.object().keys({
    vendor_id: Joi.string().regex(objectIdRegex).required(),
    review_text: Joi.string().required(),
    rating: Joi.number().valid([1, 2, 3, 4, 5]).required(),
    status: Joi.string().default("unpublished").allow("published, unpublished"),
  }),
}

export interface IGetVendorreview extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
  }
  params: {
    vendor_id: string
  }
}

export const GetVendorreviewSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    //supplier_id: Joi.string().regex(objectIdRegex).required(),
  }),
  params: Joi.object().keys({
    vendor_id: Joi.string().regex(objectIdRegex).required(),
  }),
}
