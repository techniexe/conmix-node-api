import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ReviewModel } from "../../model/review.model"
import { ObjectId } from "bson"

@injectable()
export class ReviewRepository {
  async getVendorReview(vendor_id: string, before?: string, after?: string) {
    console.log("vendor_id", vendor_id)

    const query: { [k: string]: any } = {
      status: "published",
    }
    query.vendor_id = new ObjectId(vendor_id)

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
      sort.created_at = 1
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "buyer._id": 1,
          "buyer.full_name": 1,
          //sub_category_id: 1,
          vendor_id: 1,
          review_text: 1,
          rating: 1,
          status: 1,
          created_at: 1,
          modirated_at: 1,
        },
      },
    ]

    const reviews = await ReviewModel.aggregate(aggregateArr).limit(40)

    if (sort.created_at === 1) {
      reviews.reverse()
    }

    return { reviews }
  }
}
