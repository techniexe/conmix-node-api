import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { CementBrandRepository } from "./cement_brand.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"

@controller("/v1/cement_brand")
@injectable()
export class CementBrandController {
  constructor(
    @inject(CommonTypes.CementBrandRepository)
    private cementBrandRepo: CementBrandRepository
  ) {}

  @httpGet("/")
  async getCementBrand(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.cementBrandRepo.getCementBrand(
      search,
      before,
      after
    )
    res.json({ data })
  }
}
