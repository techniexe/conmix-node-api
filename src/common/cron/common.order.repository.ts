import {
  surpriseOrderIntervel,
  razor_key,
  razor_secret,
} from "./../../utilities/config"
import {
  ClientNotificationType,
  VendorNotificationType,
  AdminNotificationType,
} from "./../../model/notification.model"
import { SurpriseOrderModel } from "./../../model/surprise_order.model"
import { SupplierOrderStatus } from "../../model/vendor.order.model"
import { injectable, inject } from "inversify"
import { IOrderDoc, OrderModel, OrderStatus } from "../../model/order.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { OrderItemModel } from "../../model/order_item.model"
import { CommonTypes } from "../common.types"
import { UniqueidService } from "../../utilities/uniqueid-service"
import {
  OrderItemStatus,
  UserType,
  supplierOrderProcessingInterval,
  //surpriseOrderIntervel,
} from "../../utilities/config"
import { PaymentStatus } from "../../model/logistics.order.model"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { AddressModel } from "../../model/address.model"
import { NotificationUtility } from "../../utilities/notification.utility"
//const surpriseOrdercCheck = surpriseOrderIntervel * 1000 * 60 * 60
import { ObjectId } from "bson"
import { CommonService } from "../../utilities/common.service"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { CementOSModel } from "../../model/cement_os.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { SandOSModel } from "../../model/sand_os.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AggregateOSModel } from "../../model/agg_os.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { AdmixtureOSModel } from "../../model/admix_os.model"
import { AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { GstSlabModel } from "../../model/gst_slab.model"
import { BillingAddressModel } from "../../model/billing_address.model"
import { StateModel } from "../../model/region.model"
import { TestService } from "../../utilities/test.service"
import { SiteModel } from "../../model/site.model"
import { MailGunService } from "../../utilities/mailgun.service"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
import { client_mail_template } from "../../utilities/client_email_tempalte"

const orderProcessCheck = supplierOrderProcessingInterval * 1000 * 60 * 60
const SurpriseOrderProcessCheck = surpriseOrderIntervel * 1000 * 60

const Razorpay = require("razorpay")

let rzp = new Razorpay({
  key_id: razor_key, // your `KEY_ID`
  key_secret: razor_secret, // your `KEY_SECRET`
})

@injectable()
export class CommonOrderRepositroy {
  constructor(
    @inject(CommonTypes.UniqueidService)
    private uniqueIdService: UniqueidService,
    @inject(CommonTypes.NotificationUtility)
    private notUtil: NotificationUtility,
    @inject(CommonTypes.CommonService) private commonService: CommonService,
    @inject(CommonTypes.TestService) private testService: TestService,
    @inject(CommonTypes.mailgunService) private mailgunService: MailGunService
    ) {}

  async processOrder(order_id: string) {
    console.log(`Processing order: ${order_id}`)
    const orderInfo = await OrderModel.findOne({ _id: order_id })
    if (isNullOrUndefined(orderInfo)) {
      console.log(`Invalid order id ${order_id} `)
      return Promise.reject(new InvalidInput(`Invalid order id`, 400))
    }
    if (orderInfo.order_status !== OrderStatus.PLACED) {
      console.log(
        `Order status is ${orderInfo.order_status}, Required order status: ${OrderStatus.PLACED}`
      )
      return Promise.reject(
        new InvalidInput(`Order is already processed.`, 400)
      )
    }
    console.log(`order info: `, orderInfo)
    const orderItems = await OrderItemModel.find({ order_id })
    if (orderItems.length === 0) {
      console.log(`No items found for order: ${order_id} `)
      return Promise.reject(new InvalidInput(`No item found on order`, 400))
    }
    console.log(`order item: `, orderItems)
    const vendorOrderData: any[] = []
    // const quotes: any[] = []
    await Promise.all(
      orderItems.map(async (oItem) => {
        let amount: any = oItem.selling_price + oItem.TM_price + oItem.CP_price
        amount = amount.toFixed(2)
        amount = parseFloat(amount)

        let unit_price: any = amount / oItem.quantity
        unit_price = unit_price.toFixed(2)
        unit_price = parseFloat(unit_price)

        let selling_price: any = unit_price * oItem.quantity
        selling_price = selling_price.toFixed(2)
        selling_price = parseFloat(selling_price)

        const gst_slab = await GstSlabModel.findOne()
        console.log("gst", gst_slab)
        let total_amount: any
        if (!isNullOrUndefined(gst_slab)) {
          let cgst_rate = gst_slab.cgst_rate
          let sgst_rate = gst_slab.sgst_rate
          let igst_rate = gst_slab.igst_rate

          console.log("cgst_rate", cgst_rate)
          console.log("sgst_rate", sgst_rate)
          console.log("igst_rate", igst_rate)
          let cgst_price: any = (amount * cgst_rate) / 100
          cgst_price = cgst_price.toFixed(2)
          cgst_price = parseFloat(cgst_price)

          let sgst_price: any = (amount * sgst_rate) / 100
          sgst_price = sgst_price.toFixed(2)
          sgst_price = parseFloat(sgst_price)

          let igst_price: any = (amount * igst_rate) / 100
          igst_price = igst_price.toFixed(2)
          igst_price = parseFloat(igst_price)

          let gst_price = cgst_price + sgst_price + igst_price
          console.log("gst_price", gst_price)

          let addressData = await AddressModel.findOne({
            _id: new ObjectId(oItem.address_id),
          })
          if (isNullOrUndefined(addressData)) {
            return Promise.reject("Address Details not found")
          }
          console.log("addressData", addressData)

          let vendorBillingAddressData = await BillingAddressModel.findOne({
            _id: new ObjectId(addressData.billing_address_id),
          })
          if (isNullOrUndefined(vendorBillingAddressData)) {
            return Promise.reject("Vendor billing address Details not found")
          }

          console.log("vendorBillingAddressData", vendorBillingAddressData)
          if (!isNullOrUndefined(vendorBillingAddressData)) {
            let stateDetails = await StateModel.findOne({
              _id: new ObjectId(vendorBillingAddressData.state_id),
            })
            if (!isNullOrUndefined(stateDetails)) {
              console.log("state", stateDetails)
              if (stateDetails.state_name === "Gujarat") {
                oItem.cgst_price = cgst_price
                oItem.sgst_price = sgst_price
                oItem.igst_price = 0
                oItem.gst_price = oItem.cgst_price + oItem.sgst_price
              } else {
                oItem.cgst_price = 0
                oItem.sgst_price = 0
                oItem.igst_price = igst_price
                oItem.gst_price = oItem.igst_price
              }
            }
          } else {
            oItem.cgst_price = cgst_price
            oItem.sgst_price = sgst_price
            oItem.igst_price = 0
            oItem.gst_price = cgst_price + sgst_price
          }
          console.log("gst_price", gst_price)
          gst_price = oItem.gst_price.toFixed(2)
          oItem.gst_price = parseFloat(gst_price)

          total_amount = amount + oItem.gst_price
        }

        total_amount = total_amount.toFixed(2)
        total_amount = parseFloat(total_amount)
        console.log("total_amount", total_amount)

        // let total_amount_without_margin = selling_price + oItem.gst_price
        // total_amount_without_margin = total_amount_without_margin.toFixed(2)
        // total_amount_without_margin = parseFloat(total_amount_without_margin)

        let total_amount_without_margin = total_amount
        total_amount_without_margin = total_amount_without_margin.toFixed(2)
        total_amount_without_margin = parseFloat(total_amount_without_margin)

        vendorOrderData.push({
          _id: await this.uniqueIdService.getUniqueSupplierOrderId(),
          user_id: oItem.vendor_id,
          design_mix_id: oItem.design_mix_id,
          buyer_id: orderInfo.user_id,
          buyer_order_id: orderInfo._id,
          buyer_order_display_id: orderInfo.display_id,
          buyer_order_item_id: oItem._id,
          buyer_order_display_item_id: oItem.display_item_id,
          quantity: oItem.quantity,
          concrete_grade_id: oItem.concrete_grade_id,
          admix_brand_id: oItem.admix_brand_id,
          admix_cat_id: oItem.admix_cat_id,
          fly_ash_source_id: oItem.fly_ash_source_id,
          aggregate2_sub_cat_id: oItem.aggregate2_sub_cat_id,
          agg_source_id: oItem.agg_source_id,
          aggregate1_sub_cat_id: oItem.aggregate1_sub_cat_id,
          sand_source_id: oItem.sand_source_id,
          cement_brand_id: oItem.cement_brand_id,
          cement_grade_id: oItem.cement_grade_id,
          cement_quantity: oItem.cement_quantity,
          sand_quantity: oItem.sand_quantity,
          aggregate1_quantity: oItem.aggregate1_quantity,
          aggregate2_quantity: oItem.aggregate2_quantity,
          fly_ash_quantity: oItem.fly_ash_quantity,
          admix_quantity: oItem.admix_quantity,
          water_quantity: oItem.water_quantity,
          cement_per_kg_rate: oItem.cement_per_kg_rate,
          sand_per_kg_rate: oItem.sand_per_kg_rate,
          aggregate1_per_kg_rate: oItem.aggregate1_per_kg_rate,
          aggregate2_per_kg_rate: oItem.aggregate2_per_kg_rate,
          fly_ash_per_kg_rate: oItem.fly_ash_per_kg_rate,
          admix_per_kg_rate: oItem.admix_per_kg_rate,
          water_per_ltr_rate: oItem.water_per_ltr_rate,
          cement_price: oItem.cement_price,
          sand_price: oItem.sand_price,
          aggreagte1_price: oItem.aggreagte1_price,
          aggreagte2_price: oItem.aggreagte2_price,
          fly_ash_price: oItem.fly_ash_price,
          admix_price: oItem.admix_price,
          water_price: oItem.water_price,
          with_TM: oItem.with_TM,
          with_CP: oItem.with_CP,
          delivery_date: oItem.delivery_date,
          end_date: oItem.end_date,
          distance: oItem.distance,
          cgst_rate: oItem.cgst_rate,
          sgst_rate: oItem.sgst_rate,
          sgst_price: oItem.sgst_price,
          cgst_price: oItem.cgst_price,
          gst_price: oItem.gst_price,
          // item_status: oItem.item_status,
          // unit_price: oItem.unit_price,
          //selling_price: oItem.selling_price,
          margin_price: oItem.margin_price,
          TM_price: oItem.TM_price,
          CP_price: oItem.CP_price,
          total_CP_price: oItem.total_CP_price,
          order_status: SupplierOrderStatus.RECEIVED,
          payment_status: PaymentStatus.UNPAID,
          address_id: oItem.address_id,
          line1: oItem.line1,
          line2: oItem.line2,
          state_id: oItem.state_id,
          city_id: oItem.city_id,
          pincode: oItem.pincode,
          location: oItem.location,
          address_type: oItem.address_type, //warehouse/factory/quarry/kapchi/home/office/
          business_name: oItem.business_name,
          city_name: oItem.city_name,
          state_name: oItem.state_name,
          max_accepted_at: new Date(Date.now() + orderProcessCheck),
          total_amount,
          total_amount_without_margin,
          unit_price,
          selling_price,
          sub_vendor_id: oItem.sub_vendor_id,
          master_vendor_id: oItem.master_vendor_id,
          // unit_price: oItem.item_status,
          // base_amount: oItem.base_amount,
          // margin_rate: oItem.margin_rate,
          // margin_amount: oItem.margin_amount,
          //total_amount: oItem.total_amount,
        })

        await OrderItemModel.updateOne(
          { _id: oItem._id },
          {
            $set: {
              payment_status: PaymentStatus.PAID,
              item_status: OrderItemStatus.PROCESSING,
              processed_at: Date.now(),
            },
          }
          //  { session }
        )

        // await this.notUtil.addNotificationForBuyer({
        //   to_user_type: UserType.BUYER,
        //   to_user_id: orderInfo.user_id,
        //   notification_type: ClientNotificationType.orderProcess,
        //   order_id,
        //   order_item_id: oItem.display_item_id,
        // })

        return Promise.resolve()
      })
    )
    // const session = await Mongoose.startSession()
    // session.startTransaction()
    try {
      //await QuoteModel.create(quotes, { session })
      await VendorOrderModel.create(vendorOrderData)
      // await QuoteModel.create(quotes)

      let vendorOrder = await VendorOrderModel.find({
        buyer_order_id: order_id,
      })
      console.log("vendorOrder", vendorOrder)
      for (let j = 0; j < vendorOrder.length; j++) {
        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: vendorOrder[j].master_vendor_id,
          notification_type: VendorNotificationType.orderPlaced,
          vendor_order_id: vendorOrder[j]._id,
          client_id: vendorOrder[j].buyer_id,
          vendor_id: vendorOrder[j].user_id,
        })

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: vendorOrder[j].sub_vendor_id,
          notification_type: VendorNotificationType.orderPlaced,
          vendor_order_id: vendorOrder[j]._id,
          client_id: vendorOrder[j].buyer_id,
          vendor_id: vendorOrder[j].user_id,
        })

        let mastervendorData = await VendorUserModel.findOne({
          _id: new ObjectId(vendorOrder[j].master_vendor_id)
        })
        if(!isNullOrUndefined(mastervendorData)){
          let vendor_message = `Hi ${mastervendorData.company_name}, Congratulations ! You have received order with Product Id ${vendorOrder[j].buyer_order_display_item_id} having an Order ID ${vendorOrder[j]._id} at Conmix. Kindly login to your Partner Account to Accept the Order. - conmix`
          let vendor_templateId = "1707163784609302492"
          await this.testService.sendSMS(mastervendorData.mobile_number, vendor_message, vendor_templateId)
       
          let new_order_master_html = vendor_mail_template.new_order
          new_order_master_html = new_order_master_html.replace("{{mastervendorData.company_name}}", mastervendorData.company_name)
          new_order_master_html = new_order_master_html.replace("{{buyer_order_display_item_id}}", vendorOrder[j].buyer_order_display_item_id)
          new_order_master_html = new_order_master_html.replace("{{order_id}}", vendorOrder[j]._id)
          var data = {
            from: "no-reply@conmate.in",
            to: mastervendorData.email,
            subject: 'Conmix - New Order',
            html : new_order_master_html
          }
         await this.mailgunService.sendEmail(data)
       
        }

        let subvendorData = await VendorUserModel.findOne({
          _id: new ObjectId(vendorOrder[j].sub_vendor_id)
        })
        if(!isNullOrUndefined(subvendorData)){
          let vendor_message = `Hi ${subvendorData.company_name}, Congratulations ! You have received order with Product Id ${vendorOrder[j].buyer_order_display_item_id} having an Order ID ${vendorOrder[j]._id} at Conmix. Kindly login to your Partner Account to Accept the Order. - conmix`
          let vendor_templateId = "1707163784609302492"
          await this.testService.sendSMS(subvendorData.mobile_number, vendor_message, vendor_templateId)
        
          let new_order_sub_vendor_html = vendor_mail_template.new_order
          new_order_sub_vendor_html = new_order_sub_vendor_html.replace("{{mastervendorData.company_name}}", subvendorData.company_name)
          new_order_sub_vendor_html = new_order_sub_vendor_html.replace("{{buyer_order_display_item_id}}", vendorOrder[j].buyer_order_display_item_id)
          new_order_sub_vendor_html = new_order_sub_vendor_html.replace("{{order_id}}", vendorOrder[j]._id)
          var data = {
            from: "no-reply@conmate.in",
            to: subvendorData.email,
            subject: 'Conmix - New Order',
            html : new_order_sub_vendor_html
          }
         await this.mailgunService.sendEmail(data)
        }
      }

      await OrderModel.updateOne(
        { _id: order_id },
        {
          $set: {
            payment_status: PaymentStatus.PAID,
            order_status: OrderStatus.PROCESSING,
            processed_at: Date.now(),
          },
        }
        //  { session }
      )

      //session.commitTransaction()
      console.log(
        `Vendor Order created. Total vendor order: ${vendorOrderData.length}`
      )
    } catch (err) {
      //session.abortTransaction()
      //console.log(`[ERROR] Unable to create quotes. ${err.message}`)
      return Promise.reject(
        new InvalidInput(`Unable to create order. ${err.message}`, 400)
      )
    }
    return Promise.resolve()
  }

  async processVendorOrder() {
    const query: { [k: string]: any } = {}

    // query.vendor_id = new ObjectId(vendor_id)

    query.$and = [
      {
        order_status: { $ne: SupplierOrderStatus.ACCEPTED },
      },
      {
        max_accepted_at: { $gte: Date.now() },
      },
      {
        is_added_in_surprise_order: false,
      },
      // {
      //   master_vendor_id: new ObjectId(vendor_id),
      // },
    ]

    const vendorOrderCursor = await VendorOrderModel.find(query)
    //order_status: "RECEIVED",
    // max_accepted_at: { $lte: Date.now() },
    // order_status: SupplierOrderStatus.RECEIVED,
    // created_at: { $lt: new Date(Date.now() - surpriseOrdercCheck) },
    //  order created at checking here

    console.log("vendorOrderCursor", vendorOrderCursor)
    const vendor_ids1: any[] = []
    const surprise_order: any[] = []
    await Promise.all(
      vendorOrderCursor.map(async (orderData) => {
        const location = orderData.location

        const addressData = await AddressModel.aggregate([
          {
            $geoNear: {
              near: location,
              distanceField: "calculated_distance",
              // distanceMultiplier: 0.001,
              // maxDistance: maxVehicleToPickupDistance * 1000, //   <distance in meters>
              // spherical: true,
              // query: {
              //   is_active: true,
              //   delivery_range: { $gte: oItem.dvlDistance },
              // },
            },
          },
          {
            $group: {
              _id: "$user.user_id",
            },
          },
        ])
        // console.log("addressData", addressData)
        if (addressData.length === 0) {
          return Promise.reject(new InvalidInput(`No addressData found `, 400))
        }
        for (let i = 0; i < addressData.length; i++) {
          vendor_ids1.push(addressData[i]._id)
        }

        for (var k = 0; k < vendor_ids1.length; k++) {
          if (vendor_ids1[k]._id.equals(orderData.user_id)) {
            console.log("heree")
            vendor_ids1.splice(k, 1)
          }
        }

        //  vendor_ids1.pull(orderData.user_id)

        const vendor_ids = await this.commonService.removeDuplicates(
          vendor_ids1
        )
        console.log("vendor_ids", vendor_ids.length)
        for (let j = 0; j < vendor_ids.length; j++) {
          surprise_order.push({
            _id: await this.uniqueIdService.getUniqueSupplierOrderId(),
            user_id: vendor_ids[j],
            vendor_order_id: orderData._id,
            design_mix_id: orderData.design_mix_id,
            buyer_id: orderData.buyer_id,
            buyer_order_id: orderData.buyer_order_id,
            buyer_order_display_id: orderData.buyer_order_display_id,
            buyer_order_item_id: orderData.buyer_order_item_id,
            buyer_order_display_item_id: orderData.buyer_order_display_item_id,
            quantity: orderData.quantity,
            concrete_grade_id: orderData.concrete_grade_id,
            admix_brand_id: orderData.admix_brand_id,
            admix_cat_id: orderData.admix_cat_id,
            fly_ash_source_id: orderData.fly_ash_source_id,
            aggregate2_sub_cat_id: orderData.aggregate2_sub_cat_id,
            agg_source_id: orderData.agg_source_id,
            aggregate1_sub_cat_id: orderData.aggregate1_sub_cat_id,
            sand_source_id: orderData.sand_source_id,
            cement_brand_id: orderData.cement_brand_id,
            cement_grade_id: orderData.cement_grade_id,
            cement_quantity: orderData.cement_quantity,
            sand_quantity: orderData.sand_quantity,
            aggregate1_quantity: orderData.aggregate1_quantity,
            aggregate2_quantity: orderData.aggregate2_quantity,
            fly_ash_quantity: orderData.fly_ash_quantity,
            admix_quantity: orderData.admix_quantity,
            water_quantity: orderData.water_quantity,
            cement_per_kg_rate: orderData.cement_per_kg_rate,
            sand_per_kg_rate: orderData.sand_per_kg_rate,
            aggregate1_per_kg_rate: orderData.aggregate1_per_kg_rate,
            aggregate2_per_kg_rate: orderData.aggregate2_per_kg_rate,
            fly_ash_per_kg_rate: orderData.fly_ash_per_kg_rate,
            admix_per_kg_rate: orderData.admix_per_kg_rate,
            water_per_ltr_rate: orderData.water_per_ltr_rate,
            cement_price: orderData.cement_price,
            sand_price: orderData.sand_price,
            aggreagte1_price: orderData.aggreagte1_price,
            aggreagte2_price: orderData.aggreagte2_price,
            fly_ash_price: orderData.fly_ash_price,
            admix_price: orderData.admix_price,
            water_price: orderData.water_price,
            with_TM: orderData.with_TM,
            with_CP: orderData.with_CP,
            delivery_date: orderData.delivery_date,
            end_date: orderData.end_date,
            distance: orderData.distance,
            cgst_rate: orderData.cgst_rate,
            sgst_rate: orderData.sgst_rate,
            sgst_price: orderData.sgst_price,
            cgst_price: orderData.cgst_price,
            gst_price: orderData.gst_price,
            // item_status: oItem.item_status,
            unit_price: orderData.unit_price,
            selling_price: orderData.selling_price,
            margin_price: orderData.margin_price,
            TM_price: orderData.TM_price,
            CP_price: orderData.CP_price,
            total_CP_price: orderData.total_CP_price,
            order_status: SupplierOrderStatus.RECEIVED,
            payment_status: PaymentStatus.UNPAID,
            address_id: orderData.address_id,
            sub_vendor_id: orderData.sub_vendor_id,
            master_vendor_id: orderData.master_vendor_id,
            line1: orderData.line1,
            line2: orderData.line2,
            state_id: orderData.state_id,
            city_id: orderData.city_id,
            pincode: orderData.pincode,
            location: orderData.location,
            address_type: orderData.address_type, //warehouse/factory/quarry/kapchi/home/office/
            business_name: orderData.business_name,
            city_name: orderData.city_name,
            state_name: orderData.state_name,
            total_amount: orderData.total_amount,
            total_amount_without_margin: orderData.total_amount_without_margin,
            max_accepted_at: new Date(Date.now() + SurpriseOrderProcessCheck),
          })

          let vendorUserData = await VendorUserModel.findOne({
            _id: new ObjectId(vendor_ids[j])
          })
          //let vendor_name: any
          let vendor_mobile_number: any
          let vendor_company_name: any
          let vendor_email: any
          if(!isNullOrUndefined(vendorUserData)){
           // vendor_name = vendorUserData.full_name
            vendor_mobile_number = vendorUserData.mobile_number
            vendor_company_name = vendorUserData.company_name
            vendor_email = vendorUserData.email
          }
          let vendor_message = `Hi ${vendor_company_name}, you have received a Surprise Order for Product Id ${orderData.buyer_order_display_item_id} having an Order Id ${orderData._id} Kindly login to Accept the order. - conmix.`
          let vendor_templateId = "1707163732943470225"
          await this.testService.sendSMS(vendor_mobile_number, vendor_message, vendor_templateId)
          

          let surprise_order_html = vendor_mail_template.surprise_order
          surprise_order_html = surprise_order_html.replace("{{vendor_company_name}}", vendor_company_name)
          surprise_order_html = surprise_order_html.replace("{{orderData.buyer_order_display_item_id}}", orderData.buyer_order_display_item_id)
          surprise_order_html = surprise_order_html.replace("{{orderData._id}}", orderData._id)
          
          var data11 = {
          from: "no-reply@conmate.in",
          to: vendor_email,
          subject: "Conmix - Surprise Order",
          html: surprise_order_html,
        }
        this.mailgunService.sendEmail(data11)
        }
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: orderData._id,
          },
          {
            $set: {
              is_added_in_surprise_order: true,
            },
          }
        )
        return Promise.resolve()
      })
    )

    console.log("surprise_order", surprise_order.length)
    try {
      await SurpriseOrderModel.create(surprise_order)
      console.log(
        `Surprise Order created. Total surprise order: ${surprise_order.length}`
      )
    } catch (err) {
      console.log(`[ERROR] Unable to Surprise order. ${err.message}`)
      return Promise.reject(
        new InvalidInput(`Unable to Surprise quotes. ${err.message}`, 400)
      )
    }
  }

  async processSurpriseOrder() {
    const surpriseOrderCursor = await SurpriseOrderModel.find({
      order_status: "RECEIVED",
      max_accepted_at: { $lte: Date.now() },
      // order_status: SupplierOrderStatus.RECEIVED,
      // created_at: { $lt: new Date(Date.now() - surpriseOrdercCheck) },
      //  order created at checking here
    })

    if (
      !isNullOrUndefined(surpriseOrderCursor) &&
      surpriseOrderCursor.length > 0
    ) {
      for (let i = 0; i < surpriseOrderCursor.length; i++) {
        await SurpriseOrderModel.findOneAndUpdate(
          {
            _id: surpriseOrderCursor[i]._id,
          },
          {
            $set: {
              order_status: SupplierOrderStatus.REJECTED,
            },
          }
        )

        let orderItemDoc = await OrderItemModel.findOneAndUpdate(
          { _id: new ObjectId(surpriseOrderCursor[i].buyer_order_item_id) },
          {
            $set: {
              item_status: OrderItemStatus.CANCELLED,
              cancelled_at: Date.now(),
            },
          }
        )

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: surpriseOrderCursor[i].user_id,
          notification_type: VendorNotificationType.orderCancelled,
          vendor_order_id: surpriseOrderCursor[i]._id,
          vendor_id: surpriseOrderCursor[i].user_id,
          order_id: surpriseOrderCursor[i].buyer_order_id,
          order_item_id: surpriseOrderCursor[i].buyer_order_item_id,
        })

        await this.notUtil.addNotificationForBuyer({
          to_user_type: UserType.BUYER,
          to_user_id: surpriseOrderCursor[i].buyer_id,
          notification_type: ClientNotificationType.partiallyOrderCancelled,
          order_id: surpriseOrderCursor[i].buyer_order_id,
          order_item_id: surpriseOrderCursor[i].buyer_order_item_id,
          vendor_id: surpriseOrderCursor[i].user_id,
        })

        if (isNullOrUndefined(orderItemDoc)) {
          return Promise.reject(
            new InvalidInput(`No Order item doc were found `, 400)
          )
        }

        // Refund.
        let ClientOrderDoc = await OrderModel.findOne({
          _id: new ObjectId(surpriseOrderCursor[i].buyer_order_id),
        })
        if (isNullOrUndefined(ClientOrderDoc)) {
          return Promise.reject(new InvalidInput(`No order Doc found.`, 400))
        }

        let gateway_transaction_id = ClientOrderDoc.gateway_transaction_id
        const resp: { [k: string]: any } = {}
        let amount: any = orderItemDoc.total_amount * 100
        await rzp.payments
          .refund(gateway_transaction_id, {
            amount: parseInt(amount),
          })
          .then((data1: any) => {
            console.log("data1", data1)
            resp.refund_id = data1.id
            // success
          })
          .catch((error: any) => {
            console.error(error)
            // error
          })

        await OrderItemModel.findOneAndUpdate(
          { _id: new ObjectId(surpriseOrderCursor[i].buyer_order_item_id) },
          {
            $set: {
              refund_id: resp.refund_id,
            },
          }
        )

        // check whole order cancelled or not.
        let doc = await OrderItemModel.find({
          order_id: new ObjectId(surpriseOrderCursor[i].buyer_order_id),
          item_status: { $ne: OrderItemStatus.CANCELLED },
        })
        if (doc.length <= 0) {
          await OrderModel.findOneAndUpdate(
            {
              _id: new ObjectId(surpriseOrderCursor[i].buyer_order_id),
            },
            {
              $set: {
                order_status: OrderStatus.CANCELLED,
                cancelled_at: Date.now(),
              },
            }
          )

          await this.notUtil.addNotificationForBuyer({
            to_user_type: UserType.BUYER,
            to_user_id: orderItemDoc.buyer_id,
            notification_type: ClientNotificationType.orderCancelled,
            order_id: surpriseOrderCursor[i].buyer_order_id,
            order_item_id: orderItemDoc.display_item_id,
            vendor_id: surpriseOrderCursor[i].user_id,
          })
      
      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i < adminUserDocs.length ; i++){
          await this.notUtil.addNotificationForAdmin({
            to_user_type: UserType.ADMIN,
            to_user_id: adminUserDocs[i]._id,
            notification_type: AdminNotificationType.orderCancelled,
            order_id: surpriseOrderCursor[i].buyer_order_id,
            order_item_id: orderItemDoc.display_item_id,
            vendor_id: surpriseOrderCursor[i].user_id,
          })
        }
          // Send notofication to Supplier.
          await this.notUtil.addNotificationForSupplier({
            to_user_type: UserType.VENDOR,
            to_user_id: surpriseOrderCursor[i].user_id,
            notification_type: VendorNotificationType.orderCancelled,
            vendor_order_id: surpriseOrderCursor[i]._id,
            vendor_id: surpriseOrderCursor[i].user_id,
            order_id: surpriseOrderCursor[i].buyer_order_id,
            order_item_id: orderItemDoc.display_item_id,
          })
        }
      }
    }
  }

  async checkProductStock() {
    await this.checkCementQuantity()
    await this.checkSandQuantity()
    await this.checkAggQuantity()
    await this.adMixQuantity()
    return Promise.resolve()
  }

  async checkCementQuantity() {
    // For cement.
    let custom: any[] = []
    let cementdata = await CementQuantityModel.find().select({
      sub_vendor_id: 1,
    })
    console.log("cementdata", cementdata)

    for (let i = 0; i < cementdata.length; i++) {
      custom.push(cementdata[i].sub_vendor_id)
    }
    const vendor_ids = await this.commonService.removeDuplicates(custom)
    console.log("vendor_id", vendor_ids)

    let cqData: any
    for (let j = 0; j < vendor_ids.length; j++) {
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor_ids[j]),
      })

      console.log("vendorData", vendorData)

      let vendor_capacity: any
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }
      console.log("vendor_capacity", vendor_capacity)

      let cement_total_quantity = 130 * vendor_capacity

      console.log("cement_total_quantity", cement_total_quantity)

      let cementquery: { [k: string]: any } = {}
      cementquery.quantity = {
        $lte: cement_total_quantity,
      }
      const aggregateArr: any[] = [
        { $match: cementquery },
        {
          $project: {
            _id: true,
            address_id: true,
            brand_id: true,
            grade_id: true,
            quantity: true,
            vendor_id: true,
            sub_vendor_id: true,
            master_vendor_id: true,
          },
        },
      ]
      cqData = await CementQuantityModel.aggregate(aggregateArr)
      console.log("cqData", cqData)
    }
    for (let i = 0; i < cqData.length; i++) {
      cqData[i]._id = new ObjectId()
      const coData = new CementOSModel(cqData[i])
      await coData.save()
    }
    return Promise.resolve()
  }

  async checkSandQuantity() {
    let custom: any[] = []
    let sanddata = await SandSourceQuantityModel.find().select({
      sub_vendor_id: 1,
    })
    console.log("sanddata", sanddata)

    for (let i = 0; i < sanddata.length; i++) {
      custom.push(sanddata[i].sub_vendor_id)
    }
    const s_vendor_ids = await this.commonService.removeDuplicates(custom)
    console.log("s_vendor_ids", s_vendor_ids)

    let sqData: any
    for (let j = 0; j < s_vendor_ids.length; j++) {
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(s_vendor_ids[j]),
      })

      console.log("vendorData", vendorData)

      let vendor_capacity: any
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }
      console.log("vendor_capacity", vendor_capacity)

      let sand_total_quantity = 640 * vendor_capacity

      console.log("sand_total_quantity", sand_total_quantity)

      let sandquery: { [k: string]: any } = {}
      sandquery.quantity = {
        $lte: sand_total_quantity,
      }
      const aggregateArr: any[] = [
        { $match: sandquery },
        {
          $project: {
            _id: true,
            address_id: true,
            source_id: true,
            quantity: true,
            vendor_id: true,
            sub_vendor_id: true,
            master_vendor_id: true,
          },
        },
      ]
      sqData = await SandSourceQuantityModel.aggregate(aggregateArr)
      console.log("sqData", sqData)
    }
    for (let i = 0; i < sqData.length; i++) {
      sqData[i]._id = new ObjectId()
      const soData = new SandOSModel(sqData[i])
      await soData.save()
    }
    return Promise.resolve()
  }

  async checkAggQuantity() {
    let custom: any[] = []
    let subcatData: any
    let aggdata = await AggregateSourceQuantityModel.find().select({
      sub_vendor_id: 1,
    })
    console.log("aggdata", aggdata)

    for (let i = 0; i < aggdata.length; i++) {
      subcatData = await AggregateSandSubCategoryModel.findOne({
        _id: new ObjectId(aggdata[i].sub_cat_id),
      })

      custom.push(aggdata[i].sub_vendor_id)
    }
    const vendor_ids = await this.commonService.removeDuplicates(custom)
    console.log("vendor_ids", vendor_ids)

    let aggData: any
    let agg_total_quantity: any
    for (let j = 0; j < vendor_ids.length; j++) {
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor_ids[j]),
      })

      console.log("vendorData", vendorData)

      let vendor_capacity: any
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }
      console.log("vendor_capacity", vendor_capacity)

      if (
        !isNullOrUndefined(subcatData) &&
        subcatData.sub_category_name === "10mm"
      ) {
        agg_total_quantity = 416 * vendor_capacity
      }
      if (
        !isNullOrUndefined(subcatData) &&
        subcatData.sub_category_name === "20mm"
      ) {
        agg_total_quantity = 848 * vendor_capacity
      }
      if (
        !isNullOrUndefined(subcatData) &&
        subcatData.sub_category_name === "40mm"
      ) {
        agg_total_quantity = 690 * vendor_capacity
      }

      console.log("agg_total_quantity", agg_total_quantity)

      let aggquery: { [k: string]: any } = {}
      aggquery.quantity = {
        $lte: agg_total_quantity,
      }
      const aggregateArr: any[] = [
        { $match: aggquery },
        {
          $project: {
            _id: true,
            address_id: true,
            source_id: true,
            sub_cat_id: true,
            quantity: true,
            vendor_id: true,
            sub_vendor_id: true,
            master_vendor_id: true,
          },
        },
      ]
      aggData = await AggregateSourceQuantityModel.aggregate(aggregateArr)
      console.log("aggData", aggData)
    }
    for (let i = 0; i < aggData.length; i++) {
      aggData[i]._id = new ObjectId()
      const soData = new AggregateOSModel(aggData[i])
      await soData.save()
    }
    return Promise.resolve()
  }

  async adMixQuantity() {
    let custom: any[] = []
    let admixdata = await AdmixtureQuantityModel.find().select({
      sub_vendor_id: 1,
    })
    console.log("admixdata", admixdata)

    for (let i = 0; i < admixdata.length; i++) {
      custom.push(admixdata[i].sub_vendor_id)
    }
    const vendor_ids = await this.commonService.removeDuplicates(custom)
    console.log("vendor_ids", vendor_ids)

    let admixData: any
    for (let j = 0; j < vendor_ids.length; j++) {
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor_ids[j]),
      })

      console.log("vendorData", vendorData)

      let vendor_capacity: any
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }
      console.log("vendor_capacity", vendor_capacity)

      let admix_total_quantity = 0.01 * vendor_capacity
      console.log("admix_total_quantity", admix_total_quantity)

      let admixquery: { [k: string]: any } = {}
      admixquery.quantity = {
        $lte: admix_total_quantity,
      }
      const aggregateArr: any[] = [
        { $match: admixquery },
        {
          $project: {
            _id: true,
            address_id: true,
            brand_id: true,
            category_id: true,
            quantity: true,
            vendor_id: true,
            sub_vendor_id: true,
            master_vendor_id: true,
          },
        },
      ]
      admixData = await AggregateSourceQuantityModel.aggregate(aggregateArr)
      console.log("admixData", admixData)
    }
    for (let i = 0; i < admixData.length; i++) {
      admixData[i]._id = new ObjectId()
      const soData = new AdmixtureOSModel(admixData[i])
      await soData.save()
    }
    return Promise.resolve()
  }

  async notifiedScheduledAppointment(notification_sent_type: string) {
    const query: { [k: string]: any } = {}
    console.log(notification_sent_type)
    let lt = new Date().getTime() + 7 * 24 * 60 * 60 * 1000
    let gt = new Date().getTime() + 6 * 24 * 60 * 60 * 1000

    if (notification_sent_type === "OneDayBefore") {
      lt = new Date().getTime() + 2 * 24 * 60 * 60 * 1000
      gt = new Date().getTime() + 1 * 24 * 60 * 60 * 1000
      query.one_day_notification_sent = {
        $exists: false,
      }
    } else {
      query.seven_day_notification_sent = {
        $exists: false,
      }
    }
    query.created_at = {
      $lt: lt,
      $gt: gt,
    }
    console.log(query)
    const orderData = await OrderModel.find(query)
    if (orderData.length < 1) {
      return Promise.reject(new InvalidInput(`Order does not exist..`, 400))
    }

    for (let i = 0; i < orderData.length; i++) {
      let check = await OrderItemPartModel.findOne({
        buyer_order_id: new ObjectId(orderData[i]._id),
      })
      if (isNullOrUndefined(check)) {
        await this.sendNotification(orderData[i], notification_sent_type)
      }
      // console.log(appointmentData)
    }
  }
  async sendNotification(orderData: IOrderDoc, notification_sent_type: string) {
    const clientUserDoc = await BuyerUserModel.findById(orderData.user_id)
    if (isNullOrUndefined(clientUserDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Client does not exist. associated with this data.`,
          400
        )
      )
    }

    // let client_name = clientUserDoc.full_name
    // let client_mobile_number = clientUserDoc.mobile_number

    let client_name: any
    let client_mobile_number: any
    if (!isNullOrUndefined(clientUserDoc)) {
      client_mobile_number = clientUserDoc.mobile_number
      if (clientUserDoc.account_type === "Individual") {
        client_name = clientUserDoc.full_name
      } else {
        client_name = clientUserDoc.company_name
      }
    }

    if (notification_sent_type === "SevenDayBefore") {
    // Send notofication to client.
    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: clientUserDoc._id,
      notification_type: ClientNotificationType.reminderBeforSevenDayForClientAssignedQty,
      order_id: orderData._id,
      //order_item_id: OrderItemData.display_item_id,
     // vendor_id,
    })

    let message =  `Hi ${client_name}, this is a reminder for you to kindly assign the RMC Qty for an order id ${orderData._id}.`
    // CP assigned.
    let templateId = "1707163773826406582"
    await this.testService.sendSMS(client_mobile_number, message, templateId)
  }

    if (notification_sent_type === "OneDayBefore") {
         // Send notoficorder_item_idation to client.
         await this.notUtil.addNotificationForBuyer({
          to_user_type: UserType.BUYER,
          to_user_id: clientUserDoc._id,
          notification_type: ClientNotificationType.reminderBeforOneDayForClientAssignedQty,
          order_id: orderData._id,
          // order_item_id: OrderItemData.display_item_id,
          // vendor_id,
        })

        //let message =  `Hi ${client_name}, this is a final reminder for you to kindly assign the RMC Qty for an order id ${orderData._id}.`
        let message =  `Hi ${client_name}, this is a final reminder for you to kindly assign the RMC Qty for an order id ${orderData._id}. If you fail to assign the qty now then your order will be lapsed as per Buyer Purchase Policy.`
        // CP assigned.
        let templateId = "1707163773848102733"
        await this.testService.sendSMS(client_mobile_number, message, templateId)
    }
    // notification to client for appointement reminder.
    // await this.notUtil.addNotificationForClient({
    //   to_user_type: UserType.CLIENT,
    //   to_user_id: appointmentData.client_id,
    //   notification_type,
    //   data: {
    //     name: clientUserDoc.full_name,
    //     appointment_id: appointmentData._id,
    //     date: appointmentData.date,
    //     booked_on: appointmentData.booked_on,
    //   },
    // })

    // let branchData = await BranchModel.findById(appointmentData.branch_id)
    // if (isNullOrUndefined(branchData)) {
    //   return Promise.reject(new InvalidInput(`Branch does not exist`, 400))
    // }

    // let serviceInfo = await ServiceModel.findById(appointmentData.service_id)
    // if (isNullOrUndefined(serviceInfo)) {
    //   return Promise.reject(new InvalidInput(`Service does not exist`, 400))
    // }

    // let stylistInfo = await StylistModel.findById(appointmentData.stylist_id)
    // if (isNullOrUndefined(stylistInfo)) {
    //   return Promise.reject(new InvalidInput(`Stylist does not exist`, 400))
    // }

    // await this.notUtil.addNotificationForClient({
    //   to_user_type: UserType.CLIENT,
    //   to_user_id: appointmentData.client_id,
    //   notification_type,
    //   name: clientUserDoc.full_name,
    //   appointment_id: appointmentData._id,
    //   date: appointmentData.date,
    //   booked_on: appointmentData.booked_on,
    //   service_name: serviceInfo.name,
    //   branch_name: branchData.name,
    //   stylist_name: stylistInfo.full_name
    // })

    // // send sms to client for appointement reminder.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_SMS_USING_TEMPLATE,
    //   event_data: {
    //     to: clientUserDoc.mobile_number,
    //     template_type,
    //     templateData: {
    //       name: clientUserDoc.full_name,
    //       date: appointmentData.date,
    //       booked_on: appointmentData.booked_on,
    //       appointment_id: appointmentData._id,
    //     },
    //   },
    // })

    // // send email to client for appointement reminder.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_MAIL_USING_TEMPLATE,
    //   event_data: {
    //     template_name: email_template_type,
    //     templateData: {
    //       subject: "Appointement Reminder.",
    //       html: {
    //         name: clientUserDoc.full_name,
    //         date: appointmentData.date,
    //         booked_on: appointmentData.booked_on,
    //         appointment_id: appointmentData._id,
    //       },
    //     },
    //     receiverInfo: {
    //       full_name: clientUserDoc.full_name,
    //       email: clientUserDoc.email,
    //     },
    //   },
    // })

    // update appointment doc.
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    if (notification_sent_type === "OneDayBefore") {
      updt.$set.one_day_notification_sent = true
    } else {
      updt.$set.seven_day_notification_sent = true
    }
    // update vendor order doc.
    await OrderModel.updateOne({ _id: new ObjectId(orderData._id) }, updt)
    return Promise.resolve()
  }

  async orderLapsed() {
    const query: { [k: string]: any } = {}
    query.created_at = { $lt: new Date() }
    query.order_status = { $ne: OrderStatus.LAPSED }

    const orderData = await OrderModel.find(query)
    if (orderData.length < 1) {
      return Promise.reject(new InvalidInput(`Order does not exist..`, 400))
    }

    for (let i = 0; i < orderData.length; i++) {
      let cnt = 0
      let siteInfo = await SiteModel.findOne({
        _id: new ObjectId(orderData[i].site_id)
      })
     // let site_name: any
      let site_mobile_number: any
      let site_person_name: any
      if(!isNullOrUndefined(siteInfo)){
       // site_name = siteInfo.site_name
        site_mobile_number = siteInfo.mobile_number
        site_person_name = siteInfo.person_name
      }
      let client_name: any
      let mobile_number: any
      let clientInfo = await BuyerUserModel.findOne({
        _id: new ObjectId(orderData[i].user_id)
      })
     
    if (!isNullOrUndefined(clientInfo)) {
      mobile_number = clientInfo.mobile_number
      if (clientInfo.account_type === "Individual") {
        client_name = clientInfo.full_name
      } else {
        client_name = clientInfo.company_name
      }

      const orderItemData = await OrderItemModel.find({
        order_id : new ObjectId(orderData[i]._id)
      })
      if(orderItemData.length> 0 ){
        for (let j = 0; j < orderItemData.length; j++) {
          let check = await OrderItemPartModel.findOne({
            buyer_order_id: new ObjectId(orderData[i]._id),
            order_item_id: new ObjectId(orderItemData[j]._id)
          })
          if (isNullOrUndefined(check)) {
            cnt++
            await OrderItemModel.findOneAndUpdate(
              {
                _id: new ObjectId(orderItemData[j]._id),
                order_id: orderData[i]._id,
              },
              {
                $set: {
                  item_status: OrderItemStatus.LAPSED,
                },
              }
            )
            //let message = `Hi ${client_name}, since you have failed to assign the qty for your order with Product having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. - conmix`
            let message = `Hi ${client_name}, since you have failed to assign the qty for your order with Product Id ${orderItemData[j]._id} having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. -conmix`
            let templateId = "1707163782156874423"
            await this.testService.sendSMS(mobile_number, message, templateId)
          //  let site_message = `Hi ${site_person_name}, since you have failed to assign the qty for your order with Product having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. - conmix`
            let site_message = `Hi ${site_person_name}, since you have failed to assign the qty for your order with Product Id ${orderItemData[j]._id} having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. -conmix`
            let site_templateId = "1707163782156874423"
            await this.testService.sendSMS(site_mobile_number, site_message, site_templateId)
            await this.notUtil.addNotificationForBuyer({
              to_user_type: UserType.BUYER,
              to_user_id: clientInfo._id,
              notification_type: ClientNotificationType.orderItemLapsed,
              order_id: orderData[i]._id,
              order_item_id: orderItemData[j]._id,
              // vendor_id,
            })

           


            if(!isNullOrUndefined(orderItemData[j].vendor_id)){
              let vendorData = await VendorUserModel.findOne({_id: new ObjectId(orderItemData[j].vendor_id)})

              if(!isNullOrUndefined(vendorData)){
                //let vendor_name = vendorData.full_name
                let vendor_mobile_number = vendorData.mobile_number
                let vendor_company_name = vendorData.company_name
               // let vendor_message = `Hi ${vendor_name}, your client ${client_name} has failed to assign the qty for the Product Id ${orderItemData[j]._id} having an Order ID ${orderData[i]._id}. - conmix`
                let vendor_message = `Hi ${vendor_company_name}, your order with product id ${orderItemData[j]._id} has been lapsed as your client ${client_name} has failed to assign its qty. - conmix`
                let vendor_templateId = "1707163842568007508"
                await this.testService.sendSMS(vendor_mobile_number, vendor_message, vendor_templateId) 
              

              await this.notUtil.addNotificationForSupplier({
                to_user_type: UserType.VENDOR,
                to_user_id: vendorData._id,
                notification_type:  VendorNotificationType.orderItemLapsed,
                order_id: orderData[i]._id,
                order_item_id: orderItemData[j]._id,
                client_id: orderData[i].user_id
                // vendor_id,
              })
            }
          }
          }
        }
        // full order lapsed
if(orderItemData.length === cnt){
  await OrderModel.findOneAndUpdate(
    {
      _id: new ObjectId(orderData[i]._id)
    },
    {
      $set: {
        order_status: OrderStatus.LAPSED,
      },
    }
  )

    //  let message = `Hi ${client_name}, since you have failed to assign the qty for your order having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. -conmix`
    //  let templateId = "1707163767375740885"
    //  await this.testService.sendSMS(mobile_number, message, templateId)

    //  let site_message = `Hi ${site_person_name}, since you have failed to assign the qty for your order having an Order ID ${orderData[i]._id}, the order has been lapsed. Kindly refer to our Purchase Policy. -conmix`
    //  let site_templateId = "1707163767375740885"
    //  await this.testService.sendSMS(site_mobile_number, site_message, site_templateId)

     let message = `Hi ${client_name}, since you have failed to assign the qty for your entire order having an order id ${orderData[i]._id}, has been lapsed. Kindly refer to our Buyer Purchase Policy. - conmix`
     let templateId = "1707163843600256683"
     await this.testService.sendSMS(mobile_number, message, templateId)

     let site_message = `Hi ${site_person_name}, since you have failed to assign the qty for your entire order having an order id ${orderData[i]._id}, has been lapsed. Kindly refer to our Buyer Purchase Policy. - conmix`
     let site_templateId = "1707163843600256683"
     await this.testService.sendSMS(site_mobile_number, site_message, site_templateId)

  await this.notUtil.addNotificationForBuyer({
    to_user_type: UserType.BUYER,
    to_user_id: clientInfo._id,
    notification_type: ClientNotificationType.orderLapsed,
    order_id: orderData[i]._id,
    //order_item_id: orderItemData[j]._id,
    // vendor_id,
  })

}
      }
    }
      }
    return Promise.resolve()
  }

  async getIdelClient(){
    let clientData = await BuyerUserModel.find()
    if(clientData.length> 0){
      for(let i = 0; i< clientData.length; i ++){
        let orderData = await OrderModel.find({
          user_id: new ObjectId(clientData[i]._id)
        }).sort({created_at: -1})
        if(orderData.length > 0){
        for(let j = 0; j< orderData.length; j++){
          let current_date = new Date()
          let date = current_date.setMonth(current_date.getMonth() - 3);
          if(orderData[j].created_at.getTime() < date){

            let clientDoc = await BuyerUserModel.findOne({
              _id: new ObjectId(clientData[i]._id)
            })
            let mobile_number: any
            let client_name: any
            let email: any
            if (!isNullOrUndefined(clientDoc)) {
              mobile_number = clientDoc.mobile_number
              email = clientDoc.email
              if (clientDoc.account_type === "Individual") {
                client_name = clientDoc.full_name
              } else {
                client_name = clientDoc.company_name
              }
            let message = `Hi ${client_name} just a reminder to login Conmix for your RMC requirements. - conmix`
            let templateId = "1707163887986113275"
            await this.testService.sendSMS(mobile_number, message, templateId)

            let ideal_client_html = client_mail_template.ideal_client
            ideal_client_html = ideal_client_html.replace("{{client_name}}", client_name)
            var data11 = {
            from: "no-reply@conmate.in",
            to: email,
            subject: "Conmix - Idle user",
            html: ideal_client_html,
          }
          this.mailgunService.sendEmail(data11)
          }
        }
      }
      }
    }
  }
  return Promise.resolve()
}

async getIdelVendor(){
  let vendorData = await VendorUserModel.find()
  if(vendorData.length> 0){
    for(let i = 0; i< vendorData.length; i++){
      let orderData = await VendorOrderModel.find({
        user_id: new ObjectId(vendorData[i]._id)
      }).sort({created_at: -1})
      if(orderData.length> 0){
        for(let j = 0; j< orderData.length; j++){
          let current_date = new Date()
          let date = current_date.setMonth(current_date.getMonth() - 2);
          if(orderData[j].created_at.getTime() < date && 
          orderData[j].order_status !== SupplierOrderStatus.ACCEPTED){

            let vendorDoc = await VendorUserModel.findOne({
              _id: new ObjectId(vendorData[i]._id)
            })
            let mobile_number: any
            if (!isNullOrUndefined(vendorDoc)) {
              mobile_number = vendorDoc.mobile_number
            let message = `Hi ${vendorDoc.full_name} just a reminder to login Conmix to accept RMC orders for more business !! - conmix`
            let templateId = "1707163888087088635"
            await this.testService.sendSMS(mobile_number, message, templateId)

            let ideal_vendor_html = vendor_mail_template.ideal_vendor
            ideal_vendor_html = ideal_vendor_html.replace("{{vendorDoc.full_name}}", vendorDoc.full_name)
              var data11 = {
              from: "no-reply@conmate.in",
              to: vendorDoc.email,
              subject: "Conmix - Idle user",
              html: ideal_vendor_html,
            }
            this.mailgunService.sendEmail(data11)
          }
         
        }
      }
    }
  }
}
return Promise.resolve()
}

async verifyVendor(){
  const query: { [k: string]: any } = {}
  let gt = new Date().getTime() + 5 * 24 * 60 * 60 * 1000
  query.created_at = {
    $gt: gt,
  }

  let vendorData = await VendorUserModel.find(query)
  for(let i = 0; i< vendorData.length ; i ++){
 
   // Check Admin Type.
   const adminQuery: { [k: string]: any } = {}
   adminQuery.$or = [
     {
       admin_type : AdminRoles.superAdmin
     },
     {
       admin_type : AdminRoles.admin_manager
     },
     {
       admin_type : AdminRoles.admin_customer_care
     }
   ]
   const adminUserDocs = await AdminUserModel.find(adminQuery)
   if (adminUserDocs.length < 0) {
     return Promise.reject(
       new InvalidInput(`No Admin data were found `, 400)
     )
   }
   for(let i = 0 ; i< adminUserDocs.length; i ++){ 
    await this.notUtil.addNotificationForAdmin({
      to_user_type: UserType.ADMIN,
      to_user_id: adminUserDocs[i]._id,
      notification_type: AdminNotificationType.reminderForVendorVerification,
      vendor_id: vendorData[i]._id,
    })
   }
  }
  return Promise.resolve()
}
}