import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { Request } from "express"
export interface IProcessOrderReq extends Request {
  query: {
    order_id: string
  }
}

export const processOrderReq: IRequestSchema = {
  query: Joi.object().keys({
    order_id: Joi.string()
      .regex(objectIdRegex)
      .required(),
  }),
}
