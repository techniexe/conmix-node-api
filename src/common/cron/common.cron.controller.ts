import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { Response, Request, NextFunction } from "express"
import { validate } from "../../middleware/joi.middleware"
import { processOrderReq, IProcessOrderReq } from "./common.order.req.schema"
import { CommonTypes } from "../common.types"
import { CommonOrderRepositroy } from "./common.order.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
//import { CommonQuoteRepository } from "./common.quote.repository"
@injectable()
@controller("/v1/cron")
export class CommonCronController {
  constructor(
    @inject(CommonTypes.CommonOrderRepositroy)
    private orderRepo: CommonOrderRepositroy // @inject(CommonTypes.CommonQuoteRepository) // private quoteRepo: CommonQuoteRepository
  ) {}
  @httpGet("/order/process", validate(processOrderReq))
  async processOrder(req: IProcessOrderReq, res: Response) {
    await this.orderRepo.processOrder(req.query.order_id)
    res.json({ data: "success" })
  }

  @httpGet("/vendor_order/process")
  async processVendorOrder(req: Request, res: Response) {
    await this.orderRepo.processVendorOrder()
    res.json({ status: "success" })
  }

  @httpGet("/surprise_order/process")
  async processSurpriseOrder(req: Request, res: Response) {
    await this.orderRepo.processSurpriseOrder()
    res.json({ status: "success" })
  }

  @httpGet("/checkStock")
  async checkProductStock(req: Request, res: Response){
    await this.orderRepo.checkProductStock()
    res.json({ status: "success" })
  }

  @httpGet("/notifiedForQuantityAssigned/:notification_sent_type")
  async notifiedScheduledAppointment(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { notification_sent_type } = req.params
      await this.orderRepo.notifiedScheduledAppointment(notification_sent_type)
      res.send()
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/orderLapsed")
  async orderLapsed(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.orderLapsed()
      res.send()
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/IdleClient")
  async getIdelClient(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.getIdelClient()
      res.send()
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/IdleVendor")
  async getIdelVendor(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.getIdelVendor()
      res.send()
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/VerifyVendor")
  async verifyVendor(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.verifyVendor()
      res.send()
    } catch (err) {
      next(err)
    }
  }

}
//   @httpGet("/quote/process")
//   async processQuote(req: Request, res: Response) {
//     await this.quoteRepo.processQuote()
//     res.json({ status: "success" })
//   }
// }
