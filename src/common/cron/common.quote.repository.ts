// import { OrderModel } from "./../../model/order.model"
// import {
//   LogisticsOrderStatus,
//   PaymentStatus,
// } from "./../../model/logistics.order.model"
// import {
//   OrderItemModel,
//   OrderItemQuoteStatus,
// } from "./../../model/order_item.model"
// import { isNullOrUndefined } from "util"
// import { injectable, inject } from "inversify"

// import {
//   // PaymentStatus,
//   LogisticsOrderModel,
// } from "../../model/logistics.order.model"
// // import { quoteProcessingIntervel } from "../../utilities/config"
// import { QuoteModel } from "../../model/quote.model"
// import { CommonTypes } from "../common.types"
// import { UniqueidService } from "../../utilities/uniqueid-service"
// import { InvalidInput } from "../../utilities/customError"
// import { ObjectId } from "bson"
// import { OrderItemStatus } from "../../utilities/config"
// //const quoteProcessCheck = 1000 // quoteProcessingIntervel * 1000 * 60 * 60

// @injectable()
// export class CommonQuoteRepository {
//   constructor(
//     @inject(CommonTypes.UniqueidService)
//     private uniqueIdService: UniqueidService
//   ) {}
//   // async processQuote() {
//   //   const orderCursor = OrderModel.find({
//   //     order_quote_status: OrderQuoteStatus.NOTPLACED,
//   //     order_status: OrderStatus.PROCESSED,
//   //     payment_status: PaymentStatus.PAID,
//   //     created_at: { $lt: new Date(Date.now() - quoteProcessCheck) },
//   //     // order created at checking here
//   //   })
//   //     .limit(50)
//   //     .cursor() // currently 50 order per call
//   //   orderCursor
//   //     .on("data", async (orderDoc: IOrderDoc) => {
//   //       orderCursor.pause()
//   //       const selectedQuotes = await QuoteModel.aggregate([
//   //         {
//   //           $match: {
//   //             order_id: orderDoc._id,
//   //             quoted_amount: { $exists: true },
//   //           },
//   //         },
//   //         {
//   //           $sort: {
//   //             quoted_amount: 1,
//   //           },
//   //         },
//   //         {
//   //           $group: {
//   //             _id: "$quote_group_id",
//   //             selected_id: { $first: "$_id" },
//   //             quote_display_id: { $first: "$quote_display_id" },
//   //             quoted_amount: { $first: "$quoted_amount" },
//   //           },
//   //         },
//   //       ])
//   //       console.log(`selectedQuotes:`, selectedQuotes)
//   //       await Promise.all(
//   //         selectedQuotes.map(async (qt) => {
//   //           const order_item_info = await OrderItemModel.findOne({
//   //             _id: qt.order_item_id,
//   //           })

//   //           if (isNullOrUndefined(order_item_info)) {
//   //             return Promise.reject(new InvalidInput(`Invalid product id`, 400))
//   //           }
//   //           console.log("qt", qt)
//   //           return new LogisticsOrderModel({
//   //             _id: await this.uniqueIdService.getUniqueLogisticsOrderId(),
//   //             logistics_user_id: qt.logistics_user_id,
//   //             buyer_id: qt.buyer_user_id,
//   //             supplier_id: qt.supplier_user_id,
//   //             product_id: qt.product_id,
//   //             buyer_order_id: qt.order_id,
//   //             buyer_order_item_id: qt.order_item_id,
//   //             vehicle_id: qt.vehicle_id,
//   //             // vehicle_category_id: Mongoose.Types.ObjectId
//   //             vehicle_sub_category_id: qt.vehicle_sub_category_id,
//   //             delivery_status: LogisticsOrderStatus.INPROGRESS,
//   //             quantity: qt.quantity,
//   //             // pickup_quantity?: number
//   //             // royality_quantity?: number
//   //             // delivery_quantity?: number
//   //             igst_rate: order_item_info.igst_rate,
//   //             cgst_rate: order_item_info.cgst_rate,
//   //             sgst_rate: order_item_info.sgst_rate,
//   //             igst_amount: order_item_info.igst_amount,
//   //             cgst_amount: order_item_info.cgst_amount,
//   //             sgst_amount: order_item_info.sgst_amount,
//   //             // total_amount: number
//   //             // created_at: Date
//   //             // bill_url?: string
//   //             // bill_uploaded_at?: Date
//   //             // bill_status?: BillStatus // verified/unverified
//   //             // admin_remarks?: string
//   //             // bill_verified_at?: Date
//   //             // logistics_payment_status: PaymentStatus
//   //             // supplier_pickup_signature?: string
//   //             // driver_pickup_signature?: string
//   //             // buyer_drop_signature?: string
//   //             // driver_drop_signature?: string
//   //             // checkedout_at?: Date
//   //             // pickedup_at?: Date
//   //             // delivered_at?: Date
//   //             // reasonForDelay: string
//   //             // delayTime: string
//   //           }).save()
//   //         })
//   //       )
//   //       orderCursor.resume()
//   //     })
//   //     .on("end", () => {
//   //       console.log(`Done all quote processing`)
//   //     })
//   // }

//   async processQuote() {
//     const orderItemCursor = await OrderItemModel.find({
//       item_quote_status: OrderItemQuoteStatus.UNDER_BIDDING,
//       item_status: OrderItemStatus.PROCESSING,
//       payment_status: PaymentStatus.PAID,
//       // created_at: { $lt: new Date(Date.now() - quoteProcessCheck) },
//       //  order created at checking here
//     })

//     const logisticsOrderData: any[] = []
//     await Promise.all(
//       orderItemCursor.map(async (oItem) => {
//         const selectedQuotes = await QuoteModel.aggregate([
//           {
//             $match: {
//               order_item_id: oItem._id,
//               quoted_amount: { $exists: true },
//             },
//           },
//           {
//             $sort: {
//               quoted_amount: 1,
//             },
//           },
//           { $limit: 1 },
//         ])
//         console.log(`selectedQuotes:`, selectedQuotes)
//         for (let i = 0; i < selectedQuotes.length; i++) {
//           const order_item_info = await OrderItemModel.findOne({
//             _id: new ObjectId(selectedQuotes[i].order_item_id),
//           })

//           if (isNullOrUndefined(order_item_info)) {
//             return Promise.reject(
//               new InvalidInput(`Invalid Order Item id`, 400)
//             )
//           }
//           logisticsOrderData.push({
//             _id: await this.uniqueIdService.getUniqueLogisticsOrderId(),
//             user_id: selectedQuotes[i].logistics_user_id,
//             buyer_id: selectedQuotes[i].buyer_user_id,
//             supplier_id: selectedQuotes[i].supplier_user_id,
//             product_id: selectedQuotes[i].product_id,
//             buyer_order_id: selectedQuotes[i].order_id,
//             buyer_order_item_id: selectedQuotes[i].order_item_id,
//             vehicle_id: selectedQuotes[i].vehicle_id,
//             // vehicle_category_id: Mongoose.Types.ObjectId
//             vehicle_sub_category_id: selectedQuotes[i].vehicle_sub_category_id,
//             delivery_status: LogisticsOrderStatus.RECEIVED,
//             quantity: selectedQuotes[i].quantity,
//             // pickup_quantity?: number
//             // royality_quantity?: number
//             // delivery_quantity?: number
//             igst_rate: order_item_info.igst_rate,
//             cgst_rate: order_item_info.cgst_rate,
//             sgst_rate: order_item_info.sgst_rate,
//             igst_amount: order_item_info.igst_amount,
//             cgst_amount: order_item_info.cgst_amount,
//             sgst_amount: order_item_info.sgst_amount,
//             total_amount: order_item_info.total_amount,
//             quote_id: selectedQuotes[i]._id,
//             quote_display_id: selectedQuotes[i].quote_display_id,
//             // total_amount: number
//             // created_at: Date
//             // bill_url?: string
//             // bill_uploaded_at?: Date
//             // bill_status?: BillStatus // verified/unverified
//             // admin_remarks?: string
//             // bill_verified_at?: Date
//             // logistics_payment_status: PaymentStatus
//             // supplier_pickup_signature?: string
//             // driver_pickup_signature?: string
//             // buyer_drop_signature?: string
//             // driver_drop_signature?: string
//             // checkedout_at?: Date
//             // pickedup_at?: Date
//             // delivered_at?: Date
//             // reasonForDelay: string
//             // delayTime: string
//           })

//           await QuoteModel.updateOne(
//             { _id: selectedQuotes[i]._id },
//             {
//               $set: {
//                 status: OrderItemQuoteStatus.ASSIGNED,
//               },
//             }
//           )

//           //** Here update order status as "CONFIRMED" */
//           await OrderModel.updateOne(
//             { _id: selectedQuotes[i].order_id },
//             {
//               $set: {
//                 order_quote_status: OrderItemQuoteStatus.ASSIGNED,
//               },
//             }
//           )

//           //** Here update order item status as "CONFIRMED" */
//           await OrderItemModel.updateOne(
//             { _id: selectedQuotes[i].order_item_id },
//             {
//               $set: {
//                 item_quote_status: OrderItemQuoteStatus.ASSIGNED,
//                 item_status: OrderItemStatus.CONFIRMED,
//               },
//             }
//           )
//         }

//         // Send sms/email to client for confirmation on order.
//         return Promise.resolve()
//       })
//     )
//     try {
//       await LogisticsOrderModel.create(logisticsOrderData)
//       console.log(
//         `Logistics Order created. Total logisctics order: ${logisticsOrderData.length}`
//       )
//     } catch (err) {
//       console.log(`[ERROR] Unable to logisctics order. ${err.message}`)
//       return Promise.reject(
//         new InvalidInput(`Unable to logisctics order. ${err.message}`, 400)
//       )
//     }

//     return Promise.resolve()
//   }
// }
