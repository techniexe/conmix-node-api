export const CommonTypes = {
  CommonService: Symbol("CommonService"),
  TwilioService: Symbol("TwilioService"),
  mailgunService: Symbol("mailgunService"),
  TestService: Symbol("TestService"),
  NotificationUtility: Symbol("NotificationUtility"),
  UniqueidService: Symbol("UniqueidService"),
  SessionController: "SessionController",
  SessionRepository: Symbol("SessionRepository"),
  RegionController: "RegionController",
  RegionRepository: Symbol("RegionRepository"),

  ProductReviewController: "ProductReviewController",
  ProductReviewRepository: Symbol("ProductReviewRepository"),
  CommonProductController: "CommonProductController",
  CommonProductRepository: Symbol("CommonProductRepository"),
  EmailVerifyController: "EmailVerifyController",
  EmailVerifyRepository: Symbol("EmailVerifyRepository"),
  RfpController: "RfpController",
  RfpRepository: Symbol("RfpRepository"),
  CommonCronController: "CommonCronController",
  CommonOrderRepositroy: Symbol("CommonOrderRepositroy"),
  // CommonQuoteRepository: Symbol("CommonQuoteRepository"),
  SourceController: "SourceController",
  SourceRepository: Symbol("SourceRepository"),
  CommonConcretePumpController: "CommonConcretePumpController",
  CommonConcretePumpRepository: Symbol("CommonConcretePumpRepository"),
  CommonTMController: "CommonTMController",
  CommonTMRepository: Symbol("CommonTMRepository"),
  ConcreteGradeController: "ConcreteGradeController",
  ConcreteGradeRepository: Symbol("ConcreteGradeRepository"),
  CementBrandController: "CementBrandController",
  CementBrandRepository: Symbol("CementBrandRepository"),
  CommonAggregateSandCategoryController:
    "CommonAggregateSandCategoryController",
  CommonAggregateSandCategoryRepository: Symbol(
    "CommonAggregateSandCategoryRepository"
  ),
  AdmixBrandController: "AdmixBrandController",
  AdmixBrandRepository: Symbol("AdmixBrandRepository"),
  AdmixCategoryController: "AdmixCategoryController",
  AdmixCategoryRepository: Symbol("AdmixCategoryRepository"),
  CementGradeController: "CementGradeController",
  CementGradeRepository: Symbol("CementGradeRepository"),
  DesignMixController: "DesignMixController",
  DesignMixRepository: Symbol("DesignMixRepository"),
  CustomMixController: "CustomMixController",
  CustomMixRepository: Symbol("CustomMixRepository"),
  PayMethodController: "PayMethodController",
  PayMethodRepository: Symbol("PayMethodRepository"),
  ReviewController: "ReviewController",
  ReviewRepository: Symbol("ReviewRepository"),
  PnfController: "PnfController",
}
