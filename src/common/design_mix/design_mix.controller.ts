import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"

import { NextFunction, Response } from "express"
import { DesignMixRepository } from "./design_mix.repository"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  IGetDesignMixListRequest,
  IGetDesignMixDetailsRequest,
  GetDesignMixDetailsSchema,
} from "./design_mix.req-schema"
import { validate } from "../../middleware/joi.middleware"
import { verifyOptionalToken } from "../../middleware/auth-token.middleware"

@controller("/v1/design_mix", verifyOptionalToken)
@injectable()
export class DesignMixController {
  constructor(
    @inject(CommonTypes.DesignMixRepository)
    private designMixRepo: DesignMixRepository
  ) {}

  @httpGet("/")
  async getDesignMixList(
    req: IGetDesignMixListRequest,
    res: Response,
    next: NextFunction
  ) {
    let uid: string | undefined
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }
    console.log("req.user", req.user)
    const data = await this.designMixRepo.getDesignMixList(req.query, uid)
    res.json({ data })
  }

  @httpGet("/:design_mix_id", validate(GetDesignMixDetailsSchema))
  async getDesignMixDetails(
    req: IGetDesignMixDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    let uid: string | undefined
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    const data = await this.designMixRepo.getDesignMixDetails(
      req.params.design_mix_id,
      req.query,
      uid
    )
    res.json({ data })
  }
}
