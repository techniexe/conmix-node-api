import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IGetDesignMixList {
  site_id?: string
  long?: number
  lat?: number
  grade_id?: string
  before?: string
  after?: string
  order_by?: string
  page_num?: number
  delivery_date: Date
  end_date: Date
  quantity: number
}

export interface IGetDesignMixListRequest extends IAuthenticatedRequest {
  query: IGetDesignMixList
}

export const GetDesignMixListSchema: IRequestSchema = {
  query: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    grade_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    order_by: Joi.string(), //comma separated fields. ex- selling_price
    page_num: Joi.number(),
    delivery_date: Joi.date(),
    end_date: Joi.date(),
    quantity: Joi.number(),
  }),
}

export interface IGetDesignMixDetails {
  site_id?: string
  long?: number
  lat?: number
  delivery_date: Date
  with_CP: boolean
  with_TM: boolean
  end_date: Date
  quantity: number
}

export interface IGetDesignMixDetailsRequest extends IAuthenticatedRequest {
  query: IGetDesignMixDetails
  params: {
    design_mix_id: string
  }
}

export const GetDesignMixDetailsSchema: IRequestSchema = {
  query: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    delivery_date: Joi.date().required(),
    with_CP: Joi.boolean(),
    with_TM: Joi.boolean(),
    end_date: Joi.date(),
    quantity: Joi.number(),
  }),
  params: Joi.object().keys({
    design_mix_id: Joi.string().regex(objectIdRegex),
  }),
}
