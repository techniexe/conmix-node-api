//import { VendorAdMixtureSettingModel } from "./../../model/vendor_ad_mix_setting.model"
//import { VendorOrderModel } from "./../../model/vendor.order.model"
import { VendorUserModel } from "./../../model/vendor.user.model"
import { injectable, inject } from "inversify"
import {
  IGetDesignMixList,
  IGetDesignMixDetails,
} from "./design_mix.req-schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { SiteModel } from "../../model/site.model"
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { deliveryRange, message } from "../../utilities/config"
import { VendorSettingModel } from "../../model/vendor.setting.model"
import { CommonTypes } from "../common.types"
import { CommonService } from "../../utilities/common.service"
import { AddressModel } from "../../model/address.model"
import { distanceFactor, getDistance } from "../../utilities/distance.utilities"
import { VendorAdMixtureSettingModel } from "../../model/vendor_ad_mix_setting.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { FlyAshSourceQuantityModel } from "../../model/fly_ash_source_quantity.model"

@injectable()
export class DesignMixRepository {
  constructor(
    @inject(CommonTypes.CommonService) private commonService: CommonService
  ) {}
  async getDesignMixList(query: IGetDesignMixList, user_id?: string) {
    const limit = 10
    let skip = 0
    let page_num = query.page_num
    if (!isNullOrUndefined(page_num)) {
      if (page_num > 50) {
        return []
      }
      skip = page_num > 0 ? Math.floor(page_num - 1) * limit : 0
    }

    const arrSortFld = ["selling_price"]
    const sort: { [k: string]: any } = {}
    //const requesterUid = new ObjectId(user_id)
    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (
      isNullOrUndefined(query.site_id) &&
      (isNullOrUndefined(query.long) || isNullOrUndefined(query.lat))
    ) {
      return Promise.reject(
        new InvalidInput(`either Site id or location is required`, 400)
      )
    }
    if (!isNullOrUndefined(query.site_id)) {
      //getting city id
      // const siteQuery = { _id: query.site_id, user_id }

      const siteQuery = { _id: query.site_id }
      const siteData = await SiteModel.findOne(siteQuery)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      dvlLocation = siteData.location
    }
    if (!isNullOrUndefined(query.long) && !isNullOrUndefined(query.lat)) {
      dvlLocation.coordinates = [Number(query.long), Number(query.lat)]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, location reference could not be found or no longer exists `,
          400
        )
      )
    }
    let query1: { [k: string]: any } = {
      //  grade_id: new ObjectId(query.grade_id),
      is_available: true,
    }

    if (!isNullOrUndefined(query.grade_id)) {
      query1.grade_id = new ObjectId(query.grade_id)
    }
    if (!isNullOrUndefined(query.before) && query.before !== "") {
      query1.created_at = { $lt: query.before }
    }
    if (!isNullOrUndefined(query.after) && query.after !== "") {
      query1.created_at = { $gt: query.after }
    }
    if (!isNullOrUndefined(query.order_by)) {
      query.order_by.split(",").reduce((obj, str) => {
        const arrSplt = str.split(":")
        if (arrSortFld.indexOf(arrSplt[0]) < 0) {
          return obj
        }
        return Object.assign(obj, {
          [arrSplt[0]]: arrSplt[1] === "desc" ? -1 : 1,
        })
      }, sort)
    }

    console.log("query1", query1)
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRange.max * 1000, // km in meter
          spherical: true,
          query: query1,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "aggregate_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_category_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_category_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "ad_mixture_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "ad_mixture_category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_media",
          localField: "vendor_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },

      {
        $project: {
          _id: 1,
          design_mix_id: 1,
          product_name: 1,
          cement_quantity: 1,
          sand_quantity: 1,
          aggregate1_quantity: 1,
          aggregate2_quantity: 1,
          fly_ash_quantity: 1,
          ad_mixture_quantity: 1,
          water_quantity: 1,
          selling_price: 1,
          is_available: 1,
          description: 1,
          is_custom: 1,
          updated_at: 1,
          updated_by_id: 1,
          calculated_distance: 1,
          vendor_id: 1,
          sub_vendor_id: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.rating": 1,
          "vendor.company_name": 1,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sand_category._id": 1,
          "aggregate1_sand_category.sub_category_name": 1,
          "aggregate2_sand_category._id": 1,
          "aggregate2_sand_category.sub_category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          vendor_media: 1,
        },
      },
    ]

    if (Object.keys(sort).length > 0) {
      aggregateArr.push({ $sort: sort })
    }

    aggregateArr.push({ $skip: skip })
    aggregateArr.push({ $limit: limit })

    let products = await DesignMixVariantModel.aggregate(aggregateArr)

    console.log("products", products)

    for (let i = 0; i < products.length; i++) {
      if (!isNullOrUndefined(products[i].master_vendor_id)) {
        const mastervendoruser = await VendorUserModel.findOne({
          _id: new ObjectId(products[i].master_vendor_id),
        })

        console.log("mastervendoruser", mastervendoruser)
        if (!isNullOrUndefined(mastervendoruser)) {
          if (mastervendoruser.verified_by_admin === false) {
            delete products[i]
            continue
          }

          if (mastervendoruser.is_blocked === true) {
            delete products[i]
            continue
          }
        }
      }
      if (!isNullOrUndefined(products[i].sub_vendor_id)) {
        const subvendoruser = await VendorUserModel.findOne({
          _id: new ObjectId(products[i].sub_vendor_id),
        })

        console.log("subvendoruser", subvendoruser)
        if (!isNullOrUndefined(subvendoruser)) {
          if (subvendoruser.verified_by_admin === false) {
            delete products[i]
            continue
          }

          if (subvendoruser.is_blocked === true) {
            delete products[i]
            continue
          }
        }
      }
      if (!isNullOrUndefined(products[i].vendor_id)) {
        const vendoruser = await VendorUserModel.findOne({
          _id: new ObjectId(products[i].vendor_id),
        })

        console.log("vendoruser", vendoruser)
        if (!isNullOrUndefined(vendoruser)) {
          if (vendoruser.verified_by_admin === false) {
            delete products[i]
            continue
          }

          if (vendoruser.is_blocked === true) {
            delete products[i]
            continue
          }
        }
      }
      // // If it is sub vendor, then check its master vendor is verified or not.
      // if (!isNullOrUndefined(vendoruser.master_vendor_id)) {
      //   const mastervendoruser = await VendorUserModel.findOne({
      //     _id: new ObjectId(vendoruser.master_vendor_id),
      //   })
      //   console.log("mastervendoruser", mastervendoruser)
      //   if (!isNullOrUndefined(mastervendoruser)) {
      //     if (
      //       vendoruser.verified_by_admin === false ||
      //       mastervendoruser.verified_by_admin === false
      //     ) {
      //       console.log("unverifiedd")
      //       console.log("products[i]")
      //       delete products[i]
      //       //break
      //       continue
      //     }
      //   }
      // }
      // If it is master vendor, then check for all it's sub vendor, is verified or not.
      // else if (isNullOrUndefined(vendoruser.master_vendor_id)) {
      //   const subvendorusers = await VendorUserModel.find({
      //     master_vendor_id: new ObjectId(vendoruser._id),
      //   })
      //   console.log("subvendorusers", subvendorusers)
      //   if (subvendorusers.length > 0) {
      //     for (let i = 0; i < subvendorusers.length; i++) {
      //       if (
      //         vendoruser.verified_by_admin === false ||
      //         subvendorusers[i].verified_by_admin === false
      //       ) {
      //         console.log("unverifiedd")
      //         console.log("products[i]")
      //         delete products[i]
      //         break
      //         // continue
      //       }
      //     }
      //   }
      // } else {
      //   if (vendoruser.verified_by_admin === false) {
      //     console.log("unverifieddddd")
      //     console.log("products[i]")
      //     delete products[i]
      //     // break
      //     continue
      //   }
      // }
      // }
      // }
      const addressData = await AddressModel.findOne({
        _id: new ObjectId(products[i].address._id),
      })
      if (!isNullOrUndefined(addressData)) {
        let distance =
          getDistance(
            addressData.location.coordinates,
            dvlLocation.coordinates
          ) * distanceFactor

        products[i].distance = distance
      }

      // let AdmixSettings = await VendorAdMixtureSettingModel.findOne({
      //   vendor_id: new ObjectId(products[i].vendor_id),
      // })

      let AdmixSettings = await VendorAdMixtureSettingModel.findOne({
        vendor_id: new ObjectId(products[i].sub_vendor_id),
      })

      if (!isNullOrUndefined(AdmixSettings)) {
        for (let k = 0; k < AdmixSettings.settings.length; k++) {
          if (
            products[i].distance > AdmixSettings.settings[k].min_km &&
            products[i].distance < AdmixSettings.settings[k].max_km
          ) {
            products[i].admix_price = AdmixSettings.settings[k].price
            products[i].ad_mixture1_brand_id =
              AdmixSettings.settings[k].ad_mixture1_brand_id
            products[i].ad_mixture1_category_id =
              AdmixSettings.settings[k].ad_mixture1_category_id
            products[i].ad_mixture2_brand_id =
              AdmixSettings.settings[k].ad_mixture2_brand_id
            products[i].ad_mixture2_category_id =
              AdmixSettings.settings[k].ad_mixture2_category_id
            break
          } else {
            products[i].admix_price = 0
            products[i].selling_price = products[i].selling_price
          }
        }

        if (products[i].admix_price !== 0) {
          products[i].admix_price =
            products[i].admix_price * products[i].ad_mixture_quantity
          products[i].selling_price =
            products[i].selling_price + products[i].admix_price
        } else {
          products[i].admix_price = 0
          products[i].selling_price = products[i].selling_price
        }
      } else {
        products[i].admix_price = 0
        products[i].selling_price = products[i].selling_price
      }

      const margin_rate = await this.commonService.getMarginRate(
        products[i].selling_price,
        true
      )
      products[i].margin_price = (products[i].selling_price * margin_rate) / 100
      products[i].selling_price_with_margin =
        products[i].selling_price + products[i].margin_price

      let final_price = await this.commonService.transcationFeesCalc(
        products[i].selling_price_with_margin
      )

      products[i].selling_price_with_margin = final_price

      let vendorSettingData = await VendorSettingModel.findOne({
        vendor_id: new ObjectId(products[i].sub_vendor_id),
      })

      if (!isNullOrUndefined(vendorSettingData)) {
        if (vendorSettingData.new_order_taken === false) {
          delete products[i]
          // break
          continue
        }
      }

      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(products[i].vendor_id),
      })

      console.log("vendorData", vendorData)
      if (
        !isNullOrUndefined(vendorData) &&
        !isNullOrUndefined(query.delivery_date)
      ) {
        let vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour

        let orderPartData: any = await OrderItemPartModel.find({
          vendor_id: new ObjectId(products[i].vendor_id),
          assigned_date: new Date(query.delivery_date),
        })

        if (!isNullOrUndefined(orderPartData)) {
          var day_capacity = 0
          for (var j = 0; j < orderPartData.length; j++) {
            day_capacity += orderPartData[j].assigned_quantity
          }
          console.log("day_capacity", day_capacity)
          console.log("vendor_capacity", vendor_capacity)
          if (day_capacity > vendor_capacity) {
            delete products[i]
          }
        }
      }

      // Check for cement quantity.
      if (!isNullOrUndefined(products[i])) {
        const cquery: { [k: string]: any } = {
          address_id: products[i].address._id,
          brand_id: products[i].cement_brand._id,
          grade_id: products[i].cement_grade._id,
        }

        cquery.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        let cqData = await CementQuantityModel.findOne(cquery)
        console.log("cqData", cqData)

        // let cqData = await CementQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   brand_id: products[i].cement_brand._id,
        //   grade_id: products[i].cement_grade._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })
        if (isNullOrUndefined(cqData)) {
          delete products[i]
        } else if (
          !isNullOrUndefined(cqData) &&
          cqData.quantity < products[i].cement_quantity
        ) {
          delete products[i]
        }
      }

      if (!isNullOrUndefined(products[i])) {
        // Check for sand quantity.

        const squery: { [k: string]: any } = {
          address_id: products[i].address._id,
          source_id: products[i].sand_source._id,
        }

        squery.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        let sqData = await SandSourceQuantityModel.findOne(squery)

        // let sqData = await SandSourceQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   source_id: products[i].sand_source._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })
        console.log("sqData", sqData)
        if (isNullOrUndefined(sqData)) {
          delete products[i]
        }
        if (
          !isNullOrUndefined(sqData) &&
          sqData.quantity < products[i].sand_quantity
        ) {
          delete products[i]
        }
      }

      // Check for agg1 quantity.
      if (!isNullOrUndefined(products[i])) {
        const aggquery: { [k: string]: any } = {
          address_id: products[i].address._id,
          source_id: products[i].aggregate_source._id,
          sub_cat_id: products[i].aggregate1_sand_category._id,
        }

        aggquery.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        let agg1Data = await AggregateSourceQuantityModel.findOne(aggquery)

        // let agg1Data = await AggregateSourceQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   source_id: products[i].aggregate_source._id,
        //   sub_cat_id: products[i].aggregate1_sand_category._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })

        console.log("agg1Data", agg1Data)
        if (isNullOrUndefined(agg1Data)) {
          delete products[i]
        }
        if (
          !isNullOrUndefined(agg1Data) &&
          agg1Data.quantity < products[i].aggregate1_quantity
        ) {
          delete products[i]
        }
      }
      // Check for agg2 quantity.
      if (!isNullOrUndefined(products[i])) {
        const agg2query: { [k: string]: any } = {
          address_id: products[i].address._id,
          source_id: products[i].aggregate_source._id,
          sub_cat_id: products[i].aggregate2_sand_category._id,
        }

        agg2query.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        let agg2Data = await AggregateSourceQuantityModel.findOne(agg2query)

        // let agg2Data = await AggregateSourceQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   source_id: products[i].aggregate_source._id,
        //   sub_cat_id: products[i].aggregate2_sand_category._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })
        console.log("agg2Data", agg2Data)
        if (isNullOrUndefined(agg2Data)) {
          delete products[i]
        }
        if (
          !isNullOrUndefined(agg2Data) &&
          agg2Data.quantity < products[i].aggregate2_quantity
        ) {
          delete products[i]
        }
      }

      if (
        !isNullOrUndefined(products[i]) &&
        (!isNullOrUndefined(products[i].ad_mixture1_brand_id) ||
          !isNullOrUndefined(products[i].ad_mixture1_brand_id)) &&
        (!isNullOrUndefined(products[i].ad_mixture1_category_id) ||
          !isNullOrUndefined(products[i].ad_mixture2_category_id))
      ) {
        let q: { [k: string]: any } = {
          address_id: products[i].address._id,
          //  vendor_id: products[i].vendor._id,
        }

        q.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        q.$or = [
          {
            brand_id: new ObjectId(products[i].ad_mixture1_brand_id),
          },
          {
            brand_id: new ObjectId(products[i].ad_mixture2_brand_id),
          },
        ]

        q.$or = [
          {
            category_id: new ObjectId(products[i].ad_mixture1_category_id),
          },
          {
            category_id: new ObjectId(products[i].ad_mixture2_category_id),
          },
        ]

        // // Check for admix quantity.
        // let admixData = await AdmixtureQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   brand_id: products[i].ad_mixture1_brand_id,
        //   category_id: products[i].ad_mixture1_category._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })
        // console.log("admixData", admixData)

        // Check for admix quantity.
       
        let admixData = await AdmixtureQuantityModel.findOne(q)
        console.log("admixData", admixData)
        if (isNullOrUndefined(admixData)) {
          delete products[i]
        }
        if (
          !isNullOrUndefined(admixData) &&
          admixData.quantity < products[i].ad_mixture_quantity
        ) {
          delete products[i]
        }
      }

      // Check for fly ash quantity.
      if (
        !isNullOrUndefined(products[i]) &&
        !isNullOrUndefined(products[i].fly_ash_source)
      ) {
        const fquery: { [k: string]: any } = {
          address_id: products[i].address._id,
          source_id: products[i].fly_ash_source._id,
        }

        fquery.$or = [
          {
            vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            sub_vendor_id: new ObjectId(products[i].vendor_id),
          },
          {
            master_vendor_id: new ObjectId(products[i].vendor_id),
          },
        ]

        let flyAshData = await FlyAshSourceQuantityModel.findOne(fquery)
        console.log("flyAshData", flyAshData)
        // let flyAshData = await FlyAshSourceQuantityModel.findOne({
        //   address_id: products[i].address._id,
        //   source_id: products[i].fly_ash_source._id,
        //   vendor_id: products[i].vendor._id,
        //   // sub_vendor_id: products[i].sub_vendor_id,
        //   // master_vendor_id: products[i].master_vendor_id
        // })
        if (isNullOrUndefined(flyAshData)) {
          delete products[i]
        }
        if (
          !isNullOrUndefined(flyAshData) &&
          flyAshData.quantity < products[i].fly_ash_quantity
        ) {
          delete products[i]
        }
      }
    }

    console.log("productssssssssssss", products)
    return products.filter((x) => x !== null)
    // return products
  }

  async getDesignMixDetails(
    design_mix_id: string,
    query: IGetDesignMixDetails,
    user_id?: string
  ) {
    console.log("query", query)
    let dvlLocation = { type: "Point", coordinates: [0, 0] }

    //const requesterUid = new ObjectId(user_id)

    if (
      isNullOrUndefined(query.site_id) &&
      (isNullOrUndefined(query.long) || isNullOrUndefined(query.long))
    ) {
      return Promise.reject(
        new InvalidInput(
          `either Site id, sub city or location is required`,
          400
        )
      )
    }

    if (!isNullOrUndefined(query.site_id)) {
      //getting city id
      const siteQuery = { _id: new ObjectId(query.site_id) }

      console.log("siteQuery", siteQuery)
      const siteData = await SiteModel.findOne(siteQuery)

      console.log("siteData", siteData)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      // sub_city = "" + siteData.sub_city_id
      dvlLocation = siteData.location
    }

    if (!isNullOrUndefined(query.long) && !isNullOrUndefined(query.lat)) {
      dvlLocation.coordinates = [Number(query.long), Number(query.lat)]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, location reference could not be found or no longer exists `,
          400
        )
      )
    }

    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRange.max * 1000, // km in meter
          spherical: true,
          query: {
            _id: new ObjectId(design_mix_id),
            is_available: true,
          },
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "aggregate_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_category_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_category_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "ad_mixture_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "ad_mixture_category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_media",
          localField: "vendor_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $project: {
          _id: 1,
          design_mix_id: 1,
          product_name: 1,
          cement_quantity: 1,
          sand_quantity: 1,
          aggregate1_quantity: 1,
          aggregate2_quantity: 1,
          fly_ash_quantity: 1,
          ad_mixture_quantity: 1,
          water_quantity: 1,
          selling_price: 1,
          is_available: 1,
          description: 1,
          is_custom: 1,
          updated_at: 1,
          updated_by_id: 1,
          calculated_distance: 1,
          vendor_id: 1,
          address_id: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "vendor.rating": 1,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sand_category._id": 1,
          "aggregate1_sand_category.sub_category_name": 1,
          "aggregate2_sand_category._id": 1,
          "aggregate2_sand_category.sub_category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          vendor_media: 1,
        },
      },
    ]

    let designMixs = await DesignMixVariantModel.aggregate(aggregateArr)
    let designMix = designMixs[0]
    console.log("designMix", designMix)

    if (isNullOrUndefined(designMix)) {
      return Promise.reject(new InvalidInput(`No Design Mix found.`, 400))
    }

    const addressData = await AddressModel.findOne({
      _id: new ObjectId(designMix.address_id),
    })

    console.log("addressData", addressData)
    if (!isNullOrUndefined(addressData)) {
      console.log(
        "addressData.location.coordinates",
        addressData.location.coordinates
      )

      console.log("location.coordinates", dvlLocation.coordinates)

      let distance =
        getDistance(addressData.location.coordinates, dvlLocation.coordinates) *
        distanceFactor

      console.log("distance", distance)

      designMix.distance = distance

      let AdmixSettings = await VendorAdMixtureSettingModel.findOne({
        vendor_id: new ObjectId(designMix.vendor_id),
      })
      if (!isNullOrUndefined(AdmixSettings)) {
        for (let k = 0; k < AdmixSettings.settings.length; k++) {
          if (
            designMix.distance > AdmixSettings.settings[k].min_km &&
            designMix.distance < AdmixSettings.settings[k].max_km
          ) {
            designMix.admix_price = AdmixSettings.settings[k].price
          } else {
            designMix.admix_price = 0
            designMix.selling_price = designMix.selling_price
          }
        }
        if (designMix.admix_price !== 0) {
          console.log("products[i].admix_price", designMix.admix_price)
          designMix.admix_price =
            designMix.admix_price * designMix.ad_mixture_quantity
          designMix.selling_price =
            designMix.selling_price + designMix.admix_price
        } else {
          designMix.admix_price = 0
          designMix.selling_price = designMix.selling_price
        }
      } else {
        designMix.admix_price = 0
        designMix.selling_price = designMix.selling_price
      }
      //   designMix.admix_price =
      //     designMix.admix_price * designMix.ad_mixture_quantity
      //   designMix.selling_price =
      //     designMix.selling_price + designMix.admix_price
      // } else {
      //   designMix.admix_price = 0
      //   designMix.selling_price = designMix.selling_price
      // }
      // Check Vendor Setting.
      let setting = await VendorSettingModel.findOne({
        vendor_id: new ObjectId(designMix.vendor_id),
      }).exec()

      if (!isNullOrUndefined(setting)) {
        if (!isNullOrUndefined(query.with_TM) && query.with_TM === true) {
          designMix.with_TM = true
          designMix.TM_price = setting.TM_price * distance
        } else {
          designMix.with_TM = false
          designMix.TM_price = 0
        }
      } else {
        designMix.with_TM = false
        designMix.TM_price = 0
      }
      // let suggestion: any = {}
      // if (!isNullOrUndefined(setting) && setting.with_TM === true) {
      //   if (
      //     isNullOrUndefined(query.with_TM) ||
      //     (!isNullOrUndefined(query.with_TM) && query.with_TM === true)
      //   ) {
      //     const resp = await this.commonService.checkTMavailibity(
      //       designMix.vendor_id,
      //       query.delivery_date
      //     )
      //     console.log("TM_resp", resp)
      //     if (resp.with_TM === true && resp.msgForTM === message.AvailableTM) {
      //       designMix.with_TM = resp.with_TM
      //       designMix.TM_price = resp.TM_price * distance
      //     } else {
      //       designMix.ErrMsgForTM = `Sorry, no Transit Mixer available on the selected date.`
      //       designMix.TM_price = 0
      //       designMix.with_TM = false
      //       designMix.with_CP = false
      //       designMix.CP_price = 0
      //     }
      //   } else {
      //     designMix.TM_price = 0
      //     designMix.with_TM = false
      //     designMix.with_CP = false
      //     designMix.CP_price = 0
      //   }
      // } else {
      //   designMix.TM_price = 0
      //   designMix.with_TM = false
      //   designMix.with_CP = false
      //   designMix.CP_price = 0
      // }

      if (
        !isNullOrUndefined(setting) &&
        setting.with_CP === true &&
        designMix.with_TM === true
      ) {
        if (
          isNullOrUndefined(query.with_CP) ||
          (!isNullOrUndefined(query.with_CP) && query.with_CP === true)
        ) {
          const resp1 = await this.commonService.checkCPavailibity(
            designMix.vendor_id,
            query.delivery_date
          )
          if (
            resp1.with_CP === true &&
            resp1.msgForCP === message.AvailableCP
          ) {
            designMix.with_CP = resp1.with_CP
            designMix.CP_price = resp1.CP_price
          } else {
            designMix.ErrMsgForCp = `Sorry, no Concrete pump available on the selected date.`
            designMix.with_CP = false
            designMix.CP_price = 0
          }
        } else {
          designMix.with_CP = false
          designMix.CP_price = 0
        }
      } else {
        designMix.with_CP = false
        designMix.CP_price = 0
      }
      designMix.selling_price_With_TM_CP =
        designMix.selling_price + designMix.TM_price + designMix.CP_price
      const margin_rate = await this.commonService.getMarginRate(
        designMix.selling_price_With_TM_CP,
        true
      )
      designMix.margin_price =
        (designMix.selling_price_With_TM_CP * margin_rate) / 100
      designMix.selling_price_with_margin =
        designMix.selling_price_With_TM_CP + designMix.margin_price

      let final_price = await this.commonService.transcationFeesCalc(
        designMix.selling_price_with_margin
      )
      designMix.selling_price_with_margin = final_price
    }

    console.log("designMix", designMix)
    return designMix
  }
}
