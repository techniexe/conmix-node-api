import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { PayMethodRepository } from "./pay_method.repository"
import { NextFunction, Response } from "express"
import { CommonTypes } from "../common.types"

@injectable()
@controller("/v1/pay_method")
export class PayMethodController {
  constructor(
    @inject(CommonTypes.PayMethodRepository)
    private payMethodRepo: PayMethodRepository
  ) {}

  @httpGet("/")
  async getPayMethod(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.payMethodRepo.getPayMethod(search, before, after)
    res.json({ data })
  }
}
