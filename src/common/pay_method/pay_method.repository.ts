import { injectable } from "inversify"
import { PayMethodModel } from "../../model/pay_method.model"
import { isNullOrUndefined } from "util"

@injectable()
export class PayMethodRepository {
  async getPayMethod(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return PayMethodModel.find(query).sort({ created_at: -1 }).limit(10)
  }
}
