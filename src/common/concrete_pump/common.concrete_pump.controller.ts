import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { IRequestSchema } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { CommonTypes } from "../common.types"
import { CommonConcretePumpRepository } from "./common.concrete_pump.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

@injectable()
@controller("/v1/concrete_pump")
export class CommonConcretePumpController {
  constructor(
    @inject(CommonTypes.CommonConcretePumpRepository)
    private ConcretePumpRepo: CommonConcretePumpRepository
  ) {}

  @httpGet("/categories")
  async listAllConcretePumpCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.listAllConcretePumpCategory()
    res.json({ data })
  }

  @httpGet("/operator")
  async listOperator(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.listOperator()
    res.json({ data })
  }
}
