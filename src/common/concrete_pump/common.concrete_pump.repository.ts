import { injectable } from "inversify"
import { ConcretePumpCategoryModel } from "../../model/concrete_pump.model"
import { OperatorInfoModel } from "../../model/operator.model"

@injectable()
export class CommonConcretePumpRepository {
  async listAllConcretePumpCategory() {
    return ConcretePumpCategoryModel.find()
  }
  async listOperator() {
    return OperatorInfoModel.find()
  }
}
