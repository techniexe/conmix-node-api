import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { deliveryRangeForVehicle } from "../../utilities/config"
import { UploadedFile } from "express-fileupload"

export interface ITM {
  user_id?: string
  TM_category_id?: string
  TM_sub_category_id?: string
  TM_rc_number: string
  per_Cu_mtr_km: number
  per_metric_ton_per_km_rate: number
  delivery_city_ids: [string]
  delivery_sub_city_ids: [string]
  pickup_city_ids: [string]
  pickup_sub_city_ids: [string]
  manufacturer_name: string
  manufacture_year: number
  TM_model: string
  is_gps_enabled: boolean
  is_insurance_active: boolean
  rc_book_image_url: string
  insurance_image_url: string
  TM_image_url: string
  driver_name: string
  driver_mobile_number: string
  driver_alt_mobile_number: string
  driver_whatsapp_number: string
  driver_pic: string
  delivery_range: number
  calculated_trip_price: number
  min_trip_price: number
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  address_id: string
  //   line1: string
  //   line2?: string
  //   state_id: string
  //   city_id: string
  //   pincode: number
  driver1_id: string
  driver2_id: string
  [k: string]: any
}
export interface IEditTM {
  TM_rc_number: string
  per_Cu_mtr_km: number
  //per_metric_ton_per_km_rate: number
  delivery_city_ids: [string]
  delivery_sub_city_ids: [string]
  pickup_city_ids: [string]
  pickup_sub_city_ids: [string]
  manufacturer_name: string
  manufacture_year: number
  TM_model: string
  is_gps_enabled: boolean
  is_insurance_active: boolean
  is_active: boolean
  rc_book_image_url: string
  insurance_image_url: string
  TM_image_url: string
  driver_name: string
  driver_mobile_number: string
  driver_alt_mobile_number: string
  driver_whatsapp_number: string
  driver_pic: string
  delivery_range: number
  calculated_trip_price: number
  min_trip_price: number
  pickup_location: {
    type: string
    coordinates: [number, number]
  }
  //   line1: string
  //   line2?: string
  //   state_id: string
  //   city_id: string
  //   pincode: number
  address_id: string
  driver1_id: string
  driver2_id: string
}

export interface IAddTMRequest extends IAuthenticatedRequest {
  body: ITM
  params: {
    TMCategoryId: string
    TMSubCategoryId: string
  }
  files: {
    rc_book_image: UploadedFile
    insurance_image: UploadedFile
    TM_image: UploadedFile
  }
}
export interface IGetTMDetailsRequest extends IAuthenticatedRequest {
  params: {
    TMId: string
  }
}

export interface IEditTMRequest extends IAuthenticatedRequest {
  params: {
    TMId: string
  }
  files: {
    rc_book_image: UploadedFile
    insurance_image: UploadedFile
    TM_image: UploadedFile
  }
  body: IEditTM
}

export const addTMSchema: IRequestSchema = {
  body: Joi.object().keys({
    TM_rc_number: Joi.string().required(),
    per_Cu_mtr_km: Joi.number().required(),
    //per_metric_ton_per_km_rate: Joi.number().required().min(3).max(5),
    manufacturer_name: Joi.string().required(),
    manufacture_year: Joi.number().max(9999).required(),
    TM_model: Joi.string().required(),
    is_gps_enabled: Joi.boolean().required(),
    is_insurance_active: Joi.boolean().required(),
    delivery_range: Joi.number()
      .min(deliveryRangeForVehicle.min)
      .max(deliveryRangeForVehicle.max)
      .required(),
    //calculated_trip_price: Joi.number().required(),
    min_trip_price: Joi.number().required().min(0),
    pickup_location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
    address_id: Joi.string().regex(objectIdRegex).required(),
    // line1: Joi.string().required(),
    // line2: Joi.string(),
    // state_id: Joi.string().required(),
    // city_id: Joi.string().required(),
    // pincode: Joi.number().min(6).required(),
    driver1_id: Joi.string().regex(objectIdRegex).required(),
    driver2_id: Joi.string().regex(objectIdRegex),
    // delivery_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // delivery_sub_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // pickup_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // pickup_sub_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // rc_book_image_url: Joi.string(),
    // insurance_image_url: Joi.string(),
    // vehicle_image_url: Joi.string(),
    // driver_name: Joi.string().required(),
    // driver_mobile_number: Joi.string().required(),
    // driver_alt_mobile_number: Joi.string(),
    // driver_whatsapp_number: Joi.string(),
    // driver_pic: Joi.string(),
  }),
  params: Joi.object().keys({
    TMCategoryId: Joi.string().regex(objectIdRegex).required(),
    TMSubCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const getTMDetailsRequest: IRequestSchema = {
  params: Joi.object().keys({
    TMId: Joi.string().regex(objectIdRegex),
  }),
}
export const editTMRequestSchema: IRequestSchema = {
  body: Joi.object().keys({
    TM_rc_number: Joi.string(),
    per_Cu_mtr_km: Joi.number(),
    // per_metric_ton_per_km_rate: Joi.number().min(3).max(5),
    manufacturer_name: Joi.string(),
    manufacture_year: Joi.number().max(9999),
    TM_model: Joi.string(),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
    is_active: Joi.boolean(),
    driver_name: Joi.string(),
    driver_mobile_number: Joi.string(),
    driver_alt_mobile_number: Joi.string(),
    driver_whatsapp_number: Joi.string(),
    driver_pic: Joi.string(),
    delivery_range: Joi.number()
      .min(deliveryRangeForVehicle.min)
      .max(deliveryRangeForVehicle.max),
    min_trip_price: Joi.number().min(0),
    //calculated_trip_price: Joi.number(),
    pickup_location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
    // line1: Joi.string(),
    // line2: Joi.string(),
    // state_id: Joi.string(),
    // city_id: Joi.string(),
    // pincode: Joi.number().min(6),
    address_id: Joi.string().regex(objectIdRegex),
    driver1_id: Joi.string().regex(objectIdRegex),
    driver2_id: Joi.string().regex(objectIdRegex),
    // rc_book_image_url: Joi.string(),
    // insurance_image_url: Joi.string(),
    // vehicle_image_url: Joi.string(),
    // delivery_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // delivery_sub_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // pickup_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
    // pickup_sub_city_ids: Joi.array()
    //   .items(Joi.string().regex(objectIdRegex))
    //   .unique(),
  }),
  params: Joi.object().keys({
    TM_Id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetTMDetails {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  TM_category_id?: string
  TM_sub_category_id?: string
  is_gps_enabled?: boolean
  is_insurance_active?: boolean
}

export interface IGetTMRequest extends IAuthenticatedRequest {
  query: IGetTMDetails
}

export const getTMSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    TM_category_id: Joi.string(),
    TM_sub_category_id: Joi.string().regex(objectIdRegex),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
  }),
}

export const listTMSubCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    TMCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IListTMSubCategoryRequest extends IAuthenticatedRequest {
  params: {
    TMCategoryId: string
  }
}
