import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { CommonTMRepository } from "./common.TM.repository"
import { NextFunction, Response } from "express"
import { IRequestSchema, validate } from "../../middleware/joi.middleware"
import {
  listTMSubCategorySchema,
  IListTMSubCategoryRequest,
} from "./common.TM.req-schema"
import { CommonTypes } from "../common.types"

@injectable()
@controller("/v1/TM")
export class CommonTMController {
  constructor(
    @inject(CommonTypes.CommonTMRepository)
    private TMRepo: CommonTMRepository
  ) {}

  @httpGet("/categories")
  async listAllTMCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.listAllTMCategory()
    res.json({ data })
  }

  @httpGet("/subCategory")
  @httpGet("/subCategory/:TMCategoryId", validate(listTMSubCategorySchema))
  async listSubCategory(
    req: IListTMSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    const { TMCategoryId } = req.params
    const data = await this.TMRepo.listSubCategory(TMCategoryId)
    res.json({ data })
  }

  @httpGet("/driver")
  async listDriver(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.listDriver()
    res.json({ data })
  }
}
