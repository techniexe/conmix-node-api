import { injectable } from "inversify"
import { TMCategoryModel, TMSubCategoryModel } from "../../model/TM.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ObjectId } from "bson"
import { DriverInfoModel } from "../../model/driver.model"

@injectable()
export class CommonTMRepository {
  async listAllTMCategory() {
    return TMCategoryModel.find()
  }

  async listSubCategory(TMCategoryId?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(TMCategoryId)) {
      query.TM_category_id = new ObjectId(TMCategoryId)
    }
    return TMSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: { path: "$TM_category" },
      },
      {
        $project: {
          _id: 1,
          sub_category_name: 1,
          load_capacity: 1,
          weight_unit_code: 1,
          weight_unit: 1,
          created_at: 1,
          "TM_category.category_name": 1,
        },
      },
    ])
  }

  async listDriver() {
    return DriverInfoModel.find()
  }
}
