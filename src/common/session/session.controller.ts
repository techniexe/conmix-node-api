import { injectable, inject } from "inversify"
import { controller, httpPost } from "inversify-express-utils"
import { CommonTypes } from "../common.types"
import { SessionRepository } from "./session.repository"
import { validate } from "../../middleware/joi.middleware"
import { createSessionSchema, ICreateSessionRequest } from "./session.schema"
import { NextFunction, Response } from "express"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { logger } from "../../logger"

@injectable()
@controller("/v1/sessions")
export class SessionController {
  constructor(
    @inject(CommonTypes.SessionRepository)
    private sessionRepo: SessionRepository
  ) {}
  @httpPost("/", validate(createSessionSchema))
  async createSession(
    req: ICreateSessionRequest,
    res: Response,
    next: NextFunction
  ) {
    logger.info("in session ")
    const { accessToken, registrationToken } = req.body
    try {
      const data = await this.sessionRepo.isAccessTokenValid(accessToken)
      logger.info("got data:", data)
      if (isNullOrUndefined(data)) {
        return res.sendStatus(403)
      }
      const token = await this.sessionRepo.createNewSessionToken(
        accessToken,
        registrationToken
      )
      return res.status(200).json({ data: { token } })
    } catch (err) {
      next(err)
    }
  }
}
