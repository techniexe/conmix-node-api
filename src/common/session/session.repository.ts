import { injectable } from "inversify"
import { createJWTToken } from "../../utilities/jwt.utilities"
import { SessionModel } from "../../model/session.model"
@injectable()
export class SessionRepository {
  createNewSessionToken(accessToken: string, registrationToken: string) {
    return createJWTToken({ accessToken, registrationToken })
  }

  async isAccessTokenValid(accessToken: string) {
    return SessionModel.findOne({ token: accessToken }).exec()
  }
}
