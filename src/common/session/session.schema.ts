import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"

export const createSessionSchema: IRequestSchema = {
  body: Joi.object().keys({
    accessToken: Joi.string().required(),
    registrationToken: Joi.string().required(),
  }),
}

export interface ISessionTokenBody {
  accessToken: string
  registrationToken: string
}
export interface ICreateSessionRequest {
  body: ISessionTokenBody
}
