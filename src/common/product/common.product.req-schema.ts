import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IGetProductList {
  site_id?: string
  sub_city?: string
  sub_category_id?: string
  state_id: string
  before?: string
  after?: string
  long?: number
  lat?: number
  source_id?: string
  order_by?: string
  page_num?: number
}

export interface IGetProductListRequest extends IAuthenticatedRequest {
  query: IGetProductList
  params: {
    categoryId: string
  }
}

export const GetProductListSchema: IRequestSchema = {
  query: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    sub_city: Joi.string().regex(objectIdRegex),
    sub_category_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    state_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    order_by: Joi.string(), //comma separated fields. ex- rating, quantity
    page_num: Joi.number(),
  }),
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetProductSubcategory extends IAuthenticatedRequest {
  params: {
    categoryId: string
  }
}

export const getProductSubcategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetProductDetails {
  site_id?: string
  long?: number
  lat?: number
  state_id: string
}

export interface IGetProductDeatilsRequest extends IAuthenticatedRequest {
  query: IGetProductDetails
  params: {
    productId: string
  }
}

export const GetProductDetailsSchema: IRequestSchema = {
  query: Joi.object().keys({
    state_id: Joi.string().regex(objectIdRegex),
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
  }),
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}
