import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { CommonProductRepository } from "./common.product.repository"
import { Request, Response, NextFunction } from "express"
import {
  IGetProductListRequest,
  GetProductListSchema,
  getProductSubcategorySchema,
  IGetProductSubcategory,
  GetProductDetailsSchema,
  IGetProductDeatilsRequest,
} from "./common.product.req-schema"
import { validate } from "../../middleware/joi.middleware"
import { verifyOptionalToken } from "../../middleware/auth-token.middleware"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
@controller("/v1/product", verifyOptionalToken)
export class CommonProductController {
  constructor(
    @inject(CommonTypes.CommonProductRepository)
    private commonProductRepo: CommonProductRepository
  ) {}

  @httpGet("/category")
  async getProductCategory(req: Request, res: Response, next: NextFunction) {
    const data = await this.commonProductRepo.getProductCategory()
    res.json({ data })
  }

  @httpGet("/category/:categoryId", validate(GetProductListSchema))
  async getProductList(req: IGetProductListRequest, res: Response) {
    let uid: string | undefined
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    console.log(req.query)
    const data = await this.commonProductRepo.getProductList(
      req.params.categoryId,
      req.query,
      uid
    )
    res.json({ data })
  }

  @httpGet("/:productId", validate(GetProductDetailsSchema))
  async getProductDetails(req: IGetProductDeatilsRequest, res: Response) {
    let uid: string | undefined
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    console.log(req.query)
    const data = await this.commonProductRepo.getProductDetails(
      req.params.productId,
      req.query,
      uid
    )
    res.json({ data })
  }

  @httpGet("/sub_category/:categoryId", validate(getProductSubcategorySchema))
  async getSubcategory(req: IGetProductSubcategory, res: Response) {
    const data = await this.commonProductRepo.getProductSubcategory(
      req.params.categoryId
    )
    res.json({ data })
  }
}
