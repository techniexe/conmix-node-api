import { injectable } from "inversify"
import {
  AggregateSandCategoryModel,
  AggregateSandSubCategoryModel,
} from "../../model/aggregate_sand_category.model"
import { IGetProductList } from "./common.product.req-schema"
import { ProductModel } from "../../model/product.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { SiteModel } from "../../model/site.model"
import { InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { SubCityModel } from "../../model/region.model"
import { getDistance, distanceFactor } from "../../utilities/distance.utilities"
import {
  GstTypes,
  deliveryRangeForVehicle,
  minOrder,
  OrderItemStatus,
} from "../../utilities/config"
import { getMarginRate } from "../../utilities/rate.utility"
import { VehicleRateModel } from "../../model/vehicle_rate.model"
import { OrderItemModel } from "../../model/order_item.model"

@injectable()
export class CommonProductRepository {
  async getProductCategory() {
    return AggregateSandCategoryModel.find({}, { created_by: 0 }).sort({
      sequence: 1,
    })
  }

  async getProductSubcategory(category_id: string) {
    return AggregateSandSubCategoryModel.find(
      { category_id },
      { created_by: false, sequence: false }
    )
  }
  async getProductList(
    category_id: string,
    query: IGetProductList,
    user_id?: string
  ) {
    const limit = 10
    let skip = 0
    let page_num = query.page_num
    if (!isNullOrUndefined(page_num)) {
      if (page_num > 50) {
        return []
      }

      skip = page_num > 0 ? Math.floor(page_num - 1) * limit : 0
    }

    const arrSortFld = ["rating", "quantity"]

    const sort: { [k: string]: any } = {}

    const requesterUid = new ObjectId(user_id)

    /** It's required for calculate per MT/KM rate. */
    let { state_id } = query

    let { sub_city } = query
    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (
      isNullOrUndefined(query.site_id) &&
      isNullOrUndefined(sub_city) &&
      (isNullOrUndefined(query.long) || isNullOrUndefined(query.lat))
    ) {
      return Promise.reject(
        new InvalidInput(
          `either Site id, sub city or location is required`,
          400
        )
      )
    }

    console.log("user_id", user_id)
    if (!isNullOrUndefined(query.site_id)) {
      //getting city id
     // const siteQuery = { _id: query.site_id, user_id }

      const siteQuery = { _id: query.site_id, user_id }
      const siteData = await SiteModel.findOne(siteQuery)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      sub_city = "" + siteData.sub_city_id
      dvlLocation = siteData.location
    }
    if (!isNullOrUndefined(sub_city)) {
      const subCityData = await SubCityModel.findById(sub_city)
      if (subCityData === null) {
        return Promise.reject(
          new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400)
        )
      }
      dvlLocation = subCityData.location
    }
    if (!isNullOrUndefined(query.long) && !isNullOrUndefined(query.lat)) {
      dvlLocation.coordinates = [query.long, query.lat]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }
    console.log("dvlLocation: ", dvlLocation)
    console.log(query)

    let query1: { [k: string]: any } = {
      category_id: new ObjectId(category_id),
      //is_deleted: false,
      is_available: true,
      verified_by_admin: true,
      // quantity: { $gt: 0 }, //* Remarks: As per discussion all product must be shown, if quantity is 0 then need to show "Out Of Stock".
    }

    if (!isNullOrUndefined(query.before) && query.before !== "") {
      query1.created_at = { $lt: query.before }
    }
    if (!isNullOrUndefined(query.after) && query.after !== "") {
      query1.created_at = { $gt: query.after }
    }

    if (!isNullOrUndefined(query.sub_category_id)) {
      query1.sub_category_id = new ObjectId(query.sub_category_id)
    }

    if (!isNullOrUndefined(query.source_id)) {
      query1.source_id = new ObjectId(query.source_id)
    }

    // if (!isNullOrUndefined(query.state_id)) {
    //   query1.source_id = new ObjectId(query.source_id)
    // }

    if (!isNullOrUndefined(query.order_by)) {
      query.order_by.split(",").reduce((obj, str) => {
        const arrSplt = str.split(":")
        if (arrSortFld.indexOf(arrSplt[0]) < 0) {
          return obj
        }
        return Object.assign(obj, {
          [arrSplt[0]]: arrSplt[1] === "desc" ? -1 : 1,
        })
      }, sort)
    }
    console.log("sort", sort)
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRangeForVehicle.max * 1000, // km in meter
          spherical: true,
          query: query1,
        },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$supplier" },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "productSubCategory.gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $lookup: {
          from: "margin_rate",
          localField: "productSubCategory.margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $unwind: { path: "$gst_slab" },
      },
      {
        $unwind: { path: "$margin_rate" },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $lookup: {
          from: "wishlist",
          let: {
            requesterUid,
            product_id: "$_id",
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $eq: ["$user_id", "$$requesterUid"],
                    },
                    {
                      $eq: ["$product_id", "$$product_id"],
                    },
                  ],
                },
              },
            },
          ],
          as: "wishlist",
        },
      },
      {
        $project: {
          name: 1,
          category_id: 1,
          sub_category_id: 1,
          quantity: 1,
          minimum_order: 1,
          unit_price: 1,
          self_logistics: 1,
          user_id: 1,
          pickup_location: 1,
          calculated_distance: 1,
          rating_count: 1,
          rating_sum: 1,
          rating: 1,
          "supplier._id": 1,
          "supplier.full_name": 1,
          "productCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "productSubCategory.gst_slab_id": 1,
          "productSubCategory.margin_rate_id": 1,
          "productSubCategory.min_quantity": 1,
          "productSubCategory.selling_unit": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.state_id": "$state._id",
          is_wishlisted: { $gt: [{ $size: "$wishlist" }, 0] },
        },
      },
    ]

    if (Object.keys(sort).length > 0) {
      aggregateArr.push({ $sort: sort })
    }

    aggregateArr.push({ $skip: skip })
    aggregateArr.push({ $limit: limit })

    let products = await ProductModel.aggregate(aggregateArr).limit(10)

    console.log(`Before products:`, JSON.stringify(products))

    console.log("state_id", state_id)

    const result = products.find((pr: any) =>
      pr.address.state_id.equals(new ObjectId(state_id))
    )

    console.log(result)
    // products = []
    // products.push(result)

    // console.log(`After products:`, JSON.stringify(products))

    const outProducts: any[] = []

    for (let j = 0; j < products.length; j++) {
      const nPrdt = products[j]
      console.log(`Finding price for product ${nPrdt._id} ${nPrdt.name}`)

      // check state of product pickup match with requested state.
      if (nPrdt.address.state_id.equals(new ObjectId(state_id))) {
        if (nPrdt.self_logistics === false) {
          nPrdt.distance =
            getDistance(
              dvlLocation.coordinates,
              nPrdt.pickup_location.coordinates
            ) * distanceFactor
          console.log(`nPrdt.distance:${nPrdt.distance}`)

          let rateDoc = await VehicleRateModel.findOne({
            state_id: new ObjectId(state_id),
          })

          if (!isNullOrUndefined(rateDoc)) {
            let per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
            nPrdt.logistics_price = nPrdt.distance * per_metric_ton_per_km_rate
          }
          // } else {
          //   return Promise.reject(
          //     new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
          //   )
          // }

          /** Remove Vehicle Calculation as per deiscussion: 02-09-2020 */

          // const vehicles = await VehicleModel.aggregate([
          //   {
          //     $geoNear: {
          //       near: nPrdt.pickup_location,
          //       distanceField: "calculated_distance",
          //       maxDistance: maxVehicleToPickupDistance * 1000, //   <distance in meters>
          //       spherical: true,
          //       query: {
          //         is_active: true,
          //         delivery_range: { $gte: nPrdt.distance },
          //       },
          //     },
          //   },
          //   { $limit: 10 },
          //   {
          //     $lookup: {
          //       from: "vehicle_sub_category",
          //       localField: "vehicle_sub_category_id",
          //       foreignField: "_id",
          //       as: "vehicleSubCategory",
          //     },
          //   },
          //   {
          //     $unwind: "$vehicleSubCategory",
          //   },
          //   {
          //     $project: {
          //       vehicle_category_id: 1,
          //       vehicle_sub_category_id: 1,
          //       per_metric_ton_per_km_rate: 1,
          //       min_trip_price: 1,
          //       delivery_range: 1,
          //       pickup_location: 1,
          //       calculated_distance: 1,
          //       "vehicleSubCategory._id": 1,
          //       "vehicleSubCategory.sub_category_name": 1,
          //       "vehicleSubCategory.min_load_capacity": 1,
          //       "vehicleSubCategory.max_load_capacity": 1,
          //       "vehicleSubCategory.weight_unit": 1,
          //       "vehicleSubCategory.weight_unit_code": 1,
          //     },
          //   },
          // ])
          // console.log(`vehicles: `, vehicles)
          // const avlVehicles = getAvgTripCosts(nPrdt.distance, vehicles)

          //** only for frontend use(Nothing more), if remove then it will create crashes in frontend. */
          nPrdt.vehicles = []
          // for (let i = 0; i < avlVehicles.length; i++) {
          //   const v = avlVehicles[i]
          // const minQuantity = v.vehicleSubCategory.min_load_capacity
          // const minQuantity = minOrder
          const baseCost = nPrdt.unit_price

          const price_with_supplier_and_logistics =
            baseCost + nPrdt.logistics_price
          const marginRate = getMarginRate(
            price_with_supplier_and_logistics,
            nPrdt.margin_rate.slab,
            true
          )

          console.log("marginRate", marginRate)
          const marginAmount =
            (price_with_supplier_and_logistics * marginRate) / 100

          /** For refrence. */
          nPrdt.marginAmount = marginAmount

          /** This value is from config. */
          nPrdt.minimum_order = minOrder
          nPrdt.price_with_margin_and_logistics =
            marginAmount + price_with_supplier_and_logistics
          //const subTotalAmount = baseCost + marginAmont + v.logistics_price
          //const gstAmount = (subTotalAmount * nPrdt.gst_slab.igst_rate) / 100
          //const totalAmount = subTotalAmount + gstAmount
          //   v.totalAmount = totalAmount
          //   nPrdt.vehicles.push(v)
          // }
        } else {
          console.log(
            "nPrdt.productSubCategory.margin_rate",
            nPrdt.productSubCategory.margin_rate
          )
          const baseCostZ =
            (nPrdt.unit_price * (nPrdt.productSubCategory.margin_rate + 100)) /
            100
          nPrdt.gst_type = GstTypes.INTERSTATE
          nPrdt.igst_amount =
            (baseCostZ * nPrdt.productSubCategory.igst_rate) / 100
          nPrdt.total_amount = baseCostZ + nPrdt.igst_amount
        }
      } else {
        return outProducts
      }
      outProducts.push(nPrdt)
    }
    return outProducts
  }

  async getProductDetails(
    product_id: string,
    query: IGetProductList,
    user_id?: string
  ) {
    let { sub_city } = query
    let dvlLocation = { type: "Point", coordinates: [0, 0] }

    const requesterUid = new ObjectId(user_id)
    let { state_id } = query
    if (
      isNullOrUndefined(query.site_id) &&
      isNullOrUndefined(sub_city) &&
      (isNullOrUndefined(query.long) || isNullOrUndefined(query.long))
    ) {
      return Promise.reject(
        new InvalidInput(
          `either Site id, sub city or location is required`,
          400
        )
      )
    }

    console.log("user_id", user_id)
    if (!isNullOrUndefined(query.site_id)) {
      //getting city id
      const siteQuery = { _id: new ObjectId(query.site_id) }

      console.log("siteQuery", siteQuery)
      const siteData = await SiteModel.findOne(siteQuery)

      console.log("siteData", siteData)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      // sub_city = "" + siteData.sub_city_id
      dvlLocation = siteData.location
    }

    // console.log("sub_city", sub_city)
    // if (!isNullOrUndefined(sub_city)) {
    //   console.log("Hereeee")
    //   const subCityData = await SubCityModel.findById(sub_city)
    //   if (subCityData === null) {
    //     return Promise.reject(
    //       new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400)
    //     )
    //   }
    //   dvlLocation = subCityData.location
    // }
    if (!isNullOrUndefined(query.long) && !isNullOrUndefined(query.lat)) {
      dvlLocation.coordinates = [query.long, query.lat]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }
    console.log("dvlLocation: ", dvlLocation)
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRangeForVehicle.max * 1000, // km in meter
          spherical: true,
          query: {
            _id: new ObjectId(product_id),
            // is_deleted: false,
            is_available: true,
            verified_by_admin: true,
            quantity: { $gt: 0 },
          },
        },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$supplier" },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "productSubCategory.gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $lookup: {
          from: "margin_rate",
          localField: "productSubCategory.margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: { path: "$gst_slab" },
      },
      {
        $unwind: { path: "$margin_rate" },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $lookup: {
          from: "wishlist",
          let: {
            requesterUid,
            product_id: "$_id",
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $eq: ["$user_id", "$$requesterUid"],
                    },
                    {
                      $eq: ["$product_id", "$$product_id"],
                    },
                  ],
                },
              },
            },
          ],
          as: "wishlist",
        },
      },
      {
        $project: {
          name: 1,
          category_id: 1,
          sub_category_id: 1,
          quantity: 1,
          minimum_order: 1,
          unit_price: 1,
          self_logistics: 1,
          user_id: 1,
          pickup_location: 1,
          calculated_distance: 1,
          rating_count: 1,
          rating_sum: 1,
          rating: 1,
          "supplier._id": 1,
          "supplier.full_name": 1,
          "productCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "productSubCategory.gst_slab_id": 1,
          "productSubCategory.margin_rate_id": 1,
          "productSubCategory.min_quantity": 1,
          "productSubCategory.selling_unit": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          is_wishlisted: { $gt: [{ $size: "$wishlist" }, 0] },
        },
      },
    ]

    const products = await ProductModel.aggregate(aggregateArr)
    console.log(`Found products:`, JSON.stringify(products))

    const outProducts: any[] = []
    // for (let j = 0; j < products.length; j++) {
    const nPrdt = products[0]
    console.log(`Finding price for product ${nPrdt._id} ${nPrdt.name}`)

    if (nPrdt.self_logistics === false) {
      nPrdt.distance =
        getDistance(
          dvlLocation.coordinates,
          nPrdt.pickup_location.coordinates
        ) * distanceFactor
      console.log(`nPrdt.distance:${nPrdt.distance}`)

      let rateDoc = await VehicleRateModel.findOne({
        state_id: new ObjectId(state_id),
      })

      if (!isNullOrUndefined(rateDoc)) {
        let per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
        nPrdt.logistics_price = nPrdt.distance * per_metric_ton_per_km_rate
      }
      // } else {
      //   return Promise.reject(
      //     new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
      //   )
      // }

      /** Remove Vehicle Calculation as per deiscussion: 02-09-2020 */

      // const vehicles = await VehicleModel.aggregate([
      //   {
      //     $geoNear: {
      //       near: nPrdt.pickup_location,
      //       distanceField: "calculated_distance",
      //       maxDistance: maxVehicleToPickupDistance * 1000, //   <distance in meters>
      //       spherical: true,
      //       query: {
      //         is_active: true,
      //         delivery_range: { $gte: nPrdt.distance },
      //       },
      //     },
      //   },
      //   { $limit: 10 },
      //   {
      //     $lookup: {
      //       from: "vehicle_sub_category",
      //       localField: "vehicle_sub_category_id",
      //       foreignField: "_id",
      //       as: "vehicleSubCategory",
      //     },
      //   },
      //   {
      //     $unwind: "$vehicleSubCategory",
      //   },
      //   {
      //     $project: {
      //       vehicle_category_id: 1,
      //       vehicle_sub_category_id: 1,
      //       per_metric_ton_per_km_rate: 1,
      //       min_trip_price: 1,
      //       delivery_range: 1,
      //       pickup_location: 1,
      //       calculated_distance: 1,
      //       "vehicleSubCategory._id": 1,
      //       "vehicleSubCategory.sub_category_name": 1,
      //       "vehicleSubCategory.min_load_capacity": 1,
      //       "vehicleSubCategory.max_load_capacity": 1,
      //       "vehicleSubCategory.weight_unit": 1,
      //       "vehicleSubCategory.weight_unit_code": 1,
      //     },
      //   },
      // ])
      // console.log(`vehicles: `, vehicles)
      // const avlVehicles = getAvgTripCosts(nPrdt.distance, vehicles)

      //** only for frontend use(Nothing more), if remove then it will create crashes in frontend. */
      nPrdt.vehicles = []
      // for (let i = 0; i < avlVehicles.length; i++) {
      //   const v = avlVehicles[i]
      // const minQuantity = v.vehicleSubCategory.min_load_capacity

      //const minQuantity = minOrder
      const baseCost = nPrdt.unit_price
      const price_with_supplier_and_logistics = baseCost + nPrdt.logistics_price
      const marginRate = getMarginRate(
        price_with_supplier_and_logistics,
        nPrdt.margin_rate.slab,
        true
      )
      const marginAmount =
        (price_with_supplier_and_logistics * marginRate) / 100

      /** For refrence. */
      nPrdt.marginAmount = marginAmount

      /** This value is from config. */
      nPrdt.minimum_order = minOrder
      nPrdt.price_with_margin_and_logistics =
        marginAmount + price_with_supplier_and_logistics

      // const subTotalAmount = baseCost + marginAmont + v.logistics_price
      // const gstAmount = (subTotalAmount * nPrdt.gst_slab.igst_rate) / 100
      // const totalAmount = subTotalAmount + gstAmount
      //   v.totalAmount = totalAmount
      //   nPrdt.vehicles.push(v)
      // }

      console.log("user_id", user_id)
      console.log("supplier_id", nPrdt.user_id)
      console.log("product_id", product_id)
      const data = await OrderItemModel.findOne({
        buyer_id: new ObjectId(user_id),
        supplier_id: new ObjectId(nPrdt.user_id),
        product_id: new ObjectId(nPrdt._id),
        item_status: { $eq: OrderItemStatus.DELIVERED },
      })

      let write_review: boolean = false
      if (!isNullOrUndefined(data)) {
        write_review = true
      }
      nPrdt.write_review = write_review
    } else {
      const baseCostZ =
        (nPrdt.unit_price * (nPrdt.productSubCategory.margin_rate + 100)) / 100
      nPrdt.gst_type = GstTypes.INTERSTATE
      nPrdt.igst_amount = (baseCostZ * nPrdt.productSubCategory.igst_rate) / 100
      nPrdt.total_amount = baseCostZ + nPrdt.igst_amount
    }
    outProducts.push(nPrdt)
    // }
    return outProducts
  }
}
