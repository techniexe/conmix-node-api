import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { AdmixBrandRepository } from "./admix_brand.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"

@controller("/v1/admixture_brand")
@injectable()
export class AdmixBrandController {
  constructor(
    @inject(CommonTypes.AdmixBrandRepository)
    private adMixBrandRepo: AdmixBrandRepository
  ) {}

  @httpGet("/")
  async getAdmixBrand(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.adMixBrandRepo.getAdmixBrand(search, before, after)
    res.json({ data })
  }
}
