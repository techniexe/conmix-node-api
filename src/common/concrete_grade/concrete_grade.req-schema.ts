import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddCg {
  name: string
  [k: string]: any
}

export interface IAddCgRequest extends IAuthenticatedRequest {
  body: IAddCg
}

export const addCgSchema: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150).required(),
  }),
}

export interface IEditCg {
  name?: string
  authentication_code: string
}

export interface IEditCgRequest extends IAuthenticatedRequest {
  body: IEditCg
}

export const editCgSchema: IRequestSchema = {
  params: Joi.object().keys({
    cgId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150),
    authentication_code: Joi.string().required(),
  }),
}

export interface IDeleteCgRequest extends IAuthenticatedRequest {
  params: {
    cgId: string
  }
  body: {
    authentication_code: string
  }
}

export const deleteCgSchema: IRequestSchema = {
  params: Joi.object().keys({
    cgId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
