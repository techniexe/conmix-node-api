import { injectable } from "inversify"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { isNullOrUndefined } from "util"

@injectable()
export class ConcreteGradeRepository {
  async getCg(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return ConcreteGradeModel.find(query).sort({ created_at: 1 })
  }
}
