import { controller, httpGet } from "inversify-express-utils"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { NextFunction, Response } from "express"

import { ConcreteGradeRepository } from "./concrete_grade.repository"
import { CommonTypes } from "../common.types"

@controller("/v1/concrete_grade")
@injectable()
export class ConcreteGradeController {
  constructor(
    @inject(CommonTypes.ConcreteGradeRepository)
    private concreteGradeRepo: ConcreteGradeRepository
  ) {}

  @httpGet("/")
  async getCg(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after } = req.query
    const data = await this.concreteGradeRepo.getCg(search, before, after)
    res.json({ data })
  }
}
