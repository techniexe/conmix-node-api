import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { CementGradeModel } from "../../model/cement_grade.model"

@injectable()
export class CementGradeRepository {
  async getCg(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return CementGradeModel.find(query).sort({ created_at: -1 })
  }
}
