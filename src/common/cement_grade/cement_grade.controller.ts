import { controller, httpGet } from "inversify-express-utils"
import { inject, injectable } from "inversify"
import { CommonTypes } from "../common.types"
import { CementGradeRepository } from "./cement_grade.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"

@controller("/v1/cement_grade")
@injectable()
export class CementGradeController {
  constructor(
    @inject(CommonTypes.CementGradeRepository)
    private cementGradeRepo: CementGradeRepository
  ) {}

  @httpGet("/")
  async getCg(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after } = req.query
    const data = await this.cementGradeRepo.getCg(search, before, after)
    res.json({ data })
  }
}
