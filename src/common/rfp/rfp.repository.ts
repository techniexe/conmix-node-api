import { injectable } from "inversify"
import { IAddRfp } from "./rfp.req.schema"
import { RfpModel } from "../../model/rfp.model"
import { isNullOrUndefined } from "util"

@injectable()
export class RfpRepository {
  async addRfp(RfpData: IAddRfp, user_id: string | undefined) {
    if (!isNullOrUndefined(user_id)) {
      RfpData.user_id = user_id
    }
    const Rfp = new RfpModel(RfpData)
    const RfpDoc = await Rfp.save()
    return Promise.resolve(RfpDoc)
  }
}
