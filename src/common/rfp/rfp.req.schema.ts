import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddRfp {
  category_id: string
  sub_category_id: string
  city_id: string
  sub_city_id: string
  quantity?: number
  message?: string
  email?: string
  user_id?: string
  mobile_number?: string
  full_name?: string
  created_at: Date
  notified_at: Date
}

export interface IAddRfpRequest {
  body: IAddRfp
  user: {
    uid?: string
  }
}

export const addRfpSchema: IRequestSchema = {
  body: Joi.object().keys({
    category_id: Joi.string()
      .regex(objectIdRegex)
      .required(),
    sub_category_id: Joi.string()
      .regex(objectIdRegex)
      .required(),
    city_id: Joi.string()
      .regex(objectIdRegex)
      .required(),
    sub_city_id: Joi.string()
      .regex(objectIdRegex)
      .required(),
    quantity: Joi.number(),
    message: Joi.string(),
    user_id: Joi.string().regex(objectIdRegex),
    email: Joi.string(),
    mobile_number: Joi.string(),
    full_name: Joi.string(),
  }),
}
