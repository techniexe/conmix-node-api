import { injectable, inject } from "inversify"
import { controller, httpPost } from "inversify-express-utils"
import { verifyOptionalToken } from "../../middleware/auth-token.middleware"
import { CommonTypes } from "../common.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { addRfpSchema, IAddRfpRequest } from "./rfp.req.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { RfpRepository } from "./rfp.repository"

@injectable()
@controller("/rfp", verifyOptionalToken)
export class RfpController {
  constructor(
    @inject(CommonTypes.RfpRepository) private rfpRepo: RfpRepository
  ) {}

  // This api used when both supplier or buyer can request for product category.
  @httpPost("/", validate(addRfpSchema))
  async addRfp(req: IAddRfpRequest, res: Response, next: NextFunction) {
    try {
      let uid = undefined
      if (!isNullOrUndefined(req.user && !isNullOrUndefined(req.user.uid))) {
        uid = req.user.uid
        await this.rfpRepo.addRfp(req.body, uid)
        return res.sendStatus(201)
      }
      if (
        isNullOrUndefined(req.body.email) &&
        isNullOrUndefined(req.body.mobile_number)
      ) {
        return res.status(400).json({
          error: { message: "User must add either email or mobile no." },
        })
      }
      await this.rfpRepo.addRfp(req.body, uid)
      return res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }
}
