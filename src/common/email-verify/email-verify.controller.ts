import { inject, injectable } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { IEmailVerifyRequest } from "./email-verify.req.schema"
import { NextFunction, Response } from "express"
import { CommonTypes } from "../common.types"
import { EmailVerifyRepository } from "./email-verify.repository"
import { createJWTToken } from "../../utilities/jwt.utilities"

@injectable()
@controller("/v1/emailVerify")
export class EmailVerifyController {
  constructor(
    @inject(CommonTypes.EmailVerifyRepository)
    private emailVerifyRepo: EmailVerifyRepository
  ) {}
  @httpGet("/:token")
  async verifyEmail(
    req: IEmailVerifyRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.emailVerifyRepo.verifyEmail(req.params.token)
      res.end("Your email has been verified.")
    } catch (e) {
      res.end(e.message)
    }
  }

  @httpGet("/register/:token")
  async verifyRegistrationEmail(
    req: IEmailVerifyRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        email,
        user_id,
        user_type,
      } = await this.emailVerifyRepo.verifyEmail(req.params.token)

      console.log(user_type)
      const jwtObj = {
        user_type,
        full_name: "",
        uid: user_id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken, email } })
    } catch (e) {
      res.end(e.message)
    }
  }
}
