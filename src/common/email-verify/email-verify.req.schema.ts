import { Request } from "express"
export interface IEmailVerifyRequest extends Request {
  params: {
    token: string
  }
}
