import { injectable } from "inversify"
import { AuthEmailModel, emailVerificationType } from "../../model/auth.model"
import { InvalidInput } from "../../utilities/customError"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { LogisticsUserModel } from "../../model/logistics.user.model"
import { ContactModel } from "../../model/contact.model"
import { SiteModel } from "../../model/site.model"

@injectable()
export class EmailVerifyRepository {
  async verifyEmail(auth_token: string): Promise<any> {
    const authData = await AuthEmailModel.findOne({ auth_token })
    const errObj = new InvalidInput(
      `The link provided is expired or invalid.`,
      400
    )
    if (authData === null) {
      return Promise.reject(errObj)
    }
    if (authData.verification_type === emailVerificationType.CONTACT) {
      let profileUpdtStatus = await ContactModel.updateOne(
        {
          email: authData.email,
          "user.user_type": authData.user.user_type,
          "user.user_id": authData.user.user_id,
        },
        { $set: { email_verified: true } }
      )
      if (profileUpdtStatus.n === 0) {
        return Promise.reject(errObj)
      }
      await AuthEmailModel.deleteOne({ _id: authData._id })
      return Promise.resolve()
    }

    if (authData.verification_type === emailVerificationType.SITE) {
      let profileUpdtStatus = await SiteModel.update(
        {
          email: authData.email,
          user_id: authData.user.user_id,
        },
        { $set: { email_verified: true } },
        { multi: true }
      )
      if (profileUpdtStatus.n === 0) {
        return Promise.reject(errObj)
      }
      await AuthEmailModel.deleteOne({ _id: authData._id })
      return Promise.resolve()
    }

    if (authData.verification_type === emailVerificationType.REGISTER) {
      return Promise.resolve({
        email: authData.email,
        user_id: authData._id,
        user_type: authData.user.user_type,
      })
    }

    let updtStatus: { n: number; nModified: number; ok: number }
    switch (authData.user.user_type) {
      case "buyer":
        updtStatus = await BuyerUserModel.updateOne(
          { email: authData.email },
          { $set: { email_verified: true } }
        )
        break
      case "supplier":
        updtStatus = await VendorUserModel.updateOne(
          { email: authData.email },
          { $set: { email_verified: true } }
        )
        break
      case "logistics":
        updtStatus = await LogisticsUserModel.updateOne(
          { email: authData.email },
          { $set: { email_verified: true } }
        )
        break
      default:
        return Promise.reject(errObj)
    }
    if (updtStatus.n === 0) {
      return Promise.reject(errObj)
    }
    await AuthEmailModel.deleteOne({ _id: authData._id })
    return Promise.resolve()
  }
}
