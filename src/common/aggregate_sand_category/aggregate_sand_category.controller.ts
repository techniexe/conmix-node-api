import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { CommonTypes } from "../common.types"
import { CommonAggregateSandCategoryRepository } from "./aggregate_sand_category.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  getSubCategoriesSchema,
  checkSchema,
} from "./aggregate_sand_category.req-schema"

@injectable()
@controller("/v1/aggregate-sand-category")
export class CommonAggregateSandCategoryController {
  constructor(
    @inject(CommonTypes.CommonAggregateSandCategoryRepository)
    private categoryRepo: CommonAggregateSandCategoryRepository
  ) {}

  @httpGet("/")
  async getCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.categoryRepo.getCategories(search, before, after)
    res.json({ data })
  }
  @httpGet("/subcategory/:categoryId", validate(getSubCategoriesSchema))
  async getSubCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.categoryRepo.getSubCategories(
      req.params.categoryId,
      search,
      before,
      after
    )
    res.json({ data })
  }

  @httpGet("/check/:id", validate(checkSchema))
  async check(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const data = await this.categoryRepo.check(req.params.id)
    res.json({ data })
  }
}
