import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { UploadedFile } from "express-fileupload"

export interface ICategory {
  category_name: string
  sequence?: number
  image_url?: string
  thumbnail_url?: string
  [k: string]: any
}

export interface IEditCategory {
  category_name?: string
  //sequence?: number
  image_url?: string
  thumbnail_url?: string
  authentication_code: string
}
export interface ICategoryRequest extends IAuthenticatedRequest {
  body: ICategory
  files: {
    image: UploadedFile
  }
}
export interface IEditCategoryRequest extends IAuthenticatedRequest {
  body: IEditCategory
  files: {
    image: UploadedFile
  }
}

export interface ISubCategory {
  sub_category_name: string
  sequence?: number
  image_url?: string
  thumbnail_url?: string
  //quantity_unit_code?: string // default MT
  //quantity_units?: string // default Metric Tonne
  // igst_rate: number
  // sgst_rate: number
  // cgst_rate: number
  margin_rate_id: string
  gst_slab_id: string
  selling_unit: [string]
  min_quantity: {
    pcs?: number
    nos?: number
    kg?: number
    MT?: number
    sqmeter?: number
    qtl?: number
  }
  [k: string]: any
}

export interface IEditSubCategory {
  sub_category_name?: string
  sequence?: number
  image_url?: string
  thumbnail_url?: string
  quantity_unit_code?: string // default MT
  quantity_units?: string // default Metric Tonne
  // igst_rate?: number
  // sgst_rate?: number
  // cgst_rate?: number
  margin_rate_id?: string
  gst_slab_id: string
  selling_unit: [string]
  min_quantity?: {
    pcs?: number
    nos?: number
    kg?: number
    MT?: number
    sqmeter?: number
    qtl?: number
  }
  authentication_code: string
}
export interface ISubCategoryRequest extends IAuthenticatedRequest {
  body: ISubCategory
  files: {
    image: UploadedFile
  }
}

export interface IEditSubCategoryRequest extends IAuthenticatedRequest {
  body: IEditSubCategory
  files: {
    image: UploadedFile
  }
}
export const categorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(3).max(150).required(),
  }),
}
export const editCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    category_name: Joi.string().min(3).max(150),
    authentication_code: Joi.string().required(),
  }),
}

export const subCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    sub_category_name: Joi.string().min(3).max(150).required(),
    sequence: Joi.number(),
    //image_url: Joi.string(),
    //thumbnail_url: Joi.string(),
    quantity_unit_code: Joi.string(),
    quantity_units: Joi.string(),
    // igst_rate: Joi.number().required(),
    // sgst_rate: Joi.number().required(),
    // cgst_rate: Joi.number().required(),
    margin_rate_id: Joi.string().regex(objectIdRegex).required(),
    gst_slab_id: Joi.string().regex(objectIdRegex).required(),
    selling_unit: Joi.array().allow("kg", "pcs", "nos", "MT", "sqmeter", "qtl"),
    min_quantity: Joi.object()
      .keys({
        pcs: Joi.number(),
        nos: Joi.number(),
        kg: Joi.number(),
        MT: Joi.number(),
        sqmeter: Joi.number(),
        qtl: Joi.number(),
      })
      .required(),
  }),
}

export const editSubCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    subCategoryId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    sub_category_name: Joi.string().min(3).max(150),
    sequence: Joi.number(),
    quantity_unit_code: Joi.string(),
    quantity_units: Joi.string(),
    gst_slab_id: Joi.string().regex(objectIdRegex),
    selling_unit: Joi.array().allow("kg", "pcs", "nos", "MT", "sqmeter", "qtl"),
    margin_rate_id: Joi.string().regex(objectIdRegex),
    min_quantity: Joi.object().keys({
      pcs: Joi.number(),
      nos: Joi.number(),
      kg: Joi.number(),
      MT: Joi.number(),
      sqmeter: Joi.number(),
      qtl: Joi.number(),
    }),
    authentication_code: Joi.string().required(),
  }),
}
export const getSubCategoriesSchema: IRequestSchema = {
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
}

export const checkSchema: IRequestSchema = {
  params: Joi.object().keys({
    id: Joi.string().regex(objectIdRegex).required(),
  }),
}
