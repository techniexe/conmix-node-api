import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  AggregateSandCategoryModel,
  AggregateSandSubCategoryModel,
} from "../../model/aggregate_sand_category.model"
import { ObjectId } from "bson"

@injectable()
export class CommonAggregateSandCategoryRepository {
  async getCategories(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return AggregateSandCategoryModel.find(query).sort({ created_at: -1 })
  }

  async getSubCategories(
    category_id: string,
    search?: string,
    before?: string,
    after?: string
  ) {
    let query: { [k: string]: any } = { category_id: new ObjectId(category_id) }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    console.log(query)
    return AggregateSandSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },

      {
        $lookup: {
          from: "gst_slab",
          localField: "gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $unwind: {
          path: "$gst_slab",
        },
      },

      {
        $lookup: {
          from: "margin_rate",
          localField: "margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: {
          path: "$margin_rate",
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "aggregate_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_category",
        },
      },
      {
        $project: {
          sub_category_name: 1,
          "aggregate_sand_category._id": 1,
          "aggregate_sand_category.category_name": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          sequence: 1,
          min_quantity: 1,
          selling_unit: 1,
          image_url: 1,
          thumbnail_url: 1,
          created_at: 1,
          created_by: 1,
        },
      },
    ])
  }

  async check(id: string) {
    let arr = await AggregateSandSubCategoryModel.find()
    for (let i = 0; i < arr.length; i++) {
      let d = arr[i].sub_category_name.substring(
        0,
        arr[i].sub_category_name.length - 2
      )
      arr[i].sub_category_name = d
      console.log(arr)
    }

    let resp = arr.sort(function (a, b) {
      return a.sub_category_name - b.sub_category_name
    })
    console.log("resp", resp)
    let final = []
    for (let i = 0; i < resp.length; i++) {
      final.push(resp[i]._id)
    }
    console.log("final", final)

    var p1 = final.slice(0, 2)
    var p2 = final.slice(2)

    console.log("p1", p1)
    console.log("p2", p2)

    var isInArray1 = p1.some(function (friend) {
      return friend.equals(id)
    })
    console.log("isInArray1", isInArray1)
    if (isInArray1 === true) {
      console.log("here")
      let resp1 = []
      for (let i = 0; i < p2.length; i++) {
        resp1.push(
          await AggregateSandSubCategoryModel.findOne({
            _id: new ObjectId(p2[i]),
          })
        )
      }
      console.log("resp1", resp1)
      return resp1
    }

    var isInArray2 = p2.some(function (friend) {
      return friend.equals(id)
    })
    console.log("isInArray2", isInArray2)
    if (isInArray2 === true) {
      let resp2 = []
      for (let i = 0; i < p1.length; i++) {
        console.log("here")

        resp2.push(
          await AggregateSandSubCategoryModel.findOne({
            _id: new ObjectId(p1[i]),
          })
        )
      }
      console.log("resp2", resp2)
      return resp2
    }
  }
}
