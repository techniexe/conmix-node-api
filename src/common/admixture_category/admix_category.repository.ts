import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { ObjectId } from "bson"

@injectable()
export class AdmixCategoryRepository {
  async getCategories(
    search?: string,
    before?: string,
    after?: string,
    brand_id?: string
  ) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(brand_id) && brand_id !== "") {
      query.brand_id = new ObjectId(brand_id)
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return AdmixtureCategoryModel.find(query).sort({ created_at: -1 })
  }
}
