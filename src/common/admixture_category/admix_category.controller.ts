import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"
import { AdmixCategoryRepository } from "./admix_category.repository"

@controller("/v1/admixture_category")
@injectable()
export class AdmixCategoryController {
  constructor(
    @inject(CommonTypes.AdmixCategoryRepository)
    private adMixCatRepo: AdmixCategoryRepository
  ) {}

  @httpGet("/")
  async getCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, brand_id } = req.query
    const data = await this.adMixCatRepo.getCategories(
      search,
      before,
      after,
      brand_id
    )
    res.json({ data })
  }
}
