import { controller, httpPost } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { CommonTypes } from "../common.types"
import { CustomMixRepository } from "./custom_mix.repository"
import { NextFunction, Response } from "express"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { validate } from "../../middleware/joi.middleware"
import {
  customMixListSchema,
  ICustomMixListRequest,
  getCustomMixDetailsSchema,
  ICustomMixDetailsRequest,
  ListSchema,
  IListRequest,
} from "./custom_mix.req-schema"
import { verifyOptionalToken } from "../../middleware/auth-token.middleware"

@controller("/v1/custom_mix", verifyOptionalToken)
@injectable()
export class CustomMixController {
  constructor(
    @inject(CommonTypes.CustomMixRepository)
    private customMixRepo: CustomMixRepository
  ) {}

  @httpPost("/", validate(customMixListSchema))
  async customMixList(
    req: ICustomMixListRequest,
    res: Response,
    next: NextFunction
  ) {
    let uid: string | undefined

    console.log("user", req.user)
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    const data = await this.customMixRepo.customMixList(req.body, uid)
    return res.json({ data })
  }

  @httpPost("/details/:vendor_id", validate(getCustomMixDetailsSchema))
  async getCustomMixDetails(
    req: ICustomMixDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    let uid: string | undefined
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    const data = await this.customMixRepo.customMixDetails(
      req.params.vendor_id,
      req.body,
      uid
    )
    res.json({ data })
  }

  @httpPost("/options", validate(ListSchema))
  async getList(req: IListRequest, res: Response, next: NextFunction) {
    let uid: string | undefined

    console.log("user", req.user)
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }

    const data = await this.customMixRepo.getList(req.body, uid)
    return res.json({ data })
  }
}
