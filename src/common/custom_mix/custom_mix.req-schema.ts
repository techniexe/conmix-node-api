import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface ICustomMixList {
  site_id?: string
  long?: number
  lat?: number
  concrete_grade_id: string
  cement_grade_id: string[]
  cement_brand_id: string[]
  cement_quantity: number
  sand_source_id: string[]
  sand_quantity: number
  aggregate1_sub_cat_id: string[]
  aggregate2_sub_cat_id: string[]
  aggregate_source_id: string[]
  aggregate1_quantity: number
  aggregate2_quantity: number
  fly_ash_source_id: string[]
  fly_ash_quantity: number
  admix_brand_id: string[]
  admix_category_id: string[]
  admix_quantity: number
  water_quantity: number
  with_CP: boolean
  with_TM: boolean
  delivery_date: Date
  end_date: Date
  quantity: number
}

export interface ICustomMixListRequest extends IAuthenticatedRequest {
  body: ICustomMixList
}

export const customMixListSchema: IRequestSchema = {
  body: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    concrete_grade_id: Joi.string().regex(objectIdRegex).required(),
    cement_grade_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    cement_brand_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    cement_quantity: Joi.number().required(),
    sand_source_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    sand_quantity: Joi.number(),
    aggregate1_sub_cat_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    aggregate2_sub_cat_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    aggregate_source_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    aggregate1_quantity: Joi.number().required(),
    aggregate2_quantity: Joi.number().required(),
    fly_ash_source_id: Joi.array().items(Joi.string().regex(objectIdRegex)),
    fly_ash_quantity: Joi.number(),
    admix_brand_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    admix_category_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    admix_quantity: Joi.number(),
    water_quantity: Joi.number().required(),
    with_CP: Joi.boolean().required(),
    with_TM: Joi.boolean().required(),
    delivery_date: Joi.date().required(),
    end_date: Joi.date(),
    quantity: Joi.number(),
  }),
}

export interface ICustomMixDetails {
  site_id?: string
  long?: number
  lat?: number
  concrete_grade_id: string
  cement_grade_id: string[]
  cement_brand_id: string[]
  cement_quantity: number
  sand_source_id: string[]
  sand_quantity: number
  aggregate1_sub_cat_id: string[]
  aggregate2_sub_cat_id: string[]
  aggregate_source_id: string[]
  aggregate1_quantity: number
  aggregate2_quantity: number
  fly_ash_source_id: string[]
  fly_ash_quantity: number
  admix_brand_id: string[]
  admix_category_id: string[]
  admix_quantity: number
  water_quantity: number
  with_CP: boolean
  with_TM: boolean
  delivery_date: Date
  address_id: string
  end_date: Date
  quantity: number
}

export interface ICustomMixDetailsRequest extends IAuthenticatedRequest {
  params: {
    vendor_id: string
  }
  body: ICustomMixDetails
}

export const getCustomMixDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    vendor_id: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    concrete_grade_id: Joi.string().regex(objectIdRegex).required(),
    cement_grade_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    cement_brand_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    cement_quantity: Joi.number().required(),
    sand_source_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    sand_quantity: Joi.number(),
    aggregate1_sub_cat_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    aggregate2_sub_cat_id: Joi.array()
      .items(Joi.string().regex(objectIdRegex).required())
      .required(),
    aggregate_source_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    aggregate1_quantity: Joi.number().required(),
    aggregate2_quantity: Joi.number().required(),
    fly_ash_source_id: Joi.array().items(Joi.string().regex(objectIdRegex)),
    fly_ash_quantity: Joi.number(),
    admix_brand_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    admix_category_id: Joi.array().items(
      Joi.string().regex(objectIdRegex).required()
    ),
    admix_quantity: Joi.number(),
    water_quantity: Joi.number().required(),
    with_CP: Joi.boolean().required(),
    with_TM: Joi.boolean().required(),
    delivery_date: Joi.date().required(),
    end_date: Joi.date(),
    quantity: Joi.number(),
  }),
}

export interface IGetList {
  site_id?: string
  long?: number
  lat?: number
}

export interface IListRequest extends IAuthenticatedRequest {
  body: IGetList
}

export const ListSchema: IRequestSchema = {
  body: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
  }),
}
