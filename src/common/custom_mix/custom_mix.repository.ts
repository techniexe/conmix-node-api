import { CementBrandModel } from "./../../model/cement_brand.model"
import { DesignMixVariantModel } from "./../../model/design_mixture_variant.model"
import { ObjectId } from "bson"
import { message } from "./../../utilities/config"
import { CommonService } from "./../../utilities/common.service"
import { AddressModel } from "./../../model/address.model"
import { injectable, inject } from "inversify"
import {
  ICustomMixList,
  ICustomMixDetails,
  IGetList,
} from "./custom_mix.req-schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { SiteModel } from "../../model/site.model"
import { deliveryRange } from "../../utilities/config"
import { CommonTypes } from "../common.types"
import { DesignMixtureModel } from "../../model/design_mixture.model "
import { VendorSettingModel } from "../../model/vendor.setting.model"
import { SandSourceModel } from "../../model/sand_source.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { VendorUserModel } from "../../model/vendor.user.model"
//import { VendorOrderModel } from "../../model/vendor.order.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"

//let finalObj: { [k: string]: any } = {}
@injectable()
export class CustomMixRepository {
  constructor(
    @inject(CommonTypes.CommonService) private commonService: CommonService
  ) {}
  async customMixList(reqData: ICustomMixList, user_id?: string) {
    console.log("reqData", reqData)
    //const requesterUid = new ObjectId(user_id)
    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (
      isNullOrUndefined(reqData.site_id) &&
      (isNullOrUndefined(reqData.long) || isNullOrUndefined(reqData.lat))
    ) {
      return Promise.reject(
        new InvalidInput(`either Site id or location is required`, 400)
      )
    }
    if (!isNullOrUndefined(reqData.site_id)) {
      //getting city id
     // const siteQuery = { _id: reqData.site_id, user_id: new ObjectId(user_id) }

      const siteQuery = { _id: reqData.site_id, user_id: new ObjectId(user_id) }
      const siteData = await SiteModel.findOne(siteQuery)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      dvlLocation = siteData.location
    }
    if (!isNullOrUndefined(reqData.long) && !isNullOrUndefined(reqData.lat)) {
      dvlLocation.coordinates = [Number(reqData.long), Number(reqData.lat)]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }

    // Check vendor exists or not in nearby.
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRange.max * 1000, // convert km into meter.
          spherical: true,
        },
      },
    ]

    let addressData = await AddressModel.aggregate(aggregateArr)
    console.log("data", addressData)
    if (addressData.length <= 0) {
      return Promise.reject(
        new InvalidInput(`No vendor exists nearby with this address..!`, 400)
      )
    }

    /**********"Check is vendor provide custom design mix and new order taken or not"**************************** */
    let custom: any[] = []
    for (let i = 0; i < addressData.length; i++) {
      const resp = await VendorSettingModel.findOne({
        vendor_id: new ObjectId(addressData[i].user.user_id),
        is_customize_design_mix: true,
        new_order_taken: true,
      })
      if (!isNullOrUndefined(resp)) {
        custom.push(addressData[i].user.user_id)
      }
    }
    console.log("custom", custom)
    const vendor1_ids = await this.commonService.removeDuplicates(custom)
    console.log("vendor_ids", vendor1_ids)

    /**********"Check vendor capacity"**************************** */
    let vendor_ids: any[] = []
    for (let j = 0; j < vendor1_ids.length; j++) {
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor1_ids[j]),
        verified_by_admin: true,
      })

      if (!isNullOrUndefined(vendorData)) {
        let vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour

        let orderPartData: any = await OrderItemPartModel.find({
          vendor_id: new ObjectId(vendor1_ids[j]),
          assigned_date: new Date(reqData.delivery_date),
        })

        if (!isNullOrUndefined(orderPartData)) {
          var day_capacity = 0
          for (var k = 0; k < orderPartData.length; k++) {
            day_capacity += orderPartData[k].assigned_quantity
          }
          console.log("day_capacity", day_capacity)

          if (day_capacity < vendor_capacity) {
            vendor_ids.push(vendor1_ids[j])
          }
        }
      }
    }

    /************CHECK FOR "TM"**************** */
    let TM_ids: any[] = []
    if (!isNullOrUndefined(reqData.with_TM) && reqData.with_TM === true) {
      for (let i = 0; i < vendor_ids.length; i++) {
        const resp = await this.commonService.checkTMavailibity(
          vendor_ids[i],
          reqData.delivery_date
        )
        console.log("TM_resp", resp)
        if (resp.with_TM === true && resp.msgForTM === message.AvailableTM) {
          TM_ids.push(vendor_ids[i])
          console.log("TM_vendor_ids", TM_ids)
        }
      }
    } else {
      console.log("When NO TM..!")
      TM_ids = vendor_ids
    }

    /************CHECK FOR "Concrete grade"**************** */
    let cg_ids: any[] = []
    if (!isNullOrUndefined(reqData.concrete_grade_id)) {
      for (let i = 0; i < TM_ids.length; i++) {
        const resp = await DesignMixtureModel.findOne({
          grade_id: new ObjectId(reqData.concrete_grade_id),
          vendor_id: new ObjectId(TM_ids[i]),
        })
        console.log("CG_resp", resp)
        if (!isNullOrUndefined(resp)) {
          cg_ids.push(TM_ids[i])
          console.log("concrete_grade_vendor_ids", cg_ids)
        }
      }

      // if (cg_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(`No vendor exists with this concrete_grade..!`, 400)
      //   )
      // }
    }
    let addrVendorCheck: any[] = []
    for (let i = 0; i < addressData.length; i++) {
      if (addrVendorCheck[addressData[i].user.user_id] === undefined) {
        addrVendorCheck[addressData[i].user.user_id] = [addressData[i]]
      } else {
        addrVendorCheck[addressData[i].user.user_id].push(addressData[i])
      }
    }
    console.log("addrVendorCheckkkkkkkkkkk", addrVendorCheck)
    const res = await this.commonService.checkQuantity(
      cg_ids,
      addrVendorCheck,
      reqData
    )

    console.log("ressssssssssssssss", res)
    let output = await this.commonService.findCustom(
      res.finalObj,
      res.admix_vendor_ids,
      res.admix_address_ids,
      reqData.cement_quantity,
      reqData.sand_quantity,
      reqData.aggregate1_quantity,
      reqData.aggregate2_quantity,
      reqData.fly_ash_quantity,
      reqData.admix_quantity,
      reqData.water_quantity,
      reqData.with_CP,
      reqData.with_TM,
      reqData.delivery_date,
      reqData.end_date,
      dvlLocation,
      1
    )

    output = output.map((e) => ({
      concrete_grade_id: reqData.concrete_grade_id,
      selling_price_with_margin: e.selling_price + e.margin_price_for_selling,
      ...e,
    }))

    for (let j = 0; j < output.length; j++) {
      let final_price = await this.commonService.transcationFeesCalc(
        output[j].selling_price_with_margin
      )
      output[j].selling_price_with_margin = final_price
    }
    console.log("output", output)
    let response = await this.commonService.getDetails(output)

    console.log("response", response)

    return Promise.resolve(response)
  }

  async customMixDetails(
    vendor_id: string,
    reqData: ICustomMixDetails,
    user_id?: string
  ) {
    console.log("reqData", reqData)

    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (
      isNullOrUndefined(reqData.site_id) &&
      (isNullOrUndefined(reqData.long) || isNullOrUndefined(reqData.lat))
    ) {
      return Promise.reject(
        new InvalidInput(`either Site id or location is required`, 400)
      )
    }
    if (!isNullOrUndefined(reqData.site_id)) {
      //getting city id
      const siteQuery = { _id: reqData.site_id, user_id }
      const siteData = await SiteModel.findOne(siteQuery)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      dvlLocation = siteData.location
    }
    if (!isNullOrUndefined(reqData.long) && !isNullOrUndefined(reqData.lat)) {
      dvlLocation.coordinates = [Number(reqData.long), Number(reqData.lat)]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }

    let TM_ids: any[] = []
    let vendor_ids = [vendor_id]

    const aggregateArr: any[] = [
      {
        $match: { _id: new ObjectId(reqData.address_id) },
      },
    ]
    let addressData = await AddressModel.aggregate(aggregateArr)
    if (isNullOrUndefined(addressData)) {
      return Promise.reject(new InvalidInput(`No Address doc found..!`, 400))
    }
    let suggestion: any = {}
    /************CHECK FOR "TM"**************** */
    if (!isNullOrUndefined(reqData.with_TM) && reqData.with_TM === true) {
      for (let i = 0; i < vendor_ids.length; i++) {
        const resp = await this.commonService.checkTMavailibity(
          vendor_ids[i],
          reqData.delivery_date
        )
        console.log("TM_resp", resp)
        if (resp.with_TM === true && resp.msgForTM === message.AvailableTM) {
          TM_ids.push(vendor_ids[i])
          console.log("TM_vendor_ids", TM_ids)
        } else {
          suggestion.ErrMsgForTM = `Sorry, no Transit Mixer available on the selected date.`
          TM_ids = vendor_ids
          reqData.with_TM = false
        }
      }
    } else {
      //suggestion.push({ ErrMsgForCp: `No TM available.` })
      TM_ids = vendor_ids
    }
    console.log("TM_ids", TM_ids)
    /************CHECK FOR "CP"**************** */
    let CP_ids: any[] = []
    if (!isNullOrUndefined(reqData.with_CP) && reqData.with_CP === true) {
      for (let i = 0; i < TM_ids.length; i++) {
        const resp1 = await this.commonService.checkCPavailibity(
          TM_ids[i],
          reqData.delivery_date
        )

        if (resp1.with_CP === true && resp1.msgForCP === message.AvailableCP) {
          CP_ids.push(TM_ids[i])
          console.log("CP_vendor_ids", CP_ids)
        } else {
          suggestion.ErrMsgForCp = `Sorry, no Concrete pump available on the selected date.`
          CP_ids = TM_ids
          reqData.with_CP = false
        }
      }
    } else {
      //suggestion.push({ ErrMsgForCp: `No TM available.` })
      CP_ids = TM_ids
    }

    /************CHECK FOR "Concrete grade"**************** */
    let cg_ids: any[] = []
    if (!isNullOrUndefined(reqData.concrete_grade_id)) {
      console.log("hereeeeee")
      console.log("CP_ids[i]", CP_ids)
      for (let i = 0; i < CP_ids.length; i++) {
        const resp = await DesignMixtureModel.findOne({
          grade_id: new ObjectId(reqData.concrete_grade_id),
          vendor_id: new ObjectId(CP_ids[i]),
        })
        console.log("CG_resp", resp)
        if (!isNullOrUndefined(resp)) {
          cg_ids.push(CP_ids[i])
          console.log("concrete_grade_vendor_ids", cg_ids)
        }
      }

      if (cg_ids.length <= 0) {
        return Promise.reject(
          new InvalidInput(`No vendor exists with this concrete_grade..!`, 400)
        )
      }
    }
    let addrVendorCheck: any[] = []
    for (let i = 0; i < addressData.length; i++) {
      if (addrVendorCheck[addressData[i].user.user_id] === undefined) {
        addrVendorCheck[addressData[i].user.user_id] = [addressData[i]]
      } else {
        addrVendorCheck[addressData[i].user.user_id].push(addressData[i])
      }
    }
    console.log("addrVendorCheckkkkkkkkkkk", addrVendorCheck)
    const res = await this.commonService.checkQuantity(
      cg_ids,
      addrVendorCheck,
      reqData
    )

    let output = await this.commonService.findCustom(
      res.finalObj,
      res.admix_vendor_ids,
      res.admix_address_ids,
      reqData.cement_quantity,
      reqData.sand_quantity,
      reqData.aggregate1_quantity,
      reqData.aggregate2_quantity,
      reqData.fly_ash_quantity,
      reqData.admix_quantity,
      reqData.water_quantity,
      reqData.with_CP,
      reqData.with_TM,
      reqData.delivery_date,
      reqData.end_date,
      dvlLocation,
      1
    )

    output = output.map((e) => ({
      concrete_grade_id: reqData.concrete_grade_id,
      selling_price_with_margin: e.selling_price + e.margin_price_for_selling,
      ...e,
    }))

    for (let j = 0; j < output.length; j++) {
      let final_price = await this.commonService.transcationFeesCalc(
        output[j].selling_price_with_margin
      )
      output[j].selling_price_with_margin = final_price
    }

    console.log("output", output)
    let response = await this.commonService.getDetails(output)
    return Promise.resolve({ response, suggestion })
  }

  async getList(reqData: IGetList, user_id?: string) {
    console.log("reqData", reqData)
    //const requesterUid = new ObjectId(user_id)
    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (
      isNullOrUndefined(reqData.site_id) &&
      (isNullOrUndefined(reqData.long) || isNullOrUndefined(reqData.lat))
    ) {
      return Promise.reject(
        new InvalidInput(`either Site id or location is required`, 400)
      )
    }

    console.log("user_id", user_id)
    if (!isNullOrUndefined(reqData.site_id)) {
      //getting city id
      // const siteQuery = { _id: reqData.site_id, user_id: new ObjectId(user_id) }

      const siteQuery = { _id: reqData.site_id}
      const siteData = await SiteModel.findOne(siteQuery)
      if (siteData === null) {
        return Promise.reject(
          new InvalidInput(`Invalid site reference provided`, 400)
        )
      }
      dvlLocation = siteData.location
    }
    if (!isNullOrUndefined(reqData.long) && !isNullOrUndefined(reqData.lat)) {
      dvlLocation.coordinates = [Number(reqData.long), Number(reqData.lat)]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }

    // Check vendor exists or not in nearby.
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: dvlLocation,
          distanceField: "calculated_distance",
          maxDistance: deliveryRange.max * 1000, // convert km into meter.
          spherical: true,
        },
      },
    ]

    let addressData = await AddressModel.aggregate(aggregateArr)
    console.log("data", addressData)
    if (addressData.length <= 0) {
      return Promise.reject(
        new InvalidInput(`No vendor exists nearby with this address..!`, 400)
      )
    }

    /**********"Check is vendor provide custom design mix or not"**************************** */
    let custom: any[] = []
    for (let i = 0; i < addressData.length; i++) {
      const resp = await VendorSettingModel.findOne({
        vendor_id: new ObjectId(addressData[i].user.user_id),
        is_customize_design_mix: true,
      })
      if (!isNullOrUndefined(resp)) {
        custom.push(addressData[i].user.user_id)
      }
    }
    console.log("custom", custom)
    const vendor_ids = await this.commonService.removeDuplicates(custom)
    console.log("vendor_ids", vendor_ids)
    if (custom.length <= 0) {
      return Promise.reject(
        new InvalidInput(`No any vendor provide custom design mix..!`, 400)
      )
    }
    let temp: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      let designMixData = await DesignMixVariantModel.find({
        vendor_id: new ObjectId(vendor_ids[i]),
      })
      console.log("designMixData", designMixData)
      if (designMixData.length > 0) {
        temp.push(designMixData)
      }
    }
    var result = temp.reduce((r, e) => (r.push(...e), r), [])

    let cement_brand: any[] = []
    let sand_source: any[] = []
    let agg_source: any[] = []
    let agg1_sub_cat: any[] = []
    let agg2_sub_cat: any[] = []
    let fly_ash_source: any[] = []
    let admix_brand: any[] = []
    let cement_grade: any[] = []
    let admix_category: any[] = []
    for (let i = 0; i < result.length; i++) {
      let cementBrandData = await CementBrandModel.findOne({
        _id: new ObjectId(result[i].cement_brand_id),
      })
      console.log("cementBrandData", cementBrandData)
      if (!isNullOrUndefined(cementBrandData)) {
        const found = cement_brand.some((el: any) =>
          el._id.equals(result[i].cement_brand_id)
        )
        if (!found)
          cement_brand.push({
            _id: cementBrandData._id,
            name: cementBrandData.name,
          })
      }

      let sandSourceData = await SandSourceModel.findOne({
        _id: new ObjectId(result[i].sand_source_id),
      })
      console.log("sandSourceData", sandSourceData)
      if (!isNullOrUndefined(sandSourceData)) {
        const found = sand_source.some((el: any) =>
          el._id.equals(result[i].sand_source_id)
        )
        if (!found)
          sand_source.push({
            _id: sandSourceData._id,
            name: sandSourceData.sand_source_name,
          })
      }

      let aggSourceData = await AggregateSourceModel.findOne({
        _id: new ObjectId(result[i].aggregate_source_id),
      })
      console.log("aggSourceData", aggSourceData)
      if (!isNullOrUndefined(aggSourceData)) {
        const found = agg_source.some((el: any) =>
          el._id.equals(result[i].aggregate_source_id)
        )
        if (!found)
          agg_source.push({
            _id: aggSourceData._id,
            name: aggSourceData.aggregate_source_name,
          })
      }

      let agg1SubCatData = await AggregateSandSubCategoryModel.findOne({
        _id: new ObjectId(result[i].aggregate1_sub_category_id),
      })
      console.log("agg1SubCatData", agg1SubCatData)
      if (!isNullOrUndefined(agg1SubCatData)) {
        const found = agg1_sub_cat.some((el: any) =>
          el._id.equals(result[i].aggregate1_sub_category_id)
        )
        if (!found)
          agg1_sub_cat.push({
            _id: agg1SubCatData._id,
            name: agg1SubCatData.sub_category_name,
          })
      }

      let agg2SubCatData = await AggregateSandSubCategoryModel.findOne({
        _id: new ObjectId(result[i].aggregate2_sub_category_id),
      })
      console.log("agg2SubCatData", agg2SubCatData)
      if (!isNullOrUndefined(agg2SubCatData)) {
        const found = agg2_sub_cat.some((el: any) =>
          el._id.equals(result[i].aggregate2_sub_category_id)
        )
        if (!found)
          agg2_sub_cat.push({
            _id: agg2SubCatData._id,
            name: agg2SubCatData.sub_category_name,
          })
      }

      let flyAshData = await FlyAshSourceModel.findOne({
        _id: new ObjectId(result[i].fly_ash_source_id),
      })
      console.log("flyAshData", flyAshData)
      if (!isNullOrUndefined(flyAshData)) {
        const found = fly_ash_source.some((el: any) =>
          el._id.equals(result[i].fly_ash_source_id)
        )
        if (!found)
          fly_ash_source.push({
            _id: flyAshData._id,
            name: flyAshData.fly_ash_source_name,
          })
      }

      let adMixData = await AdmixtureBrandModel.findOne({
        _id: new ObjectId(result[i].ad_mixture_brand_id),
      })
      console.log("adMixData", adMixData)
      if (!isNullOrUndefined(adMixData)) {
        const found = admix_brand.some((el: any) =>
          el._id.equals(result[i].ad_mixture_brand_id)
        )
        if (!found)
          admix_brand.push({
            _id: adMixData._id,
            name: adMixData.name,
          })
      }

      let cgData = await CementGradeModel.findOne({
        _id: new ObjectId(result[i].cement_grade_id),
      })
      console.log("cgData", cgData)
      if (!isNullOrUndefined(cgData)) {
        const found = cement_grade.some((el: any) =>
          el._id.equals(result[i].cement_grade_id)
        )
        if (!found)
          cement_grade.push({
            _id: cgData._id,
            name: cgData.name,
          })
      }

      let adMixCatData = await AdmixtureCategoryModel.findOne({
        _id: new ObjectId(result[i].ad_mixture_category_id),
      })
      console.log("adMixCatData", adMixCatData)
      if (!isNullOrUndefined(adMixCatData)) {
        const found = admix_category.some((el: any) =>
          el._id.equals(result[i].ad_mixture_category_id)
        )
        if (!found)
          admix_category.push({
            _id: adMixCatData._id,
            name: adMixCatData.category_name,
          })
      }
    }
    console.log("cement_brand", cement_brand)
    console.log("sand_source", sand_source)
    console.log("agg_source", agg_source)
    console.log("agg1_sub_cat", agg1_sub_cat)
    console.log("agg2_sub_cat", agg2_sub_cat)
    console.log("fly_ash_source", fly_ash_source)
    console.log("admix_brand", admix_brand)
    console.log("cement_grade", cement_grade)
    console.log("admix_category", admix_category)
    return Promise.resolve({
      cement_brand,
      sand_source,
      agg_source,
      agg1_sub_cat,
      agg2_sub_cat,
      fly_ash_source,
      admix_brand,
      cement_grade,
      admix_category,
    })
  }
}
