import { injectable } from "inversify"
import {
  ICountryDoc,
  CountryModel,
  StateModel,
  IStateDoc,
  ICityDoc,
  CityModel,
  SubCityModel,
  ISubCityDoc,
} from "../../model/region.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  IndiaPostModel,
  IIndiaPinCodeDoc,
} from "../../model/india.pincode.model"
import { escapeRegExp } from "../../utilities/mongoose.utilities"
import { IaddRegion } from "./region.request-definations"

@injectable()
export class RegionRepository {
  async getCountries(
    search?: string,
    before?: string,
    after?: string
  ): Promise<ICountryDoc[]> {
    let query: { [k: string]: any } = {}

    query.country_code = "IN"

    if (!isNullOrUndefined(search) && search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(search)
      query.$or = [
        // {
        //   country_code: { $regex, $options },
        // },
        {
          country_name: { $regex, $options },
        },
      ]
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return CountryModel.find(query)
  }

  async getStates(
    country_id?: string,
    search?: string,
    before?: string,
    after?: string
  ): Promise<IStateDoc[]> {
    let query: { [k: string]: any } = {}

    query.country_code = "IN"

    if (!isNullOrUndefined(country_id) && country_id !== "") {
      query.country_id = country_id
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.state_name = {
        $regex: escapeRegExp(search),
        $options: "i",
      }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return StateModel.find(query)
  }

  async getCities(
    state_id?: string,
    search?: string,
    before?: string,
    after?: string
  ): Promise<ICityDoc[]> {
    let query: { [k: string]: any } = {}

    query.country_code = "IN"
    if (!isNullOrUndefined(state_id) && state_id !== "") {
      query.state_id = state_id
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.city_name = {
        $regex: escapeRegExp(search),
        $options: "i",
      }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return CityModel.find(query)
  }

  async getSubcities(
    city_id?: string,
    search?: string,
    before?: string,
    after?: string
  ): Promise<ISubCityDoc[]> {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(city_id) && city_id !== "") {
      query.city_id = city_id
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return SubCityModel.find(query).limit(40)
  }

  async getPincodes(): Promise<IIndiaPinCodeDoc[]> {
    return IndiaPostModel.find().limit(40)
  }

  async addRegion(regionData: IaddRegion) {
    const stateData = await StateModel.findOne({
      state_name: regionData.state_name,
    })

    const cityData = await CityModel.findOne({
      city_name: regionData.city_name,
      state_name: regionData.state_name,
    })

    // If state and city both exists.
    if (!isNullOrUndefined(stateData) && !isNullOrUndefined(cityData)) {
      return cityData
    }
    // If State is exists but city is not exists so add city data return.
    else if (!isNullOrUndefined(stateData) && isNullOrUndefined(cityData)) {
      const saveCityData = {
        state_id: stateData._id,
        city_name: regionData.city_name,
        location: regionData.location,
        state_name: stateData.state_name,
        country_id: stateData.country_id,
        country_code: stateData.country_code,
        country_name: stateData.country_name,
      }
      const newCityData = await new CityModel(saveCityData).save()
      console.log("New city data Added!")
      return newCityData

      // Add both state/city date and return.
    } else {
      const saveStateData = {
        state_name: regionData.state_name,
        country_id: 1,
        country_code: "IN",
        country_name: "India",
      }
      const newStateData = await new StateModel(saveStateData).save()
      const saveCityData = {
        state_id: newStateData._id,
        city_name: regionData.city_name,
        location: regionData.location,
        state_name: newStateData.state_name,
        country_id: newStateData.country_id,
        country_code: newStateData.country_code,
        country_name: newStateData.country_name,
      }
      console.log("Both State and city name Added!")
      const newCityData = await new CityModel(saveCityData).save()
      return newCityData
    }
  }
}
