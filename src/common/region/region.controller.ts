import { injectable, inject } from "inversify"
import { controller, httpGet, httpPost } from "inversify-express-utils"
import { CommonTypes } from "../common.types"
import { RegionRepository } from "./region.repository"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { NextFunction, Response } from "express"
import {
  IgetStatesByCountryId,
  IgetCitiesByStateId,
  IgetSubcitiesByCityId,
  IaddRegionRequest,
} from "./region.request-definations"
import { validate } from "../../middleware/joi.middleware"
import {
  getStatesByCountryId,
  getCitiesByStateId,
  getSubcitiesByCityId,
  addRegionSchema,
} from "./region.request-schema"

@injectable()
@controller("/v1/region")
export class RegionController {
  constructor(
    @inject(CommonTypes.RegionRepository) private regionRepo: RegionRepository
  ) {}
  @httpGet("/getCountries")
  async getCountries(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after } = req.query
      const data = await this.regionRepo.getCountries(search, before, after)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/getStates")
  @httpGet("/getStatesByCountryId/:countryId", validate(getStatesByCountryId))
  async getStatesByCountryId(
    req: IgetStatesByCountryId,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    try {
      const data = await this.regionRepo.getStates(
        req.params.countryId,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/getCities")
  @httpGet("/getCitiesByStateId/:stateId", validate(getCitiesByStateId))
  async getCitiesByStateId(
    req: IgetCitiesByStateId,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    try {
      const data = await this.regionRepo.getCities(
        req.params.stateId,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/getSubcities")
  @httpGet("/getSubcitiesByCityId/:cityId", validate(getSubcitiesByCityId))
  async getSubcitiesByCityId(
    req: IgetSubcitiesByCityId,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    try {
      const data = await this.regionRepo.getSubcities(
        req.params.cityId,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/getPincodes")
  async getPincodes(
    req: IgetStatesByCountryId,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.regionRepo.getPincodes()
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/", validate(addRegionSchema))
  async addRegion(req: IaddRegionRequest, res: Response, next: NextFunction) {
    try {
      const data = await this.regionRepo.addRegion(req.body)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
