import { IRequestSchema } from "../../middleware/joi.middleware"
import * as Joi from "joi"

export const getStatesByCountryId: IRequestSchema = {
  params: Joi.object().keys({
    countryId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export const getCitiesByStateId: IRequestSchema = {
  params: Joi.object().keys({
    stateId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export const getSubcitiesByCityId: IRequestSchema = {
  params: Joi.object().keys({
    cityId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export const addRegionSchema: IRequestSchema = {
  body: Joi.object().keys({
    state_name: Joi.string().required(),
    city_name: Joi.string().required(),
    location: Joi.object()
      .keys({
        type: Joi.string().default("Point"),
        coordinates: Joi.array()
          .ordered([
            Joi.number().min(-180).max(180).required(),
            Joi.number().min(-90).max(90).required(),
          ])
          .length(2)
          .required(),
      })
      .required(),
  }),
}
