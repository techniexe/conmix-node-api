import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IgetStatesByCountryId extends IAuthenticatedRequest {
  params: {
    countryId: string
  }
  query: {
    search?: string
    before?: string
    after?: string
  }
}

export interface IgetCitiesByStateId extends IAuthenticatedRequest {
  params: {
    stateId: string
  }
  query: {
    search?: string
    before?: string
    after?: string
  }
}

export interface IgetSubcitiesByCityId extends IAuthenticatedRequest {
  params: {
    cityId: string
  }
  query: {
    search?: string
    before?: string
    after?: string
  }
}

export interface IaddRegion {
  city_name: string
  state_name: string
  location: {
    type: { type: String; enum: ["Point"]; default: "Point" }
    coordinates: { type: [Number]; default: [0, 0] }
  }
}

export interface IaddRegionRequest extends IAuthenticatedRequest {
  body: IaddRegion
}
