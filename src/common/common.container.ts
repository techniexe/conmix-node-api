import "reflect-metadata"
import { Container } from "inversify"
import { TYPE } from "inversify-express-utils"
import { SessionController } from "./session/session.controller"
import { CommonTypes } from "./common.types"
import { SessionRepository } from "./session/session.repository"
import { PnfController } from "./pnf/pnf.controller"
import { TwilioService } from "../utilities/twilio-service"
import { RegionController } from "./region/region.controller"
import { RegionRepository } from "./region/region.repository"

import { ProductReviewController } from "./product-review/product-review.controller"
import { ProductReviewRepository } from "./product-review/product-review.repository"
import { CommonProductController } from "./product/common.product.controller"
import { CommonProductRepository } from "./product/common.product.repository"
import { UniqueidService } from "../utilities/uniqueid-service"
import { EmailVerifyController } from "./email-verify/email-verify.controller"
import { EmailVerifyRepository } from "./email-verify/email-verify.repository"
import { RfpController } from "./rfp/rfp.controller"
import { RfpRepository } from "./rfp/rfp.repository"
import { CommonCronController } from "./cron/common.cron.controller"
import { CommonOrderRepositroy } from "./cron/common.order.repository"
// import { CommonQuoteRepository } from "./cron/common.quote.repository"
import { SourceController } from "./source/common.source.controller"
import { SourceRepository } from "./source/common.source.repository"
import { CommonConcretePumpController } from "./concrete_pump/common.concrete_pump.controller"
import { CommonConcretePumpRepository } from "./concrete_pump/common.concrete_pump.repository"
import { CommonTMController } from "./TM/common.TM.controller"
import { CommonTMRepository } from "./TM/common.TM.repository"
import { ConcreteGradeController } from "./concrete_grade/concrete_grade.controller"
import { ConcreteGradeRepository } from "./concrete_grade/concrete_grade.repository"
import { CementBrandController } from "./cement_brand/cement_brand.controller"
import { CementBrandRepository } from "./cement_brand/cement_brand.repository"
import { CommonAggregateSandCategoryController } from "./aggregate_sand_category/aggregate_sand_category.controller"
import { CommonAggregateSandCategoryRepository } from "./aggregate_sand_category/aggregate_sand_category.repository"
import { AdmixBrandController } from "./admixture_brand/admix_brand.controller"
import { AdmixBrandRepository } from "./admixture_brand/admix_brand.repository"
import { AdmixCategoryController } from "./admixture_category/admix_category.controller"
import { AdmixCategoryRepository } from "./admixture_category/admix_category.repository"
import { CementGradeController } from "./cement_grade/cement_grade.controller"
import { CementGradeRepository } from "./cement_grade/cement_grade.repository"
import { DesignMixController } from "./design_mix/design_mix.controller"
import { DesignMixRepository } from "./design_mix/design_mix.repository"
import { CustomMixController } from "./custom_mix/custom_mix.controller"
import { CustomMixRepository } from "./custom_mix/custom_mix.repository"
import { CommonService } from "../utilities/common.service"
import { ReviewController } from "./review/common.review.controller"
import { ReviewRepository } from "./review/common.review.repository"
import { NotificationUtility } from "../utilities/notification.utility"
import { PayMethodController } from "./pay_method/pay_method.controller"
import { PayMethodRepository } from "./pay_method/pay_method.repository"
import { TestService } from "../utilities/test.service"
import { MailGunService } from "../utilities/mailgun.service"

export const commonContainer = new Container()

commonContainer
  .bind(CommonTypes.TwilioService)
  .to(TwilioService)
  .inSingletonScope()

commonContainer
  .bind(CommonTypes.UniqueidService)
  .to(UniqueidService)
  .inSingletonScope()

commonContainer
  .bind(CommonTypes.CommonService)
  .to(CommonService)
  .inSingletonScope()

commonContainer
.bind(CommonTypes.TestService)
.to(TestService)
.inSingletonScope()

commonContainer.bind(CommonTypes.mailgunService).to(MailGunService).inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(SessionController)
  .whenTargetNamed(CommonTypes.SessionController)
commonContainer
  .bind(CommonTypes.SessionRepository)
  .to(SessionRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(RegionController)
  .whenTargetNamed(CommonTypes.RegionController)

commonContainer
  .bind(CommonTypes.RegionRepository)
  .to(RegionRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(ProductReviewController)
  .whenTargetNamed(CommonTypes.ProductReviewController)
commonContainer
  .bind(CommonTypes.ProductReviewRepository)
  .to(ProductReviewRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CommonProductController)
  .whenTargetNamed(CommonTypes.CommonProductController)

commonContainer
  .bind(CommonTypes.CommonProductRepository)
  .to(CommonProductRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(EmailVerifyController)
  .whenTargetNamed(CommonTypes.EmailVerifyController)

commonContainer
  .bind(CommonTypes.EmailVerifyRepository)
  .to(EmailVerifyRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(RfpController)
  .whenTargetNamed(CommonTypes.RfpController)

commonContainer
  .bind(CommonTypes.RfpRepository)
  .to(RfpRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CommonCronController)
  .whenTargetNamed(CommonTypes.CommonCronController)

commonContainer
  .bind(CommonTypes.CommonOrderRepositroy)
  .to(CommonOrderRepositroy)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(SourceController)
  .whenTargetNamed(CommonTypes.SourceController)

commonContainer
  .bind(CommonTypes.SourceRepository)
  .to(SourceRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CommonConcretePumpController)
  .whenTargetNamed(CommonTypes.CommonConcretePumpController)

commonContainer
  .bind(CommonTypes.CommonConcretePumpRepository)
  .to(CommonConcretePumpRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CommonTMController)
  .whenTargetNamed(CommonTypes.CommonTMController)

commonContainer
  .bind(CommonTypes.CommonTMRepository)
  .to(CommonTMRepository)
  .inSingletonScope()

// commonContainer
//   .bind(CommonTypes.CommonQuoteRepository)
//   .to(CommonQuoteRepository)
//   .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(ConcreteGradeController)
  .whenTargetNamed(CommonTypes.ConcreteGradeController)

commonContainer
  .bind(CommonTypes.ConcreteGradeRepository)
  .to(ConcreteGradeRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CementBrandController)
  .whenTargetNamed(CommonTypes.CementBrandController)

commonContainer
  .bind(CommonTypes.CementBrandRepository)
  .to(CementBrandRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CommonAggregateSandCategoryController)
  .whenTargetNamed(CommonTypes.CommonAggregateSandCategoryController)

commonContainer
  .bind(CommonTypes.CommonAggregateSandCategoryRepository)
  .to(CommonAggregateSandCategoryRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(AdmixBrandController)
  .whenTargetNamed(CommonTypes.AdmixBrandController)

commonContainer
  .bind(CommonTypes.AdmixBrandRepository)
  .to(AdmixBrandRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(AdmixCategoryController)
  .whenTargetNamed(CommonTypes.AdmixCategoryController)

commonContainer
  .bind(CommonTypes.AdmixCategoryRepository)
  .to(AdmixCategoryRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CementGradeController)
  .whenTargetNamed(CommonTypes.CementGradeController)

commonContainer
  .bind(CommonTypes.CementGradeRepository)
  .to(CementGradeRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(DesignMixController)
  .whenTargetNamed(CommonTypes.DesignMixController)

commonContainer
  .bind(CommonTypes.DesignMixRepository)
  .to(DesignMixRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(CustomMixController)
  .whenTargetNamed(CommonTypes.CustomMixController)

commonContainer
  .bind(CommonTypes.CustomMixRepository)
  .to(CustomMixRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(ReviewController)
  .whenTargetNamed(CommonTypes.ReviewController)
commonContainer
  .bind(CommonTypes.ReviewRepository)
  .to(ReviewRepository)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(PayMethodController)
  .whenTargetNamed(CommonTypes.PayMethodController)
commonContainer
  .bind(CommonTypes.PayMethodRepository)
  .to(PayMethodRepository)
  .inSingletonScope()

commonContainer
  .bind(CommonTypes.NotificationUtility)
  .to(NotificationUtility)
  .inSingletonScope()

commonContainer
  .bind(TYPE.Controller)
  .to(PnfController)
  .whenTargetNamed(CommonTypes.PnfController)
