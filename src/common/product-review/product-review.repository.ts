import { ObjectId } from "bson"
import { injectable } from "inversify"

import { isNullOrUndefined } from "../../utilities/type-guards"
import { ReviewModel } from "../../model/review.model"

@injectable()
export class ProductReviewRepository {
  async getProductReview(
    sub_category_id: string,
    before?: string,
    after?: string
  ) {
    const query: { [k: string]: any } = {
      status: "published",
    }
    query.sub_category_id = new ObjectId(sub_category_id)

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "buyer._id": 1,
          "buyer.full_name": 1,
          sub_category_id: 1,
          product_id: 1,
          review_text: 1,
          rating: 1,
          status: 1,
          created_at: 1,
          modirated_at: 1,
        },
      },
    ]

    // const data = await ReviewModel.aggregate(aggregateArr).limit(40)

    // if (sort.created_at === 1) {
    //   data.reverse()
    // }
    // return Promise.resolve(data)

    const reviews = await ReviewModel.aggregate(aggregateArr).limit(40)

    if (sort.created_at === 1) {
      reviews.reverse()
    }

    //let data: any = []
    const data: { [k: string]: any } = {}
    data.write_review = true
    data.reviews = reviews

    // data.push({
    //   write_review: true,
    //   reviews,
    // })

    console.log("data", data)
    return Promise.resolve(data)
  }

  async getReviewByProduct(
    product_id: string,
    before?: string,
    after?: string,
    user_id?: string
  ) {
    const query: { [k: string]: any } = {
      status: "published",
    }
    query.product_id = new ObjectId(product_id)

    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "buyer._id": 1,
          "buyer.full_name": 1,
          sub_category_id: 1,
          product_id: 1,
          review_text: 1,
          rating: 1,
          status: 1,
          created_at: 1,
          modirated_at: 1,
        },
      },
    ]

    const reviews = await ReviewModel.aggregate(aggregateArr).limit(40)

    if (sort.created_at === 1) {
      reviews.reverse()
    }

    return Promise.resolve(reviews)
  }
}
