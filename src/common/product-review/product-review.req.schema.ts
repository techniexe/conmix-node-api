import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetProductreview extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
  }
  params: {
    subCategoryId: string
  }
}

export const GetProductreviewSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
  }),
  params: Joi.object().keys({
    subCategoryId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetReviewByProduct extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
    user_id?: string
  }
  params: {
    product_id: string
  }
}

export const GetReviewByProductSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    user_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    product_id: Joi.string().regex(objectIdRegex),
  }),
}
