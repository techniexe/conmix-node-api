import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { CommonTypes } from "../common.types"
import { NextFunction, Response } from "express"

import { validate } from "../../middleware/joi.middleware"
import {
  GetProductreviewSchema,
  IGetProductreview,
  GetReviewByProductSchema,
  IGetReviewByProduct,
} from "./product-review.req.schema"
import { ProductReviewRepository } from "./product-review.repository"

@injectable()
@controller("/v1/product-review")
export class ProductReviewController {
  constructor(
    @inject(CommonTypes.ProductReviewRepository)
    private ProductReviewRepo: ProductReviewRepository
  ) {}

  @httpGet("/getProductReview/:subCategoryId", validate(GetProductreviewSchema))
  async getProductReview(
    req: IGetProductreview,
    res: Response,
    next: NextFunction
  ) {
    const { subCategoryId } = req.params
    const { before, after } = req.query
    try {
      const data = await this.ProductReviewRepo.getProductReview(
        subCategoryId,
        before,
        after
      )

      console.log(data)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:product_id", validate(GetReviewByProductSchema))
  async getReviewByProduct(
    req: IGetReviewByProduct,
    res: Response,
    next: NextFunction
  ) {
    const { product_id } = req.params
    const { before, after, user_id } = req.query

    try {
      const data = await this.ProductReviewRepo.getReviewByProduct(
        product_id,
        before,
        after,
        user_id
      )
      console.log(data)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
