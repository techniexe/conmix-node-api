import { injectable } from "inversify"
import * as AWS from "aws-sdk"
import { generateSMSMessage } from "./twilio-utilities"
import { SmsLogModel } from "../model/sms.log.model"
import { isNullOrUndefined } from "./type-guards"

import * as request from "request"

const sms91Authkey = process.env.sms91Authkey || ""
const sms91Senderid = process.env.sms91Senderid || "CONMAT"

export interface ISendSms {
  user_id?: string
  user_type?: string
  to_mobileNumber: string
  text: string
  sent_at: Date
  send_status: string
  send_error?: string
}

@injectable()
export class TwilioService {
  sns: AWS.SNS
  constructor() {
    this.sns = new AWS.SNS({
      region: "ap-south-1",
      apiVersion: "2010-03-31",
    })
  }

  // private invokeFunction(payload: string) {
  //   return this.sns
  //     .publish({
  //       Message: payload,
  //       TopicArn: "arn:aws:sns:ap-south-1:595508262098:send_auth_code",
  //     })
  //     .promise()
  // }

  sendCode(target: string, code: string | number, method: "sms" | "call") {
    const payload = generateSMSMessage(code)
    return this.sendSMSUsingMSG91(target, payload)
    // return this.invokeFunction(JSON.stringify({ target, method, payload }))
  }

  async sendSMS(target: string, payload: string, smsData: ISendSms) {
    const data = await this.addSmsLog(smsData)
    try {
      await this.sendSMSUsingMSG91(target, payload)
      // await this.invokeFunction(
      //   JSON.stringify({ target, payload, method: "sms" })
      // )
      await this.updateSmsLog(data._id, "sent")
    } catch (err) {
      await this.updateSmsLog(data._id, "error", err.message)
      return Promise.resolve()
    }
  }

  async addSmsLog(smsData: ISendSms) {
    const newDoc = {
      user: {
        user_type: smsData.user_type,
        user_id: smsData.user_id,
      },
      to_mobileNumber: smsData.to_mobileNumber,
      text: smsData.text,
    }

    return new SmsLogModel(newDoc).save()
  }

  async updateSmsLog(logId: string, send_status: string, error?: string) {
    const $set: { [k: string]: any } = { send_status }
    if (!isNullOrUndefined(error)) {
      $set.send_error = error
    }
    return SmsLogModel.findOneAndUpdate({ _id: logId }, $set)
  }

  async sendSMSUsingMSG91(to: string, message: string) {
    let unicode = 0
    if (/[^\u0000-\u00ff]/.test(message)) {
      unicode = 1
    }
    const url = `https://api.msg91.com/api/sendhttp.php?mobiles=${encodeURIComponent(
      to
    )}&authkey=${sms91Authkey}&route=4&sender=${sms91Senderid}&message=${encodeURIComponent(
      message
    )}&country=91&unicode=${unicode}&DLT_TE_ID=1207161820673491057&dev_mode=1`

    return new Promise((resolve, reject) => {
      request.post(
        url,
        {
          forever: true,
        },
        function (err: any, res, body: any) {
          if (err) {
            console.log(err)
            return reject(err)
          }
          resolve()
        }
      )
    })
  }
}
