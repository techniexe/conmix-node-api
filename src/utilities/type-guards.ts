export function isNullOrUndefined(val: any): val is null | undefined {
  return val === undefined || val === null
}

export function isVoid(val: any): val is void {
  return val === undefined || val === null
}

export function escapeRegExp(str: string) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") // $& means the whole matched string
}
