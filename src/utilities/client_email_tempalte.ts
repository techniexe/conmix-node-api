export const client_mail_template = {
    welcome: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Customer Welcome</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#fff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100"
                                                style="display:block;width:100%;max-width:300px;"
                                                alt="img"
                                                src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/Welcome_Email.png">
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;line-height: 36px;">
                                                <b style="display: block;">Hi {{client_name}},</b>
                                                <b>welcome to CONMIX!</b>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <p>An advanced E-Commerce Website for the purchase of Ready Mix Concrete.  You can now buy RMC online anytime at Conmix and manage your concrete pours efficiently at site. </p> <br><br>
                                                <p>Stay away from daily hassles of calling the RMC supplier to check on the order status and delivery of the concrete at your site. You can place Custom Mix Order, if not the Standard Mix,  with your own Design Mix and get it delivered right at your site from your chosen RMC supplier.</p><br><br>
                                                <p>You can also track your order anytime on the Web and Mobile App.</p>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 24px;color: #282828;">
                                                Thank you for registration!
                                              </td>
                                            </tr>
                                         
                                            <tr>
                                              <td height="25">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0"
                                                  cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com"
                                                          target="_blank" style="color: #ffffff">GO
                                                        TO SITE</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    one_time_pwd: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others. </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    registration_otp: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Client,</strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is your OTP for registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others. You can order the RMC product of your choice one you are registered on Conmix.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    forgot_password_otp: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is your OTP to Reset Password at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others. Make sure you remember the new password to access Conmix in future.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    password_updated_successfully: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Password Update Successful</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_forgotpwd.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Password Update Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}}, </strong> You have successfully updated your password for login on Conmix. Kindly save it for future use.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                           
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;">You can now continue ordering your RMC - Custom Mix or Standard Mix as per the requirement. Log in to Conmix Mobile App or website now !!</td>
                                            </tr>
    
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    password_updated_successfully_site: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Password Update Successful</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_forgotpwd.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Password Update Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{site_person_name}}, </strong> Your Login Password on Conmix has been updated. Kindly contact {{client_name}} for the updated password.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            // </tr>
                                            
                                           
                                            //  <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;">You can now continue ordering your RMC - Custom Mix or Standard Mix as per the requirement. Log in to Conmix Mobile App or website now !!</td>
                                            // </tr>
    
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    new_order: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>New Order</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">New Order Placed</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;"><strong>Dear {{client_name}}, </strong>
                                                <br>Thanks for using Conmix. You have successfully placed an order with an Order ID <strong><span style="font-size: 15px">'{{orderData.display_id}}' </span></strong>
                                                for your <span style="font-size: 14px;color: #146e9d"> '{{orderData.site_name}}' </span>  site.  Kindly login to Conmix App to track your order online.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_delieverd: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Delivered</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been delivered</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{client_name}},</strong> Your product id {{orderItem.display_item_id}} having an Order ID {{orderDoc.display_id}} for your {{site_name}} site has been fully delivered, as per the Buyer purchase policy, and completed by RMC Supplier {{vendor_company_name}}. Kindly share your feedback here 'weblink'</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>TRACK YOUR ORDER</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                    
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_buyer: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Cancelled</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been cancelled</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{client_name}},</strong> You have cancelled an entire order having an Order ID {{display_id}}. You may now login to your Conmix account to place a new order for your RMC requirements.
                                                <br>
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;"> You may again try to place the order after some time as per your requirement.</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_supplier: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Cancelled</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been cancelled</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
    
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{client_name}},</strong> Sorry to inform you that your placed order with Product Id {{data.buyer_order_display_item_id}} having an Order ID {{data.buyer_order_display_id}} has not been accepted by any RMC Supplier. Thus your placed order is cancelled with No Action.
                                                <br>
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
    
    
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_buyer_payment_refund: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> Your payment refund for the cancelled order with Product Id {{orderItemDoc.display_item_id}} having an Order ID {{display_id}} will be processed as per our Buyer Purchase Policy.
                                                <br><br>
                                                Kindly refer to the policy and contact Conmix Customer Care for more information.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                  <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_supplier_payment_refund: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> Your payment refund for the order with Product Id {{data.buyer_order_display_item_id}} having an Order ID {{data.buyer_order_display_id}} is cancelled as no RMC Supplier has accepted the order. Payment refund will be processed as per our Buyer Purchase Policy.
                                                <br><br>
                                                Kindly refer to the policy and contact Conmix Customer Care for more information.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                  <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    material_rejected_buyer_side_payment_refund: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> Your payment refund for the rejected material as supplied by RMC Supplier {{sub_vendor_company_name}} for the Product Id {{OrderItemData.display_item_id}} having an Order ID {{display_id}} will be processed as per our <span style="color: red">Material Rejection & Refund Policy.</span>
                                                <br><br>
                                                Kindly refer to the policy and contact Conmix Customer Care for more information.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                  <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    material_rejected_buyer_side: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Rejected</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been Rejected.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{client_name}},</strong> You have rejected the material delivered to your site {{site_name}} for the Product Id {{OrderItemData.display_item_id}} having an Order ID {{display_id}}
                                                <br><br>
                                                We will intimate and take action against the RMC Supplier {{master_vendor_company_name}} as per our Supplier/Vendor Policy
                                              </td>
    
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    payment_successfully: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Successful </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your payment was successful </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                               <strong>Dear {{client_name}},</strong>  Thank You. We have received the payment for your order having an Order ID {{orderData.display_id}}
                                                <br>
                                                Your order has been sucessfully placed and is waiting for confirmation from the RMC Supplier.
                                                <br>
                                                You may check the order status to plan the Concrete Pour at your site accordingly.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    payment_failed: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_payment_faield.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your payment couldn not be processed</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{client_name}},</strong> Payment attempt by you has failed and order has not been placed. Kindly retry.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                 <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    tm_assigned: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Assign Transit Mixer </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_truck.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Assign Transit Mixer</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif; font-size: 14px;color: #282828;line-height: 24px;"><strong>Dear {{person_name}},</strong> Kindly note that the RMC supplier {{vendor_company_name}} has assigned a TM for {{trackData.pickup_quantity}} Cum qty for your Product Id {{VendorOrderData.buyer_order_display_item_id}} having an Order ID {{VendorOrderData.buyer_order_display_id}} for your {{site_name}} site. You can also track your order online.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>Track Order</u></a></td>
                                            </tr> -->
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    cp_assigned: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Assign Concrete Pump</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_pump.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Assign Concrete Pump</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 28px;"><strong>Dear {{site_person_name}},</strong> Kindly note that the RMC supplier {{vendor_company_name}} has assigned a CP for your Product Id {{VendorOrderData.buyer_order_display_item_id}} having an Order ID {{VendorOrderData.buyer_order_display_id}} for your {{site_name}} site. You can also track your order online.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    ideal_client: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Idle User</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_user.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Idle User Account</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{client_name}},</strong> Thank you for registration at Conmix. Kindly explore the custom mix (RMC) solutions offered by Conmix at affordable rates and faster delivery. You can also track your RMC order online to plan the pour at site in time !! </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                          
                                            
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px; ">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    accept_order_by_supplier: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Accept Order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;"><strong>Dear {{client_name}},</strong> <br>Your order for Product Id {{vendorOrderData.buyer_order_display_item_id}} having an Order ID {{vendorOrderData.buyer_order_display_id}} has been accepted and thus confirmed by the RMC Supplier {{vendor_company_name}}.
                                                <br>Kindly login to Conmix Website and assign RMC quantity with dates as per your site requirement.
                                                <br>You may then track the order online to plan your concrete pour at site.
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.google.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_fully_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Completion of entire order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;">
                                                <strong>Dear {{client_name}},</strong> <br>Your order having an Order ID {{orderDoc.display_id}} has been fully delivered and completed by RMC Suppliers.
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.google.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    product_partially_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Product partially delivered in an order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;">
                                                <strong>Dear {{client_name}},</strong> <br>This is to inform you that your Product Id {{orderItemData.display_item_id}} having an Order ID {{orderDoc.display_id}} for your site {{site_name}} has been partially delivered by RMC Supplier {{vendor_company_name}}. <br/>
                                                Kindly login to Conmix Website to plan for concrete pour at site.
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.google.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    product_cancelled: `/<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Product Cancelled in an Order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;">
                                                <strong>Dear {{client_name}},</strong> <br>You have cancelled an order for Product Id {{orderItemDoc.display_item_id}} having an Order Id {{display_id}} placed to RMC Supplier {{sub_vendor_company_name}}. Your order is thus partially cancelled. <br/>
                                                Kindly be infromed that you can cancel your order partially or fully before the order is accepted and confirmed by RMC Suppliers.
                                              </td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.google.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`
}