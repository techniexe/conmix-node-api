import { injectable } from "inversify"
import { FcmTokensModel, IFcmTokensDoc } from "../model/fcm-tokens.model"

import { ObjectId } from "bson"
import { IFcmTokenDetails } from "../buyer/fcm-tokens/fcm-tokens.request-schema"

@injectable()
export class FcmTokenUtility {
  async addUpdateToken(
    user_id: string,
    tokenData: IFcmTokenDetails,
    user_type: string
  ) {
    await FcmTokensModel.findOneAndUpdate(
      { user_id, device_id: tokenData.deviceID },
      {
        $set: {
          user_id,
          token: tokenData.token,
          device_id: tokenData.deviceID,
          platform: tokenData.platform,
          user_type,
        },
        $setOnInsert: {
          created_at: Date.now(),
        },
      },
      { upsert: true }
    ).exec()
    return Promise.resolve()
  }

  async deleteByDeviceID(
    user_id: string,
    device_id: string
  ): Promise<IFcmTokensDoc | null> {
    return FcmTokensModel.findOneAndRemove({
      user_id,
      device_id,
    })
  }

  async removeExpiredTokens(user_id: string | ObjectId, tokens: string[]) {
    return Promise.all(
      tokens.map((token) => FcmTokensModel.deleteOne({ user_id, token }))
    )
  }
  async handleFailedAPNsToken(
    apnResp: {
      device: string
      error?: Error
      status?: string
      response?: {
        reason: string
        timestamp?: string
      }
    }[],
    tokenId: ObjectId | string
  ) {
    apnResp.map((r) => {
      console.log(
        `APNs Failed. status: ${r.status}, reason: ${
          r.response && r.response.reason
        }`,
        r.status
      )
    })
    // return Promise.resolve()
    if (
      apnResp.length > 0 &&
      apnResp[0].status &&
      ["400", "410"].includes(apnResp[0].status)
    ) {
      console.log(`Deleting failed APN tokens: _id: ${tokenId}`)
      return FcmTokensModel.deleteOne({ _id: tokenId })
    }
    return Promise.resolve()
  }
}
