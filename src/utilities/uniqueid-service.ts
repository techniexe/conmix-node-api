import { injectable } from "inversify"
import { UniqueidModel } from "../model/uniqueid.model"
import { CouponModel } from "../model/coupon.model"

@injectable()
export class UniqueidService {
  randNumber() {
    return Math.ceil(1000 + Math.random() * 9000)
  }
  async getUniqueOrderid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "order" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueOrderItemId(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "order_item" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }
  async getUniqueInvoiceid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "invoice" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }
  async getUniqueQuoteid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "quote" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueLogisticsOrderId(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "logistics_order" },
      { $inc: { last_num: 1 } },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueSupplierOrderId(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "supplier_order" },
      { $inc: { last_num: 1 } },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueTicketid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "support_ticket" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueBuyerid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "buyer" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueSupplierid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "supplier" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }

  async getUniqueLogisticsid(): Promise<string> {
    const { last_num } = await UniqueidModel.findOneAndUpdate(
      { _id: "logistics_user" },
      {
        $inc: { last_num: 1 },
      },
      { new: true, upsert: true }
    )
    return Promise.resolve(
      `${this.randNumber()}${last_num}${this.randNumber()}`
    )
  }
  async random_string_without_O(): Promise<string> {
    const list = "ABCDEFGHIJKLMNPQRSTUVWXYZ"
    var res = ""
    for (var i = 0; i < 6; i++) {
      var rnd = Math.floor(Math.random() * list.length)
      res = res + list.charAt(rnd)
    }
    const data = await CouponModel.findOne({
      code: res,
    })
    if (data === null) {
      return res
    }
    return this.random_string_without_O()
  }
}
