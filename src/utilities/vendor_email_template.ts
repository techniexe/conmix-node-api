export const vendor_mail_template = { 
    welcome:   `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Customer Welcome</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#fff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100"
                                                style="display:block;width:100%;max-width:300px;"
                                                alt="img"
                                                src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/Welcome_Email.png">
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;line-height: 36px;">
                                                <b style="display: block;">Dear {{createBody.company_name}},</b>
                                                <b>welcome to CONMIX!</b>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <p>an advanced E-Commerce Website to market and sell your Ready Mix Concrete.  You can now register and list your RMC products at Conmix for more business. </p> <br><br>
                                                <p>With Conmix, you can manage your RMC production, material stock and dispatches efficiently. </p><br><br>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 24px;color: #282828;">
                                                Thank you for registration!
                                              </td>
                                            </tr>
                                         
                                            <tr>
                                              <td height="25">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0"
                                                  cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com"
                                                          target="_blank" style="color: #ffffff">GO
                                                        TO SITE</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    registration_otp: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Vendor, </strong> <span style="font-size: 18px;font-weight: 700;">{{authData.code}}</span> is your OTP for registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others. You may now add your RMC Plant and Standard RMC Design Mix to start business at Conmix.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    forgot_pwd_otp: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Vendor,</strong> <span style="font-size: 18px;font-weight: 700;">{{authData.code}}</span> is an OTP to Reset Password for your Partner Account at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others. </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    password_updated_successfully: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Password Update Successful</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_forgotpwd.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Password Update Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{doc.company_name}}, </strong> You have successfully updated the login password for your Partner Account at Conmix. </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                           
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;">Kindly save your updated password and login today for more business !!</td>
                                            </tr>
    
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    new_order:`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>New Order</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">New Order Placed</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;"><strong>Dear {{mastervendorData.company_name}}, </strong>
                                                <br>Congratulations ! You have received an order with Product Id <strong><span style="font-size: 15px">{{buyer_order_display_item_id}}</span></strong> having an Order ID <strong><span style="font-size: 15px">{{order_id}}</span></strong> at Conmix. Kindly login to your Partner Account to Accept the Order.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Delivered</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been delivered</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{vendor_company_name}},</strong> You have now fully delivered an order for your client {{client_name}} for its {{site_name}} site with Product Id {{display_item_id}} having an Order ID {{vendorOrderDoc._id}}. Kindly login to your Partner Account for more information.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>TRACK YOUR ORDER</u></a></td>
                                            </tr> -->
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                    
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    one_time_pwd: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Vendor,</strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is an OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others. </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    one_time_pwd_with_vendorName: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Vendor,</strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is an OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others. </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_buyer: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Canceled</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Order has been canceled.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{vendor_name}},</strong> Sorry. Your client {{client_name}} has cancelled their order with Product Id {{orderItemDoc.display_item_id}} having an Order ID {{vendorOrderDoc._id}}.
                                                <br>
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    material_rejected_buyer_side: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Rejected</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been Rejected.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear {{vendor_name}}, </strong> Your client {{client_name}} has rejected the RMC material supplied by you for their order with Product Id {{OrderItemData.display_item_id}} having an Order ID {{display_id}}.
                                                <br><br>
                                                Kindly do not supply such Poor Quality Material if any. You have now been warned for such action by our client.
                                                <br><br>
                                                Kindly be advised your account will be blocked/blacklisted upon receiving 3 nos. of warnings and all your dues shall be forfeited as per our Supplier/Vendor Policy.
                                                <br><br>
                                                Kindly refer to the Supplier/Vendor policy here 'weblink'.
                                              </td>
    
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    tm_assigned: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Assign Transit Mixer </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_truck.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Assign Transit Mixer</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif; font-size: 14px;color: #282828;line-height: 24px;"><strong>Dear {{vendor_company_name}}, </strong> You have assigned a TM for {{trackData.pickup_quantity}} Cum qty for Product Id {{VendorOrderData.buyer_order_display_item_id}} having an Order ID {{VendorOrderData._id}} for your Client {{client_name}} to be delivered to their {{site_name}} site.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>Track Order</u></a></td>
                                            </tr> -->
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    cp_assigned: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Assign Concrete Pump</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_pump.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Assign Concrete Pump</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 28px;">
                                                <strong>Dear {{vendor_company_name}}, </strong> You have assigned a CP for {{trackData.pickup_quantity}} Cum qty for Product Id {{VendorOrderData.buyer_order_display_item_id}} having an Order ID {{VendorOrderData._id}} for your Client {{client_name}} to be delivered to their {{site_name}} site.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    ideal_vendor: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Idle User</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_user.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Idle User Account</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{vendorDoc.full_name}},</strong> Thank you for the Partner Account Registration at Conmix. Kindly explore the Conmix website for more business.</td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                You can now sell, manage your RMC supply and plan for efficient deliveries. Login to your Conmix partner account now and accept the orders to avoid deactivation of your account.</td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                          
                                            
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px; ">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_deactivate: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_user.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Account Deactive</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr >
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 24px;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong> <br/>
                                                Sorry. Your Partner Account has been deactivated at Conmix. Kindly check your registered email and contact Conmix Customer Care for more information
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_activated: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_user.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Account Re-Active Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr >
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 24px;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong> <br/>
                                                Congratulations !! We are glad to inform you that your Partner Account has been successfully re-activated at Conmix.<br/>
                                                You may now login and continue your business with Conmix !<br/>
                                                Kindly check your registered email or contact Conmix Customer Care for more information.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    vendor_block: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_user.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Account Block</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr >
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 24px;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong> <br/>
                                                This is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    accept_order: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Accept Order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr >
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 24px;">
                                                <strong>Dear {{vendor_company_name}},</strong> <br/>
                                                You have accepted and thus confirmed the order with Product Id {{vendorOrderData.buyer_order_display_item_id}} having an Order ID {{vendorOrderData.buyer_order_display_id}} for your Client {{client_name}}.<br/>
                                                This order is to be delivered at their {{site_name}} site.<br/>
                                                Kindly login to your partner account to plan for the delivery.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    surprise_order: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Surprise Order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendor_company_name}},</strong><br/>
                                                You have received a Surprise Order for Product Id {{orderData.buyer_order_display_item_id}} having an Order Id {{orderData._id}}. Kindly login now to Accept the order. <br>
                                                This Surprise order will be available for acceptance only for 30 minutes on first come first served basis. <br>
                                                If you fail to accept the order first then you may lose the chance to accept the order. <br>
                                                Kindly refer to the Vendor/Supplier/Partner Policy for more information.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_fully_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Completion of entire order</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendor_company_name}},</strong><br/>
                                                You have now fully delivered and completed an entire order for Product Id {{orderItem.display_item_id}} having an Order ID {{vendorOrderDoc._id}} for your client {{client_name}} as per the Vendor/Supplier/Partner Policy. <br>
                                                Kindly login to your Conmix Partner account for more details.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    warning: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Warning</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendor_company_name}},</strong><br/>
                                                You have received the warning for not complying to the Conmix Vendor/Supplier/Partner Policy for the order placed by your client {{client_name}} with Product Id {{OrderItemData.display_item_id}} having an Order ID {{vendor_order_id}}.
                                                Your account will be blocked and upon 3 nos. of such warnings and your dues shall not be paid.<br/>
                                                You may kindly refer to the Vendor/Supplier/Partner policy for more information.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    product_partially_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Product partially delivered</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendor_company_name}},</strong><br/>
                                                You have partially delivered the order with Product Id {{orderItemData.display_item_id}} having an Order ID {{vendorOrderData._id}} for your client {{client_name}}.<br/>
                                                Kindly login to your Conmix Partner account for more details.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_verified: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Details Verified by Admin</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong><br/>
                                                Your account registration detail has been successfully verified by Conmix Admin. You may now add your RMC Plant details in your registered partner account. <br/>
                                                Kindly contact Conmix Sales Department for more information on the account set up.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_rejected: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_inactiveuser.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Detail Rejected by Admin</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong><br/>
                                                Your vendor details as submitted by you have been rejected by Conmix Admin.<br/>
                                                You need to re-submit the vendor details for verification and approval. <br/>
                                                Kindly contact Conmix Customer Care for more information.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    plant_block: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_block.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">RMC Plant Blocked by Admin </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">
                                                <strong>Dear {{vendorUserData.company_name}},</strong><br/>
                                                Your RMC Plant {{addressData.business_name}} has been blocked by Conmix Admin as per our Vendor/Supplier/Partner Policy. <br/>
    Kindly refer to the polciy and contact Conmix Customer Care for more information.
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`
}