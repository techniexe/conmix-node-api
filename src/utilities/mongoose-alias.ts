/**
 * this module can be used with mongoose-autopopulate to rename fields virtually
 */
import * as Mongoose from "mongoose"
import { isArray } from "util"
export function mongooseAlias(schema: Mongoose.Schema) {
  let aliasFld: { [k: string]: any } = {}
  for (let [key] of Object.entries(schema.obj)) {
    const checkObj = isArray(schema.obj[key])
      ? schema.obj[key][0]
      : schema.obj[key]
    if (checkObj.alias && checkObj.autopopulate) {
      aliasFld[key] = checkObj.alias
    }
  }
  schema.set("toJSON", {
    // getters: true, // if enable will add extra id field
    // virtuals: true,
    transform: (tDoc: any, tRec: any, tOpt: any) => {
      for (let [k, v] of Object.entries(aliasFld)) {
        tRec[v] = tRec[k]
        delete tRec[k]
      }
      return tRec
    },
  })
  schema.set("toObject", {
    // getters: true, // if enable will add extra id field
    // virtuals: true,
    transform: (tDoc: any, tRec: any, tOpt: any) => {
      for (let [k, v] of Object.entries(aliasFld)) {
        tRec[v] = tRec[k]
        delete tRec[k]
      }
      return tRec
    },
  })
}
