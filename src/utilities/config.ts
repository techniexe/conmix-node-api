export const emailTemplateTypes = [
  //do not remove any items from this list
  "welcome_user",
  "new_order",
  "new_invoice",
  "payment_received",
  "item_dispatched",
  "item_delivered",
  "email_verification",
  "product_complaint",
  "accept_quote",
  "supplier_new_order",
  "forgot_password",
]
export const allowedAttachmentTypes = [
  { ext: ".jpg", mime: "image/jpg" },
  { ext: ".jpeg", mime: "image/jpeg" },
  { ext: ".png", mime: "image/png" },
  { ext: ".gif", mime: "image/gif" },
  { ext: ".pdf", mime: "application/pdf" },
]

const baseUrl = process.env.BASE_URL || "http://localhost:3030/v1/"

export const emailVerificationLinkURI = `${baseUrl}emailVerify/`

export const adminEmails = [
  {
    name: "Dhwani",
    email: "dhwani.kothari@techniexe.com",
    mobile_number: "+919999999999",
  },
]

export const complaintTypes = [
  "wrong product deliever",
  "product quality is too low",
  "delayed delievery",
  "other",
]

export const monthsCode = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
]

export enum PaymentStatus {
  PAID = "PAID",
  UNPAID = "UNPAID",
}

export enum GstTypes {
  INTERSTATE = "INTERSTATE",
  INTRASTATE = "INTRASTATE",
}

export const SmsLanguage = ["ENGLISH", "HINDI/GUJARATI"]

export const SmsTemplateTypes = [
  "OTP",
  "PAYMENT_RECEIVED",
  "ORDER_ACCEPTED",
  "NEW_QUOTE",
]

export const SmsEncodings = ["GSM-7", "ASCII"]

export const snsARN = {
  devlopment: "arn:aws:sns:ap-south-1:595508262098:material_queue_dev",
  production: "arn:aws:sns:ap-south-1:595508262098:material_queue_prod",
}

export const twilioConfig = {
  accountSID: "AC6c57cd8a262ebe9a8a88a5c902b2922b",
  authToken: "7f9861fe918380bf158cbbf4cdf6be8b",
  twilioNumber: "+17035963626",
}

export enum EventType {
  SEND_SMS = "SEND_SMS",
  PROCESS_PAID_ORDER = "PROCESS_PAID_ORDER",
  SEND_SMS_USING_TEMPLATE = "SEND_SMS_USING_TEMPLATE",
  ACCEPT_QUOTE = "ACCEPT_QUOTE",
  SEND_MAIL_USING_TEMPLATE = "SEND_MAIL_USING_TEMPLATE",
}

export enum UserType {
  ADMIN = "admin",
  VENDOR = "vendor",
  BUYER = "buyer",
}

export const awsConfig = {
  s3BucketRegion: process.env.s3BucketRegion || "ap-south-1",
  s3MediaBucket: process.env.s3MediaBucket || "tec-conmix",
  mediaBaseURL:
    process.env.STATIC_BASE_URL || "https://d2tlw91f21wjoy.cloudfront.net",
  dynamicCdnBaseURL: "https://dpumhldtcfgc0.cloudfront.net",
}

export enum SupportTicketStatus {
  "OPEN" = "OPEN",
  //"INPROCESS" = "INPROCESS",
  "CLOSED" = "CLOSED",
  //"SOLVED" = "SOLVED",
}

export const QuestionTypes = [
  "Problem on payment",
  "Problem on product",
  "Enable to order",
  "Request for product category",
  "Other",
]

export const Severity = ["High", "Urgent", "Low", "Normal"]

export enum OrderItemStatus {
  // PROCESSING = "PROCESSING",
  // PROCESSED = "PROCESSED",
  // PICKUP = "PICKUP",
  // DELIVERED = "DELIVERED",
  // CANCELLED = "CANCELLED",
  // REJECT = "REJECT",
  // REFUNDED = "REFUNDED",
  // TRUCK_ASSIGNED = "TRUCK_ASSIGNED",
  // DELAY = "DELAY",

  PLACED = "PLACED",
  PROCESSING = "PROCESSING",
  CONFIRMED = "CONFIRMED",
  TM_ASSIGNED = "TM_ASSIGNED",
  PICKUP = "PICKUP",
  DELAYED = "DELAYED",
  DELIVERED = "DELIVERED",
  CANCELLED = "CANCELLED",
  REJECTED = "REJECTED",
  LAPSED = "LAPSED"
}

export const deliveryRangeForVehicle = {
  min: 50,
  max: 700,
}
export const maxVehicleToPickupDistance = 200 // in km

export const maxVehicleMarginRate = 5 // in percentage
export const discountTypes = {
  fix: "fix",
  percentage: "percentage",
}

export const minOrder = 10 //in MT

export const quoteProcessingIntervel = 24 //in hours

export const supplierOrderProcessingInterval = 1 // in hour
export const surpriseOrderIntervel = 30 // in min

export enum ProposalStatus {
  "Unverified" = "Unverified",
  "Verified" = "Verified",
}

export enum BillStatus {
  ACCEPTED = "ACCEPTED",
  PENDING = "PENDING",
  REJECTED = "REJECTED",
}

export enum RegionType {
  PLAIN = "PLAIN",
  HILLY = "HILLY",
}

export enum Platforms {
  ANDROID = "android",
  IOS = "ios",
  WEB = "web",
}

// export const apnConfig = {
//   keyId: "7FNZ75NMX8",
//   teamId: "F35336M4BU",
//   topic: "com.yippeeinfonet.yippeeapp",
// }

export const apnConfig = {
  keyId: "KRDBF8852V",
  teamId: "44QZ64T4B6",
  topic: "com.conmix.app",
}
export const deliveryRange = {
  min: 50,
  max: 700,
}

// export const snsARNs = {
//   IMAGE_MANIPULATION: "arn:aws:sns:ap-south-1:309433714429:yippee_resize_image",
//   IMAGE_MANIPULATION_DEV:
//     "arn:aws:sns:ap-south-1:309433714429:yippee_resize_image_dev",
//   // CREATE_COLLAGE: "arn:aws:sns:ap-south-1:309433714429:yippee-collage",
//   CREATE_VIDEO: "arn:aws:sns:ap-south-1:309433714429:yippee-video",
//   COMPRESS_VIDEO: "arn:aws:sns:ap-south-1:309433714429:yippee-video",
//   CREATE_VIDEO_DEV: "arn:aws:sns:ap-south-1:309433714429:yippee-video-dev",
//   COMPRESS_VIDEO_DEV: "arn:aws:sns:ap-south-1:309433714429:yippee-video-dev",
//   SEND_SMS: "arn:aws:sns:ap-south-1:309433714429:send_sms",
//   SEND_SMS_DEV: "arn:aws:sns:ap-south-1:309433714429:send_sms_dev",
//   SEND_EMAIL: "arn:aws:sns:ap-south-1:309433714429:send_email",
//   SEND_EMAIL_DEV: "arn:aws:sns:ap-south-1:309433714429:send_email_dev",
//   PUSH_NOTIFICATION: "arn:aws:sns:ap-south-1:309433714429:pushNotification",
// }

export const message = {
  AvailableTM: "TM Available",
  NotAvailableTM: "No TM Available",
  NotAvailableCP: "No CP Available",
  AvailableCP: "CP Available",
}

export const transcationFeesPer = 2

export const razor_key = "rzp_test_OLwi971Xr4Ow5u"
export const razor_secret = "gBb5F7uIY560BuO3A83WSVLB"
export const senderId = "CONMX"
