import { injectable } from "inversify"
import * as AWS from "aws-sdk"
//import { logger } from "../logger"
import { snsARN } from "./config"

interface IPublishMessage {
  event_type: string
  event_data: { [k: string]: any }
  [k: string]: any
}

@injectable()
export class SnsService {
  sns: AWS.SNS
  snsARN: string
  constructor() {
    if (process.env.NODE_ENV === "production") {
      this.snsARN = snsARN.production
    } else {
      this.snsARN = snsARN.devlopment
    }
    this.sns = new AWS.SNS({
      region: "ap-south-1",
      apiVersion: "2010-03-31",
    })
  }
  publishMessage(data: IPublishMessage) {
    //logger.info("Currently publishing is disabled. Data:", data)
    console.log(JSON.stringify(data))
    return Promise.resolve()
    // return this.sns
    //   .publish({
    //     Message: JSON.stringify(data),
    //     TopicArn: this.snsARN,
    //   })
    //   .promise()
  }
}
