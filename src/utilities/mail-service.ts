import { injectable } from "inversify"
import * as Mustache from "mustache"
import { Transporter, createTransport, SendMailOptions } from "nodemailer"
import * as smtpTransport from "nodemailer-smtp-transport"
import { isNullOrUndefined } from "./type-guards"
import { InvalidInput } from "./customError"
import {
  IEmailTemplateDoc,
  EmailTemplateModel,
} from "../model/email.template.model"
import { EmailLogModel } from "../model/email.log.model"
import * as pdf from "html-pdf"

export interface IViewObject {
  subject?: any
  html?: any
}

export interface IParsedTemplate extends IEmailTemplateDoc {
  parsedSubject: string
  parsedHtml: string
}
export interface ISendMail {
  user_id?: string
  user_type?: string
  to_email: string
  to_name?: string
  from_email: string
  from_name?: string
  html: string
  subject: string
  cc?: string
  bcc?: string
  headers?: [{ header_name: string; header_value: string }]
  sent_at: Date
  send_status: string
  send_error?: string
  [k: string]: any
}

@injectable()
export class MailService {
  private opts: smtpTransport.SmtpOptions
  public mailTransport: Transporter
  constructor() {
    this.opts = {
      host: process.env.SMTP_HOST || "localhost",
      port: parseInt("" + process.env.SMTP_PORT, 10) || 25,
      auth: {
        user: process.env.SMTP_USER || "",
        pass: process.env.SMTP_PASS || "",
      },
    }
    this.mailTransport = createTransport(smtpTransport(this.opts))
  }
  async sendMailUsingTemplate(
    template_name: string,
    templateData: IViewObject,
    receiverInfo: { full_name: string; email: string },
    attachement?: { filename: string; path?: string; content?: any }
  ) {
    const emailTemplt = await this.getRenderedTemplate(
      template_name,
      templateData
    )

    console.log("emailTemplt", emailTemplt)
    const mailOpt: SendMailOptions = {}
    mailOpt.subject = emailTemplt.parsedSubject
    mailOpt.html = emailTemplt.parsedHtml
    mailOpt.to = `"${receiverInfo.full_name}" ${receiverInfo.email}`
    mailOpt.from = emailTemplt.from_email

    if (!isNullOrUndefined(attachement)) {
      mailOpt.attachments = [attachement]
    }
    if (!isNullOrUndefined(emailTemplt.from_name)) {
      mailOpt.from = `"${emailTemplt.from_name}" ${emailTemplt.from_email}`
    }
    if (!isNullOrUndefined(emailTemplt.cc)) {
      mailOpt.cc = emailTemplt.cc
    }
    if (!isNullOrUndefined(emailTemplt.bcc)) {
      mailOpt.bcc = emailTemplt.bcc
    }
    if (!isNullOrUndefined(emailTemplt.headers)) {
      mailOpt.headers = emailTemplt.headers.map((i: any) => {
        return { key: i.header_name, value: i.header_value }
      })
    }

    console.log(mailOpt)
    return this.mailTransport.sendMail(mailOpt)
  }

  // async getRenderedTemplate(template_type: string, view: IViewObject) {
  //   const emailDoc = await EmailTemplateModel.findOne({ template_type })
  //   if (emailDoc === null) {
  //     return Promise.reject(
  //       new InvalidInput(`Unable to get email template.`, 400)
  //     )
  //   }
  //   return Promise.resolve({
  //     parsedSubject: Mustache.render(emailDoc.subject, view.subject),
  //     parsedHtml: Mustache.render(emailDoc.html, view.html),
  //     ...emailDoc,
  //   })
  // }

  async getRenderedTemplate(template_type: string, view: IViewObject) {
    const emailDoc = await EmailTemplateModel.findOne({ template_type }).lean()
    if (emailDoc === null) {
      return Promise.reject(
        new InvalidInput(`Unable to get email template.`, 400)
      )
    }
    emailDoc.subject = emailDoc.subject || ""
    emailDoc.html = emailDoc.html || ""
    emailDoc.parsedSubject = Mustache.render(emailDoc.subject, view.subject)
    emailDoc.parsedHtml = Mustache.render(emailDoc.html, view.html)
    return Promise.resolve(emailDoc)
  }

  async addEmailLog(emailData: ISendMail) {
    const newDoc = {
      user: {
        user_type: emailData.user_type,
        user_id: emailData.user_id,
      },
      to_email: emailData.to_email,
      to_name: emailData.to_name,
      from_email: emailData.from_email,
      from_name: emailData.from_name,
      subject: emailData.subject,
      html: emailData.html,
      cc: emailData.cc,
      bcc: emailData.bcc,
      headers: emailData.headers,
      attachement: emailData.attachement,
    }

    return new EmailLogModel(newDoc).save()
  }

  async sentMail(emailData: ISendMail) {
    const data = await this.addEmailLog(emailData)
    const mailOpt: SendMailOptions = {
      from: emailData.from_email,
      to: emailData.to_email,
      subject: emailData.subject,
      html: emailData.html,
      // attachments: [
      //   {
      //     filename: emailData.attachement,
      //     content: "Hello World..!",
      //   },
      // ],
    }
    try {
      await this.mailTransport.sendMail(mailOpt)
      await this.updateEmailLog(data._id, "sent")
    } catch (err) {
      await this.updateEmailLog(data._id, "error", err.message)
      return Promise.resolve()
    }
  }

  async updateEmailLog(logId: string, send_status: string, error?: string) {
    const $set: { [k: string]: any } = { send_status }
    if (!isNullOrUndefined(error)) {
      $set.send_error = error
    }
    return EmailLogModel.findOneAndUpdate({ _id: logId }, $set)
  }

  async generatepdf(mailData: string) {
    return new Promise((resolve, reject) => {
      pdf.create(mailData).toStream(function (err, stream) {
        if (err) return reject(err)
        return resolve(stream)
      })
    })
  }
}
