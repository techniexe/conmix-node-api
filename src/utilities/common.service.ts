import { OrderTrackModel } from "./../model/order_track.model"
import { OrderModel } from "./../model/order.model"
import { VendorOrderModel } from "./../model/vendor.order.model"
import { VendorMediaModel } from "./../model/vendor_media.model"
import { MarginRateModel } from "./../model/margin_rate.model"
import { AddressModel } from "./../model/address.model"
import { WaterRateModel } from "./../model/water_rate.model"
import { AdmixtureRateModel } from "./../model/admixture_rate.model"
import { FlyAshSourceRateModel } from "./../model/fly_ash_source_rate.model"
import { SandSourceRateModel } from "./../model/sand_source_rate.model"
import { CementQuantityModel } from "./../model/cement_quantity.model"
import { injectable } from "inversify"
import * as crypto from "crypto"
import {
  IAuthEmailDoc,
  AuthEmailModel,
  emailVerificationType,
} from "../model/auth.model"
import { InvalidInput } from "./customError"
import { AccessTypes, AccessLogsModel } from "../model/access_logs.model"
import { VendorSettingModel } from "../model/vendor.setting.model"
import { ObjectId } from "bson"
import { TMModel } from "../model/TM.model"
import { TM_unModel } from "../model/TM_un.model"
import { message, transcationFeesPer } from "./config"
import { SandSourceQuantityModel } from "../model/sand_source_quantity.model"
import { AggregateSourceQuantityModel } from "../model/aggregate_source_quantity.model"
import { FlyAshSourceQuantityModel } from "../model/fly_ash_source_quantity.model"
import { AdmixtureQuantityModel } from "../model/admixture_quantity.model"
import { CementRateModel } from "../model/cement_rate.model"
import { AggregateSourceRateModel } from "../model/aggregate_source_rate.model"
import { ConcretePumpModel } from "../model/concrete_pump.model"
import { CP_unModel } from "../model/CP_un.model"
import { getDistance, distanceFactor } from "./distance.utilities"
import { VendorUserModel } from "../model/vendor.user.model"
import { ConcreteGradeModel } from "../model/concrete_grade.model"
import { CementGradeModel } from "../model/cement_grade.model"
import { CementBrandModel } from "../model/cement_brand.model"
import { SandSourceModel } from "../model/sand_source.model"
import { AggregateSandSubCategoryModel } from "../model/aggregate_sand_category.model"
import { AggregateSourceModel } from "../model/aggregate_source.model"
import { FlyAshSourceModel } from "../model/fly_ash.model"
import { AdmixtureCategoryModel } from "../model/admixture_category.model"
import { AdmixtureBrandModel } from "../model/admixture_brand.model"
import { VendorAdMixtureSettingModel } from "../model/vendor_ad_mix_setting.model"
import { generateRandomNumber } from "./generate-random-number"
import { CityModel, StateModel } from "../model/region.model"
import { OrderItemModel } from "../model/order_item.model"
import { isNullOrUndefined } from "./type-guards"
import { BuyerBillingAddressModel } from "../model/buyer_billing_address.model"
import { SiteModel } from "../model/site.model"
import { BillingAddressModel } from "../model/billing_address.model"
import { BuyerUserModel } from "../model/buyer.user.model"
import { GstSlabModel } from "../model/gst_slab.model"
import { BankInfoModel } from "../model/bank.model"
const fs = require("fs")
const path = require("path")
const utils = require("util")
const puppeteer = require("puppeteer")
const hb = require("handlebars")
const readFile = utils.promisify(fs.readFile)

// export const variants = {
//   cement_grade: ["OPC", "PPC"],
//   cement_brand: ["JK", "Ultratech"],
//   send_source: ["rajpadi", "sevalia"],
//   aggregate_subcat: ["10mm", "20mm"],
//   aggregate_source: ["aggregate_source"],
//   fly_ash_source: ["fly_source"],
//   admix_cate: ["admix_cat1", "admix_cat2"],
//   admix_brand: ["admix_brand1", "admix_brand2"],
// }
let finalObj: { [k: string]: any } = {}

var aa = [
  "",
  "one ",
  "two ",
  "three ",
  "four ",
  "five ",
  "six ",
  "seven ",
  "eight ",
  "nine ",
  "ten ",
  "eleven ",
  "twelve ",
  "thirteen ",
  "fourteen ",
  "fifteen ",
  "sixteen ",
  "seventeen ",
  "eighteen ",
  "nineteen ",
]
var bb = [
  "",
  "",
  "twenty",
  "thirty",
  "forty",
  "fifty",
  "sixty",
  "seventy",
  "eighty",
  "ninety",
]
let n: any

@injectable()
export class CommonService {
  removeDuplicates = (custom: any) => {
    const ids: any[] = []
    return custom.reduce((sum: any, element: any) => {
      if (!ids.includes(element.toString())) {
        sum.push(element)
        ids.push(element.toString())
      }
      return sum
    }, [])
  }

  async inWords(num: any) {
    console.log("Inwordsssss")
    if ((num = num.toString()).length > 9) return "overflow"
    n = ("000000000" + num)
      .substr(-9)
      .match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/)
    console.log("n", n)
    if (!n) return
    var str = ""
    str +=
      n[1] !== 0
        ? (aa[Number(n[1])] || bb[n[1][0]] + " " + aa[n[1][1]]) + "crore "
        : ""
    str +=
      n[2] !== 0
        ? (aa[Number(n[2])] || bb[n[2][0]] + " " + aa[n[2][1]]) + "lakh "
        : ""
    str +=
      n[3] !== 0
        ? (aa[Number(n[3])] || bb[n[3][0]] + " " + aa[n[3][1]]) + "thousand "
        : ""
    str +=
      n[4] !== 0
        ? (aa[Number(n[4])] || bb[n[4][0]] + " " + aa[n[4][1]]) + "hundred "
        : ""
    str +=
      n[5] !== 0
        ? (str !== "" ? "and " : "") +
          (aa[Number(n[5])] || bb[n[5][0]] + " " + aa[n[5][1]]) +
          "only "
        : ""
    console.log("str", str)
    return str
  }

  async price_in_words(price: any) {
    var sglDigit = [
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
      ],
      dblDigit = [
        "Ten",
        "Eleven",
        "Twelve",
        "Thirteen",
        "Fourteen",
        "Fifteen",
        "Sixteen",
        "Seventeen",
        "Eighteen",
        "Nineteen",
      ],
      tensPlace = [
        "",
        "Ten",
        "Twenty",
        "Thirty",
        "Forty",
        "Fifty",
        "Sixty",
        "Seventy",
        "Eighty",
        "Ninety",
      ],
      handle_tens = function (dgt: any, prevDgt: any) {
        return 0 === dgt
          ? ""
          : " " + (1 === dgt ? dblDigit[prevDgt] : tensPlace[dgt])
      },
      handle_utlc = function (dgt: any, nxtDgt: any, denom: any) {
        return (
          (0 !== dgt && 1 !== nxtDgt ? " " + sglDigit[dgt] : "") +
          (0 !== nxtDgt || dgt > 0 ? " " + denom : "")
        )
      }

    var str = "",
      digitIdx = 0,
      digit = 0,
      nxtDigit = 0,
      words = []
    if (((price += ""), isNaN(parseInt(price)))) str = ""
    else if (parseInt(price) > 0 && price.length <= 10) {
      for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--)
        switch (
          ((digit = price[digitIdx] - 0),
          (nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0),
          price.length - digitIdx - 1)
        ) {
          case 0:
            words.push(handle_utlc(digit, nxtDigit, ""))
            break
          case 1:
            words.push(handle_tens(digit, price[digitIdx + 1]))
            break
          case 2:
            words.push(
              0 !== digit
                ? " " +
                    sglDigit[digit] +
                    " Hundred" +
                    (0 !== price[digitIdx + 1] && 0 !== price[digitIdx + 2]
                      ? " and"
                      : "")
                : ""
            )
            break
          case 3:
            words.push(handle_utlc(digit, nxtDigit, "Thousand"))
            break
          case 4:
            words.push(handle_tens(digit, price[digitIdx + 1]))
            break
          case 5:
            words.push(handle_utlc(digit, nxtDigit, "Lakh"))
            break
          case 6:
            words.push(handle_tens(digit, price[digitIdx + 1]))
            break
          case 7:
            words.push(handle_utlc(digit, nxtDigit, "Crore"))
            break
          case 8:
            words.push(handle_tens(digit, price[digitIdx + 1]))
            break
          case 9:
            words.push(
              0 !== digit
                ? " " +
                    sglDigit[digit] +
                    " Hundred" +
                    (0 !== price[digitIdx + 1] || 0 !== price[digitIdx + 2]
                      ? " and"
                      : " Crore")
                : ""
            )
        }
      str = words.reverse().join("")
    } else str = ""
    return str
  }

  async getEmailAuthToken(
    email: string,
    user_type: string,
    user_id: string,
    verification_type: emailVerificationType
  ): Promise<IAuthEmailDoc> {
    console.log("email", email)
    console.log("user_type", user_type)
    console.log("user_id", user_id)
    console.log("ver", verification_type)
    const randStr = user_id + user_type + Date.now() + Math.random()
    const auth_token = crypto.createHash("md5").update(randStr).digest("hex")
    const authData = await AuthEmailModel.findOneAndUpdate(
      {
        "user.user_id": user_id,
        "user.user_type": user_type,
        email,
        verification_type,
      },
      {
        $set: { created_at: Date.now() },
        $setOnInsert: {
          auth_token,
        },
      },
      { upsert: true, new: true }
    ).exec()

    console.log(authData)
    if (authData === null) {
      return Promise.reject(
        new InvalidInput(`Unable to get AuthEmail Token.`, 400)
      )
    }
    return Promise.resolve(authData)
  }

  async addActivityLogs(
    user_id: string,
    user_type: string,
    access_type: AccessTypes,
    logs_text?: string,
    old_value?: any,
    new_value?: any
  ) {
    const accessLogsData = {
      user_id,
      user_type,
      access_type,
      logs_text,
      old_value,
      new_value,
    }
    const accessLogs = new AccessLogsModel(accessLogsData)
    await accessLogs.save()
    return Promise.resolve()
  }

  async checkIsValid(modelName: any, data: any[]) {
    for (let i = 0; i < data.length; i++) {
      const isValid = await modelName.findOne({
        _id: data[i],
      })

      if (isValid === null) {
        return Promise.reject(new InvalidInput(`No match data found.`, 400))
      }
    }

    return Promise.resolve(true)
  }

  async checkTMavailibity(vendor_id: any, delivery_date: Date) {
    let setting = await VendorSettingModel.findOne({
      vendor_id: new ObjectId(vendor_id),
    }).exec()
    let with_TM: any = false
    let msgForTM: any = message.NotAvailableTM
    let TM_price: any = 0
    if (!isNullOrUndefined(setting) && setting.with_TM === true) {
      // TM_price = setting.TM_price
      // Check Registered TM.

      const TMquery: { [k: string]: any } = { is_active: true }

      TMquery.$or = [
        {
          user_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let TMdata = await TMModel.find(TMquery)
      // Check Registered TM.
      // let TMUNdata = await TM_unModel.find({
      //  // vendor_id: new ObjectId(vendor_id),
      //   unavailable_at: { $gte: new Date(delivery_date) },
      // })

      const TMUNquery: { [k: string]: any } = {
        unavailable_at: { $gte: new Date(delivery_date) },
      }

      TMUNquery.$or = [
        {
          vendor_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let TMUNdata = await TM_unModel.find(TMUNquery)

      console.log(TMdata.length)
      console.log(TMUNdata.length)

      if (TMdata.length <= TMUNdata.length) {
        let TMarr = []
        for (let i = 0; i < TMUNdata.length; i++) {
          let d1 = TMUNdata[i].unavailable_at
          let d2 = new Date(delivery_date)

          console.log("d1.getTime()", d1.getTime())
          console.log("d2.getTime()", d2.getTime())
          if (d1.getTime() !== d2.getTime()) {
            TMarr.push(TMUNdata[i])
          }
        }
        console.log("TMarr.length", TMarr.length)
        if (TMarr.length > 0) {
          with_TM = true
          msgForTM = message.AvailableTM
          TM_price = setting.TM_price
        }
      } else {
        with_TM = true
        msgForTM = message.AvailableTM
        TM_price = setting.TM_price
      }
    } else {
      TM_price = 0
    }
    return Promise.resolve({ with_TM, msgForTM, TM_price })
  }

  async checkCPavailibity(vendor_id: string, delivery_date: any) {
    let setting = await VendorSettingModel.findOne({
      vendor_id: new ObjectId(vendor_id),
    }).exec()
    let with_CP: any = false
    let msgForCP: any = message.NotAvailableCP
    let CP_price: any = 0
    if (!isNullOrUndefined(setting) && setting.with_CP === true) {
      // with_CP = true
      // CP_price = setting.CP_price
      // Check Registered CP.
      // let CPdata = await ConcretePumpModel.find({
      //   user_id: new ObjectId(vendor_id),
      //   is_active: true,
      // })

      const CPquery: { [k: string]: any } = { is_active: true }

      CPquery.$or = [
        {
          user_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let CPdata = await ConcretePumpModel.find(CPquery)

      //console.log(CPdata.length)

      // Check Registered CP.
      // let CPUNdata = await CP_unModel.find({
      //   vendor_id: new ObjectId(vendor_id),
      //   unavailable_at: { $gte: new Date(delivery_date) },
      // })

      const CPUNquery: { [k: string]: any } = {
        unavailable_at: { $gte: new Date(delivery_date) },
      }

      CPUNquery.$or = [
        {
          vendor_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let CPUNdata = await CP_unModel.find(CPUNquery)

      //console.log(CPUNdata.length)
      if (CPdata.length <= CPUNdata.length) {
        let CParr = []
        for (let i = 0; i < CPUNdata.length; i++) {
          let d1 = CPUNdata[i].unavailable_at
          let d2 = new Date(delivery_date)
          if (d1.getTime() !== d2.getTime()) {
            CParr.push(CPUNdata[i])
          }
        }

        console.log(CParr)

        if (CParr.length > 0) {
          with_CP = true
          msgForCP = message.AvailableCP
          CP_price = setting.CP_price
        }
      } else {
        with_CP = true
        msgForCP = message.AvailableCP
        CP_price = setting.CP_price
      }
    } else {
      CP_price = 0
    }
    return Promise.resolve({ with_CP, msgForCP, CP_price })
  }

  // tslint:disable-next-line:no-shadowed-variable
  async combinations(variants: any) {
    return (function recurse(keys) {
      if (!keys.length) return [{}]
      let result = recurse(keys.slice(1))
      return variants[keys[0]].reduce(
        (acc: any, value: any) =>
          acc.concat(
            result.map((item: any) =>
              Object.assign({}, item, {
                [keys[0]]: value,
              })
            )
          ),
        []
      )
    })(Object.keys(variants))
  }

  async checkQuantity(cg_ids: any[], addrVendorCheck: any, reqData: any) {
    console.log("reqDataaaaaaaaaaaa", reqData)
    /************CHECK FOR "CEMENT"**************** */
    let cement_vendor_ids: any[] = []
    let cement_address_ids: any[] = []

    if (
      !isNullOrUndefined(reqData.cement_brand_id) &&
      !isNullOrUndefined(reqData.cement_grade_id) &&
      reqData.cement_brand_id.length > 0 &&
      reqData.cement_grade_id.length > 0 &&
      !isNullOrUndefined(reqData.cement_quantity)
    ) {
      let cementdata = await this.cementQuantity(
        cg_ids,
        addrVendorCheck,
        reqData.cement_brand_id,
        reqData.cement_grade_id,
        reqData.cement_quantity
      )

      cement_vendor_ids = cementdata.final_cement_vendor_ids
      cement_address_ids = cementdata.final_cement_address_ids
      finalObj = cementdata.finalObj
      // if (cement_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No cement associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
    }
    console.log("cement_vendor_ids", cement_vendor_ids)
    console.log("cement_address_ids", cement_address_ids)
    /************CHECK FOR "SAND"**************** */
    let sand_vendor_ids: any[] = []
    let sand_address_ids: any[] = []
    if (
      !isNullOrUndefined(reqData.sand_source_id) &&
      reqData.sand_source_id.length > 0 &&
      !isNullOrUndefined(reqData.sand_quantity)
    ) {
      const sanddata = await this.sandQuantity(
        cement_vendor_ids,
        cement_address_ids,
        reqData.sand_source_id,
        reqData.sand_quantity
      )
      sand_vendor_ids = sanddata.final_sand_vendor_ids
      sand_address_ids = sanddata.final_sand_address_ids
      finalObj = sanddata.finalObj

      // if (sand_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No sand associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
    } else {
      sand_vendor_ids = cement_vendor_ids
      sand_address_ids = cement_address_ids
    }
    console.log("sand_vendor_ids", sand_vendor_ids)
    console.log("sand_address_ids", sand_address_ids)
    /************CHECK FOR "Aggregate1"**************** */
    let agg1_vendor_ids: any[] = []
    let agg1_address_ids: any[] = []

    if (
      !isNullOrUndefined(reqData.aggregate1_sub_cat_id) &&
      !isNullOrUndefined(reqData.aggregate_source_id) &&
      reqData.aggregate1_sub_cat_id.length > 0 &&
      reqData.aggregate_source_id.length > 0 &&
      !isNullOrUndefined(reqData.aggregate1_quantity)
    ) {
      const aggregate1Data = await this.aggregate1Quantity(
        sand_vendor_ids,
        sand_address_ids,
        reqData.aggregate1_sub_cat_id,
        reqData.aggregate_source_id,
        reqData.aggregate1_quantity
      )

      console.log("aggregate1Data", aggregate1Data)

      agg1_vendor_ids = aggregate1Data.final_agg1_vendor_ids
      agg1_address_ids = aggregate1Data.final_agg1_address_ids
      finalObj = aggregate1Data.finalObj

      console.log("agg1_vendor_ids", agg1_vendor_ids)
      console.log("agg1_address_ids", agg1_address_ids)

      // if (agg1_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No Aggregate1 associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
    } else if (
      !isNullOrUndefined(reqData.aggregate1_sub_cat_id) &&
      reqData.aggregate1_sub_cat_id.length > 0 &&
      !isNullOrUndefined(reqData.aggregate1_quantity)
    ) {
      const aggregate1Data = await this.aggregate1QuantityWithoutSource(
        sand_vendor_ids,
        sand_address_ids,
        reqData.aggregate1_sub_cat_id,
        reqData.aggregate1_quantity
      )
      agg1_vendor_ids = aggregate1Data.final_agg1_vendor_ids
      agg1_address_ids = aggregate1Data.final_agg1_address_ids
      finalObj = aggregate1Data.finalObj

      console.log("agg1_vendor_ids", agg1_vendor_ids)
      console.log("agg1_address_ids", agg1_address_ids)
    } else {
      agg1_vendor_ids = sand_vendor_ids
      agg1_address_ids = sand_address_ids
    }

    /************CHECK FOR "Aggregate2"**************** */
    let agg2_vendor_ids: any[] = []
    let agg2_address_ids: any[] = []

    if (
      !isNullOrUndefined(reqData.aggregate2_sub_cat_id) &&
      !isNullOrUndefined(reqData.aggregate_source_id) &&
      reqData.aggregate2_sub_cat_id.length > 0 &&
      reqData.aggregate_source_id.length > 0 &&
      !isNullOrUndefined(reqData.aggregate2_quantity)
    ) {
      const aggregate2Data = await this.aggregate2Quantity(
        agg1_vendor_ids,
        agg1_address_ids,
        reqData.aggregate2_sub_cat_id,
        reqData.aggregate_source_id,
        reqData.aggregate2_quantity
      )
      agg2_vendor_ids = aggregate2Data.final_agg2_vendor_ids
      agg2_address_ids = aggregate2Data.final_agg2_address_ids
      finalObj = aggregate2Data.finalObj

      console.log("agg2_vendor_ids", agg2_vendor_ids)
      console.log("agg2_address_ids", agg2_address_ids)

      // if (agg2_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No Aggregate2 associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
    } else if (
      !isNullOrUndefined(reqData.aggregate2_sub_cat_id) &&
      reqData.aggregate2_sub_cat_id.length > 0 &&
      !isNullOrUndefined(reqData.aggregate2_quantity)
    ) {
      const aggregate2Data = await this.aggregate2QuantityWithoutSource(
        agg1_vendor_ids,
        agg1_address_ids,
        reqData.aggregate2_sub_cat_id,
        reqData.aggregate2_quantity
      )
      agg2_vendor_ids = aggregate2Data.final_agg2_vendor_ids
      agg2_address_ids = aggregate2Data.final_agg2_address_ids
      finalObj = aggregate2Data.finalObj

      console.log("agg2_vendor_ids", agg2_vendor_ids)
      console.log("agg2_address_ids", agg2_address_ids)
    } else {
      agg2_vendor_ids = agg1_vendor_ids
      agg2_address_ids = agg1_address_ids
    }

    /************CHECK FOR "Fly_Ash"**************** */
    let fly_ash_vendor_ids: any[] = []
    let fly_ash_address_ids: any[] = []
    if (
      !isNullOrUndefined(reqData.fly_ash_source_id) &&
      reqData.fly_ash_source_id.length > 0 &&
      !isNullOrUndefined(reqData.fly_ash_quantity)
    ) {
      console.log("flyyyyyyyyyyyyyyyyyyyyyyyyyyyy")
      const flyAshData = await this.flyAshQuantity(
        agg2_vendor_ids,
        agg2_address_ids,
        reqData.fly_ash_source_id,
        reqData.fly_ash_quantity
      )
      fly_ash_vendor_ids = flyAshData.final_fly_ash_vendor_ids
      fly_ash_address_ids = flyAshData.final_fly_ash_address_ids
      finalObj = flyAshData.finalObj

      // if (fly_ash_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No fly Ash associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
    } else {
      fly_ash_vendor_ids = agg2_vendor_ids
      fly_ash_address_ids = agg2_address_ids

      console.log("fly_ash_vendor_ids", fly_ash_vendor_ids)
      console.log("fly_ash_address_ids", fly_ash_address_ids)
    }

    /************CHECK FOR "ADMIX"**************** */
    let admix_vendor_ids: any[] = []
    let admix_address_ids: any[] = []

    if (
      !isNullOrUndefined(reqData.admix_brand_id) &&
      !isNullOrUndefined(reqData.admix_category_id) &&
      !isNullOrUndefined(reqData.admix_quantity)
    ) {
      const admixData = await this.AdmixQuantity(
        fly_ash_vendor_ids,
        fly_ash_address_ids,
        reqData.admix_category_id,
        reqData.admix_brand_id,
        reqData.admix_quantity
      )
      admix_vendor_ids = admixData.final_admix_vendor_ids
      admix_address_ids = admixData.final_admix_address_ids
      finalObj = admixData.finalObj

      // if (admix_vendor_ids.length <= 0) {
      //   return Promise.reject(
      //     new InvalidInput(
      //       `No Admixture associated data found with this vendor....!`,
      //       400
      //     )
      //   )
      // }
      console.log("finalObj1", finalObj)
      console.log("admix_vendor_ids", admix_vendor_ids)
      console.log("admix_address_ids", admix_address_ids)
    } else {
      admix_vendor_ids = fly_ash_vendor_ids
      admix_address_ids = fly_ash_address_ids
    }
    console.log("after finalObj1", finalObj)
    console.log("after admix_vendor_ids", admix_vendor_ids)
    console.log("after admix_address_ids", admix_address_ids)
    return {
      finalObj,
      admix_vendor_ids,
      admix_address_ids,
    }
  }

  // check for cement exists with quantity.
  async cementQuantity(
    vendor_ids: any[],
    addressData: any[],
    brand_id: any,
    grade_id: any,
    cement_quantity: any
  ) {
    let cement_vendor_ids: any[] = []
    let cement_grade_id: any[] = []
    let cement_brand_id: any[] = []
    let cement_address_id: any[] = []
    let final_cement_vendor_ids: any[] = []
    let final_cement_address_ids: any[] = []

    for (let i = 0; i < vendor_ids.length; i++) {
      let addArr = addressData[vendor_ids[i]]
      if (addArr.length > 0) {
        let Cementresp: any = {}
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            brand_id,
            grade_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = cement_quantity
            return o
          })
          console.log("result", result)
          Cementresp = await this.cementExists(result)

          console.log("Cementresp", Cementresp)

          if (!isNullOrUndefined(Cementresp) && Cementresp.length > 0) {
            for (let k = 0; k < Cementresp.length; k++) {
              cement_vendor_ids.push(Cementresp[k].vendor_id)
              cement_grade_id.push(Cementresp[k].grade_id)
              cement_brand_id.push(Cementresp[k].brand_id)
              cement_address_id.push(Cementresp[k].address_id)
            }
          }
        }
        final_cement_vendor_ids = await this.removeDuplicates(cement_vendor_ids)
        final_cement_address_ids = await this.removeDuplicates(
          cement_address_id
        )
        finalObj.final_cement_grade_id = await this.removeDuplicates(
          cement_grade_id
        )
        finalObj.final_cement_brand_id = await this.removeDuplicates(
          cement_brand_id
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }

    console.log("final_cement_vendor_ids", final_cement_vendor_ids)
    console.log("final_cement_address_ids", final_cement_address_ids)
    console.log("finalObj", finalObj)

    return Promise.resolve({
      final_cement_vendor_ids,
      final_cement_address_ids,
      finalObj,
    })
  }

  async cementExists(cementData: any[]) {
    let data: any[] = []
    for (let i = 0; i < cementData.length; i++) {
      console.log("cementData", cementData)

      const cquery: { [k: string]: any } = {
        address_id: cementData[i].address_id,
        brand_id: cementData[i].brand_id,
        grade_id: cementData[i].grade_id,
        quantity: { $gte: cementData[i].quantity },
      }

      cquery.$or = [
        {
          vendor_id: new ObjectId(cementData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(cementData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(cementData[i].vendor_id),
        },
      ]

      let doc = await CementQuantityModel.findOne(cquery)

      // const doc = await CementQuantityModel.findOne({
      //   address_id: cementData[i].address_id,
      //   brand_id: cementData[i].brand_id,
      //   grade_id: cementData[i].grade_id,
      //   vendor_id: cementData[i].vendor_id,
      //   quantity: { $gte: cementData[i].quantity },
      // })
      console.log("doc", doc)
      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async sandQuantity(
    vendor_ids: any[],
    addArr: any[],
    sand_source_id: any,
    sand_quantity: any
  ) {
    let sand_vendor_ids: any[] = []
    let sand_source_ids: any[] = []
    let sand_address_id: any[] = []
    let final_sand_vendor_ids: any[] = []
    let final_sand_address_ids: any[] = []

    for (let i = 0; i < vendor_ids.length; i++) {
      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            sand_source_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = sand_quantity
            return o
          })

          const Sandresp = await this.sandExists(result)
          console.log("Sandresp", Sandresp)
          if (!isNullOrUndefined(Sandresp) && Sandresp.length > 0) {
            for (let k = 0; k < Sandresp.length; k++) {
              sand_vendor_ids.push(Sandresp[k].vendor_id)
              sand_source_ids.push(Sandresp[k].source_id)
              sand_address_id.push(Sandresp[k].address_id)
            }
          }
        }
        final_sand_vendor_ids = await this.removeDuplicates(sand_vendor_ids)
        console.log("final_sand_vendor_ids", final_sand_vendor_ids)
        final_sand_address_ids = await this.removeDuplicates(sand_address_id)
        finalObj.final_sand_source_id = await this.removeDuplicates(
          sand_source_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_sand_vendor_ids,
      final_sand_address_ids,
      finalObj,
    })
  }

  async sandExists(sandData: any[]) {
    let data: any[] = []
    for (let i = 0; i < sandData.length; i++) {
      console.log("sandData", sandData)
      const squery: { [k: string]: any } = {
        address_id: sandData[i].address_id,
        source_id: sandData[i].sand_source_id,
        //vendor_id: sandData[i].vendor_id,
        quantity: { $gte: sandData[i].quantity },
      }

      squery.$or = [
        {
          vendor_id: new ObjectId(sandData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(sandData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(sandData[i].vendor_id),
        },
      ]

      let doc = await SandSourceQuantityModel.findOne(squery)

      // const doc = await SandSourceQuantityModel.findOne({
      //   address_id: sandData[i].address_id,
      //   source_id: sandData[i].sand_source_id,
      //   vendor_id: sandData[i].vendor_id,
      //   quantity: { $gte: sandData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async aggregate1Quantity(
    vendor_ids: any[],
    addArr: any[],
    aggregate1_sub_cat_id: any,
    aggregate_source_id: any,
    aggregate1_quantity: any
  ) {
    let agg_vendor_ids: any[] = []
    let aggregate1_sub_cat_ids: any[] = []
    let agg_source_ids: any[] = []
    let agg_address_ids: any[] = []
    let final_agg1_vendor_ids: any[] = []
    let final_agg1_address_ids: any[] = []
    // let final_agg_sand_sub_cat_ids: any[] = []
    // let final_agg_source_ids: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      // let addArr = addressData[vendor_ids[i]]
      console.log("hereee", addArr)
      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            aggregate1_sub_cat_id,
            aggregate_source_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = aggregate1_quantity
            return o
          })
          console.log("aggggresult", result)
          const Aggresp = await this.agg1Exists(result)

          if (!isNullOrUndefined(Aggresp) && Aggresp.length > 0) {
            for (let k = 0; k < Aggresp.length; k++) {
              agg_vendor_ids.push(Aggresp[k].vendor_id)
              agg_source_ids.push(Aggresp[k].source_id)
              aggregate1_sub_cat_ids.push(Aggresp[k].sub_cat_id)
              agg_address_ids.push(Aggresp[k].address_id)
            }
          }
        }
        final_agg1_vendor_ids = await this.removeDuplicates(agg_vendor_ids)
        final_agg1_address_ids = await this.removeDuplicates(agg_address_ids)
        finalObj.final_aggregate1_sub_cat_id = await this.removeDuplicates(
          aggregate1_sub_cat_ids
        )
        finalObj.final_agg_source_id = await this.removeDuplicates(
          agg_source_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_agg1_vendor_ids,
      final_agg1_address_ids,
      finalObj,
    })
  }

  async agg1Exists(aggData: any[]) {
    let data: any[] = []
    for (let i = 0; i < aggData.length; i++) {

      const aggquery: { [k: string]: any } = {
        address_id: aggData[i].address_id,
        source_id: aggData[i].aggregate_source_id,
        sub_cat_id: aggData[i].aggregate1_sub_cat_id,
       // vendor_id: aggData[i].vendor_id,
        quantity: { $gte: aggData[i].quantity },
      }

      aggquery.$or = [
        {
          vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
      ]

      let doc = await AggregateSourceQuantityModel.findOne(aggquery)

      // const doc = await AggregateSourceQuantityModel.findOne({
      //   address_id: aggData[i].address_id,
      //   source_id: aggData[i].aggregate_source_id,
      //   sub_cat_id: aggData[i].aggregate1_sub_cat_id,
      //   vendor_id: aggData[i].vendor_id,
      //   quantity: { $gte: aggData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    console.log("data", data)
    return Promise.resolve(data)
  }

  async aggregate1QuantityWithoutSource(
    vendor_ids: any[],
    addArr: any[],
    aggregate1_sub_cat_id: any,
    aggregate1_quantity: any
  ) {
    let agg_vendor_ids: any[] = []
    let aggregate1_sub_cat_ids: any[] = []
    let agg_address_ids: any[] = []
    let final_agg1_vendor_ids: any[] = []
    let final_agg1_address_ids: any[] = []
    // let final_agg_sand_sub_cat_ids: any[] = []
    // let final_agg_source_ids: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      // let addArr = addressData[vendor_ids[i]]

      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            aggregate1_sub_cat_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = aggregate1_quantity
            return o
          })
          console.log("result", result)
          const Aggresp = await this.agg1ExistsWithoutSource(result)

          if (!isNullOrUndefined(Aggresp) && Aggresp.length > 0) {
            for (let k = 0; k < Aggresp.length; k++) {
              agg_vendor_ids.push(Aggresp[k].vendor_id)
              aggregate1_sub_cat_ids.push(Aggresp[k].sub_cat_id)
              agg_address_ids.push(Aggresp[k].address_id)
            }
          }
        }
        final_agg1_vendor_ids = await this.removeDuplicates(agg_vendor_ids)
        final_agg1_address_ids = await this.removeDuplicates(agg_address_ids)
        finalObj.final_aggregate1_sub_cat_id = await this.removeDuplicates(
          aggregate1_sub_cat_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_agg1_vendor_ids,
      final_agg1_address_ids,
      finalObj,
    })
  }

  async agg1ExistsWithoutSource(aggData: any[]) {
    let data: any[] = []
    for (let i = 0; i < aggData.length; i++) {

      const aggquery: { [k: string]: any } = {
        address_id: aggData[i].address_id,
        sub_cat_id: aggData[i].aggregate1_sub_cat_id,
        //vendor_id: aggData[i].vendor_id,
        quantity: { $gte: aggData[i].quantity },
      }

      aggquery.$or = [
        {
          vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
      ]

      let doc = await AggregateSourceQuantityModel.findOne(aggquery)

      // const doc = await AggregateSourceQuantityModel.findOne({
      //   address_id: aggData[i].address_id,
      //   sub_cat_id: aggData[i].aggregate1_sub_cat_id,
      //   vendor_id: aggData[i].vendor_id,
      //   quantity: { $gte: aggData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    console.log("data", data)
    return Promise.resolve(data)
  }

  async aggregate2Quantity(
    vendor_ids: any[],
    addArr: any[],
    aggregate2_sub_cat_id: any,
    aggregate_source_id: any,
    aggregate2_quantity: any
  ) {
    let agg_vendor_ids: any[] = []
    let aggregate2_sub_cat_ids: any[] = []
    let agg_source_ids: any[] = []
    let agg_address_ids: any[] = []
    let final_agg2_vendor_ids: any[] = []
    let final_agg2_address_ids: any[] = []
    // let final_agg_sand_sub_cat_ids: any[] = []
    // let final_agg_source_ids: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      // let addArr = addressData[vendor_ids[i]]

      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            aggregate2_sub_cat_id,
            aggregate_source_id,
          }
          const final = await this.combinations(reqObj)
          console.log("result", final)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = aggregate2_quantity
            return o
          })
          console.log("result", result)
          const Aggresp = await this.agg2Exists(result)

          if (!isNullOrUndefined(Aggresp) && Aggresp.length > 0) {
            for (let k = 0; k < Aggresp.length; k++) {
              agg_vendor_ids.push(Aggresp[k].vendor_id)
              agg_source_ids.push(Aggresp[k].source_id)
              aggregate2_sub_cat_ids.push(Aggresp[k].sub_cat_id)
              agg_address_ids.push(Aggresp[k].address_id)
            }
          }
        }
        final_agg2_vendor_ids = await this.removeDuplicates(agg_vendor_ids)
        final_agg2_address_ids = await this.removeDuplicates(agg_address_ids)
        finalObj.final_aggregate2_sub_cat_id = await this.removeDuplicates(
          aggregate2_sub_cat_ids
        )
        finalObj.final_agg_source_id = await this.removeDuplicates(
          agg_source_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_agg2_vendor_ids,
      final_agg2_address_ids,
      finalObj,
    })
  }

  async aggregate2QuantityWithoutSource(
    vendor_ids: any[],
    addArr: any[],
    aggregate2_sub_cat_id: any,
    aggregate2_quantity: any
  ) {
    let agg_vendor_ids: any[] = []
    let aggregate2_sub_cat_ids: any[] = []
    let agg_address_ids: any[] = []
    let final_agg2_vendor_ids: any[] = []
    let final_agg2_address_ids: any[] = []
    // let final_agg_sand_sub_cat_ids: any[] = []
    // let final_agg_source_ids: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      // let addArr = addressData[vendor_ids[i]]

      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            aggregate2_sub_cat_id,
          }
          const final = await this.combinations(reqObj)
          console.log("result", final)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = aggregate2_quantity
            return o
          })
          console.log("result", result)
          const Aggresp = await this.agg2ExistsWithoutSource(result)

          if (!isNullOrUndefined(Aggresp) && Aggresp.length > 0) {
            for (let k = 0; k < Aggresp.length; k++) {
              agg_vendor_ids.push(Aggresp[k].vendor_id)
              aggregate2_sub_cat_ids.push(Aggresp[k].sub_cat_id)
              agg_address_ids.push(Aggresp[k].address_id)
            }
          }
        }
        final_agg2_vendor_ids = await this.removeDuplicates(agg_vendor_ids)
        final_agg2_address_ids = await this.removeDuplicates(agg_address_ids)
        finalObj.final_aggregate2_sub_cat_id = await this.removeDuplicates(
          aggregate2_sub_cat_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_agg2_vendor_ids,
      final_agg2_address_ids,
      finalObj,
    })
  }
  async agg2Exists(aggData: any[]) {
    let data: any[] = []
    for (let i = 0; i < aggData.length; i++) {

      const agg2query: { [k: string]: any } = {
        address_id: aggData[i].address_id,
        source_id: aggData[i].aggregate_source_id,
        sub_cat_id: aggData[i].aggregate2_sub_cat_id,
       // vendor_id: aggData[i].vendor_id,
        quantity: { $gte: aggData[i].quantity },
      }

      agg2query.$or = [
        {
          vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
      ]

      let doc = await AggregateSourceQuantityModel.findOne(agg2query)

      // const doc = await AggregateSourceQuantityModel.findOne({
      //   address_id: aggData[i].address_id,
      //   source_id: aggData[i].aggregate_source_id,
      //   sub_cat_id: aggData[i].aggregate2_sub_cat_id,
      //   vendor_id: aggData[i].vendor_id,
      //   quantity: { $gte: aggData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async agg2ExistsWithoutSource(aggData: any[]) {
    let data: any[] = []
    for (let i = 0; i < aggData.length; i++) {

      const agg2query: { [k: string]: any } = {
        address_id: aggData[i].address_id,
        sub_cat_id: aggData[i].aggregate2_sub_cat_id,
        vendor_id: aggData[i].vendor_id,
        quantity: { $gte: aggData[i].quantity },
      }

      agg2query.$or = [
        {
          vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(aggData[i].vendor_id),
        },
      ]

      let doc = await AggregateSourceQuantityModel.findOne(agg2query)


      // const doc = await AggregateSourceQuantityModel.findOne({
      //   address_id: aggData[i].address_id,
      //   sub_cat_id: aggData[i].aggregate2_sub_cat_id,
      //   vendor_id: aggData[i].vendor_id,
      //   quantity: { $gte: aggData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async flyAshQuantity(
    vendor_ids: any[],
    addArr: any[],
    fly_ash_source_id: any,
    fly_ash_quantity: any
  ) {
    let fly_ash_vendor_ids: any[] = []
    let fly_ash_source_ids: any[] = []
    let fly_ash_address_ids: any[] = []
    let final_fly_ash_vendor_ids: any[] = []
    let final_fly_ash_address_ids: any[] = []
    //let final_fly_ash_source_ids: any[] = []
    for (let i = 0; i < vendor_ids.length; i++) {
      //let addArr = addressData[vendor_ids[i]]

      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            fly_ash_source_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = fly_ash_quantity
            return o
          })
          // console.log("result", result)
          const flyAshresp = await this.flyAshExists(result)

          //console.log("Cementresp", Cementresp)
          if (!isNullOrUndefined(flyAshresp) && flyAshresp.length > 0) {
            for (let k = 0; k < flyAshresp.length; k++) {
              fly_ash_vendor_ids.push(flyAshresp[k].vendor_id)
              fly_ash_source_ids.push(flyAshresp[k].source_id)
              fly_ash_address_ids.push(flyAshresp[k].address_id)
            }
          }
        }
        final_fly_ash_vendor_ids = await this.removeDuplicates(
          fly_ash_vendor_ids
        )
        final_fly_ash_address_ids = await this.removeDuplicates(
          fly_ash_address_ids
        )
        finalObj.final_fly_ash_source_id = await this.removeDuplicates(
          fly_ash_source_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_fly_ash_vendor_ids,
      final_fly_ash_address_ids,
      finalObj,
    })
  }

  async flyAshExists(flyAshData: any[]) {
    let data: any[] = []
    for (let i = 0; i < flyAshData.length; i++) {

      const fquery: { [k: string]: any } = {
        address_id: flyAshData[i].address_id,
        source_id: flyAshData[i].fly_ash_source_id,
       // vendor_id: flyAshData[i].vendor_id,
        quantity: { $gte: flyAshData[i].quantity },
      }

      fquery.$or = [
        {
          vendor_id: new ObjectId(flyAshData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(flyAshData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(flyAshData[i].vendor_id),
        },
      ]

      let doc = await FlyAshSourceQuantityModel.findOne(fquery)

      // const doc = await FlyAshSourceQuantityModel.findOne({
      //   address_id: flyAshData[i].address_id,
      //   source_id: flyAshData[i].fly_ash_source_id,
      //   vendor_id: flyAshData[i].vendor_id,
      //   quantity: { $gte: flyAshData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async AdmixQuantity(
    vendor_ids: any[],
    addArr: any[],
    admix_category_id: any,
    admix_brand_id: any,
    admix_quantity: any
  ) {
    let admix_vendor_ids: any[] = []
    let admix_cat_ids: any[] = []
    let admix_brand_ids: any[] = []
    let admix_address_ids: any[] = []
    let final_admix_vendor_ids: any[] = []
    let final_admix_address_ids: any[] = []
    // let final_admix_cat_ids: any[] = []
    // let final_admix_brand_ids: any[] = []

    for (let i = 0; i < vendor_ids.length; i++) {
      // let addArr = addressData[vendor_ids[i]]

      if (addArr.length > 0) {
        for (let j = 0; j < addArr.length; j++) {
          let reqObj = {
            admix_category_id,
            admix_brand_id,
          }
          const final = await this.combinations(reqObj)
          var result = final.map(function (el: any) {
            var o = Object.assign({}, el)
            o.address_id = addArr[j]._id
            o.vendor_id = vendor_ids[i]
            o.quantity = admix_quantity
            return o
          })
          console.log("result", result)
          const Admixresp = await this.admixExists(result)

          if (!isNullOrUndefined(Admixresp) && Admixresp.length > 0) {
            for (let k = 0; k < Admixresp.length; k++) {
              admix_vendor_ids.push(Admixresp[k].vendor_id)
              admix_cat_ids.push(Admixresp[k].category_id)
              admix_brand_ids.push(Admixresp[k].brand_id)
              admix_address_ids.push(Admixresp[k].address_id)
            }
          }
        }
        final_admix_address_ids = await this.removeDuplicates(admix_address_ids)
        final_admix_vendor_ids = await this.removeDuplicates(admix_vendor_ids)
        finalObj.final_admix_cat_id = await this.removeDuplicates(admix_cat_ids)
        finalObj.final_admix_brand_id = await this.removeDuplicates(
          admix_brand_ids
        )
      } else {
        return Promise.reject(
          new InvalidInput(`No Address details found for this vendor....!`, 400)
        )
      }
    }
    return Promise.resolve({
      final_admix_vendor_ids,
      final_admix_address_ids,
      finalObj,
    })
  }

  async admixExists(admixData: any[]) {
    let data: any[] = []
    for (let i = 0; i < admixData.length; i++) {

      const squery: { [k: string]: any } = {
        address_id: admixData[i].address_id,
        category_id: admixData[i].admix_category_id,
        brand_id: admixData[i].admix_brand_id,
       // vendor_id: admixData[i].vendor_id,
        quantity: { $gte: admixData[i].quantity },
      }

      squery.$or = [
        {
          vendor_id: new ObjectId(admixData[i].vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(admixData[i].vendor_id),
        },
        {
          master_vendor_id: new ObjectId(admixData[i].vendor_id),
        },
      ]

      let doc = await AdmixtureQuantityModel.findOne(squery)

      // const doc = await AdmixtureQuantityModel.findOne({
      //   address_id: admixData[i].address_id,
      //   category_id: admixData[i].admix_category_id,
      //   brand_id: admixData[i].admix_brand_id,
      //   vendor_id: admixData[i].vendor_id,
      //   quantity: { $gte: admixData[i].quantity },
      // })

      if (!isNullOrUndefined(doc)) {
        data.push(doc)
      }
    }
    return Promise.resolve(data)
  }

  async findCustom(
    data: any,
    vendor_ids: any,
    addArr: any[],
    cement_quantity: any,
    sand_quantity: any,
    aggregate1_quantity: any,
    aggregate2_quantity: any,
    fly_ash_quantity: any,
    admix_quantity: any,
    water_quantity: any,
    with_CP: any,
    with_TM: any,
    delivery_date: any,
    end_date: any,
    dvlLocation: any,
    per_Qubic_meter: number
  ) {
    let finalResp: any[] = []
    let resp: any
    const res = await this.combinations(data)

    console.log("ressssssssssss", res)
    for (let i = 0; i < res.length; i++) {
      for (let j = 0; j < vendor_ids.length; j++) {
        //let addArr = addressData[vendor_ids[j]]
        if (addArr.length > 0) {
          for (let k = 0; k < addArr.length; k++) {
            resp = await this.findPerKgRate(
              res[i],
              vendor_ids[j],
              addArr[k]._id,
              cement_quantity,
              sand_quantity,
              aggregate1_quantity,
              aggregate2_quantity,
              fly_ash_quantity,
              admix_quantity,
              water_quantity,
              with_CP,
              with_TM,
              delivery_date,
              end_date,
              dvlLocation,
              per_Qubic_meter
            )

            finalResp.push(resp)
          }
        }
      }
    }
    return Promise.resolve(finalResp)
  }

  async findPerKgRate(
    data: any,
    vendor_id: string,
    address_id: string,
    cement_quantity: number,
    sand_quantity: number,
    aggregate1_quantity: number,
    aggregate2_quantity: number,
    fly_ash_quantity: number,
    admix_quantity: number,
    water_quantity: number,
    with_CP: boolean,
    with_TM: boolean,
    delivery_date: any,
    end_date: any,
    dvlLocation: any,
    per_Qubic_meter: number
  ) {
    /***Cement per kg Price Calculate**/
    if (
      !isNullOrUndefined(data.final_cement_brand_id) &&
      !isNullOrUndefined(data.final_cement_grade_id)
    ) {
      const cementData = await CementRateModel.findOne({
        brand_id: new ObjectId(data.final_cement_brand_id),
        grade_id: new ObjectId(data.final_cement_grade_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })
      if (!isNullOrUndefined(cementData)) {
        data.vendor_id = vendor_id
        data.address_id = address_id
        data.cement_per_kg_rate = cementData.per_kg_rate
      } else {
        data.ErrormsgForCement = `No rate define for cement..!`
      }
    }

    /***Sand per kg Price Calculate**/
    if (!isNullOrUndefined(data.final_sand_source_id)) {
      const sandData = await SandSourceRateModel.findOne({
        source_id: new ObjectId(data.final_sand_source_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })

      if (!isNullOrUndefined(sandData)) {
        data.sand_per_kg_rate = sandData.per_kg_rate
      } else {
        data.sand_per_kg_rate = 0
        data.ErrormsgForSand = `No rate define for sand..!`
      }
    }
    /***Aggregate1 per kg Price Calculate**/

    if (
      !isNullOrUndefined(data.final_agg_source_id) &&
      !isNullOrUndefined(data.final_aggregate1_sub_cat_id)
    ) {
      const agg1Data = await AggregateSourceRateModel.findOne({
        source_id: new ObjectId(data.final_agg_source_id),
        sub_cat_id: new ObjectId(data.final_aggregate1_sub_cat_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })

      if (!isNullOrUndefined(agg1Data)) {
        data.aggregate1_per_kg_rate = agg1Data.per_kg_rate
      } else {
        data.aggregate1_per_kg_rate = 0
        data.ErrormsgForAgg = `No rate define for aggregate1..!`
      }
    }

    /***Aggregate1 per kg Price without source Calculate**/

    if (!isNullOrUndefined(data.final_aggregate1_sub_cat_id)) {
      const agg1Data = await AggregateSourceRateModel.find({
        sub_cat_id: new ObjectId(data.final_aggregate1_sub_cat_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      }).sort({ per_kg_rate: 1 })

      if (!isNullOrUndefined(agg1Data) && agg1Data.length > 0) {
        data.aggregate1_per_kg_rate = agg1Data[0].per_kg_rate
      } else {
        data.aggregate1_per_kg_rate = 0
        data.ErrormsgForAgg = `No rate define for aggregate1..!`
      }
    }

    /***Aggregate2 per kg Price Calculate**/
    if (
      !isNullOrUndefined(data.final_agg_source_id) &&
      !isNullOrUndefined(data.final_aggregate2_sub_cat_id)
    ) {
      const agg2Data = await AggregateSourceRateModel.findOne({
        source_id: new ObjectId(data.final_agg_source_id),
        sub_cat_id: new ObjectId(data.final_aggregate2_sub_cat_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })
      if (!isNullOrUndefined(agg2Data)) {
        data.aggregate2_per_kg_rate = agg2Data.per_kg_rate
      } else {
        data.aggregate2_per_kg_rate = 0
        data.ErrormsgForAgg = `No rate define for aggregate2..!`
      }
    }

    /***Aggregate2 per kg Price Calculate**/
    if (!isNullOrUndefined(data.final_aggregate2_sub_cat_id)) {
      const agg2Data = await AggregateSourceRateModel.find({
        sub_cat_id: new ObjectId(data.final_aggregate2_sub_cat_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      }).sort({ per_kg_rate: 1 })

      if (!isNullOrUndefined(agg2Data)) {
        data.aggregate2_per_kg_rate = agg2Data[0].per_kg_rate
      } else {
        data.aggregate2_per_kg_rate = 0
        data.ErrormsgForAgg = `No rate define for aggregate2..!`
      }
    }

    /***Fly Ash per kg Price Calculate**/
    if (!isNullOrUndefined(data.final_fly_ash_source_id)) {
      const flyAshData = await FlyAshSourceRateModel.findOne({
        source_id: new ObjectId(data.final_fly_ash_source_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })
      if (!isNullOrUndefined(flyAshData)) {
        data.fly_ash_per_kg_rate = flyAshData.per_kg_rate
      } else {
        data.fly_ash_per_kg_rate = 0
        data.ErrormsgForAgg = `No rate define for Fly Ash..!`
      }
    }

    /***Admix per kg Price Calculate**/
    if (
      !isNullOrUndefined(data.final_admix_brand_id) &&
      !isNullOrUndefined(data.final_admix_cat_id)
    ) {
      const admixData = await AdmixtureRateModel.findOne({
        brand_id: new ObjectId(data.final_admix_brand_id),
        category_id: new ObjectId(data.final_admix_cat_id),
        vendor_id: new ObjectId(vendor_id),
        address_id: new ObjectId(address_id),
      })
      if (!isNullOrUndefined(admixData)) {
        data.admix_per_kg_rate = admixData.per_kg_rate
      } else {
        data.ErrormsgForAgg = `No rate define for Admix.!`
      }
    } else {
      console.log("Hereeeeeeeeeeeeeee")
      const addressData = await AddressModel.findById(data.address_id)
      if (!isNullOrUndefined(addressData)) {
        const distance =
          getDistance(
            addressData.location.coordinates,
            dvlLocation.coordinates
          ) * distanceFactor
        console.log(
          "addressData.location.coordinates",
          addressData.location.coordinates
        )
        console.log("dvlLocation.coordinates", dvlLocation.coordinates)
        console.log("distance", distance)
        data.distance = distance

        let AdmixSettings = await VendorAdMixtureSettingModel.findOne({
          vendor_id: new ObjectId(vendor_id),
        })
        if (!isNullOrUndefined(AdmixSettings)) {
          for (let k = 0; k < AdmixSettings.settings.length; k++) {
            if (
              data.distance > AdmixSettings.settings[k].min_km &&
              data.distance < AdmixSettings.settings[k].max_km
            ) {
              data.admix_price = AdmixSettings.settings[k].price
            } else {
              data.admix_price = 0
            }
          }
          // console.log("products[i].admix_price", products[i].admix_price)
          // products[i].admix_price =
          //   products[i].admix_price * products[i].ad_mixture_quantity
          // products[i].selling_price =
          //   products[i].selling_price + products[i].admix_price
        } else {
          data.admix_price = 0
        }
      }
    }

    const waterData = await WaterRateModel.findOne({
      vendor_id: new ObjectId(vendor_id),
      address_id: new ObjectId(address_id),
    })
    if (!isNullOrUndefined(waterData)) {
      data.water_per_ltr_rate = waterData.per_ltr_rate
    } else {
      data.ErrormsgForAgg = `No rate define for Admix.!`
    }

    console.log("gooooooooooDataa", data)
    return await this.calculatePrice(
      data,
      cement_quantity,
      sand_quantity,
      aggregate1_quantity,
      aggregate2_quantity,
      fly_ash_quantity,
      admix_quantity,
      water_quantity,
      with_CP,
      with_TM,
      delivery_date,
      end_date,
      dvlLocation,
      per_Qubic_meter
    )
  }

  async calculatePrice(
    data: any,
    cement_quantity: number,
    sand_quantity: number,
    aggregate1_quantity: number,
    aggregate2_quantity: number,
    fly_ash_quantity: number,
    admix_quantity: number,
    water_quantity: number,
    with_CP: boolean,
    with_TM: boolean,
    delivery_date: any,
    end_date: any,
    dvlLocation: any,
    per_Qubic_meter: number
  ) {
    console.log("dataaaaa", data)
    /***Cement Total Price Calculate**/
    const cement_price = data.cement_per_kg_rate * cement_quantity
    data.cement_price = cement_price
    data.cement_quantity = cement_quantity

    /***Sand Total Price Calculate**/
    if (!isNullOrUndefined(data.sand_per_kg_rate)) {
      const sand_price = data.sand_per_kg_rate * sand_quantity
      data.sand_price = sand_price
      data.sand_quantity = sand_quantity
    } else {
      data.sand_price = 0
    }

    /***Aggregate1 Total Price Calculate**/
    const aggreagte1_price = data.aggregate1_per_kg_rate * aggregate1_quantity
    data.aggreagte1_price = aggreagte1_price
    data.aggregate1_quantity = aggregate1_quantity

    /***Aggregate2 Total Price Calculate**/
    const aggreagte2_price = data.aggregate2_per_kg_rate * aggregate2_quantity
    data.aggreagte2_price = aggreagte2_price
    data.aggregate2_quantity = aggregate2_quantity

    console.log("data.fly_ash_per_kg_rate", data.fly_ash_per_kg_rate)
    /***Fly Ash Total Price Calculate**/
    if (
      !isNullOrUndefined(data.fly_ash_per_kg_rate) &&
      !isNullOrUndefined(fly_ash_quantity)
    ) {
      const fly_ash_price = data.fly_ash_per_kg_rate * fly_ash_quantity
      data.fly_ash_price = fly_ash_price
      data.fly_ash_quantity = fly_ash_quantity
    } else {
      data.fly_ash_price = 0
    }

    console.log("admix_quantity", admix_quantity)
    /***Admix Total Price Calculate**/
    if (
      !isNullOrUndefined(data.admix_per_kg_rate) &&
      !isNullOrUndefined(admix_quantity)
    ) {
      const admix_price = data.admix_per_kg_rate * admix_quantity
      data.admix_price = admix_price
      data.admix_quantity = admix_quantity
    } else {
      data.admix_price = 0
    }

    /***Water Total Price Calculate**/
    const water_price = data.water_per_ltr_rate * water_quantity
    data.water_price = water_price
    data.water_quantity = water_quantity

    /****Check CP availibilty***************** */
    if (!isNullOrUndefined(with_CP) && with_CP === true) {
      const resp = await this.checkCPavailibity(data.vendor_id, delivery_date)
      if (resp.with_CP === true && resp.msgForCP === message.AvailableCP) {
        data.with_CP = with_CP
        data.CP_price = resp.CP_price
        data.CP_price = data.CP_price.toFixed(2)
        data.CP_price = parseFloat(data.CP_price)
      } else {
        data.ErrMsgForCp = `Sorry, no Concrete pump available on the selected date.`
      }
    } else {
      data.with_CP = false
      data.CP_price = 0
    }

    const addressData = await AddressModel.findById(data.address_id)
    if (!isNullOrUndefined(addressData)) {
      const distance =
        getDistance(addressData.location.coordinates, dvlLocation.coordinates) *
        distanceFactor
      console.log(
        "addressData.location.coordinates",
        addressData.location.coordinates
      )
      console.log("dvlLocation.coordinates", dvlLocation.coordinates)
      console.log("distance", distance)
      data.distance = distance
      /**********Check TM PRICE */
      if (!isNullOrUndefined(with_TM) && with_TM === true) {
        const vendorSettingData = await VendorSettingModel.findOne({
          vendor_id: data.vendor_id,
        })

        if (isNullOrUndefined(per_Qubic_meter)) {
          per_Qubic_meter = 1
        }

        if (!isNullOrUndefined(vendorSettingData)) {
          data.with_TM = with_TM
          data.TM_price =
            vendorSettingData.TM_price * distance * per_Qubic_meter

          data.TM_price = data.TM_price.toFixed(2)
          data.TM_price = parseFloat(data.TM_price)
          data.TM_price_for_unit = vendorSettingData.TM_price * distance
          data.TM_price_for_unit = data.TM_price_for_unit.toFixed(2)
          data.TM_price_for_unit = parseFloat(data.TM_price_for_unit)
        }
      } else {
        data.with_TM = false
        data.TM_price = 0
        data.with_CP = false
        data.CP_price = 0
        data.TM_price_for_unit = 0
      }
    }

    console.log("cement_pice", data.cement_price)
    console.log("sand_price", data.sand_price)
    console.log("aggreagte1_price", data.aggreagte1_price)
    console.log("aggreagte2_price", data.aggreagte2_price)
    console.log("fly_ash_price", data.fly_ash_price)
    console.log("admix_price", data.admix_price)
    console.log("water_price", data.water_price)
    console.log("CP_price", data.CP_price)
    console.log("TM_price", data.TM_price)
    console.log("TM_price_for_unit", data.TM_price_for_unit)

    data.selling_price =
      data.cement_price +
      data.sand_price +
      data.aggreagte1_price +
      data.aggreagte2_price +
      data.fly_ash_price +
      data.admix_price +
      data.water_price +
      data.CP_price +
      data.TM_price

    data.unit_price =
      data.cement_price +
      data.sand_price +
      data.aggreagte1_price +
      data.aggreagte2_price +
      data.fly_ash_price +
      data.admix_price +
      data.water_price +
      data.CP_price +
      data.TM_price_for_unit

    console.log("data.selling_price", data.selling_price)
    console.log("per_Qubic_meter", per_Qubic_meter)
    console.log("unit_price", data.unit_price)
    //data.unit_price = data.selling_price + data.CP_price + data.TM_price
    //data.unit_price = data.selling_price

    data.selling_price = data.selling_price * per_Qubic_meter

    const margin_rate_for_selling = await this.getMarginRate(
      data.selling_price,
      true
    )
    data.margin_price_for_selling =
      (data.selling_price * margin_rate_for_selling) / 100

    const margin_rate_for_unit = await this.getMarginRate(data.unit_price, true)
    data.margin_price_for_unit = (data.unit_price * margin_rate_for_unit) / 100

    data.margin_price_for_unit = data.margin_price_for_unit.toFixed(2)
    data.margin_price_for_unit = parseFloat(data.margin_price_for_unit)

    return data
  }

  async getMarginRate(selling_price: number, maximum?: boolean) {
    const marginData = await MarginRateModel.find()
    let marginSlab = marginData[0].slab

    if (maximum === true) {
      const max = marginSlab.reduce(function (prev, current) {
        return prev.upto < current.upto ? prev : current
      })
      return max.rate
    }
    const slab = marginSlab.sort((a, b) => {
      if (a.upto > b.upto) {
        return -1
      }
      if (a.upto < b.upto) {
        return 1
      }
      return 0
    })
    let returnRate = slab[0].rate
    for (let i = 0; i < slab.length; i++) {
      if (selling_price < slab[i].upto) {
        returnRate = slab[i].rate
      } else {
        break
      }
    }
    console.log("returnRate", returnRate)
    return returnRate
  }

  async getDetails(data: any[]) {
    let resp: any[] = []

    console.log("dataaaaaaaaaaaaaaa", data)
    for (let i = 0; i < data.length; i++) {
      /***********Vendor***************** */
      if (!isNullOrUndefined(data[i].vendor_id)) {
        let vendorData = await VendorUserModel.findOne({
          _id: data[i].vendor_id,
        })
        if (!isNullOrUndefined(vendorData)) {
          data[i].vendor_name = vendorData.full_name
          data[i].company_name = vendorData.company_name
        }

        let vendorMedia = await VendorMediaModel.find({
          vendor_id: data[i].vendor_id,
        })
        if (!isNullOrUndefined(vendorMedia)) {
          data[i].vendor_media = vendorMedia
        }
      }

      /********Concrete grade */
      if (!isNullOrUndefined(data[i].concrete_grade_id)) {
        let cgData = await ConcreteGradeModel.findOne({
          _id: data[i].concrete_grade_id,
        })
        if (!isNullOrUndefined(cgData)) {
          data[i].concrete_grade_name = cgData.name
        }
      }
      /********Cement Grade and Brand */
      if (!isNullOrUndefined(data[i].final_cement_grade_id)) {
        let cementgradeData = await CementGradeModel.findOne({
          _id: data[i].final_cement_grade_id,
        })
        if (!isNullOrUndefined(cementgradeData)) {
          data[i].cement_grade_name = cementgradeData.name
        }
      }
      if (!isNullOrUndefined(data[i].final_cement_brand_id)) {
        let cementBrandData = await CementBrandModel.findOne({
          _id: data[i].final_cement_brand_id,
        })
        if (!isNullOrUndefined(cementBrandData)) {
          data[i].cement_brand_name = cementBrandData.name
        }
      }

      /********Sand********* */
      if (!isNullOrUndefined(data[i].final_sand_source_id)) {
        let sandData = await SandSourceModel.findOne({
          _id: data[i].final_sand_source_id,
        })
        if (!isNullOrUndefined(sandData)) {
          data[i].sand_source_name = sandData.sand_source_name
        }
      }
      /**********Aggregate********* */
      if (!isNullOrUndefined(data[i].final_aggregate1_sub_cat_id)) {
        let aggSubcatData = await AggregateSandSubCategoryModel.findOne({
          _id: data[i].final_aggregate1_sub_cat_id,
        })
        if (!isNullOrUndefined(aggSubcatData)) {
          data[i].aggregate1_sub_category_name = aggSubcatData.sub_category_name
        }
      }

      if (!isNullOrUndefined(data[i].final_aggregate2_sub_cat_id)) {
        let aggSubcatData = await AggregateSandSubCategoryModel.findOne({
          _id: data[i].final_aggregate2_sub_cat_id,
        })
        if (!isNullOrUndefined(aggSubcatData)) {
          data[i].aggregate2_sub_category_name = aggSubcatData.sub_category_name
        }
      }

      if (!isNullOrUndefined(data[i].final_agg_source_id)) {
        let aggSourceData = await AggregateSourceModel.findOne({
          _id: data[i].final_agg_source_id,
        })
        if (!isNullOrUndefined(aggSourceData)) {
          data[i].aggregate_source_name = aggSourceData.aggregate_source_name
        }
      }
      /**************Fly_Ash********************* */
      if (!isNullOrUndefined(data[i].final_fly_ash_source_id)) {
        let flyAshData = await FlyAshSourceModel.findOne({
          _id: data[i].final_fly_ash_source_id,
        })
        if (!isNullOrUndefined(flyAshData)) {
          data[i].fly_ash_source_name = flyAshData.fly_ash_source_name
        }
      }

      /************ Admix******************************** */
      if (!isNullOrUndefined(data[i].final_admix_cat_id)) {
        let admixCatData = await AdmixtureCategoryModel.findOne({
          _id: new ObjectId(data[i].final_admix_cat_id),
        })
        if (!isNullOrUndefined(admixCatData)) {
          data[i].admix_category_name = admixCatData.category_name
        }
      }

      if (!isNullOrUndefined(data[i].final_admix_brand_id)) {
        let admixBrandData = await AdmixtureBrandModel.findOne({
          _id: new ObjectId(data[i].final_admix_brand_id),
        })
        if (!isNullOrUndefined(admixBrandData)) {
          data[i].admix_brand_name = admixBrandData.name
        }
      }

      if (!isNullOrUndefined(data[i].address_id)) {
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(data[i].address_id),
        })
        if (!isNullOrUndefined(addressData)) {
          data[i].line1 = addressData.line1
          data[i].line2 = addressData.line2
          data[i].state_id = addressData.state_id
          data[i].city_id = addressData.city_id
          data[i].pincode = addressData.pincode
          data[i].location = addressData.location
          data[i].address_type = addressData.address_type

          if (!isNullOrUndefined(data[i].state_id)) {
            let stateData = await StateModel.findOne({
              _id: new ObjectId(data[i].state_id._id),
            })
            if (!isNullOrUndefined(stateData)) {
              data[i].state_name = stateData.state_name
            }
          }

          if (!isNullOrUndefined(data[i].city_id)) {
            let cityData = await CityModel.findOne({
              _id: new ObjectId(data[i].city_id._id),
            })
            if (!isNullOrUndefined(cityData)) {
              data[i].city_name = cityData.city_name
            }
          }
        }
      }
      resp.push(data[i])
    }
    return resp
  }

  async transcationFeesCalc(selling_price_with_margin: any) {
    console.log("selling_price_with_margin", selling_price_with_margin)

    let final_price =
      selling_price_with_margin +
      selling_price_with_margin /
        ((100 - transcationFeesPer) / transcationFeesPer)

    return final_price

    //return selling_price_with_margin
  }

  async CheckTime() {
    // time of first timespan
    var x = new Date("01/01/2001 8:30:00").getTime()
    var y = new Date("01/01/2001 9:30:00").getTime()

    // time of second timespan
    var a = new Date("01/01/2001 8:54:00").getTime()
    var b = new Date("01/01/2001 9:00:00").getTime()

    if (Math.min(x, y) <= Math.max(a, b) && Math.max(x, y) >= Math.min(a, b)) {
      console.log("Heree")
      // between
    }
  }

  async getVendorTemplateHtml() {
    console.log("Loading template file in memory")
    try {
      const invoicePath = path.resolve("./Conmix_invoice _suppliar.html")
      return await readFile(invoicePath, "utf8")
    } catch (err) {
      return Promise.reject("Could not load html template")
    }
  }

  async getTemplateHtml() {
    console.log("Loading template file in memory")
    try {
      const invoicePath = path.resolve("./invoice_test.html")
      return await readFile(invoicePath, "utf8")
    } catch (err) {
      return Promise.reject("Could not load html template")
    }
  }

  async generatePdf(order_id: string, track_id: string) {
    console.log("hereeee")
    let vendorOrderData = await VendorOrderModel.findOne({
      _id: order_id,
    })
    if (isNullOrUndefined(vendorOrderData)) {
       return Promise.reject(
        new InvalidInput("Vendor order data not found", 400)
      )
    }
    console.log("vendorOrderData", vendorOrderData)
    let buyerOrderData = await OrderModel.findOne({
      _id: new ObjectId(vendorOrderData.buyer_order_id),
    })
    if (isNullOrUndefined(buyerOrderData)) {
      return Promise.reject(
        new InvalidInput("Buyer order data not found", 400)
      )
    }

    console.log("buyerOrderData", buyerOrderData)

    let orderTrackData = await OrderTrackModel.findOne({
      _id: new ObjectId(track_id),
    })
    if (isNullOrUndefined(orderTrackData)) {
      return Promise.reject(
        new InvalidInput("Order track Details not found", 400)
      )
    }

    console.log("orderTrackData", orderTrackData)

    let orderItemData = await OrderItemModel.findOne({
      _id: new ObjectId(orderTrackData.order_item_id),
    })
    if (isNullOrUndefined(orderItemData)) {
      return Promise.reject(
        new InvalidInput("Order Item Details not found", 400)
      )
    }

    console.log("orderItemData", orderItemData)

    let addressData = await AddressModel.findOne({
      _id: new ObjectId(orderItemData.address_id),
    })
    if (isNullOrUndefined(addressData)) {
      return Promise.reject(
        new InvalidInput("Address Details not found", 400)
      )
    }

    console.log("addressData", addressData)

    let siteData = await SiteModel.findOne({
      _id: new ObjectId(buyerOrderData.site_id),
    })
    if (isNullOrUndefined(siteData)) {
      return Promise.reject(
        new InvalidInput("Site Details not found", 400)
      )
    }
    console.log("siteData", siteData)
    let cityData = await CityModel.findOne({
      _id: new ObjectId(addressData.cityDetails._id),
    })
    if (isNullOrUndefined(cityData)) {
      return Promise.reject(
        new InvalidInput("City Details not found", 400)
      )
    }
    console.log("cityData", cityData)
    let vendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderData.user_id),
    })
    if (isNullOrUndefined(vendorUserData)) {
      return Promise.reject(
        new InvalidInput("Vendor user Details not found", 400)
      )
    }
    console.log("vendorUserData", vendorUserData)
    let masterVendorData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderData.master_vendor_id),
    })

    console.log("masterVendorData", masterVendorData)
    if (isNullOrUndefined(masterVendorData)) {
      return Promise.reject(
        new InvalidInput("Master Vendor user Details not found", 400)
      )
    }

    let vendorBillingAddressData = await BillingAddressModel.findOne({
      _id: new ObjectId(addressData.billing_address_id),
    })
    if (isNullOrUndefined(vendorBillingAddressData)) {
      return Promise.reject(
        new InvalidInput("Vendor billing address Details not found", 400)
      )

    }
    console.log("vendorBillingAddressData", vendorBillingAddressData)
    let buyerBillingAddressData = await BuyerBillingAddressModel.findOne({
      _id: new ObjectId(buyerOrderData.billing_address_id),
    })
    if (isNullOrUndefined(buyerBillingAddressData)) {
      return Promise.reject(
        new InvalidInput("Buyer billing address Details not found", 400)
      )
    }

    let buyerBillingcityData = await CityModel.findOne({
      _id: new ObjectId(buyerBillingAddressData.city_id),
    })
    if (isNullOrUndefined(buyerBillingcityData)) {
      return Promise.reject(
        new InvalidInput("Buyer billing City Details not found", 400)
      )
    }

    let buyerUserData = await BuyerUserModel.findOne({
      _id: new ObjectId(buyerOrderData.user_id),
    })
    if (isNullOrUndefined(buyerUserData)) {
      return Promise.reject(
        new InvalidInput("Buyer Details not found", 400)
      )
    }

    let cgData = await ConcreteGradeModel.findOne({
      _id: new ObjectId(orderItemData.concrete_grade_id),
    })
    if (isNullOrUndefined(cgData)) {
      return Promise.reject(
        new InvalidInput("cg details not found", 400)
      )
    }
    let Date_of_Invoice: any = new Date()
    Date_of_Invoice = Date_of_Invoice.toISOString().slice(0, 10)

    let Delivery_date: any = orderTrackData.start_time
    Delivery_date = Delivery_date.toISOString().slice(0, 10)

    let Order_date: any = buyerOrderData.created_at
    Order_date = Order_date.toISOString().slice(0, 10)

    let buyer_name: any
    if (buyerUserData.account_type !== "Individual") {
      buyer_name = buyerUserData.company_name
    } else {
      buyer_name = buyerUserData.full_name
    }

    let amount: any
    amount =
      orderTrackData.pickup_quantity * orderItemData.unit_price_With_Margin
    amount = amount.toFixed(2)
    amount = parseFloat(amount)

    let resp = await this.calBuyerPrice(amount, orderItemData, buyerOrderData)
    console.log("resp", resp)

    let total_tax = resp.cgst_price + resp.sgst_price + resp.igst_price

    var num = resp.total_amount
    var splittedNum = num.toString().split(".")
    var nonDecimal = splittedNum[0]
    var decimal = splittedNum[1]
    var amount_in_words =
      (await this.price_in_words(Number(nonDecimal))) +
      "and" +
      (await this.price_in_words(Number(decimal))) +
      "paise"

    console.log("amount_in_words", amount_in_words)

    var num1 = total_tax
    var splittedNum1 = num1.toString().split(".")
    var nonDecimal1 = splittedNum1[0]
    var decimal1 = splittedNum1[1]
    var tax_amount_in_words =
      (await this.price_in_words(Number(nonDecimal1))) +
      "and" +
      (await this.price_in_words(Number(decimal1))) +
      "paise"

    console.log("tax_amount_in_words", tax_amount_in_words)

    let data: any = {
      // generate random  number here.
      Invoice_number: generateRandomNumber(),
      Date_of_Invoice,
      Order_id: buyerOrderData.display_id,
      Order_date,
      Delivery_date,

      supplier_site_name: addressData.business_name,
      supplier_site_address_line1: addressData.line1,
      supplier_site_address_line2: addressData.line2,
      supplier_city_name: cityData.city_name,
      supplier_state_name: cityData.state_name,
      supplier_pincode: addressData.pincode,
      supplier_gst_number: vendorBillingAddressData.gst_number,
      supplier_pan_no: masterVendorData.pan_number,
      supplier_mobile_number: vendorUserData.mobile_number,
      supplier_email: vendorUserData.email,

      buyer_name,
      buyer_billing_address_name: buyerBillingAddressData.company_name,
      buyer_billing_address_line1: buyerBillingAddressData.line1,
      buyer_billing_address_line2: buyerBillingAddressData.line2,
      buyer_billing_address_city_name: buyerBillingcityData.city_name,
      buyer_billing_address_state_name: buyerBillingcityData.state_name,
      buyer_billing_address_pincode: buyerBillingAddressData.pincode,
      buyer_gst_number: buyerBillingAddressData.gst_number,
      buyer_pan_no: buyerUserData.pan_number,
      buyer_mobile_number: buyerUserData.mobile_number,
      buyer_email: buyerUserData.email,

      item_name: cgData.name,
      item_quantity: orderTrackData.pickup_quantity,
      item_unit_price: orderItemData.unit_price_With_Margin,
      item_amount: amount,
      after_disc_amount: resp.after_disc_amount,
      disc_per: buyerOrderData.coupon_per,

      item_igst_rate: resp.igst_rate,
      item_cgst_rate: resp.cgst_rate,
      item_sgst_rate: resp.sgst_rate,
      item_igst_price: resp.igst_price,
      item_cgst_price: resp.cgst_price,
      item_sgst_price: resp.sgst_price,
      HSN_code: resp.HSN_Code,
      total_tax,
      total_amount: resp.total_amount,
      coupon_amount: resp.coupon_amount,
      amount_in_words,
      tax_amount_in_words,

      // item_igst_rate: orderItemData.igst_rate,
      // item_cgst_rate: orderItemData.cgst_rate,
      // item_sgst_rate: orderItemData.sgst_rate,
      // item_igst_price: orderItemData.igst_price,
      // item_cgst_price: orderItemData.cgst_price,
      // item_sgst_price: orderItemData.sgst_price,

      // Consigner_Phone: "079 - 1234 5678",
      // Consigner_Email: "info@test.com",
      // Consigner_State: "Gujarat , Code (24)",
      // Consigner_GST_No: "GS123456789",
      // Consigner_PAN_No: "123456BG",
      // Consignee_address:
      //   "102, Sumel II, Near Gurudwara,SG Highway Ahmedabad 380054",
      // Consignee_Phone: "079 - 1234 5678",
      // Consignee_Email: "info@test.com",
      // Consignee_State: "Gujarat , Code (24)",
      // Consignee_GST_No: "GS123456789",
      // Consignee_PAN_No: "123456BG",

      // Description_of_Product: "Aggregate : 10mm",
      // HSN_Code: "1234",
      // Qty: 47.72,
      // Unit: "MT",
      // Price: 750.0,
      // CGST_Rate: "2.50%",
      // CGST_Amount: 894.75,
      // SGST_Rate: "2.50 %",
      // SGST_Amount: 897.75,
      // Amount: "37,579.00",
      // rount_off: 0.5,
      // final_amount: "37,580",
      // grand_total: "47.720 MT",
      // Tax_Rate: "5%",
      // Main_Qty: "47.720",
      // Taxable_Amt: "35,790.00",
      // CGST_Amt: "897.75",
      // SGST_Amt: "894.75",
      // Total_Tax: "1,789.50",
    }

    await this.createPDF(data)

    console.log("data", data)
    return data
    //   if(resp.total_amount > 50000) {
    //   amount = (orderTrackData.pickup_quantity/2) * orderItemData.unit_price
    //   for(let i = 0; i < 2; i++) {
    //     await this.calPrice(amount, orderItemData, buyerOrderData)
    //     await this.createPDF(data)
    //    }
    //   } else {
    //   await this.createPDF(data)
    //  }
  }

  async generateVendorPdf(order_id: string, track_id: string) {
    console.log("hereeee")
    let vendorOrderData = await VendorOrderModel.findOne({
      _id: order_id,
    })
    if (isNullOrUndefined(vendorOrderData)) {
      return Promise.reject(
        new InvalidInput("Vendor order data not found", 400)
      )
    }
    console.log("vendorOrderData", vendorOrderData)
    let buyerOrderData = await OrderModel.findOne({
      _id: new ObjectId(vendorOrderData.buyer_order_id),
    })
    if (isNullOrUndefined(buyerOrderData)) {
      return Promise.reject(
        new InvalidInput("Buyer order data not found", 400)
      )
    }

    console.log("buyerOrderData", buyerOrderData)

    let orderTrackData = await OrderTrackModel.findOne({
      _id: new ObjectId(track_id),
    })
    if (isNullOrUndefined(orderTrackData)) {
      return Promise.reject(
        new InvalidInput("Order track Details not found", 400)
      )
    }

    console.log("orderTrackData", orderTrackData)

    let orderItemData = await OrderItemModel.findOne({
      _id: new ObjectId(orderTrackData.order_item_id),
    })
    if (isNullOrUndefined(orderItemData)) {
      return Promise.reject(
        new InvalidInput("Order Item Details not found", 400)
      )
    }

    console.log("orderItemData", orderItemData)

    let addressData = await AddressModel.findOne({
      _id: new ObjectId(orderItemData.address_id),
    })
    if (isNullOrUndefined(addressData)) {
      return Promise.reject(
        new InvalidInput("Address Details not found", 400)
      )
    }

    console.log("addressData", addressData)

    let siteData = await SiteModel.findOne({
      _id: new ObjectId(buyerOrderData.site_id),
    })
    if (isNullOrUndefined(siteData)) {
      return Promise.reject(
        new InvalidInput("Site Details not found", 400)
      )
    }
    console.log("siteData", siteData)
    let cityData = await CityModel.findOne({
      _id: new ObjectId(addressData.cityDetails._id),
    })
    if (isNullOrUndefined(cityData)) {
      return Promise.reject(
        new InvalidInput("City Details not found", 400)
      )
    }
    console.log("cityData", cityData)
    let vendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderData.user_id),
    })
    if (isNullOrUndefined(vendorUserData)) {
      return Promise.reject(
        new InvalidInput("Vendor user Details not found", 400)
      )
    }

    let masterVendorData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderData.master_vendor_id),
    })

    console.log("masterVendorData", masterVendorData)

    if (isNullOrUndefined(masterVendorData)) {
      return Promise.reject(
        new InvalidInput("Master vendor details not found.", 400)
      )
    }
    console.log("vendorUserData", vendorUserData)
    let vendorBillingAddressData = await BillingAddressModel.findOne({
      _id: new ObjectId(addressData.billing_address_id),
    })
    if (isNullOrUndefined(vendorBillingAddressData)) {
      return Promise.reject(
        new InvalidInput("Vendor billing address Details not found", 400)
      )
    }
    console.log("vendorBillingAddressData", vendorBillingAddressData)
    let buyerBillingAddressData = await BuyerBillingAddressModel.findOne({
      _id: new ObjectId(buyerOrderData.billing_address_id),
    })
    if (isNullOrUndefined(buyerBillingAddressData)) {
      return Promise.reject(
        new InvalidInput("Buyer billing address Details not found", 400)
      )
    }

    let buyerBillingcityData = await CityModel.findOne({
      _id: new ObjectId(buyerBillingAddressData.city_id),
    })
    if (isNullOrUndefined(buyerBillingcityData)) {
      return Promise.reject(
        new InvalidInput("Buyer billing City Details not found", 400)
      )
    }

    let buyerUserData = await BuyerUserModel.findOne({
      _id: new ObjectId(buyerOrderData.user_id),
    })
    if (isNullOrUndefined(buyerUserData)) {
      return Promise.reject(
        new InvalidInput("Buyer Details not found", 400)
      )
    }

    let cgData = await ConcreteGradeModel.findOne({
      _id: new ObjectId(orderItemData.concrete_grade_id),
    })
    if (isNullOrUndefined(cgData)) {
      return Promise.reject(
        new InvalidInput("cg details not found", 400)
      )
    }
    let Date_of_Invoice: any = new Date()
    Date_of_Invoice = Date_of_Invoice.toISOString().slice(0, 10)

    let Delivery_date: any = orderTrackData.start_time
    Delivery_date = Delivery_date.toISOString().slice(0, 10)

    let Order_date: any = buyerOrderData.created_at
    Order_date = Order_date.toISOString().slice(0, 10)

    let buyer_name: any
    if (buyerUserData.account_type !== "Individual") {
      buyer_name = buyerUserData.company_name
    } else {
      buyer_name = buyerUserData.full_name
    }

    let vendorBankInfo = await BankInfoModel.findOne({
      "user.user_id": new ObjectId(vendorOrderData.master_vendor_id),
      //  is_default: true,
    })

    if (isNullOrUndefined(vendorBankInfo)) {
      return Promise.reject(
        new InvalidInput("vendor bank details not found", 400)
      )
    }

    let amount: any
    amount = orderTrackData.pickup_quantity * vendorOrderData.unit_price

    amount = amount.toFixed(2)
    amount = parseFloat(amount)

    let resp = await this.calVendorPrice(
      amount,
      orderItemData,
      buyerOrderData,
      vendorBillingAddressData
    )
    console.log("vendor__resp", resp)

    let total_tax = resp.cgst_price + resp.sgst_price

    var num = resp.total_amount
    var splittedNum = num.toString().split(".")
    var nonDecimal = splittedNum[0]
    var decimal = splittedNum[1]
    var amount_in_words =
      (await this.price_in_words(Number(nonDecimal))) +
      "and" +
      (await this.price_in_words(Number(decimal))) +
      "paise"

    console.log("amount_in_words", amount_in_words)

    var num1 = total_tax
    var splittedNum1 = num1.toString().split(".")
    var nonDecimal1 = splittedNum1[0]
    var decimal1 = splittedNum1[1]
    var tax_amount_in_words =
      (await this.price_in_words(Number(nonDecimal1))) +
      "and" +
      (await this.price_in_words(Number(decimal1))) +
      "paise"

    console.log("tax_amount_in_words", tax_amount_in_words)

    let data: any = {
      // generate random  number here.
      Invoice_number: generateRandomNumber(),
      Date_of_Invoice,
      Order_id: order_id,
      Order_date,
      Delivery_date,

      supplier_site_name: addressData.business_name,
      supplier_site_address_line1: addressData.line1,
      supplier_site_address_line2: addressData.line2,
      supplier_city_name: cityData.city_name,
      supplier_state_name: cityData.state_name,
      supplier_pincode: addressData.pincode,
      supplier_gst_number: vendorBillingAddressData.gst_number,
      supplier_pan_no: masterVendorData.pan_number,
      supplier_mobile_number: vendorUserData.mobile_number,
      supplier_email: vendorUserData.email,
      supplier_name: vendorUserData.full_name,

      buyer_name,
      buyer_billing_address_name: buyerBillingAddressData.company_name,
      buyer_billing_address_line1: buyerBillingAddressData.line1,
      buyer_billing_address_line2: buyerBillingAddressData.line2,
      buyer_billing_address_city_name: buyerBillingcityData.city_name,
      buyer_billing_address_state_name: buyerBillingcityData.state_name,
      buyer_billing_address_pincode: buyerBillingAddressData.pincode,
      buyer_gst_number: buyerBillingAddressData.gst_number,
      buyer_pan_no: buyerUserData.pan_number,
      buyer_mobile_number: buyerUserData.mobile_number,
      buyer_email: buyerUserData.email,

      supplier_bank_name: vendorBankInfo.bank_name,
      supplier_account_holder_name: vendorBankInfo.account_holder_name,
      supplier_account_number: vendorBankInfo.account_number,
      supplier_ifsc: vendorBankInfo.ifsc,
      supplier_account_type: vendorBankInfo.account_type,

      item_name: cgData.name,
      item_quantity: orderTrackData.pickup_quantity,
      item_unit_price: vendorOrderData.unit_price,
      item_amount: amount,

      item_igst_rate: resp.igst_rate,
      item_cgst_rate: resp.cgst_rate,
      item_sgst_rate: resp.sgst_rate,
      item_igst_price: resp.igst_price,
      item_cgst_price: resp.cgst_price,
      item_sgst_price: resp.sgst_price,
      total_tax,
      HSN_code: resp.HSN_Code,
      total_amount: resp.total_amount,
      amount_in_words,
      tax_amount_in_words,
      coupon_amount: buyerOrderData.coupon_amount,

      // item_igst_rate: orderItemData.igst_rate,
      // item_cgst_rate: orderItemData.cgst_rate,
      // item_sgst_rate: orderItemData.sgst_rate,
      // item_igst_price: orderItemData.igst_price,
      // item_cgst_price: orderItemData.cgst_price,
      // item_sgst_price: orderItemData.sgst_price,

      // Consigner_Phone: "079 - 1234 5678",
      // Consigner_Email: "info@test.com",
      // Consigner_State: "Gujarat , Code (24)",
      // Consigner_GST_No: "GS123456789",
      // Consigner_PAN_No: "123456BG",
      // Consignee_address:
      //   "102, Sumel II, Near Gurudwara,SG Highway Ahmedabad 380054",
      // Consignee_Phone: "079 - 1234 5678",
      // Consignee_Email: "info@test.com",
      // Consignee_State: "Gujarat , Code (24)",
      // Consignee_GST_No: "GS123456789",
      // Consignee_PAN_No: "123456BG",

      // Description_of_Product: "Aggregate : 10mm",
      // HSN_Code: "1234",
      // Qty: 47.72,
      // Unit: "MT",
      // Price: 750.0,
      // CGST_Rate: "2.50%",
      // CGST_Amount: 894.75,
      // SGST_Rate: "2.50 %",
      // SGST_Amount: 897.75,
      // Amount: "37,579.00",
      // rount_off: 0.5,
      // final_amount: "37,580",
      // grand_total: "47.720 MT",
      // Tax_Rate: "5%",
      // Main_Qty: "47.720",
      // Taxable_Amt: "35,790.00",
      // CGST_Amt: "897.75",
      // SGST_Amt: "894.75",
      // Total_Tax: "1,789.50",
    }

    await this.createVendorPDF(data)
    return data
  }

  async createPDF(data: any) {
    await this.getTemplateHtml()
      .then(async (res) => {
        // Now we have the html code of our template in res object
        // you can check by logging it on console
        // console.log(res)
        console.log("Compiing the template with handlebars")
        const template = hb.compile(res, {
          strict: true,
        })
        // we have compile our code with handlebars
        const result = template(data)
        // We can use this to add dyamic data to our handlebas template at run time from database or API as per need. you can read the official doc to learn more https://handlebarsjs.com/
        const html = result

        // we are using headless mode
        const browser = await puppeteer.launch()
        const page = await browser.newPage()

        // We set the page content as the generated html by handlebars
        await page.setContent(html)

        // we Use pdf function to generate the pdf in the same folder as this file.
        await page.pdf({
          path: `${data.Invoice_number}.pdf`,
          format: "A4",
          pageRanges: "1",
        })
        await browser.close()
        console.log("PDF Generated")
      })
      .catch((err) => {
        console.error(err)
      })
  }

  async createVendorPDF(data: any) {
    await this.getVendorTemplateHtml()
      .then(async (res) => {
        // Now we have the html code of our template in res object
        // you can check by logging it on console
        // console.log(res)
        console.log("Compiing the template with handlebars")
        const template = hb.compile(res, {
          strict: true,
        })
        // we have compile our code with handlebars
        const result = template(data)
        // We can use this to add dyamic data to our handlebas template at run time from database or API as per need. you can read the official doc to learn more https://handlebarsjs.com/
        const html = result

        // we are using headless mode
        const browser = await puppeteer.launch()
        const page = await browser.newPage()

        // We set the page content as the generated html by handlebars
        await page.setContent(html)

        // we Use pdf function to generate the pdf in the same folder as this file.
        await page.pdf({
          path: `${data.Invoice_number}.pdf`,
          format: "A4",
        })
        await browser.close()
        console.log("PDF Generated")
      })
      .catch((err) => {
        console.error(err)
      })
  }

  async calBuyerPrice(amount: any, orderItemData: any, buyerOrderData: any) {
    let coupon_amount: any
    let after_disc_amount: any

    console.log("pppppp")
    console.log("buyerOrderData", buyerOrderData)
    console.log("buyerOrderData.coupon_per", buyerOrderData.coupon_per)

    // if(!isNullOrUndefined(buyerOrderData.coupon_per)){
    //   coupon_amount = amount * buyerOrderData.coupon_per/100

    //   coupon_amount = coupon_amount.toFixed(2)
    //   coupon_amount = parseFloat(coupon_amount)

    //   after_disc_amount = amount - coupon_amount
    //   after_disc_amount = after_disc_amount.toFixed(2)
    //   after_disc_amount = parseFloat(after_disc_amount)
    // } else {
    //   coupon_amount = 0
    //   after_disc_amount = amount - coupon_amount
    //   after_disc_amount = after_disc_amount.toFixed(2)
    //   after_disc_amount = parseFloat(after_disc_amount)
    // }

    console.log("buyerOrderData.coupon_amount", buyerOrderData.coupon_amount)
    if (!isNullOrUndefined(buyerOrderData.coupon_amount)) {
      coupon_amount =
        (amount * buyerOrderData.coupon_amount) /
        buyerOrderData.selling_price_With_Margin

      coupon_amount = coupon_amount.toFixed(2)
      coupon_amount = parseFloat(coupon_amount)

      after_disc_amount = amount - coupon_amount
      after_disc_amount = after_disc_amount.toFixed(2)
      after_disc_amount = parseFloat(after_disc_amount)
    } else {
      coupon_amount = 0
      after_disc_amount = amount - coupon_amount
      after_disc_amount = after_disc_amount.toFixed(2)
      after_disc_amount = parseFloat(after_disc_amount)
    }
    console.log("coupon_amount", coupon_amount)
    const gst_slab = await GstSlabModel.findOne()
    console.log("gst", gst_slab)
    if (!isNullOrUndefined(gst_slab)) {
      let cgst_rate = gst_slab.cgst_rate
      let sgst_rate = gst_slab.sgst_rate
      let igst_rate = gst_slab.igst_rate
      let HSN_Code = gst_slab.hsn_code

      console.log("cgst_rate", cgst_rate)
      console.log("sgst_rate", sgst_rate)
      console.log("igst_rate", igst_rate)

      let cgst_price: any = (after_disc_amount * cgst_rate) / 100
      cgst_price = cgst_price.toFixed(2)
      cgst_price = parseFloat(cgst_price)

      let sgst_price: any = (after_disc_amount * sgst_rate) / 100
      sgst_price = sgst_price.toFixed(2)
      sgst_price = parseFloat(sgst_price)

      let igst_price: any = (after_disc_amount * igst_rate) / 100
      igst_price = igst_price.toFixed(2)
      igst_price = parseFloat(igst_price)

      let gst_price = cgst_price + sgst_price
      console.log("gst_price", gst_price)
      let total_amount: any
      if (!isNullOrUndefined(buyerOrderData.billing_address_id)) {
        console.log("heree")
        const billingAddressInfo = await BuyerBillingAddressModel.findOne({
          _id: new ObjectId(buyerOrderData.billing_address_id),
        })
        console.log("billingAddressInfo", billingAddressInfo)
        // cartData.billingAddressInfo = await this.getBillingAddressInfo(cartData.billing_address_id)
        // let gst_number = billingAddressInfo.gst_number
        // if (!isNullOrUndefined(gst_number)) {
        //   let no = gst_number.slice(0, 2)
        //   console.log("no", no)
        //   let num = parseInt(no)
        //   console.log("num", num)
        //   if (num === 24) {
        //     cgst_price = cgst_price
        //     sgst_price = sgst_price
        //     igst_price = 0
        //     gst_price = cgst_price + sgst_price
        //   } else {
        //     cgst_price = 0
        //     sgst_price = 0
        //     igst_price = igst_price
        //     gst_price = igst_price
        //   }
        // }
        if (!isNullOrUndefined(billingAddressInfo)) {
          let stateDetails = await StateModel.findOne({
            _id: new ObjectId(billingAddressInfo.state_id),
          })
          if (!isNullOrUndefined(stateDetails)) {
            console.log("state", stateDetails)
            if (stateDetails.state_name === "Gujarat") {
              cgst_price = cgst_price
              sgst_price = sgst_price
              igst_price = 0
              gst_price = cgst_price + sgst_price
            } else {
              cgst_price = 0
              sgst_price = 0
              igst_price = igst_price
              gst_price = igst_price
            }
          }
        } else {
          cgst_price = cgst_price
          sgst_price = sgst_price
          igst_price = 0
          gst_price = cgst_price + sgst_price
        }
      }
      console.log("gst_price", gst_price)
      gst_price = gst_price.toFixed(2)
      gst_price = parseFloat(gst_price)

      total_amount = after_disc_amount + gst_price

      total_amount = total_amount.toFixed(2)
      total_amount = parseFloat(total_amount)
      console.log("total_amount", total_amount)

      let resp: any = {
        after_disc_amount,
        coupon_amount,
        cgst_rate,
        igst_rate,
        sgst_rate,
        cgst_price,
        igst_price,
        sgst_price,
        gst_price,
        total_amount,
        HSN_Code,
      }
      return resp
    }
  }
  async calVendorPrice(
    amount: any,
    orderItemData: any,
    buyerOrderData: any,
    vendorBillingAddressData: any
  ) {
    // let coupon_amount = amount * buyerOrderData.coupon_per/100
    // amount = amount -= coupon_amount
    const gst_slab = await GstSlabModel.findOne()
    console.log("gst", gst_slab)
    if (!isNullOrUndefined(gst_slab)) {
      let cgst_rate = gst_slab.cgst_rate
      let sgst_rate = gst_slab.sgst_rate
      let igst_rate = gst_slab.igst_rate
      let HSN_Code = gst_slab.hsn_code

      console.log("cgst_rate", cgst_rate)
      console.log("sgst_rate", sgst_rate)
      console.log("igst_rate", igst_rate)

      let cgst_price: any = (amount * cgst_rate) / 100
      cgst_price = cgst_price.toFixed(2)
      cgst_price = parseFloat(cgst_price)

      let sgst_price: any = (amount * sgst_rate) / 100
      sgst_price = sgst_price.toFixed(2)
      sgst_price = parseFloat(sgst_price)

      let igst_price: any = (amount * igst_rate) / 100
      igst_price = igst_price.toFixed(2)
      igst_price = parseFloat(igst_price)

      let gst_price = cgst_price + sgst_price
      console.log("gst_price", gst_price)
      let total_amount: any

      console.log("vendorBillingAddressData", vendorBillingAddressData)
      if (!isNullOrUndefined(vendorBillingAddressData)) {
        let stateDetails = await StateModel.findOne({
          _id: new ObjectId(vendorBillingAddressData.state_id),
        })
        if (!isNullOrUndefined(stateDetails)) {
          console.log("state", stateDetails)
          if (stateDetails.state_name === "Gujarat") {
            cgst_price = cgst_price
            sgst_price = sgst_price
            igst_price = 0
            gst_price = cgst_price + sgst_price
          } else {
            cgst_price = 0
            sgst_price = 0
            igst_price = igst_price
            gst_price = igst_price
          }
        }
      } else {
        cgst_price = cgst_price
        sgst_price = sgst_price
        igst_price = 0
        gst_price = cgst_price + sgst_price
      }

      // if (!isNullOrUndefined(buyerOrderData.billing_address_id)) {
      //   console.log("heree")
      //   const billingAddressInfo = await BuyerBillingAddressModel.findOne({
      //     _id: new ObjectId(buyerOrderData.billing_address_id),
      //   })
      //   console.log("billingAddressInfo", billingAddressInfo)
      //   // cartData.billingAddressInfo = await this.getBillingAddressInfo(cartData.billing_address_id)
      //   // let gst_number = billingAddressInfo.gst_number
      //   // if (!isNullOrUndefined(gst_number)) {
      //   //   let no = gst_number.slice(0, 2)
      //   //   console.log("no", no)
      //   //   let num = parseInt(no)
      //   //   console.log("num", num)
      //   //   if (num === 24) {
      //   //     cgst_price = cgst_price
      //   //     sgst_price = sgst_price
      //   //     igst_price = 0
      //   //     gst_price = cgst_price + sgst_price
      //   //   } else {
      //   //     cgst_price = 0
      //   //     sgst_price = 0
      //   //     igst_price = igst_price
      //   //     gst_price = igst_price
      //   //   }
      //   // }
      //   if (!isNullOrUndefined(billingAddressInfo)) {
      //     let stateDetails = await StateModel.findOne({
      //       _id: new ObjectId(billingAddressInfo.state_id),
      //     })
      //     if (!isNullOrUndefined(stateDetails)) {
      //       console.log("state", stateDetails)
      //       if (stateDetails.state_name === "Gujarat") {
      //         cgst_price = cgst_price
      //         sgst_price = sgst_price
      //         igst_price = 0
      //         gst_price = cgst_price + sgst_price
      //       } else {
      //         cgst_price = 0
      //         sgst_price = 0
      //         igst_price = igst_price
      //         gst_price = igst_price
      //       }
      //     }
      //   } else {
      //     cgst_price = cgst_price
      //     sgst_price = sgst_price
      //     igst_price = 0
      //     gst_price = cgst_price + sgst_price
      //   }
      // }
      console.log("gst_price", gst_price)
      gst_price = gst_price.toFixed(2)
      gst_price = parseFloat(gst_price)

      total_amount = amount + gst_price

      total_amount = total_amount.toFixed(2)
      total_amount = parseFloat(total_amount)
      console.log("total_amount", total_amount)

      let resp: any = {
        cgst_rate,
        igst_rate,
        sgst_rate,
        cgst_price,
        igst_price,
        sgst_price,
        gst_price,
        total_amount,
        HSN_Code,
      }
      return resp
    }
  }
}
