import * as querystring from "querystring"

export function generateSMSMessage(code: number | string) {
  return `Hi. Your Verification Code is ${code}. CONMIX!`
}
export function generateCallURL(code: number | string) {
  const codeText = code
    .toString()
    .split("")
    .reduce((str, val) => str + `${val}. . `, "")
  const message = `. . . . . . . . . . Hello, from Spotter. . Your Verification Code is ${codeText} Thank You.`
  return `http://twimlets.com/message?${querystring.escape(
    "Message[0]"
  )}=${querystring.escape(message)}&`
}
