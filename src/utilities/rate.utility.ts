import { ObjectID } from "bson"

export function getMarginRate(
  amount: number,
  marginSlab: [
    {
      upto: number
      rate: number
    }
  ],
  maximum?: boolean
) {
  if (maximum === true) {
    const max = marginSlab.reduce(function (prev, current) {
      return prev.upto < current.upto ? prev : current
    })
    return max.rate
  } else {
    const slab = marginSlab.sort((a, b) => {
      if (a.upto > b.upto) {
        return -1
      }
      if (a.upto < b.upto) {
        return 1
      }
      return 0
    })
    let returnRate = slab[0].rate
    for (let i = 0; i < slab.length; i++) {
      if (amount < slab[i].upto) {
        returnRate = slab[i].rate
      } else {
        break
      }
    }
    return returnRate
  }
}

export function getAvgTripCosts(
  distance: number,
  vehicles: {
    vehicle_category_id: string | ObjectID
    delivery_range: number
    pickup_location: any
    calculated_distance: number
    vehicle_sub_category_id: string | ObjectID
    per_metric_ton_per_km_rate: number
    min_trip_price: number
    vehicleSubCategory: {
      _id: string | ObjectID
      min_load_capacity: number
      sub_category_name: string
      max_load_capacity: number
      weight_unit: string
      weight_unit_code: string
    }
  }[]
) {
  const outVehicle: {
    [k: string]: {
      vehicle_category_id: string | ObjectID
      delivery_range: number
      pickup_location: any
      calculated_distance: number
      vehicle_sub_category_id: string | ObjectID
      per_metric_ton_per_km_rate: number
      min_trip_price: number
      logistics_price: number
      vehicleCount: number
      vehicleSubCategory: {
        min_load_capacity: number
        _id: string | ObjectID
        sub_category_name: string
        max_load_capacity: number
        weight_unit: string
        weight_unit_code: string
        [k: string]: any
      }
      [k: string]: any
    }
  } = {}
  console.log(`getAvgTripCosts() ==================================`)
  vehicles.map((v) => {
    const vSubCatId = "" + v.vehicle_sub_category_id
    let currentTripCost =
      v.vehicleSubCategory.min_load_capacity *
      distance *
      v.per_metric_ton_per_km_rate
    console.log(
      `currentTripCost: ${currentTripCost}, v.min_trip_price:${v.min_trip_price}`
    )
    currentTripCost =
      currentTripCost > v.min_trip_price ? currentTripCost : v.min_trip_price
    console.log(`currentTripCost: ${currentTripCost}`)
    outVehicle[vSubCatId] = outVehicle[vSubCatId] || v
    const prevSum = outVehicle[vSubCatId].logistics_price || 0
    const prevVehicleCount = outVehicle[vSubCatId].vehicleCount || 0
    console.log(`prevSum: ${prevSum}, prevCount:${prevVehicleCount}`)
    outVehicle[vSubCatId].logistics_price =
      (prevSum * prevVehicleCount + currentTripCost) / (prevVehicleCount + 1)
    outVehicle[vSubCatId].vehicleCount = prevVehicleCount + 1
    console.log(`logistics_price:${outVehicle[vSubCatId].logistics_price}`)
  })
  return Object.values(outVehicle)
}
