export const distanceFactor = 1.33 // percentage value that will increase on straight distance

/**
 *
 * @param point1 array of number [long,lat]
 * @param point2 array of number [long,lat]
 * @param unit   where: 'K' is kilometers (default)
 *                      'M' is statute miles
 *                      'N' is nautical miles
 * @returns number (default in KM)
 */

export function getDistance(
  point1: number[],
  point2: number[],
  unit = "K"
): number {
  // var radlat1 = Math.PI * lat1/180
  var radlat1 = (Math.PI * point1[1]) / 180
  // var radlat2 = Math.PI * lat2/180
  var radlat2 = (Math.PI * point2[1]) / 180
  var theta = point1[0] - point2[0]
  var radtheta = (Math.PI * theta) / 180
  var dist =
    Math.sin(radlat1) * Math.sin(radlat2) +
    Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
  if (dist > 1) {
    dist = 1
  }
  dist = Math.acos(dist)
  dist = (dist * 180) / Math.PI
  dist = dist * 60 * 1.1515
  if (unit === "K") {
    dist = dist * 1.609344
  }
  if (unit === "N") {
    dist = dist * 0.8684
  }
  // let dist1 = number_format(dist, 2)
  // console.log("dddddddddddddddddddddddddddddddddddddd", dist1)
  return dist
}

export function number_format(val: any, decimals: any) {
  //Parse the value as a float value
  // val = parseFloat(val)
  //Format the value w/ the specified number
  //of decimal places and return it.
  return val.toFixed(decimals)
}
