import { CPOrderTrackModel } from "./../model/CP_order_track.model"
import {
  VendorNotificationType,
  AdminNotificationType,
} from "./../model/notification.model"
import { ObjectID } from "bson"
import { injectable } from "inversify"
import * as admin from "firebase-admin"
import { resolve } from "path"
import { isNullOrUndefined } from "./type-guards"
import { UserType, apnConfig } from "./config"
import { INewNotification } from "../buyer/notification/buyer.notification.repository"
import { FcmTokensModel } from "../model/fcm-tokens.model"
import { FcmTokenUtility } from "./fcm-token.utilities"
import {
  NotificationModel,
  ClientNotificationType,
} from "../model/notification.model"
import { BuyerUserModel } from "../model/buyer.user.model"
import { AdminUserModel } from "../model/admin.user.model"
import * as apn from "apn"
import { OrderItemModel } from "../model/order_item.model"
import { LogisticsUserModel } from "../model/logistics.user.model"
import { VendorUserModel } from "../model/vendor.user.model"
import { VendorOrderModel } from "../model/vendor.order.model"
import path = require("path")
import { OrderModel } from "../model/order.model"
import { OrderTrackModel } from "../model/order_track.model"
import { SiteModel } from "../model/site.model"
import { SupportTicketModel } from "../model/support_ticket.model"
//import { SiteModel } from "../model/site.model"

const serviceAccountKeyPath = resolve(__dirname, "../serviceAccountKey.json")

const firebaseAdmin = admin.initializeApp({
  credential: admin.credential.cert(serviceAccountKeyPath),
  //  databaseURL: "https://conmatebuyerapp-ios.firebaseio.com",
})

let apnOptions = {
  token: {
    key: path.join(__dirname, "../cert_notifications.p8"),
    keyId: apnConfig.keyId,
    teamId: apnConfig.teamId,
  },
  production: false,
}

@injectable()
export class NotificationUtility {
  // apnProvider: apn.Provider
  // constructor(
  //   @inject(BuyerTypes.FcmTokensRepository)
  //   private fcmTokenRepo: FcmTokensRepository
  // ) {
  //   this.apnProvider = new apn.Provider(apnOptions)
  // }
  apnProvider: apn.Provider
  private FcmTokenUtil: FcmTokenUtility
  constructor() {
    this.FcmTokenUtil = new FcmTokenUtility()
    this.apnProvider = new apn.Provider(apnOptions)
  }

  // async sendToAPNs(
  //   message: string,
  //   payloadData: INewNotification,
  //   deviceTokens: { apn_token: string; _id: ObjectID | string }[]
  // ) {
  //   return Promise.all(
  //     deviceTokens.map((dt) => {
  //       if (dt.apn_token === "") {
  //         return Promise.resolve()
  //       }
  //       // console.log(`Sending APN notification to token_id: ${dt._id}`)
  //       const notification = new apn.Notification({
  //         sound: "default",
  //         alert: message,
  //         topic: apnConfig.topic,
  //         payload: { data: this.generatePayload(payloadData) },
  //       })
  //       return this.apnProvider
  //         .send(notification, dt.apn_token)
  //         .then((res) => {
  //           if (res.failed.length > 0) {
  //             this.fcmTokenRepo.handleFailedAPNsToken(res.failed, dt._id)
  //           }
  //           return Promise.resolve()
  //         })
  //         .catch((err) => {
  //           console.log(`[ERROR] Unable to send notification. ${err.message}`)
  //           return Promise.resolve()
  //         })
  //     })
  //   )
  //}

  // async sendToFcmToken(
  //   message: string,
  //   payloadData: INewNotification,
  //   fcmTokens: { token: string }[]
  // ) {
  //   if (fcmTokens.length === 0) {
  //     return Promise.resolve()
  //   }
  //   const notificationTitle = payloadData.notificationTitle || "Yippee"
  //   payloadData.body = message
  //   payloadData.title = notificationTitle
  //   payloadData.clickAction = ".MainActivity"
  //   Object.keys(payloadData).forEach((k) => {
  //     payloadData[k] = "" + payloadData[k]
  //   })
  //   Promise.all(
  //     fcmTokens.map(async (f) => {
  //       var messageObj = {
  //         android: {
  //           data: payloadData,
  //         },
  //         apns: {
  //           // headers: { "apns-priority": "10" },
  //           payload: {
  //             aps: {
  //               alert: { title: notificationTitle, body: message },
  //               sound: "default",
  //               "mutable-content": 1,
  //               "attachment-url": payloadData["attachment-url"],
  //             },
  //             data: payloadData,
  //           },
  //         },
  //         token: f.token,
  //       }
  //       return firebaseAdmin
  //         .messaging()
  //         .send(messageObj)
  //         .then()
  //         .catch(async (error) => {
  //           if (
  //             [
  //               "messaging/invalid-registration-token",
  //               "messaging/registration-token-not-registered",
  //               "messaging/invalid-recipient",
  //             ].includes(error.code)
  //           ) {
  //             console.log(
  //               `Removing expired token. user id: ${payloadData.to_user_id}. token: ${f.token}`
  //             )
  //             return this.fcmTokenRepo.removeExpiredTokens(
  //               payloadData.to_user_id,
  //               [f.token]
  //             )
  //           }
  //           return Promise.reject(error)
  //         })
  //     })
  //   )
  //     // .then(resp => {
  //     //   console.log(`FCM sent. res:`, resp)
  //     // })
  //     .catch((err) => {
  //       console.log(`Unable to send message to fcm. Error`, err)
  //     })
  // }
  // async send(message: string, payloadData: INewNotification) {
  //   const deviceList = await FcmTokensModel.find({
  //     user_id: payloadData.to_user_id,
  //   })
  //   if (deviceList.length === 0) {
  //     return Promise.resolve()
  //   }
  //   const fcmTokens = deviceList
  //     .filter((userCheck) => isNullOrUndefined(userCheck.apn_token))
  //     .map((fcmUsr) => {
  //       return { token: fcmUsr.token, _id: fcmUsr._id }
  //     })
  //   const apnTokens = deviceList
  //     .filter((userCheck) => !isNullOrUndefined(userCheck.apn_token))
  //     .map((apnUser) => ({
  //       apn_token: apnUser.apn_token || "",
  //       _id: apnUser._id,
  //     }))
  //   if (fcmTokens.length > 0) {
  //     await this.sendToFcmToken(message, payloadData, fcmTokens)
  //   }
  //   if (apnTokens.length > 0) {
  //     await this.sendToAPNs(message, payloadData, apnTokens)
  //   }
  //   return Promise.resolve()
  // }

  /** Converts name value pairs in string */
  // generatePayload(obj: { [k: string]: any }) {
  //   const outObj: { [k: string]: string } = {}
  //   for (let [key, val] of Object.entries(obj)) {
  //     outObj[key] = "" + val
  //   }
  //   return outObj
  // }

  //********************************************************************** */

  async send(message: string, payloadData: INewNotification) {
    const deviceList = await FcmTokensModel.find({
      user_id: payloadData.to_user_id,
    })

    console.log("deviceList", deviceList)
    if (deviceList.length === 0) {
      return Promise.resolve()
    }
    const fcmTokens = deviceList
      .filter((userCheck) => isNullOrUndefined(userCheck.apn_token))
      .map((fcmUsr) => {
        return { token: fcmUsr.token, _id: fcmUsr._id }
      })
    const apnTokens = deviceList
      .filter((userCheck) => !isNullOrUndefined(userCheck.apn_token))
      .map((apnUser) => ({
        apn_token: apnUser.apn_token || "",
        _id: apnUser._id,
      }))

    console.log("fcmTokens", fcmTokens)
    if (fcmTokens.length > 0) {
      await this.sendToFcmToken(message, payloadData, fcmTokens)
    }

    console.log("apnTokens", apnTokens)
    if (apnTokens.length > 0) {
      await this.sendToAPNs(message, payloadData, apnTokens)
    }
    return Promise.resolve()
  }

  async sendToAPNs(
    message: string,
    payloadData: INewNotification,
    deviceTokens: { apn_token: string; _id: ObjectID | string }[]
  ) {
    return Promise.all(
      deviceTokens.map((dt) => {
        if (dt.apn_token === "") {
          return Promise.resolve()
        }

        console.log("apnConfig", apnOptions)
        // console.log(`Sending APN notification to token_id: ${dt._id}`)
        const notification = new apn.Notification({
          sound: "default",
          alert: message,
          topic: apnConfig.topic,
          payload: { data: this.generatePayload(payloadData) },
        })
        console.log("dt", dt)
        console.log("notification", notification)
        return this.apnProvider
          .send(notification, dt.apn_token)
          .then((res: any) => {
            if (res.failed.length > 0) {
              console.log("Hereeeee")
              this.FcmTokenUtil.handleFailedAPNsToken(res.failed, dt._id)
            }
            return Promise.resolve()
          })

          .catch((err: any) => {
            console.log(`[ERROR] Unable to send notification. ${err.message}`)
            return Promise.resolve()
          })
      })
    )
  }

  async sendToFcmToken(
    message: string,
    payloadData: INewNotification,
    fcmTokens: { token: string }[]
  ) {
    if (fcmTokens.length === 0) {
      return Promise.resolve()
    }
    const notificationTitle = payloadData.notificationTitle || "Conmix"
    payloadData.body = message
    payloadData.title = notificationTitle
    payloadData.clickAction = ".MainActivity"
    Object.keys(payloadData).forEach((k) => {
      payloadData[k] = "" + payloadData[k]
    })
    Promise.all(
      fcmTokens.map(async (f) => {
        var messageObj = {
          android: {
            data: payloadData,
          },
          apns: {
            // headers: { "apns-priority": "10" },
            payload: {
              aps: {
                alert: { title: notificationTitle, body: message },
                sound: "default",
                "mutable-content": 1,
                "attachment-url": payloadData["attachment-url"],
              },
              data: payloadData,
            },
          },
          token: f.token,
        }

        console.log("messageObj", messageObj)
        return firebaseAdmin
          .messaging()
          .send(messageObj)
          .then()
          .catch(async (error) => {
            if (
              error.code === "messaging/invalid-registration-token" ||
              error.code === "messaging/registration-token-not-registered"
            ) {
              console.log(
                `Removing expired token. user id: ${payloadData.to_user_id}. token: ${f.token}`
              )
              return this.FcmTokenUtil.removeExpiredTokens(
                payloadData.to_user_id,
                [f.token]
              )
            }

            console.log("errorsssssssssssssss", error)
            return Promise.reject(error)
          })
      })
    )
      // .then(resp => {
      //   console.log(`FCM sent. res:`, resp)
      // })
      .catch((err) => {
        console.log(`Unable to send message to fcm. Error`, err)
      })
  }

  generatePayload(obj: { [k: string]: any }) {
    let outObj: { [k: string]: string } = {}
    for (let [key, val] of Object.entries(obj)) {
      outObj[key] = "" + val
    }
    return outObj
  }

  async addNotificationForAdmin(
    notificationObj: INewNotification
  ): Promise<any> {
    if (isNullOrUndefined(notificationObj.to_user_id)) {
      return Promise.resolve()
    }
    return Promise.all([
      AdminUserModel.updateOne(
        { _id: notificationObj.to_user_id },
        { $inc: { notification_count: 1, unseen_message_count: 1 } }
      ),
      new NotificationModel(notificationObj).save(),
      this.sendNotificationToSns(notificationObj),
    ])
  }

  async addNotificationForBuyer(
    notificationObj: INewNotification
  ): Promise<any> {
    if (isNullOrUndefined(notificationObj.to_user_id)) {
      return Promise.resolve()
    }

    console.log("notificationObj", notificationObj)
    return Promise.all([
      BuyerUserModel.updateOne(
        { _id: notificationObj.to_user_id },
        { $inc: { notification_count: 1, unseen_message_count: 1 } }
      ),

      new NotificationModel(notificationObj).save(),
      this.sendNotificationToSns(notificationObj),
    ])
  }

  async addNotificationForLogi(
    notificationObj: INewNotification
  ): Promise<any> {
    console.log("notificationObj", notificationObj)
    if (isNullOrUndefined(notificationObj.to_user_id)) {
      return Promise.resolve()
    }

    console.log("Add into logiiiiii")
    return Promise.all([
      LogisticsUserModel.updateOne(
        { _id: notificationObj.to_user_id },
        { $inc: { notification_count: 1, unseen_message_count: 1 } }
      ),

      new NotificationModel(notificationObj).save(),
      this.sendNotificationToSns(notificationObj),
    ])
  }

  async addNotificationForSupplier(
    notificationObj: INewNotification
  ): Promise<any> {
    if (isNullOrUndefined(notificationObj.to_user_id)) {
      return Promise.resolve()
    }
    return Promise.all([
      VendorUserModel.updateOne(
        { _id: notificationObj.to_user_id },
        { $inc: { notification_count: 1, unseen_message_count: 1 } }
      ),
      new NotificationModel(notificationObj).save(),
      this.sendNotificationToSns(notificationObj),
    ])
  }

  async sendNotificationToSns(notificationObj: INewNotification): Promise<any> {
    console.log("senttttttttttt")
    console.log("notificationObj", notificationObj)
    let userData: any

    if (notificationObj.to_user_type === UserType.BUYER) {
      userData = await BuyerUserModel.findById(notificationObj.to_user_id, {
        notification_opted: 1,
        company_type: 1,
        full_name: 1,
        company_name: 1,
        account_type: 1,
      })

      if (userData === null || userData.notification_opted === false) {
        return Promise.resolve() // User has not opted notification
      }
    }

    if (notificationObj.to_user_type === UserType.VENDOR) {
      userData = await VendorUserModel.findById(notificationObj.to_user_id, {
        notification_opted: 1,
      })
      if (userData === null || userData.notification_opted === false) {
        return Promise.resolve() // User has not opted notification
      }
    }

    if (notificationObj.to_user_type === UserType.ADMIN) {
      userData = await AdminUserModel.findById(notificationObj.to_user_id, {
        notification_opted: 1,
      })
      if (userData === null || userData.notification_opted === false) {
        return Promise.resolve() // User has not opted notification
      }
    }

    // if (notificationObj.to_user_type === UserType.LOGISTICS) {
    //   userData = await LogisticsUserModel.findById(notificationObj.to_user_id, {
    //     notification_opted: 1,
    //   })
    //   if (userData === null || userData.notification_opted === false) {
    //     return Promise.resolve() // User has not opted notification
    //   }
    // }

    let message = ""

    let receiverData: any
    let buyerOrderData: any
    let orderData: any
    //let vehicleData: any
    let vendorOrderData: any
    let clientData: any
    let vendorData: any
    let orderTrackData: any
    let CPorderTrackData: any
    let siteData: any
    let ticketData: any
    //let logiscticsOrderData: any
    receiverData = await BuyerUserModel.findById(notificationObj.to_user_id, {
      full_name: 1,
      company_type: 1,
      company_name: 1,
      account_type: 1,
    })

    if (
      notificationObj.to_user_type === UserType.BUYER ||
      notificationObj.to_user_type === UserType.ADMIN
    ) {
      buyerOrderData = await OrderModel.findOne({
        _id: notificationObj.order_id,
      }).select({ display_id: 1 })

      siteData = await SiteModel.findOne({
        _id: notificationObj.site_id,
      }).select({ site_name: 1 })

      orderData = await OrderItemModel.findOne({
        order_id: notificationObj.order_id,
      }).select({ display_item_id: 1 })

      clientData = await BuyerUserModel.findOne({
        _id: notificationObj.client_id,
      }).select({
        company_name: 1,
        full_name: 1,
        company_type: 1,
        account_type: 1,
      })

      vendorData = await VendorUserModel.findOne({
        _id: notificationObj.vendor_id,
      }).select({ full_name: 1, company_name: 1 })

      orderTrackData = await OrderTrackModel.findOne({
        order_item_id: notificationObj.item_id,
      }).select({ _id: 1, reasonForDelay: 1, delayTime: 1 })

      CPorderTrackData = await CPOrderTrackModel.findOne({
        order_item_id: notificationObj.item_id,
      }).select({ _id: 1, reasonForDelay: 1, delayTime: 1 })

      ticketData = await SupportTicketModel.findOne({
        _id: notificationObj.ticket_id,
      }).select({ _id: 1, ticket_id: 1 })
    }

    if (notificationObj.to_user_type === UserType.VENDOR) {
      receiverData = await VendorUserModel.findById(
        notificationObj.to_user_id,
        {
          company_name: 1,
        }
      )
      vendorData = await VendorUserModel.findOne({
        _id: notificationObj.to_user_id,
      }).select({ full_name: 1, company_name: 1 })

      vendorOrderData = await VendorOrderModel.findOne({
        _id: notificationObj.vendor_order_id,
      }).select({ _id: 1, buyer_order_display_item_id: 1 })

      orderData = await OrderItemModel.findOne({
        order_id: notificationObj.order_id,
      }).select({ display_item_id: 1 })

      clientData = await BuyerUserModel.findOne({
        _id: notificationObj.client_id,
      }).select({
        company_name: 1,
        full_name: 1,
        company_type: 1,
        account_type: 1,
      })

      orderTrackData = await OrderTrackModel.findOne({
        order_item_id: notificationObj.item_id,
      }).select({ _id: 1, reasonForDelay: 1, delayTime: 1 })

      CPorderTrackData = await CPOrderTrackModel.findOne({
        order_item_id: notificationObj.item_id,
      }).select({ _id: 1, reasonForDelay: 1, delayTime: 1 })

      siteData = await SiteModel.findOne({
        _id: notificationObj.site_id,
      }).select({ site_name: 1 })

      buyerOrderData = await OrderModel.findOne({
        _id: notificationObj.order_id,
      }).select({ display_id: 1 })

      ticketData = await SupportTicketModel.findOne({
        _id: notificationObj.ticket_id,
      }).select({ _id: 1, ticket_id: 1 })
    }

    if (notificationObj.to_user_type === UserType.ADMIN) {
      receiverData = await AdminUserModel.findById(notificationObj.to_user_id, {
        full_name: 1,
      })
    }
    console.log("clientData", clientData)
    console.log("receiverData", receiverData)
    console.log("notificationObj", notificationObj)
    console.log("userData", userData)
    console.log("orderTrackData", orderTrackData)

    let client_name: any
    if (!isNullOrUndefined(clientData)) {
      console.log("1111")
      if (clientData.account_type === "Individual") {
        client_name = clientData.full_name
      } else {
        client_name = clientData.company_name
      }
    } else if (!isNullOrUndefined(receiverData)) {
      console.log("22222")
      if (receiverData.account_type === "Individual") {
        client_name = receiverData.full_name
      } else {
        client_name = receiverData.company_name
      }
    } else if (!isNullOrUndefined(userData)) {
      console.log("3333")
      if (userData.account_type === "Individual") {
        client_name = userData.full_name
      } else {
        client_name = userData.company_name
      }
    }

    console.log("client_name", client_name)

    console.log("vendorData", vendorData)

    // if (
    //   (!isNullOrUndefined(clientData) &&
    //   clientData.company_type === "Individual" ) ||
    //   (!isNullOrUndefined(receiverData) &&
    //   receiverData.company_type === "Individual") ||
    //   (!isNullOrUndefined(userData) &&
    //   userData.company_type === "Individual")){
    //   clientData.full_name = clientData.full_name
    // } else {
    //   clientData.full_name = clientData.company_name
    // }
    // if (notificationObj.to_user_type === UserType.LOGISTICS) {
    //   receiverData = await LogisticsUserModel.findById(
    //     notificationObj.to_user_id,
    //     {
    //       full_name: 1,
    //     }
    //   )
    //   vehicleData = await VehicleModel.findOne({
    //     _id: notificationObj.vehicle_id,
    //   }).select({ vehicle_rc_number: 1 })

    //   logiscticsOrderData = await LogisticsOrderModel.findOne({
    //     _id: notificationObj.logistics_order_id,
    //   }).select({
    //     _id: 1,
    //     buyer_order_display_item_id: 1,
    //   })
    // }
    // console.log("receiverData", receiverData)
    // console.log("notificationObj", notificationObj)
    // console.log("userData", userData)

    if (notificationObj.to_user_type === UserType.BUYER) {
      switch (notificationObj.notification_type) {
        case ClientNotificationType.orderPlaced:
          if (receiverData === null) {
            return Promise.resolve()
          }
          //message = `Hi, ${receiverData.full_name}, your order (Order ID ${buyerOrderData.display_id}) has been Placed. It will be confirm soon.`
          message = `Hi "${client_name}", your order with order Id ${buyerOrderData.display_id} has been placed. We will intimate you once your order is confirmed.`
          break

        case ClientNotificationType.orderProcess:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi, ${client_name} your item (Id ${orderData.display_item_id}) of order (Id ${buyerOrderData.display_id}) has been Confirmed by RMC supplier and is under process.`
          }
          break

        case ClientNotificationType.orderAccept:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Hi, ${receiverData.full_name}, your item (Id ${orderData.display_item_id}) of order (Id ${orderData.display_id}) has been accepted by the supplier ${vendorData.full_name}.`
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having order id ${buyerOrderData.display_id} has been accepted & confirmed by RMC Supplier ${vendorData.company_name}. `
            break
          }

        case ClientNotificationType.assignQty:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            break
          }

        case ClientNotificationType.orderRejected:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            // message = `Dear, ${receiverData.full_name} Your order for item ${orderData.display_item_id} has been Rejected by vendor.`
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been rejected by the RMC Supplier ${vendorData.company_name}.`
            break
          }

        case ClientNotificationType.orderConfirm:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Dear, ${client_name} Your order for item ${orderData.display_item_id} has been Confirmed.`
            break
          }

        // if (notificationObj.to_user_type === UserType.ADMIN) {
        //   console.log("Admin")
        //   message = `Order with Id ${orderData.display_id}has been confirmed.`
        //   break
        // }
        // break

        case ClientNotificationType.TMAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, Transit Mixer has been assigned to your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} by RMC Supplier ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.orderPickup:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            // message = `Dear, ${client_name} Your Order for item ${orderData.display_item_id} has been picked up.`
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been picked up by RMC Supplier ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.orderItemReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            // message = `Dear, ${client_name} Your Order item for item ${orderData.display_item_id} has been rejected.`
            message = `Hi ${client_name}, you have rejected the product id ${orderData.display_item_id} supplied by ${vendorData.company_name} having an order id ${buyerOrderData.display_id}.`
          }
          break

        case ClientNotificationType.orderReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //   message = `Dear, ${client_name} Your Order for item for item ${orderData.display_item_id} has been rejected.`
            message = `Hi ${client_name}, you have rejected the order having an order id ${buyerOrderData.display_id} supplied by ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.orderDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            // message = `Dear, ${client_name} Your Order for item ${orderData.display_item_id} has been delayed.`
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been delayed by ${orderTrackData.delayTime} owing to ${orderTrackData.reasonForDelay}.`
          }
          break

        case ClientNotificationType.partiallyProductDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //  message = `Dear, ${client_name} Your Order has been delivered partially.`
            // message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been delieverd by the RMC Supplier ${vendorData.full_name}.`
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been partially delieverd by the RMC Supplier ${vendorData.company_name}.`
          }
          break
        case ClientNotificationType.fullyProductDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been fully delieverd by the RMC Supplier ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.orderDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = ` Hi ${client_name}, your order with an order id ${buyerOrderData.display_id} has been fully delivered and successfully completed by the RMC Supplier ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.partiallyOrderCancelled:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, you have cancelled the product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id}. Your order is thus partially cancelled.`
          }
          break

        case ClientNotificationType.orderCancelled:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, you have cancelled your order fully having an order id ${buyerOrderData.display_id}.`
          }
          break

        case ClientNotificationType.orderShortClose:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} As balanced quantity is less than 3, So your order is short closed & revised quantity is ${receiverData.revised_quantity}.`
            //message = `Hi ${client_name}, your product id ${orderData.display_item_id} having order id ${buyerOrderData.display_id} has been accepted & confirmed by RMC Supplier ${vendorData.full_name}. `
            message = `Hi ${client_name}, your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been shortclosed by the RMC supplier ${vendorData.company_name} as the balance quantity is less than 3 cum.`
          }
          break

        case ClientNotificationType.CPAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, CP has been assigned to your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} by RMC Supplier ${vendorData.company_name}.`
          }
          break

        case ClientNotificationType.reminderBeforSevenDayForClientAssignedQty:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, this is a reminder for you to kindly assign the RMC Qty for an order id ${buyerOrderData.display_id}.`
          }
          break

        case ClientNotificationType.reminderBeforOneDayForClientAssignedQty:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, this is a final reminder for you to kindly assign the RMC Qty for an order id ${buyerOrderData.display_id}. If you fail to assign the qty now then your order will be cancelled as per Buyer Purchase Policy.`
          }
          break

        case ClientNotificationType.CPPickup:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, CP has been picked up by RMC supplier ${vendorData.company_name} for delivery at your ${siteData.site_name} site for your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id}. `
          }
          break

        case ClientNotificationType.CPDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, CP has been delivered by RMC supplier ${vendorData.company_name} at your ${siteData.site_name} site for your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id}. `
          }
          break

        case ClientNotificationType.orderItemLapsed:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, your order with product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} has been lapsed as you failed to assign its qty.`
          }
          break

        case ClientNotificationType.orderLapsed:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, your order having an order id ${buyerOrderData.display_id} has been lapsed as you failed to assign its qty.`
          }
          break

        case ClientNotificationType.CPDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            //message = `Dear, ${client_name} TM has been assigned to your order for item ${orderData.display_item_id}.`
            //message = `Hi ${client_name}, delivery of your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. `
            message = `Hi ${client_name}, CP has been delayed for your product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} ${CPorderTrackData.delayTime} owing to ${CPorderTrackData.reasonForDelay}`
          }
          break
        case ClientNotificationType.ReplyOfSupportTicketByAdmin:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, Admin has replied to your support ticket with ticket no. ${ticketData.ticket_id}`
          }
          break

        case ClientNotificationType.ResolveSupportTicketByAdmin:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.BUYER) {
            message = `Hi ${client_name}, Admin has resolved and closed your support ticket with ticket no. ${ticketData.ticket_id}.`
          }
          break
      }
    }

    if (notificationObj.to_user_type === UserType.VENDOR) {
      switch (notificationObj.notification_type) {
        case VendorNotificationType.orderPlaced:
          if (receiverData === null) {
            return Promise.resolve()
          }
          console.log("hereee")
          if (notificationObj.to_user_type === UserType.VENDOR) {
            // message = `Hi ${receiverData.full_name}, you have received an order request, with an order Id ${vendorOrderData._id} from ${client_name}. You may accept the order for further process. `
            message = `Hi ${receiverData.company_name}, you have received an order request, with an order Id ${vendorOrderData._id} from ${client_name}. You may accept the order for further process. `
          }
          break

        // case VendorNotificationType.orderAccept:
        //   if (receiverData === null) {
        //     return Promise.resolve()
        //   }
        //   if (notificationObj.to_user_type === UserType.VENDOR) {
        //     message = `Dear, ${receiverData.full_name} Your Order has been accepted.`
        //   }
        //   break

        case VendorNotificationType.orderRejected:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi '${receiverData.company_name}, you have rejected the product id "${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} placed by client ${client_name}`
          }
          break

        case VendorNotificationType.TMAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //  message = `Dear, ${receiverData.full_name} TM with no ${vehicleData.vehicle_rc_number} has been assigned to your order of id ${vendorOrderData._id}.`
            message = `Hi ${receiverData.company_name}, you have assigned a Transit mixer for the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} placed by client ${client_name}.`
          }
          break

        case VendorNotificationType.orderPickup:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            // message = `Dear, ${receiverData.full_name} Your Order has been picked up.`
            message = `Hi ${receiverData.company_name}, you have picked up the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} placed by client ${client_name}. This Product has to be delivered at their ${siteData.site_name} site.`
          }
          break

        case VendorNotificationType.orderItemReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            // message = `Hi '${receiverData.full_name}, you have rejected the product id "${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} placed by client ${clientData.full_name}`
            // message = `Hi ${receiverData.full_name}, sorry to inform you that your product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} has been rejected by your client ${client_name}.`
            message = `Hi ${receiverData.company_name}, sorry to inform you that your product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} has been rejected by your client ${client_name}.`
          }
          break

        case VendorNotificationType.orderDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //message = `Dear, ${receiverData.full_name} Your Order has been delayed.`
            message = `Hi ${receiverData.company_name}, the supply of product id ${vendorOrderData.buyer_order_display_item_id}, for client ${client_name}, having an order id (${vendorOrderData._id}), has been delayed by ${orderTrackData.delayTime} owing to ${orderTrackData.reasonForDelay}.`
          }
          break

        case VendorNotificationType.partiallyProductDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            // message = `Order has been delivered partially.`
            message = `Hi ${receiverData.company_name}, you have partially delivered the product id ${vendorOrderData.buyer_order_display_item_id}, for your client ${client_name}, having an order id ${vendorOrderData._id}. Payment for the delivered product will be processed soon.`
          }
          break

        case VendorNotificationType.orderDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //  message = `Dear, ${receiverData.full_name} Your Order has been delivered.`
            message = `Hi ${receiverData.company_name}, you have fully delivered and successfully complete the order with an order id ${vendorOrderData._id} for your client ${client_name}.`
          }
          break

        case VendorNotificationType.orderCancelled:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //  message = `Dear, ${receiverData.full_name} Your Order has been delivered.`
            message = `Hi ${receiverData.company_name}, your order with an order id ${vendorOrderData._id} has been fully cancelled by the client ${client_name}. `
          }
          break

        case VendorNotificationType.orderShortClose:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //  message = `Dear, ${receiverData.full_name} Your Order has been delivered.`
            message = `Hi ${receiverData.company_name}, your supply order with product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} for the client ${client_name} has been shortclosed by the Admin as the balance quantity is less than 3 cum.`
          }
          break

        case VendorNotificationType.CPAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //message = `Dear, ${receiverData.full_name} TM with no ${vehicleData.vehicle_rc_number} has been assigned to your order of id ${vendorOrderData._id}.`
            message = `Hi ${receiverData.company_name}, you have assigned a CP for the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id} placed by client ${client_name}.`
          }
          break

        case VendorNotificationType.CPPickup:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, you have picked up the CP to be delivered at ${siteData.site_name} site for your client ${client_name} for the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id}. `
          }
          break

        case VendorNotificationType.CPDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, you have delivered the CP at ${siteData.site_name} site for your client ${client_name} for the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id}. `
          }
          break

        case VendorNotificationType.assignQtyByBuyer:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            //  message = `Hi ${receiverData.full_name}, you have delivered the CP at ${siteData.site_name} site for your client ${client_name} for the product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData._id}. `
            message = `Hi ${receiverData.company_name} your client ${client_name} has assigned the RMC Qty ${notificationObj.assigned_quantity} cum for the product id ${orderData.display_item_id} having an order id ${vendorOrderData._id}.`
          }
          break

        case VendorNotificationType.CPDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, CP has been delayed for the supply of product id ${orderData.display_item_id}, for client ${client_name}, having an order id (${vendorOrderData._id}) by ${CPorderTrackData.delayTime} owing to ${CPorderTrackData.reasonForDelay}. `
          }
          break

        case VendorNotificationType.orderItemLapsed:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, your order for product id ${orderData.display_item_id} has been lapsed as your client ${client_name} has failed to assign the qty.`
          }
          break
        case VendorNotificationType.DebitNoteAccept:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, your uploaded debit note for product id ${orderData.display_item_id} having an order id ${vendorOrderData._id} has been verified and approved by Conmix.`
          }
          break
        case VendorNotificationType.DebitNoteReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, your uploaded debit note for product id ${orderData.display_item_id} having an order id ${vendorOrderData._id} has been rejected by Conmix. Kindly upload the correct debit note for the order.`
          }
          break
        case VendorNotificationType.ReplyOfSupportTicketByAdmin:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, Admin has replied to your support ticket with ticket no. ${ticketData.ticket_id}`
          }
          break

        case VendorNotificationType.ResolveSupportTicketByAdmin:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.VENDOR) {
            message = `Hi ${receiverData.company_name}, Admin has resolved and closed your support ticket with ticket no. ${ticketData.ticket_id}`
          }
          break
      }
    }

    if (notificationObj.to_user_type === UserType.ADMIN) {
      switch (notificationObj.notification_type) {
        case AdminNotificationType.orderPlaced:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            //message = `Hi Admin, an order has been placed with an order Id ${buyerOrderData.display_id} by ${client_name}. `
            message = `Hi Admin, an order has been placed with an order Id ${buyerOrderData.display_id} by ${client_name}. `
          }
          break

        case AdminNotificationType.orderAccept:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Hi Admin, RMC Supplier ${vendorData.full_name} has accepted & confirmed the product id ${orderData.display_item_id} having Order Id ${orderData.display_id} placed by ${client_name}.`
            message = `Hi Admin, RMC Supplier ${vendorData.company_name} has accepted & confirmed the product id ${orderData.display_item_id} having Order Id ${buyerOrderData.display_id} placed by ${client_name}.`
          }
          break

        case AdminNotificationType.orderRejected:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Order with id ${orderData.display_id} has been rejected.`
            message = `Hi Admin, an order request with the product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} placed by ${client_name} has been rejected by RMC Supplier ${vendorData.company_name}.`
          }
          break

        case AdminNotificationType.orderConfirm:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            message = `Order has been confirmed.`
          }
          break

        case AdminNotificationType.TMAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            //  message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, Transit mixer has been assigned by RMC Supplier ${vendorData.company_name} for the product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} placed by ${client_name}. `
          }
          break

        case AdminNotificationType.orderPickup:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            //message = `Order has been picked up.`
            message = `Hi Admin, RMC Supplier ${vendorData.company_name} has picked up the product id ${orderData.display_item_id}, for client ${client_name}, having an order id ${buyerOrderData.display_id}.`
          }
          break

        case AdminNotificationType.orderItemReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Dear, ${receiverData.full_name} Your Order item for item ${orderData.display_item_id} has been rejected.`
            message = `Hi Admin,  your client ${client_name} has rejected the product id ${orderData.display_item_id} supplied by ${vendorData.company_name} having an order id ${buyerOrderData.display_id}.`
          }
          break

        case AdminNotificationType.orderReject:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            message = `Order has been rejected.`
          }
          break

        case AdminNotificationType.orderDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Order has been delayed.`
            message = `Hi Admin, the product id ${orderData.display_item_id}, for the client ${client_name}, having an order id ${buyerOrderData.display_id} has been delayed by ${orderTrackData.delayTime} owing to ${orderTrackData.reasonForDelay}.`
          }
          break

        case AdminNotificationType.partiallyProductDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Order has been delivered partially.`
            message = `Hi Admin, RMC Supplier ${vendorData.company_name} has partially delivered the product id ${orderData.display_item_id}, for the client ${client_name}, having an order id ${buyerOrderData.display_id}. `
          }
          break

        case AdminNotificationType.fullyProductDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Order has been delivered partially.`
            message = `Hi Admin, RMC Supplier ${vendorData.company_name} has fully delivered  the product id ${orderData.display_item_id}, for the client ${client_name}, having an order id ${buyerOrderData.display_id}. `
          }
          break

        case AdminNotificationType.orderDelivered:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `Order has been delivered.`
            message = `Hi Admin, the order for client ${client_name} with an Order Id ${buyerOrderData.display_id} has been fully delivered and successfully completed by the RMC Supplier ${vendorData.company_name}.`
          }
          break

        case AdminNotificationType.partiallyOrderCancelled:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            //message = `Order has been delivered partially.`
            message = `Hi Admin, your client ${client_name} has cancelled the product id ${orderData.display_item_id}, which was to be supplied by the RMC Supplier ${vendorData.company_name}, having an order id ${buyerOrderData.display_id}. Client has thus partially cancelled the order.`
          }
          break

        case AdminNotificationType.orderCancelled:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            //  message = `Order has been delivered.`
            message = `Hi Admin, your client ${client_name} has fully cancelled the order with an order id ${buyerOrderData.display_id}.`
          }
          break

        case AdminNotificationType.orderShortClose:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, supply of product id ${orderData.display_item_id}, for your client ${client_name}, having an order id ${buyerOrderData.display_id} has been shortclosed by RMC Supplier ${vendorData.company_name} as the balance quantity is less than 3 cum.`
          }
          break

        case AdminNotificationType.CPAssigned:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, CP has been assigned by RMC Supplier ${vendorData.company_name} for the product id ${orderData.display_item_id} having an order id ${buyerOrderData.display_id} placed by client ${client_name}. `
          }
          break

        case AdminNotificationType.CPDelay:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, CP has been delayed for the product id ${orderData.display_item_id} for the client ${client_name}, having an order id ${buyerOrderData.display_id} by ${CPorderTrackData.delayTime} owing to ${CPorderTrackData.reasonForDelay}.`
          }
          break

        case AdminNotificationType.createSupportTicketByBuyer:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, ${client_name} has created a support ticket with ticket no. ${ticketData.ticket_id}.`
          }
          break

        case AdminNotificationType.createSupportTicketByVendor:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, ${vendorData.company_name} has created a support ticket with ticket no. ${ticketData.ticket_id}.`
          }
          break

        case AdminNotificationType.replyOfSupportTicketByBuyer:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, ${client_name} has replied to your support ticket with ticket no. ${ticketData.ticket_id}.`
          }
          break

        case AdminNotificationType.replyOfSupportTicketByVendor:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, ${vendorData.company_name} has replied to your support ticket with ticket no. ${ticketData.ticket_id}.`
          }
          break

        case AdminNotificationType.vendorRegister:
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, ${vendorData.company_name} has applied for registration and its application is due for verification by Admin. Kindly verify the same within 5 working days.`
          }
          break
        case AdminNotificationType.reminderForVendorVerification:  
          if (receiverData === null) {
            return Promise.resolve()
          }
          if (notificationObj.to_user_type === UserType.ADMIN) {
            // message = `TM with no ${vehicleData.vehicle_rc_number} has been assigned to order of id ${vendorOrderData._id}.`
            message = `Hi Admin, this is a reminder for verification of registration application by ${vendorData.company_name}.`
          }
          break
      }
    }

    /********************************************************** */

    console.log(message)
    if (message === "") {
      console.log(`message is empty.`)
      return Promise.resolve()
    }
    return this.send(message, notificationObj)
  }
}
