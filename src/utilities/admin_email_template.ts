export const admin_mail_template = {
    forgot_pwd_otp:`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Admin, </strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is the OTP to Reset Password for Conmix Admin. It is valid upto 60 seconds. Kindly do not share the OTP with others.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    password_updated_successfully: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Password Update Successful</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_forgotpwd.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Password Update Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear {{doc.full_name}},  </strong> Your password has been updated successfully. Kindly do not share your password with others.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
    
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!--button-->
                                                <table border="0" bgcolor="#146e9d" cellpadding="0" cellspacing="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center" height="40" width="170" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;letter-spacing: 0.5px;">
                                                        <a href="http://www.example.com" target="_blank" style="color: #ffffff">GO TO SITE</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                             
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    one_time_otp: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>One Time Password</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">One Time Password</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Admin, </strong> <span style="font-size: 18px;font-weight: 700;">{{code}}</span> is the OTP 
                                               for Conmix Admin. It is valid upto 60 seconds. Kindly do not share the OTP with others.  </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <!-- <tr>
                                             <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;">Your Verification Code is : <span style="font-weight: bold;">123456</span> </td>
                                            </tr> -->
    
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                     
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    new_order :`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>New Order</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">New Order Placed</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828; line-height: 24px;"><strong>Dear Admin, </strong>
                                                <br>An order with an Order ID <strong>{{orderData.display_id}}</strong> has been placed by client <strong>{{client_name}}.</strong>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>VIEW ORDER DETAIL</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Delivered</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                      <!-- <td align="right"
                                                        style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #ffffff;">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #ffffff">Help Center</a>
                                                      </td> -->
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order_confirm.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been delivered</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear Admin,</strong> An order with Product Id <strong>{{orderItem.display_item_id}}</strong> having an Order ID <strong>{{orderDoc.display_id}}</strong> for client <strong>{{client_name}}</strong> has been fully delivered at client's <strong>{{site_name}}</strong> site  as per the purchase policy, and thus  completed by RMC Supplier <strong>{{vendor_company_name}}</strong> </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;text-decoration: underline"><a href="http://www.emailpaws.com" target="_blank" style="color: #282828"><u>TRACK YOUR ORDER</u></a></td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <!-- <tr>
                                              <td align="center"
                                                style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 18px;color: #dadada;font-weight: 400;">
                                                Get in Touch
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellspacing="0" cellpadding="0" border="0"
                                                  align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/fb.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/tw.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                      <td width="25">
                                                        <img width="25"
                                                          style="display:block;width:100%;max-width:25px;"
                                                          src="http://emailmug.com/premium-template/emailpaws/notif/in.png">
                                                      </td>
                                                      <td width="10">&nbsp;</td>
                                                    
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr> -->
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_buyer_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Canceled</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been cancelled.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear Admin,</strong> Your client {{client_name}} has cancelled their entire order having an Order ID {{display_id}}.
                                                <br>
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_supplier_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Canceled</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Your order has been canceled.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
    
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear Admin,</strong> An order placed by your client {{client_name}} with Product Id {{data.buyer_order_display_item_id}} having an Order ID {{data.buyer_order_display_id}} has not been accepted by any RMC Supplier. Thus your client's  order is cancelled with No Action.
                                                <br>
                                              </td>
    
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 15px;color: #282828;"> You may again try to place the order after some time as per your requirement.</td>
                                            </tr>
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
    
    
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_buyer_payment_refund_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Admin,</strong> A payment refund of your client {{client_name}} for their cancelled order with Product Id {{orderItemDoc.display_item_id}} having an Order ID {{display_id}} is to be processed as per our Buyer Purchase Policy.
                                                <br><br>
                                                Kindly mark the instruction to the Accounts Team at Conmix.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_supplier_payment_refund_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Your order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Admin,</strong> A payment refund of your client {{client_name}} for their order with Product Id {{data.buyer_order_display_item_id}} having an Order ID {{data.buyer_order_display_id}} has been cancelled as not accepted by any Supplier. Payment refund to your client has to be processed as per our Buyer Pruchase Policy.
                                                <br><br>
                                                Kindly mark an instruction to the Accounts Team at Conmix.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    material_rejected_buyer_side_payment_refund_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Refund </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse;line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Order amount has been refunded </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;"><strong>Dear Admin, </strong> A payment refund of your client {{client_name}} for the rejected material as supplied by RMC Supplier {{sub_vendor_company_name}} with the Product Id {{OrderItemData.display_item_id}} having an Order ID {{display_id}} has to be processed as per our  <span style="color: red">Material Rejection & Refund Policy.</span>
                                                <br><br>
                                                Kindly mark an instruction to the Accounts Team at Conmix.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    material_rejected_buyer_side_admin: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Order Rejected</title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_cancelorder.png"></td>
                                            </tr>
                                            
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 28px;color: #282828;">Order has been Rejected.</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                          
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 16px;color: #282828;"><strong>Dear Admin,</strong> Your client {{client_name}} has rejected the material delivered to their site {{site_name}} as supplied by the RMC Supplier {{master_vendor_company_name}} with Product id {{OrderItemData.display_item_id}} having an Order ID {{display_id}}
                                                <br><br>
                                                Kindly contact and warn the RMC supplier for such Poor Quality Material Supply.
                                              </td>
    
                                            <tr>
                                              <td height="30">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
       
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    payment_successfully: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Successful </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_success.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Payment received successful </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                               <strong>Dear Admin, </strong>  You have received the payment for an order having an Order ID {{orderData.display_id}}
                                                <br>
                                                Your order has been sucessfully placed and is waiting for confirmation from the RMC Supplier.
                                                <br>
                                                This Order is waiting for confirmation from the RMC Supplier.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    payment_failed: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_payment_faield.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Payment couldn not be processed</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear Admin,</strong> Payment attempt by your client {{client_name}} has failed and order has not been placed.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_deactivate: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_block.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Account Deactive</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                RMC Supplier {{vendorUserData.company_name}} account has been deactivated at Conmix.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_activated: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_confirm.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Account Re-Active Successful</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                We are glad to inform you that RMC Supplier {{vendorUserData.company_name}}'s account has been successfully re-activated at Conmix.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    vendor_block: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_block.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Account Block</td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                This is to inform you that RMC Supplier {{vendorUserData.company_name}}'s account has been blocked at Conmix.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    accept_order: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Accept Order (by Supplier) </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                Your client {{client_name}}'s order for Product Id {{vendorOrderData.buyer_order_display_item_id}} having an Order ID {{vendorOrderData.buyer_order_display_id}} has been accepted and thus confirmed by the RMC Supplier {{vendor_company_name}}.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_fully_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Completion of entire order </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                An order for your client {{client_name}} having an Order ID {{orderDoc.display_id}} has been fully delivered and completed by RMC Suppliers.
    
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    warning: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Warning </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                Warning has been issued to RMC Supplier {{vendor_company_name}} in regards to their order placed by the client {{client_name}} with Product Id {{OrderItemData.display_item_id}} having an Order Id {{vendor_order_id}}. <br>
                                                Kindly contact the RMC Supplier to inform and advise them to avoid such warnings.<br>
                                                For your information, RMC Supplier account will be blocked on receiving 3rd warning.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    order_cancelled_by_buyer: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;"> Partial Order Canceled </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                Your client {{client_name}} has cancelled their order for Product Id {{orderItemDoc.display_item_id}} having an Order Id {{vendorOrderDoc._id}} placed to RMC Supplier {{vendor_name}}.<br>
                                                Kindly note that the client can cancel their order partially or fully before an order is accepted and confirmed by RMC Suppliers.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    vendor_verification: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;"> OTP </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear Super Admin,</strong> <br>
                                                {{authData.code}} is an OTP for verification and approval of vendor details of the Registered RMC Supplier {{vendor_company_name}}. It is valid upto 120 seconds.
                                                Verification of the Vendor Registration details will be processed by Admin Manager.<br>
                                                Thus, kindly do not share the OTP with others.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    account_verified: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_confirm.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;"> Vendor Detail Verification </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                You have successfully verified and approved the vendor details for regisetered RMC Supplier {{vendorUserData.company_name}}.<br>
                                                Kindly communicate with Admin Sales to conduct traning of the Vendor Supplier for Conmix website.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    vendor_rejection: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;"> OTP </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear Super Admin,</strong> <br>
                                                {{authData.code}} is an OTP for rejection of the vendor details of the Registered RMC Supplier {{vendor_company_name}} . It is valid upto 120 seconds. <br>
                                                  Verification and Rejection of the Vendor details will be processed by Admin Manager. <br>
                                                  Thus, kindly do not share the OTP with others
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    plant_block: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Payment Failed </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_otp.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;"> OTP </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear Super Admin,</strong> <br>
                                                {{authData.code}} is an OTP to Block the RMC Plant {{address_business_name}} of the RMC Supplier {{vendor_company_name}} .
                                                It is valid upto 120 seconds.<br>
                                                RMC Plant  Block processe will be done by Admin Manager.<br>
                                                Thus, kindly do not share the OTP with others
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`,
    product_partially_delivered: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <!--[if (gte mso 9)|(IE)]> <xml> <o:OfficeDocumentSettings> <o:AllowPNG/> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no">
        <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no">
        <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no">
        <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no">
        <!-- disable auto email linking in iOS -->
        <title>Conmix </title>
        <!-- google font, if you use custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lora:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
        <!-- CSS - you can't modify or add anything of it, as the StampReady Editor uses a copy of these styles. -->
        <style type="text/css">
          /* basics */
          body{margin:0px !important;padding:0px !important;display:block !important;min-width:100% !important;width:100% !important;-webkit-text-size-adjust:none;}
          table{border-spacing:0;mso-table-lspace:0pt;mso-table-rspace:0pt;}
          table td{border-collapse:collapse; line-height: 24px;}
          strong{font-weight:bold !important;}
          b{font-weight:bold !important;}
          td img{-ms-interpolation-mode:bicubic;display:block;width:auto;max-width:auto;height:auto;margin:auto;display:block !important;border:0px !important;}
          td p{margin:0 !important;padding:0 !important;display:inline-block !important;font-family:inherit !important;}
          td a{text-decoration:none !important;}
          /* outlook */
          .ExternalClass{width:100%;}
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:inherit;}
          .ReadMsgBody{width:100%;background-color:#ffffff;}
          /* iOS blue links */
          a[x-apple-data-detectors]{color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important;}
          /* gmail blue links */
          u+#body a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
          /* buttons fix */
          .undoreset a, .undoreset a:hover{text-decoration:none !important;}
          .yshortcuts a{border-bottom:none !important;}
          .ios-footer a{color:#aaaaaa !important;text-decoration:none;}
          /* responsive */
          @media screen and (max-width:640px){
            td.img-responsive img{width:100% !important;max-width:100% !important;height:auto !important;margin:auto;}
            table.row{width:100% !important;max-width:100% !important;}
            table.center-float, td.center-float{float:none !important;}
            /* stops floating modules next to each other */
            td.center-text{text-align:center !important;}
            td.container-padding{width:100% !important;padding-left:15px !important;padding-right:15px !important;}
            table.hide-mobile, tr.hide-mobile, td.hide-mobile, br.hide-mobile{display:none !important;}
            td.menu-container{text-align:center !important;}
            td.autoheight{height:auto !important;}
            table.mobile-padding{margin:15px 0 !important;}
            table.br-mobile-true td br{display:initial !important;}
          }
        </style>
      </head>
      <body>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table border="0" width="100%" cellpadding="0"
                                                  cellspacing="0" align="center">
                                                  <tbody>
                                                    <tr>
                                                      <td width="180" align="left">
                                                        <img width="180"
                                                          style="display:block;width:100%;max-width:180px;"
                                                          alt="img"
                                                          src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/logo.png">
                                                      </td>
                                                      <td>&nbsp;</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#ffffff" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->    
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"><img width="100" style="display:block;width:100%;max-width:100px;" alt="img" src="https://tec-conmix.s3.ap-south-1.amazonaws.com/d/email_template_images/ic_order.png"></td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Josefin Sans', Arial, Helvetica, sans-serif;font-size: 30px;color: #282828;">Product partially Delivered </td>
                                            </tr>
                                            <tr>
                                              <td height="18">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size: 14px;color: #282828;line-height: 22px;">
                                                <strong>Dear {{adminUserDoc.full_name}},</strong> <br>
                                                An order for your client {{client_name}} with the Product Id {{orderItemData.display_item_id}} having an Order ID {{vendorOrderData._id}} for their {{site_name}} site has been partially delivered by RMC Supplier {{vendor_company_name}}.
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="20">&nbsp;</td>
                                            </tr>
                                           
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <!--module-->
        <table style="width:100%;max-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody>
            <tr>
              <td bgcolor="#F4F4F4" align="center">
                <!--container-->
                <table class="row" style="width:600px;max-width:600px;" width="600" cellspacing="0" cellpadding="0"
                  border="0" align="center">
                  <tbody>
                    <tr>
                      <td bgcolor="#003872" align="center">
                        <!--wrapper-->
                        <table class="row" style="width:540px;max-width:540px;" width="540" cellspacing="0"
                          cellpadding="0" border="0" align="center">
                          <tbody>
                            <tr>
                              <td class="container-padding" align="center">
                                <!-- content container -->
                                <table width="540" border="0" cellpadding="0" cellspacing="0" align="center"
                                  class="row" style="width:540px;max-width:540px;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <!-- content -->
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0"
                                          align="center" style="width:100%; max-width:100%;">
                                          <tbody>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center"
                                                style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 19px;">
                                                This email was sent to : support@conmix.in<br>
                                                You are receiving this email because you are subscribed
                                                to our mailing list.<br>
                                                For any questions please send to support@conmix.in
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                  <tbody>
                                                    <tr>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Privacy Policy</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Help Center</a>
                                                      </td>
                                                      <td width="20" align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;">
                                                        |
                                                      </td>
                                                      <td align="center"
                                                        style="font-family:'Roboto', Arial, Helvetica, sans-serif;font-size: 13px;color: #dadada;line-height: 20px;text-decoration: underline">
                                                        <a href="http://www.example.com"
                                                          target="_blank"
                                                          style="color: #dadada">Unsubscrib</a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>`
 }  