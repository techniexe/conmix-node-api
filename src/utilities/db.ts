import * as mongoose from "mongoose"
const dbURL =
  process.env.DATABASE_URL ||
  "mongodb://conmate:cM12Bh2Hhl87Hkh12Hlk@15.206.148.217:22017/admin"

const dbName = process.env.DB_NAME || "conmix_new"
mongoose.set("useNewUrlParser", true)
mongoose.set("useFindAndModify", false)
mongoose.set("useCreateIndex", true)
// mongoose.set("useUnifiedTopology", true)
export function reconnectDb() {
  console.log(`Connecting to mongodb`)
  mongoose.connect(dbURL, { dbName })
}
reconnectDb()
