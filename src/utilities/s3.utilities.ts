import { S3 } from "aws-sdk"
import { awsConfig } from "./config"

export const s3: S3 = new S3({ region: awsConfig.s3BucketRegion })

export async function getObject(Bucket: string, Key: string) {
  return s3
    .getObject({
      Bucket,
      Key,
    })
    .promise()
}

export async function putObject(
  Bucket: string,
  Key: string,
  Body: Buffer,
  ContentType: string
): Promise<any> {
  console.log(`uploading on bucket ${Bucket}`)
  return s3
    .putObject({
      Bucket,
      Key,
      Body,
      ContentType,
      ACL: "public-read",
    })
    .promise()
}
/** if qs is false it will not include dynamic query string
 * @param objectPath :string
 * @param qs :boolean (if false it will not include query string)
 */
export function getS3MediaURL(objectPath: string, qs = true) {
  const q = qs ? `?i=${Date.now()}` : ""
  return `${awsConfig.mediaBaseURL}/${objectPath}${q}`
}

export function getS3DynamicCdnURL(objectPath: string) {
  return `${awsConfig.dynamicCdnBaseURL}/${objectPath}?i=${Date.now()}`
}
