import { injectable } from "inversify"

const api_key =
  process.env.MAILGUN_API_KEY ||
  "8aae6409d1c68fb48b44a45ff7ac7455-adf6de59-838c4ad0";
const domain = process.env.MAILGUN_DOMAIN || "conmate.in";

var mailgun = require("mailgun-js")({ apiKey: api_key, domain });

@injectable()
export class MailGunService {
  async sendEmail(data: any) {
    return mailgun.messages().send(data, function (error: any) {
      if (error) {
        console.log("got an error: ", error);
      }
      console.log("mail sent successfully");
    });
  }
}
