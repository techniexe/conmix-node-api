import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddPayMethod {
  name: string
  [k: string]: any
}

export interface IAddPayMethodRequest extends IAuthenticatedRequest {
  body: IAddPayMethod
}

export const addPayMethodSchema: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150).required(),
  }),
}

export interface IEditPayMethod {
  name?: string
  is_active?: boolean
  authentication_code: string
}

export interface IEditPayMethodRequest extends IAuthenticatedRequest {
  body: IEditPayMethod
}

export const editPayMethodSchema: IRequestSchema = {
  params: Joi.object().keys({
    payId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150),
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
}

export interface IDeletePayMethodRequest extends IAuthenticatedRequest {
  params: {
    payId: string
  }
  body: {
    authentication_code: string
  }
}

export const deletePayMethodSchema: IRequestSchema = {
  params: Joi.object().keys({
    payId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
