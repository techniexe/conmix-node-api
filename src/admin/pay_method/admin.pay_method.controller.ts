import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { PaymentMethodRepository } from "./admin.pay_method.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addPayMethodSchema,
  IAddPayMethodRequest,
  editPayMethodSchema,
  IEditPayMethodRequest,
  deletePayMethodSchema,
  IDeletePayMethodRequest,
} from "./admin.pay_method.req-schema"

@injectable()
@controller("/pay_method", verifyCustomToken("admin"))
export class PaymentMethodController {
  constructor(
    @inject(AdminTypes.PaymentMethodRepository)
    private payMethodRepo: PaymentMethodRepository
  ) {}
  @httpPost("/", validate(addPayMethodSchema))
  async addPayMethod(
    req: IAddPayMethodRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = await this.payMethodRepo.addPayMethod(
        req.body,
        req.user.uid
      )
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:payId", validate(editPayMethodSchema))
  async editPayMethod(
    req: IEditPayMethodRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.payMethodRepo.editPayMethod(
        req.user.uid,
        req.params.payId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getPayMethod(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.payMethodRepo.getPayMethod(search, before, after)
    res.json({ data })
  }

  @httpDelete("/:payId", validate(deletePayMethodSchema))
  async deletePayMethod(
    req: IDeletePayMethodRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.payMethodRepo.deletePayMethod(
        req.user.uid,
        req.params.payId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
