import { injectable, inject } from "inversify"
import { IAddPayMethod, IEditPayMethod } from "./admin.pay_method.req-schema"
import { PayMethodModel } from "../../model/pay_method.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "util"

@injectable()
export class PaymentMethodRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addPayMethod(payData: IAddPayMethod, created_by: string) {
    const check = await PayMethodModel.findOne({
      name: payData.name,
    })
    if (check !== null) {
      const err = new UnexpectedInput(
        `This payment method name "${payData.name}" was already added by Supplier`
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    payData._id = new ObjectId()
    payData.created_by = created_by

    const newPay = new PayMethodModel(payData)
    const newPayDoc = await newPay.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_PAY_METHOD,
      `Admin added payment method of id ${newPayDoc._id} .`
    )

    return Promise.resolve(newPayDoc)
  }

  async editPayMethod(user_id: string, payId: string, payData: IEditPayMethod) {
    const result = await AuthModel.findOne({
      code: payData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const flds = ["name", "is_active"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(payData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const res = await PayMethodModel.findOneAndUpdate(
      { _id: payId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this payment method`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_PAY_METHOD,
      `Admin Updated details of payment method id ${payId}.`,
      res,
      editDoc
    )

    await AuthModel.deleteOne({
      user_id,
      code: payData.authentication_code,
    })

    return Promise.resolve()
  }

  async getPayMethod(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return PayMethodModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async deletePayMethod(
    user_id: string,
    payId: string,
    authentication_code: string
  ) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await PayMethodModel.updateOne(
      {
        _id: new ObjectId(payId),
        created_by: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_PAY_METHOD_BY_ADMIN,
      `Admin deleted Pay method of id ${payId} .`
    )

    return Promise.resolve()
  }
}
