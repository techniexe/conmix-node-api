import "reflect-metadata"
import { Container } from "inversify"
import { TYPE } from "inversify-express-utils"
import { AdminTypes } from "./admin.types"
import { AdminUserController } from "./user/admin.user.controller"
import { AdminUserRepository } from "./user/admin.user.repository"
import { AdminAuthRepository } from "./auth/admin.auth.repository"
import { AdminAuthController } from "./auth/admin.auth.controller"
import { TwilioService } from "../utilities/twilio-service"
import { EmailTemplateController } from "./email-template/email.template.controller"
import { EmailTemplateRepository } from "./email-template/email.template.repository"
import { MailService } from "../utilities/mail-service"

import { RegionController } from "./region/admin.region.controller"
import { RegionRepository } from "./region/admin.region.repository"

import { AdminProductController } from "./product/admin.product.controller"
import { AdminProductRepository } from "./product/admin.product.repository"
import { VehicleController } from "./vehicle/vehicle.controller"
import { VehicleRepository } from "./vehicle/vehicle.repository"
import { BuyerSiteRepository } from "./site/admin.buyersite.repository"
import { BuyerSiteController } from "./site/admin.buyersite.controller"
import { SupplierBankController } from "./bank/admin.supplierbank.controller"
import { SupplierBankRepository } from "./bank/admin.supplierbank.repository"
import { ReviewController } from "./review/admin.review.controller"
import { ReviewRepository } from "./review/admin.review.repository"
import { ComplaintRepository } from "./complaint/admin.complaint.repository"
import { ComplaintController } from "./complaint/admin.complaint.controller"
import { OrderController } from "./order/admin.order.controller"
import { OrderRepository } from "./order/admin.order.repository"
import { TransactionController } from "./transaction/admin.transaction.controller"
import { TransactionRepository } from "./transaction/admin.transaction.repository"
import { AggregateSandCategoryController } from "./aggregate_sand_category/aggregate_sand_category.controller"
import { AggregateSandCategoryRepository } from "./aggregate_sand_category/aggregate_sand_category.repository"
import { RfpController } from "./rfp/admin.rfp.controller"
import { RfpRepository } from "./rfp/admin.rfp.repository"
import { AccessLogsController } from "./access-logs/access-logs.controller"
import { AccessLogsRepository } from "./access-logs/access-logs.repository"
import { SendMailController } from "./send-email/admin.send-email.controller"
import { SendMailRepository } from "./send-email/admin.send-email.repository"
import { SendSmsController } from "./send-sms/admin.send-sms.controller"
import { SendSmsRepository } from "./send-sms/admin.send-sms.repository"
import { DashboardController } from "./dashboard/admin.dashboard.controller"
import { DashboardRepository } from "./dashboard/admin.dashboard.repository"
import { OrderTrackController } from "./order_track/admin.order_track.controller"
import { OrderTrackRepository } from "./order_track/admin.order_track.repository"
import { SmsTemplateController } from "./sms-template/sms.template.controller"
import { SmsTemplateRepository } from "./sms-template/sms.template.repository"
import { SnsService } from "../utilities/sns.service"
import { AdminSupportTicketController } from "./support_ticket/admin.support_ticket.controller"
import { AdminSupportTicketRepository } from "./support_ticket/admin.support_ticket.repository"
import { AdminCouponController } from "./coupon/admin.coupon.controller"
import { AdminCouponRepository } from "./coupon/admin.coupon.repository"
import { CommonService } from "../utilities/common.service"
import { UniqueidService } from "../utilities/uniqueid-service"
import { AdminMarginRateController } from "./margin_rate/admin.margin_rate.controller"
import { AdminMarginRateRepository } from "./margin_rate/admin.margin_rate.repository"
import { AdminGstSlabController } from "./gst_slab/admin.gst_slab.controller"
import { AdminGstSlabRepository } from "./gst_slab/admin.gst_slab.repository"
import { AdminInvoiceController } from "./invoice/admin.invoice.controller"
import { AdminInvoiceRepository } from "./invoice/admin.invoice.repository"
import { AdminReportController } from "./report/admin.report.controller"
import { AdminReportRepository } from "./report/admin.report.repository"

import { VehicleRateController } from "./vehicle/vehicle_rate/vehicle_rate.controller"
import { VehicleRateRepository } from "./vehicle/vehicle_rate/vehicle_rate.repository"
import { AdminConcreteGradeController } from "./concrete_grade/admin.concrete_grade.controller"
import { AdminConcreteGradeRepository } from "./concrete_grade/admin.concrete_grade.repository"
import { AdminCementBrandController } from "./cement_brand/admin.cement_brand.controller"
import { AdminCementBrandRepository } from "./cement_brand/admin.cement_brand.repository"
import { AdminAdmixBrandController } from "./admixture_brand/admin.admix_brand.controller"
import { AdminAdmixBrandRepository } from "./admixture_brand/admin.admix_brand.repository"
import { AdminAggregateSourceController } from "./aggregate_source/admin.aggregate_source.controller"
import { AdminAggregateSourceRepository } from "./aggregate_source/admin.aggregate_source.repository"
import { AdminSandSourceController } from "./sand_source/admin.sand_source.controller"
import { AdminSandSourceRepository } from "./sand_source/admin.sand_source.repository"
import { AdminFlyAshSourceController } from "./fly_ash_source/admin.fly_ash.controller"
import { AdminFlyAshSourceRepository } from "./fly_ash_source/admin.fly_ash.repository"
import { AdminTMController } from "./TM/admin.TM.controller"
import { AdminTMRepository } from "./TM/admin.TM.repository"
import { AdminConcretePumpController } from "./concrete_pump/admin.concrete_pump.controller"
import { AdminConcretePumpRepository } from "./concrete_pump/admin.concrete_pump.repository"
import { AdminAdmixCategoryController } from "./admixture_category/admin.admix_category.controller"
import { AdminAdmixCategoryRepository } from "./admixture_category/admin.admix_category.repository"
import { AdminCementGradeController } from "./cement_grade/admin.cement_grade.controller"
import { AdminCementGradeRepository } from "./cement_grade/admin.cement_grade.repository"
import { NotificationUtility } from "../utilities/notification.utility"
import { NotificationController } from "./notification/admin.notification.controller"
import { NotificationRepository } from "./notification/admin.notification.repository"
import { AdminDesignMixtureController } from "./design_mixture/admin.design_mix.controller"
import { AdminDesignMixtureRepository } from "./design_mixture/admin.design_mix.repository"
import { PaymentMethodController } from "./pay_method/admin.pay_method.controller"
import { PaymentMethodRepository } from "./pay_method/admin.pay_method.repository"
import { TestService } from "../utilities/test.service"
import { MailGunService } from "../utilities/mailgun.service"

export const adminContainer = new Container()

adminContainer
  .bind(AdminTypes.TwilioService)
  .to(TwilioService)
  .inSingletonScope()

adminContainer
  .bind(AdminTypes.TestService)
  .to(TestService)
  .inSingletonScope()  

adminContainer.bind(AdminTypes.MailService).to(MailService).inSingletonScope()

adminContainer.bind(AdminTypes.SnsService).to(SnsService).inSingletonScope()
adminContainer.bind(AdminTypes.mailgunService).to(MailGunService).inSingletonScope()



adminContainer
  .bind(TYPE.Controller)
  .to(AdminUserController)
  .whenTargetNamed(AdminTypes.AdminUserController)

adminContainer
  .bind(AdminTypes.AdminUserRepository)
  .to(AdminUserRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminAuthController)
  .whenTargetNamed(AdminTypes.AdminAuthController)
adminContainer
  .bind(AdminTypes.AdminAuthRepository)
  .to(AdminAuthRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AggregateSandCategoryController)
  .whenTargetNamed(AdminTypes.AggregateSandCategoryController)
adminContainer
  .bind(AdminTypes.AggregateSandCategoryRepository)
  .to(AggregateSandCategoryRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(EmailTemplateController)
  .whenTargetNamed(AdminTypes.EmailTemplateController)
adminContainer
  .bind(AdminTypes.EmailTemplateRepository)
  .to(EmailTemplateRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(RegionController)
  .whenTargetNamed(AdminTypes.RegionController)
adminContainer
  .bind(AdminTypes.RegionRepository)
  .to(RegionRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminProductController)
  .whenTargetNamed(AdminTypes.AdminProductController)
adminContainer
  .bind(AdminTypes.AdminProductRepository)
  .to(AdminProductRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(VehicleController)
  .whenTargetNamed(AdminTypes.VehicleController)
adminContainer
  .bind(AdminTypes.VehicleRepository)
  .to(VehicleRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(BuyerSiteController)
  .whenTargetNamed(AdminTypes.BuyerSiteController)
adminContainer
  .bind(AdminTypes.BuyerSiteRepository)
  .to(BuyerSiteRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(SupplierBankController)
  .whenTargetNamed(AdminTypes.SupplierBankController)
adminContainer
  .bind(AdminTypes.SupplierBankRepository)
  .to(SupplierBankRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(ReviewController)
  .whenTargetNamed(AdminTypes.ReviewController)
adminContainer
  .bind(AdminTypes.ReviewRepository)
  .to(ReviewRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(ComplaintController)
  .whenTargetNamed(AdminTypes.ComplaintController)
adminContainer
  .bind(AdminTypes.ComplaintRepository)
  .to(ComplaintRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(OrderController)
  .whenTargetNamed(AdminTypes.OrderController)
adminContainer
  .bind(AdminTypes.OrderRepository)
  .to(OrderRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(TransactionController)
  .whenTargetNamed(AdminTypes.TransactionController)
adminContainer
  .bind(AdminTypes.TransactionRepository)
  .to(TransactionRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(RfpController)
  .whenTargetNamed(AdminTypes.RfpController)

adminContainer
  .bind(AdminTypes.RfpRepository)
  .to(RfpRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AccessLogsController)
  .whenTargetNamed(AdminTypes.AccessLogsController)

adminContainer
  .bind(AdminTypes.AccessLogsRepository)
  .to(AccessLogsRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(SendMailController)
  .whenTargetNamed(AdminTypes.SendMailController)

adminContainer
  .bind(AdminTypes.SendMailRepository)
  .to(SendMailRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(SendSmsController)
  .whenTargetNamed(AdminTypes.SendSmsController)

adminContainer
  .bind(AdminTypes.SendSmsRepository)
  .to(SendSmsRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(DashboardController)
  .whenTargetNamed(AdminTypes.DashboardController)

adminContainer
  .bind(AdminTypes.DashboardRepository)
  .to(DashboardRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(OrderTrackController)
  .whenTargetNamed(AdminTypes.OrderTrackController)

adminContainer
  .bind(AdminTypes.OrderTrackRepository)
  .to(OrderTrackRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(SmsTemplateController)
  .whenTargetNamed(AdminTypes.SmsTemplateController)

adminContainer
  .bind(AdminTypes.SmsTemplateRepository)
  .to(SmsTemplateRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminSupportTicketController)
  .whenTargetNamed(AdminTypes.AdminSupportTicketController)
adminContainer
  .bind(AdminTypes.AdminSupportTicketRepository)
  .to(AdminSupportTicketRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminCouponController)
  .whenTargetNamed(AdminTypes.AdminCouponController)

adminContainer
  .bind(AdminTypes.AdminCouponRepository)
  .to(AdminCouponRepository)
  .inSingletonScope()

adminContainer
  .bind(AdminTypes.CommonService)
  .to(CommonService)
  .inSingletonScope()

adminContainer
  .bind(AdminTypes.UniqueidService)
  .to(UniqueidService)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminMarginRateController)
  .whenTargetNamed(AdminTypes.AdminMarginRateController)

adminContainer
  .bind(AdminTypes.AdminMarginRateRepository)
  .to(AdminMarginRateRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminGstSlabController)
  .whenTargetNamed(AdminTypes.AdminGstSlabController)

adminContainer
  .bind(AdminTypes.AdminGstSlabRepository)
  .to(AdminGstSlabRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminInvoiceController)
  .whenTargetNamed(AdminTypes.AdminInvoiceController)

adminContainer
  .bind(AdminTypes.AdminInvoiceRepository)
  .to(AdminInvoiceRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminReportController)
  .whenTargetNamed(AdminTypes.AdminReportController)

adminContainer
  .bind(AdminTypes.AdminReportRepository)
  .to(AdminReportRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminAggregateSourceController)
  .whenTargetNamed(AdminTypes.AdminAggregateSourceController)

adminContainer
  .bind(AdminTypes.AdminAggregateSourceRepository)
  .to(AdminAggregateSourceRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(VehicleRateController)
  .whenTargetNamed(AdminTypes.VehicleRateController)
adminContainer
  .bind(AdminTypes.VehicleRateRepository)
  .to(VehicleRateRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminConcreteGradeController)
  .whenTargetNamed(AdminTypes.AdminConcreteGradeController)

adminContainer
  .bind(AdminTypes.AdminConcreteGradeRepository)
  .to(AdminConcreteGradeRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminCementBrandController)
  .whenTargetNamed(AdminTypes.AdminCementBrandController)

adminContainer
  .bind(AdminTypes.AdminCementBrandRepository)
  .to(AdminCementBrandRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminAdmixBrandController)
  .whenTargetNamed(AdminTypes.AdminAdmixBrandController)

adminContainer
  .bind(AdminTypes.AdminAdmixBrandRepository)
  .to(AdminAdmixBrandRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminSandSourceController)
  .whenTargetNamed(AdminTypes.AdminSandSourceController)

adminContainer
  .bind(AdminTypes.AdminSandSourceRepository)
  .to(AdminSandSourceRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminFlyAshSourceController)
  .whenTargetNamed(AdminTypes.AdminFlyAshSourceController)

adminContainer
  .bind(AdminTypes.AdminFlyAshSourceRepository)
  .to(AdminFlyAshSourceRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminTMController)
  .whenTargetNamed(AdminTypes.AdminTMController)

adminContainer
  .bind(AdminTypes.AdminTMRepository)
  .to(AdminTMRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminConcretePumpController)
  .whenTargetNamed(AdminTypes.AdminConcretePumpController)

adminContainer
  .bind(AdminTypes.AdminConcretePumpRepository)
  .to(AdminConcretePumpRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminAdmixCategoryController)
  .whenTargetNamed(AdminTypes.AdminAdmixCategoryController)

adminContainer
  .bind(AdminTypes.AdminAdmixCategoryRepository)
  .to(AdminAdmixCategoryRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminCementGradeController)
  .whenTargetNamed(AdminTypes.AdminCementGradeController)

adminContainer
  .bind(AdminTypes.AdminCementGradeRepository)
  .to(AdminCementGradeRepository)
  .inSingletonScope()

adminContainer
  .bind(AdminTypes.NotificationUtility)
  .to(NotificationUtility)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(NotificationController)
  .whenTargetNamed(AdminTypes.NotificationController)
adminContainer
  .bind(AdminTypes.NotificationRepository)
  .to(NotificationRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(AdminDesignMixtureController)
  .whenTargetNamed(AdminTypes.AdminDesignMixtureController)

adminContainer
  .bind(AdminTypes.AdminDesignMixtureRepository)
  .to(AdminDesignMixtureRepository)
  .inSingletonScope()

adminContainer
  .bind(TYPE.Controller)
  .to(PaymentMethodController)
  .whenTargetNamed(AdminTypes.PaymentMethodController)

adminContainer
  .bind(AdminTypes.PaymentMethodRepository)
  .to(PaymentMethodRepository)
  .inSingletonScope()
