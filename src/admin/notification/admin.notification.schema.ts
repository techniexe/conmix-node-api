import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetNotificationsRequest extends IAuthenticatedRequest {
  query: {
    /** ISO Date String */
    before?: string
    /** ISO Date String */
    after?: string
    max_notification_type?: number
  }
}
export interface ISetNotificationAsSeenRequest extends IAuthenticatedRequest {
  params: {
    notificationId: string
  }
}

export const getNotificationSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    max_notification_type: Joi.number(),
  }),
}
export const setNotificationAsSeen: IRequestSchema = {
  params: Joi.object().keys({
    notificationId: Joi.string().regex(objectIdRegex).required(),
  }),
}
