import { injectable, inject } from "inversify"
import {
  VehicleCategoryeModel,
  IVehicleCategoryDoc,
  VehicleSubCategoryModel,
  IVehicleSubCategoryDoc,
  VehicleModel,
} from "../../model/vehicle.model"
import {
  IVehicleSubCategory,
  IEditVehicleSubCategory,
  IListVehicleDetails,
  IVehicleCategory,
} from "./vehicle.req.schema"
import { InvalidInput } from "../../utilities/customError"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AuthModel } from "../../model/auth.model"
import { ObjectId } from "bson"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { VehicleRateModel } from "../../model/vehicle_rate.model"
import { minOrder, deliveryRangeForVehicle } from "../../utilities/config"

@injectable()
export class VehicleRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addVehicleCategory(
    user_id: string,
    category_name: string
  ): Promise<IVehicleCategoryDoc> {
    const newVehicleCat = new VehicleCategoryeModel({ category_name })
    const newVehicleCatDoc = await newVehicleCat.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_VEHICLE_CATEGORY,
      `Admin added new vehicle category  of id ${newVehicleCatDoc._id} .`
    )

    return Promise.resolve(newVehicleCatDoc)
  }

  async addVehicleSubCategory(
    user_id: string,
    vehicle_category_id: string,
    subCatObj: IVehicleSubCategory
  ): Promise<IVehicleSubCategoryDoc> {
    subCatObj.vehicle_category_id = vehicle_category_id

    const newVehicleSubCat = new VehicleSubCategoryModel(subCatObj)
    const newVehicleSubCatDoc = await newVehicleSubCat.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_VEHICLE_SUB_CATEGORY,
      `Admin added new vehicle category  of id ${newVehicleSubCatDoc._id} .`
    )

    return Promise.resolve(newVehicleSubCatDoc)

    // return new VehicleSubCategoryModel(subCatObj).save()
  }
  async editVehicleCategory(
    user_id: string,
    vehicle_category_id: string,
    categoryData: IVehicleCategory
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: categoryData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await VehicleCategoryeModel.findOneAndUpdate(
      {
        _id: vehicle_category_id,
      },
      {
        $set: {
          category_name: categoryData.category_name,
          is_active: categoryData.is_active,
          updated_at: Date.now(),
          updated_by: user_id,
        },
      }
    )
    if (updtRes === null) {
      const err1 = new InvalidInput(`No such Vehicle Category exists.`)
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await AuthModel.deleteOne({
      user_id,
      code: categoryData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_VEHICLE_CATEGORY,
      `Admin Updated details of Vehicle category of id ${vehicle_category_id}.`,
      updtRes,
      categoryData
    )

    return Promise.resolve()
  }

  async editVehicleSubCategory(
    user_id: string,
    vehicle_sub_category_id: string,
    catData: IEditVehicleSubCategory
  ) {
    const result = await AuthModel.findOne({
      code: catData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtObj: { [k: string]: any } = {}
    const updtFld = [
      "sub_category_name",
      "min_load_capacity",
      "max_load_capacity",
      "weight_unit_code",
      "weight_unit",
      "is_active",
    ]
    for (const [key, val] of Object.entries(catData)) {
      if (updtFld.indexOf(key) > -1) {
        updtObj[key] = val
      }
    }
    if (Object.keys(updtObj).length === 0) {
      const err1 = new InvalidInput(`At least one filed is required for update.`)
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const updtRes = await VehicleSubCategoryModel.findOneAndUpdate(
      {
        _id: vehicle_sub_category_id,
      },
      { $set: updtObj }
    )
    if (updtRes === null) {
      const err1 = new InvalidInput(`No such Vehicle Sub Category exists.`)
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await AuthModel.deleteOne({
      user_id,
      code: catData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_VEHICLE_SUB_CATEGORY,
      `Admin Updated details of Vehicle Sub category of id ${vehicle_sub_category_id}.`,
      updtRes,
      updtObj
    )

    return Promise.resolve()
  }
  async listVehicleCategory(): Promise<IVehicleCategoryDoc[]> {
    return VehicleCategoryeModel.find()
  }

  async listVehicleSubCategory(
    vehicle_category_id?: string
  ): Promise<IVehicleSubCategoryDoc[]> {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(vehicle_category_id)) {
      query.vehicle_category_id = vehicle_category_id
    }
    return VehicleSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "vehicle_category",
          localField: "vehicle_category_id",
          foreignField: "_id",
          as: "vehicle_category",
        },
      },
      {
        $unwind: { path: "$vehicle_category" },
      },
      {
        $project: {
          _id: 1,
          sub_category_name: 1,
          vehicle_category_id: 1,
          min_load_capacity: 1,
          max_load_capacity: 1,
          weight_unit_code: 1,
          weight_unit: 1,
          created_at: 1,
          "vehicle_category.category_name": 1,
        },
      },
    ])
  }

  async listVehicles(searchParam: IListVehicleDetails) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.vehicle_category_id)) {
      query.vehicle_category_id = new ObjectId(searchParam.vehicle_category_id)
    }
    if (!isNullOrUndefined(searchParam.vehicle_sub_category_id)) {
      query.vehicle_sub_category_id = new ObjectId(
        searchParam.vehicle_sub_category_id
      )
    }
    if (!isNullOrUndefined(searchParam.user_id)) {
      query.user_id = new ObjectId(searchParam.user_id)
    }

    if (!isNullOrUndefined(searchParam.is_gps_enabled)) {
      query.is_gps_enabled = searchParam.is_gps_enabled
    }

    if (!isNullOrUndefined(searchParam.is_insurance_active)) {
      query.is_insurance_active = searchParam.is_insurance_active
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "logistics_user",
          localField: "user_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: { path: "$logistics_user" },
      },

      {
        $lookup: {
          from: "vehicle_category",
          localField: "vehicle_category_id",
          foreignField: "_id",
          as: "vehicle_category",
        },
      },
      {
        $unwind: { path: "$vehicle_category" },
      },

      {
        $lookup: {
          from: "vehicle_sub_category",
          localField: "vehicle_sub_category_id",
          foreignField: "_id",
          as: "vehicle_sub_category",
        },
      },
      {
        $unwind: { path: "$vehicle_sub_category" },
      },

      {
        $project: {
          vehicle_category_id: 1,
          vehicle_sub_category_id: 1,
          vehicle_rc_number: 1,
          // per_metric_ton_per_km_rate: 1,
          pickup_location: 1,
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          vehicle_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          vehicle_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          state_id: 1,
          "logistics_user.full_name": 1,
          "vehicle_category.category_name": 1,
          "vehicle_sub_category.sub_category_name": 1,
        },
      },
    ]

    const data = await VehicleModel.aggregate(aggregateArr)
    for (let i = 0; i < data.length; i++) {
      let rateDoc = await VehicleRateModel.findOne({
        state_id: new ObjectId(data[i].state_id),
      })

      if (!isNullOrUndefined(rateDoc)) {
        data[i].per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
        data[i].calculated_trip_price =
          minOrder *
          deliveryRangeForVehicle.min *
          data[i].per_metric_ton_per_km_rate
      } else {
        return Promise.reject(
          new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
        )
      }
    }
    return Promise.resolve(data)
  }
}
