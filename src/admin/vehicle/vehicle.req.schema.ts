import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddVehicleCategoryRequest extends IAuthenticatedRequest {
  body: {
    category_name: string
  }
}
export interface IVehicleSubCategory {
  vehicle_category_id?: string
  sub_category_name: string
  min_load_capacity: number
  max_load_capacity: number
  weight_unit_code: string // MT
  weight_unit: string // Metric tonne
}

export interface IAddVehicleSubCategoryRequest extends IAuthenticatedRequest {
  body: IVehicleSubCategory
  params: {
    vehicleCategoryId: string
  }
}

export interface IListVehicleSubCategoryRequest extends IAuthenticatedRequest {
  params: {
    vehicleCategoryId: string
  }
}

export interface IVehicleCategory {
  category_name: string
  is_active: boolean
  authentication_code: string
}

export interface IEditVehicleCategoryRequest extends IAuthenticatedRequest {
  body: IVehicleCategory
  params: {
    vehicleCategoryId: string
  }
}

export interface IEditVehicleSubCategory {
  sub_category_name?: string
  min_load_capacity?: number
  max_load_capacity?: number
  weight_unit_code?: string
  weight_unit?: string // Metric tonne
  is_active: boolean
  authentication_code: string
}

export interface IEditVehicleSubCategoryRequest extends IAuthenticatedRequest {
  body: IEditVehicleSubCategory
  params: {
    vehicleSubCategoryId: string
  }
}

export const addVehicleCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string()
      .min(2)
      .max(64)
      .required(),
  }),
}

export const addVehicleSubCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    sub_category_name: Joi.string()
      .max(64)
      .min(2)
      .required(),
    min_load_capacity: Joi.number().required(),
    max_load_capacity: Joi.number().required(),
    weight_unit_code: Joi.string(), // default: MT
    weight_unit: Joi.string(), // default: Metric tonne
  }),
  params: Joi.object().keys({
    vehicleCategoryId: Joi.string()
      .regex(objectIdRegex)
      .required(),
  }),
}

export const listVehicleSubCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    vehicleCategoryId: Joi.string()
      .regex(objectIdRegex)
      .required(),
  }),
}

export const editVehicleCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string()
      .min(2)
      .max(64),
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    vehicleCategoryId: Joi.string()
      .regex(objectIdRegex)
      .required(),
  }),
}

export const editVehicleSubCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    sub_category_name: Joi.string()
      .max(64)
      .min(2),
    min_load_capacity: Joi.number(),
    max_load_capacity: Joi.number(),
    weight_unit_code: Joi.string(),
    weight_unit: Joi.string(), // Metric tonne
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    vehicleSubCategoryId: Joi.string()
      .regex(objectIdRegex)
      .required(),
  }),
}

export interface IListVehicleDetails {
  is_active?: boolean
  vehicle_category_id?: string
  vehicle_sub_category_id?: string
  user_id?: string
  is_gps_enabled?: boolean
  is_insurance_active?: boolean
  before?: string
  after?: string
}

export interface IListVehicleRequest extends IAuthenticatedRequest {
  body: IListVehicleDetails
}

export const listVehicleSchema: IRequestSchema = {
  query: Joi.object().keys({
    is_active: Joi.boolean(),
    vehicle_category_id: Joi.string(),
    vehicle_sub_category_id: Joi.string().regex(objectIdRegex),
    user_id: Joi.string().regex(objectIdRegex),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}
