import { VehicleRateModel } from "./../../../model/vehicle_rate.model"
import { injectable, inject } from "inversify"
import { IAddVehicleRate, IEditVehicleRate } from "./vehicle_rate.req.schema"
import { StateModel } from "../../../model/region.model"
import { isNullOrUndefined } from "../../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../../utilities/customError"
import { CommonService } from "../../../utilities/common.service"
import { AdminTypes } from "../../admin.types"
import { AccessTypes } from "../../../model/access_logs.model"

@injectable()
export class VehicleRateRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addVehicleRate(data: IAddVehicleRate, user_id: string) {
    const stateData = await StateModel.findById(data.state_id)
    if (isNullOrUndefined(stateData)) {
      return Promise.reject(new InvalidInput(`No State data found.`, 400))
    }
    const saveDoc = {
      state_id: data.state_id,
      state_name: stateData.state_name,
      per_metric_ton_per_km_rate: data.per_metric_ton_per_km_rate,
      region_type: data.region_type,
      created_by_id: user_id,
      created_at: Date.now(),
    }

    const newRate = new VehicleRateModel(saveDoc)
    const newrateDoc = await newRate.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_VEHICLE_RATE_BY_ADMIN,
      `Admin added source of id ${newrateDoc._id} .`
    )

    return new VehicleRateModel(saveDoc).save()
  }

  async getVehicleRateList() {
    return await VehicleRateModel.find()
  }

  async editVehicleRate(
    user_id: string,
    rateId: string,
    vehicleRateData: IEditVehicleRate
  ) {
    const flds = ["per_metric_ton_per_km_rate", "region_type"]

    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(vehicleRateData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id

    const res = await VehicleRateModel.findOneAndUpdate(
      { _id: rateId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This vehicle rate doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_VEHICLE_RATE_BY_ADMIN,
      `Admin updated details vehicle rate of id ${rateId} .`,
      res,
      editDoc
    )

    return Promise.resolve()
  }
}
