import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../../middleware/auth-token.middleware"
import { validate } from "../../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminTypes } from "../../admin.types"
import {
  addvehicleRateSchema,
  IAddVehicleRateRequest,
  editVehicleRateSchema,
  IEditVehicleRateRequest,
} from "./vehicle_rate.req.schema"
import { VehicleRateRepository } from "./vehicle_rate.repository"

@injectable()
@controller("/vehicle_rate", verifyCustomToken("admin"))
export class VehicleRateController {
  constructor(
    @inject(AdminTypes.VehicleRateRepository)
    private vehicleRateRepo: VehicleRateRepository
  ) {}

  @httpPost("/", validate(addvehicleRateSchema))
  async addVehicleRate(
    req: IAddVehicleRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = await this.vehicleRateRepo.addVehicleRate(
        req.body,
        req.user.uid
      )

      res.json({ data: { _id } })
    } catch (err) {
      return next(err)
    }
  }

  @httpGet("/")
  async getVehicleRateList(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.vehicleRateRepo.getVehicleRateList()
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:rateId", validate(editVehicleRateSchema))
  async editVehicleRate(
    req: IEditVehicleRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { rateId } = req.params
      await this.vehicleRateRepo.editVehicleRate(req.user.uid, rateId, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
