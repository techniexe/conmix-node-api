import * as Joi from "joi"
import {
  IRequestSchema,
  objectIdRegex,
} from "../../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../../middleware/auth-token.middleware"

export interface IAddVehicleRate {
  state_id: string
  per_metric_ton_per_km_rate: number
  region_type: string
}

export interface IAddVehicleRateRequest extends IAuthenticatedRequest {
  body: IAddVehicleRate
}

export const addvehicleRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    state_id: Joi.string().regex(objectIdRegex).required(),
    per_metric_ton_per_km_rate: Joi.number().required(),
    region_type: Joi.string().required().allow("PLAIN", "HILLY"),
  }),
}

export interface IEditVehicleRate {
  per_metric_ton_per_km_rate: number
  region_type: string
}

export interface IEditVehicleRateRequest extends IAuthenticatedRequest {
  params: {
    rateId: string
  }
  body: IEditVehicleRate
}

export const editVehicleRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    rateId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    per_metric_ton_per_km_rate: Joi.number(),
    region_type: Joi.string().allow("PLAIN, HILLY"),
  }),
}
