import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import { AdminTypes } from "../admin.types"
import { VehicleRepository } from "./vehicle.repository"
import {
  IAddVehicleCategoryRequest,
  addVehicleCategorySchema,
  IAddVehicleSubCategoryRequest,
  addVehicleSubCategorySchema,
  listVehicleSubCategorySchema,
  IListVehicleSubCategoryRequest,
  editVehicleCategorySchema,
  IEditVehicleCategoryRequest,
  editVehicleSubCategorySchema,
  IEditVehicleSubCategoryRequest,
  listVehicleSchema,
  IListVehicleRequest,
} from "./vehicle.req.schema"
import { NextFunction, Response } from "express-serve-static-core"
import { validate, IRequestSchema } from "../../middleware/joi.middleware"
import { InvalidInput } from "../../utilities/customError"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"

@injectable()
@controller("/vehicle", verifyCustomToken("admin"))
export class VehicleController {
  constructor(
    @inject(AdminTypes.VehicleRepository) private vehicleRepo: VehicleRepository
  ) {}
  @httpPost("/category", validate(addVehicleCategorySchema))
  async addVehicleCategory(
    req: IAddVehicleCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.vehicleRepo.addVehicleCategory(
        req.user.uid,
        req.body.category_name
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = `Unable to add Vehicle Category`
      if (err.code === 11000) {
        msg = `This Vehicle category "${req.body.category_name}" already added `
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPost(
    "/subCategory/:vehicleCategoryId",
    validate(addVehicleSubCategorySchema)
  )
  async addVehicleSubCategory(
    req: IAddVehicleSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.vehicleRepo.addVehicleSubCategory(
        req.user.uid,
        req.params.vehicleCategoryId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This Vehicle subcategory "${req.body.sub_category_name}" already added.`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch(
    "/category/:vehicleCategoryId",
    validate(editVehicleCategorySchema)
  )
  async updateVehicleCategory(
    req: IEditVehicleCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.vehicleRepo.editVehicleCategory(
      req.user.uid,
      req.params.vehicleCategoryId,
      req.body
    )
    res.sendStatus(201)
  }

  @httpPatch(
    "/subCategory/:vehicleSubCategoryId",
    validate(editVehicleSubCategorySchema)
  )
  async updateVehicleSubCategory(
    req: IEditVehicleSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.vehicleRepo.editVehicleSubCategory(
      req.user.uid,
      req.params.vehicleSubCategoryId,
      req.body
    )
    res.sendStatus(201)
  }
  @httpGet("/categories")
  async listAllVehicleCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.vehicleRepo.listVehicleCategory()
    res.json({ data })
  }

  @httpGet("/subCategory")
  @httpGet(
    "/subCategory/:vehicleCategoryId",
    validate(listVehicleSubCategorySchema)
  )
  async listSubCategory(
    req: IListVehicleSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    const { vehicleCategoryId } = req.params
    const data = await this.vehicleRepo.listVehicleSubCategory(
      vehicleCategoryId
    )
    res.json({ data })
  }

  @httpGet("/", validate(listVehicleSchema))
  async listVehicles(
    req: IListVehicleRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.vehicleRepo.listVehicles(req.query)
    res.json({ data })
  }
}
