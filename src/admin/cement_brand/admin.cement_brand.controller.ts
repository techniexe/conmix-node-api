import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { AdminCementBrandRepository } from "./admin.cement_brand.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addCementBrandSchema,
  IAddCementBrandRequest,
  IEditCementBrandRequest,
  editCementBrandSchema,
  deleteCementBrandSchema,
  IDeleteCementBrandRequest,
} from "./admin.cement_brand.req-schema"

@controller("/cement_brand", verifyCustomToken("admin"))
@injectable()
export class AdminCementBrandController {
  constructor(
    @inject(AdminTypes.AdminCementBrandRepository)
    private cementBrandRepo: AdminCementBrandRepository
  ) {}
  @httpPost("/", validate(addCementBrandSchema))
  async addCementBrand(
    req: IAddCementBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = await this.cementBrandRepo.addCementBrand(
        req.body,
        req.user.uid,
        req.files
      )
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:cementBrandId", validate(editCementBrandSchema))
  async editCementBrand(
    req: IEditCementBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementBrandRepo.editCementBrand(
        req.user.uid,
        req.params.cementBrandId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCementBrand(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.cementBrandRepo.getCementBrand(
      search,
      before,
      after
    )
    res.json({ data })
  }

  @httpDelete("/:cementBrandId", validate(deleteCementBrandSchema))
  async deleteCementBrand(
    req: IDeleteCementBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementBrandRepo.deleteCementBrand(
        req.user.uid,
        req.params.cementBrandId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
