import { injectable, inject } from "inversify"
import { UploadedFile } from "express-fileupload"
import { CementBrandModel } from "../../model/cement_brand.model"
import {
  IAddCementBrand,
  IEditCementBrand,
} from "./admin.cement_brand.req-schema"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  putObject,
  getS3MediaURL,
  getS3DynamicCdnURL,
} from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class AdminCementBrandRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addCementBrand(
    cementBrandData: IAddCementBrand,
    created_by: string,
    fileData: { image: UploadedFile }
  ) {
    const cementBrandCheck = await CementBrandModel.findOne({
      name: cementBrandData.name,
    })
    if (cementBrandCheck !== null) {
      const err = new UnexpectedInput(
        `This Cement Brand name "${cementBrandData.name}" already added`
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    cementBrandData._id = new ObjectId()
    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/cement_brand`
      let objectName = "" + cementBrandData._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      cementBrandData.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      cementBrandData.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
    }

    cementBrandData.created_by = created_by

    const newCementBrand = new CementBrandModel(cementBrandData)
    const newCementBrandDoc = await newCementBrand.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_CEMENT_BRAND,
      `Admin added cement_brand of id ${newCementBrandDoc._id} .`
    )

    return Promise.resolve(newCementBrandDoc)
  }

  async editCementBrand(
    user_id: string,
    cementBrandId: string,
    cementBrandData: IEditCementBrand,
    fileData: { image: UploadedFile }
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: cementBrandData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const cementBrandCheck = await CementBrandModel.findOne({
      name: cementBrandData.name,
    })

    if (
      !isNullOrUndefined(cementBrandCheck) &&
      cementBrandCheck._id.equals(cementBrandId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`This Cement Brand name "${cementBrandData.name}" already added`, 400)
      )
    }

    const flds = ["name"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(cementBrandData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/cement_brand`
      let objectName = `${cementBrandId}` + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      editDoc.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      editDoc.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
      editDoc.thumbnail_low_url = getS3DynamicCdnURL(
        `${objectDir}/w_100,q_10/${objectName}`
      )
    }

    const res = await CementBrandModel.findOneAndUpdate(
      { _id: cementBrandId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_CEMENT_BRAND,
      `Admin Updated details of cement brand id ${cementBrandId}.`,
      res,
      editDoc
    )
    
    await AuthModel.deleteOne({
      user_id,
      code: cementBrandData.authentication_code,
    })
    return Promise.resolve()
  }

  async getCementBrand(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return CementBrandModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async deleteCementBrand(
    user_id: string,
    cementBrandId: string,
    authentication_code: string
  ) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await CementBrandModel.updateOne(
      {
        _id: new ObjectId(cementBrandId),
        created_by: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_CEMENT_BRAND_BY_ADMIN,
      `Admin deleted concreted grade of id ${cementBrandId} .`
    )

    return Promise.resolve()
  }
}
