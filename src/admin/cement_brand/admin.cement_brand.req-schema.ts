import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { UploadedFile } from "express-fileupload"

export interface IAddCementBrand {
  category_name: string
  image_url?: string
  thumbnail_url?: string
  [k: string]: any
}

export interface IAddCementBrandRequest extends IAuthenticatedRequest {
  body: IAddCementBrand
  files: {
    image: UploadedFile
  }
}

export const addCementBrandSchema: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150).required(),
  }),
}

export interface IEditCementBrand {
  name?: string
  image_url?: string
  thumbnail_url?: string
  authentication_code: string
}

export interface IEditCementBrandRequest extends IAuthenticatedRequest {
  body: IEditCementBrand
  files: {
    image: UploadedFile
  }
}

export const editCementBrandSchema: IRequestSchema = {
  params: Joi.object().keys({
    cementBrandId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150),
    authentication_code: Joi.string().required(),
  }),
}

export interface IDeleteCementBrandRequest extends IAuthenticatedRequest {
  params: {
    cementBrandId: string
  }
  body: {
    authentication_code: string
  }
}

export const deleteCementBrandSchema: IRequestSchema = {
  params: Joi.object().keys({
    cementBrandId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
