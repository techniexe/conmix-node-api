import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddSource {
  region_id: string
  fly_ash_source_name: string
  [k: string]: any
}

export const addSourceSchema: IRequestSchema = {
  body: Joi.object().keys({
    region_id: Joi.string().regex(objectIdRegex).required(),
    fly_ash_source_name: Joi.string().required(),
  }),
}

export interface IAddSourceRequest extends IAuthenticatedRequest {
  body: IAddSource
}

export interface IUpdateSourceDetails {
  fly_ash_source_name?: string
}

export interface IUpdateSourceRequest extends IAuthenticatedRequest {
  body: IUpdateSourceDetails
}

export const updateSourceSchema: IRequestSchema = {
  params: Joi.object().keys({
    source_id: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    fly_ash_source_name: Joi.string(),
    authentication_code: Joi.string().required(),
  }),
}

export interface IRemoveSourceRequest extends IAuthenticatedRequest {
  params: {
    source_id: number
    [k: string]: any
  }
}

export const removeSourceSchema: IRequestSchema = {
  params: Joi.object().keys({
    source_id: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
