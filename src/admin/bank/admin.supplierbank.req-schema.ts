import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetSupplierBankDetailsReq extends IAuthenticatedRequest {
  query: {
    user_id?: string
    user_type?: string
    search?: string
    before?: string
    after?: string
    account_number?: number
  }
}

export const getSupplierBankDetailsSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    user_type: Joi.string(),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    account_number: Joi.number(),
  }),
}
