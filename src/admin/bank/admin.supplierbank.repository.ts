import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { BankInfoModel } from "../../model/bank.model"

@injectable()
export class SupplierBankRepository {
  async getSupplierBankDetails(
    user_id?: string,
    search?: string,
    before?: string,
    after?: string,
    account_number?: number,
    user_type?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_type)) {
      query["user.user_type"] = user_type
    }
    if (!isNullOrUndefined(user_id)) {
      query["user.user_id"] = user_id
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(account_number)) {
      query.account_number = account_number
    }

    return BankInfoModel.find(query).limit(10)
  }
}
