import { controller, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { SupplierBankRepository } from "./admin.supplierbank.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  getSupplierBankDetailsSchema,
  IGetSupplierBankDetailsReq,
} from "./admin.supplierbank.req-schema"

@injectable()
@controller("/supplierbankdetails", verifyCustomToken("admin"))
export class SupplierBankController {
  constructor(
    @inject(AdminTypes.SupplierBankRepository)
    private supplierBankRepo: SupplierBankRepository
  ) {}

  @httpGet("/", validate(getSupplierBankDetailsSchema))
  async getSupplierBankDetails(
    req: IGetSupplierBankDetailsReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        search,
        before,
        after,
        user_id,
        account_number,
        user_type,
      } = req.query
      const data = await this.supplierBankRepo.getSupplierBankDetails(
        user_id,
        search,
        before,
        after,
        account_number,
        user_type
      )
      res.json(data)
    } catch (err) {
      next(err)
    }
  }
}
