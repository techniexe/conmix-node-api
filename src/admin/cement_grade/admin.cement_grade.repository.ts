import { injectable, inject } from "inversify"

import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { CommonService } from "../../utilities/common.service"
import { AdminTypes } from "../admin.types"
import { AccessTypes } from "../../model/access_logs.model"
import { AuthModel } from "../../model/auth.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import {
  IAddCementgrade,
  IEditCementgrade,
} from "./admin.cement_grade.req-schema"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class AdminCementGradeRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addCg(cgData: IAddCementgrade, created_by: string) {
    const cgCheck = await CementGradeModel.findOne({
      name: cgData.name,
    })
    if (cgCheck !== null) {
      const err = new UnexpectedInput(
        `This Cement Grade name "${cgData.name}" already added`
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    cgData._id = new ObjectId()
    cgData.created_by = created_by

    const newCg = new CementGradeModel(cgData)
    const newCgDoc = await newCg.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_CEMENT_GRADE,
      `Admin added cement_grade of id ${newCgDoc._id} .`
    )

    return Promise.resolve(newCgDoc)
  }

  async editCg(user_id: string, cgId: string, cgData: IEditCementgrade) {
    const result = await AuthModel.findOne({
      code: cgData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const cgCheck = await CementGradeModel.findOne({
      name: cgData.name,
    })
    if (
      !isNullOrUndefined(cgCheck) &&
      cgCheck._id.equals(cgId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`This Cement Grade name "${cgData.name}" already added`, 400)
      )
    }
    const flds = ["name"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(cgData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const res = await CementGradeModel.findOneAndUpdate(
      { _id: cgId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This cement grade doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_CEMENT_GRADE,
      `Admin Updated details of cement_grade id ${cgId}.`,
      res,
      editDoc
    )

    await AuthModel.deleteOne({
      user_id,
      code: cgData.authentication_code,
    })

    return Promise.resolve()
  }

  async getCg(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return CementGradeModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async deleteCg(user_id: string, cgId: string, authentication_code: string) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await CementGradeModel.updateOne(
      {
        _id: new ObjectId(cgId),
        created_by: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by_id: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_CEMENT_GRADE_BY_ADMIN,
      `Admin deleted cement_grade of id ${cgId} .`
    )

    return Promise.resolve()
  }
}
