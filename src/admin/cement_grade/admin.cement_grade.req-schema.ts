import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddCementgrade {
  name: string
  [k: string]: any
}

export interface IAddCementgradeRequest extends IAuthenticatedRequest {
  body: IAddCementgrade
}

export const addCementgradeSchema: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150).required(),
  }),
}

export interface IEditCementgrade {
  name?: string
  authentication_code: string
}

export interface IEditCementgradeRequest extends IAuthenticatedRequest {
  body: IEditCementgrade
}

export const editCementgradeSchema: IRequestSchema = {
  params: Joi.object().keys({
    cgId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150),
    authentication_code: Joi.string().required(),
  }),
}

export interface IDeleteCementgradeRequest extends IAuthenticatedRequest {
  params: {
    cgId: string
  }
  body: {
    authentication_code: string
  }
}

export const deleteCementgradeSchema: IRequestSchema = {
  params: Joi.object().keys({
    cgId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
