import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminCementGradeRepository } from "./admin.cement_grade.repository"
import {
  addCementgradeSchema,
  IAddCementgradeRequest,
  editCementgradeSchema,
  IEditCementgradeRequest,
  deleteCementgradeSchema,
  IDeleteCementgradeRequest,
} from "./admin.cement_grade.req-schema"

@controller("/cement_grade", verifyCustomToken("admin"))
@injectable()
export class AdminCementGradeController {
  constructor(
    @inject(AdminTypes.AdminCementGradeRepository)
    private cementGradeRepo: AdminCementGradeRepository
  ) {}
  @httpPost("/", validate(addCementgradeSchema))
  async addCg(req: IAddCementgradeRequest, res: Response, next: NextFunction) {
    try {
      console.log(req)
      const data = await this.cementGradeRepo.addCg(req.body, req.user.uid)
      res.json(data)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:cgId", validate(editCementgradeSchema))
  async editCg(
    req: IEditCementgradeRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementGradeRepo.editCg(req.user.uid, req.params.cgId, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCg(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after } = req.query
    const data = await this.cementGradeRepo.getCg(search, before, after)
    res.json({ data })
  }

  @httpDelete("/:cgId", validate(deleteCementgradeSchema))
  async deleteCg(
    req: IDeleteCementgradeRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementGradeRepo.deleteCg(
        req.user.uid,
        req.params.cgId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
