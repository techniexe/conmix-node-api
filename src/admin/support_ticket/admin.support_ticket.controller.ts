import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { AdminSupportTicketRepository } from "./admin.support_ticket.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  createSupportTicketByAdminSchema,
  ICreateSupportTicketByAdminRequest,
  changeSupportTicketStatusByAdminSchema,
  IChangeSupportTicketByAdminRequest,
  replySupportTicketByAdminSchema,
  IReplySupportTicketByAdminRequest,
  getSupportTicketByAdminSchema,
  getSupportTicketInfoByAdminSchema,
  IGetSupportTicketInfoByAdminRequest,
  getReplyOfSupportTicketByAdminSchema,
  IGetReplyOfTicketByAdminRequest,
} from "./admin.support_ticket.req-schema"
import { NextFunction, Response } from "express-serve-static-core"

@injectable()
@controller("/support_ticket", verifyCustomToken("admin"))
export class AdminSupportTicketController {
  constructor(
    @inject(AdminTypes.AdminSupportTicketRepository)
    private adminsupportTicketRepo: AdminSupportTicketRepository
  ) {}

  /**
   *  Create Support Ticket.
   */
  @httpPost("/", validate(createSupportTicketByAdminSchema))
  async createSupportTicketByAdmin(
    req: ICreateSupportTicketByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminsupportTicketRepo.createSupportTicketByAdmin(
        req.user.uid,
        req.body,
        req.files
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:ticketId", validate(changeSupportTicketStatusByAdminSchema))
  async changeSupportTicketStatusByAdmin(
    req: IChangeSupportTicketByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { ticketId } = req.params
      const { uid } = req.user
      await this.adminsupportTicketRepo.changeSupportTicketStatusByAdmin(
        uid,
        ticketId,
        req.body
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /**
   * Reply of support ticket.
   */
  @httpPost("/reply", validate(replySupportTicketByAdminSchema))
  async replySupportTicketByAdmin(
    req: IReplySupportTicketByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.adminsupportTicketRepo.replySupportTicketByAdmin(
        uid,
        req.body,
        req.files
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /**
 * Get listing of support ticket.
 */
  @httpGet("/", validate(getSupportTicketByAdminSchema))
  async getSupportTicketByAdmin(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.adminsupportTicketRepo.getSupportTicketByAdmin(
        uid,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Get Info of support ticket.
   */
  @httpGet("/:ticketId", validate(getSupportTicketInfoByAdminSchema))
  async getSupportTicketInfoByAdmin(
    req: IGetSupportTicketInfoByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.adminsupportTicketRepo.getSupportTicketInfoByAdmin(
        uid,
        req.params.ticketId
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }
  /**
 * Get Reply list of support ticket.
 */
  @httpGet("/reply/:ticketId", validate(getReplyOfSupportTicketByAdminSchema))
  async getReplyListOfTicketByAdmin(
    req: IGetReplyOfTicketByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { ticketId } = req.params
      const data = await this.adminsupportTicketRepo.getReplyListOfTicketByAdmin(
        uid,
        ticketId,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
