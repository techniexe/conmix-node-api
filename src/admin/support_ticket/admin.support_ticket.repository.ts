import { injectable, inject } from "inversify"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { AdminTypes } from "../admin.types"
import { SnsService } from "../../utilities/sns.service"
import {
  ICreateSupportTicketByAdmin,
  IChangeSupportTicketStatusByAdmin,
  IReplySupportTicketByAdmin,
  IGetSupportTicketByAdmin,
  IGetReplyOfSupportTicketInfoByAdmin,
} from "./admin.support_ticket.req-schema"
import { UploadedFile } from "express-fileupload"
import {
  UserType,
  awsConfig,
  EventType,
  SupportTicketStatus,
} from "../../utilities/config"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { SupportTicketModel } from "../../model/support_ticket.model"
import { AdminUserModel } from "../../model/admin.user.model"
import { SupportTicketReplyModel } from "../../model/support_ticket_reply_model"
import { ObjectId } from "bson"
import { AuthModel } from "../../model/auth.model"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import { ClientNotificationType, VendorNotificationType } from "../../model/notification.model"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { VendorUserModel } from "../../model/vendor.user.model"

@injectable()
export class AdminSupportTicketRepository {
  private uniqueId: UniqueidService
  constructor(
    @inject(AdminTypes.SnsService) private snsService: SnsService,
    @inject(AdminTypes.CommonService) private commonService: CommonService,
    @inject(AdminTypes.NotificationUtility)
    private notUtil: NotificationUtility
  ) {
    this.uniqueId = new UniqueidService()
  }

  async createSupportTicketByAdmin(
    user_id: string,
    supportTicketData: ICreateSupportTicketByAdmin,
    fileData?: {
      attachments?: [UploadedFile]
    }
  ) {
    let doc: { [k: string]: any } = {}
    console.log(user_id)
    doc.created_by_id = user_id
    doc.created_by_type = UserType.ADMIN
    doc.client_id = supportTicketData.client_id
    doc.client_type = supportTicketData.client_type
    doc.question_type = supportTicketData.question_type
    doc.severity = supportTicketData.severity
    doc.subject = supportTicketData.subject
    doc.description = supportTicketData.description
    doc.ticket_id = await this.uniqueId.getUniqueTicketid()

    if (!isNullOrUndefined(supportTicketData.order_id)) {
      doc.order_id = supportTicketData.order_id
    }

    let mediaArr: {
      url: string
      type: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/support_ticket/attachments`

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments) &&
      Array.isArray(fileData.attachments)
    ) {
      await Promise.all(
        fileData.attachments.map(async (attachments) => {
          let objectName = `${user_id}_${Math.random()}`
          let media_type = "image"
          if (
            attachments.mimetype === "video/mp4" &&
            /\.mp4$/i.test(attachments.name)
          ) {
            objectName += ".mp4"
            media_type = "video"
          } else if (
            ["image/jpg", "image/jpeg", "image/png"].includes(
              attachments.mimetype
            ) &&
            /\.jpe?g$/i.test(attachments.name)
          ) {
            objectName += ".jpg"
            media_type = "image"
          } else if (
            ["application/msword"].includes(attachments.mimetype) &&
            /\.doc$/i.test(attachments.name)
          ) {
            objectName += ".doc"
            media_type = "msword"
          } else if (
            [
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            ].includes(attachments.mimetype) &&
            /\.docx$/i.test(attachments.name)
          ) {
            objectName += ".docx"
            media_type = "msword"
          } else if (
            attachments.mimetype === "application/pdf" &&
            /\.pdf$/i.test(attachments.name)
          ) {
            objectName += ".pdf"
            media_type = "pdf"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            attachments.data,
            attachments.mimetype
          )
          mediaArr.push({
            url: getS3MediaURL(`${objectDir}/${objectName}`),
            type: media_type,
          })
          if (mediaArr.length > 0) {
            doc.attachments = mediaArr
          }
          console.log(mediaArr)
          return Promise.resolve()
        })
      )
    }
    const supportTicketDoc = new SupportTicketModel(doc)
    const data = await supportTicketDoc.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_SUPPORT_TICKET_BY_ADMIN,
      `Admin added support_ticket of id ${data._id} .`
    )

    let adminUserDoc = await AdminUserModel.findById(user_id)
    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(
        new InvalidInput(`Sorry we did not find any data for this Admin user`, 400)
      )
    }

    // send mail for support ticket to client.
    await this.snsService.publishMessage({
      event_type: EventType.SEND_MAIL_USING_TEMPLATE,
      event_data: {
        template_name: "SUPPORT_TICKET",
        templateData: {
          subject: "Support Ticket Created.",
          html: {
            name: adminUserDoc.full_name,
            tickedId: doc.ticket_id,
          },
        },
        receiverInfo: {
          full_name: adminUserDoc.full_name,
          email: adminUserDoc.email,
        },
      },
    })

    // send email and sms to associated client also.
    return Promise.resolve(data)
  }

  async changeSupportTicketStatusByAdmin(
    user_id: string,
    ticketId: string,
    supportTicketStatusData: IChangeSupportTicketStatusByAdmin
  ) {
    const result = await AuthModel.findOne({
      code: supportTicketStatusData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const ticketData = await SupportTicketModel.findOne({
      ticket_id: ticketId,
    })

    if (isNullOrUndefined(ticketData)) {
      return Promise.reject(new InvalidInput(`No support ticket record found.`, 400))
    }

    let ticket_status = ticketData.support_ticket_status
    let status = ""

    // If status is OPEN then it update to CLOSED.
    if (
      ticket_status === SupportTicketStatus.OPEN &&
      supportTicketStatusData.support_ticket_status ===
        SupportTicketStatus.CLOSED
     
    ) {
      status = supportTicketStatusData.support_ticket_status
      if(ticketData.created_by_type === UserType.BUYER){
        await this.notUtil.addNotificationForBuyer({
          to_user_type: UserType.BUYER,
          to_user_id: ticketData.created_by_id,
          notification_type: ClientNotificationType.ResolveSupportTicketByAdmin,
          client_id: ticketData.created_by_id,
          ticket_id: ticketData._id,
        })
      }

      if(ticketData.created_by_type === UserType.VENDOR){
        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: ticketData.created_by_id,
          notification_type: VendorNotificationType.ResolveSupportTicketByAdmin,
          vendor_id: ticketData.created_by_id,
          ticket_id: ticketData._id,
        })
      }

      // If status is CLOSED then it update to OPEN.
    } else if (
      ticket_status === SupportTicketStatus.CLOSED &&
      supportTicketStatusData.support_ticket_status === SupportTicketStatus.OPEN
    ) {
      status = supportTicketStatusData.support_ticket_status

      //   // If status is SOLVED then it update to OPEN.
      // } else if (
      //   ticket_status === SupportTicketStatus.SOLVED &&
      //   supportTicketStatusData.support_ticket_status === SupportTicketStatus.OPEN
      // ) {
      //   status = supportTicketStatusData.support_ticket_status
    } else {
      return Promise.reject(
        new InvalidInput(
          ` Support ticket status couldn't be updated as support ticket  is ${supportTicketStatusData.support_ticket_status}`,
          400
        )
      )
    }
    await SupportTicketModel.updateOne(
      { ticket_id: ticketId },
      {
        $set: {
          support_ticket_status: status,
          updated_by_id: user_id,
          updated_by_type: UserType.ADMIN,
        },
      }
    )
    await AuthModel.deleteOne({
      user_id,
      code: supportTicketStatusData.authentication_code,
    })
    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_SUPPORT_TICKET_STATUS_BY_ADMIN,
      `Admin Updated details of support ticket id ${ticketId}.`,
      ticketData.support_ticket_status,
      status
    )

    return Promise.resolve()
  }

  async replySupportTicketByAdmin(
    user_id: string,
    supportTicketData: IReplySupportTicketByAdmin,
    fileData?: {
      attachments?: [UploadedFile]
    }
  ) {
    let doc: { [k: string]: any } = {}

    // Check ticket exists or not.
    const ticketData = await SupportTicketModel.findOne({
      ticket_id: supportTicketData.ticket_id,
    })

    if (isNullOrUndefined(ticketData)) {
      return Promise.reject(new InvalidInput(`No support ticket record found.`, 400))
    }

    doc.ticket_id = supportTicketData.ticket_id
    doc.reply_by_id = user_id
    doc.reply_by_type = UserType.ADMIN
    doc.comment = supportTicketData.comment

    let mediaArr: {
      url: string
      type: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/support_ticket/attachments`

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments) &&
      Array.isArray(fileData.attachments)
    ) {
      await Promise.all(
        fileData.attachments.map(async (attachments) => {
          let objectName = `${user_id}_${Math.random()}`
          let media_type = "image"
          if (
            attachments.mimetype === "video/mp4" &&
            /\.mp4$/i.test(attachments.name)
          ) {
            objectName += ".mp4"
            media_type = "video"
          } else if (
            ["image/jpg", "image/jpeg", "image/png"].includes(
              attachments.mimetype
            ) &&
            /\.jpe?g$/i.test(attachments.name)
          ) {
            objectName += ".jpg"
            media_type = "image"
          } else if (
            ["application/msword"].includes(attachments.mimetype) &&
            /\.doc$/i.test(attachments.name)
          ) {
            objectName += ".doc"
            media_type = "msword"
          } else if (
            [
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            ].includes(attachments.mimetype) &&
            /\.docx$/i.test(attachments.name)
          ) {
            objectName += ".docx"
            media_type = "msword"
          } else if (
            attachments.mimetype === "application/pdf" &&
            /\.pdf$/i.test(attachments.name)
          ) {
            objectName += ".pdf"
            media_type = "pdf"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            attachments.data,
            attachments.mimetype
          )
          mediaArr.push({
            url: getS3MediaURL(`${objectDir}/${objectName}`),
            type: media_type,
          })
          if (mediaArr.length > 0) {
            doc.attachments = mediaArr
          }
          console.log(mediaArr)
          return Promise.resolve()
        })
      )
    }
    const supportTicketDoc = new SupportTicketReplyModel(doc)
    const data = await supportTicketDoc.save()

    if(ticketData.created_by_type === UserType.VENDOR){
      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: ticketData.created_by_id,
        notification_type: VendorNotificationType.ReplyOfSupportTicketByAdmin,
        vendor_id: ticketData.created_by_id,
        ticket_id: ticketData._id,
      })
    }

    if(ticketData.created_by_type === UserType.BUYER){
      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: ticketData.created_by_id,
        notification_type: ClientNotificationType.ReplyOfSupportTicketByAdmin,
        client_id: ticketData.created_by_id,
        ticket_id: ticketData._id,
      })
    }
    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REPLY_OF_SUPPORT_TICKET_BY_ADMIN,
      `Admin replied of support_ticket of id ${data._id} .`
    )

    // Update ticket status. If existing status is CLOSED OR SOLVED then update it to OPEN else whatever user passes it updates.

    if (!isNullOrUndefined(supportTicketData.support_ticket_status)) {
      await SupportTicketModel.updateOne(
        { ticket_id: supportTicketData.ticket_id },
        {
          $set: {
            support_ticket_status: supportTicketData.support_ticket_status,
            updated_by_id: user_id,
            updated_by_type: UserType.ADMIN,
          },
        }
      )
    } else if (
      isNullOrUndefined(supportTicketData.support_ticket_status) &&
      // (ticketData.support_ticket_status === SupportTicketStatus.SOLVED ||
      ticketData.support_ticket_status === SupportTicketStatus.CLOSED
    ) {
      await SupportTicketModel.updateOne(
        { ticket_id: supportTicketData.ticket_id },
        {
          $set: {
            support_ticket_status: SupportTicketStatus.OPEN,
            updated_by_id: user_id,
            updated_by_type: UserType.ADMIN,
          },
        }
      )
    }
    return Promise.resolve(data)
  }

  async getSupportTicketByAdmin(
    user_id: string,
    searchParam: IGetSupportTicketByAdmin
  ) {
    let query: { [k: string]: any } = {}
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.support_ticket_status)) {
      query.support_ticket_status = searchParam.support_ticket_status
    }
    if (!isNullOrUndefined(searchParam.client_id)) {
      query.client_id = new ObjectId(searchParam.client_id)
    }
    if (!isNullOrUndefined(searchParam.severity)) {
      query.severity = searchParam.severity
    }
    if (!isNullOrUndefined(searchParam.subject)) {
      query.subject = searchParam.subject
    }
    if(!isNullOrUndefined(searchParam.ticket_id)){
      query.ticket_id = searchParam.ticket_id
    }
    if(!isNullOrUndefined(searchParam.client_type)){
      query.client_type = searchParam.client_type
    }

    if(!isNullOrUndefined(searchParam.buyer_name)){
      let buyerData = await BuyerUserModel.findOne({
        full_name : searchParam.buyer_name
      })
      if(!isNullOrUndefined(buyerData)){
        query.client_id = new ObjectId(buyerData._id)
      }else {
        query.client_id = ""
      }
    }

    if (!isNullOrUndefined(searchParam.company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: searchParam.company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
          {
            client_id: new ObjectId(user_id1),
          },
        ]
      } else {
        query.client_id = ""
      }
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "client_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "client_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "logistics_user",
          localField: "client_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: { path: "$logistics_user", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          question_type: true,
          severity: true,
          subject: true,
          description: true,
          attachments: true,
          support_ticket_status: true,
          order_id: true,
          created_at: true,
          created_by_id: true,
          created_by_type: true,
          client_id: true,
          client_type: true,
          "buyer.full_name": 1,
          "buyer.company_name": 1,
          "logistics_user.full_name": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1
        },
      },
    ]

    const ticketData = await SupportTicketModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      ticketData.reverse()
    }
    return Promise.resolve(ticketData)
  }
  async getSupportTicketInfoByAdmin(user_id: string, ticket_id: string) {
    let query: { [k: string]: any } = { ticket_id }
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "buyer",
          localField: "client_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "client_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "logistics_user",
          localField: "client_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: { path: "$logistics_user", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          question_type: true,
          severity: true,
          subject: true,
          description: true,
          attachments: true,
          support_ticket_status: true,
          order_id: true,
          created_at: true,
          created_by_id: true,
          created_by_type: true,
          client_id: true,
          client_type: true,
          "buyer.full_name": 1,
          "buyer.company_name": 1,
          "logistics_user.full_name": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1
        },
      },
    ]

    const ticketData = await SupportTicketModel.aggregate(aggregateArr)
    // // Find ticket data.
    // const ticketData = await SupportTicketModel.find({
    //   ticket_id,
    // }).select({
    //   ticket_id: 1,
    //   question_type: 1,
    //   severity: 1,
    //   subject: 1,
    //   description: 1,
    //   attachments: 1,
    //   support_ticket_status: 1,
    //   order_id: true,
    //   created_at: true,
    //   created_by_id: true,
    //   created_by_type: true,
    //   client_id: true,
    //   client_type: true,
    //   "buyer.full_name": 1,
    //   "logistics_user.full_name": 1,
    //   "supplier.full_name":1
    // })

    // if (ticketData.length < 0) {
    //   return Promise.reject(new InvalidInput(`Support ticket data was not found `, 400))
    // }
    // Find ticket reply data.
    const ticketReplyData = await SupportTicketReplyModel.find({
      ticket_id,
    }).limit(5)
    return Promise.resolve({
      ticketData,
      ticketReplyData,
    })
  }

  async getReplyListOfTicketByAdmin(
    user_id: string,
    ticket_id: string,
    searchParam: IGetReplyOfSupportTicketInfoByAdmin
  ) {
    let query: { [k: string]: any } = {
      ticket_id,
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
      sort.created_at = 1
    }

    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "reply_by_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "reply_by_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "logistics_user",
          localField: "reply_by_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: { path: "$logistics_user", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          comment: true,
          attachments: true,
          reply_by_id: true,
          reply_by_type: true,
          created_at: true,
          "buyer.full_name": 1,
          "buyer.company_name": 1,
          "logistics_user.full_name": 1,
          "supplier.full_name": 1,
        },
      },
    ]

    const ticketData = await SupportTicketReplyModel.aggregate(aggregateArr)

    if (isNullOrUndefined(ticketData)) {
      return Promise.reject(new InvalidInput(`No Ticket Doc found.`, 400))
    }
    if (sort.created_at === 1) {
      ticketData.reverse()
    }
    return Promise.resolve(ticketData)
  }
}
