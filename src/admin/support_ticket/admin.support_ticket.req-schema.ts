import { QuestionTypes, Severity } from "./../../utilities/config"
import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { ObjectId } from "bson"
import { UploadedFile } from "express-fileupload"

export interface ICreateSupportTicketByAdmin {
  question_type: string
  severity: string
  subject: string
  description: string
  support_ticket_status: string
  order_id: string
  client_id: string | ObjectId
  client_type: string
  created_by_id: string | ObjectId
  created_by_type: string
  files: {
    attachments: [UploadedFile]
  }
}

export const createSupportTicketByAdminSchema: IRequestSchema = {
  body: Joi.object().keys({
    question_type: Joi.string().valid(QuestionTypes).required(),
    severity: Joi.string().valid(Severity).required(),
    subject: Joi.string().required(),
    description: Joi.string().required(),
    order_id: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex).required(),
    client_type: Joi.string().valid("Buyer", "Vendor", "Logistics").required(),
  }),
}

export interface ICreateSupportTicketByAdminRequest
  extends IAuthenticatedRequest {
  body: ICreateSupportTicketByAdmin
}

export interface IChangeSupportTicketStatusByAdmin {
  support_ticket_status: string
  authentication_code: string
}

export const changeSupportTicketStatusByAdminSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    support_ticket_status: Joi.string()
      //.allow("OPEN", "INPROCESS", "CLOSED", "SOLVED")
      .valid("OPEN", "CLOSED")
      .required(),
    authentication_code: Joi.string().required(),
  }),
}

export interface IChangeSupportTicketByAdminRequest
  extends IAuthenticatedRequest {
  body: IChangeSupportTicketStatusByAdmin
}

export interface IReplySupportTicketByAdmin {
  ticket_id: string
  reply_by_id: string | ObjectId
  reply_by_type: string
  comment: string
  files: {
    attachments: [UploadedFile]
  }
  support_ticket_status?: string
}

export const replySupportTicketByAdminSchema: IRequestSchema = {
  body: Joi.object().keys({
    ticket_id: Joi.string().required(),
    comment: Joi.string().required(),
    support_ticket_status: Joi.string().allow("CLOSED", "SOLVED"),
  }),
}

export interface IReplySupportTicketByAdminRequest
  extends IAuthenticatedRequest {
  body: IReplySupportTicketByAdmin
}

export interface IGetSupportTicketByAdmin {
  support_ticket_status?: string
  before?: string
  after?: string
  client_id?: string
  subject?: string
  severity?: string
  ticket_id?: string
  client_type?: string
  buyer_name?: string
  company_name?: string
}

export interface IGetSupportTicketByAdminRequest extends IAuthenticatedRequest {
  query: IGetSupportTicketByAdmin
}

export const getSupportTicketByAdminSchema: IRequestSchema = {
  query: Joi.object().keys({
    support_ticket_status: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex),
    subject: Joi.string(),
    severity: Joi.string().valid(Severity),
    ticket_id: Joi.string(),
    client_type: Joi.string(),
    buyer_name: Joi.string(),
    company_name: Joi.string(),
  }),
}

export interface IGetSupportTicketInfoByAdmin {
  ticketId: string
  [k: string]: any
}

export interface IGetSupportTicketInfoByAdminRequest
  extends IAuthenticatedRequest {
  params: IGetSupportTicketInfoByAdmin
}

export const getSupportTicketInfoByAdminSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string(),
  }),
}

export interface IGetReplyOfSupportTicketInfoByAdmin {
  before?: string
  after?: string
}

export interface IGetReplyOfTicketByAdminRequest extends IAuthenticatedRequest {
  query: IGetReplyOfSupportTicketInfoByAdmin
}

export const getReplyOfSupportTicketByAdminSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
  }),
}
