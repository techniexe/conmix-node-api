import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddConcretePumpCategoryRequest extends IAuthenticatedRequest {
  body: {
    category_name: string
  }
}

export const addConcretePumpCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(2).max(64).required(),
  }),
}

export const editConcretePumpCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(2).max(64),
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    concretePumpCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IConcretePumpCategory {
  category_name: string
  is_active: boolean
  authentication_code: string
}

export interface IEditConcretePumpCategoryRequest
  extends IAuthenticatedRequest {
  body: IConcretePumpCategory
  params: {
    concretePumpCategoryId: string
  }
}

export interface IGetConcretePumpList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  concrete_pump_category_id?: string
  user_id?: string
}

export interface IGetConcretePumpRequest extends IAuthenticatedRequest {
  query: IGetConcretePumpList
}

export const getConcretePumpSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    concrete_pump_category_id: Joi.string().regex(objectIdRegex),
    user_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetConcretePumpDetailsRequest extends IAuthenticatedRequest {
  params: {
    ConcretePumpId: string
  }
}

export const getConcretePumpDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    ConcretePumpId: Joi.string().regex(objectIdRegex),
  }),
}
