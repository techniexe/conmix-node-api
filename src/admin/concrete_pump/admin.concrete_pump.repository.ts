import { InvalidInput, UnexpectedInput } from "./../../utilities/customError"
import { AuthModel } from "./../../model/auth.model"
import { CommonService } from "./../../utilities/common.service"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import {
  ConcretePumpCategoryModel,
  ConcretePumpModel,
} from "../../model/concrete_pump.model"
import { AccessTypes } from "../../model/access_logs.model"
import {
  IConcretePumpCategory,
  IGetConcretePumpList,
} from "./admin.concrete_pump.req-schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ObjectId } from "bson"

@injectable()
export class AdminConcretePumpRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addConcretePumpCategory(user_id: string, category_name: string) {
    const newConcretePump = new ConcretePumpCategoryModel({ category_name })
    const newConcretePumpDoc = await newConcretePump.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_CONCRETE_PUMP_CATEGORY,
      `Admin added new Concrete_pump category  of id ${newConcretePumpDoc._id} .`
    )

    return Promise.resolve(newConcretePumpDoc)
  }

  async updateConcretePumpCategory(
    user_id: string,
    concretePumpCategoryId: string,
    categoryData: IConcretePumpCategory
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: categoryData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    let data = await ConcretePumpCategoryModel.findOne({
      category_name: categoryData.category_name,
    })

      if (
      !isNullOrUndefined(data) &&
      data._id.equals(concretePumpCategoryId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`This concrete_pump category name "${categoryData.category_name}" already added.`, 400)
      )
    }

    const flds = ["category_name", "is_active"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(categoryData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by = user_id
    const res = await ConcretePumpCategoryModel.findOneAndUpdate(
      { _id: concretePumpCategoryId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await AuthModel.deleteOne({
      user_id,
      code: categoryData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_CONCRETE_PUMP_CATEGORY,
      `Admin Updated details of ConcretePump category of id ${concretePumpCategoryId}.`,
      res,
      categoryData
    )

    return Promise.resolve()
  }

  async listAllConcretePumpCategory() {
    return ConcretePumpCategoryModel.find()
  }

  async getConcretePump(user_id: string, searchParam: IGetConcretePumpList) {
    const query: { [k: string]: any } = {}

    //query.user_id = new ObjectId(user_id)

    let limit: number = 10
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.concrete_pump_category_id)) {
      query.concrete_pump_category_id = new ObjectId(
        searchParam.concrete_pump_category_id
      )
    }

    if (!isNullOrUndefined(searchParam.user_id)) {
      query.user_id = searchParam.user_id
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "concrete_pump_category_id",
          foreignField: "_id",
          as: "concrete_pump_category",
        },
      },
      {
        $unwind: {
          path: "$concrete_pump_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "operator_info",
          localField: "operator_id",
          foreignField: "_id",
          as: "operator_info",
        },
      },
      {
        $unwind: {
          path: "$operator_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: 1,
          concrete_pump_category_id: 1,
          company_name: 1,
          concrete_pump_model: 1,
          manufacture_year: 1,
          pipe_connection: 1,
          concrete_pump_capacity: 1,
          is_Operator: 1,
          operator_id: 1,
          is_helper: 1,
          transportation_charge: 1,
          concrete_pump_image_url: 1,
          concrete_pump_price: 1,
          created_at: 1,
          updated_at: 1,
          "concrete_pump_category.category_name": 1,
          "concrete_pump_category.is_active": 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "operator_info._id": 1,
          "operator_info.operator_name": 1,
          "operator_info.operator_mobile_number": 1,
        },
      },
    ]
    const data = await ConcretePumpModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getConcretePumpDetails(user_id: string, ConcretePumpId: string) {
    const query: { [k: string]: any } = {}
    query._id = new ObjectId(ConcretePumpId)
    //query.user_id = new ObjectId(user_id)

    console.log(query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "concrete_pump_category_id",
          foreignField: "_id",
          as: "concrete_pump_category",
        },
      },
      {
        $unwind: {
          path: "$concrete_pump_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          concrete_pump_category_id: 1,
          company_name: 1,
          concrete_pump_model: 1,
          manufacture_year: 1,
          pipe_connection: 1,
          concrete_pump_capacity: 1,
          is_Operator: 1,
          operator_id: 1,
          is_helper: 1,
          transportation_charge: 1,
          concrete_pump_image_url: 1,
          concrete_pump_price: 1,
          created_at: 1,
          updated_at: 1,
          "concrete_pump_category.category_name": 1,
          "concrete_pump_category.is_active": 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
        },
      },
    ]

    const data = await ConcretePumpModel.aggregate(aggregateArr)
    return Promise.resolve(data[0])
  }
}
