import { AdminTypes } from "./../admin.types"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { validate, IRequestSchema } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { InvalidInput } from "../../utilities/customError"
import { AdminConcretePumpRepository } from "./admin.concrete_pump.repository"
import {
  addConcretePumpCategorySchema,
  IAddConcretePumpCategoryRequest,
  editConcretePumpCategorySchema,
  IEditConcretePumpCategoryRequest,
  getConcretePumpSchema,
  IGetConcretePumpRequest,
  getConcretePumpDetailsSchema,
  IGetConcretePumpDetailsRequest,
} from "./admin.concrete_pump.req-schema"

@controller("/concrete_pump", verifyCustomToken("admin"))
@injectable()
export class AdminConcretePumpController {
  constructor(
    @inject(AdminTypes.AdminConcretePumpRepository)
    private ConcretePumpRepo: AdminConcretePumpRepository
  ) {}

  @httpPost("/category", validate(addConcretePumpCategorySchema))
  async addConcretePumpCategory(
    req: IAddConcretePumpCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.ConcretePumpRepo.addConcretePumpCategory(
        req.user.uid,
        req.body.category_name
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = `Unable to add concrete_pump Category`
      if (err.code === 11000) {
        msg = `This concrete_pump category "${req.body.category_name}" already added`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch(
    "/category/:concretePumpCategoryId",
    validate(editConcretePumpCategorySchema)
  )
  async updateConcretePumpCategory(
    req: IEditConcretePumpCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.ConcretePumpRepo.updateConcretePumpCategory(
      req.user.uid,
      req.params.concretePumpCategoryId,
      req.body
    )
    res.sendStatus(201)
  }

  @httpGet("/category")
  async listAllConcretePumpCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.listAllConcretePumpCategory()
    res.json({ data })
  }

  @httpGet("/", validate(getConcretePumpSchema))
  async getConcretePump(
    req: IGetConcretePumpRequest,
    res: Response,
    next: NextFunction
  ) {
    console.log(req.user.uid)
    const data = await this.ConcretePumpRepo.getConcretePump(
      req.user.uid,
      req.query
    )
    res.json({ data })
  }
  @httpGet("/:ConcretePumpId", validate(getConcretePumpDetailsSchema))
  async getConcretePumpDetails(
    req: IGetConcretePumpDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.getConcretePumpDetails(
      req.user.uid,
      req.params.ConcretePumpId
    )
    res.json({ data })
  }
}
