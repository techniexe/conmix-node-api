import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ICountry {
  country_id: number
  country_name: string
  country_code: string
}

export interface IEditCountry {
  country_name?: string
  country_code?: string
  authentication_code?: any
}

export interface IState {
  state_name: string
  country_id: number
  country_code: string
  country_name: string
}

export interface IEditState {
  state_name?: string
  country_id?: number
  country_code?: string
  country_name?: string
  authentication_code?: any
}

export interface ICity {
  state_id: string
  city_name: string
  location: {
    type?: string
    coordinates: [number]
  }
}

export interface IEditCity {
  city_name?: string
  location: {
    type?: string
    coordinates: [number]
  }
  authentication_code?: any
}

export interface ISubCity {
  sub_city_name: string
  city_id: string
  location: {
    type?: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
}

export interface IEditSubCity {
  sub_city_name?: string
  city_id?: string
  location?: {
    type?: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
}

export interface ICountryRequest extends IAuthenticatedRequest {
  body: ICountry
}

export interface IEditContryRequest extends IAuthenticatedRequest {
  params: {
    countryId: number
    [k: string]: any
  }
  body: IEditCountry
}

export interface IDeleteCountryRequest extends IAuthenticatedRequest {
  params: {
    countryId: number
    [k: string]: any
  }
}

export interface IStateRequest extends IAuthenticatedRequest {
  body: IState
}

export interface IEditStateRequest extends IAuthenticatedRequest {
  body: IEditState
}

export interface IDeleteStateRequest extends IAuthenticatedRequest {
  params: {
    stateId: string
  }
}

export interface ICityRequest extends IAuthenticatedRequest {
  body: ICity
}

export interface IEditCityRequest extends IAuthenticatedRequest {
  params: {
    cityId: string
  }
  body: IEditCity
}

export interface IDeleteCityRequest extends IAuthenticatedRequest {
  params: {
    cityId: string
  }
}

export interface ISubCityRequest extends IAuthenticatedRequest {
  body: ISubCity
}

export interface IEditSubCityRequest extends IAuthenticatedRequest {
  params: {
    subCityId: string
  }
  body: IEditSubCity
}

export interface IDeleteSubCityRequest extends IAuthenticatedRequest {
  params: {
    subCityId: string
  }
}

export interface ICountrySearchByAdmin {
  country_name?: string
  before?: string
  after?: string
}

export interface ICountrySearchByAdminReq extends IAuthenticatedRequest {
  query: ICountrySearchByAdmin
}

export interface IStateSearchByAdmin {
  state_name?: string
  country_code?: string
  before?: string
  after?: string
  is_page?: boolean
}

export interface IStateSearchByAdminReq extends IAuthenticatedRequest {
  query: IStateSearchByAdmin
}

export interface ICitySearchByAdmin {
  city_name?: string
  state_id?: string
  before?: string
  after?: string
  is_page?: boolean
  country_code?: string
}

export interface ICitySearchByAdminReq extends IAuthenticatedRequest {
  query: ICitySearchByAdmin
}

export interface ISubCitySearchByAdmin {
  sub_city_name?: string
  city_id?: string
}

export interface ISubCitySearchByAdminReq extends IAuthenticatedRequest {
  query: ISubCitySearchByAdmin
}
