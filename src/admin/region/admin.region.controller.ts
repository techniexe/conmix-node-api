import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { RegionRepository } from "./admin.region.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  countrySchema,
  editCountrySchema,
  removeCountrySchema,
  stateSchema,
  editStateSchema,
  removeStateSchema,
  citySchema,
  editCitySchema,
  removeCitySchema,
  subCitySchema,
  editsubCitySchema,
  removesubCitySchema,
  countrySearchByAdminReqSchema,
  stateSearchByAdminReqSchema,
  citySearchByAdminReqSchema,
  subCitySearchByAdminReqSchema,
} from "./admin.region.req-schema"
import {
  ICountryRequest,
  IEditContryRequest,
  IDeleteCountryRequest,
  IStateRequest,
  IEditStateRequest,
  IDeleteStateRequest,
  ICityRequest,
  IEditCityRequest,
  IDeleteCityRequest,
  ISubCityRequest,
  IEditSubCityRequest,
  IDeleteSubCityRequest,
  ICountrySearchByAdminReq,
  IStateSearchByAdminReq,
  ICitySearchByAdminReq,
  ISubCitySearchByAdminReq,
} from "./admin.region.req-defination"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/region", verifyCustomToken("admin"))
export class RegionController {
  constructor(
    @inject(AdminTypes.RegionRepository) private regionRepo: RegionRepository
  ) {}

  // Add country.
  @httpPost("/addCountry", validate(countrySchema))
  async addCountry(req: ICountryRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.regionRepo.addCountry(req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = `Unable to add country.`
      if (err.code === 11000) {
        msg = `This country has been added already.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Edit country.
  @httpPatch("/editCountry/:countryId", validate(editCountrySchema))
  async editCountry(
    req: IEditContryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { countryId } = req.params
      await this.regionRepo.editCountry(countryId, req.body)
      res.sendStatus(202)
    } catch (err) {
      let msg = `Unable to edit country.`
      if (err.code === 11000) {
        msg = `This country details has been added already.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Delete country.
  @httpDelete("/removeCountry/:countryId", validate(removeCountrySchema))
  async removeCountry(
    req: IDeleteCountryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.regionRepo.deleteCountry(req.params.countryId)
      res.sendStatus(204)
    } catch (err) {
      if (err.message === "CThis country doesn't exists") {
        return res.status(422).json({
          error: {
            message: "Invalid Request",
            details: [{ path: "countryId", message: err.message }],
          },
        })
      }
      next(err)
    }
  }

  @httpGet("/country", validate(countrySearchByAdminReqSchema))
  async searchCountry(req: ICountrySearchByAdminReq, res: Response) {
    const data = await this.regionRepo.searchCountry(req.query)
    res.json({ data })
  }

  // Add state.
  @httpPost("/addState", validate(stateSchema))
  async addState(req: IStateRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.regionRepo.addState(req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `State already exists.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Edit state.
  @httpPatch("/editState/:stateId", validate(editStateSchema))
  async editState(req: IEditStateRequest, res: Response, next: NextFunction) {
    try {
      const { stateId } = req.params
      await this.regionRepo.editState(stateId, req.body)
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `State details already exists.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Delete state.
  @httpDelete("/removeState/:stateId", validate(removeStateSchema))
  async removeState(
    req: IDeleteStateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.regionRepo.deleteState(req.params.stateId, req.body.authentication_code)
      res.sendStatus(204)
    } catch (err) {
      if (err.message === "State doesn't exists") {
        return res.status(422).json({
          error: {
            message: "Invalid Request",
            details: [{ path: "stateId", message: err.message }],
          },
        })
      }
      next(err)
    }
  }

  @httpGet("/state", validate(stateSearchByAdminReqSchema))
  async searchState(req: IStateSearchByAdminReq, res: Response) {
    const data = await this.regionRepo.searchState(req.query)
    res.json({ data })
  }

  // Add city.
  @httpPost("/addCity", validate(citySchema))
  async addCity(req: ICityRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.regionRepo.addCity(req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `City already exists.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Edit city.
  @httpPatch("/editCity/:cityId", validate(editCitySchema))
  async editCity(req: IEditCityRequest, res: Response, next: NextFunction) {
    try {
      const { cityId } = req.params
      await this.regionRepo.editCity(cityId, req.body)
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `City already exists.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Delete city.
  @httpDelete("/removeCity/:cityId", validate(removeCitySchema))
  async removeCity(req: IDeleteCityRequest, res: Response, next: NextFunction) {
    try {
      await this.regionRepo.deleteCity(req.params.cityId, req.body.authentication_code)
      res.sendStatus(204)
    } catch (err) {
      if (err.message === "City doesn't exists") {
        return res.status(422).json({
          error: {
            message: "Invalid Request",
            details: [{ path: "cityId", message: err.message }],
          },
        })
      }
      next(err)
    }
  }

  @httpGet("/city", validate(citySearchByAdminReqSchema))
  async searchCity(req: ICitySearchByAdminReq, res: Response) {
    const data = await this.regionRepo.searchCity(req.query)
    res.json({ data })
  }

  // Add subcity.
  @httpPost("/addSubCity", validate(subCitySchema))
  async addSubCity(req: ISubCityRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.regionRepo.addSubCity(req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `SubCity already exists.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  // Edit subcity.
  @httpPatch("/editSubCity/:subCityId", validate(editsubCitySchema))
  async editSubCity(
    req: IEditSubCityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { subCityId } = req.params
      await this.regionRepo.editSubCity(subCityId, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Delete subcity.
  @httpDelete("/removeSubCity/:subCityId", validate(removesubCitySchema))
  async removeSubCity(
    req: IDeleteSubCityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.regionRepo.deleteSubCity(req.params.subCityId)
      res.sendStatus(204)
    } catch (err) {
      if (err.message === "SubCity doesn't exists") {
        return res.status(422).json({
          error: {
            message: "Invalid Request",
            details: [{ path: "cityId", message: err.message }],
          },
        })
      }
      next(err)
    }
  }

  @httpGet("/subCity", validate(subCitySearchByAdminReqSchema))
  async searchSubCity(req: ISubCitySearchByAdminReq, res: Response) {
    const cities = await this.regionRepo.searchSubCity(req.query)
    res.json({ cities })
  }
}
