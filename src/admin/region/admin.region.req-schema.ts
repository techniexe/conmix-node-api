import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export const countrySchema: IRequestSchema = {
  body: Joi.object().keys({
    country_id: Joi.number().required(),
    country_name: Joi.string().required(),
    country_code: Joi.string().required(),
  }),
}

export const editCountrySchema: IRequestSchema = {
  params: Joi.object().keys({
    countryId: Joi.number().required(),
  }),
  body: Joi.object().keys({
    country_name: Joi.string(),
    country_code: Joi.string(),
    authentication_code: Joi.string(),


  }),
}

export const removeCountrySchema: IRequestSchema = {
  params: Joi.object().keys({
    countryId: Joi.number().required(),
  }),
}

export const countrySearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    country_name: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export const stateSchema: IRequestSchema = {
  body: Joi.object().keys({
    state_name: Joi.string().required(),
    country_id: Joi.number().required(),
    country_code: Joi.string().required(),
    country_name: Joi.string().required(),
    is_page: Joi.boolean(),
    authentication_code: Joi.string(),
  }),
}

export const editStateSchema: IRequestSchema = {
  params: Joi.object().keys({
    stateId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    state_name: Joi.string(),
    country_id: Joi.number(),
    country_code: Joi.string(),
    country_name: Joi.string(),
    authentication_code: Joi.string(),
  }),
}

export const removeStateSchema: IRequestSchema = {
  params: Joi.object().keys({
    stateId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string(),
  }),
}

export const stateSearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    state_name: Joi.string(),
    country_code: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    is_page: Joi.boolean(),
  }),
}

export const citySchema: IRequestSchema = {
  body: Joi.object().keys({
    state_id: Joi.string().regex(objectIdRegex).required(),
    city_name: Joi.string().required(),
    location: Joi.object()
      .keys({
        type: Joi.string().default("Point"),
        coordinates: Joi.array()
          .ordered([
            Joi.number().min(-180).max(180).required(),
            Joi.number().min(-90).max(90).required(),
          ])
          .length(2)
          .required(),
      })
      .required(),
  }),
}

export const editCitySchema: IRequestSchema = {
  params: Joi.object().keys({
    cityId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
    city_name: Joi.string(),
    authentication_code: Joi.string(),
  }),
}

export const removeCitySchema: IRequestSchema = {
  params: Joi.object().keys({
    cityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string(),
  }),
}
export const citySearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    country_code: Joi.string(),
    city_name: Joi.string(),
    state_id: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    is_page: Joi.boolean(),
  }),
}

export const subCitySchema: IRequestSchema = {
  body: Joi.object().keys({
    sub_city_name: Joi.string().required(),
    city_id: Joi.string().regex(objectIdRegex).required(),
    location: Joi.object()
      .keys({
        type: Joi.string().default("Point"),
        coordinates: Joi.array()
          .ordered([
            Joi.number().min(-180).max(180).required(),
            Joi.number().min(-90).max(90).required(),
          ])
          .length(2)
          .required(),
      })
      .required(),
  }),
}

export const editsubCitySchema: IRequestSchema = {
  params: Joi.object().keys({
    subCityId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    city_id: Joi.string().regex(objectIdRegex),
    sub_city_name: Joi.string().required(),
    location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
  }),
}

export const removesubCitySchema: IRequestSchema = {
  params: Joi.object().keys({
    subCityId: Joi.string().regex(objectIdRegex),
  }),
}

export const subCitySearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    sub_city_name: Joi.string(),
    city_id: Joi.string(),
  }),
}
