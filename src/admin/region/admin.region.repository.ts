import { injectable } from "inversify"
import {
  ICountry,
  IEditCountry,
  IState,
  IEditState,
  ICity,
  IEditCity,
  ISubCity,
  IEditSubCity,
  ICountrySearchByAdmin,
  IStateSearchByAdmin,
  ICitySearchByAdmin,
  ISubCitySearchByAdmin,
} from "./admin.region.req-defination"
import {
  ICountryDoc,
  CountryModel,
  IStateDoc,
  StateModel,
  CityModel,
  ICityDoc,
  ISubCityDoc,
  SubCityModel,
} from "../../model/region.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { escapeRegExp } from "../../utilities/mongoose.utilities"
import { AuthModel } from "../../model/auth.model"

@injectable()

// Add country.
export class RegionRepository {
  async addCountry(countryData: ICountry): Promise<ICountryDoc> {
    const { country_id, country_name, country_code } = countryData

    const saveCountryData = {
      country_id,
      country_name,
      country_code,
    }
    return new CountryModel(saveCountryData).save()
  }

  // Edit country
  async editCountry(
    country_id: number,
    countryData: IEditCountry
  ): Promise<any> {

    const result = await AuthModel.findOne({
      code: countryData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }
    const flds = ["country_name", "country_code"]
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    for (const [key, val] of Object.entries(countryData)) {
      if (flds.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    if (Object.keys(updt.$set).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const res = await CountryModel.updateOne({ country_id }, updt)
    if (res.n !== 1) {
      const err = new UnexpectedInput(`This country doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    return Promise.resolve()
  }

  // Delete country
  async deleteCountry(country_id: number) {
    const status = await CountryModel.deleteOne({ country_id })
    if (status.n !== 1) {
      throw new Error("Country doesn't exists")
    }
    return Promise.resolve()
  }

  async searchCountry(searchParam: ICountrySearchByAdmin) {
    const query: { [k: string]: any } = {}

    query.country_code = "IN"
    if (!isNullOrUndefined(searchParam.country_name)) {
      query.country_name = new RegExp(
        escapeRegExp(searchParam.country_name),
        "i"
      )
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
    }

    return CountryModel.find(query).sort({ created_at: -1 }).select({
      country_id: 1,
      country_code: 1,
      country_name: 1,
      created_at: 1,
    })
  }

  //Add state.
  async addState(stateData: IState): Promise<IStateDoc> {
    const { state_name, country_id, country_code, country_name } = stateData

    const saveStateData = {
      state_name,
      country_id,
      country_code,
      country_name,
    }
    return new StateModel(saveStateData).save()
  }

  // Edit state.
  async editState(state_id: string, stateData: IEditState): Promise<any> {
    
    const result = await AuthModel.findOne({
      code: stateData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }
    
    const flds = ["state_name", "country_id", "country_code", "country_name"]
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    for (const [key, val] of Object.entries(stateData)) {
      if (flds.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    if (Object.keys(updt.$set).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const res = await StateModel.updateOne({ _id: state_id }, updt)
    if (res.n !== 1) {
      const err = new UnexpectedInput(`This state doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    return Promise.resolve()
  }

  // Delete state.
  async deleteState(state_id: string, authentication_code: any) {
    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }
   
    const status = await StateModel.deleteOne({ _id: state_id })
    if (status.n !== 1) {
      throw new Error("State doesn't exists")
    }
    return Promise.resolve()
  }

  async searchState(searchParam: IStateSearchByAdmin) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.state_name)) {
      query.state_name = new RegExp(escapeRegExp(searchParam.state_name), "i")
    }
    query.country_code = "IN"
    if (!isNullOrUndefined(searchParam.country_code)) {
      query.country_code = searchParam.country_code
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
    }
    console.log("search", searchParam)
    if (
      !isNullOrUndefined(searchParam.is_page) &&
      searchParam.is_page === true
    ) {
      console.log("Hereee")
      return StateModel.find(query)
        .sort({ created_at: -1 })
        .select({
          state_name: 1,
          country_id: 1,
          country_code: 1,
          country_name: 1,
          created_at: 1,
        })
        .limit(10)
    }

    return StateModel.find(query).sort({ created_at: 1 }).select({
      state_name: 1,
      country_id: 1,
      country_code: 1,
      country_name: 1,
      created_at: 1,
    })
  }

  // Add city.
  async addCity(cityData: ICity): Promise<ICityDoc> {
    const { state_id, city_name, location } = cityData

    const stateData = await StateModel.findById(state_id)
    if (isNullOrUndefined(stateData)) {
      return Promise.reject(new InvalidInput(`No State data found.`, 400))
    }

    const saveCityData = {
      state_id,
      city_name,
      location,
      state_name: stateData.state_name,
      country_id: stateData.country_id,
      country_code: stateData.country_code,
      country_name: stateData.country_name,
    }
    return new CityModel(saveCityData).save()
  }

  // Edit city.
  async editCity(city_id: string, cityData: IEditCity): Promise<any> {
    
    const result = await AuthModel.findOne({
      code: cityData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }
    
    const flds = ["location", "city_name"]
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    for (const [key, val] of Object.entries(cityData)) {
      if (flds.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    if (Object.keys(updt.$set).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const res = await CityModel.updateOne({ _id: city_id }, updt)
    if (res.n !== 1) {
      const err = new UnexpectedInput(`This city doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    return Promise.resolve()
  }

  // Delete city.
  async deleteCity(city_id: string, authentication_code: any) {

    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const status = await CityModel.deleteOne({ _id: city_id })
    if (status.n !== 1) {
      throw new Error("City doesn't exists")
    }
    return Promise.resolve()
  }

  async searchCity(searchParam: ICitySearchByAdmin) {
    const query: { [k: string]: any } = {}

    query.country_code = "IN"
    if (!isNullOrUndefined(searchParam.country_code)) {
      query.country_code = searchParam.country_code
    }
    if (!isNullOrUndefined(searchParam.city_name)) {
      query.city_name = new RegExp(escapeRegExp(searchParam.city_name), "i")
    }
    if (!isNullOrUndefined(searchParam.state_id)) {
      query.state_id = searchParam.state_id
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
    }

    if (
      !isNullOrUndefined(searchParam.is_page) &&
      searchParam.is_page === true
    ) {
      return CityModel.find(query)
        .sort({ created_at: -1 })
        .select({
          city_name: 1,
          state_id: 1,
          state_name: 1,
          country_id: 1,
          country_code: 1,
          country_name: 1,
          created_at: 1,
          location: 1,
        })
        .limit(10)
    }
    return CityModel.find(query).sort({ created_at: 1 }).select({
      city_name: 1,
      state_id: 1,
      state_name: 1,
      country_id: 1,
      country_code: 1,
      country_name: 1,
      created_at: 1,
      location: 1,
    })
  }

  // Add subcity.
  async addSubCity(subCityData: ISubCity): Promise<ISubCityDoc> {
    const { sub_city_name, city_id, location } = subCityData

    const saveSubcityData = {
      sub_city_name,
      city_id,
      location,
    }
    return new SubCityModel(saveSubcityData).save()
  }

  // Edit subcity.
  async editSubCity(
    subCity_id: string,
    subCityData: IEditSubCity
  ): Promise<any> {
    const flds = ["sub_city_name", "city_id", "location"]
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    for (const [key, val] of Object.entries(subCityData)) {
      if (flds.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    if (Object.keys(updt.$set).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const res = await SubCityModel.updateOne({ _id: subCity_id }, updt)
    if (res.n !== 1) {
      const err = new UnexpectedInput(`This subCity doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    return Promise.resolve()
  }

  // Delete subcity.
  async deleteSubCity(subCity_id: string) {
    const status = await SubCityModel.deleteOne({ _id: subCity_id })
    if (status.n !== 1) {
      throw new Error("subCity doesn't exists")
    }
    return Promise.resolve()
  }

  async searchSubCity(searchParam: ISubCitySearchByAdmin) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.sub_city_name)) {
      query.city_name = new RegExp(escapeRegExp(searchParam.sub_city_name), "i")
    }
    if (!isNullOrUndefined(searchParam.city_id)) {
      query.state_id = searchParam.city_id
    }

    return SubCityModel.find(query).sort({ city_name: 1 }).limit(10).select({
      sub_city_name: 1,
      city_id: 1,
      location: 1,
      created_at: 1,
    })
  }
}
