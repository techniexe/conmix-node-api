import { inject, injectable } from "inversify"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { AuthModel, IAuthDoc } from "../../model/auth.model"
import { admin_mail_template } from "../../utilities/admin_email_template"
import { InvalidInput } from "../../utilities/customError"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { MailGunService } from "../../utilities/mailgun.service"
import { TestService } from "../../utilities/test.service"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AdminTypes } from "../admin.types"
//import { generateRandomNumber } from "../../utilities/generate-random-number"

@injectable()
export class AdminAuthRepository {
  constructor(
    @inject(AdminTypes.TestService) private testService: TestService,
 // //  @inject(VendorTypes.MailService) private mailService: MailService,
 // //  @inject(VendorTypes.TwilioService) private twilioService: TwilioService
 @inject(AdminTypes.mailgunService) private mailgunService: MailGunService 
 ) {}
  async getAdminAuthCode(identifier: string, user_id?: string) {
     const code = generateRandomNumber()

     let query: { [k: string]: any } = {}
     query.$or = [{
      mobile_number : identifier
     },
     {
      email : identifier
     }
    ]
    let adminUserData = await AdminUserModel.findOne(query)
    if(!isNullOrUndefined(adminUserData)){
      if(adminUserData.admin_type !== AdminRoles.superAdmin){
        return Promise.reject(
          new InvalidInput(`You are not authorized to reset password.`, 400)
        )
      }
    }

    const $setOnInsert: { [k: string]: any } = {
      identifier,
      user_type: "admin",
      code,
    }
    if (!isNullOrUndefined(user_id)) {
      $setOnInsert.user_id = user_id
    }
    
    const authData = await AuthModel.findOneAndUpdate(
      { identifier, user_type: "admin" },
      {
        $set: { created_at: Date.now() },
        $setOnInsert,
      },
      { upsert: true, new: true }
    ).exec()

    console.log(authData)
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let message = `${authData.code} is the OTP to Reset Password for Conmix Admin. It is valid upto 60 seconds. Kindly do not share the OTP with others.`  
    let templateId = "1707163673886746771"
    await this.testService.sendSMS(identifier, message, templateId)
  
    let forgot_pwd_html = admin_mail_template.forgot_pwd_otp
    forgot_pwd_html = forgot_pwd_html.replace("{{code}}", authData.code)
    var data = {
      from: "no-reply@conmate.in",
      to: identifier,
      subject: 'Conmix - Forgot Password One Time Password',
      html : forgot_pwd_html
    }
   await this.mailgunService.sendEmail(data)
  }
  async checkVerificationCodeWithDb(
    identifier: string,
    user_type: string,
    code: string
  ): Promise<IAuthDoc | null> {
    return AuthModel.findOne({ identifier, code, user_type }).exec()
  }
}
