import { controller, httpPost, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { NextFunction, Response } from "express"
import { verifySessionToken } from "../../middleware/auth-token.middleware"
import { loginUserSchema, ILoginUserRequest } from "./admin.auth.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  AdminUserRepository,
  getUserIdentifierType,
} from "../user/admin.user.repository"
import { createJWTToken } from "../../utilities/jwt.utilities"
//import { TwilioService } from "../../utilities/twilio-service"
import { AdminAuthRepository } from "./admin.auth.repository"
import { validate } from "../../middleware/joi.middleware"
//import { TestService } from "../../utilities/test.service"

@controller("/auth", verifySessionToken)
@injectable()
export class AdminAuthController {
  constructor(
    @inject(AdminTypes.AdminUserRepository)
    private adminUserRepo: AdminUserRepository,
   // @inject(AdminTypes.TestService) private testService: TestService,
    @inject(AdminTypes.AdminAuthRepository)
    private authRepo: AdminAuthRepository
  ) {}

  /**
   * Get auth code for a user that exists in the database.
   * This api will be called when a user with a mobile number
   * has forgotten his/her password.
   * @param req Express Authenticated Request
   * @param res Express Response
   * @param next Express Next Function
   */
  @httpGet("/users/:identifier")
  async getAuthCodeForUser(req: any, res: Response, next: NextFunction) {
    const { identifier } = req.params
    //const { method = "sms" } = req.query

    try {
      const identifierType = getUserIdentifierType(identifier)
      const user = await this.adminUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (user === null) {
        return res.status(404).json({
          error: {
            message: `Sorry. No user found with this email Id/mobile no.`,
          },
        })
      }

      if (isNullOrUndefined(user.mobile_number)) {
        return res.status(400).json({
          error: {
            message:
              "This User is not registered with entered mobile number. Password recovery request is Invalid",
          },
        })
      }

      const auth = await this.authRepo.getAdminAuthCode(identifier)

      console.log("auth", auth)
      if (auth === null) {
        return next("We are unable to send OTP to your mobile no.")
      }
    //  await this.testService.sendSMS()
      return res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /**
   * This API is called when the user with mobile number
   * wants to verify the code sent to him/her. (Forgot password/ for registered user only)
   * @param req
   * @param res
   * @param next
   */
  @httpPost("/users/:identifier/codes/:code")
  async verifyAuthCodeForUser(req: any, res: Response, next: NextFunction) {
    const { identifier, code } = req.params

    try {
      const identifierType = getUserIdentifierType(identifier)

      const user = await this.adminUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (isNullOrUndefined(user)) {
        return res.status(400).json({
          error: {
            message: `User with this ${identifier}  could not be found`,
          },
        })
      }

      const codeRow = await this.authRepo.checkVerificationCodeWithDb(
        identifier,
        "admin",
        code
      )

      if (isNullOrUndefined(codeRow)) {
        return res.status(400).json({
          error: { message: "You entered the wrong OTP. Kindly try again" },
        })
      }
      const jwtObj = {
        user_type: "admin",
        full_name: user.full_name,
        uid: user._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      next(err)
    }
  }
  /** User login with identifier */
  @httpPost("/:identifier", validate(loginUserSchema))
  async loginUserJwt(
    req: ILoginUserRequest,
    res: Response,
    next: NextFunction
  ) {
    console.log(req.body)
    const { identifier } = req.params
    const { password } = req.body

    try {
      const identifierType = getUserIdentifierType(identifier)
      const errorMsg = `Kindly enter valid email/mobile no. and password.`
      const user = await this.adminUserRepo.getUserWithPassword(
        identifierType,
        identifier,
        password
      )

      if (user === null) {
        return res.status(400).json({ error: { message: errorMsg } })
      }
      const jwtObj = {
        user_type: "admin",
        full_name: user.full_name,
        uid: user._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      next(err)
    }
  }
}
