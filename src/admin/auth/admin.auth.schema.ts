import { IJWTAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"

export const loginUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    password: Joi.string().required(),
  }),
}

export interface ILoginUserRequest extends IJWTAuthenticatedRequest {
  params: {
    identifier: string
  }
  body: {
    password: string
  }
}
