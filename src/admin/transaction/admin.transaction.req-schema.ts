import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetTransaction extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
    user_id?: string
    order_id?: string
    gateway_transaction_id?: string
    payment_gateway?: string
    transaction_status?: string
  }
}

export const getTransaction: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    user_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    gateway_transaction_id: Joi.string(),
    payment_gateway: Joi.string(),
    transaction_status: Joi.string(),
  }),
}

export interface IGetTransactionlog extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
    transaction_id?: string
    order_id?: string
    payment_gateway?: string
  }
}

export const getTransactionlog: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    transaction_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    payment_gateway: Joi.string(),
  }),
}
