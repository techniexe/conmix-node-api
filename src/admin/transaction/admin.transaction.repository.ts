import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  TransactionModel,
  TransactionLogModel,
} from "../../model/transaction.model"

@injectable()
export class TransactionRepository {
  async getTransaction(
    before?: string,
    after?: string,
    user_id?: string,
    order_id?: string,
    gateway_transaction_id?: string,
    transaction_status?: string,
    payment_gateway?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(user_id)) {
      query["user.user_id"] = user_id
    }
    if (!isNullOrUndefined(order_id)) {
      query.order_id = order_id
    }
    if (!isNullOrUndefined(gateway_transaction_id)) {
      query.gateway_transaction_id = gateway_transaction_id
    }
    if (!isNullOrUndefined(transaction_status)) {
      query.transaction_status = transaction_status
    }
    if (!isNullOrUndefined(payment_gateway)) {
      query.payment_gateway = payment_gateway
    }

    return TransactionModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 40,
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer" },
      },
      {
        $project: {
          gateway_transaction_id: 1,
          user_id: 1,
          order_id: 1,
          total_amount: 1,
          currency_code: 1,
          payment_gateway: 1,
          transaction_status: 1,
          created_at: 1,
          processed_at: 1,
          "buyer.full_name": 1,
        },
      },
    ])
  }

  async getTransactionlog(
    before?: string,
    after?: string,
    transaction_id?: string,
    order_id?: string,
    payment_gateway?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(transaction_id)) {
      query.transaction_id = transaction_id
    }
    if (!isNullOrUndefined(order_id)) {
      query.order_id = order_id
    }
    if (!isNullOrUndefined(payment_gateway)) {
      query.payment_gateway = payment_gateway
    }

    return TransactionLogModel.find(query)
      .sort({ created_at: -1 })
      .limit(40)
  }
}
