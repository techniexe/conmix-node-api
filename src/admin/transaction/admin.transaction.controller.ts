import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import { getTransaction, IGetTransaction } from "./admin.transaction.req-schema"
import { TransactionRepository } from "./admin.transaction.repository"

@injectable()
@controller("/transaction", verifyCustomToken("admin"))
export class TransactionController {
  constructor(
    @inject(AdminTypes.TransactionRepository)
    private transactionRepo: TransactionRepository
  ) {}

  @httpGet("/", validate(getTransaction))
  async getTransaction(
    req: IGetTransaction,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        before,
        after,
        user_id,
        order_id,
        gateway_transaction_id,
        transaction_status,
        payment_gateway,
      } = req.query
      const data = await this.transactionRepo.getTransaction(
        before,
        after,
        user_id,
        order_id,
        gateway_transaction_id,
        transaction_status,
        payment_gateway
      )
      res.json({ data })
    } catch (err) {
      return next(err)
    }
  }

  // @httpGet("/log", validate(getTransactionlog))
  // async getTransactionlog(
  //   req: IGetTransactionlog,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     const {
  //       before,
  //       after,
  //       transaction_id,
  //       order_id,
  //       payment_gateway,
  //     } = req.query
  //     const data = await this.transactionRepo.getTransactionlog(
  //       before,
  //       after,
  //       transaction_id,
  //       order_id,
  //       payment_gateway
  //     )
  //     res.json({ data })
  //   } catch (err) {
  //     return next(err)
  //   }
  // }
}
