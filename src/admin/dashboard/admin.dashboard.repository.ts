import { UserType } from "./../../utilities/config"
//import { LogisticsOrderStatus } from "./../../model/logistics.order.model"
import { SupplierOrderStatus } from "../../model/vendor.order.model"
// import { SupplierOrderStatus } from "./../../model/supplier.order.model"
import { OrderStatus } from "./../../model/order.model"
import { AdminUserModel } from "./../../model/admin.user.model"
import { injectable } from "inversify"
import { OrderModel } from "../../model/order.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { monthsCode } from "../../utilities/config"
import {
  LogisticsOrderModel,
  // DeliveryStatus,
} from "../../model/logistics.order.model"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { InvalidInput } from "../../utilities/customError"
import {
  IGetLatestOrderDetails,
  IGetLatestVendorOrderDetails,
  IGetLatestLogisticsOrderDetails,
  IGetUnverifiedProduct,
} from "./admin.dashboard.req-schema"
import { ProductModel } from "../../model/product.model"

@injectable()
export class DashboardRepository {
  async getDashboardDetails(user_id: string) {
    let adminUserDoc = await AdminUserModel.findById(user_id)
    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(
        new InvalidInput(`No Admin user found`, 400)
      )
    }

    let query: { [k: string]: any } = {}
    let data: { [k: string]: any } = {}
    //Get Total order
    let totalOrder = await OrderModel.countDocuments({})
    // Total Order Amount
    let totalOrderAmount = await OrderModel.aggregate([
      {
        $group: {
          _id: null,
          total: {
            $sum: "$total_amount",
          },
        },
      },
    ])

    // //Get Total Logistics order
    // let totalLogisticsOrder = await LogisticsOrderModel.countDocuments({})
    // // Total Logistics order amount
    // let totalLogisticsOrderAmount = await LogisticsOrderModel.aggregate([
    //   {
    //     $group: {
    //       _id: null,
    //       total: {
    //         $sum: "$total_amount",
    //       },
    //     },
    //   },
    // ])

    //Get Total Supplier order
    let totalVendorOrder = await VendorOrderModel.countDocuments({})

    // Total supplier order amount
    let totalVendorOrderAmount = await VendorOrderModel.aggregate([
      {
        $group: {
          _id: null,
          total: {
            $sum: "$total_amount_without_margin",
          },
        },
      },
    ])

    //Get Today's order and amount
    let start = new Date()
    start.setHours(0, 0, 0, 0)
    query = { created_at: { $gte: start } }
    let todayOrder = await OrderModel.countDocuments(query)

    let todayOrderAmount = await OrderModel.aggregate([
      { $match: query },
      { $group: { _id: null, total: { $sum: "$total_amount" } } },
    ])

    // //Get Today's Logistics order and amount
    // let todayLogisticsOrder = await LogisticsOrderModel.countDocuments(query)
    // let todayLogisticsOrderAmount = await LogisticsOrderModel.aggregate([
    //   { $match: query },
    //   { $group: { _id: null, total: { $sum: "$amount" } } },
    // ])

    //Get Today's Supplier order and amount
    let todayVendorOrder = await VendorOrderModel.countDocuments(query)
    let todayVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    //Get This week's order and amount
    let curr = new Date()
    curr.setHours(0, 0, 0, 0)
    var first = curr.getDate() - curr.getDay()
    var firstdayOfWeek = new Date(curr.setDate(first))
    query = { created_at: { $gte: firstdayOfWeek } }
    let OrderOfThisWeek = await OrderModel.countDocuments(query)

    let thisWeekOrderAmount = await OrderModel.aggregate([
      { $match: query },
      { $group: { _id: null, total: { $sum: "$total_amount" } } },
    ])

    // //Get This week's Logistics order and amount
    // let logisticsOrderOfThisWeek = await LogisticsOrderModel.countDocuments(
    //   query
    // )

    // let thisWeekLogisticsOrderAmount = await LogisticsOrderModel.aggregate([
    //   { $match: query },
    //   { $group: { _id: null, total: { $sum: "$amount" } } },
    // ])

    //Get This week's Supplier order
    let vendorOrderOfThisWeek = await VendorOrderModel.countDocuments(query)

    let thisWeekVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    //Get This month's order and amount
    var nowdate = new Date()
    var monthStartDay = new Date(nowdate.getFullYear(), nowdate.getMonth(), 1)
    query = { created_at: { $gte: monthStartDay } }
    let OrderOfThisMonth = await OrderModel.countDocuments(query)
    let thisMonthAmount = await OrderModel.aggregate([
      { $match: query },
      { $group: { _id: null, total: { $sum: "$total_amount" } } },
    ])

    // //Get This month's Logistics order and amount
    // let logisticsOrderOfThisMonth = await LogisticsOrderModel.countDocuments(
    //   query
    // )

    // let thisMonthLogisticsOrderAmount = await LogisticsOrderModel.aggregate([
    //   { $match: query },
    //   { $group: { _id: null, total: { $sum: "$amount" } } },
    // ])

    //Get This month's Supplier order
    let vendorOrderOfThisMonth = await VendorOrderModel.countDocuments(query)
    let thisMonthVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    // cancel order of buyer.
    query = { order_status: OrderStatus.CANCELLED }
    //const cancelOrderData = await OrderModel.find(query)

    const cancelorderaggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          display_id: true,
          created_at: true,
          total_amount: true,
          order_status: true,
          payment_status: true,
          admin_remarks: true,
          client_remarks: true,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const cancelOrderData = await OrderModel.aggregate(
      cancelorderaggregateArr
    ).limit(5)
    // Processing order of buyer.
    query = { order_status: OrderStatus.PROCESSING }
    // const processingOrderData = await OrderModel.find(query)

    const processingorderaggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          display_id: true,
          created_at: true,
          total_amount: true,
          order_status: true,
          payment_status: true,
          admin_remarks: true,
          client_remarks: true,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const processingOrderData = await OrderModel.aggregate(
      processingorderaggregateArr
    ).limit(5)

    let processingOrderDataAmount = await OrderModel.aggregate([
      { $match: query },
      { $group: { _id: null, total: { $sum: "$total_amount" } } },
    ])

    // cancel order of supplier.
    query = { order_status: SupplierOrderStatus.CANCELLED }

    console.log("here", query)
    const cancelVendorOrderData = await VendorOrderModel.find(query)

    // Processing order of supplier.
    query = { order_status: SupplierOrderStatus.RECEIVED }
    const processingVendorOrderData = await VendorOrderModel.find(query)
    let processingVendorOrderDataAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    // // cancel order of logisctics.
    // query = { delivery_status: LogisticsOrderStatus.CANCELLED }
    // const cancelLogiscticsOrderData = await LogisticsOrderModel.find(query)

    // // Processing order of logisctics.
    // query = { delivery_status: LogisticsOrderStatus.RECEIVED }
    // const processingLogisticsOrderData = await LogisticsOrderModel.find(query)
    // let processingLogisticsOrderDataAmount = await LogisticsOrderModel.aggregate(
    //   [
    //     { $match: query },
    //     { $group: { _id: null, total: { $sum: "$total_amount" } } },
    //   ]
    // )

    if (adminUserDoc.admin_type === "buyer_Admin") {
      console.log("sdddddddd")
      data.totalOrder = totalOrder
      data.totalOrderAmount = totalOrderAmount
      data.todayOrder = todayOrder
      data.todayOrderAmount = todayOrderAmount
      data.OrderOfThisWeek = OrderOfThisWeek
      data.thisWeekOrderAmount = thisWeekOrderAmount
      data.OrderOfThisMonth = OrderOfThisMonth
      data.thisMonthAmount = thisMonthAmount
      data.cancelOrderData = cancelOrderData
      data.processingOrderData = processingOrderData
      data.processingOrderDataAmount = processingOrderDataAmount
      return data
    }

    // if (adminUserDoc.admin_type === "logistics_Admin") {
    //   data.totalLogisticsOrder = totalLogisticsOrder
    //   data.totalLogisticsOrderAmount = totalLogisticsOrderAmount
    //   data.todayLogisticsOrder = todayLogisticsOrder
    //   data.todayLogisticsOrderAmount = todayLogisticsOrderAmount
    //   data.logisticsOrderOfThisWeek = logisticsOrderOfThisWeek
    //   data.thisWeekLogisticsOrderAmount = thisWeekLogisticsOrderAmount
    //   data.logisticsOrderOfThisMonth = logisticsOrderOfThisMonth
    //   data.thisMonthLogisticsOrderAmount = thisMonthLogisticsOrderAmount
    //   return data
    // }

    if (adminUserDoc.admin_type === "vendor_Admin") {
      data.totalVendorOrder = totalVendorOrder
      data.totalVendorOrderAmount = totalVendorOrderAmount
      data.todayVendorOrder = todayVendorOrder
      data.todayVendorOrderAmount = todayVendorOrderAmount
      data.vendorOrderOfThisWeek = vendorOrderOfThisWeek
      data.thisWeekVendorOrderAmount = thisWeekVendorOrderAmount
      data.vendorOrderOfThisMonth = vendorOrderOfThisMonth
      data.thisMonthVendorOrderAmount = thisMonthVendorOrderAmount
      data.cancelVendorOrderData = cancelVendorOrderData
      data.processingVendorOrderData = processingVendorOrderData
      data.processingVendorOrderDataAmount = processingVendorOrderDataAmount
      return data
    }

    data.totalOrder = totalOrder
    data.totalOrderAmount = totalOrderAmount
    data.todayOrder = todayOrder
    data.todayOrderAmount = todayOrderAmount
    data.OrderOfThisWeek = OrderOfThisWeek
    data.thisWeekOrderAmount = thisWeekOrderAmount
    data.OrderOfThisMonth = OrderOfThisMonth
    data.thisMonthAmount = thisMonthAmount
    data.cancelOrderData = cancelOrderData
    data.processingOrderData = processingOrderData
    data.processingOrderDataAmount = processingOrderDataAmount
    // data.totalLogisticsOrder = totalLogisticsOrder
    // data.totalLogisticsOrderAmount = totalLogisticsOrderAmount
    // data.todayLogisticsOrder = todayLogisticsOrder
    // data.todayLogisticsOrderAmount = todayLogisticsOrderAmount
    // data.logisticsOrderOfThisWeek = logisticsOrderOfThisWeek
    // data.thisWeekLogisticsOrderAmount = thisWeekLogisticsOrderAmount
    // data.logisticsOrderOfThisMonth = logisticsOrderOfThisMonth
    // data.thisMonthLogisticsOrderAmount = thisMonthLogisticsOrderAmount
    data.totalVendorOrder = totalVendorOrder
    data.totalVendorOrderAmount = totalVendorOrderAmount
    data.todayVendorOrder = todayVendorOrder
    data.todayVendorOrderAmount = todayVendorOrderAmount
    data.vendorOrderOfThisWeek = vendorOrderOfThisWeek
    data.thisWeekVendorOrderAmount = thisWeekVendorOrderAmount
    data.vendorOrderOfThisMonth = vendorOrderOfThisMonth
    data.thisMonthVendorOrderAmount = thisMonthVendorOrderAmount
    data.cancelVendorOrderData = cancelVendorOrderData
    data.processingVendorOrderData = processingVendorOrderData
    data.processingVendorOrderDataAmount = processingVendorOrderDataAmount
    // data.cancelLogiscticsOrderData = cancelLogiscticsOrderData
    // data.processingLogisticsOrderData = processingLogisticsOrderData
    // data.processingLogisticsOrderDataAmount = processingLogisticsOrderDataAmount
    return data
  }

  async getChartByDay(
    month: string | undefined,
    year: number | undefined,
    user_type: string
  ) {
    var now = new Date()
    let query: { [k: string]: any } = {}
    let vendorOrderQuery: { [k: string]: any } = {}
    //  let logisticsOrderQuery: { [k: string]: any } = {}
    // If month or year not passed, then check with current date.
    if (isNullOrUndefined(month) || isNullOrUndefined(year)) {
      if (user_type === UserType.BUYER) {
        query.created_at = {
          $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
        }
      }

      if (user_type === UserType.VENDOR) {
        vendorOrderQuery.created_at = {
          $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
        }
      }

      // logisticsOrderQuery.created_at = {
      //   $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
      // }
    } else {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)

      if (user_type === UserType.BUYER) {
        query.created_at = {
          $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
          $lte: new Date(endDate.getTime() - 1),
        }
      }
      if (user_type === UserType.VENDOR) {
        vendorOrderQuery.created_at = {
          $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
          $lte: new Date(endDate.getTime() - 1),
        }
      }

      // logisticsOrderQuery.created_at = {
      //   $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
      //   $lte: new Date(endDate.getTime() - 1),
      // }
    }

    // Total order, its amount and cancel order data.
    if (user_type === UserType.BUYER) {
      let TotalorderData = await OrderModel.aggregate([
        { $match: query },
        {
          $project: {
            _id: 1,
            year: { $year: "$created_at" },
            month: { $month: "$created_at" },
            day: { $dayOfMonth: "$created_at" },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: { year: "$year", month: "$month", day: "$day" },
            total_amount: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      query.order_status = OrderStatus.CANCELLED
      let CancelorderData = await OrderModel.aggregate([
        { $match: query },
        {
          $project: {
            _id: 1,
            year: { $year: "$created_at" },
            month: { $month: "$created_at" },
            day: { $dayOfMonth: "$created_at" },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: { year: "$year", month: "$month", day: "$day" },
            total_amount: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      return {
        TotalorderData,
        CancelorderData,
      }
    }
    if (user_type === UserType.VENDOR) {
      // Total supplier order, its amount and cancel supplier order data.
      let TotalorderData = await VendorOrderModel.aggregate([
        { $match: vendorOrderQuery },
        {
          $project: {
            _id: 1,
            year: { $year: "$created_at" },
            month: { $month: "$created_at" },
            day: { $dayOfMonth: "$created_at" },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: { year: "$year", month: "$month", day: "$day" },
            total_amount: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      vendorOrderQuery.order_status = SupplierOrderStatus.CANCELLED
      let CancelorderData = await VendorOrderModel.aggregate([
        { $match: vendorOrderQuery },
        {
          $project: {
            _id: 1,
            year: { $year: "$created_at" },
            month: { $month: "$created_at" },
            day: { $dayOfMonth: "$created_at" },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: { year: "$year", month: "$month", day: "$day" },
            total_amount: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      return {
        TotalorderData,
        CancelorderData,
      }
    }

    // Total logistics order, its amount and cancel logistics order data.
    // let TotalLogisticsorderData = await LogisticsOrderModel.aggregate([
    //   { $match: logisticsOrderQuery },
    //   {
    //     $project: {
    //       _id: 1,
    //       year: { $year: "$created_at" },
    //       month: { $month: "$created_at" },
    //       day: { $dayOfMonth: "$created_at" },
    //       total_amount: 1,
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: { year: "$year", month: "$month", day: "$day" },
    //       total_amount: { $sum: "$total_amount" },
    //       count: { $sum: 1 },
    //     },
    //   },
    // ])

    // logisticsOrderQuery.delivery_status = LogisticsOrderStatus.CANCELLED
    // let CancelLogisticsorderData = await LogisticsOrderModel.aggregate([
    //   { $match: logisticsOrderQuery },
    //   {
    //     $project: {
    //       _id: 1,
    //       year: { $year: "$created_at" },
    //       month: { $month: "$created_at" },
    //       day: { $dayOfMonth: "$created_at" },
    //       total_amount: 1,
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: { year: "$year", month: "$month", day: "$day" },
    //       total_amount: { $sum: "$total_amount" },
    //       count: { $sum: 1 },
    //     },
    //   },
    // ])
  }

  async getChartByMonth(year: number | undefined, user_type: string) {
    let query: { [k: string]: any } = {}
    let vendorOrderQuery: { [k: string]: any } = {}
    //let logisticsOrderQuery: { [k: string]: any } = {}
    console.log("user_type", user_type)
    // If year is not passed, then check with first date of current year.
    if (isNullOrUndefined(year)) {
      if (user_type === UserType.BUYER) {
        query.created_at = {
          $gte: new Date(new Date().getFullYear(), 0, 1),
        }
      }

      if (user_type === UserType.VENDOR) {
        vendorOrderQuery.created_at = {
          $gte: new Date(new Date().getFullYear(), 0, 1),
        }
      }
      // logisticsOrderQuery.created_at = {
      //   $gte: new Date(new Date().getFullYear(), 0, 1),
      // }
    } else {
      // If year is passed, then check with first and last date of given year.
      if (user_type === UserType.BUYER) {
        console.log("hereee")
        query.created_at = {
          $gte: new Date(year, 0, 1),
          $lte: new Date(year, 11, 31),
        }
      }
      if (user_type === UserType.VENDOR) {
        vendorOrderQuery.created_at = {
          $gte: new Date(year, 0, 1),
          $lte: new Date(year, 11, 31),
        }
      }

      // logisticsOrderQuery.created_at = {
      //   $gte: new Date(year, 0, 1),
      //   $lte: new Date(year, 11, 31),
      // }
    }

    console.log("queryyyyyyyyyy", query)
    if (user_type === UserType.BUYER) {
      let TotalorderData = await OrderModel.aggregate([
        { $match: query },
        {
          $project: {
            month: { $substr: ["$created_at", 0, 7] },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: "$month",
            sum: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      query.order_status = OrderStatus.CANCELLED
      let CancelorderData = await OrderModel.aggregate([
        { $match: query },
        {
          $project: {
            month: { $substr: ["$created_at", 0, 7] },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: "$month",
            sum: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      return {
        TotalorderData,
        CancelorderData,
      }
    }

    if (user_type === UserType.VENDOR) {
      let TotalorderData = await VendorOrderModel.aggregate([
        { $match: vendorOrderQuery },
        {
          $project: {
            month: { $substr: ["$created_at", 0, 7] },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: "$month",
            sum: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      vendorOrderQuery.order_status = SupplierOrderStatus.CANCELLED
      let CancelorderData = await VendorOrderModel.aggregate([
        { $match: vendorOrderQuery },
        {
          $project: {
            month: { $substr: ["$created_at", 0, 7] },
            total_amount: 1,
          },
        },
        {
          $group: {
            _id: "$month",
            sum: { $sum: "$total_amount" },
            count: { $sum: 1 },
          },
        },
      ])

      return {
        TotalorderData,
        CancelorderData,
      }
    }

    // let TotalLogisticsOrderData = await LogisticsOrderModel.aggregate([
    //   { $match: logisticsOrderQuery },
    //   {
    //     $project: {
    //       month: { $substr: ["$created_at", 0, 7] },
    //       total_amount: 1,
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: "$month",
    //       sum: { $sum: "$total_amount" },
    //       count: { $sum: 1 },
    //     },
    //   },
    // ])

    // logisticsOrderQuery.delivery_status = LogisticsOrderStatus.CANCELLED
    // let CancelLogisticsOrderData = await LogisticsOrderModel.aggregate([
    //   { $match: logisticsOrderQuery },
    //   {
    //     $project: {
    //       month: { $substr: ["$created_at", 0, 7] },
    //       total_amount: 1,
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: "$month",
    //       sum: { $sum: "$total_amount" },
    //       count: { $sum: 1 },
    //     },
    //   },
    // ])
  }

  async latestOrder(searchParam: IGetLatestOrderDetails) {
    const query: { [k: string]: any } = {}
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
        },
      },
      {
        $project: {
          _id: 1,
          display_id: 1,
          user_id: 1,
          order_status: 1,
          payment_status: 1,
          selling_price: 1,
          margin_price: 1,
          gst_amount: 1,
          TM_price: 1,
          CP_price: 1,
          delivery_location: 1,
          payment_attempt: 1,
          payment_mode: 1,
          base_amount: 1,
          logistics_amount: 1,
          margin_amount: 1,
          coupon_amount: 1,
          total_amount: 1,
          site_id: 1,
          site_name: 1,
          company_name: "$site.company_name",
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          created_at: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          mobile_number: "$site.mobile_number",
          alt_mobile_number: "$site.alt_mobile_number",
          "invoice._id": true,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const orderData = await OrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      orderData.reverse()
    }
    return Promise.resolve(orderData)
  }

  async latestVendorOrder(searchParam: IGetLatestVendorOrderDetails) {
    const query: { [k: string]: any } = {}
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    // Find delivered order by supplier in which bill alredy uploaded.

    // query.order_status = SupplierOrderStatus.DELIVERED
    // query.bill_path = { $exists: true }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "vendor._id": true,
          "vendor.full_name": true,
          "vendor.company_name": true,
        },
      },
    ]
    const orderData = await VendorOrderModel.aggregate(aggregateArr)
    return Promise.resolve(orderData)
  }

  async latestLogisticsOrder(searchParam: IGetLatestLogisticsOrderDetails) {
    const query: { [k: string]: any } = {}
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    // Find delivered order by logistics in which bill alredy uploaded.

    //    query.delivery_status = DeliveryStatus.DELIVERED
    //  query.bill_url = { $exists: true }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "vehicle",
          localField: "vehicle_id",
          foreignField: "_id",
          as: "vehicle_info",
        },
      },
      {
        $unwind: {
          path: "$vehicle_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vehicle_category",
          localField: "vehicle_info.vehicle_category_id",
          foreignField: "_id",
          as: "vehicle_category_info",
        },
      },
      {
        $unwind: { path: "$vehicle_category_info" },
      },

      {
        $lookup: {
          from: "vehicle_sub_category",
          localField: "vehicle_info.vehicle_sub_category_id",
          foreignField: "_id",
          as: "vehicle_sub_category_info",
        },
      },
      {
        $unwind: { path: "$vehicle_sub_category_info" },
      },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer_info",
        },
      },
      {
        $unwind: {
          path: "$buyer_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "supplier_id",
          foreignField: "_id",
          as: "supplier_info",
        },
      },
      {
        $unwind: {
          path: "$supplier_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "product",
          localField: "product_id",
          foreignField: "_id",
          as: "product_info",
        },
      },
      {
        $unwind: {
          path: "$product_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "product_info.pickup_address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "display_id",
          as: "order_info",
        },
      },
      {
        $unwind: {
          path: "$order_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "order_item",
          localField: "buyer_order_item_id",
          foreignField: "display_item_id",
          as: "order_item_info",
        },
      },
      {
        $unwind: {
          path: "$order_item_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order_info.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },

      {
        $project: {
          _id: true,
          created_at: true,
          quantity: true,
          pickup_quantity: true,
          royality_quantity: true,
          delivery_status: true,
          "vehicle_info._id": true,
          "vehicle_info.vehicle_category_id": true,
          "vehicle_info.vehicle_sub_category_id": true,
          "vehicle_info.vehicle_rc_number": true,
          "vehicle_info.vehicle_image_url": true,
          "vehicle_category_info._id": true,
          "vehicle_category_info.category_name": true,
          "vehicle_sub_category_info._id": true,
          "vehicle_sub_category_info.sub_category_name": true,
          "vehicle_sub_category_info.min_load_capacity": true,
          "vehicle_sub_category_info.max_load_capacity": true,
          "buyer_info._id": true,
          "buyer_info.full_name": true,
          "buyer_info.mobile_number": true,
          "supplier_info._id": true,
          "supplier_info.full_name": true,
          "supplier_info.mobile_number": true,
          "pickup_address_info.pickup_location":
            "$product_info.pickup_location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "delivery_address_info.pincode": true,
          "order_info._id": true,
          "order_item_info._id": true,
        },
      },
    ]
    const orderData = await LogisticsOrderModel.aggregate(aggregateArr)
    return Promise.resolve(orderData)
  }

  async getUnverifiedProductList(searchParam: IGetUnverifiedProduct) {
    const query: { [k: string]: any } = { verified_by_admin: false }
    const sort = { created_at: 1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = -1
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: {
          path: "$productSubCategory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          self_logistics: 1,
          user_id: 1,
          "productCategory._id": 1,
          "productSubCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ]
    const productData = await ProductModel.aggregate(aggregateArr)
    return Promise.resolve(productData)
  }
}
