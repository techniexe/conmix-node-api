import { controller, httpGet } from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { NextFunction, Response } from "express"
import { DashboardRepository } from "./admin.dashboard.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  GetDashboardChartDataByDay,
  IGetDashboardChartDataByDayReq,
  GetDashboardChartDataByMonth,
  IGetDashboardChartDataByMonthReq,
  IGetLatestOrderRequest,
  IGetLatestLogisticsOrderRequest,
  IGetUnverifiedProductRequest,
  IGetLatestVendorOrderRequest,
} from "./admin.dashboard.req-schema"

@injectable()
@controller("/dashboard", verifyCustomToken("admin"))
export class DashboardController {
  constructor(
    @inject(AdminTypes.DashboardRepository)
    private dashboardRepo: DashboardRepository
  ) {}

  @httpGet("/")
  async getDashboardDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getDashboardDetails(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/daychart", validate(GetDashboardChartDataByDay))
  async getChartByDay(
    req: IGetDashboardChartDataByDayReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getChartByDay(
        req.query.month,
        req.query.year,
        req.query.user_type
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/monthchart", validate(GetDashboardChartDataByMonth))
  async getChartByMonth(
    req: IGetDashboardChartDataByMonthReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getChartByMonth(
        req.query.year,
        req.query.user_type
      )

      console.log("Data", data)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/latestOrder")
  async latestOrder(
    req: IGetLatestOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.latestOrder(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/latestVendorOrder")
  async latestVendorOrder(
    req: IGetLatestVendorOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.latestVendorOrder(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
  /***************************************************************************************************************** */

  @httpGet("/latestLogisticsOrder")
  async latestLogisticsOrder(
    req: IGetLatestLogisticsOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.latestLogisticsOrder(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/unverifiedProduct")
  async getUnverifiedProductList(
    req: IGetUnverifiedProductRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getUnverifiedProductList(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
