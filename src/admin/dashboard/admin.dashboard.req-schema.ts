import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode } from "../../utilities/config"

export interface IGetDashboardChartDataByDay {
  month?: string
  year?: number
  user_type: string
}

export interface IGetDashboardChartDataByDayReq extends IAuthenticatedRequest {
  query: IGetDashboardChartDataByDay
}

export const GetDashboardChartDataByDay: IRequestSchema = {
  query: Joi.object().keys({
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    user_type: Joi.string(),
  }),
}

export interface IGetDashboardChartDataByMonth {
  year?: number
  user_type: string
}

export interface IGetDashboardChartDataByMonthReq
  extends IAuthenticatedRequest {
  query: IGetDashboardChartDataByMonth
}

export const GetDashboardChartDataByMonth: IRequestSchema = {
  query: Joi.object().keys({
    year: Joi.number(),
    user_type: Joi.string(),
  }),
}
export interface IGetLatestOrderDetails {
  before?: string
  after?: string
}
export interface IGetLatestOrderRequest extends IAuthenticatedRequest {
  body: IGetLatestOrderDetails
}

export interface IGetLatestVendorOrderDetails {
  before?: string
  after?: string
}
export interface IGetLatestVendorOrderRequest extends IAuthenticatedRequest {
  body: IGetLatestVendorOrderDetails
}

export interface IGetLatestLogisticsOrderDetails {
  before?: string
  after?: string
}
export interface IGetLatestLogisticsOrderRequest extends IAuthenticatedRequest {
  body: IGetLatestOrderDetails
}

export interface IGetUnverifiedProduct {
  before?: string
  after?: string
}
export interface IGetUnverifiedProductRequest extends IAuthenticatedRequest {
  body: IGetUnverifiedProduct
}
