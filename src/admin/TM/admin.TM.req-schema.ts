import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IAddTMCategoryRequest extends IAuthenticatedRequest {
  body: {
    category_name: string
  }
}

export const addTMCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(2).max(64).required(),
  }),
}

export const editTMCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(2).max(64),
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    TMCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface ITMCategory {
  category_name: string
  is_active: boolean
  authentication_code: string
}

export interface IEditTMCategoryRequest extends IAuthenticatedRequest {
  body: ITMCategory
  params: {
    TMCategoryId: string
  }
}

export interface ITMSubCategory {
  TM_category_id?: any
  sub_category_name: string
  load_capacity: number
  weight_unit_code: string
  weight_unit: string
}

export interface IAddTMSubCategoryRequest extends IAuthenticatedRequest {
  body: ITMSubCategory
  params: {
    TMCategoryId: string
  }
}

export const addTMSubCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    sub_category_name: Joi.string().max(64).min(2).required(),
    load_capacity: Joi.number().required(),
    weight_unit_code: Joi.string(), // default: Cu.mtr
    weight_unit: Joi.string(), // default: cubic metre
  }),
  params: Joi.object().keys({
    TMCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IEditTMSubCategory {
  sub_category_name?: string
  load_capacity?: number
  weight_unit_code?: string
  weight_unit?: string // Metric tonne
  is_active: boolean
  authentication_code: string
}

export interface IEditTMSubCategoryRequest extends IAuthenticatedRequest {
  body: IEditTMSubCategory
  params: {
    TMSubCategoryId: string
  }
}

export const editTMSubCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    sub_category_name: Joi.string().max(64).min(2),
    load_capacity: Joi.number(),
    weight_unit_code: Joi.string(),
    weight_unit: Joi.string(), // Metric tonne
    is_active: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    TMSubCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IListTMSubCategoryRequest extends IAuthenticatedRequest {
  params: {
    TMCategoryId: string
  }
}

export const listTMSubCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    TMCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IGetTMList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  TM_category_id?: string
  TM_sub_category_id?: string
  is_gps_enabled?: boolean
  is_insurance_active?: boolean
  user_id?: string
  TM_rc_number?: string
  company_name?: string
  min_trip_price?: any
  min_delivery_range?: any
  min_delivery_range_value?: any
  min_trip_price_range_value?: any
}

export interface IGetTMRequest extends IAuthenticatedRequest {
  query: IGetTMList
}

export const getTMSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    TM_category_id: Joi.string(),
    TM_sub_category_id: Joi.string().regex(objectIdRegex),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
    user_id: Joi.string().regex(objectIdRegex),
    TM_rc_number: Joi.string(),
    company_name: Joi.string(),
    min_trip_price: Joi.string(),
    min_delivery_range : Joi.number(),
    min_delivery_range_value: Joi.string(),
    min_trip_price_range_value: Joi.string(),
  }),
}

export const getTMDetailsRequest: IRequestSchema = {
  params: Joi.object().keys({
    TMId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetTMDetailsRequest extends IAuthenticatedRequest {
  params: {
    TMId: string
  }
}
