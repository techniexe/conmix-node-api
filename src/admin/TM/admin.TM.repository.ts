import { ObjectId } from "bson"
import {
  ITMCategory,
  ITMSubCategory,
  IEditTMSubCategory,
  IGetTMList,
} from "./admin.TM.req-schema"
import { injectable, inject } from "inversify"
import {
  TMCategoryModel,
  TMSubCategoryModel,
  TMModel,
} from "../../model/TM.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { VendorUserModel } from "../../model/vendor.user.model"

@injectable()
export class AdminTMRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addTMCategory(user_id: string, category_name: string) {
    const newVehicleCat = new TMCategoryModel({ category_name })
    const newVehicleCatDoc = await newVehicleCat.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_TM_CATEGORY,
      `Admin added new TM category  of id ${newVehicleCatDoc._id} .`
    )

    return Promise.resolve(newVehicleCatDoc)
  }

  async updateTMCategory(
    user_id: string,
    TMCategoryId: string,
    categoryData: ITMCategory
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: categoryData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const flds = ["category_name", "is_active"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(categoryData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by = user_id
    const res = await TMCategoryModel.findOneAndUpdate(
      { _id: TMCategoryId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await AuthModel.deleteOne({
      user_id,
      code: categoryData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_TM_CATEGORY,
      `Admin Updated details of TM category of id ${TMCategoryId}.`,
      res,
      categoryData
    )

    return Promise.resolve()
  }

  async listAllTMCategory() {
    return TMCategoryModel.find()
  }

  async addTMSubCategory(
    user_id: string,
    TMCategoryId: string,
    subCatObj: ITMSubCategory
  ) {
    console.log("Here")
    console.log("TMCategoryId", TMCategoryId)
    subCatObj.TM_category_id = new ObjectId(TMCategoryId)

    const newTMSubCat = new TMSubCategoryModel(subCatObj)
    const newTMSubCatDoc = await newTMSubCat.save()

    console.log("newTMSubCatDoc", newTMSubCatDoc)

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_TM_SUB_CATEGORY,
      `Admin added new TM category  of id ${newTMSubCatDoc._id} .`
    )

    return Promise.resolve(newTMSubCatDoc)
  }

  async updateTMSubCategory(
    user_id: string,
    TMSubCategoryId: string,
    catData: IEditTMSubCategory
  ) {
    const result = await AuthModel.findOne({
      code: catData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const updtObj: { [k: string]: any } = {}
    const updtFld = [
      "sub_category_name",
      "load_capacity",
      "weight_unit_code",
      "weight_unit",
      "is_active",
    ]
    for (const [key, val] of Object.entries(catData)) {
      if (updtFld.indexOf(key) > -1) {
        updtObj[key] = val
      }
    }
    if (Object.keys(updtObj).length === 0) {
      const err1 = new InvalidInput(
        `At least one filed is required for update.`
      )
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const updtRes = await TMSubCategoryModel.findOneAndUpdate(
      {
        _id: new ObjectId(TMSubCategoryId),
      },
      { $set: updtObj }
    )
    if (updtRes === null) {
      const err1 = new InvalidInput(`No such TM Sub Category exists.`)
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await AuthModel.deleteOne({
      user_id,
      code: catData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_TM_SUB_CATEGORY,
      `Admin Updated details of TM Sub category of id ${TMSubCategoryId}.`,
      updtRes,
      updtObj
    )

    return Promise.resolve()
  }

  async listVehicleSubCategory(TM_category_id?: string, searchParam?: any) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(TM_category_id)) {
      query.TM_category_id = new ObjectId(TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.load_capacity)) {
      query.load_capacity = parseInt(searchParam.load_capacity)
    }
    console.log("query", query)
    return TMSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      // {
      //   $limit: 10,
      // },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: { path: "$TM_category", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          sub_category_name: 1,
          TM_category_id: 1,
          load_capacity: 1,
          weight_unit_code: 1,
          weight_unit: 1,
          created_at: 1,
          "TM_category.category_name": 1,
        },
      },
    ])
  }

  async getTM(user_id: string, searchParam: IGetTMList) {
    const query: { [k: string]: any } = {}

    // query.user_id = new ObjectId(user_id)

    let limit: number = 10
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.TM_sub_category_id)) {
      query.TM_sub_category_id = new ObjectId(searchParam.TM_sub_category_id)
    }

    if (!isNullOrUndefined(searchParam.is_gps_enabled)) {
      query.is_gps_enabled = searchParam.is_gps_enabled
    }

    if (!isNullOrUndefined(searchParam.is_insurance_active)) {
      query.is_insurance_active = searchParam.is_insurance_active
    }

    if (!isNullOrUndefined(searchParam.user_id)) {
      query.user_id = searchParam.user_id
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number
    }

    if (!isNullOrUndefined(searchParam.min_trip_price) && 
    !isNullOrUndefined(searchParam.min_trip_price_range_value) &&
    searchParam.min_trip_price_range_value === "less") {
      query.min_trip_price = {$lte : parseInt(searchParam.min_trip_price)}
    }

    if (!isNullOrUndefined(searchParam.min_trip_price) && 
    !isNullOrUndefined(searchParam.min_trip_price_range_value) &&
    searchParam.min_trip_price_range_value === "more") {
      query.min_trip_price = {$gte : parseInt(searchParam.min_trip_price)}
    }

    if (!isNullOrUndefined(searchParam.min_delivery_range) &&
    !isNullOrUndefined(searchParam.min_delivery_range_value) &&
    searchParam.min_delivery_range_value === "less" ) {
      query.delivery_range = {$lte: parseInt(searchParam.min_delivery_range)}
    }

    if (!isNullOrUndefined(searchParam.min_delivery_range) &&
    !isNullOrUndefined(searchParam.min_delivery_range_value) &&
    searchParam.min_delivery_range_value === "more" ) {
      query.delivery_range = {$gte: parseInt(searchParam.min_delivery_range)}
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number
    }

    if (!isNullOrUndefined(searchParam.company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: searchParam.company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
        ]
      } else {
        query.vendor_id = ""
      }
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    console.log("query", query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: {
          path: "$TM_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: 1,
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.min_load_capacity": 1,
          "TM_sub_category.max_load_capacity": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getTMDetails(user_id: string, TMId: string) {
    const query: { [k: string]: any } = {}
    query._id = new ObjectId(TMId)
    // query.user_id = new ObjectId(user_id)

    console.log(query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: { path: "$TM_category" },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: { path: "$TM_sub_category" },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info" },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: "$address",
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: "$vendor",
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          pickup_location: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: 1,
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.load_capacity": 1,
          "driver1_info._id": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info._id": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)
    return Promise.resolve(data[0])
  }
}
