import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { AdminTMRepository } from "./admin.TM.repository"
import { validate, IRequestSchema } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addTMCategorySchema,
  IAddTMCategoryRequest,
  editTMCategorySchema,
  IEditTMCategoryRequest,
  addTMSubCategorySchema,
  IAddTMSubCategoryRequest,
  editTMSubCategorySchema,
  IEditTMSubCategoryRequest,
  listTMSubCategorySchema,
  IListTMSubCategoryRequest,
  getTMSchema,
  IGetTMRequest,
  getTMDetailsRequest,
  IGetTMDetailsRequest,
} from "./admin.TM.req-schema"
import { InvalidInput } from "../../utilities/customError"

@injectable()
@controller("/TM", verifyCustomToken("admin"))
export class AdminTMController {
  constructor(
    @inject(AdminTypes.AdminTMRepository)
    private TMRepo: AdminTMRepository
  ) {}

  @httpPost("/category", validate(addTMCategorySchema))
  async addTMCategory(
    req: IAddTMCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.TMRepo.addTMCategory(req.user.uid, req.body.category_name)
      res.sendStatus(202)
    } catch (err) {
      let msg = `Sorry, we could not add TM category`
      if (err.code === 11000) {
        msg = `This TM category "${req.body.category_name}" already added`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch("/category/:TMCategoryId", validate(editTMCategorySchema))
  async updateTMCategory(
    req: IEditTMCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try{
      await this.TMRepo.updateTMCategory(
        req.user.uid,
        req.params.TMCategoryId,
        req.body
      )
      res.sendStatus(201)
    }catch(err){
      let msg = `Sorry, we could not update TM category`
      if (err.code === 11000) {
        msg = `This TM category "${req.body.category_name}" already added`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  
  }

  @httpGet("/category")
  async listAllTMCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.listAllTMCategory()
    res.json({ data })
  }

  @httpPost("/subCategory/:TMCategoryId", validate(addTMSubCategorySchema))
  async addTMSubCategory(
    req: IAddTMSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.TMRepo.addTMSubCategory(
        req.user.uid,
        req.params.TMCategoryId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This TM subcategory "${req.body.sub_category_name}" already added.`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch("/subCategory/:TMSubCategoryId", validate(editTMSubCategorySchema))
  async updateTMSubCategory(
    req: IEditTMSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try{
      await this.TMRepo.updateTMSubCategory(
        req.user.uid,
        req.params.TMSubCategoryId,
        req.body
      )
      res.sendStatus(201)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This TM subcategory "${req.body.sub_category_name}" already added.`
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  
  }

  @httpGet("/subCategory")
  @httpGet("/subCategory/:TMCategoryId", validate(listTMSubCategorySchema))
  async listSubCategory(
    req: IListTMSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    const { TMCategoryId } = req.params
    const data = await this.TMRepo.listVehicleSubCategory(TMCategoryId, req.query)
    res.json({ data })
  }

  @httpGet("/", validate(getTMSchema))
  async getTM(req: IGetTMRequest, res: Response, next: NextFunction) {
    console.log(req.user.uid)
    const data = await this.TMRepo.getTM(req.user.uid, req.query)
    res.json({ data })
  }

  @httpGet("/:TMId", validate(getTMDetailsRequest))
  async getTMDetails(
    req: IGetTMDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.getTMDetails(req.user.uid, req.params.TMId)
    res.json({ data })
  }
}
