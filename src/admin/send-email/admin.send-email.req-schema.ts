import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { ISendMail } from "../../utilities/mail-service"
import { allowedAttachmentTypes } from "../../utilities/config"
import { UploadedFile } from "express-fileupload"

export interface ISendMailReq extends IAuthenticatedRequest {
  body: ISendMail
  files: {
    attachments: UploadedFile[]
  }
}

const allowedMimes = allowedAttachmentTypes.map(at => at.mime)
const allowedExtRegexp = new RegExp(
  "(\\" + allowedAttachmentTypes.map(at => at.ext).join("|\\") + ")$",
  "i"
)

const attachmentItemsSchema = Joi.object()
  .keys({
    name: Joi.string()
      .regex(allowedExtRegexp)
      .required(),
    data: Joi.any().required(),
    size: Joi.number().required(),
    mimetype: Joi.string()
      .valid(...allowedMimes)
      .required(),
  })
  .required()

export const sendMailSchema: IRequestSchema = {
  body: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    user_type: Joi.string(),
    to_email: Joi.string(),
    to_name: Joi.string(),
    from_email: Joi.string(),
    from_name: Joi.string(),
    html: Joi.string(),
    subject: Joi.string(),
    cc: Joi.string(),
    bcc: Joi.string(),
    headers: Joi.array().items({
      header_name: Joi.string().required(),
      header_value: Joi.string().required(),
    }),
    send_status: Joi.string(),
    send_error: Joi.string(),
  }),
  files: Joi.object().keys({
    attachments: Joi.array().items(attachmentItemsSchema),
  }),
}
