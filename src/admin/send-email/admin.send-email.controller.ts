import { controller, httpPost } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { sendMailSchema, ISendMailReq } from "./admin.send-email.req-schema"
import { SendMailRepository } from "./admin.send-email.repository"

@injectable()
@controller("/send-mail", verifyCustomToken("admin"))
export class SendMailController {
  constructor(
    @inject(AdminTypes.SendMailRepository)
    private sendMailRepo: SendMailRepository
  ) {}

  @httpPost("", validate(sendMailSchema))
  async sendMail(req: ISendMailReq, res: Response, next: NextFunction) {
    try {
      await this.sendMailRepo.sendMail(req.body, req.files)
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }
}
