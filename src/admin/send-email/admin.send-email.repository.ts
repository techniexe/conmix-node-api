import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { MailService, ISendMail } from "../../utilities/mail-service"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "../../utilities/type-guards"
import * as path from "path"
import { allowedAttachmentTypes, awsConfig } from "../../utilities/config"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"

const allowedAttachmentExt = allowedAttachmentTypes.map(at => at.ext)
const templateObjectDir = `${process.env.NODE_ENV !== "production"
  ? "d"
  : "p"}/email-template`

@injectable()
export class SendMailRepository {
  constructor(
    @inject(AdminTypes.MailService) private mailService: MailService
  ) {}

  async sendMail(
    emailData: ISendMail,
    fileData: { attachments?: UploadedFile[] }
  ) {
    try {
      if (
        !isNullOrUndefined(fileData) &&
        !isNullOrUndefined(fileData.attachments)
      ) {
        emailData.attachments = await Promise.all(
          fileData.attachments.map(async at => {
            const ext = path.extname(at.name)
            if (!allowedAttachmentExt.includes(ext.toLowerCase())) {
              return Promise.reject(
                new InvalidInput(
                  `Invalid attachment. Only ${allowedAttachmentExt.join(
                    ", "
                  )} allowed`,
                  400
                )
              )
            }
            const objectPath = `${templateObjectDir}/${Math.random()}-${Date.now()}${ext}`
            await putObject(
              awsConfig.s3MediaBucket,
              objectPath,
              at.data,
              at.mimetype
            )
            return Promise.resolve({
              attachment_url: getS3MediaURL(objectPath, false),
              attachment_name: at.name,
              attachment_type: at.mimetype,
              size: at.size,
            })
          })
        )
      }
      await this.mailService.sentMail(emailData)
      return Promise.resolve()
    } catch (err) {
      return Promise.resolve()
    }
  }
}
