import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ObjectId } from "bson"
import { InvoiceModel } from "../../model/invoice.model"

@injectable()
export class AdminInvoiceRepository {
  async getInvoice(
    user_id?: string,
    site_id?: string,
    product_subcategory_id?: string,
    order_id?: string,
    start_date?: string,
    end_date?: string
  ) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(user_id)) {
      query.user_id = user_id
    }

    if (!isNullOrUndefined(order_id)) {
      query.order_id = order_id
    }

    if (!isNullOrUndefined(site_id)) {
      query.site_id = site_id
    }
    if (!isNullOrUndefined(product_subcategory_id)) {
      query["items.sub_category_id"] = new ObjectId(product_subcategory_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.created_at = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.created_at = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.created_at = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    return InvoiceModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer" },
      },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          user_id: 1,
          order_id: 1,
          items: 1,
          payment_mode: 1,
          payment_status: 1,
          total_price: 1,
          created_at: 1,
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          state_name: "$state.state_name",
          city_name: "$city.city_name",
          pincode: 1,
          "buyer.full_name": 1,
          "buyer.mobile_number": 1,
          "buyer.landline_number": 1,
          "buyer.email": 1,
          "buyer.gst_number": 1,
          // person_name: 1,
          // title: 1,
          // email: 1,
          // mobile_number:1,
          // alt_mobile_number: 1,
          // whatsapp_number:1,
          // landline_number: 1
        },
      },
    ])

    // return await InvoiceModel.find(query)
    //   .sort({ created_at: -1 })
    //   .limit(40)
  }
}
