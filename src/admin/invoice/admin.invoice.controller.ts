import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { AdminInvoiceRepository } from "./admin.invoice.repository"
import { validate } from "../../middleware/joi.middleware"
import { Response, NextFunction } from "express"
import {
  IGetInvoiceByAdminRequest,
  getInvoiceByAdminSchema,
} from "./admin.invoice.schema"

@injectable()
@controller("/invoice", verifyCustomToken("admin"))
export class AdminInvoiceController {
  constructor(
    @inject(AdminTypes.AdminInvoiceRepository)
    private invoiceRepo: AdminInvoiceRepository
  ) {}

  @httpGet("/", validate(getInvoiceByAdminSchema))
  async getInvoice(
    req: IGetInvoiceByAdminRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        user_id,
        site_id,
        product_subcategory_id,
        order_id,
        start_date,
        end_date,
      } = req.query

      const data = await this.invoiceRepo.getInvoice(
        user_id,
        site_id,
        product_subcategory_id,
        order_id,
        start_date,
        end_date
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
