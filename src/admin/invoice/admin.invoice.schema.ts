import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetInvoiceByAdmin {
  user_id?: string
  site_id?: string
  product_subcategory_id?: string
  order_id?: string
  start_date?: string
  end_date?: string
}

export interface IGetInvoiceByAdminRequest extends IAuthenticatedRequest {
  query: IGetInvoiceByAdmin
}

export const getInvoiceByAdminSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    site_id: Joi.string().regex(objectIdRegex),
    product_subcategory_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}
