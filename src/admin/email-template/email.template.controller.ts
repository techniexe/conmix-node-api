import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { EmailTemplateRepository } from "./email.template.repository"
import {
  getTemplateSchema,
  addEmailTemplateSchema,
  editEmailTemplateSchema,
  IEditEmailTemplateReq,
  IAddEmailTemplateReq,
  addAttachmentSchema,
  IAddAttachmentReq,
  removeAttachmentSchema,
  IRemoveAttachmentReq,
} from "./email.template.req-schema"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"

@injectable()
@controller("/email-template", verifyCustomToken("admin"))
export class EmailTemplateController {
  constructor(
    @inject(AdminTypes.EmailTemplateRepository)
    private emailTempRepo: EmailTemplateRepository
  ) {}

  @httpPost("/:templateType", validate(addEmailTemplateSchema))
  async addEmailTemplate(
    req: IAddEmailTemplateReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.emailTempRepo.addEmailTemplate(
        req.params.templateType,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:templateType", validate(editEmailTemplateSchema))
  async addEditEmailTemplate(
    req: IEditEmailTemplateReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.emailTempRepo.modifyEmailTemplate(
        req.params.templateType,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/attachments/:templateType", validate(addAttachmentSchema))
  async addAttachment(req: IAddAttachmentReq, res: Response) {
    await this.emailTempRepo.addAttachment(req.params.templateType, req.files)
    return res.sendStatus(201)
  }

  @httpDelete("/attachments/:templateType", validate(removeAttachmentSchema))
  async deleteAttachments(req: IRemoveAttachmentReq, res: Response) {
    await this.emailTempRepo.deleteAttachments(
      req.params.templateType,
      req.body.attachment_ids
    )
    res.sendStatus(201)
  }

  @httpGet("/")
  async getEmailTemplateList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.emailTempRepo.getAllEmailTemplates()
    res.json({ data })
  }

  @httpGet("/:templateType", validate(getTemplateSchema))
  async getTemplate(req: IAuthenticatedRequest, res: Response) {
    const data = await this.emailTempRepo.getEmailTemplate(
      req.params.templateType
    )
    // const data = await this.emailTempRepo.getRenderedTemplate(req.params.templateType, { html: { name: "shambhu" }, subject: { name: "sham" } })
    res.json({ data })
  }
}
