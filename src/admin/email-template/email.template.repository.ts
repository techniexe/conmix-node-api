import { injectable } from "inversify"
import * as path from "path"
import {
  EmailTemplateModel,
  IEmailTemplateDoc,
} from "../../model/email.template.model"
import { IEmailTemplate } from "./email.template.req-schema"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "util"
import { allowedAttachmentTypes, awsConfig } from "../../utilities/config"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"

const templateObjectDir = `${
  process.env.NODE_ENV !== "production" ? "d" : "p"
}/email-template`

const allowedAttachmentExt = allowedAttachmentTypes.map((at) => at.ext)
@injectable()
export class EmailTemplateRepository {
  async addEmailTemplate(
    template_type: string,
    body: IEmailTemplate,
    fileData: { attachments?: UploadedFile[] }
  ): Promise<any> {
    const tCheck = await EmailTemplateModel.findOne({ template_type })
    if (tCheck !== null) {
      return Promise.reject(
        new InvalidInput(`This template has been added already.`, 400)
      )
    }

    body.template_type = template_type
    const arrFields = [
      "template_type",
      "html",
      "subject",
      "cc",
      "bcc",
      "from_email",
      "from_name",
      "headers",
    ]
    const tDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(body)) {
      if (arrFields.indexOf(key) > -1) {
        tDoc[key] = val
      }
    }

    console.log(tDoc)
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments)
    ) {
      tDoc.attachments = await Promise.all(
        fileData.attachments.map(async (at) => {
          const ext = path.extname(at.name)
          if (!allowedAttachmentExt.includes(ext.toLowerCase())) {
            return Promise.reject(
              new InvalidInput(
                `Invalid attachment. Only ${allowedAttachmentExt.join(
                  ", "
                )} allowed`,
                400
              )
            )
          }
          const objectPath = `${templateObjectDir}/${Math.random()}-${Date.now()}${ext}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            at.data,
            at.mimetype
          )
          return Promise.resolve({
            attachment_url: getS3MediaURL(objectPath, false),
            attachment_name: at.name,
            attachment_type: at.mimetype,
            size: at.size,
          })
        })
      )
    }
    return new EmailTemplateModel(tDoc).save()
  }

  async modifyEmailTemplate(
    template_type: string,
    body: IEmailTemplate
  ): Promise<any> {
    const arrFields = [
      "html",
      "subject",
      "cc",
      "bcc",
      "from_email",
      "from_name",
      "headers",
    ]
    const updated_at = Date.now()
    const updt: {
      $set: { [k: string]: any }
    } = {
      $set: { updated_at },
    }
    for (const [key, val] of Object.entries(body)) {
      if (arrFields.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    const res = await EmailTemplateModel.updateOne({ template_type }, updt)
    if (res.n === 0) {
      return Promise.reject(
        new InvalidInput(`Email template doesn't exists`, 400)
      )
    }
    return Promise.resolve()
  }

  async addAttachment(
    template_type: string,
    fileData: { attachments: UploadedFile[] }
  ) {
    const tCheck = await EmailTemplateModel.findOne({ template_type })
    if (tCheck === null) {
      return Promise.reject(
        new InvalidInput(`This template doen't exists.`, 400)
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments)
    ) {
      const attachments = await Promise.all(
        fileData.attachments.map(async (at) => {
          const ext = path.extname(at.name)
          if (!allowedAttachmentExt.includes(ext.toLowerCase())) {
            return Promise.reject(
              new InvalidInput(
                `Invalid attachment. Only ${allowedAttachmentExt.join(
                  ", "
                )} allowed`,
                400
              )
            )
          }
          const objectPath = `${templateObjectDir}/${Math.random()}-${Date.now()}${ext}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            at.data,
            at.mimetype
          )
          return Promise.resolve({
            attachment_url: getS3MediaURL(objectPath, false),
            attachment_name: at.name,
            attachment_type: at.mimetype,
            size: at.size,
          })
        })
      )
      await EmailTemplateModel.updateOne(
        { template_type },
        { $push: { attachments: { $each: attachments } } }
      )
    } else {
      return Promise.reject(new InvalidInput(`Attachement doen't exists.`, 400))
    }

    return Promise.resolve()
  }

  async deleteAttachments(template_type: string, attachment_ids: string[]) {
    const res = await EmailTemplateModel.updateOne(
      { template_type },
      {
        $pull: {
          attachments: { _id: { $in: attachment_ids } },
        },
      }
    )
    if (res.n !== 1) {
      return Promise.reject(new InvalidInput(`No Template matched`, 400))
    }
    return Promise.resolve()
  }
  async getEmailTemplate(
    template_type: string
  ): Promise<IEmailTemplateDoc | null> {
    return EmailTemplateModel.findOne({ template_type })
  }
  async getAllEmailTemplates(): Promise<IEmailTemplateDoc[]> {
    return EmailTemplateModel.find().select({
      html: 0,
    })
  }
}
