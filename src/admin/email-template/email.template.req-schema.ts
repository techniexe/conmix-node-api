import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import {
  emailTemplateTypes,
  allowedAttachmentTypes,
} from "../../utilities/config"
import { UploadedFile } from "express-fileupload"

export interface IEmailTemplate {
  template_type: string
  html?: string
  subject?: string
  cc?: string
  bcc?: string
  from_email: string
  from_name?: string
  headers?: [{ header_name: string; header_value: string }]
}

export interface IAddEmailTemplateReq extends IAuthenticatedRequest {
  body: IEmailTemplate
  params: {
    templateType: string
  }
  files: {
    attachments: UploadedFile[]
  }
}
export interface IEditEmailTemplateReq extends IAuthenticatedRequest {
  body: IEmailTemplate
  params: {
    templateType: string
  }
}

export interface IAddAttachmentReq extends IAuthenticatedRequest {
  params: {
    templateType: string
  }
  files: {
    attachments: UploadedFile[]
  }
}

export interface IRemoveAttachmentReq extends IAuthenticatedRequest {
  params: {
    templateType: string
  }
  body: {
    attachment_ids: string[]
  }
}

const allowedMimes = allowedAttachmentTypes.map(at => at.mime)
const allowedExtRegexp = new RegExp(
  "(\\" + allowedAttachmentTypes.map(at => at.ext).join("|\\") + ")$",
  "i"
)

const attachmentItemsSchema = Joi.object()
  .keys({
    name: Joi.string()
      .regex(allowedExtRegexp)
      .required(),
    data: Joi.any().required(),
    size: Joi.number().required(),
    mimetype: Joi.string()
      .valid(...allowedMimes)
      .required(),
  })
  .required()

const emailSchema = Joi.object().keys({
  html: Joi.string().required(),
  subject: Joi.string().required(),
  cc: Joi.string(),
  bcc: Joi.string(),
  from_email: Joi.string().required(),
  from_name: Joi.string(),
  headers: Joi.array().items({
    header_name: Joi.string().required(),
    header_value: Joi.string().required(),
  }),
})

const EditTemplateSchema = Joi.object().keys({
  html: Joi.string(),
  subject: Joi.string(),
  cc: Joi.string(),
  bcc: Joi.string(),
  from_email: Joi.string(),
  from_name: Joi.string(),
  headers: Joi.array().items({
    header_name: Joi.string(),
    header_value: Joi.string(),
  }),
})

export const addEmailTemplateSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string()
      .valid(emailTemplateTypes)
      .required(),
  }),
  body: emailSchema,
  files: Joi.object().keys({
    attachments: Joi.array().items(attachmentItemsSchema),
  }),
}
export const editEmailTemplateSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string()
      .valid(emailTemplateTypes)
      .required(),
  }),
  body: EditTemplateSchema,
}

export const addAttachmentSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string()
      .valid(emailTemplateTypes)
      .required(),
  }),
  files: Joi.object().keys({
    attachments: Joi.array()
      .items(attachmentItemsSchema)
      .required(),
  }),
}

export const removeAttachmentSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string()
      .valid(emailTemplateTypes)
      .required(),
  }),
  body: Joi.object().keys({
    attachment_ids: Joi.array().items(
      Joi.string()
        .regex(objectIdRegex)
        .required()
    ),
  }),
}

export const getTemplateSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string().valid(emailTemplateTypes),
  }),
}
