import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { AdminGstSlabRepository } from "./admin.gst_slab.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addGstSlabSchema,
  IAddGstSlabRequest,
  updateGstSlabSchema,
  IUpdateGstSlabRequest,
} from "./admin.gst_slab.schema"

@controller("/gst_slab", verifyCustomToken("admin"))
@injectable()
export class AdminGstSlabController {
  constructor(
    @inject(AdminTypes.AdminGstSlabRepository)
    private adminGstSlabRepo: AdminGstSlabRepository
  ) {}

  @httpPost("/", validate(addGstSlabSchema))
  async addGstSlab(req: IAddGstSlabRequest, res: Response, next: NextFunction) {
    try {
      const data = await this.adminGstSlabRepo.addGstSlab(
        req.user.uid,
        req.body
      )
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:gst_slab_id", validate(updateGstSlabSchema))
  async updateGstSlab(
    req: IUpdateGstSlabRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminGstSlabRepo.updateGstSlab(
        req.user.uid,
        req.params.gst_slab_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getGstSlab(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminGstSlabRepo.getGstSlab()
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
