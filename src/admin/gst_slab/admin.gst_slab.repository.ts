import { injectable, inject } from "inversify"
import { GstSlabModel } from "../../model/gst_slab.model"
import { IAddGstSlab, IUpdateGstSlab } from "./admin.gst_slab.schema"
import { InvalidInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class AdminGstSlabRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addGstSlab(user_id: string, gstSlabData: IAddGstSlab) {
    gstSlabData.created_by_id = user_id
    const newGstSlab = new GstSlabModel(gstSlabData)
    const newGstSlabDoc = await newGstSlab.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_GST_SLAB,
      `Admin added gst_slab of id ${newGstSlabDoc._id} .`
    )

    return Promise.resolve(newGstSlabDoc)
  }

  async updateGstSlab(
    user_id: string,
    gst_slab_id: string,
    gstSlabData: IUpdateGstSlab
  ) {
    const res = await AuthModel.findOne({
      code: gstSlabData.authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const arrEditFld = [
      "title",
      "igst_rate",
      "cgst_rate",
      "sgst_rate",
      "hsn_code",
    ]

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(gstSlabData)) {
      if (arrEditFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id
    try {
      const oldDoc = await GstSlabModel.findById(gst_slab_id)

      if (isNullOrUndefined(oldDoc)) {
        return Promise.reject(new InvalidInput(`No data was found `, 400))
      }
      const updtRes = await GstSlabModel.findOneAndUpdate(
        { _id: gst_slab_id },
        { $set: editDoc }
      )
      if (updtRes === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "admin",
        AccessTypes.UPDATE_GST_SLAB,
        `Admin Updated details of gst_slab id ${gst_slab_id}`,
        oldDoc,
        editDoc
      )
      await AuthModel.deleteOne({
        user_id,
        code: gstSlabData.authentication_code,
      })
      return Promise.resolve()
    } catch (err) {
      return Promise.reject(err)
    }
  }

  async getGstSlab() {
    return await GstSlabModel.find()
  }
}
