import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema } from "../../middleware/joi.middleware"

export interface IAddGstSlab {
  title: string
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  hsn_code: string
  created_by_id: string
}

export interface IAddGstSlabRequest extends IAuthenticatedRequest {
  body: IAddGstSlab
}

export const addGstSlabSchema: IRequestSchema = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    igst_rate: Joi.number().required(),
    cgst_rate: Joi.number().required(),
    sgst_rate: Joi.number().required(),
    hsn_code: Joi.string(),
  }),
}

export interface IUpdateGstSlab {
  title: string
  igst_rate: number
  cgst_rate: number
  sgst_rate: number
  hsn_code: string
  updated_by_id: string
  updated_at: string
  authentication_code: string
}

export interface IUpdateGstSlabRequest extends IAuthenticatedRequest {
  params: {
    gst_slab_id: string
  }
  body: IUpdateGstSlab
}

export const updateGstSlabSchema: IRequestSchema = {
  body: Joi.object().keys({
    title: Joi.string(),
    igst_rate: Joi.number(),
    cgst_rate: Joi.number(),
    sgst_rate: Joi.number(),
    hsn_code: Joi.string(),
    authentication_code: Joi.string().required(),
  }),
}
