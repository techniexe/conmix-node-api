import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { IAdminUserDoc, AdminUserModel, AdminRoles } from "../../model/admin.user.model"
import {
  ICreateAdminProfile,
  IEditAdminProfile,
  IGetAdminUserListing,
  IEditVendorProfile,
  IEditVendorplantAddress,
} from "./admin.user.req-defination"
import { isNullOrUndefined } from "../../utilities/type-guards"
import * as bcrypt from "bcryptjs"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { IBuyerUserDoc, BuyerUserModel } from "../../model/buyer.user.model"
import { escapeRegExp } from "../../utilities/mongoose.utilities"
import { IVendorUserDoc, VendorUserModel } from "../../model/vendor.user.model"
import { AuthModel } from "../../model/auth.model"
// import { generateRandomNumber } from "../../utilities/generate-random-number"
import { AdminTypes } from "../admin.types"
//import { TwilioService } from "../../utilities/twilio-service"
import { DeletedAdminUserModel } from "../../model/deleted_admin_user.model"
import { AccessTypes } from "../../model/access_logs.model"
import { CommonService } from "../../utilities/common.service"
import { AddressModel } from "../../model/address.model"
import { TestService } from "../../utilities/test.service"
import { MailGunService } from "../../utilities/mailgun.service"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { admin_mail_template } from "../../utilities/admin_email_template"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
import { TMCategoryModel, TMSubCategoryModel } from "../../model/TM.model"
import {
  AggregateSandCategoryModel,
  AggregateSandSubCategoryModel,
} from "../../model/aggregate_sand_category.model"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { CementBrandModel } from "../../model/cement_brand.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { ConcretePumpCategoryModel } from "../../model/concrete_pump.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { SandSourceModel } from "../../model/sand_source.model"
import { CityModel, StateModel } from "../../model/region.model"
import { CouponModel } from "../../model/coupon.model"
const SALT_WORK_FACTOR = 10

/**
 * Ways in which a user can be identified
 */
export enum UserIdentifiers {
  mobile_number,
  email,
}
export function getUserIdentifierType(identifier: string): UserIdentifiers {
  if (identifier.length === 0) {
    throw new Error("Identifier cannot be of length zero.")
  }
  if (identifier.charAt(0) === "+") {
    return UserIdentifiers.mobile_number
  }
  return UserIdentifiers.email
}

@injectable()
export class AdminUserRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService,
    @inject(AdminTypes.TestService) private testService: TestService,
    @inject(AdminTypes.mailgunService) private mailgunService: MailGunService // @inject(AdminTypes.TwilioService) private twilioService: TwilioService
  ) {}
  async getUserFromDbUsing(
    identifierType: UserIdentifiers,
    value: string
  ): Promise<IAdminUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = value
    return AdminUserModel.findOne(query).exec()
  }

  async getUserWithPassword(
    identifierType: UserIdentifiers,
    identifierValue: string,
    password: string
  ): Promise<IAdminUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = identifierValue
    console.log("query:", query)
    const doc = await AdminUserModel.findOne(query).exec()

    if (!isNullOrUndefined(doc) && doc.is_active === false) {
      return Promise.reject(
        new InvalidInput(
          `Sorry you cannot login right now, as your account is deactivated`,
          400
        )
      )
    }

    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.resolve(null)
    }
    if (await bcrypt.compare(password, doc.password)) {
      return Promise.resolve(doc)
    }
    return Promise.resolve(null)
  }

  /**method used for creating fresh user with mobile number token */
  async createUser(
    uid: string,
    createBody: ICreateAdminProfile
  ): Promise<IAdminUserDoc> {
    let adminUserDoc = await AdminUserModel.findById(uid)
    if (
      !isNullOrUndefined(adminUserDoc) &&
      adminUserDoc.admin_type === "superAdmin"
    ) {
      let createDoc: { [k: string]: any } = {
        full_name: createBody.full_name.trim(),
        password: await bcrypt.hash(createBody.password, SALT_WORK_FACTOR),
        created_by: uid,
      }

      if (!isNullOrUndefined(createBody.mobile_number)) {
        let clientDoc = await BuyerUserModel.findOne({
          mobile_number: createBody.mobile_number,
        })

        if (!isNullOrUndefined(clientDoc)) {
          return Promise.reject(
            new InvalidInput(
              `Same mobile number already exists with Client.`,
              400
            )
          )
        }

        let vendorDoc = await VendorUserModel.findOne({
          mobile_number: createBody.mobile_number,
        })

        if (!isNullOrUndefined(vendorDoc)) {
          return Promise.reject(
            new InvalidInput(
              `Same mobile number already exists with Vendor.`,
              400
            )
          )
        }

        createDoc.mobile_verified = true
      }
      if (!isNullOrUndefined(createBody.email)) {
        let clientDoc = await BuyerUserModel.findOne({
          email: createBody.email,
        })

        if (!isNullOrUndefined(clientDoc)) {
          return Promise.reject(
            new InvalidInput(`Same email already exists with Client.`, 400)
          )
        }

        let vendorDoc = await VendorUserModel.findOne({
          email: createBody.email,
        })

        if (!isNullOrUndefined(vendorDoc)) {
          return Promise.reject(
            new InvalidInput(`Same email already exists with Vendor.`, 400)
          )
        }

        createDoc.email_verified = true
      }
      const arrKeys = ["email", "mobile_number", "admin_type"]
      for (const [key, value] of Object.entries(createBody)) {
        if (arrKeys.indexOf(key) > -1) {
          createDoc[key] = value
        }
      }

      const doc = new AdminUserModel(createDoc)
      const newUserDoc = await doc.save()

      await this.commonService.addActivityLogs(
        uid,
        "admin",
        AccessTypes.CREATE_ADMIN_PROFILE,
        `Admin profile created with id ${newUserDoc._id} .`
      )
      return Promise.resolve(newUserDoc)
    } else {
      return Promise.reject(
        new InvalidInput(
          `You are not authorized to create an Admin account.`,
          400
        )
      )
    }
  }

  async getUserProfile(uid: string): Promise<IAdminUserDoc | null> {
    return AdminUserModel.findById(uid)
  }

  async requestOTP(user_id: string, reqData: any) {
    // const userDoc = await AdminUserModel.findById(user_id)

    // if (isNullOrUndefined(userDoc)) {
    //   return Promise.reject(
    //     new InvalidInput(`No Admin document could be found`, 400)
    //   )
    // }

    const userDoc = await AdminUserModel.findOne({
      admin_type: "superAdmin",
    })

    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    }

    if (
      !isNullOrUndefined(reqData.old_password) &&
      !isNullOrUndefined(reqData.password)
    ) {
      if (await bcrypt.compare(reqData.old_password, userDoc.password)) {
        const code = generateRandomNumber()

        const authData = await AuthModel.findOneAndUpdate(
          {
            identifier: userDoc.mobile_number,
            user_id,
          },
          {
            $set: { code, created_at: Date.now() },
          },
          { upsert: true, new: true }
        ).exec()
        if (authData === null) {
          return Promise.reject(
            new InvalidInput(`Unable to get Auth Code.`, 400)
          )
        }

        let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
        // Registration OTP.
        let templateId = "1707163645095255064"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
        return Promise.resolve(authData)
      } else {
        return Promise.reject(
          new InvalidInput(
            `The old password not matched with existing password. `,
            400
          )
        )
      }
    }

    const code = generateRandomNumber()

    const authData = await AuthModel.findOneAndUpdate(
      {
        identifier: userDoc.mobile_number,
        user_id,
      },
      {
        $set: { code, created_at: Date.now() },
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    if (reqData.event_type === "edit_TM_category") {
      if (
        !isNullOrUndefined(reqData.category_name) &&
        !isNullOrUndefined(reqData.TMCategoryId)
      ) {
        const catData = await TMCategoryModel.findOne({
          category_name: reqData.category_name,
        })
        if (
          !isNullOrUndefined(catData) &&
          catData._id.equals(reqData.TMCategoryId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This TM category "${reqData.category_name}" already added`,
              400
            )
          )
        }

        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of TM Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164095482055121"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "edit_TM_subcategory") {
      if (
        !isNullOrUndefined(reqData.sub_category_name) &&
        !isNullOrUndefined(reqData.TMSubCategoryId)
      ) {
        const subcatData = await TMSubCategoryModel.findOne({
          sub_category_name: reqData.sub_category_name,
        })
        if (
          !isNullOrUndefined(subcatData) &&
          subcatData._id.equals(reqData.TMSubCategoryId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This TM subcategory "${reqData.sub_category_name}" already added.`,
              400
            )
          )
        }
        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of TM SubCategory for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164095488581812"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )

      }
    } 
    else if (reqData.event_type === "edit_agg_category") {
      console.log("heree")
      console.log("reqData", reqData)
      if (
        !isNullOrUndefined(reqData.agg_cat_name) &&
        !isNullOrUndefined(reqData.categoryId)
      ) {
        const catCheck = await AggregateSandCategoryModel.findOne({
          category_name: reqData.agg_cat_name,
        })

        if (
          !isNullOrUndefined(catCheck) &&
          catCheck._id.equals(reqData.categoryId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `The entered category name "${reqData.agg_cat_name}" already added `,
              400
            )
          )
        }
      }
    } else if (reqData.event_type === "edit_agg_sub_category") {
      if (
        !isNullOrUndefined(reqData.agg_sub_cat_name) &&
        !isNullOrUndefined(reqData.subCategoryId)
      ) {
        const subCatCheck = await AggregateSandSubCategoryModel.findOne({
          sub_category_name: reqData.agg_sub_cat_name,
        })

        if (
          !isNullOrUndefined(subCatCheck) &&
          subCatCheck._id.equals(reqData.subCategoryId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `The entered sub category name "${reqData.agg_sub_cat_name}" already added `,
              400
            )
          )
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Aggregate and Sand Sub Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095294533467"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_cg") {
      if (
        !isNullOrUndefined(reqData.cg_name) &&
        !isNullOrUndefined(reqData.cgId)
      ) {
        const check = await ConcreteGradeModel.findOne({
          name: reqData.cg_name,
        })

        if (
          !isNullOrUndefined(check) &&
          check._id.equals(reqData.cgId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This Concrete Grade name "${reqData.cg_name}" already added.`,
              400
            )
          )
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Concrete Grade for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094607165037"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    
    else if (reqData.event_type === "edit_cement_brand") {
      if (
        !isNullOrUndefined(reqData.cement_brand_name) &&
        !isNullOrUndefined(reqData.cementBrandId)
      ) {
        const cementBrandCheck = await CementBrandModel.findOne({
          name: reqData.cement_brand_name,
        })

        if (
          !isNullOrUndefined(cementBrandCheck) &&
          cementBrandCheck._id.equals(reqData.cementBrandId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This Cement Brand name "${reqData.cement_brand_name}" already added.`,
              400
            )
          )
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Cement Brand for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094613806499"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    else if (reqData.event_type === "edit_admix_brand") {
      if (
        !isNullOrUndefined(reqData.admix_brand_name) &&
        !isNullOrUndefined(reqData.admixBrandId)
      ) {
        const admixBrandCheck = await AdmixtureBrandModel.findOne({
          name: reqData.admix_brand_name,
        })

        if (
          !isNullOrUndefined(admixBrandCheck) &&
          admixBrandCheck._id.equals(reqData.admixBrandId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `The entered Admix Brand name" ${reqData.admix_brand_name}" already added. `,
              400
            )
          )
        }
      }
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Admixture Brand for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094629044112"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    
    else if (reqData.event_type === "edit_pump_cat") {
      if (
        !isNullOrUndefined(reqData.pump_cat_name) &&
        !isNullOrUndefined(reqData.concretePumpCategoryId)
      ) {
        let data = await ConcretePumpCategoryModel.findOne({
          category_name: reqData.pump_cat_name,
        })

        if (
          !isNullOrUndefined(data) &&
          data._id.equals(reqData.concretePumpCategoryId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This concrete_pump category name "${reqData.pump_cat_name}" already added.`,
              400
            )
          )
        }
      }
    } else if (reqData.event_type === "edit_admix_cat") {
      if (
        !isNullOrUndefined(reqData.admix_cat_name) &&
        !isNullOrUndefined(reqData.admix_brand_id) &&
        !isNullOrUndefined(reqData.admixture_type) &&
        !isNullOrUndefined(reqData.admixCatId)
      ) {
        const catCheck = await AdmixtureCategoryModel.findOne({
          category_name: reqData.admix_cat_name,
          brand_id: reqData.admix_brand_id,
          admixture_type: reqData.admixture_type,
        })

        if (
          !isNullOrUndefined(catCheck) &&
          catCheck._id.equals(reqData.admixCatId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `Selected Admixture Brand, Type and code already exist`,
              400
            )
          )
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Admixture Type for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095229986754"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_cement_grade") {
      if (
        !isNullOrUndefined(reqData.cement_grade_name) &&
        !isNullOrUndefined(reqData.cementGradeId)
      ) {
        const cgCheck = await CementGradeModel.findOne({
          name: reqData.cement_grade_name,
        })
        if (
          !isNullOrUndefined(cgCheck) &&
          cgCheck._id.equals(reqData.cementGradeId) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `This Cement Grade name "${reqData.cement_grade_name}" already added`,
              400
            )
          )
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Cement Grade for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094624526052"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    else if (reqData.event_type === "edit_agg_source") {
      if (
        !isNullOrUndefined(reqData.aggregate_source_name) &&
        !isNullOrUndefined(reqData.agg_source_id)
      ) {
        const Check = await AggregateSourceModel.findOne({
          aggregate_source_name: reqData.aggregate_source_name,
        })
        if (
          !isNullOrUndefined(Check) &&
          Check._id.equals(reqData.agg_source_id) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `Same source name with this region already registered.`,
              400
            )
          )
        }
        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Aggregate Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164095300859760"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }


    } else if (reqData.event_type === "edit_flyAsh_source") {
      if (
        !isNullOrUndefined(reqData.fly_ash_source_name) &&
        !isNullOrUndefined(reqData.flyAsh_source_id)
      ) {
        const Check = await FlyAshSourceModel.findOne({
          fly_ash_source_name: reqData.fly_ash_source_name,
        })
        if (
          !isNullOrUndefined(Check) &&
          Check._id.equals(reqData.flyAsh_source_id) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `Same source name with this region already registered.`,
              400
            )
          )
        }

        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of FlyAsh Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164095452781611"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "edit_sand_source") {
      if (
        !isNullOrUndefined(reqData.sand_source_name) &&
        !isNullOrUndefined(reqData.sand_source_id)
      ) {
        const Check = await SandSourceModel.findOne({
          sand_source_name: reqData.sand_source_name,
        })
        if (
          !isNullOrUndefined(Check) &&
          Check._id.equals(reqData.sand_source_id) === false
        ) {
          return Promise.reject(
            new InvalidInput(
              `Same source name with this region already registered.`,
              400
            )
          )
        }

        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Sand Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164095430275668"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "vendor_verification") {
      if (!isNullOrUndefined(reqData.vendor_id)) {
        let vendor_company_name: any
        let vendorData = await VendorUserModel.findOne({
          _id: new ObjectId(reqData.vendor_id),
        })
        if (!isNullOrUndefined(vendorData)) {
          vendor_company_name = vendorData.company_name
        }
        //let message = `"${authData.code}" is the OTP for approval of the details and verification of the Registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP for approval of the details and verification of the Registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732937301141"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )

        let vendor_verification_html = admin_mail_template.vendor_verification
        vendor_verification_html = vendor_verification_html.replace(
          "{{vendor_company_name}}",
          vendor_company_name
        )
        vendor_verification_html = vendor_verification_html.replace(
          "{{authData.code}}",
          authData.code
        )
        var data = {
          from: "no-reply@conmate.in",
          to: userDoc.email,
          subject: "Conmix - Vendor verification",
          html: vendor_verification_html,
        }
        await this.mailgunService.sendEmail(data)
      }
    } else if (reqData.event_type === "vendor_rejection") {
      if (!isNullOrUndefined(reqData.vendor_id)) {
        let vendor_company_name: any
        let vendorData = await VendorUserModel.findOne({
          _id: new ObjectId(reqData.vendor_id),
        })
        if (!isNullOrUndefined(vendorData)) {
          vendor_company_name = vendorData.company_name
        }
        // let message = `"${authData.code}" is the OTP for rejection of the details of the Registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP for rejection of the details of the Registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732938931695"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )

        let vendor_rejection_html = admin_mail_template.vendor_rejection
        vendor_rejection_html = vendor_rejection_html.replace(
          "{{vendor_company_name}}",
          vendor_company_name
        )
        vendor_rejection_html = vendor_rejection_html.replace(
          "{{authData.code}}",
          authData.code
        )
        var data1 = {
          from: "no-reply@conmate.in",
          to: userDoc.email,
          subject: "Conmix - Vendor rejection",
          html: vendor_rejection_html,
        }
        await this.mailgunService.sendEmail(data1)
      }
    } else if (reqData.event_type === "plant_block") {
      if (!isNullOrUndefined(reqData.address_id)) {
        let address_business_name: any
        let vendor_company_name: any
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(reqData.address_id),
        })
        if (!isNullOrUndefined(addressData)) {
          address_business_name = addressData.business_name
          let vendorData = await VendorUserModel.findOne({
            _id: new ObjectId(addressData.sub_vendor_id),
          })
          if (!isNullOrUndefined(vendorData)) {
            vendor_company_name = vendorData.company_name
          }
        }
        // let message = `"${authData.code}" is the OTP to Block the RMC Plant ${address_business_name} of the RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP to Block the RMC Plant ${address_business_name} of the RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732941445880"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )

        let plant_block_admin_html = admin_mail_template.plant_block
        plant_block_admin_html = plant_block_admin_html.replace(
          "{{vendor_company_name}}",
          vendor_company_name
        )
        plant_block_admin_html = plant_block_admin_html.replace(
          "{{authData.code}}",
          authData.code
        )
        plant_block_admin_html = plant_block_admin_html.replace(
          "{{address_business_name}}",
          address_business_name
        )
        var a_data1 = {
          from: "no-reply@conmate.in",
          to: userDoc.email,
          subject: "Conmix - Plant block",
          html: plant_block_admin_html,
        }
        await this.mailgunService.sendEmail(a_data1)
      }
    } else if (reqData.event_type === "plant_unblock") {
      if (!isNullOrUndefined(reqData.address_id)) {
        let address_business_name: any
        let vendor_company_name: any
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(reqData.address_id),
        })
        if (!isNullOrUndefined(addressData)) {
          address_business_name = addressData.business_name
          let vendorData = await VendorUserModel.findOne({
            _id: new ObjectId(addressData.sub_vendor_id),
          })
          if (!isNullOrUndefined(vendorData)) {
            vendor_company_name = vendorData.company_name
          }
        }
        // let message = `"${authData.code}" is the OTP to Unblock the RMC Plant ${address_business_name} of the RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP to Unblock the RMC Plant ${address_business_name} of the RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732948437254"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "plant_verification") {
      if (!isNullOrUndefined(reqData.address_id)) {
        let address_business_name: any
        let vendor_company_name: any
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(reqData.address_id),
        })
        if (!isNullOrUndefined(addressData)) {
          address_business_name = addressData.business_name
          let vendorData = await VendorUserModel.findOne({
            _id: new ObjectId(addressData.sub_vendor_id),
          })
          if (!isNullOrUndefined(vendorData)) {
            vendor_company_name = vendorData.company_name
          }
        }
        // let message = `"${authData.code}" is the OTP for approval of the details and verification of the RMC Plant ${address_business_name} of the registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP for approval of the details and verification of the RMC Plant ${address_business_name} of the registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732958067423"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "plant_rejection") {
      if (!isNullOrUndefined(reqData.address_id)) {
        let address_business_name: any
        let vendor_company_name: any
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(reqData.address_id),
        })
        if (!isNullOrUndefined(addressData)) {
          address_business_name = addressData.business_name
          let vendorData = await VendorUserModel.findOne({
            _id: new ObjectId(addressData.sub_vendor_id),
          })
          if (!isNullOrUndefined(vendorData)) {
            vendor_company_name = vendorData.company_name
          }
        }
        // let message = `"${authData.code}" is the OTP for rejection of the details of the RMC Plant ${address_business_name} of the registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
        let message = `"${authData.code}" is the OTP for rejection of the details of the RMC Plant ${address_business_name} of the registered RMC Supplier ${vendor_company_name}. It is valid upto 60 seconds. Kindly do not share the OTP with others. - conmix`
        let templateId = "1707163732961618507"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "account_unblock") {
      if (!isNullOrUndefined(reqData.vendor_id)) {
        let vendor_company_name: any
        let vendorData = await VendorUserModel.findOne({
          _id: new ObjectId(reqData.vendor_id),
        })
        if (!isNullOrUndefined(vendorData)) {
          vendor_company_name = vendorData.company_name
        }
        // let message = `"Hi Admin, "${authData.code}" is an OTP to Unblock the Supplier Account for RMC Supplier ${vendor_company_name}. Kindly do not share this OTP with others.`
        let message = `Hi Admin, "${authData.code}" is an OTP to Unblock the Supplier Account for RMC Supplier ${vendor_company_name}. Kindly do not share this OTP with others. - conmix`
        let templateId = "1707163878193881117"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
      }
    } else if (reqData.event_type === "edit_admin") {  
        let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
        let templateId = "1707164094534944711"
        await this.testService.sendSMS(
          userDoc.mobile_number,
          message,
          templateId
        )
    } else if (reqData.event_type === "delete_admin") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the details of Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094541591563"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "active_admin") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to activate Admin User Type for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094557739850"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "Inactive_admin") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to de-activate Admin User Type for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094567662389"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_country") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Country for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094573291704"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_state") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of State for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094579189983"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "delete_state") {  

      let state_name: any
      if(!isNullOrUndefined(reqData.state_id)){
        let stateData = await StateModel.findOne({
          _id : new ObjectId(reqData.state_id)
        })

        if(!isNullOrUndefined(stateData)){
          state_name =  stateData.state_name
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the entry of ${state_name} State for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094589351716"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_city") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of City for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094594906938"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "delete_city") { 
      let city_name: any
      if(!isNullOrUndefined(reqData.city_id)){
        let cityData = await CityModel.findOne({
          _id : new ObjectId(reqData.city_id)
        })

        if(!isNullOrUndefined(cityData)){
          city_name =  cityData.city_name
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the entry of ${city_name} City for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164094601292611"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    // else if (reqData.event_type === "edit_concrete_grade") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Concrete Grade for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164094607165037"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_cement_brand") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Cement Brand for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164094613806499"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_cement_grade") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Cement Grade for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164094624526052"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_Admix_brand") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Admixture Brand for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164094629044112"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // }
    //  else if (reqData.event_type === "edit_Agg_sand_category") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Aggregate and Sand Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095283648841"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_Agg_sand_sub_category") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Aggregate and Sand Sub Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095294533467"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_Agg_Source") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Aggregate Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095300859760"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    else if (reqData.event_type === "delete_Agg_Source") {  
      let agg_source_name: any
      if(!isNullOrUndefined(reqData.agg_source_id)){
        let aggData = await AggregateSourceModel.findOne({
          _id : new ObjectId(reqData.agg_source_id)
        })
        if(!isNullOrUndefined(aggData)){
          agg_source_name = aggData.aggregate_source_name
        }
      }
      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the details of Aggregate Source ${agg_source_name} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095307154059"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    // else if (reqData.event_type === "edit_Sand_Source") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Sand Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095430275668"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    else if (reqData.event_type === "delete_Sand_Source") {  

      let sand_source_name: any
      if(!isNullOrUndefined(reqData.sand_source_id)){
        let sandData = await SandSourceModel.findOne({
          _id : new ObjectId(reqData.sand_source_id)
        })
        if(!isNullOrUndefined(sandData)){
          sand_source_name = sandData.sand_source_name
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the details of Sand Source ${sand_source_name} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "17071640954424906600"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    // else if (reqData.event_type === "edit_FlyAsh_Source") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of FlyAsh Source for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095452781611"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    else if (reqData.event_type === "delete_FlyAsh_Source") {  
      let flyAsh_source_name: any
      if(!isNullOrUndefined(reqData.fly_Ash_source_id)){
        let flyAshData = await FlyAshSourceModel.findOne({
          _id : new ObjectId(reqData.fly_Ash_source_id)
        })
        if(!isNullOrUndefined(flyAshData)){
          flyAsh_source_name = flyAshData.fly_ash_source_name
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the details of FlyAsh Source ${flyAsh_source_name} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095458958581"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    else if (reqData.event_type === "edit_MarginSlab") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Margin Slab for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095467681556"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_gst_info") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of GST info for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095474330123"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } 
    // else if (reqData.event_type === "edit_TM_Category") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of TM Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095482055121"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    // else if (reqData.event_type === "edit_TM_Sub_Category") {  
    //   let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of TM SubCategory for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
    //   let templateId = "1707164095488581812"
    //   await this.testService.sendSMS(
    //     userDoc.mobile_number,
    //     message,
    //     templateId
    //   )
    // } 
    else if (reqData.event_type === "edit_Concrete_pump_Category") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Concrete Pump Category for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095500683028"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_Coupon") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of Coupon for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095511979545"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "delete_Coupon") {  

      let coupon_code: any
      if(!isNullOrUndefined(reqData.coupon_id)){
        let couponData = await CouponModel.findOne({
          _id : new ObjectId(reqData.coupon_id)
        })
        if(!isNullOrUndefined(couponData)){
          coupon_code = couponData.code
        }
      }

      let message = `Hi Super Admin, ${authData.code} is an OTP to Delete the details of Coupon Code ${coupon_code} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095518099603"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "active_Coupon") {
      
      let coupon_code: any
      if(!isNullOrUndefined(reqData.coupon_id)){
        let couponData = await CouponModel.findOne({
          _id : new ObjectId(reqData.coupon_id)
        })
        if(!isNullOrUndefined(couponData)){
          coupon_code = couponData.code
        }
      }
      
      let message = `Hi Super Admin, ${authData.code} is an OTP to Activate Coupon with Code ${coupon_code} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095523617587"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "Inactive_Coupon") {
      
      let coupon_code: any
      if(!isNullOrUndefined(reqData.coupon_id)){
        let couponData = await CouponModel.findOne({
          _id : new ObjectId(reqData.coupon_id)
        })
        if(!isNullOrUndefined(couponData)){
          coupon_code = couponData.code
        }
      }
      let message = `Hi Super Admin, ${authData.code} is an OTP to De-activate Coupon with Code ${coupon_code} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095528447014"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "Resolve_Ticket") { 
      let ticket_id: any
      if(!isNullOrUndefined(reqData.ticket_id)){
        ticket_id = reqData.ticket_id
      }
      let message = `Hi Super Admin, ${authData.code} is an OTP to Resolve Customer Support Ticket with ticket no. ${ticket_id} for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095534209934"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "edit_Review") {  
      let message = `Hi Super Admin, ${authData.code} is an OTP to Update the details of the Review for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095540011922"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "change_password") {  
      let message = `Hi Super Admin, ${authData.code} is the OTP to Change Password for login to Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095544972482"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "update_email") {  
      let message = `Hi Super Admin, ${authData.code} is the OTP to update email id for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095549708234"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    } else if (reqData.event_type === "update_mobile") {  
      let message = `Hi Super Admin, ${authData.code} is the OTP to update mobile no. for Conmix Admin Module. It is valid upto 60 seconds. Kindly do not share this OTP with others.`
      let templateId = "1707164095553812533"
      await this.testService.sendSMS(
        userDoc.mobile_number,
        message,
        templateId
      )
    }
    else {
      let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Registration OTP.
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(userDoc.mobile_number, message, templateId)
    }
    return Promise.resolve(authData)
  }

  async editUser(
    uid: string,
    editBody: IEditAdminProfile
  ): Promise<IAdminUserDoc> {
    let editDoc: { [k: string]: any } = {}
    if (!isNullOrUndefined(editBody.authentication_code)) {
    const result = await AuthModel.findOne({
      code: editBody.authentication_code,
    })

    console.log(result)
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }
  }

    if (!isNullOrUndefined(editBody.new_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.new_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.full_name)) {
      editDoc.full_name = editBody.full_name.trim()
    }
    if (!isNullOrUndefined(editBody.password)) {
      editDoc.password = await bcrypt.hash(editBody.password, SALT_WORK_FACTOR)
    }

    if (!isNullOrUndefined(editBody.is_active)) {
      editDoc.is_active = editBody.is_active
    }

    if (!isNullOrUndefined(editBody.email)) {
      editDoc.email = editBody.email
    }
    // let clientDoc = await BuyerUserModel.findOne({
    //   email : editBody.email
    // })

    // if(!isNullOrUndefined(clientDoc)){
    //   return Promise.reject(
    //     new InvalidInput(
    //       `Same email already exists with Client.`,
    //       400
    //     )
    //   )
    // }

    // let vendorDoc = await VendorUserModel.findOne({
    //   email : editBody.email
    // })

    // if(!isNullOrUndefined(vendorDoc)){
    //   return Promise.reject(
    //     new InvalidInput(
    //       `Same email already exists with Vendor.`,
    //       400
    //     )
    //   )
    // }

    //}

    if (!isNullOrUndefined(editBody.mobile_number)) {
      editDoc.mobile_number = editBody.mobile_number

      // let clientDoc = await BuyerUserModel.findOne({
      //   mobile_number : editBody.mobile_number
      // })

      // if(!isNullOrUndefined(clientDoc)){
      //   return Promise.reject(
      //     new InvalidInput(
      //       `Same mobile number already exists with Client.`,
      //       400
      //     )
      //   )
      // }

      // let vendorDoc = await VendorUserModel.findOne({
      //   mobile_number : editBody.mobile_number
      // })

      // if(!isNullOrUndefined(vendorDoc)){
      //   return Promise.reject(
      //     new InvalidInput(
      //       `Same mobile number already exists with Vendor.`,
      //       400
      //     )
      //   )
      // }
    }

    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const userDoc = await AdminUserModel.findOneAndUpdate(
      { _id: uid },
      editDoc,
      {
        new: true,
      }
    )
    if (userDoc === null) {
      const err = new UnexpectedInput(`Update failed as no record exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_email_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_email_code,
    })
    await AuthModel.deleteOne({
      uid,
      code: editBody.authentication_code,
    })
    // await AuthModel.deleteOne({
    //   uid,
    //   code: editBody.authentication_code,
    // })
    // await AuthModel.deleteOne({
    //   uid,
    //   code: editBody.authentication_code,
    // })

    await this.commonService.addActivityLogs(
      uid,
      "admin",
      AccessTypes.UPDATE_ADMIN_PROFILE,
      `Admin Updated profile with id ${uid}.`,
      userDoc,
      editDoc
    )

    return Promise.resolve(userDoc)
  }

  async getAdminUserListing(searchParam: IGetAdminUserListing) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(searchParam.search) && searchParam.search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(searchParam.search)
      query.$or = [
        {
          mobile_number: searchParam.search,
        },
        {
          email: searchParam.search,
        },
        {
          full_name: { $regex, $options },
        },
      ]
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
    }

    if (
      !isNullOrUndefined(searchParam.admin_type) &&
      searchParam.admin_type !== ""
    ) {
      query.admin_type = searchParam.admin_type
    }

    if (!isNullOrUndefined(searchParam.is_deleted)) {
      query.is_deleted = searchParam.is_deleted
    }
    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    return AdminUserModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getBuyerUserlist(
    search?: string,
    before?: string,
    after?: string,
    user_id?: string,
    company_name?: string,
    account_type?: string
  ): Promise<IBuyerUserDoc[]> {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(search)
      query.$or = [
        {
          mobile_number: search,
        },
        {
          email: search,
        },
        {
          full_name: { $regex, $options },
        },
      ]
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(user_id) && user_id !== "") {
      query.user_id = user_id
    }
    if (!isNullOrUndefined(company_name) && company_name !== "") {
      query.$text = { $search: company_name }
    }
    // if (!isNullOrUndefined(company_name) && company_name !== "") {
    //   query.company_name = company_name
    // }
    if (!isNullOrUndefined(account_type) && account_type !== "") {
      query.account_type = account_type
    }

    console.log("qqqq", query)

    return BuyerUserModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getVendorUserlist(
    search?: string,
    before?: string,
    after?: string,
    company_type?: string,
    pan_number?: string,
    verified_by_admin?: boolean
  ): Promise<IVendorUserDoc[]> {
    let query: { [k: string]: any } = {}
    query.master_vendor_id = { $exists: false }
    if (!isNullOrUndefined(search) && search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(search)
      query.$or = [
        {
          mobile_number: search,
        },
        {
          email: search,
        },
        {
          company_name: { $regex, $options },
        },
      ]
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(company_type) && company_type !== "") {
      query.company_type = company_type
    }
    if (!isNullOrUndefined(pan_number) && pan_number !== "") {
      query.pan_number = pan_number
    }
    if (!isNullOrUndefined(verified_by_admin)) {
      query.verified_by_admin = verified_by_admin
    }

    let vendorData: any = await VendorUserModel.find(query)
      .sort({ created_at: -1 })
      .limit(10)
    console.log("vendorData", vendorData)
    console.log("vendorData.length", vendorData.length)
    for (let i = 0; i < vendorData.length; i++) {
      let q1: { [k: string]: any } = {}
      q1.master_vendor_id = new ObjectId(vendorData[i]._id)

      let doc = await VendorUserModel.countDocuments(q1)

      console.log("doc", doc)
      let plant_cnt = doc
      console.log("plant_cnt", plant_cnt)
      vendorData[i].plant_cnt = plant_cnt
      // let doc = await VendorUserModel.find({
      //   master_vendor_id: new ObjectId(vendorData[i]._id),
      // })
      // if (doc.length > 0) {
      //   for (let j = 0; j < doc.length; j++) {
      //     console.log("doc.length", doc[j])
      //     vendorData[i].length = doc[j]
      //     console.log("vendorData", vendorData)
      //   }
      // }
      // console.log(vendorData)
    }
    console.log("vendorData", vendorData)
    return vendorData
  }

  async getVendorAddressListing(
    vendor_id: any,
    search?: string,
    before?: string,
    after?: string
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(vendor_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
    ]
    // let query: { [k: string]: any } = {
    //   "user.user_id": new ObjectId(user_id),
    //   "user.user_type": "vendor",
    // }

    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    console.log(query)
    return AddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "source",
          localField: "source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "billingAddressDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "billingAddressDetails.state_id",
          foreignField: "_id",
          as: "billingAddressStateDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressStateDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "billingAddressDetails.city_id",
          foreignField: "_id",
          as: "billingAddressCityDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressCityDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          location: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          address_type: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          business_name: 1,
          region_id: 1,
          region_name: "$state.state_name",
          source_id: 1,
          source_name: "$source.source_name",
          created_at: 1,
          billing_address_id: 1,
          is_verified: 1,
          is_blocked: 1,
          is_accepted: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "billingAddressDetails._id": 1,
          "billingAddressDetails.company_name": 1,
          "billingAddressDetails.line1": 1,
          "billingAddressDetails.line2": 1,
          "billingAddressDetails.pincode": 1,
          "billingAddressDetails.gst_number": 1,
          "billingAddressDetails.gst_image_url": 1,
          "billingAddressDetails.created_at": 1,
          "billingAddressStateDetails._id": "$stateDetails._id",
          "billingAddressStateDetails.state_name": "$stateDetails.state_name",
          "billingAddressStateDetails.country_id": "$stateDetails.country_id",
          "billingAddressStateDetails.country_code":
            "$stateDetails.country_code",
          "billingAddressStateDetails.country_name":
            "$stateDetails.country_name",
          "billingAddressStateDetails.created_at": "$stateDetails.created_at",
          "billingAddressCityDetails._id": "$cityDetails._id",
          "billingAddressCityDetails.city_name": "$cityDetails.city_name",
          "billingAddressCityDetails.location": "$cityDetails.location",
          "billingAddressCityDetails.state_id": "$cityDetails.state_id",
          "billingAddressCityDetails.created_at": "$cityDetails.created_at",
          "billingAddressCityDetails.country_code": "$cityDetails.country_code",
          "billingAddressCityDetails.country_id": "$cityDetails.country_id",
          "billingAddressCityDetails.country_name": "$cityDetails.country_name",
          "billingAddressCityDetails.state_name": "$cityDetails.state_name",
          "billingAddressCityDetails.popularity": "$cityDetails.popularity",
        },
      },
    ])
    // return AddressModel.find(query).limit(40)
  }

  async updateVendorAddressStatus(
    admin_id: string,
    address_id: string,
    vendor_id: string,
    editBody: IEditVendorplantAddress
  ) {

    const res = await AuthModel.findOne({
      user_id: new ObjectId(admin_id),
      code: editBody.authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(
        new InvalidInput(`The code entered by you is wrong.`, 400)
      )
    }

    let vendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    if (isNullOrUndefined(vendorUserData)) {
      return Promise.reject(new InvalidInput(`No vendor data found.`, 400))
    }

    let addressData = await AddressModel.findOne({
      _id: new ObjectId(address_id),
    })
    if (isNullOrUndefined(addressData)) {
      return Promise.reject(new InvalidInput(`No address data found.`, 400))
    }

    let editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(editBody.is_verified)) {
      editDoc.is_verified = editBody.is_verified
      if (editBody.is_verified === true) {
        let message = `Hi ${vendorUserData.company_name}, your RMC Plant ${addressData.business_name} details have been successfully verified by Conmix Admin. You may now add Design Mix for the RMC Plant.`
        let templateId = "1707163731392371017"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        await VendorUserModel.findOneAndUpdate(
          {
            _id: new ObjectId(vendor_id),
          },
          {
            $set: { verified_by_admin: true },
          }
        ).exec()
      } else {
        let message = `Hi ${vendorUserData.company_name}, your RMC Plant ${addressData.business_name} details have been rejected by Conmix Admin. Kindly upload and submit the correct details of your RMC Plant again.`
        let templateId = "1707163731400306059"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )
      }
    }

    if (!isNullOrUndefined(editBody.is_accepted)) {
      if (editBody.is_accepted === false) {
        // If admin reject plant then remove from db and send email with "Reject_reason"
        editDoc.reject_reason = editBody.reject_reason
        await AddressModel.findOneAndRemove({
          _id: new ObjectId(address_id),
        })

        await VendorUserModel.findOneAndRemove({
          _id: new ObjectId(vendor_id),
        })

        return Promise.resolve()
      } else {
        editDoc.is_accepted = editBody.is_accepted
      }
    }

    if (!isNullOrUndefined(editBody.is_accepted)) {
      editDoc.is_accepted = editBody.is_accepted
    }

    if (!isNullOrUndefined(editBody.is_blocked)) {
      let mastervendorUserData: any
      if (!isNullOrUndefined(vendorUserData.master_vendor_id)) {
        mastervendorUserData = await VendorUserModel.findOne({
          _id: new ObjectId(vendorUserData.master_vendor_id),
        })
        if (isNullOrUndefined(mastervendorUserData)) {
          return Promise.reject(
            new InvalidInput(`No master vendor data found.`, 400)
          )
        }
      }

      editDoc.is_blocked = editBody.is_blocked
      if (editBody.is_blocked === true) {
        await VendorUserModel.findOneAndUpdate(
          {
            _id: new ObjectId(vendor_id),
          },
          {
            $set: { is_blocked: true },
          }
        ).exec()

        let message = `Hi ${vendorUserData.company_name}, your RMC Plant ${addressData.business_name} has been blocked by Conmix Admin as per our Vendor/Supplier Policy. You may contact Conmix Customer Care Team.`
        let templateId = "1707163731379300023"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        if (!isNullOrUndefined(mastervendorUserData)) {
          let message = `Hi ${mastervendorUserData.company_name}, this is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.`
          let templateId = "1707163731316863833"
          await this.testService.sendSMS(
            mastervendorUserData.mobile_number,
            message,
            templateId
          )
        }

        let plant_block_html = vendor_mail_template.plant_block
        plant_block_html = plant_block_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        plant_block_html = plant_block_html.replace(
          "{{addressData.business_name}}",
          addressData.business_name
        )
        var data = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - RMC Plant Block",
          html: plant_block_html,
        }
        await this.mailgunService.sendEmail(data)

        if (!isNullOrUndefined(mastervendorUserData)) {
          let vendor_block_html = vendor_mail_template.vendor_block
          vendor_block_html = vendor_block_html.replace(
            "{{vendorUserData.company_name}}",
            mastervendorUserData.company_name
          )
          var m_data11 = {
            from: "no-reply@conmate.in",
            to: mastervendorUserData.email,
            subject: "Conmix - Account Block",
            html: vendor_block_html,
          }
          await this.mailgunService.sendEmail(m_data11)
        }
      } else {
        await VendorUserModel.findOneAndUpdate(
          {
            _id: new ObjectId(vendor_id),
          },
          {
            $set: { is_blocked: false },
          }
        ).exec()
        let message = `Hi ${vendorUserData.company_name}, your RMC Plant ${addressData.business_name} has been Unblocked by Conmix Admin. You may continue business with Conmix as per Vendor/Supplier Policy.`
        let templateId = "1707163731385527779"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        if (!isNullOrUndefined(mastervendorUserData)) {
          let m_message = `Hi ${mastervendorUserData.company_name} we are glad to inform you that your account at Conmix has been Unblocked. You may login Conmix to accept RMC orders for more business !! - conmix`
          let m_templateId = "1707163843699480431"
          await this.testService.sendSMS(
            mastervendorUserData.mobile_number,
            m_message,
            m_templateId
          )
        }
      }
    }

    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const addressDoc = await AddressModel.findOne({
      _id: new ObjectId(address_id),
    })
    if (!isNullOrUndefined(addressDoc)) {
      await AddressModel.findOneAndUpdate(
        {
          _id: new ObjectId(address_id),
        },
        editDoc,
        {
          new: true,
        }
      )

      await AuthModel.deleteOne({
        user_id: new ObjectId(admin_id),
        code: editBody.authentication_code,
      })
    } else {
      return Promise.reject(
        new InvalidInput(`Sorry, we could not find any address details.`, 400)
      )
    }
  }

  async deleteProfile(
    user_id: string,
    deleted_by: string,
    authentication_code: string
  ) {
    const res = await AuthModel.findOne({
      user_id: new ObjectId(deleted_by),
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(
        new InvalidInput(`You are not authorized to delete this user.`, 400)
      )
    }

    // check login user is "superAdmin" then delete the requested user entry from main table and move it into "deleted_admin" collection.
    let adminUserDoc = await AdminUserModel.findById(deleted_by)
    if (
      !isNullOrUndefined(adminUserDoc) &&
      adminUserDoc.admin_type === "superAdmin"
    ) {
      let adminDoc = await AdminUserModel.findById(user_id)

      if (isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new InvalidInput(
            `Sorry we could not find Admin user for the entered Id`,
            400
          )
        )
      }

      let deletedAdminDoc = {
        full_name: adminDoc.full_name,
        mobile_number: adminDoc.mobile_number,
        email: adminDoc.email,
        admin_type: adminDoc.admin_type,
        identity_image_url: adminDoc.identity_image_url,
        password: adminDoc.password,
        created_by: adminDoc.created_by,
        created_at: adminDoc.created_at,
        deleted_by,
      }

      await new DeletedAdminUserModel(deletedAdminDoc).save()
      if (isNullOrUndefined(deletedAdminDoc)) {
        return Promise.reject(
          new InvalidInput(`Delete failed as no record exists`, 400)
        )
      }

      // Delete Authentication code.
      await AuthModel.deleteOne({
        user_id: new ObjectId(deleted_by),
        code: authentication_code,
      })

      // Delete user from main admin db.
      await AdminUserModel.deleteOne({
        mobile_number: adminDoc.mobile_number,
        email: adminDoc.email,
        admin_type: adminDoc.admin_type,
      })

      await this.commonService.addActivityLogs(
        deleted_by,
        "admin",
        AccessTypes.DELETE_ADMIN_PROFILE,
        `Admin profile deleted with id ${user_id}.`
      )
      return Promise.resolve()
    } else {
      return Promise.reject(
        new InvalidInput(`You are not authorized to delete admin profile.`, 400)
      )
    }
  }

  async getUserDetailsById(user_id: string) {
    const data = await AdminUserModel.findById(user_id).select({ password: 0 })
    if (isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we did not find any user with the this details `,
          404
        )
      )
    }
    return data
  }

  async editVendor(
    uid: string,
    vendor_id: string,
    editBody: IEditVendorProfile
  ) {
    console.log("vendor_id", vendor_id)
    const res = await AuthModel.findOne({
      user_id: new ObjectId(uid),
      code: editBody.authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(
        new InvalidInput(`The code entered by you is wrong.`, 400)
      )
    }

    let vendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    if (isNullOrUndefined(vendorUserData)) {
      return Promise.reject(new InvalidInput(`No vendor data found.`, 400))
    }

    const adminUserDoc = await AdminUserModel.findOne({
      admin_type: "superAdmin",
    })

    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    }

    let editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(editBody.is_accepted)) {
      if (editBody.is_accepted === false) {
        let message = `Hi ${vendorUserData.company_name}, Sorry. Your Partner Account has been deactivated at Conmix. Kindly check your registered email and contact Conmix Customer Care for more information.`
        let templateId = "1707163731305872378"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        let vendor_acc_deactivate_html = vendor_mail_template.account_deactivate
        vendor_acc_deactivate_html = vendor_acc_deactivate_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var data = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - Account Deactive",
          html: vendor_acc_deactivate_html,
        }
        await this.mailgunService.sendEmail(data)

        let vendor_acc_deactivate_admin_html =
          admin_mail_template.account_deactivate
        vendor_acc_deactivate_admin_html = vendor_acc_deactivate_admin_html.replace(
          "{{adminUserDoc.full_name}}",
          adminUserDoc.full_name
        )
        vendor_acc_deactivate_admin_html = vendor_acc_deactivate_admin_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var data1 = {
          from: "no-reply@conmate.in",
          to: adminUserDoc.email,
          subject: "Conmix - Account Deactive",
          html: vendor_acc_deactivate_admin_html,
        }
        await this.mailgunService.sendEmail(data1)

        // If vendor reject then remove from db and send email with "Reject_reason"
        editDoc.reject_reason = editBody.reject_reason
        await VendorUserModel.findOneAndRemove({
          _id: new ObjectId(vendor_id),
        })
        return Promise.resolve()
      } else {
        let message = `Hi ${vendorUserData.company_name}, we are glad to inform you that your Partner Account has been successfully re-activated at Conmix. Kindly check your registered email or contact Conmix Customer Care for more information.`
        let templateId = "1707163731311253602"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        let vendor_acc_activate_html = vendor_mail_template.account_activated
        vendor_acc_activate_html = vendor_acc_activate_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var v_data = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - Account Active",
          html: vendor_acc_activate_html,
        }
        await this.mailgunService.sendEmail(v_data)

        let vendor_acc_activate_admin_html =
          admin_mail_template.account_activated
        vendor_acc_activate_admin_html = vendor_acc_activate_admin_html.replace(
          "{{adminUserDoc.full_name}}",
          adminUserDoc.full_name
        )
        vendor_acc_activate_admin_html = vendor_acc_activate_admin_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var v_data1 = {
          from: "no-reply@conmate.in",
          to: adminUserDoc.email,
          subject: "Conmix - Account Active",
          html: vendor_acc_activate_admin_html,
        }
        await this.mailgunService.sendEmail(v_data1)

        editDoc.is_accepted = editBody.is_accepted
      }
    }

    if (!isNullOrUndefined(editBody.verified_by_admin)) {
      editDoc.verified_by_admin = editBody.verified_by_admin
      if (editBody.verified_by_admin === false) {
        let message = `Hi ${vendorUserData.company_name}, your account registraiton details have been rejected by Conmix Admin. You need to register again and upload correct details. Contact Customer Care for more information.`
        let templateId = "1707163731373328381"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        let vendor_acc_rejected_html = vendor_mail_template.account_rejected
        vendor_acc_rejected_html = vendor_acc_rejected_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var v11_data = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - Account Rejected",
          html: vendor_acc_rejected_html,
        }
        await this.mailgunService.sendEmail(v11_data)

        // let vendor_acc_rejected_admin_html = admin_mail_template.account_rejected
        // vendor_acc_rejected_admin_html = vendor_acc_rejected_admin_html.replace("{{adminUserDoc.full_name}}", adminUserDoc.full_name)
        // vendor_acc_rejected_admin_html = vendor_acc_rejected_admin_html.replace("{{vendorUserData.company_name}}", vendorUserData.company_name)
        // var a11_data = {
        //   from: "no-reply@conmate.in",
        //   to: adminUserDoc.email,
        //   subject: "Conmix - Account Rejected",
        //   html: vendor_acc_rejected_admin_html,
        // }
        // await this.mailgunService.sendEmail(a11_data)
      } else {
        // let v_message = `Hi ${vendorUserData.full_name}, your account registraiton detail has been successfully verified by Conmix Admin. You may now add your RMC Plant details in your registered account`
        let v_message = `Hi ${vendorUserData.company_name}, your account registration detail has been successfully verified by Conmix Admin. You may now add your RMC Plant details in your registered account`
        let v_templateId = "1707163731366246771"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          v_message,
          v_templateId
        )

        let vendor_acc_verified_html = vendor_mail_template.account_verified
        vendor_acc_verified_html = vendor_acc_verified_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var v1_data = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - Account Verified",
          html: vendor_acc_verified_html,
        }
        await this.mailgunService.sendEmail(v1_data)

        let vendor_acc_admin_verified_html =
          admin_mail_template.account_verified
        vendor_acc_admin_verified_html = vendor_acc_admin_verified_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        vendor_acc_admin_verified_html = vendor_acc_admin_verified_html.replace(
          "{{adminUserDoc.full_name}}",
          adminUserDoc.full_name
        )
        var a1_data = {
          from: "no-reply@conmate.in",
          to: adminUserDoc.email,
          subject: "Conmix - Account Verified",
          html: vendor_acc_admin_verified_html,
        }
        await this.mailgunService.sendEmail(a1_data)
      }
    }

    if (!isNullOrUndefined(editBody.is_blocked)) {
      if (editBody.is_blocked === false) {
        editDoc.is_blocked = editBody.is_blocked
        let message = `Hi ${vendorUserData.company_name} we are glad to inform you that your account at Conmix has been Unblocked. You may login Conmix to accept RMC orders for more business !! - conmix`
        let templateId = "1707163843699480431"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        let subvendorData = await VendorUserModel.find({
          master_vendor_id: new ObjectId(vendor_id),
        })

        if (subvendorData.length > 0) {
          for (let i = 0; i < subvendorData.length; i++) {
            let s_message = `Hi ${subvendorData[i].company_name} we are glad to inform you that your account at Conmix has been Unblocked. You may login Conmix to accept RMC orders for more business !! - conmix`
            let s_templateId = "1707163843699480431"
            await this.testService.sendSMS(
              subvendorData[i].mobile_number,
              s_message,
              s_templateId
            )
            await VendorUserModel.findOneAndUpdate(
              { _id: new ObjectId(subvendorData[i]._id) },
              {
                $set: { is_blocked: false },
              }
            )

            await AddressModel.findOneAndUpdate(
              { sub_vendor_id: new ObjectId(subvendorData[i]._id) },
              {
                $set: { is_blocked: false },
              }
            )
          }
        }

        // editDoc.is_blocked = editBody.is_blocked
        // let message = `Hi ${vendorUserData.company_name} we are glad to inform you that your account at Conmix has been Unblocked. You may login Conmix to accept RMC orders for more business !! - conmix`
        // let templateId = "1707163843699480431"
        // await this.testService.sendSMS(
        //   vendorUserData.mobile_number,
        //   message,
        //   templateId
        // )
      } else {
        editDoc.is_blocked = editBody.is_blocked
        let message = `Hi ${vendorUserData.company_name}, this is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.`
        let templateId = "1707163731316863833"
        await this.testService.sendSMS(
          vendorUserData.mobile_number,
          message,
          templateId
        )

        let vendor_block_html = vendor_mail_template.vendor_block
        vendor_block_html = vendor_block_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var v_data11 = {
          from: "no-reply@conmate.in",
          to: vendorUserData.email,
          subject: "Conmix - Account Block",
          html: vendor_block_html,
        }
        await this.mailgunService.sendEmail(v_data11)

        let subvendorData = await VendorUserModel.find({
          master_vendor_id: new ObjectId(vendor_id),
        })

        if (subvendorData.length > 0) {
          for (let i = 0; i < subvendorData.length; i++) {
            let s_message = `Hi ${subvendorData[i].company_name}, this is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.`
            let s_templateId = "1707163731316863833"
            await this.testService.sendSMS(
              subvendorData[i].mobile_number,
              s_message,
              s_templateId
            )
            await VendorUserModel.findOneAndUpdate(
              { _id: new ObjectId(subvendorData[i]._id) },
              {
                $set: { is_blocked: true },
              }
            )

            await AddressModel.findOneAndUpdate(
              { sub_vendor_id: new ObjectId(subvendorData[i]._id) },
              {
                $set: { is_blocked: true },
              }
            )

            let vendor_block_html = vendor_mail_template.vendor_block
            vendor_block_html = vendor_block_html.replace(
              "{{vendorUserData.company_name}}",
              subvendorData[i].company_name
            )
            var v_data11 = {
              from: "no-reply@conmate.in",
              to: subvendorData[i].email,
              subject: "Conmix - Account Block",
              html: vendor_block_html,
            }
            await this.mailgunService.sendEmail(v_data11)
          }
        }

        // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }
      for(let i = 0 ; i < adminUserDocs.length ; i ++){
        let vendor_block_admin_html = admin_mail_template.vendor_block
        vendor_block_admin_html = vendor_block_admin_html.replace(
          "{{adminUserDoc.full_name}}",
          adminUserDocs[i].full_name
        )
        vendor_block_admin_html = vendor_block_admin_html.replace(
          "{{vendorUserData.company_name}}",
          vendorUserData.company_name
        )
        var admin_d = {
          from: "no-reply@conmate.in",
          to: adminUserDocs[i].email,
          subject: "Conmix - Account Block",
          html: vendor_block_admin_html,
        }
        await this.mailgunService.sendEmail(admin_d)
      }
       
      }
    }

    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const userDoc = await VendorUserModel.findOneAndUpdate(
      { _id: new ObjectId(vendor_id) },
      editDoc,
      {
        new: true,
      }
    )
    if (userDoc === null) {
      const err = new UnexpectedInput(`Update failed as no record exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.authentication_code,
    })
    await this.commonService.addActivityLogs(
      uid,
      "admin",
      AccessTypes.UPDATE_VENDOR_PROFILE_BY_ADMIN,
      `Vendor profile Updated with id ${vendor_id}.`,
      userDoc,
      editDoc
    )

    return Promise.resolve(userDoc)
  }

  async changePassword(
    uid: string,
    old_password: string,
    password: string,
    authentication_code: string
  ): Promise<any> {
    console.log(uid)

    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const adminUserDoc = await AdminUserModel.findOne({
      admin_type: "superAdmin",
    })

    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    }

    const doc = await AdminUserModel.findById(uid)

    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }

    if (await bcrypt.compare(old_password, doc.password)) {
      await AdminUserModel.updateOne(
        { _id: uid },
        { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
      )

      await AuthModel.deleteOne({
        user_id: new ObjectId(uid),
        code: authentication_code,
      })

      await this.commonService.addActivityLogs(
        uid,
        "admin",
        AccessTypes.CHANGE_PASSWORD_BY_ADMIN,
        `Admin Changed password with id ${uid}.`,
        old_password,
        password
      )
      console.log("hereeee")
      let message = `Hi ${doc.full_name}, your password has been updated successfully. Kindly do not share your password with others. - conmix.`
      // Password Update Successful.
      let templateId = "1707163732935455230"
      await this.testService.sendSMS(doc.mobile_number, message, templateId)

      let password_updated_successfully_html =
        admin_mail_template.password_updated_successfully
      password_updated_successfully_html = password_updated_successfully_html.replace(
        "{{doc.full_name}}",
        adminUserDoc.full_name
      )
      var data = {
        from: "no-reply@conmate.in",
        to: doc.email,
        subject: "Conmix - Password Update Successful",
        html: password_updated_successfully_html,
      }
      await this.mailgunService.sendEmail(data)

      return Promise.resolve()
    }

    return Promise.reject(
      new InvalidInput(
        `The New password and  Confirm password do not match `,
        400
      )
    )
  }

  async forgotPassword(uid: string, password: string) {
    console.log(uid)

    const doc = await AdminUserModel.findById(uid)

    if (isNullOrUndefined(doc)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }

    await AdminUserModel.updateOne(
      { _id: uid },
      { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
    )

    let message = `Hi ${doc.full_name}, your password has been updated successfully. Kindly do not share your password with others. - conmix.`
    // Password Update Successful.
    let templateId = "1707163732935455230"
    await this.testService.sendSMS(doc.mobile_number, message, templateId)

    let password_updated_successfully_html =
      admin_mail_template.password_updated_successfully
    password_updated_successfully_html = password_updated_successfully_html.replace(
      "{{doc.full_name}}",
      doc.full_name
    )
    var data = {
      from: "no-reply@conmate.in",
      to: doc.email,
      subject: "Conmix - Password Update Successful",
      html: password_updated_successfully_html,
    }
    await this.mailgunService.sendEmail(data)

    await this.commonService.addActivityLogs(
      uid,
      "admin",
      AccessTypes.FORGOT_PASSWORD_BY_ADMIN,
      `Admin Forgot password with id ${uid}.`,
      doc.password,
      password
    )
  }

  async requestOTPForUpdateProfile(user_id: string, reqData: any) {
    const userDoc = await AdminUserModel.findById(user_id)
    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }
    let oldmobileauthData: any
    if (!isNullOrUndefined(reqData.old_mobile_no)) {
      const code = generateRandomNumber()
      oldmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${oldmobileauthData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Send OTP On Old mobile number
      let templateId = "1707163532596026621"
      await this.testService.sendSMS(reqData.old_mobile_no, message, templateId)
    }

    let newmobileauthData: any
    if (!isNullOrUndefined(reqData.new_mobile_no)) {
      const numberCheck = await AdminUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })
      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(userDoc._id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other admin.`,
            400
          )
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new InvalidInput(
            `Same mobile number already exists with Client.`,
            400
          )
        )
      }

      let vendorDoc = await VendorUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (!isNullOrUndefined(vendorDoc)) {
        return Promise.reject(
          new InvalidInput(
            `Same mobile number already exists with Vendor.`,
            400
          )
        )
      }

      const code = generateRandomNumber()
      newmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${newmobileauthData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Send OTP On Old mobile number
      let templateId = "1707163532596026621"
      await this.testService.sendSMS(reqData.new_mobile_no, message, templateId)
    }

    let oldemailauthData: any
    if (!isNullOrUndefined(reqData.old_email)) {
      const code = generateRandomNumber()
      oldemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let one_time_pwd_html = admin_mail_template.one_time_otp
      one_time_pwd_html = one_time_pwd_html.replace(
        "{{code}}",
        oldemailauthData.code
      )
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.old_email,
        subject: "Conmix - One Time Password",
        html: one_time_pwd_html,
      }
      await this.mailgunService.sendEmail(data)
    }

    let newemailauthData: any
    if (!isNullOrUndefined(reqData.new_email)) {
      const userDoc = await AdminUserModel.findById(user_id)
      if (isNullOrUndefined(userDoc)) {
        return Promise.reject(
          new InvalidInput(`No document could be found`, 400)
        )
      }

      const numberCheck = await AdminUserModel.findOne({
        email: reqData.new_email,
      })
      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(userDoc._id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other admin.`,
            400
          )
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        email: reqData.new_email,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new InvalidInput(`Same email already exists with Client.`, 400)
        )
      }

      let vendorDoc = await VendorUserModel.findOne({
        email: reqData.new_email,
      })

      if (!isNullOrUndefined(vendorDoc)) {
        return Promise.reject(
          new InvalidInput(`Same email already exists with Vendor.`, 400)
        )
      }

      const code = generateRandomNumber()
      newemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let one_time_pwd_html = admin_mail_template.one_time_otp
      one_time_pwd_html = one_time_pwd_html.replace(
        "{{code}}",
        newemailauthData.code
      )
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.new_email,
        subject: "Conmix - One Time Password",
        html: one_time_pwd_html,
      }
      await this.mailgunService.sendEmail(data)

      // let message = `"${newmobileauthData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // // Send OTP On Old mobile number
      // let templateId = "1707163532596026621"
      // await this.testService.sendSMS(reqData.new_mobile_no, message, templateId)
    }

    return Promise.resolve({
      oldmobileauthData,
      newmobileauthData,
      oldemailauthData,
      newemailauthData,
    })
  }
}
