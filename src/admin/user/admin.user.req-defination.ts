import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ICreateAdminUserDetails {
  full_name: string
  email: string
  image?: string
}

export interface ICreateAdminProfile {
  full_name: string
  mobile_number: string
  email: string
  admin_type: string // sales/support/account_manager
  password: string
}

export interface ICreateAdminProfileRequest extends IAuthenticatedRequest {
  body: ICreateAdminProfile
}

export interface IEditAdminProfile {
  full_name: string
  password: string
  authentication_code: string
  is_active: boolean
  email?: string
  mobile_number?: string
  new_mobile_code: string
  new_email_code: string
  old_mobile_code: string
  old_email_code: string
}
export interface IEditAdminProfileRequest extends IAuthenticatedRequest {
  body: IEditAdminProfile
}

export interface IGetAdminUserListing {
  is_deleted?: boolean
  is_active?: boolean
  search?: string
  admin_type?: string
  before?: string
  after?: string
}

export interface IGetAdminUserListingRequest extends IAuthenticatedRequest {
  body: IGetAdminUserListing
}

export interface IDeleteAdminProfile {
  authentication_code: string
}
export interface IDeleteAdminProfileRequest extends IAuthenticatedRequest {
  body: IDeleteAdminProfile
}

export interface IGetUserDetailsById {
  userId: string
  [k: string]: any
}

export interface IGetUserDetailsByIdRequest extends IAuthenticatedRequest {
  params: IGetUserDetailsById
}

export interface IEditVendorProfile {
  verified_by_admin: boolean
  is_accepted: boolean
  is_blocked: boolean
  reject_reason: string
  authentication_code: string
}
export interface IEditVendorProfileRequest extends IAuthenticatedRequest {
  body: IEditVendorProfile
}

export interface IChangePasswordRequest extends IAuthenticatedRequest {
  body: {
    old_password: string
    password: string
    authentication_code: string
  }
}

export interface IForgotPasswordRequest extends IAuthenticatedRequest {
  body: {
    password: string
  }
}

export interface IEditVendorplantAddress {
  is_verified: boolean
  is_accepted: boolean 
  is_blocked: boolean
  vendor_id: string
  reject_reason: string
  authentication_code: string
}
export interface IEditVendorPlantAddressRequest extends IAuthenticatedRequest {
  body: IEditVendorplantAddress
}