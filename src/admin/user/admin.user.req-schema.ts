import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
const FullNameRegex = /^[a-z. ]*$/i

export const createAdminUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .required()
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      }),
    email: Joi.string().email().required(),
    mobile_number: Joi.string().required(),
    admin_type: Joi.string()
      .valid([
        "admin_manager",
        "admin_marketing",
        "admin_sales",
        "admin_accounts",
        "admin_customer_care",
        "admin_grievances",
      ])
      .required(),
    password: Joi.string().min(6).max(20).required(),
  }),
}

export const editAdminUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      }),
    password: Joi.string().min(6).max(20),
    //authentication_code: Joi.string().required(),
    is_active: Joi.boolean(),
    old_mobile_code: Joi.string(),
    new_mobile_code: Joi.string(),
    old_email_code: Joi.string(),
    new_email_code: Joi.string(),
    email: Joi.string(),
    mobile_number: Joi.string(),
  }),
}
export const editAnotherAdminSchema: IRequestSchema = {
  params: Joi.object().keys({
    userId: Joi.string().regex(objectIdRegex),
  }),
  body: editAdminUserSchema.body,
}

export const getAdminUserListingSchema: IRequestSchema = {
  query: Joi.object().keys({
    is_deleted: Joi.boolean(),
    is_active: Joi.boolean(),
    search: Joi.string(),
    admin_type: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export const deleteAnotherAdminSchema: IRequestSchema = {
  params: Joi.object().keys({
    userId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export const getUserDeatilsByIdSchema: IRequestSchema = {
  params: Joi.object().keys({
    userId: Joi.string().regex(objectIdRegex),
  }),
}

export const editVendorUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    verified_by_admin: Joi.boolean(),
    is_accepted: Joi.boolean(),
    is_blocked: Joi.boolean(),
    reject_reason: Joi.string(),
    authentication_code: Joi.string().required(),
  }),
}

export const changePasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    old_password: Joi.string().min(6).max(20).required(),
    password: Joi.string().min(6).max(20).required(),
    authentication_code: Joi.string().required(),
  }),
}



export const updateVendorAddressStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    addressId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    vendor_id: Joi.string().regex(objectIdRegex).required(),
    is_verified: Joi.boolean(),
    is_accepted: Joi.boolean(),
    is_blocked: Joi.boolean(),
    reject_reason: Joi.string(),
  }),
}

export const forgotPasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    password: Joi.string().min(6).max(20).required()
  }),
}

export const requestOTPForUpdateProfileSchema: IRequestSchema = {
  body: Joi.object().keys({
    new_mobile_no: Joi.string(),
    new_email: Joi.string(),
    old_mobile_no: Joi.string(),
    old_email: Joi.string(),
  }),
}
export const requestOTPSchema: IRequestSchema = {
  query: Joi.object().keys({
    event_type: Joi.string(),
    vendor_id: Joi.string(),
    address_id: Joi.string(),
    old_password: Joi.string().min(6).max(20),
    password: Joi.string().min(6).max(20),
    category_name: Joi.string().min(2).max(64),
    TMCategoryId: Joi.string().regex(objectIdRegex),
    sub_category_name: Joi.string().max(64).min(2),
    TMSubCategoryId: Joi.string().regex(objectIdRegex),
    agg_cat_name: Joi.string().min(3).max(150),
    categoryId: Joi.string().regex(objectIdRegex),
    agg_sub_cat_name: Joi.string().min(3).max(150),
    subCategoryId: Joi.string().regex(objectIdRegex),
    cg_name: Joi.string().min(3).max(150),
    cgId: Joi.string().regex(objectIdRegex),
    cement_brand_name: Joi.string().min(3).max(150),
    cementBrandId: Joi.string().regex(objectIdRegex),
    admix_brand_name: Joi.string().min(3).max(150),
    admixBrandId: Joi.string().regex(objectIdRegex),
    pump_cat_name: Joi.string().min(3).max(150),
    concretePumpCategoryId: Joi.string().regex(objectIdRegex),
    admix_cat_name: Joi.string().min(3).max(150),
    admix_brand_id: Joi.string().regex(objectIdRegex),
    admixture_type: Joi.string(),
    admixCatId: Joi.string().regex(objectIdRegex),
    cement_grade_name: Joi.string().min(3).max(150),
    cementGradeId: Joi.string().regex(objectIdRegex),
    aggregate_source_name: Joi.string(),
    agg_source_id: Joi.string().regex(objectIdRegex),
    fly_ash_source_name: Joi.string(),
    flyAsh_source_id: Joi.string().regex(objectIdRegex),
    sand_source_name: Joi.string(),
    sand_source_id: Joi.string().regex(objectIdRegex),
  })
}