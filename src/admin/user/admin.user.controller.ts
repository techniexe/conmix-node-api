import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import { AdminTypes } from "../admin.types"
import { AdminUserRepository } from "./admin.user.repository"
import {
  ICreateAdminProfileRequest,
  IEditAdminProfileRequest,
  IGetAdminUserListingRequest,
  IDeleteAdminProfileRequest,
  IGetUserDetailsByIdRequest,
  IEditVendorProfileRequest,
  IChangePasswordRequest,
  IForgotPasswordRequest,
  IEditVendorPlantAddressRequest,
} from "./admin.user.req-defination"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  createAdminUserSchema,
  editAdminUserSchema,
  editAnotherAdminSchema,
  getAdminUserListingSchema,
  deleteAnotherAdminSchema,
  getUserDeatilsByIdSchema,
  editVendorUserSchema,
  changePasswordSchema,
  updateVendorAddressStatusSchema,
  forgotPasswordSchema,
  requestOTPForUpdateProfileSchema,
  requestOTPSchema,
} from "./admin.user.req-schema"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { InvalidInput } from "../../utilities/customError"

@injectable()
@controller("/users", verifyCustomToken("admin"))
export class AdminUserController {
  constructor(
    @inject(AdminTypes.AdminUserRepository)
    private adminUserRepo: AdminUserRepository
  ) {}

  @httpPost("/requestOTPForUpdateProfile", validate(requestOTPForUpdateProfileSchema))
  async requestOTPForUpdateProfile(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminUserRepo.requestOTPForUpdateProfile(req.user.uid, req.body)
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }
  
  /** If user is login then this service may be used to change his password */
  @httpPatch("/changePassword", validate(changePasswordSchema))
  async changePassword(
    req: IChangePasswordRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.adminUserRepo.changePassword(
      req.user.uid,
      req.body.old_password,
      req.body.password,
      req.body.authentication_code
    )
    res.sendStatus(201)
  }

  /** Forgot Password. */
  @httpPatch("/forgotPassword", validate(forgotPasswordSchema))
  async forgotPassword(
    req: IForgotPasswordRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminUserRepo.forgotPassword(req.user.uid, req.body.password)
      res.sendStatus(201)
    } catch (err) {
      return next(err)
    }
  }

  @httpPost("/", validate(createAdminUserSchema))
  async createProfile(
    req: ICreateAdminProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminUserRepo.createUser(req.user.uid, req.body)
      res.sendStatus(201)
    } 
    catch (err) {
      //let msg = `Unable to update user details.`
      if (err.code === 11000) {
        let msg = `User with this email or mobile is already registered.`
        return Promise.reject(new InvalidInput(msg, 400))
      }
      next(err)
    }
  }

  @httpGet("/profile")
  async getUserDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const user = await this.adminUserRepo.getUserProfile(req.user.uid)
      if (user === null) {
        return res.status(404).json({
          error: {
            message: `Sorry we did not find any user with this details`,
          },
        })
      }
      return res.json({ data: user })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/requestOTP", validate(requestOTPSchema))
  async requestOTP(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminUserRepo.requestOTP(req.user.uid, req.query)
      return res.send({ data: data.code })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:userId", validate(editAnotherAdminSchema))
  @httpPatch("/", validate(editAdminUserSchema))
  async editProfile(
    req: IEditAdminProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const uId = req.params.userId || req.user.uid
      await this.adminUserRepo.editUser(uId, req.body)
      res.sendStatus(204)
    } catch (err) {
      //let msg = `Unable to update user details.`
      if (err.code === 11000) {
        let msg = `User with this email or mobile is already registered.`
        return Promise.reject(new InvalidInput(msg, 400))
      }
      next(err)
    }
  }

  @httpGet("/adminlisting", validate(getAdminUserListingSchema))
  async getAdminUserListing(
    req: IGetAdminUserListingRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminUserRepo.getAdminUserListing(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/buyerlisting")
  async getBuyerUser(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after, user_id, company_name, account_type } = req.query
      const data = await this.adminUserRepo.getBuyerUserlist(
        search,
        before,
        after,
        user_id,
        company_name,
        account_type
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/vendorlisting")
  async getVendorUser(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after, company_type, pan_number, verified_by_admin } = req.query
      const data = await this.adminUserRepo.getVendorUserlist(
        search,
        before,
        after,
        company_type,
        pan_number,
        verified_by_admin
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/vendorAddresslisting")
  async getVendorAddressListing(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after, vendor_id } = req.query
      const data = await this.adminUserRepo.getVendorAddressListing(
        vendor_id,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/updateVendorAddress/:addressId",
    validate(updateVendorAddressStatusSchema)
  )
  async updateVendorAddressStatus(
    req: IEditVendorPlantAddressRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminUserRepo.updateVendorAddressStatus(
        req.user.uid,
        req.params.addressId,
        req.body.vendor_id,
        req.body
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpDelete("/:userId", validate(deleteAnotherAdminSchema))
  async deleteProfile(
    req: IDeleteAdminProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminUserRepo.deleteProfile(
        req.params.userId,
        req.user.uid,
        req.body.authentication_code
      )
      res.sendStatus(201)
    } catch (err) {
      return next(err)
    }
  }

  @httpGet("/:userId", validate(getUserDeatilsByIdSchema))
  async getUserDetailsById(
    req: IGetUserDetailsByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminUserRepo.getUserDetailsById(
        req.params.userId
      )
      res.json({ data })
    } catch (err) {
      return next(err)
    }
  }

  @httpPatch("/editVendor/:vendor_id", validate(editVendorUserSchema))
  async editVendor(
    req: IEditVendorProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminUserRepo.editVendor(
        req.user.uid,
        req.params.vendor_id,
        req.body
      )
      res.sendStatus(201)
    } catch (err) {
      return next(err)
    }
  }


}
