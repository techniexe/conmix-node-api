import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetOrderTrack {
  user_id?: string
  user_type?: string
  item_id?: string
  order_id?: string
  logistics_order_id?: string
  before?: string
  after?: string
}

export interface IOrderTrack {
  order_id: string
  item_id: string
  logistics_order_id: string
  text?: string
  location?: {
    type: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
  place_name?: string
  added_by_id?: string
  added_by_type: string
  event_date: Date
}

export interface IAddOrderTrackRequest extends IAuthenticatedRequest {
  body: IOrderTrack
}

export const addOrderTrackSchema: IRequestSchema = {
  body: Joi.object().keys({
    order_id: Joi.string().required(),
    item_id: Joi.string().regex(objectIdRegex).required(),
    logistics_order_id: Joi.string().regex(objectIdRegex).required(),
    text: Joi.string(),
    location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
    place_name: Joi.string(),
    event_date: Joi.date(),
  }),
}

export interface IGetOrderTrackRequest extends IAuthenticatedRequest {
  query: IGetOrderTrack
}

export const getOrderTrackSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    user_type: Joi.string(),
    item_id: Joi.string().regex(objectIdRegex),
    logistics_order_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export interface IGetOrderItemTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
  }
}

export const getOrderItemTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetBuyerOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    tracking_id: string
  }
}

export const getBuyerOrderTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
    tracking_id: Joi.string(),
  }),
}

export interface IGetBuyerOrderCPTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_part_id: string
  }
}

export const getBuyerOrderCPTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_part_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetSupplierOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    tracking_id: string
  }
}

export const getSupplierOrderTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
    tracking_id: Joi.string(),
  }),
}

export interface IGetSupplierOrderCPTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_part_id: string
  }
}

export const getSupplierOrderCPTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_part_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetLogisticsOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    tracking_id: string
  }
}

export const getLogisticsOrderTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
    tracking_id: Joi.string(),
  }),
}
