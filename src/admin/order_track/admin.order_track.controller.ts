import { injectable, inject } from "inversify"
import { controller, httpPost, httpGet } from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { OrderTrackRepository } from "./admin.order_track.repository"
import {
  addOrderTrackSchema,
  IAddOrderTrackRequest,
  getOrderTrackSchema,
  IGetOrderTrackRequest,
  getOrderItemTrackSchema,
  IGetOrderItemTrackRequest,
  getBuyerOrderTrackSchema,
  IGetBuyerOrderTrackRequest,
  getSupplierOrderTrackSchema,
  IGetSupplierOrderTrackRequest,
  getLogisticsOrderTrackSchema,
  IGetLogisticsOrderTrackRequest,
  IGetBuyerOrderCPTrackRequest,
  getBuyerOrderCPTrackSchema,
  IGetSupplierOrderCPTrackRequest,
  getSupplierOrderCPTrackSchema,
} from "./admin.order_track.req-schema"

@injectable()
@controller("/order_track", verifyCustomToken("admin"))
export class OrderTrackController {
  constructor(
    @inject(AdminTypes.OrderTrackRepository)
    private orderTrackRepo: OrderTrackRepository
  ) {}
  @httpPost("/", validate(addOrderTrackSchema))
  async addOrderTrack(
    req: IAddOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.addOrderTrack(req.body, req.user.uid)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/", validate(getOrderTrackSchema))
  async getOrderTrack(
    req: IGetOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        user_id,
        user_type,
        item_id,
        order_id,
        logistics_order_id,
        before,
        after,
      } = req.query

      const data = await this.orderTrackRepo.getOrderTrack(
        user_id,
        user_type,
        item_id,
        order_id,
        logistics_order_id,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /*****************************************************/

  // get tracking details for buyer order item. [Used for web.]
  @httpGet("/buyerOrder/:order_item_id", validate(getOrderItemTrackSchema))
  async getOrderItemTrack(
    req: IGetOrderItemTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id } = req.params

      const data = await this.orderTrackRepo.getOrderItemTrack(order_item_id)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/buyerOrder/CP_track/:order_item_part_id",
    validate(getBuyerOrderCPTrackSchema)
  )
  async getBuyerOrderCPTrack(
    req: IGetBuyerOrderCPTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_part_id } = req.params

      const data = await this.orderTrackRepo.getBuyerOrderCPTrack(
        order_item_part_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/buyerOrder/:order_item_id/:tracking_id",
    validate(getBuyerOrderTrackSchema)
  )
  async getBuyerOrderTrack(
    req: IGetBuyerOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id, tracking_id } = req.params

      const data = await this.orderTrackRepo.getBuyerOrderTrack(
        order_item_id,
        tracking_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/vendorOrder/CP_track/:order_item_part_id",
    validate(getSupplierOrderCPTrackSchema)
  )
  async getSupplierOrderCPTrack(
    req: IGetSupplierOrderCPTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_part_id } = req.params
      const data = await this.orderTrackRepo.getSupplierOrderCPTrack(
        order_item_part_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/vendorOrder/:order_item_id/:tracking_id",
    validate(getSupplierOrderTrackSchema)
  )
  async getSupplierOrderTrack(
    req: IGetSupplierOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id, tracking_id } = req.params
      const data = await this.orderTrackRepo.getSupplierOrderTrack(
        order_item_id,
        tracking_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/logisticsOrder/:order_item_id/:tracking_id",
    validate(getLogisticsOrderTrackSchema)
  )
  async getLogisticsOrderTrack(
    req: IGetLogisticsOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id, tracking_id } = req.params
      const data = await this.orderTrackRepo.getLogisticsOrderTrack(
        order_item_id,
        tracking_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
