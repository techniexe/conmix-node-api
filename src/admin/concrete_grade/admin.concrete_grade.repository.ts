import { injectable, inject } from "inversify"
import { IAddCg, IEditCg } from "./admin.concrete_grade.req-schema"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { CommonService } from "../../utilities/common.service"
import { AdminTypes } from "../admin.types"
import { AccessTypes } from "../../model/access_logs.model"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class AdminConcreteGradeRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addCg(cgData: IAddCg, created_by: string) {
    console.log(cgData)
    const cgCheck = await ConcreteGradeModel.findOne({
      name: cgData.name,
    })
    console.log("cgCheck", cgCheck)
    if (cgCheck !== null) {
      const err = new UnexpectedInput(
        `This Concrete Grade name "${cgData.name}" already added`
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    cgData._id = new ObjectId()
    cgData.created_by = created_by

    const newCg = new ConcreteGradeModel(cgData)
    const newCgDoc = await newCg.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_CONCRETE_GRADE,
      `Admin added concreted_grade of id ${newCgDoc._id} .`
    )

    return Promise.resolve(newCgDoc)
  }

  async editCg(user_id: string, cgId: string, cgData: IEditCg) {
    const result = await AuthModel.findOne({
      code: cgData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    if (!isNullOrUndefined(cgData.name)) {
      const check = await ConcreteGradeModel.findOne({
        name: cgData.name,
      })

      if (!isNullOrUndefined(check) && check._id.equals(cgId) === false) {
        return Promise.reject(
          new InvalidInput(
            `This Concrete Grade name "${cgData.name}" already added.`,
            400
          )
        )
      }
    }

    // let data = await ConcreteGradeModel.findOne({
    //   name: cgData.name,
    // })
    // if (!isNullOrUndefined(data)) {
    //   return Promise.reject(
    //     new InvalidInput(
    //       `This Concrete Grade name "${cgData.name}" already added.`,
    //       400
    //     )
    //   )
    // }

    const flds = ["name"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(cgData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const res = await ConcreteGradeModel.findOneAndUpdate(
      { _id: cgId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This concreted grade doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_CONCRETE_GRADE,
      `Admin Updated details of concreted_grade id ${cgId}.`,
      res,
      editDoc
    )

    await AuthModel.deleteOne({
      user_id,
      code: cgData.authentication_code,
    })

    return Promise.resolve()
  }

  async getCg(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lte: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gte: new Date(after) }
    }
    return ConcreteGradeModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async deleteCg(user_id: string, cgId: string, authentication_code: string) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const updtRes = await ConcreteGradeModel.updateOne(
      {
        _id: new ObjectId(cgId),
        created_by: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by_id: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_CONCRETE_GRADE_BY_ADMIN,
      `Admin deleted concreted grade of id ${cgId} .`
    )

    return Promise.resolve()
  }
}
