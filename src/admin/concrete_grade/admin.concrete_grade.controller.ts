import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminConcreteGradeRepository } from "./admin.concrete_grade.repository"
import {
  IAddCgRequest,
  addCgSchema,
  editCgSchema,
  IEditCgRequest,
  deleteCgSchema,
  IDeleteCgRequest,
} from "./admin.concrete_grade.req-schema"

@controller("/concrete_grade", verifyCustomToken("admin"))
@injectable()
export class AdminConcreteGradeController {
  constructor(
    @inject(AdminTypes.AdminConcreteGradeRepository)
    private concreteGradeRepo: AdminConcreteGradeRepository
  ) {}
  @httpPost("/", validate(addCgSchema))
  async addCg(req: IAddCgRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.concreteGradeRepo.addCg(req.body, req.user.uid)
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:cgId", validate(editCgSchema))
  async editCg(req: IEditCgRequest, res: Response, next: NextFunction) {
    try {
      await this.concreteGradeRepo.editCg(
        req.user.uid,
        req.params.cgId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCg(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after } = req.query
    const data = await this.concreteGradeRepo.getCg(search, before, after)

    console.log("data", data)
    res.json({ data })
  }

  @httpDelete("/:cgId", validate(deleteCgSchema))
  async deleteCg(req: IDeleteCgRequest, res: Response, next: NextFunction) {
    try {
      await this.concreteGradeRepo.deleteCg(
        req.user.uid,
        req.params.cgId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
