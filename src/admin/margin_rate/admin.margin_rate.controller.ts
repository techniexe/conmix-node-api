import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { AdminMarginRateRepository } from "./admin.margin_rate.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addMarginRateSchema,
  IAddMarginRateRequest,
  IUpdateMarginRateRequest,
  updateMarginRateSchema,
} from "./admin.margin_rate.schema"

@controller("/margin_rate", verifyCustomToken("admin"))
@injectable()
export class AdminMarginRateController {
  constructor(
    @inject(AdminTypes.AdminMarginRateRepository)
    private adminMarginRateRepo: AdminMarginRateRepository
  ) {}

  @httpPost("/", validate(addMarginRateSchema))
  async addMarginRate(
    req: IAddMarginRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminMarginRateRepo.addMarginRate(
        req.user.uid,
        req.body
      )
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:margin_rate_id", validate(updateMarginRateSchema))
  async updateMarginRate(
    req: IUpdateMarginRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adminMarginRateRepo.updateMarginRate(
        req.user.uid,
        req.params.margin_rate_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getMarginRateList(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.adminMarginRateRepo.getMarginRateList()
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
