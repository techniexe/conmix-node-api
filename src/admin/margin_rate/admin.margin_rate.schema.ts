import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema } from "../../middleware/joi.middleware"

export interface IAddMarginRate {
  title: string
  slab: [
    {
      upto: number
      rate: number
    }
  ]
  created_by_id: string
}

export interface IAddMarginRateRequest extends IAuthenticatedRequest {
  body: IAddMarginRate
}

export const addMarginRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    slab: Joi.array()
      .items({
        upto: Joi.number()
          .min(1)
          .required(),
        rate: Joi.number()
          .min(0)
          .required(),
      })
      .required(),
  }),
}

export interface IUpdateMarginRate {
  title: string
  slab: [
    {
      upto: number
      rate: number
    }
  ]
  updated_by_id: string
  updated_at: string
  authentication_code: string
}

export interface IUpdateMarginRateRequest extends IAuthenticatedRequest {
  params: {
    margin_rate_id: string
  }
  body: IUpdateMarginRate
}

export const updateMarginRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    title: Joi.string(),
    slab: Joi.array().items({
      upto: Joi.number()
        .min(1)
        .required(),
      rate: Joi.number()
        .min(0)
        .required(),
    }),
    authentication_code: Joi.string().required(),
  }),
}
