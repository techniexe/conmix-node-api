import { injectable, inject } from "inversify"
import { IAddMarginRate, IUpdateMarginRate } from "./admin.margin_rate.schema"
import { MarginRateModel } from "../../model/margin_rate.model"
import { InvalidInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class AdminMarginRateRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addMarginRate(user_id: string, marginRateData: IAddMarginRate) {
    marginRateData.created_by_id = user_id
    const newMarginRate = new MarginRateModel(marginRateData)
    const newMarginRateDoc = await newMarginRate.save()

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.ADD_MARGIN_RATE,
      `Admin added margin_rate of id ${newMarginRateDoc._id} .`
    )

    return Promise.resolve(newMarginRateDoc)
  }

  async updateMarginRate(
    user_id: string,
    margin_rate_id: string,
    gstSlabData: IUpdateMarginRate
  ) {
    const res = await AuthModel.findOne({
      code: gstSlabData.authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const arrEditFld = ["title", "slab"]

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(gstSlabData)) {
      if (arrEditFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id
    try {
      const updtRes = await MarginRateModel.findOneAndUpdate(
        { _id: margin_rate_id },
        { $set: editDoc }
      )
      if (updtRes === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "admin",
        AccessTypes.CHANGE_MARGIN_RATE,
        `Admin Updated details of margin_rate id ${margin_rate_id}.`,
        updtRes,
        editDoc
      )

      await AuthModel.deleteOne({
        user_id,
        code: gstSlabData.authentication_code,
      })
      return Promise.resolve()
    } catch (err) {
      return Promise.reject(err)
    }
  }

  async getMarginRateList() {
    return await MarginRateModel.find()
  }
}
