import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminTypes } from "../admin.types"
import { AdminAggregateSourceRepository } from "./admin.aggregate_source.repository"
import { UnexpectedInput } from "../../utilities/customError"
import {
  addSourceSchema,
  IAddSourceRequest,
  updateSourceSchema,
  IUpdateSourceRequest,
  IRemoveSourceRequest,
  removeSourceSchema,
} from "./admin.aggregate_source.req-schema"

@injectable()
@controller("/aggregate_source", verifyCustomToken("admin"))
export class AdminAggregateSourceController {
  constructor(
    @inject(AdminTypes.AdminAggregateSourceRepository)
    private aggregatesourceRepo: AdminAggregateSourceRepository
  ) {}
  @httpPost("/", validate(addSourceSchema))
  async addSource(req: IAddSourceRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.aggregatesourceRepo.addSource(
        req.body,
        req.user.uid
      )
      res.json({ data: { _id } })
    } catch (err) {
      let msg = `Aggreagte source can't be added.`
      if(err.code === 11000){
        msg = `Same source name with this region already registered.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch("/:source_id", validate(updateSourceSchema))
  async updateSource(
    req: IUpdateSourceRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.aggregatesourceRepo.updateSource(
        req.user.uid,
        req.params.source_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = `Aggreagte source can't be edited.`
      if(err.code === 11000){
        msg = `Same source name with this region already registered.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpDelete("/:source_id", validate(removeSourceSchema))
  async removeSource(
    req: IRemoveSourceRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.aggregatesourceRepo.removeSource(
        req.user.uid,
        req.params.source_id,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async sourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.aggregatesourceRepo.sourceList(req.user.uid, req.query)
    res.json({ data })
  }
}
