import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { IAddSource } from "./admin.aggregate_source.req-schema"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { InvalidInput } from "../../utilities/customError"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class AdminAggregateSourceRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addSource(sourceData: IAddSource, created_by: string) {
    sourceData.created_by_id = created_by
    const newsource = new AggregateSourceModel(sourceData)
    const newsourceDoc = await newsource.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_AGGREGATE_SOURCE_BY_ADMIN,
      `Admin added aggregate source of id ${newsourceDoc._id} .`
    )

    return Promise.resolve(newsourceDoc)
  }

  async updateSource(
    user_id: string,
    source_id: string,
    sourceData: { [k: string]: any }
  ) {
    const res = await AuthModel.findOne({
      code: sourceData.authentication_code,
    })

    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const arrEditFld = ["aggregate_source_name"]

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(sourceData)) {
      if (arrEditFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id
   // try {
      const updtRes = await AggregateSourceModel.findOneAndUpdate(
        { _id: source_id },
        { $set: editDoc }
      )
      if (updtRes === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "admin",
        AccessTypes.UPDATE_AGGREGATE_SOURCE_BY_ADMIN,
        `Admin updated details source of id ${source_id} .`,
        updtRes,
        editDoc
      )

      await AuthModel.deleteOne({
        user_id,
        code: sourceData.authentication_code,
      })

      return Promise.resolve()
    // } catch (err) {
    //   Promise.reject(err)
    // }
  }

  async removeSource(user_id: string, source_id: number, authentication_code: string) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })

    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const status = await AggregateSourceModel.deleteOne({
      _id: new ObjectId(source_id),
    })
    if (status.n !== 1) {
      throw new Error("We could not find this source")
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    return Promise.resolve()
  }

  async sourceList(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.region_id)) {
      query.region_id = new ObjectId(searchParam.region_id)  
    }

    const aggregateArr = [
      { $match: query },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          aggregate_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await AggregateSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }
}
