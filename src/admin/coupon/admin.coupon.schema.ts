import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { ObjectID } from "bson"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { discountTypes } from "../../utilities/config"

export interface IAddCouponDetails {
  code?: string
  discount_type: string
  discount_value: number
  min_order: number
  max_discount: number
  max_usage: number
  unique_use: boolean
  start_date: Date
  end_date: Date
  supplier_ids: [string | ObjectID]
  buyer_ids: [string | ObjectID]
  info: string
  tnc: string
  created_by_id: string
  created_by_type: string
  [k: string]: any
}

export interface IAddCouponRequest extends IAuthenticatedRequest {
  body: IAddCouponDetails
}

export const addCouponSchema: IRequestSchema = {
  body: Joi.object().keys({
    code: Joi.string(),
    discount_type: Joi.string().required().valid(Object.keys(discountTypes)),
    discount_value: Joi.number().required(),
    min_order: Joi.number().required(),
    max_discount: Joi.number().required(),
    max_usage: Joi.number(),
    unique_use: Joi.boolean(),
    start_date: Joi.date().iso().required(),
    end_date: Joi.date().iso().required(),
    //supplier_ids: Joi.array().items(Joi.string().regex(objectIdRegex)),
    buyer_ids: Joi.array().items(Joi.string().regex(objectIdRegex)),
    info: Joi.string().max(250),
    tnc: Joi.string(),
  }),
}

export interface IGetCouponDetails {
  is_deleted?: boolean
  is_active?: boolean
  code?: string
  buyer_id?: string
  supplier_id?: string
  discount_type?: string
  before?: string
  after?: string
  discount_value?: any
  start_date?: Date
  end_date?: Date
  min_order?: any
  max_discount?: any
  min_order_range_value?: any
  max_discount_range_value?: any
}

export interface IGetCouponListRequest extends IAuthenticatedRequest {
  body: IGetCouponDetails
}

export const getCouponSchema: IRequestSchema = {
  query: Joi.object().keys({
    is_deleted: Joi.boolean(),
    buyer_id: Joi.string().regex(objectIdRegex),
    supplier_id: Joi.string().regex(objectIdRegex),
    discount_type: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    code: Joi.string(),
    discount_value: Joi.number(),
    start_date: Joi.date().iso(),
    end_date: Joi.date().iso(),
    min_order: Joi.number(),
    max_discount: Joi.number(),
    is_active: Joi.boolean(),
    min_order_range_value: Joi.string(),
    max_discount_range_value: Joi.string(),
  }),
}

export interface IDeleteCouponRequest extends IAuthenticatedRequest {
  params: {
    couponId: string
  }
  body: {
    authentication_code: string
  }
}

export const deleteCouponSchema: IRequestSchema = {
  params: Joi.object().keys({
    couponId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IUpdateCouponDetails {
  discount_type?: string
  discount_value?: number
  min_order?: number
  max_discount?: number
  max_usage?: number
  unique_use?: boolean
  is_active?: boolean
  start_date?: Date
  end_date?: Date
  //supplier_ids?: [string | ObjectID]
  buyer_ids?: [string | ObjectID]
  info?: string
  tnc?: string
  updated_by_id: string
  authentication_code: string
}

export interface IUpdateCouponRequest extends IAuthenticatedRequest {
  body: IUpdateCouponDetails
}

export const updateCouponSchema: IRequestSchema = {
  params: Joi.object().keys({
    couponId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    discount_type: Joi.string().valid(Object.keys(discountTypes)),
    discount_value: Joi.number(),
    min_order: Joi.number(),
    max_discount: Joi.number(),
    max_usage: Joi.number(),
    unique_use: Joi.boolean(),
    is_active: Joi.boolean(),
    start_date: Joi.date().iso(),
    end_date: Joi.date().iso(),
    //supplier_ids: Joi.array().items(Joi.string().regex(objectIdRegex)),
    buyer_ids: Joi.array().items(Joi.string().regex(objectIdRegex)),
    info: Joi.string().max(250).allow(""),
    tnc: Joi.string().allow(""),
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetCouponDetailsRequest extends IAuthenticatedRequest {
  params: {
    couponId: string
  }
}

export const getCouponDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    couponId: Joi.string(),
  }),
}
