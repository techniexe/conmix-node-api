import {
  controller,
  httpPost,
  httpGet,
  httpDelete,
  httpPatch,
} from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { AdminCouponRepository } from "./admin.coupon.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addCouponSchema,
  IAddCouponRequest,
  getCouponSchema,
  IGetCouponListRequest,
  deleteCouponSchema,
  IDeleteCouponRequest,
  updateCouponSchema,
  IUpdateCouponRequest,
  getCouponDetailsSchema,
  IGetCouponDetailsRequest,
} from "./admin.coupon.schema"

@controller("/coupon", verifyCustomToken("admin"))
@injectable()
export class AdminCouponController {
  constructor(
    @inject(AdminTypes.AdminCouponRepository)
    private AdminCouponRepo: AdminCouponRepository
  ) {}

  @httpPost("/", validate(addCouponSchema))
  async addCoupon(req: IAddCouponRequest, res: Response, next: NextFunction) {
    try {
      const data = await this.AdminCouponRepo.addCoupon(req.user.uid, req.body)
      return res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Get coupon list.
   */
  @httpGet("/", validate(getCouponSchema))
  async getCouponList(
    req: IGetCouponListRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.AdminCouponRepo.getCouponList(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Remove coupon.
   */
  @httpDelete("/:couponId", validate(deleteCouponSchema))
  async deleteCoupon(
    req: IDeleteCouponRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.AdminCouponRepo.deleteCoupon(
        req.user.uid,
        req.params.couponId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  /**
   * Update coupon.
   */
  @httpPatch("/:couponId", validate(updateCouponSchema))
  async updateCoupon(
    req: IUpdateCouponRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.AdminCouponRepo.updateCoupon(
        req.user.uid,
        req.params.couponId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:couponId", validate(getCouponDetailsSchema))
  async getCouponDetails(
    req: IGetCouponDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { couponId } = req.params
      const data = await this.AdminCouponRepo.getCouponDetails(couponId)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
