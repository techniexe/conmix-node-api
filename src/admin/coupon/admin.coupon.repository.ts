import { injectable, inject } from "inversify"
import { IAddCouponDetails, IGetCouponDetails } from "./admin.coupon.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { CouponModel } from "../../model/coupon.model"
import { InvalidInput } from "../../utilities/customError"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { UserType } from "../../utilities/config"
import { ObjectId } from "bson"
import { AuthModel } from "../../model/auth.model"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { AccessTypes } from "../../model/access_logs.model"

@injectable()
export class AdminCouponRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService,
    @inject(AdminTypes.UniqueidService) private uniqueId: UniqueidService
  ) {}
  async addCoupon(user_id: string, couponData: IAddCouponDetails) {
    try {
      if (!isNullOrUndefined(couponData.code)) {
        const couponCheck = await CouponModel.findOne({ code: couponData.code })
        if (!isNullOrUndefined(couponCheck)) {
          return Promise.reject(
            new InvalidInput(`This coupon code has been added already`, 400)
          )
        }
      } else {
        var code = await this.uniqueId.random_string_without_O()
        couponData.code = code
      }

      // if (!isNullOrUndefined(couponData.supplier_ids)) {
      //   await this.commonService.checkIsValid(
      //     VendorUserModel,
      //     couponData.supplier_ids
      //   )
      // }

      if (!isNullOrUndefined(couponData.buyer_ids)) {
        await this.commonService.checkIsValid(
          BuyerUserModel,
          couponData.buyer_ids
        )
      }
      if (!isNullOrUndefined(couponData.max_usage)) {
        couponData.remaining_usage = couponData.max_usage
      }
      couponData.created_by_id = user_id
      couponData.created_by_type = UserType.ADMIN
      const newCoupon = new CouponModel(couponData)
      const newCouponDoc = await newCoupon.save()

      await this.commonService.addActivityLogs(
        user_id,
        "admin",
        AccessTypes.ADD_COUPON_BY_ADMIN,
        `Admin added coupon of id ${newCouponDoc._id} .`
      )

      return Promise.resolve(newCouponDoc.code)
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }

  async getCouponList(searchParam: IGetCouponDetails) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(searchParam.is_deleted)) {
      query.is_deleted = searchParam.is_deleted
    }
    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }
   
    if (!isNullOrUndefined(searchParam.discount_type)) {
      query.discount_type = searchParam.discount_type
    }
    if (!isNullOrUndefined(searchParam.buyer_id)) {
      query.buyer_ids = new ObjectId(searchParam.buyer_id)
    }
    // if (!isNullOrUndefined(searchParam.supplier_id)) {
    //   query.supplier_ids = new ObjectId(searchParam.supplier_id)
    // }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.code)) {
      query.code = searchParam.code
    }
    if (!isNullOrUndefined(searchParam.discount_value)) {
      query.discount_value =  parseInt(searchParam.discount_value)
    }
    if (!isNullOrUndefined(searchParam.start_date)) {
      query.start_date =  { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date)) {
      query.end_date =  { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      !isNullOrUndefined(searchParam.end_date)
    ) {
      query.created_at = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }
    if (!isNullOrUndefined(searchParam.min_order) && 
    !isNullOrUndefined(searchParam.min_order_range_value) && 
    searchParam.min_order_range_value === "less") {
      query.min_order =  { $lte: parseInt(searchParam.min_order) }
    }
    if (!isNullOrUndefined(searchParam.min_order) && 
    !isNullOrUndefined(searchParam.min_order_range_value) &&
    searchParam.min_order_range_value === "more")  {
      query.min_order =  { $gte: parseInt(searchParam.min_order) }
    }
    if (!isNullOrUndefined(searchParam.max_discount) 
    && !isNullOrUndefined(searchParam.max_discount_range_value) &&
    searchParam.max_discount_range_value === "less") {
      query.max_discount =  { $lte: parseInt(searchParam.max_discount) }
    }
    if (!isNullOrUndefined(searchParam.max_discount) && 
    !isNullOrUndefined(searchParam.max_discount_range_value) &&
    searchParam.max_discount_range_value === "more") {
      query.max_discount =  { $gte: parseInt(searchParam.max_discount) }
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $project: {
          discount_type: true,
          discount_value: true,
          min_order: true,
          max_discount: true,
          start_date: true,
          end_date: true,
          code: true,
          created_at: true,
          info: true,
          tnc: true,
          supplier_id: true,
          buyer_ids: true,
          is_deleted: true,
          is_active: true,
          unique_use: true,
          max_usage: true,
        },
      },
    ]

    const couponData = await CouponModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      couponData.reverse()
    }
    return Promise.resolve(couponData)
  }

  async deleteCoupon(
    user_id: string,
    couponId: string,
    authentication_code: string
  ) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await CouponModel.updateOne(
      {
        _id: couponId,
        created_by_id: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by_id: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_COUPON_BY_ADMIN,
      `Admin deleted coupon of id ${couponId} .`
    )

    return Promise.resolve()
  }

  async updateCoupon(
    user_id: string,
    couponId: string,
    couponData: { [k: string]: any }
  ) {
    const res = await AuthModel.findOne({
      code: couponData.authentication_code,
    })

    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const arrEditFld = [
      "discount_type",
      "discount_value",
      "min_order",
      "max_discount",
      "max_usage",
      "unique_use",
      "is_active",
      "start_date",
      "end_date",
      // "supplier_ids",
      "buyer_ids",
      "info",
      "tnc",
    ]

    // if (!isNullOrUndefined(couponData.supplier_ids)) {
    //   await this.commonService.checkIsValid(
    //     VendorUserModel,
    //     couponData.supplier_ids
    //   )
    // }

    if (!isNullOrUndefined(couponData.buyer_ids)) {
      await this.commonService.checkIsValid(
        BuyerUserModel,
        couponData.buyer_ids
      )
    }

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(couponData)) {
      if (arrEditFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id
    try {
      const updtRes = await CouponModel.findOneAndUpdate(
        { _id: couponId },
        { $set: editDoc }
      )
      if (updtRes === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "admin",
        AccessTypes.UPDATE_COUPON_BY_ADMIN,
        `Admin updated details coupon of id ${couponId} .`,
        updtRes,
        editDoc
      )

      await AuthModel.deleteOne({
        user_id,
        code: couponData.authentication_code,
      })
      return Promise.resolve()
    } catch (err) {
      Promise.reject(err)
    }
  }

  async getCouponDetails(coupon_id: string) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(coupon_id),
    }

    const aggregateArr = [
      { $match: query },

      // {
      //   $lookup: {
      //     from: "supplier_user",
      //     localField: "supplier_ids",
      //     foreignField: "_id",
      //     as: "supplier",
      //   },
      // },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_ids",
          foreignField: "_id",
          as: "buyer",
        },
      },
      // {
      //   $unwind: {
      //     path: "$buyer",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      {
        $project: {
          discount_type: true,
          discount_value: true,
          min_order: true,
          max_discount: true,
          start_date: true,
          end_date: true,
          code: true,
          created_at: true,
          info: true,
          tnc: true,
          is_deleted: true,
          is_active: true,
          unique_use: true,
          "buyer._id": true,
          "buyer.mobile_verified": true,
          "buyer.email_verified": true,
          "buyer.mobile_number": true,
          "buyer.email": true,
          "buyer.full_name": true,
          "buyer.gender": true,
          // "supplier._id": true,
          // "supplier.mobile_verified": true,
          // "supplier.business_name": true,
          // "supplier.mobile_number": true,
          // "supplier.email": true,
          // "supplier.email_verified": true,
          // "supplier.full_name": true,
        },
      },
    ]

    const couponData = await CouponModel.aggregate(aggregateArr)
    if (couponData.length > 0) {
      return Promise.resolve(couponData[0])
    }
    return Promise.reject(new InvalidInput(`No couponData  found.`, 400))
  }
}
