import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { BillStatus, monthsCode } from "../../utilities/config"

export interface IGetOrder {
  user_id?: string
  before?: string
  after?: string
  gateway_transaction_id?: string
  site_id?: string
  month?: string
  year?: number
  date: Date,
  order_status?: string
  payment_status?: string
  buyer_order_id?: string
  buyer_name?: string
  payment_mode?:string
}

export interface IGetOrderRequest extends IAuthenticatedRequest {
  query: IGetOrder
}

export const getOrder: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    gateway_transaction_id: Joi.string(),
    site_id: Joi.string().regex(objectIdRegex),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
    order_status: Joi.string(),
    payment_status: Joi.string(),
    buyer_order_id: Joi.string(),
    buyer_name: Joi.string(),
    payment_mode : Joi.string(),
  }),
}

export interface IUpdateOrder {
  order_status?: string
  payment_status?: string
  authentication_code: string
  admin_remarks?: string
}

export interface IUpdateOrderRequest extends IAuthenticatedRequest {
  body: IUpdateOrder
  params: {
    orderId: string
  }
}

export const updateOrder: IRequestSchema = {
  body: Joi.object().keys({
    order_status: Joi.string(),
    payment_status: Joi.string(),
    authentication_code: Joi.string().required(),
    admin_remarks: Joi.string().max(1000),
  }),
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
}

export interface IGetOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const getOrderById: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
}

export interface IUpdateOrderItem {
  order_item_status?: string
  authentication_code: string
  admin_remarks?: string
}

export interface IUpdateOrderItemRequest extends IAuthenticatedRequest {
  body: IUpdateOrderItem
  params: {
    orderItemId: string
  }
}

export const updateOrderItemShema: IRequestSchema = {
  body: Joi.object().keys({
    order_item_status: Joi.string(),
    admin_remarks: Joi.string().max(1000),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    orderItemId: Joi.string().required(),
  }),
}

export interface IGetVendorOrder {
  user_id?: string
  before?: string
  after?: string
  design_mix_id?: string
  buyer_id?: string
  address_id?: string
  month?: string
  year?: number
  date: Date
  payment_status?: string
  order_status?: string
  order_id?: string
  company_name?: string
  payment_mode?: string
}

export interface IGetVendorOrderRequest extends IAuthenticatedRequest {
  query: IGetVendorOrder
}

export const getVendorOrderSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    design_mix_id: Joi.string().regex(objectIdRegex),
    buyer_id: Joi.string().regex(objectIdRegex),
    address_id: Joi.string().regex(objectIdRegex),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
    payment_status: Joi.string(),
    order_status: Joi.string(),
    order_id: Joi.string(),
    company_name: Joi.string(),
    payment_mode: Joi.string(),
  }),
}

export interface IGetVendorOrderDetails {
  user_id?: string
  before?: string
  after?: string
}

export interface IGetVendorOrderDetailsRequest extends IAuthenticatedRequest {
  query: IGetVendorOrderDetails
  params: {
    orderId: string
  }
}

export const getVendorOrderDetailsSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
}

export interface IGetLogiscticsOrder {
  user_id?: string
  before?: string
  after?: string
}

export interface IGetLogiscticsOrderRequest extends IAuthenticatedRequest {
  query: IGetLogiscticsOrder
}

export const getLogiscticsOrderSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export interface IGetLogiscticsOrderDetails {
  user_id?: string
  before?: string
  after?: string
}

export interface IGetLogiscticsOrderDetailsRequest
  extends IAuthenticatedRequest {
  query: IGetLogiscticsOrderDetails
  params: {
    orderId: string
  }
}

export const getLogiscticsOrderDetailsSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
  }),
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
}

export interface IUpdateSupplierOrderBillStatusRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    bill_id: string
    bill_status: string
    bill_reject_reason: string
  }
}

export const updateSupplierOrderBillStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    bill_id: Joi.string().regex(objectIdRegex).required(),
    bill_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    bill_reject_reason: Joi.string(),
  }),
}

export interface IUpdateSupplierOrderStatusRequest
  extends IAuthenticatedRequest {
  params: {
    supplier_orderId: string
  }
  body: {
    order_status: string
  }
}

export const updateSupplierOrderStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    supplier_orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    order_status: Joi.string().required(),
  }),
}

export interface IUpdateLogiscticsOrderBillStatusRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    bill_id: string
    bill_status: string
    bill_reject_reason: string
  }
}

export const updateLogisticsOrderBillStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    bill_id: Joi.string().regex(objectIdRegex).required(),
    bill_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    bill_reject_reason: Joi.string(),
  }),
}

export interface IUpdateLogiscticsOrderCreditNoteRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    credit_note_id: string
    credit_note_status: string
    credit_note_reject_reason: string
  }
}

export const updateLogisticsOrderCreditNoteSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    credit_note_id: Joi.string().regex(objectIdRegex).required(),
    credit_note_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    credit_note_reject_reason: Joi.string(),
  }),
}

export interface IUpdateLogiscticsOrderDebitNoteRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    debit_note_id: string
    debit_note_status: string
    debit_note_reject_reason: string
  }
}

export const updateLogisticsOrderDebitNoteSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    debit_note_id: Joi.string().regex(objectIdRegex).required(),
    debit_note_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    debit_note_reject_reason: Joi.string(),
  }),
}

export interface IUpdateSupplierOrderCreditNoteRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    credit_note_id: string
    credit_note_status: string
    credit_note_reject_reason: string
  }
}

export const updateSupplierOrderCreditNoteSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    credit_note_id: Joi.string().regex(objectIdRegex).required(),
    credit_note_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    credit_note_reject_reason: Joi.string(),
  }),
}

export interface IUpdateSupplierOrderDebitNoteRequest
  extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  body: {
    debit_note_id: string
    debit_note_status: string
    debit_note_reject_reason: string
  }
}

export const updateSupplierOrderDebitNoteSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    debit_note_id: Joi.string().regex(objectIdRegex).required(),
    debit_note_status: Joi.string()
      .valid(BillStatus.ACCEPTED, BillStatus.REJECTED)
      .required(),
    debit_note_reject_reason: Joi.string(),
  }),
}
