import { injectable, inject } from "inversify"
import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import {
  getOrder,
  IGetOrderRequest,
  IUpdateOrderRequest,
  getOrderById,
  IGetOrderByIdRequest,
  updateOrder,
  updateOrderItemShema,
  IUpdateOrderItemRequest,
  getLogiscticsOrderSchema,
  IGetLogiscticsOrderRequest,
  getLogiscticsOrderDetailsSchema,
  IGetLogiscticsOrderDetailsRequest,
  updateSupplierOrderBillStatusSchema,
  IUpdateSupplierOrderBillStatusRequest,
  updateSupplierOrderStatusSchema,
  IUpdateSupplierOrderStatusRequest,
  updateLogisticsOrderBillStatusSchema,
  IUpdateLogiscticsOrderBillStatusRequest,
  updateLogisticsOrderCreditNoteSchema,
  IUpdateLogiscticsOrderCreditNoteRequest,
  updateLogisticsOrderDebitNoteSchema,
  IUpdateLogiscticsOrderDebitNoteRequest,
  updateSupplierOrderCreditNoteSchema,
  IUpdateSupplierOrderCreditNoteRequest,
  updateSupplierOrderDebitNoteSchema,
  IUpdateSupplierOrderDebitNoteRequest,
  getVendorOrderSchema,
  IGetVendorOrderRequest,
  getVendorOrderDetailsSchema,
  IGetVendorOrderDetailsRequest,
} from "./admin.order.req-schema"
import { OrderRepository } from "./admin.order.repository"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"

@injectable()
@controller("/order", verifyCustomToken("admin"))
export class OrderController {
  constructor(
    @inject(AdminTypes.OrderRepository) private orderRepo: OrderRepository
  ) {}

  // Get Buyer order list.
  @httpGet("/", validate(getOrder))
  async getOrder(req: IGetOrderRequest, res: Response, next: NextFunction) {
    const {
      user_id,
      before,
      after,
      order_status,
      payment_status,
      gateway_transaction_id,
      site_id,
      month,
      year,
      date,
      buyer_order_id,
      payment_mode,
      buyer_name
    } = req.query

    const data = await this.orderRepo.getOrder(
      user_id,
      before,
      after,
      order_status,
      payment_status,
      gateway_transaction_id,
      site_id,
      month,
      year,
      date,
      buyer_order_id,
      payment_mode,
      buyer_name
    )
    res.json({ data })
  }

  // Get Vendor order list.
  @httpGet("/vendorOrder", validate(getVendorOrderSchema))
  async getVendorOrder(
    req: IGetVendorOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    const { user_id, before, after, payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      order_id,
      month,
      year,
      date,
      payment_mode,
      company_name} = req.query
    const data = await this.orderRepo.getVendorOrder(user_id, before, after,
      payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      order_id,
      month,
      year,
      date,
      payment_mode,
      company_name)
    res.json({ data })
  }
  // Get Buyer order details.
  @httpGet("/:orderId", validate(getOrderById))
  async getOrderById(
    req: IGetOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.orderRepo.getOrderById(req.params.orderId)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  // update buyer order.
  @httpPatch("/:orderId", validate(updateOrder))
  async updateOrder(
    req: IUpdateOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.orderRepo.updateOrder(req.user.uid, req.params.orderId, req.body)
    res.sendStatus(201)
  }

  // update buyer order items.
  @httpPatch("/item/:orderItemId", validate(updateOrderItemShema))
  async updateOrderItem(
    req: IUpdateOrderItemRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.orderRepo.updateOrderItem(
      req.user.uid,
      req.params.orderItemId,
      req.body
    )
    res.sendStatus(201)
  }

  // Get Vendor order details.
  @httpGet("/vendorOrder/:orderId", validate(getVendorOrderDetailsSchema))
  async getVendorOrderDetails(
    req: IGetVendorOrderDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { user_id } = req.query

    const data = await this.orderRepo.getVendorOrderDetails(
      req.params.orderId,
      user_id
    )
    res.json({ data })
  }

  @httpPatch(
    "/:orderId/updateVendorOrderBillStatus",
    validate(updateSupplierOrderBillStatusSchema)
  )
  async updateSupplierOrderBillStatus(
    req: IUpdateSupplierOrderBillStatusRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateSupplierOrderBillStatus(
        req.params.orderId,
        req.body.bill_id,
        req.body.bill_status,
        req.body.bill_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:orderId/updateVendorOrderCreditNoteStatus",
    validate(updateSupplierOrderCreditNoteSchema)
  )
  async updateSupplierCreditNoteStatus(
    req: IUpdateSupplierOrderCreditNoteRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateSupplierCreditNoteStatus(
        req.params.orderId,
        req.body.credit_note_id,
        req.body.credit_note_status,
        req.body.credit_note_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:orderId/updateVendorOrderDebitNoteStatus",
    validate(updateSupplierOrderDebitNoteSchema)
  )
  async updateSupplierDebitNoteStatus(
    req: IUpdateSupplierOrderDebitNoteRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateSupplierDebitNoteStatus(
        req.params.orderId,
        req.body.debit_note_id,
        req.body.debit_note_status,
        req.body.debit_note_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:supplier_orderId/updateVendorOrderStatus",
    validate(updateSupplierOrderStatusSchema)
  )
  async updateSupplierOrderStatus(
    req: IUpdateSupplierOrderStatusRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateSupplierOrderStatus(
        req.params.supplier_orderId,
        req.body.order_status
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:orderId/updateLogisticsOrderBillStatus",
    validate(updateLogisticsOrderBillStatusSchema)
  )
  async updateLogisticsOrderBillStatus(
    req: IUpdateLogiscticsOrderBillStatusRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateLogisticsOrderBillStatus(
        req.params.orderId,
        req.body.bill_id,
        req.body.bill_status,
        req.body.bill_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:orderId/updateLogisticsOrderCreditNoteStatus",
    validate(updateLogisticsOrderCreditNoteSchema)
  )
  async updateLogisticsCreditNoteStatus(
    req: IUpdateLogiscticsOrderCreditNoteRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateLogisticsCreditNoteStatus(
        req.params.orderId,
        req.body.credit_note_id,
        req.body.credit_note_status,
        req.body.credit_note_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:orderId/updateLogisticsOrderDebitNoteStatus",
    validate(updateLogisticsOrderDebitNoteSchema)
  )
  async updateLogisticsDebitNoteStatus(
    req: IUpdateLogiscticsOrderDebitNoteRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateLogisticsDebitNoteStatus(
        req.params.orderId,
        req.body.debit_note_id,
        req.body.debit_note_status,
        req.body.debit_note_reject_reason
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/logisticsOrder", validate(getLogiscticsOrderSchema))
  async getLogisticsOrder(
    req: IGetLogiscticsOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    const { user_id, before, after } = req.query

    const data = await this.orderRepo.getLogisticsOrder(user_id, before, after)
    res.json({ data })
  }

  @httpGet(
    "/logisticsOrder/:orderId",
    validate(getLogiscticsOrderDetailsSchema)
  )
  async getLogisticsOrderDetails(
    req: IGetLogiscticsOrderDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.orderRepo.getLogisticsOrderDetails(
      req.params.orderId
    )
    res.json({ data })
  }
}
