import { CPOrderTrackModel } from "./../../model/CP_order_track.model"
import {
  VehicleModel,
  VehicleCategoryeModel,
} from "./../../model/vehicle.model"
import { BillStatus, monthsCode, UserType } from "./../../utilities/config"
import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { OrderModel } from "../../model/order.model"
import { IUpdateOrder, IUpdateOrderItem } from "./admin.order.req-schema"
import { InvalidInput } from "../../utilities/customError"
import { AuthModel } from "../../model/auth.model"
import { ObjectId } from "bson"
import { OrderItemModel } from "../../model/order_item.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { LogisticsOrderModel } from "../../model/logistics.order.model"
import { OrderTrackModel } from "../../model/order_track.model"
import { DriverInfoModel } from "../../model/driver.model"
import { TMModel, TMCategoryModel } from "../../model/TM.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { TestService } from "../../utilities/test.service"
import { VendorNotificationType } from "../../model/notification.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import { BuyerUserModel } from "../../model/buyer.user.model"
@injectable()
export class OrderRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService,
    @inject(AdminTypes.TestService) private testService: TestService,
    @inject(AdminTypes.NotificationUtility)
    private notUtil: NotificationUtility
  ) {}
  async getOrder(
    user_id?: string,
    before?: string,
    after?: string,
    order_status?: string,
    payment_status?: string,
    gateway_transaction_id?: string,
    site_id?: string,
    month?: string,
    year?: number,
    date?: Date,
    buyer_order_id?: string,
    payment_mode?: string,
    buyer_name?: string,
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }
    const sort = { created_at: -1 }

    // If month or year not passed, then check with current date.
    //  if (isNullOrUndefined(month) || isNullOrUndefined(year)) {
    //   var now = new Date()
    //   query.created_at = {
    //     $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
    //   }
    // }

    if(!isNullOrUndefined(buyer_order_id)){
      query.display_id = buyer_order_id
    }

    if (!isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }

    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    if (!isNullOrUndefined(date)) {
      var d1 = new Date(date)
      console.log("d1", d1)
      let next_day = new Date(d1.setDate(d1.getDate() + 1))
      console.log("date", date)
      console.log("next_day", next_day)
      query.created_at = {
        $gte: date,
        $lte: next_day,
      }
    }

    // if (!isNullOrUndefined(year)) {
    //   query.created_at = {
    //     $gte: new Date(year, 0, 1),
    //     $lte: new Date(year, 11, 31),
    //   }
    // }

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }
    if (!isNullOrUndefined(order_status)) {
      query.order_status = order_status
    }
    if (!isNullOrUndefined(payment_status)) {
      query.payment_status = payment_status
    }
    if (!isNullOrUndefined(gateway_transaction_id)) {
      query.gateway_transaction_id = gateway_transaction_id
    }
    if (!isNullOrUndefined(site_id)) {
      query.site_id = site_id
    }
    if (!isNullOrUndefined(payment_mode)) {
      query.payment_mode = payment_mode
    }

    if(!isNullOrUndefined(buyer_name)){
      let buyerData = await BuyerUserModel.findOne({
        full_name : buyer_name
      })
      if(!isNullOrUndefined(buyerData)){
        query.user_id = new ObjectId(buyerData._id)
      }else {
        query.user_id = ""
      }
    }
    console.log("order query: ", query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
        },
      },
      {
        $project: {
          _id: true,
          display_id: true,
          total_amount: true,
          created_at: true,
          order_status: true,
          payment_status: true,
          fee: true,
          tax: true,
          gateway_transaction_id: true,
          payment_mode: true,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const orderData = await OrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      orderData.reverse()
    }
    return Promise.resolve(orderData)
  }
  async updateOrder(user_id: string, orderId: string, orderData: IUpdateOrder) {
    const result = await AuthModel.findOne({
      code: orderData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(orderData.order_status)) {
      editDoc.order_status = orderData.order_status
    }
    if (!isNullOrUndefined(orderData.payment_status)) {
      editDoc.payment_status = orderData.payment_status
    }
    if (!isNullOrUndefined(orderData.admin_remarks)) {
      editDoc.admin_remarks = orderData.admin_remarks
    }
    if (Object.keys(editDoc).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const res = await OrderModel.findOneAndUpdate(
      { _id: orderId },
      { $set: editDoc }
    )
    if (res === null) {
      const err1 = new InvalidInput("No matched record found.")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_ORDER_BY_ADMIN,
      `Admin Updated details of order id ${orderId}.`,
      res,
      editDoc
    )

    await AuthModel.deleteOne({
      user_id,
      code: orderData.authentication_code,
    })

    return Promise.resolve()
  }
  async getOrderById(orderId: string) {
    const orderQuery: { [k: string]: any } = {}
    orderQuery._id = new ObjectId(orderId)
    const aggregateOrderArr = [
      { $match: orderQuery },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "site",
          localField: "site_id",
          foreignField: "_id",
          as: "site",
        },
      },
      {
        $unwind: "$site",
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          display_id: 1,
          user_id: 1,
          order_status: 1,
          payment_status: 1,
          selling_price: 1,
          margin_price: 1,
          gst_amount: 1,
          TM_price: 1,
          CP_price: 1,
          delivery_location: 1,
          payment_attempt: 1,
          payment_mode: 1,
          base_amount: 1,
          logistics_amount: 1,
          margin_amount: 1,
          coupon_amount: 1,
          cgst_price: 1,
          sgst_price: 1,
          gst_price: 1,
          total_amount: 1,
          site_id: 1,
          site_name: 1,
          company_name: "$site.company_name",
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          created_at: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          mobile_number: "$site.mobile_number",
          alt_mobile_number: "$site.alt_mobile_number",
          fee: 1,
          tax: 1,
          gateway_transaction_id: 1,
          total_CP_price: 1,
          "invoice._id": true,
          "buyer._id": true,
          "buyer.full_name": true,
          "buyer.company_name": true,
        },
      },
    ]
    const orderData = await OrderModel.aggregate(aggregateOrderArr)
    if (orderData.length <= 0) {
      return Promise.reject(
        new InvalidInput(`We could not find order details for this order`, 400)
      )
    }
    const orderItemQuery: { [k: string]: any } = {}
    orderItemQuery.order_id = new ObjectId(orderId)
    console.log(orderItemQuery)
    const aggregateArr = [
      { $match: orderItemQuery },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "vendor_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item_part",
          localField: "order_id",
          foreignField: "buyer_order_id",
          as: "orderItemPartData",
        },
      },
      // {
      //   $lookup: {
      //     from: "product_sub_category",
      //     localField: "product_sub_category_id",
      //     foreignField: "_id",
      //     as: "product_sub_category",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$product_sub_category",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      {
        $project: {
          display_item_id: 1,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          design_mix_id: 1,
          cement_quantity: 1,
          sand_quantity: 1,
          aggregate1_quantity: 1,
          aggregate2_quantity: 1,
          fly_ash_quantity: 1,
          admix_quantity: 1,
          water_quantity: 1,
          vendor_id: 1,
          buyer_id: 1,
          order_id: 1,
          quantity: 1,
          revised_quantity: 1,
          cement_per_kg_rate: 1,
          sand_per_kg_rate: 1,
          aggregate1_per_kg_rate: 1,
          aggregate2_per_kg_rate: 1,
          fly_ash_per_kg_rate: 1,
          admix_per_kg_rate: 1,
          water_per_ltr_rate: 1,
          cement_price: 1,
          sand_price: 1,
          aggreagte1_price: 1,
          aggreagte2_price: 1,
          fly_ash_price: 1,
          admix_price: 1,
          water_price: 1,
          with_TM: 1,
          with_CP: 1,
          distance: 1,
          unit_price: 1,
          selling_price: 1,
          margin_price: 1,
          TM_price: 1,
          CP_price: 1,
          sgst_rate: 1,
          igst_price: 1,
          cgst_price: 1,
          gst_price: 1,
          coupon_amount: 1,
          gst_type: 1, // IGST/ SGST,CGST
          igst_rate: 1,
          cgst_rate: 1,
          igst_amount: 1,
          sgst_amount: 1,
          cgst_amount: 1,
          item_status: 1,
          created_at: 1,
          created_by_id: 1,
          created_by_type: 1,
          payment_status: 1,
          address_id: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          location: 1,
          address_type: 1,
          business_name: 1,
          city_name: 1,
          state_name: 1,
          total_amount: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "vendor.mobile_number": 1,
          "vendor.email": 1,
          vendor_media: 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "buyer.company_name": true,
          end_date: true,
          part_quantity: true,
          orderItemPartData: true,
          total_CP_price: true,
          selling_price_With_Margin: true
        },
      },
    ]
    const orderItemData = await OrderItemModel.aggregate(aggregateArr)

    for (let i = 0; i < orderItemData.length; i++) {
      for (let j = 0; j < orderItemData[i].orderItemPartData.length; j++) {
        let TMtrack_details = await OrderTrackModel.find({
          order_item_part_id: new ObjectId(
            orderItemData[i].orderItemPartData[j]._id
          ),
        })
        orderItemData[i].orderItemPartData[j].TMtrack_details = TMtrack_details

        let CPtrack_details = await CPOrderTrackModel.find({
          order_item_part_id: new ObjectId(
            orderItemData[i].orderItemPartData[j]._id
          ),
        })
        orderItemData[i].orderItemPartData[j].CPtrack_details = CPtrack_details
      }
    }
    return Promise.resolve({
      orderData: orderData[0],
      orderItemData,
    })
  }
  async updateOrderItem(
    user_id: string,
    orderItemId: string,
    orderItemData: IUpdateOrderItem
  ) {
    const result = await AuthModel.findOne({
      code: orderItemData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const editDoc: { [k: string]: any } = {}
    if (!isNullOrUndefined(orderItemData.order_item_status)) {
      editDoc.item_status = orderItemData.order_item_status
    }
    if (!isNullOrUndefined(orderItemData.admin_remarks)) {
      editDoc.admin_remarks = orderItemData.admin_remarks
    }
    if (Object.keys(editDoc).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const res = await OrderItemModel.findOneAndUpdate(
      { _id: orderItemId },
      { $set: editDoc }
    )
    if (res === null) {
      const err1 = new InvalidInput("No matched record found.")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_ORDER_ITEM_BY_ADMIN,
      `Admin Updated details of order id ${orderItemId}.`,
      res,
      editDoc
    )
    await AuthModel.deleteOne({
      user_id,
      code: orderItemData.authentication_code,
    })

    return Promise.resolve()
  }

  async getVendorOrder(
    user_id?: string,
    before?: string,
    after?: string,
    payment_status?: string,
    design_mix_id?: string,
    order_status?: string,
    buyer_id?: string,
    address_id?: string,
    order_id?: string,
    month?: string,
    year?: number,
    date?: Date,
    payment_mode?: string,
    company_name?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }
    if (!isNullOrUndefined(payment_status)) {
      query.payment_status = payment_status
    }
    if (!isNullOrUndefined(design_mix_id)) {
      query.design_mix_id = design_mix_id
    }

    if (!isNullOrUndefined(order_status)) {
      query.order_status = order_status
    }

    if (!isNullOrUndefined(buyer_id)) {
      let buyerUserData = await BuyerUserModel.findOne({
        user_id: buyer_id,
      })
      if (!isNullOrUndefined(buyerUserData)) {
        query.buyer_id = buyerUserData._id
      }
    }

    if (!isNullOrUndefined(address_id)) {
      query.address_id = new ObjectId(address_id)
    }

    if (!isNullOrUndefined(order_id)) {
      query._id = order_id
    }


    if (!isNullOrUndefined(payment_mode)) {
      query.payment_mode = payment_mode
    }

    if (!isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }


    if (!isNullOrUndefined(year) && !isNullOrUndefined(month)) {
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    if (!isNullOrUndefined(date)) {
      var d1 = new Date(date)
      console.log("d1", d1)
      let next_day = new Date(d1.setDate(d1.getDate() + 1))
      console.log("date", date)
      console.log("next_day", next_day)
      query.created_at = {
        $gte: date,
        $lte: next_day,
      }
    }

    if (!isNullOrUndefined(company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
          {
            user_id: new ObjectId(user_id1),
          },
        ]
      }else {
        query.user_id = ""
      }
    }

    // if (!isNullOrUndefined(year)) {
    //   query.created_at = {
    //     $gte: new Date(year, 0, 1),
    //     $lte: new Date(year, 11, 31),
    //   }
    // }
    console.log("query", query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          unit_price: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          margin_rate: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          total_CP_price: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "vendor._id": true,
          "vendor.company_name": true
        },
      },
    ]



    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      vendorOrderData.reverse()
    }
    return Promise.resolve(vendorOrderData)
  }

  async getVendorOrderDetails(order_id: string, user_id?: string) {
    const query: { [k: string]: any } = { _id: order_id }
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "pickup_address",
        },
      },
      {
        $unwind: { path: "$pickup_address" },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_display_id",
          foreignField: "display_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "order.state_id",
          foreignField: "_id",
          as: "stateData",
        },
      },
      {
        $unwind: { path: "$stateData" },
      },
      {
        $lookup: {
          from: "city",
          localField: "order.city_id",
          foreignField: "_id",
          as: "cityData",
        },
      },
      {
        $unwind: { path: "$cityData" },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "user_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item_part",
          localField: "buyer_order_id",
          foreignField: "buyer_order_id",
          as: "orderItemPartData",
        },
      },
      // {
      //   $lookup: {
      //     from: "order_item",
      //     localField: "buyer_order_item_id",
      //     foreignField: "_id",
      //     as: "orderItem",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$orderItem",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },

      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          unit_price: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          base_amount: true,
          margin_rate: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          vendor_media: true,
          "vendor._id": true,
          "vendor.full_name": true,
          "vendor.company_name": true,
          "vendor.mobile_number": true,
          "vendor.landline_number": true,
          "vendor.email": true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          "pickup_address.state_name": "$state_name",
          "pickup_address.city_name": "$city_name",
          "pickup_address.line1": true,
          "pickup_address.line2": true,
          "pickup_address.pincode": true,
          "pickup_address.location": true,
          "delivery_address.site_id": "$order.site_id",
          "delivery_address.site_name": "$order.site_name",
          "delivery_address.address_line1": "$order.address_line1",
          "delivery_address.address_line2": "$order.address_line2",
          "delivery_address.state_id": "$order.state_id",
          "delivery_address.city_id": "$order.city_id",
          "delivery_address.pincode": "$order.pincode",
          "delivery_address.location": "$order.delivery_location",
          "delivery_address.created_at": "$order.created_at",
          "delivery_address.state_name": "$stateData.state_name",
          "delivery_address.city_name": "$cityData.city_name",
          delivery_date: true,
          end_date: true,
          orderItemPartData: true,
          //orderItem: true,
        },
      },
    ]

    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (vendorOrderData.length <= 0) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any  Suppplier order details.`,
          400
        )
      )
    }

    const orderTrackData = await OrderTrackModel.find({
      vendor_order_id: order_id,
    })
    vendorOrderData[0].orderTrackData = orderTrackData
    for (let i = 0; i < vendorOrderData[0].orderTrackData.length; i++) {
      const TMData = await TMModel.findById(
        vendorOrderData[0].orderTrackData[i].TM_id
      )

      if (!isNullOrUndefined(TMData)) {
        vendorOrderData[0].orderTrackData[i].TM_rc_number = TMData.TM_rc_number
        const TMCategoryData = await TMCategoryModel.findById(
          TMData.TM_category_id
        )

        if (!isNullOrUndefined(TMCategoryData)) {
          console.log("TMCategoryData", TMCategoryData)
          vendorOrderData[0].orderTrackData[i].category_name =
            TMCategoryData.category_name
        }
        const driverInfo = await DriverInfoModel.findById(TMData.driver1_id)
        if (!isNullOrUndefined(driverInfo)) {
          vendorOrderData[0].orderTrackData[i].driver_name =
            driverInfo.driver_name
          vendorOrderData[0].orderTrackData[i].driver_mobile_number =
            driverInfo.driver_mobile_number
        }
      }

      // Latest Changes.
      vendorOrderData[0].orderTrackData[i].TM_rc_number =
        orderTrackData[i].TM_rc_number
      vendorOrderData[0].orderTrackData[i].category_name =
        orderTrackData[i].TM_category_name
      vendorOrderData[0].orderTrackData[i].TM_driver1_name =
        orderTrackData[i].TM_driver1_name
      vendorOrderData[0].orderTrackData[i].TM_driver2_name =
        orderTrackData[i].TM_driver2_name
      vendorOrderData[0].orderTrackData[i].TM_driver1_mobile_number =
        orderTrackData[i].TM_driver1_mobile_number
      vendorOrderData[0].orderTrackData[i].TM_driver2_mobile_number =
        orderTrackData[i].TM_driver2_mobile_number
    }

    const orderItemPartQuery: { [k: string]: any } = {}
    orderItemPartQuery.buyer_order_id = vendorOrderData[0].buyer_order_id

    const orderItemPartaggregateArr = [
      { $match: orderItemPartQuery },

      {
        $lookup: {
          from: "order_track",
          localField: "_id",
          foreignField: "order_item_part_id",
          as: "TMtrack_details",
        },
      },
      {
        $lookup: {
          from: "CP_order_track",
          localField: "_id",
          foreignField: "order_item_part_id",
          as: "CPtrack_details",
        },
      },
      {
        $project: {
          _id: true,
          order_item_id: true,
          assigned_quantity: true,
          start_time: true,
          end_time: true,
          buyer_order_id: true,
          buyer_id: true,
          temp_quantity: true,
          created_at: true,
          TMtrack_details: true,
          CPtrack_details: true,
        },
      },
    ]

    const orderItemPartData = await OrderItemPartModel.aggregate(
      orderItemPartaggregateArr
    )
    vendorOrderData[0].orderItemPartData = orderItemPartData
    return Promise.resolve(vendorOrderData[0])
  }

  async getLogisticsOrder(user_id?: string, before?: string, after?: string) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "logistics_user",
          localField: "user_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: {
          path: "$logistics_user",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "product",
          localField: "product_id",
          foreignField: "_id",
          as: "product",
        },
      },
      {
        $unwind: {
          path: "$product",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "product.category_id",
          foreignField: "_id",
          as: "aggregate_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "product.sub_category_id",
          foreignField: "_id",
          as: "product_sub_category",
        },
      },
      {
        $unwind: {
          path: "$product_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          buyer_id: true,
          supplier_id: true,
          product_id: true,
          buyer_order_id: true,
          buyer_order_item_id: true,
          vehicle_id: true,
          vehicle_sub_category_id: true,
          delivery_status: true,
          quantity: true,
          pickup_quantity: true,
          royality_quantity: true,
          delivery_quantity: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          igst_amount: true,
          cgst_amount: true,
          sgst_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          bill_url: true,
          bill_uploaded_at: true,
          bill_status: true,
          bill_verified_at: true,
          logistics_payment_status: true,
          supplier_pickup_signature: true,
          driver_pickup_signature: true,
          buyer_drop_signature: true,
          driver_drop_signature: true,
          qube_test_report_7days: true,
          qube_test_report_28days: true,
          checkedout_at: true,
          pickedup_at: true,
          delivered_at: true,
          reasonForDelay: true,
          delayTime: true,
          royalty_pass_image_url: true,
          way_slip_image_url: true,
          challan_image_url: true,
          "logistics_user._id": true,
          "logistics_user.full_name": true,
          "aggregate_sand_category.category_name": true,
          "product_sub_category.sub_category_name": true,
        },
      },
    ]
    const logisticsOrderData = await LogisticsOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      logisticsOrderData.reverse()
    }
    return Promise.resolve(logisticsOrderData)
  }

  async getLogisticsOrderDetails(order_id: string) {
    const query: { [k: string]: any } = { _id: order_id }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "logistics_user",
          localField: "user_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: {
          path: "$logistics_user",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "supplier_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: {
          path: "$supplier",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "product",
          localField: "product_id",
          foreignField: "_id",
          as: "product",
        },
      },
      {
        $unwind: {
          path: "$product",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "product.category_id",
          foreignField: "_id",
          as: "aggregate_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "product.sub_category_id",
          foreignField: "_id",
          as: "product_sub_category",
        },
      },
      {
        $unwind: {
          path: "$product_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vehicle",
          localField: "vehicle_id",
          foreignField: "_id",
          as: "vehicle",
        },
      },
      {
        $unwind: {
          path: "$vehicle",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vehicle_sub_category",
          localField: "vehicle.vehicle_sub_category_id",
          foreignField: "_id",
          as: "vehicle_sub_category",
        },
      },
      {
        $unwind: {
          path: "$vehicle_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "product.pickup_address_id",
          foreignField: "_id",
          as: "pickup_address",
        },
      },
      {
        $unwind: { path: "$pickup_address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "pickup_address.state_id",
          foreignField: "_id",
          as: "pickup_address_state",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_state",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address.city_id",
          foreignField: "_id",
          as: "pickup_address_city",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "orderDetails",
        },
      },
      {
        $unwind: {
          path: "$orderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "orderDetails.site_id",
          foreignField: "_id",
          as: "delivery_address",
        },
      },
      {
        $unwind: {
          path: "$delivery_address",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "delivery_address.state_id",
          foreignField: "_id",
          as: "delivery_address_state",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_state",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address.city_id",
          foreignField: "_id",
          as: "delivery_address_city",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "buyer_order_item_id",
          foreignField: "_id",
          as: "orderItem",
        },
      },
      {
        $lookup: {
          from: "quote",
          localField: "quote_id",
          foreignField: "_id",
          as: "quoteDetails",
        },
      },
      {
        $unwind: {
          path: "$quoteDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          buyer_id: true,
          supplier_id: true,
          product_id: true,
          buyer_order_id: true,
          buyer_order_item_id: true,
          vehicle_id: true,
          vehicle_sub_category_id: true,
          delivery_status: true,
          quantity: true,
          pickup_quantity: true,
          royality_quantity: true,
          delivery_quantity: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          igst_amount: true,
          cgst_amount: true,
          sgst_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          bill_url: true,
          bill_uploaded_at: true,
          bill_status: true,
          bill_verified_at: true,
          logistics_payment_status: true,
          supplier_pickup_signature: true,
          driver_pickup_signature: true,
          buyer_drop_signature: true,
          driver_drop_signature: true,
          qube_test_report: true,
          checkedout_at: true,
          pickedup_at: true,
          delivered_at: true,
          reasonForDelay: true,
          delayTime: true,
          royalty_pass_image_url: true,
          way_slip_image_url: true,
          challan_image_url: true,
          "logistics_user._id": true,
          "logistics_user.full_name": true,
          "supplier._id": true,
          "supplier.full_name": true,
          "supplier.mobile_number": true,
          "supplier.email": true,
          "buyer._id": true,
          "buyer.full_name": true,
          "pickup_address.line1": 1,
          "pickup_address.line2": 1,
          "pickup_address.state_id": 1,
          "pickup_address.city_id": 1,
          "pickup_address.sub_city_id": 1,
          "pickup_address.pincode": 1,
          "pickup_address.location": 1,
          "pickup_address_state.state_name": 1,
          "pickup_address_city.city_name": 1,
          "delivery_address.site_name": 1,
          "delivery_address.address_line1": 1,
          "delivery_address.address_line2": 1,
          "delivery_address.country_id": 1,
          "delivery_address.state_id": 1,
          "delivery_address.city_id": 1,
          "delivery_address.pincode": 1,
          "delivery_address.location": 1,
          "delivery_address_state.state_name": 1,
          "delivery_address_city.city_name": 1,
          "vehicle._id": 1,
          "vehicle.vehicle_rc_number": 1,
          "vehicle_sub_category._id": 1,
          "vehicle_sub_category.sub_category_name": 1,
          "product._id": true,
          "product.name": true,
          "aggregate_sand_category._id": true,
          "aggregate_sand_category.category_name": true,
          "aggregate_sand_category.image_url": true,
          "aggregate_sand_category.thumbnail_url": true,
          "product_sub_category._id": true,
          "product_sub_category.sub_category_name": true,
          "product_sub_category.image_url": true,
          "product_sub_category.thumbnail_url": true,
          orderItem: true,
          "quoteDetails._id": true,
          "quoteDetails.quoted_amount": true,
        },
      },
    ]
    const orderData = await LogisticsOrderModel.aggregate(aggregateArr)
    if (orderData.length <= 0) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details`,
          400
        )
      )
    }

    const orderTrackData = await OrderTrackModel.find({
      logistics_order_id: order_id,
    })
    orderData[0].orderTrackData = orderTrackData

    for (let i = 0; i < orderData[0].orderTrackData.length; i++) {
      const vehicleData = await VehicleModel.findById(
        orderData[0].orderTrackData[i].vehicle_id
      )

      if (!isNullOrUndefined(vehicleData)) {
        orderData[0].orderTrackData[i].vehicle_rc_number =
          vehicleData.vehicle_rc_number

        const vehicleCategoryData = await VehicleCategoryeModel.findById(
          vehicleData.vehicle_category_id
        )

        if (!isNullOrUndefined(vehicleCategoryData)) {
          console.log("vehicleCategoryData", vehicleCategoryData)
          orderData[0].orderTrackData[i].category_name =
            vehicleCategoryData.category_name
        }
        const driverInfo = await DriverInfoModel.findById(
          vehicleData.driver1_id
        )
        if (!isNullOrUndefined(driverInfo)) {
          orderData[0].orderTrackData[i].driver_name = driverInfo.driver_name
          orderData[0].orderTrackData[i].driver_mobile_number =
            driverInfo.driver_mobile_number
        }
      }

      orderData[0].orderTrackData[i].TM_rc_number =
        orderTrackData[i].TM_rc_number
      orderData[0].orderTrackData[i].category_name =
        orderTrackData[i].TM_category_name
      orderData[0].orderTrackData[i].TM_driver1_name =
        orderTrackData[i].TM_driver1_name
      orderData[0].orderTrackData[i].TM_driver2_name =
        orderTrackData[i].TM_driver2_name
      orderData[0].orderTrackData[i].TM_driver1_mobile_number =
        orderTrackData[i].TM_driver1_mobile_number
      orderData[0].orderTrackData[i].TM_driver2_mobile_number =
        orderTrackData[i].TM_driver2_mobile_number
    }
    return Promise.resolve(orderData[0])
  }

  async updateSupplierOrderBillStatus(
    order_id: string,
    bill_id: string,
    bill_status: string,
    bill_reject_reason: string
  ) {
    const orderData = await VendorOrderModel.findOne({
      _id: order_id,
      bills: { $exists: true },
      "bills._id": new ObjectId(bill_id),
    })

    if (!isNullOrUndefined(orderData)) {
      if (bill_status === BillStatus.ACCEPTED) {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "bills._id": new ObjectId(bill_id),
          },
          {
            $set: {
              "bills.$.status": bill_status,
              "bills.$.verified_at": Date.now(),
            },
          }
        )
      } else {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "bills._id": new ObjectId(bill_id),
          },
          {
            $set: {
              "bills.$.status": bill_status,
              "bills.$.reject_reason": bill_reject_reason,
            },
          }
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }

  async updateSupplierCreditNoteStatus(
    order_id: string,
    credit_note_id: string,
    credit_note_status: string,
    credit_note_reject_reason: string
  ) {
    const orderData = await VendorOrderModel.findOne({
      _id: order_id,
      creditNotes: { $exists: true },
      "creditNotes._id": new ObjectId(credit_note_id),
    })

    if (!isNullOrUndefined(orderData)) {
      if (credit_note_status === BillStatus.ACCEPTED) {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "creditNotes._id": new ObjectId(credit_note_id),
          },
          {
            $set: {
              "creditNotes.$.status": credit_note_status,
              "creditNotes.$.verified_at": Date.now(),
            },
          }
        )
      } else {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "creditNotes._id": new ObjectId(credit_note_id),
          },
          {
            $set: {
              "creditNotes.$.status": credit_note_status,
              "creditNotes.$.reject_reason": credit_note_reject_reason,
            },
          }
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }

  async updateSupplierDebitNoteStatus(
    order_id: string,
    debit_note_id: string,
    debit_note_status: string,
    debit_note_reject_reason: string
  ) {
    const orderData = await VendorOrderModel.findOne({
      _id: order_id,
      debitNotes: { $exists: true },
      "debitNotes._id": new ObjectId(debit_note_id),
    })
    let m_vendor_company_name: any
    let m_vendor_mobile_number: any
    if (!isNullOrUndefined(orderData)) {
      let m_vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(orderData.master_vendor_id),
      })
      if (!isNullOrUndefined(m_vendorData)) {
        m_vendor_company_name = m_vendorData.company_name
        m_vendor_mobile_number = m_vendorData.mobile_number
      }
    }

    let s_vendor_company_name: any
    let s_vendor_mobile_number: any
    if (!isNullOrUndefined(orderData)) {
      let s_vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(orderData.sub_vendor_id),
      })
      if (!isNullOrUndefined(s_vendorData)) {
        s_vendor_company_name = s_vendorData.company_name
        s_vendor_mobile_number = s_vendorData.mobile_number
      }
    }

    if (!isNullOrUndefined(orderData)) {
      if (debit_note_status === BillStatus.ACCEPTED) {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "debitNotes._id": new ObjectId(debit_note_id),
          },
          {
            $set: {
              "debitNotes.$.status": debit_note_status,
              "debitNotes.$.verified_at": Date.now(),
            },
          }
        )

        let m_message = `Hi ${m_vendor_company_name}, your uploaded debit note for product id ${orderData.buyer_order_display_item_id} having an order id ${orderData._id} has been verified and approved by Conmix. - conmix`
        let m_templateId = "1707163842346650901"
        await this.testService.sendSMS(
          m_vendor_mobile_number,
          m_message,
          m_templateId
        )

        let s_message = `Hi ${s_vendor_company_name}, your uploaded debit note for product id ${orderData.buyer_order_display_item_id} having an order id ${orderData._id} has been verified and approved by Conmix. - conmix`
        let s_templateId = "1707163842346650901"
        await this.testService.sendSMS(
          s_vendor_mobile_number,
          s_message,
          s_templateId
        )

        // Send notofication to Supplier.
        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: orderData.master_vendor_id,
          notification_type: VendorNotificationType.DebitNoteAccept,
          vendor_order_id: order_id,
          vendor_id: orderData.master_vendor_id,
          order_id: orderData.buyer_order_id,
          order_item_id: orderData.buyer_order_display_item_id,
          client_id: orderData.buyer_id,
        })

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: orderData.sub_vendor_id,
          notification_type: VendorNotificationType.DebitNoteAccept,
          vendor_order_id: order_id,
          vendor_id: orderData.sub_vendor_id,
          order_id: orderData.buyer_order_id,
          order_item_id: orderData.buyer_order_display_item_id,
          client_id: orderData.buyer_id,
        })
      } else {
        await VendorOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "debitNotes._id": new ObjectId(debit_note_id),
          },
          {
            $set: {
              "debitNotes.$.status": debit_note_status,
              "debitNotes.$.reject_reason": debit_note_reject_reason,
            },
          }
        )

        let m_message = `Hi ${m_vendor_company_name}, your uploaded debit note for product id ${orderData.buyer_order_display_item_id} having an order id ${orderData._id} has been rejected by Conmix. Kindly upload the correct debit note for the order. - conmix`
        let m_templateId = "1707163842358215382"
        await this.testService.sendSMS(
          m_vendor_mobile_number,
          m_message,
          m_templateId
        )

        let s_message = `Hi ${s_vendor_company_name}, your uploaded debit note for product id ${orderData.buyer_order_display_item_id} having an order id ${orderData._id} has been rejected by Conmix. Kindly upload the correct debit note for the order. - conmix`
        let s_templateId = "1707163842358215382"
        await this.testService.sendSMS(
          s_vendor_mobile_number,
          s_message,
          s_templateId
        )

        // Send notofication to Supplier.
        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: orderData.master_vendor_id,
          notification_type: VendorNotificationType.DebitNoteReject,
          vendor_order_id: order_id,
          vendor_id: orderData.master_vendor_id,
          order_id: orderData.buyer_order_id,
          order_item_id: orderData.buyer_order_display_item_id,
          client_id: orderData.buyer_id,
        })

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: orderData.sub_vendor_id,
          notification_type: VendorNotificationType.DebitNoteReject,
          vendor_order_id: order_id,
          vendor_id: orderData.sub_vendor_id,
          order_id: orderData.buyer_order_id,
          order_item_id: orderData.buyer_order_display_item_id,
          client_id: orderData.buyer_id,
        })
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }

  async updateSupplierOrderStatus(
    supplier_order_id: string,
    order_status: string
  ) {
    const supplierOrderData = await VendorOrderModel.findOne({
      _id: supplier_order_id,
    })

    if (isNullOrUndefined(supplierOrderData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find Supplier order for the ${supplier_order_id}.`,
          400
        )
      )
    }

    await VendorOrderModel.findOneAndUpdate(
      {
        _id: supplier_order_id,
      },
      {
        $set: {
          order_status,
        },
      }
    )

    await OrderItemModel.findOneAndUpdate(
      {
        _id: new ObjectId(supplierOrderData.buyer_order_item_id),
      },
      {
        $set: {
          item_status: order_status,
        },
      }
    )

    return Promise.resolve()
  }

  async updateLogisticsOrderBillStatus(
    order_id: string,
    bill_id: string,
    bill_status: string,
    bill_reject_reason: string
  ) {
    const orderData = await LogisticsOrderModel.findOne({
      _id: order_id,
      bills: { $exists: true },
      "bills._id": new ObjectId(bill_id),
    })

    if (!isNullOrUndefined(orderData)) {
      if (bill_status === BillStatus.ACCEPTED) {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "bills._id": new ObjectId(bill_id),
          },
          {
            $set: {
              "bills.$.status": bill_status,
              "bills.$.verified_at": Date.now(),
            },
          }
        )
      } else {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "bills._id": new ObjectId(bill_id),
          },
          {
            $set: {
              "bills.$.status": bill_status,
              "bills.$.reject_reason": bill_reject_reason,
            },
          }
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }

  async updateLogisticsCreditNoteStatus(
    order_id: string,
    credit_note_id: string,
    credit_note_status: string,
    credit_note_reject_reason: string
  ) {
    const orderData = await LogisticsOrderModel.findOne({
      _id: order_id,
      creditNotes: { $exists: true },
      "creditNotes._id": new ObjectId(credit_note_id),
    })

    if (!isNullOrUndefined(orderData)) {
      if (credit_note_status === BillStatus.ACCEPTED) {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "creditNotes._id": new ObjectId(credit_note_id),
          },
          {
            $set: {
              "creditNotes.$.status": credit_note_status,
              "creditNotes.$.verified_at": Date.now(),
            },
          }
        )
      } else {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "creditNotes._id": new ObjectId(credit_note_id),
          },
          {
            $set: {
              "creditNotes.$.status": credit_note_status,
              "creditNotes.$.reject_reason": credit_note_reject_reason,
            },
          }
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }

  async updateLogisticsDebitNoteStatus(
    order_id: string,
    debit_note_id: string,
    debit_note_status: string,
    debit_note_reject_reason: string
  ) {
    const orderData = await LogisticsOrderModel.findOne({
      _id: order_id,
      debitNotes: { $exists: true },
      "debitNotes._id": new ObjectId(debit_note_id),
    })

    if (!isNullOrUndefined(orderData)) {
      if (debit_note_status === BillStatus.ACCEPTED) {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "debitNotes._id": new ObjectId(debit_note_id),
          },
          {
            $set: {
              "debitNotes.$.status": debit_note_status,
              "debitNotes.$.verified_at": Date.now(),
            },
          }
        )
      } else {
        await LogisticsOrderModel.findOneAndUpdate(
          {
            _id: order_id,
            "debitNotes._id": new ObjectId(debit_note_id),
          },
          {
            $set: {
              "debitNotes.$.status": debit_note_status,
              "debitNotes.$.reject_reason": debit_note_reject_reason,
            },
          }
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details or bill is not attched with this order.`,
          400
        )
      )
    }
  }
}
