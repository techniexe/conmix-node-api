import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addEditSmsTemplateSchema,
  IAddEditSmsTemplateReq,
  IGetSmsTemplateReq,
  getSmsTemplateSchema,
} from "./sms.template.req-schema"
import { SmsTemplateRepository } from "./sms.template.repository"

@injectable()
@controller("/sms-template", verifyCustomToken("admin"))
export class SmsTemplateController {
  constructor(
    @inject(AdminTypes.SmsTemplateRepository)
    private smsTempRepo: SmsTemplateRepository
  ) {}
  @httpPost("/:templateType", validate(addEditSmsTemplateSchema))
  @httpPatch("/:templateType", validate(addEditSmsTemplateSchema))
  async addSmsEmailTemplate(
    req: IAddEditSmsTemplateReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.smsTempRepo.modifySmsTemplate(
        req.params.templateType,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/", validate(getSmsTemplateSchema))
  async getSmsTemplateList(
    req: IGetSmsTemplateReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { before, after, template_type, sms_language, encoding } = req.query
      const data = await this.smsTempRepo.getSmsTemplates(
        before,
        after,
        template_type,
        sms_language,
        encoding
      )
      res.json({ data })
    } catch (err) {
      return next(err)
    }
  }
}
