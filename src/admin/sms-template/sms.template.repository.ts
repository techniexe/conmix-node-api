import { injectable } from "inversify"
import { ISmsTemplate } from "./sms.template.req-schema"
import { SmsTemplateModel } from "../../model/sms.template.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class SmsTemplateRepository {
  async modifySmsTemplate(
    template_type: string,
    body: ISmsTemplate
  ): Promise<any> {
    const arrFields = ["text", "encoding", "sms_language"]

    const updated_at = Date.now()
    const updt: {
      $set: { [k: string]: any }
    } = {
      $set: { updated_at },
    }
    for (const [key, val] of Object.entries(body)) {
      if (arrFields.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    return SmsTemplateModel.updateOne({ template_type }, updt, {
      upsert: true,
    })
  }

  async getSmsTemplates(
    before?: string,
    after?: string,
    template_type?: string,
    sms_language?: string,
    encoding?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(template_type)) {
      query.template_type = template_type
    }
    if (!isNullOrUndefined(sms_language)) {
      query.sms_language = sms_language
    }
    if (!isNullOrUndefined(encoding)) {
      query.encoding = encoding
    }
    return SmsTemplateModel.find(query)
      .sort({ created_at: -1 })
      .limit(40)
  }
}
