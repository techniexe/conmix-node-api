import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"
import {
  SmsTemplateTypes,
  SmsLanguage,
  SmsEncodings,
} from "../../utilities/config"

export interface ISmsTemplate {
  text: string
  encoding: string
  sms_language: string
}

export interface IAddEditSmsTemplateReq extends IAuthenticatedRequest {
  body: ISmsTemplate
}

export const addEditSmsTemplateSchema: IRequestSchema = {
  params: Joi.object().keys({
    templateType: Joi.string().valid(SmsTemplateTypes),
  }),
  body: Joi.object().keys({
    text: Joi.string().required(),
    encoding: Joi.string()
      .valid(SmsEncodings)
      .required(),
    sms_language: Joi.string()
      .valid(SmsLanguage)
      .required(),
  }),
}

export interface IGetSmsTemplateReq extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
    template_type?: string
    sms_language?: string
    encoding?: string
  }
}

export const getSmsTemplateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    template_type: Joi.string(),
    sms_language: Joi.string(),
    encoding: Joi.string(),
  }),
}
