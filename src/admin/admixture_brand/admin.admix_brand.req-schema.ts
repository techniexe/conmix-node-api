import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { UploadedFile } from "express-fileupload"

export interface IAddAdmixBrand {
  category_name: string
  image_url?: string
  thumbnail_url?: string
  [k: string]: any
}

export interface IAddAdmixBrandRequest extends IAuthenticatedRequest {
  body: IAddAdmixBrand
  files: {
    image: UploadedFile
  }
}

export const addAdmixBrandSchema: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150).required(),
  }),
}

export interface IEditAdmixBrand {
  name?: string
  image_url?: string
  thumbnail_url?: string
  authentication_code: string
}

export interface IEditAdmixBrandRequest extends IAuthenticatedRequest {
  body: IEditAdmixBrand
  files: {
    image: UploadedFile
  }
}

export const editAdmixBrandSchema: IRequestSchema = {
  params: Joi.object().keys({
    admixBrandId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(150),
    authentication_code: Joi.string().required(),
  }),
}

export interface IDeleteAdmixBrandRequest extends IAuthenticatedRequest {
  params: {
    admixBrandId: string
  }
  body: {
    authentication_code: string
  }
}

export const deleteAdmixBrandSchema: IRequestSchema = {
  params: Joi.object().keys({
    admixBrandId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}
