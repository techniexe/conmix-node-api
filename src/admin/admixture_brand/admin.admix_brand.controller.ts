import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"

import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"

import { AdminAdmixBrandRepository } from "./admin.admix_brand.repository"
import {
  addAdmixBrandSchema,
  IAddAdmixBrandRequest,
  editAdmixBrandSchema,
  IEditAdmixBrandRequest,
  deleteAdmixBrandSchema,
  IDeleteAdmixBrandRequest,
} from "./admin.admix_brand.req-schema"

@controller("/admixture_brand", verifyCustomToken("admin"))
@injectable()
export class AdminAdmixBrandController {
  constructor(
    @inject(AdminTypes.AdminAdmixBrandRepository)
    private adMixBrandRepo: AdminAdmixBrandRepository
  ) {}
  @httpPost("/", validate(addAdmixBrandSchema))
  async addAdmixBrand(
    req: IAddAdmixBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = await this.adMixBrandRepo.addAdmixBrand(
        req.body,
        req.user.uid,
        req.files
      )
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:admixBrandId", validate(editAdmixBrandSchema))
  async editAdmixBrand(
    req: IEditAdmixBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adMixBrandRepo.editAdmixBrand(
        req.user.uid,
        req.params.admixBrandId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getAdmixBrand(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.adMixBrandRepo.getAdmixBrand(search, before, after)
    res.json({ data })
  }

  @httpDelete("/:admixBrandId", validate(deleteAdmixBrandSchema))
  async deleteAdmixBrand(
    req: IDeleteAdmixBrandRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adMixBrandRepo.deleteAdmixBrand(
        req.user.uid,
        req.params.admixBrandId,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
