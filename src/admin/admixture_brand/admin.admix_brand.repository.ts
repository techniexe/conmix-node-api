import { injectable, inject } from "inversify"
import { UploadedFile } from "express-fileupload"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  putObject,
  getS3MediaURL,
  getS3DynamicCdnURL,
} from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AuthModel } from "../../model/auth.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { IAddAdmixBrand, IEditAdmixBrand } from "./admin.admix_brand.req-schema"

@injectable()
export class AdminAdmixBrandRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addAdmixBrand(
    admixBrandData: IAddAdmixBrand,
    created_by: string,
    fileData: { image: UploadedFile }
  ) {
    const admixBrandCheck = await AdmixtureBrandModel.findOne({
      name: admixBrandData.name,
    })
    if (admixBrandCheck !== null) {
      const err = new UnexpectedInput(
        `The entered Admix Brand name"${admixBrandData.name}" already added `
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    admixBrandData._id = new ObjectId()
    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/admixture_brand`
      let objectName = "" + admixBrandData._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      admixBrandData.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      admixBrandData.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
    }

    admixBrandData.created_by = created_by

    const newAdmixBrand = new AdmixtureBrandModel(admixBrandData)
    const newAdmixBrandDoc = await newAdmixBrand.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_ADMIXTURE_BRAND,
      `Admin added admix_brand of id ${newAdmixBrandDoc._id} .`
    )

    return Promise.resolve(newAdmixBrandDoc)
  }

  async editAdmixBrand(
    user_id: string,
    admixBrandId: string,
    admixBrandData: IEditAdmixBrand,
    fileData: { image: UploadedFile }
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: admixBrandData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const admixBrandCheck = await AdmixtureBrandModel.findOne({
      name: admixBrandData.name,
    })
 
    if (
      !isNullOrUndefined(admixBrandCheck) &&
      admixBrandCheck._id.equals(admixBrandId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`The entered Admix Brand name"${admixBrandData.name}" already added `, 400)
      )
    }

    const flds = ["name"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(admixBrandData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/admixture_brand`
      let objectName = `${admixBrandId}` + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      editDoc.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      editDoc.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
      editDoc.thumbnail_low_url = getS3DynamicCdnURL(
        `${objectDir}/w_100,q_10/${objectName}`
      )
    }

    const res = await AdmixtureBrandModel.findOneAndUpdate(
      { _id: admixBrandId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_ADMIXTURE_BRAND,
      `Admin Updated details of cement brand id ${admixBrandId}.`,
      res,
      editDoc
    )

    return Promise.resolve()
  }

  async getAdmixBrand(search?: string, before?: string, after?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return AdmixtureBrandModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async deleteAdmixBrand(
    user_id: string,
    admixBrandId: string,
    authentication_code: string
  ) {
    const res = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updtRes = await AdmixtureBrandModel.updateOne(
      {
        _id: new ObjectId(admixBrandId),
        created_by: user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
          deleted_by: user_id,
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.REMOVE_ADMIXTURE_BRAND_BY_ADMIN,
      `Admin deleted concreted grade of id ${admixBrandId} .`
    )

    return Promise.resolve()
  }
}
