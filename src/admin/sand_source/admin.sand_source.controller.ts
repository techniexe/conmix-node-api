import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminTypes } from "../admin.types"
import { UnexpectedInput } from "../../utilities/customError"
import { AdminSandSourceRepository } from "./admin.sand_source.repository"
import {
  IAddSourceRequest,
  updateSourceSchema,
  IUpdateSourceRequest,
  removeSourceSchema,
  IRemoveSourceRequest,
  addSourceSchema,
} from "./admin.sand_source.req-schema"

@injectable()
@controller("/sand_source", verifyCustomToken("admin"))
export class AdminSandSourceController {
  constructor(
    @inject(AdminTypes.AdminSandSourceRepository)
    private sandsourceRepo: AdminSandSourceRepository
  ) {}
  @httpPost("/", validate(addSourceSchema))
  async addSource(req: IAddSourceRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.sandsourceRepo.addSource(
        req.body,
        req.user.uid
      )
      res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `Same sand source name registered with this region.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpPatch("/:source_id", validate(updateSourceSchema))
  async updateSource(
    req: IUpdateSourceRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.sandsourceRepo.updateSource(
        req.user.uid,
        req.params.source_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `Same sand source name registered with this region.`
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpDelete("/:source_id", validate(removeSourceSchema))
  async removeSource(
    req: IRemoveSourceRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.sandsourceRepo.removeSource(
        req.user.uid,
        req.params.source_id,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async sourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sandsourceRepo.sourceList(req.user.uid, req.query)
    res.json({ data })
  }
}
