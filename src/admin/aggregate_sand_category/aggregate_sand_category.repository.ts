import {
  ICategory,
  ISubCategory,
  IEditCategory,
  IEditSubCategory,
} from "./aggregate_sand_category.req-schema"
import { injectable, inject } from "inversify"
import {
  AggregateSandCategoryModel,
  IAggregateSandCategoryDoc,
  AggregateSandSubCategoryModel,
  IAggregateSandSubCategoryDoc,
} from "../../model/aggregate_sand_category.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AuthModel } from "../../model/auth.model"
import { UploadedFile } from "express-fileupload"
import {
  putObject,
  getS3MediaURL,
  getS3DynamicCdnURL,
} from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { ObjectId } from "bson"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
@injectable()
export class AggregateSandCategoryRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addCategory(
    catData: ICategory,
    created_by: string,
    fileData: { image: UploadedFile }
  ): Promise<IAggregateSandCategoryDoc> {
    const catCheck = await AggregateSandCategoryModel.findOne({
      category_name: catData.category_name,
    })
    if (catCheck !== null) {
      const err = new UnexpectedInput(
        `The entered category name "${catData.category_name}" already added `
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    catData._id = new ObjectId()
    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/aggregate_sand-category`
      let objectName = "" + catData._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      catData.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      catData.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
    }

    catData.created_by = created_by

    const newCat = new AggregateSandCategoryModel(catData)
    const newCatDoc = await newCat.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_AGGREGATE_SAND_CATEGORY,
      `Admin added aggregate_sand_category of id ${newCatDoc._id} .`
    )

    return Promise.resolve(newCatDoc)
  }

  async editCategory(
    user_id: string,
    categoryId: string,
    catData: IEditCategory,
    fileData: { image: UploadedFile }
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: catData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const catCheck = await AggregateSandCategoryModel.findOne({
      category_name: catData.category_name,
    })

    if (
      !isNullOrUndefined(catCheck) &&
      catCheck._id.equals(categoryId) === false
    ) {
      return Promise.reject(
        new InvalidInput(
          `The entered category name "${catData.category_name}" already added `,
          400
        )
      )
    }

    const flds = ["category_name", "sequence"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(catData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/aggregate_sand-category`
      let objectName = `${categoryId}` + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      editDoc.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      editDoc.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
      editDoc.thumbnail_low_url = getS3DynamicCdnURL(
        `${objectDir}/w_100,q_10/${objectName}`
      )
    }

    const res = await AggregateSandCategoryModel.findOneAndUpdate(
      { _id: categoryId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_AGGREGATE_SAND_CATEGORY,
      `Admin Updated details of Aggregate sand category id ${categoryId}.`,
      res,
      editDoc
    )
    await AuthModel.deleteOne({
      user_id,
      code: catData.authentication_code,
    })
    return Promise.resolve()
  }
  async addSubCategory(
    subCatData: ISubCategory,
    category_id: string,
    created_by: string,
    fileData: { image: UploadedFile }
  ) {
    subCatData._id = new ObjectId()
    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/aggregate_sand-sub_category`
      let objectName = "" + subCatData._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      subCatData.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      subCatData.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
    }
    subCatData.category_id = category_id
    subCatData.created_by = created_by

    const newsubCat = new AggregateSandSubCategoryModel(subCatData)
    const newsubCatDoc = await newsubCat.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_AGGREGATE_SAND_SUB_CATEGORY,
      `Admin added aggregate_sand sub category of id ${newsubCatDoc._id} .`
    )

    return Promise.resolve(newsubCatDoc)
  }

  async editSubCategory(
    user_id: string,
    subCatId: string,
    subCatData: IEditSubCategory,
    fileData: { image: UploadedFile }
  ) {
    const result = await AuthModel.findOne({
      code: subCatData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const flds = [
      "sub_category_name",
      "sequence",
      "image_url",
      "thumbnail_url",
      "margin_rate_id",
      "min_quantity",
      "selling_unit",
      "gst_slab_id",
    ]
    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(subCatData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData.image) &&
      !isNullOrUndefined(fileData.image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/aggregate_sand-sub_category`
      let objectName = `${subCatId}` + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.image.data,
        fileData.image.mimetype
      )
      editDoc.image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      editDoc.thumbnail_url = getS3DynamicCdnURL(
        `${objectDir}/w_400/${objectName}`
      )
      editDoc.thumbnail_low_url = getS3DynamicCdnURL(
        `${objectDir}/w_100,q_10/${objectName}`
      )
    }

    const res = await AggregateSandSubCategoryModel.findOneAndUpdate(
      { _id: subCatId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this subcategory`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_AGGREGATE_SAND_SUB_CATEGORY,
      `Admin Updated details of aggregate_sand category id ${subCatId}.`,
      res,
      editDoc
    )

    await AuthModel.deleteOne({
      user_id,
      code: subCatData.authentication_code,
    })

    return Promise.resolve()
  }
  async getCategories(
    search?: string,
    before?: string,
    after?: string
  ): Promise<IAggregateSandCategoryDoc[]> {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return AggregateSandCategoryModel.find(query)
      .sort({ created_at: -1 })
      .limit(10)
  }

  async getSubCategories(
    category_id: string,
    search?: string,
    before?: string,
    after?: string,
    sub_category_name?: string,
    margin_rate_id?: any
  ): Promise<IAggregateSandSubCategoryDoc[]> {
    let query: { [k: string]: any } = { category_id: new ObjectId(category_id) }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(sub_category_name) && sub_category_name !== "") {
      query.sub_category_name = sub_category_name
    }
    if (!isNullOrUndefined(margin_rate_id)) {
      query.margin_rate_id = new ObjectId(margin_rate_id)
    }
    console.log(query)
    return AggregateSandSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $unwind: {
          path: "$gst_slab",
        },
      },

      {
        $lookup: {
          from: "margin_rate",
          localField: "margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: {
          path: "$margin_rate",
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "aggregate_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_category",
        },
      },
      {
        $project: {
          sub_category_name: 1,
          category_id: 1,
          "aggregate_sand_category._id": 1,
          "aggregate_sand_category.category_name": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          sequence: 1,
          min_quantity: 1,
          selling_unit: 1,
          image_url: 1,
          thumbnail_url: 1,
          created_at: 1,
          created_by: 1,
        },
      },
    ])

    // return ProductSubCategoryModel.find(query)
    //   .sort({ sequence: 1 })
    //   .limit(40)
  }
}
