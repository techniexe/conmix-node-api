import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import {
  ICategoryRequest,
  ISubCategoryRequest,
  categorySchema,
  subCategorySchema,
  getSubCategoriesSchema,
  editCategorySchema,
  IEditCategoryRequest,
  editSubCategorySchema,
  IEditSubCategoryRequest,
} from "./aggregate_sand_category.req-schema"
import { NextFunction, Response } from "express"
import { AdminTypes } from "../admin.types"
import { AggregateSandCategoryRepository } from "./aggregate_sand_category.repository"
import {
  IAuthenticatedRequest,
  verifyCustomToken,
} from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/aggregate-sand-category", verifyCustomToken("admin"))
export class AggregateSandCategoryController {
  constructor(
    @inject(AdminTypes.AggregateSandCategoryRepository)
    private categoryRepo: AggregateSandCategoryRepository
  ) {}
  @httpPost("/", validate(categorySchema))
  async addCategory(req: ICategoryRequest, res: Response, next: NextFunction) {
    try {
      console.log(req)
      const { _id } = await this.categoryRepo.addCategory(
        req.body,
        req.user.uid,
        req.files
      )
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:categoryId", validate(editCategorySchema))
  async editCategory(
    req: IEditCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.categoryRepo.editCategory(
        req.user.uid,
        req.params.categoryId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/subcategory/:categoryId", validate(subCategorySchema))
  async addSubCategories(
    req: ISubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { _id } = await this.categoryRepo.addSubCategory(
        req.body,
        req.params.categoryId,
        req.user.uid,
        req.files
      )
      res.json({ data: { _id } })
    } catch (err) {
      const err1 = new UnexpectedInput(err.message)
      err1.httpStatusCode = 400
      next(err1)
    }
  }
  @httpPatch("/subcategory/:subCategoryId", validate(editSubCategorySchema))
  async editSubCategory(
    req: IEditSubCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.categoryRepo.editSubCategory(
        req.user.uid,
        req.params.subCategoryId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after } = req.query
    const data = await this.categoryRepo.getCategories(search, before, after)
    res.json({ data })
  }
  @httpGet("/subcategory/:categoryId", validate(getSubCategoriesSchema))
  async getSubCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, sub_category_name, margin_rate_id } = req.query
    const data = await this.categoryRepo.getSubCategories(
      req.params.categoryId,
      search,
      before,
      after,
      sub_category_name,
      margin_rate_id
    )
    res.json({ data })
  }
}
