import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetAccessLogsReq extends IAuthenticatedRequest {
  query: {
    access_type?: string
    user_type?: string
    search?: string
    before?: string
    after?: string
    user_id?: string
    start_date?: string
    end_date?: string
  }
}

export const getAccessLogsSchema: IRequestSchema = {
  query: Joi.object().keys({
    access_type: Joi.string(),
    user_type: Joi.string(),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    user_id: Joi.string().regex(objectIdRegex),
    start_date: Joi.string(),
    end_date: Joi.string(),
    
  }),
}

export interface IGetEmailAccessLogsReq extends IAuthenticatedRequest {
  query: {
    before?: string
    after?: string
    user_id?: string
    from_email?: string
    to_email?: string
    subject?: string
  }
}

export const getEmailAccessLogsSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    user_id: Joi.string().regex(objectIdRegex),
    from_email: Joi.string(),
    to_email: Joi.string(),
    subject: Joi.string(),
  }),
}
