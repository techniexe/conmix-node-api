import { controller, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  getAccessLogsSchema,
  IGetAccessLogsReq,
  IGetEmailAccessLogsReq,
  getEmailAccessLogsSchema,
} from "./access-logs.req-schema"
import { AccessLogsRepository } from "./access-logs.repository"

@injectable()
@controller("/access-logs", verifyCustomToken("admin"))
export class AccessLogsController {
  constructor(
    @inject(AdminTypes.AccessLogsRepository)
    private accessLogsRepo: AccessLogsRepository
  ) {}

  @httpGet("/", validate(getAccessLogsSchema))
  async getAccessLogs(
    req: IGetAccessLogsReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        access_type,
        user_type,
        search,
        user_id,
        before,
        after,
        start_date,
        end_date,
      } = req.query
      const data = await this.accessLogsRepo.getAccessLogs(
        access_type,
        user_type,
        search,
        user_id,
        before,
        after,
        start_date,
        end_date
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/email", validate(getEmailAccessLogsSchema))
  async getEmailAccessLogs(
    req: IGetEmailAccessLogsReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        user_id,
        from_email,
        to_email,
        subject,
        before,
        after,
      } = req.query
      const data = await this.accessLogsRepo.getEmailAccessLogs(
        user_id,
        from_email,
        to_email,
        subject,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
