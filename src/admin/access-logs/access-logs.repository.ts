import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { AccessLogsModel } from "../../model/access_logs.model"
import { EmailLogModel } from "../../model/email.log.model"

@injectable()
export class AccessLogsRepository {
  async getAccessLogs(
    access_type?: string,
    user_type?: string,
    search?: string,
    user_id?: string,
    before?: string,
    after?: string,
    start_date?: string,
    end_date?: string,
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(access_type)) {
      query.access_type = access_type
    }
    if (!isNullOrUndefined(user_type)) {
      query.user_type = user_type
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(user_id)) {
      query.user_id = user_id
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.log_added_at = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.log_added_at = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.created_at = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    return AccessLogsModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getEmailAccessLogs(
    user_id?: string,
    from_email?: string,
    to_email?: string,
    subject?: string,
    before?: string,
    after?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = user_id
    }
    if (!isNullOrUndefined(from_email)) {
      query.from_email = from_email
    }
    if (!isNullOrUndefined(to_email)) {
      query.to_email = to_email
    }
    if (!isNullOrUndefined(subject)) {
      query.subject = subject
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return EmailLogModel.find(query).sort({ created_at: -1 }).limit(10)
  }
}
