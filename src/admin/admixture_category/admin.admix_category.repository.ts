import { injectable, inject } from "inversify"
import { ICategory, IEditCategory } from "./admin.admix_category.req-schema"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class AdminAdmixCategoryRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async addCategory(catData: ICategory, created_by: string) {
    const catCheck = await AdmixtureCategoryModel.findOne({
      category_name: catData.category_name,
    })
    if (catCheck !== null) {
      const err = new UnexpectedInput(
        `The entered category name" ${catData.category_name}" already added `
      )
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    catData._id = new ObjectId()
    catData.created_by = created_by

    const newCat = new AdmixtureCategoryModel(catData)
    const newCatDoc = await newCat.save()

    await this.commonService.addActivityLogs(
      created_by,
      "admin",
      AccessTypes.ADD_ADMIXTURE_CATEGORY,
      `Admin added Admixture_category of id ${newCatDoc._id} .`
    )

    return Promise.resolve(newCatDoc)
  }

  async editCategory(
    user_id: string,
    categoryId: string,
    catData: IEditCategory
  ): Promise<any> {
    const result = await AuthModel.findOne({
      code: catData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const catCheck = await AdmixtureCategoryModel.findOne({
      category_name: catData.category_name,
    })

    if (
      !isNullOrUndefined(catCheck) &&
      catCheck._id.equals(categoryId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`The entered category name" ${catData.category_name}" already added `, 400)
      )
    }

    const flds = ["category_name", "brand_id", "admixture_type"]
    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(catData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const res = await AdmixtureCategoryModel.findOneAndUpdate(
      { _id: categoryId },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`We could not find this category`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_ADMIXTURE_CATEGORY,
      `Admin Updated details of Admixture_category category id ${categoryId}.`,
      res,
      editDoc
    )

    return Promise.resolve()
  }

  async getCategories(
    search?: string,
    before?: string,
    after?: string,
    brand_id?: string
  ) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(brand_id) && brand_id !== "") {
      query.brand_id = new ObjectId(brand_id)
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    const aggregateArr = [
      { $match: query },
      { $sort: { created_at: -1 } },
      { $limit: 10 },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          category_name: 1,
          admixture_type: 1,
          created_by: 1,
          created_at: 1,
          updated_at: 1,
          updated_by_id: 1,
          brand_id: "$admixture_brand._id",
          admixture_brand_name: "$admixture_brand.name",
        },
      },
    ]

    const admixCatData = await AdmixtureCategoryModel.aggregate(aggregateArr)
    if (isNullOrUndefined(admixCatData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }
    return Promise.resolve(admixCatData)
  }
}
