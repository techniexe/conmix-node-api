import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface ICategory {
  category_name: string
  brand_id: string
  admixture_type: string
  [k: string]: any
}

export interface IEditCategory {
  category_name?: string
  brand_id?: string
  admixture_type?: string
  authentication_code: string
}

export interface IAdmixCategoryRequest extends IAuthenticatedRequest {
  body: ICategory
}

export interface IAdmixEditCategoryRequest extends IAuthenticatedRequest {
  body: IEditCategory
}

export const AdmixCategorySchema: IRequestSchema = {
  body: Joi.object().keys({
    category_name: Joi.string().min(3).max(150).required(),
    brand_id: Joi.string().regex(objectIdRegex).required(),
    admixture_type: Joi.string().required(),
  }),
}

export const AdmixeditCategorySchema: IRequestSchema = {
  params: Joi.object().keys({
    categoryId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    category_name: Joi.string().min(3).max(150),
    brand_id: Joi.string().regex(objectIdRegex),
    admixture_type: Joi.string(),
    authentication_code: Joi.string().required(),
  }),
}
