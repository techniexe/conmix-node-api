import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { injectable, inject } from "inversify"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdminTypes } from "../admin.types"
import { AdminAdmixCategoryRepository } from "./admin.admix_category.repository"
import {
  AdmixCategorySchema,
  IAdmixCategoryRequest,
  AdmixeditCategorySchema,
  IAdmixEditCategoryRequest,
} from "./admin.admix_category.req-schema"

@controller("/admixture_category", verifyCustomToken("admin"))
@injectable()
export class AdminAdmixCategoryController {
  constructor(
    @inject(AdminTypes.AdminAdmixCategoryRepository)
    private adMixCatRepo: AdminAdmixCategoryRepository
  ) {}
  @httpPost("/", validate(AdmixCategorySchema))
  async addCategory(
    req: IAdmixCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log(req)
      const { _id } = await this.adMixCatRepo.addCategory(
        req.body,
        req.user.uid
      )
      res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:categoryId", validate(AdmixeditCategorySchema))
  async editCategory(
    req: IAdmixEditCategoryRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.adMixCatRepo.editCategory(
        req.user.uid,
        req.params.categoryId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, brand_id } = req.query
    const data = await this.adMixCatRepo.getCategories(
      search,
      before,
      after,
      brand_id
    )
    res.json({ data })
  }
}
