import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { ProductModel } from "../../model/product.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { IGetProduct, IUpdateProduct } from "./admin.product.req-schema"
import { InvalidInput } from "../../utilities/customError"
import { AuthModel } from "../../model/auth.model"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"

@injectable()
export class AdminProductRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async getProduct(reqObj: IGetProduct) {
    //const arrSortFld = ["name", "category_id", "sub_category_id"]
    const query: { [k: string]: any } = {}
    // const option: { [k: string]: any } = {
    //   limit: 40,
    // }
    if (!isNullOrUndefined(reqObj.user_id)) {
      query.user_id = reqObj.user_id
    }
    if (!isNullOrUndefined(reqObj.search) && reqObj.search !== "") {
      query.$text = { $search: reqObj.search }
    }
    if (!isNullOrUndefined(reqObj.before) && reqObj.before !== "") {
      query.created_at = { $lt: reqObj.before }
    }
    if (!isNullOrUndefined(reqObj.after) && reqObj.after !== "") {
      query.created_at = { $gt: reqObj.after }
    }
    if (!isNullOrUndefined(reqObj.categories)) {
      query.category_id = { $in: reqObj.categories }
    }
    if (!isNullOrUndefined(reqObj.sub_categories)) {
      query.sub_category_id = { $in: reqObj.sub_categories }
    }
    if (!isNullOrUndefined(reqObj.verified_by_admin)) {
      query.verified_by_admin = reqObj.verified_by_admin
    }
    if (!isNullOrUndefined(reqObj.is_available)) {
      query.is_available = reqObj.is_available
    }
    // if (reqObj.deleted === "yes") {
    //   query.is_deleted = true
    // } else if (reqObj.deleted === "no") {
    //   query.is_deleted = false
    // }
    // if (!isNullOrUndefined(reqObj.order_by)) {
    //   option.sort = reqObj.order_by.split(",").reduce((obj, str) => {
    //     const arrSplt = str.split(":")
    //     if (arrSortFld.indexOf(arrSplt[0]) < 0) {
    //       return obj
    //     }
    //     return Object.assign(obj, {
    //       [arrSplt[0]]: arrSplt[1] === "desc" ? -1 : 1,
    //     })
    //   }, {})
    // }
    // return ProductModel.find(query, null, option)
    console.log(query)
    return ProductModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: {
          path: "$productSubCategory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          self_logistics: 1,
          "productCategory._id": 1,

          "productSubCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }

  async updateProduct(
    user_id: string,
    productId: string,
    updtBody: IUpdateProduct
  ) {
    const result = await AuthModel.findOne({
      code: updtBody.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }
    const arrKyes = [
      "name",
      "minimum_order",
      "unit_price",
      "self_logistics",
      "is_available",
      "verified_by_admin",
    ]

    const updt: { [k: string]: any } = {}

    for (const [key, value] of Object.entries(updtBody)) {
      if (arrKyes.indexOf(key) > -1) {
        updt[key] = value
      }
    }

    if (!isNullOrUndefined(updtBody.verified_by_admin)) {
      updt.verified_by_admin = updtBody.verified_by_admin
      updt.verified_at = Date.now()
    }

    if (Object.keys(updt).length < 1) {
      return Promise.reject(new InvalidInput("Kindly enter a field for update", 400))
    }
    const res = await ProductModel.findOneAndUpdate(
      { _id: productId },
      { $set: updt }
    )
    if (res === null) {
      return Promise.reject(new InvalidInput("No matched record found.", 400))
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.EDIT_PRODUCT_BY_ADMIN,
      `Admin Updated details of product id ${productId}.`,
      res,
      updt
    )

    await AuthModel.deleteOne({
      user_id,
      code: updtBody.authentication_code,
    })

    return Promise.resolve()
  }

  async getProductDetails(product_id: string) {
    const productQuery: { [k: string]: any } = {}
    productQuery._id = new ObjectId(product_id)

    const aggregateArr = [
      { $match: productQuery },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: {
          path: "$productSubCategory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          self_logistics: 1,
          "productCategory._id": 1,
          "productSubCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ]
    const productData = await ProductModel.aggregate(aggregateArr)
    if (isNullOrUndefined(productData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(productData[0])
  }
}
