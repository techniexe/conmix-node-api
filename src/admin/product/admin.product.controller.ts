import { injectable, inject } from "inversify"
import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { AdminTypes } from "../admin.types"
import { AdminProductRepository } from "./admin.product.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  getProduct,
  IGetProductRequest,
  IUpdateProductRequest,
  updateProductRequest,
  getProductDetailsSchema,
  IGetProductDetailsRequest,
} from "./admin.product.req-schema"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"

@injectable()
@controller("/product", verifyCustomToken("admin"))
export class AdminProductController {
  constructor(
    @inject(AdminTypes.AdminProductRepository)
    private productRepo: AdminProductRepository
  ) {}

  @httpGet("/", validate(getProduct))
  async getProduct(req: IGetProductRequest, res: Response, next: NextFunction) {
    const data = await this.productRepo.getProduct(req.query)
    res.json({ data })
  }

  @httpPatch("/:productId", validate(updateProductRequest))
  async updateProduct(
    req: IUpdateProductRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.productRepo.updateProduct(
      req.user.uid,
      req.params.productId,
      req.body
    )
    res.sendStatus(201)
  }

  @httpGet("/:productId", validate(getProductDetailsSchema))
  async getProductDetails(
    req: IGetProductDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.productRepo.getProductDetails(req.params.productId)
    res.json({ data })
  }
}
