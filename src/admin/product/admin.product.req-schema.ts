import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetProduct {
  user_id?: string
  search?: string
  before?: string
  after?: string
  categories?: [string]
  sub_categories?: [string]
  deleted?: string
  // order_by?: string
  verified_by_admin?: boolean
  is_available?: boolean
}

export interface IGetProductRequest extends IAuthenticatedRequest {
  query: IGetProduct
}

export interface IUpdateProduct {
  user_id?: string
  name?: string
  minimum_order?: number
  unit_price?: number
  self_logistics?: boolean
  // contact_person_id?: string
  is_available?: boolean
  verified_by_admin?: boolean
  authentication_code: string
}

export interface IUpdateProductRequest extends IAuthenticatedRequest {
  body: IUpdateProduct
  params: {
    productId: string
  }
}
export const getProduct: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    sub_categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    deleted: Joi.string().valid(["yes", "no", "both"]).default("no"),
    // order_by: Joi.string(), //comma separated fields. ex- name:desc,category_id,sub_category_id
    verified_by_admin: Joi.boolean(),
    is_available: Joi.boolean(),
  }),
}

export const updateProductRequest: IRequestSchema = {
  body: Joi.object().keys({
    name: Joi.string().min(3).max(100),
    minimum_order: Joi.number(),
    unit_price: Joi.number().min(0),
    self_logistics: Joi.boolean(),
    // contact_person_id: Joi.string().regex(objectIdRegex),
    is_available: Joi.boolean(),
    verified_by_admin: Joi.boolean(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetProductDetailsRequest extends IAuthenticatedRequest {
  params: {
    productId: string
  }
}

export const getProductDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}
