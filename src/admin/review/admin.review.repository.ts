import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ReviewModel } from "../../model/review.model"
import { IEditVendorreview } from "./admin.review.req.schema"
import { InvalidInput } from "../../utilities/customError"
import { AuthModel } from "../../model/auth.model"
import { ObjectId } from "bson"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { VendorUserModel } from "../../model/vendor.user.model"

@injectable()
export class ReviewRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async getVendorReview(
    user_id?: string,
    search?: string,
    before?: string,
    after?: string,
    vendor_id?: string,
    status?: string,
    buyer_name?: string,
    company_name?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(vendor_id)) {
      query.vendor_id = new ObjectId(vendor_id)
    }
    if (!isNullOrUndefined(status)) {
      query.status = status
    }
    if (!isNullOrUndefined(buyer_name)) {
      let buyerData = await BuyerUserModel.findOne({
        full_name: buyer_name,
      })
      if (!isNullOrUndefined(buyerData)) {
        query.user_id = new ObjectId(buyerData._id)
      } else {
        query.user_id = ""
      }
    }

    if (!isNullOrUndefined(company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
          {
            client_id: new ObjectId(user_id1),
          },
        ]
      } else {
        query.vendor_id = ""
      }
    }

    console.log("query", query)
    return ReviewModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { updated_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyerDetails",
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$buyerDetails" },
      },
      {
        $unwind: { path: "$vendorDetails" },
      },
      // {
      //   $lookup: {
      //     from: "aggregate_sand_category",
      //     localField: "productDetails.category_id",
      //     foreignField: "_id",
      //     as: "productCategory",
      //   },
      // },
      // {
      //   $lookup: {
      //     from: "product_sub_category",
      //     localField: "productDetails.sub_category_id",
      //     foreignField: "_id",
      //     as: "productSubCategory",
      //   },
      // },

      // {
      //   $lookup: {
      //     from: "supplier",
      //     localField: "productDetails.user_id",
      //     foreignField: "_id",
      //     as: "supplier",
      //   },
      // },

      // {
      //   $unwind: { path: "$productCategory" },
      // },
      // {
      //   $unwind: { path: "$productSubCategory" },
      // },
      // {
      //   $unwind: { path: "$supplier" },
      // },
      {
        $project: {
          user_id: 1,
          vendor_id: 1,
          review_text: 1,
          rating: 1,
          status: 1,
          created_at: 1,
          modirated_at: 1,
          // "productCategory.category_name": 1,
          // "productCategory.image_url": 1,
          // "productCategory.thumbnail_url": 1,
          // "productSubCategory.sub_category_name": 1,
          // "productSubCategory.image_url": 1,
          // "productSubCategory.thumbnail_url": 1,
          // "productDetails.name": 1,
          // "productDetails.user_id": 1,
          // "productDetails.media": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.company_name": 1,
          "buyerDetails.full_name": 1,
        },
      },
    ])
  }

  async editVendorReview(
    user_id: string,
    reviewId: string,
    vendorReviewData: IEditVendorreview
  ) {
    const result = await AuthModel.findOne({
      code: vendorReviewData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(vendorReviewData.status)) {
      editDoc.status = vendorReviewData.status
    }

    if (!isNullOrUndefined(vendorReviewData.review_text)) {
      editDoc.review_text = vendorReviewData.review_text
      editDoc.modirated_at = Date.now()
    }
    if (Object.keys(editDoc).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const res = await ReviewModel.findOneAndUpdate(
      { _id: reviewId },
      { $set: editDoc }
    )

    if (res === null) {
      const err1 = new InvalidInput("No matched record found.")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_VENDOR_REVIEW,
      `Admin Updated review of vendor`,
      res,
      editDoc
    )
    await AuthModel.deleteOne({
      user_id,
      code: vendorReviewData.authentication_code,
    })

    return Promise.resolve()
  }
}
