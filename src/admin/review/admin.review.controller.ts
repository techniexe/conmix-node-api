import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { Response, NextFunction } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  GetVendorreviewSchema,
  IGetVendorreview,
  editVendorreviewSchema,
  IEditVendorReviewRequest,
} from "./admin.review.req.schema"
import { ReviewRepository } from "./admin.review.repository"

@controller("/review", verifyCustomToken("admin"))
@injectable()
export class ReviewController {
  constructor(
    @inject(AdminTypes.ReviewRepository)
    private reviewRepo: ReviewRepository
  ) {}

  @httpGet("/", validate(GetVendorreviewSchema))
  async getVendorReview(
    req: IGetVendorreview,
    res: Response,
    next: NextFunction
  ) {
    const { user_id, search, before, after, vendor_id, status,buyer_name , company_name} = req.query
    const data = await this.reviewRepo.getVendorReview(
      user_id,
      search,
      before,
      after,
      vendor_id,
      status,
      buyer_name,
      company_name
    )
    res.json({ data })
  }

  @httpPatch("/:reviewId", validate(editVendorreviewSchema))
  async editVendorReview(
    req: IEditVendorReviewRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { reviewId } = req.params
      await this.reviewRepo.editVendorReview(req.user.uid, reviewId, req.body)
      return res.json(202)
    } catch (err) {
      next(err)
    }
  }
}
