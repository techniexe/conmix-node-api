import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetVendorreview extends IAuthenticatedRequest {
  query: {
    user_id?: string
    search?: string
    before?: string
    after?: string
    //sub_category_id?: string
    vendor_id?: string
    status?: string
    buyer_name?: string
    company_name?: string
  }
}

export const GetVendorreviewSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    //sub_category_id: Joi.string().regex(objectIdRegex),
    vendor_id: Joi.string().regex(objectIdRegex),
    status: Joi.string().valid(["published", "unpublished"]),
    buyer_name: Joi.string(),
    company_name: Joi.string(),
  }),
}

export interface IEditVendorreview {
  status?: string
  review_text?: string
  authentication_code: string
}

export interface IEditVendorReviewRequest extends IAuthenticatedRequest {
  body: IEditVendorreview
}

export const editVendorreviewSchema: IRequestSchema = {
  params: Joi.object().keys({
    reviewId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    status: Joi.string().valid(["published", "unpublished"]),
    review_text: Joi.string(),
    authentication_code: Joi.string().required(),
  }),
}
