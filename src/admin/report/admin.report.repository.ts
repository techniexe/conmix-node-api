import { ObjectId } from "bson"
import { injectable } from "inversify"
import {
  IBuyerSearchByAdmin,
  ISupplierSearchByAdmin,
  IGetDesignMixture,
  IGetCouponDetails,
  IGetSupportTicketByAdmin,
  IGetTMList,
} from "./admin.report.schema"
import { escapeRegExp, isNullOrUndefined } from "../../utilities/type-guards"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { InvalidInput } from "../../utilities/customError"
import { VendorUserModel } from "../../model/vendor.user.model"

//import { monthsCode } from "../../utilities/config"
import { OrderModel } from "../../model/order.model"
import { monthsCode } from "../../utilities/config"
import { OrderItemModel } from "../../model/order_item.model"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { ProductModel } from "../../model/product.model"

import { DesignMixtureModel } from "../../model/design_mixture.model "
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { CementBrandModel } from "../../model/cement_brand.model"
import { SandSourceModel } from "../../model/sand_source.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { AggregateSandCategoryModel, AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { AddressModel } from "../../model/address.model"
import { CityModel, StateModel } from "../../model/region.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import { ConcretePumpCategoryModel } from "../../model/concrete_pump.model"
import { CouponModel } from "../../model/coupon.model"
import { SupportTicketModel } from "../../model/support_ticket.model"
import { GstSlabModel } from "../../model/gst_slab.model"
import { MarginRateModel } from "../../model/margin_rate.model"
import { TMCategoryModel, TMModel, TMSubCategoryModel } from "../../model/TM.model"

@injectable()
export class AdminReportRepository {
  async buyerListing(searchParam: IBuyerSearchByAdmin) {
    const query: { [k: string]: any } = {}
    const limit = 10

    if (!isNullOrUndefined(searchParam.search) && searchParam.search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(searchParam.search)
      query.$or = [
        {
          mobile_number: searchParam.search,
        },
        {
          email: searchParam.search,
        },
        {
          full_name: { $regex, $options },
        },
      ]
    }

    if (!isNullOrUndefined(searchParam.user_id) && searchParam.user_id !== "") {
      query.user_id = searchParam.user_id
    }
    if (!isNullOrUndefined(searchParam.company_name) && searchParam.company_name !== "") {
      query.company_name = searchParam.company_name
    }
    if (!isNullOrUndefined(searchParam.account_type) && searchParam.account_type !== "") {
      query.account_type = searchParam.account_type
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
    }

    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.buyer_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.buyer_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.buyer_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr: any[] = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "_id",
          foreignField: "user_id",
          as: "orderDetails",
        },
      },
      {
        $project: {
          _id: true,
          user_id: true,
          account_type: true,
          company_type: true,
          company_name: true,
          mobile_number: true,
          mobile_verified: true,
          email: true,
          email_verified: true,
          signup_type: true,
          password: true,
          gst_number: true,
          pan_number: true,
          identity_image_url: true,
          landline_number: true,
          notification_count: true,
          notification_opted: true,
          full_name: true,
          created_at: true,
          orderDetails: true,
        },
      },
    ]

    if (isNullOrUndefined(searchParam.is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const buyerData = await BuyerUserModel.aggregate(aggregateArr)
    if (buyerData.length < 0) {
      return Promise.reject(new InvalidInput(`No Buyer data were found .`, 400))
    }

    for (let i = 0; i < buyerData.length; i++) {
      buyerData[i].order_count = buyerData[i].orderDetails.length
      //delete buyerData[i].orderDetails
    }
    return buyerData
  }

  async vendorListing(searchParam: ISupplierSearchByAdmin) {
    const query: { [k: string]: any } = {}
    const limit = 10

    if (!isNullOrUndefined(searchParam.search) && searchParam.search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(searchParam.search)
      query.$or = [
        {
          mobile_number: searchParam.search,
        },
        {
          email: searchParam.search,
        },
        {
          company_name: { $regex, $options },
        },
        // {
        //   full_name : { $regex, $options },
        // }
      ]
    }

    if (!isNullOrUndefined(searchParam.company_type) && searchParam.company_type !== "") {
      query.company_type = searchParam.company_type
    }

    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
    }

    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.vendor_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.vendor_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.vendor_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr: any[] = [
      { $match: query },
      {
        $lookup: {
          from: "vendor_order",
          localField: "_id",
          foreignField: "user_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $lookup: {
          from: "TM",
          localField: "_id",
          foreignField: "user_id",
          as: "vendorTMDetails",
        },
      },
      {
        $lookup: {
          from: "concrete_pump",
          localField: "_id",
          foreignField: "user_id",
          as: "vendorCPDetails",
        },
      },
      {
        $project: {
          _id: true,
          full_name: true,
          company_type: true,
          company_name: true,
          mobile_number: true,
          email: true,
          company_certification_number: true,
          company_certification_image_url: true,
          verified_by_admin: true,
          created_at: true,
          vendorOrderDetails: true,
          vendorTMDetails: true,
          vendorCPDetails: true,
        },
      },
    ]

    if (isNullOrUndefined(searchParam.is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const supplierData = await VendorUserModel.aggregate(aggregateArr)
    if (supplierData.length < 0) {
      return Promise.reject(new InvalidInput(`No Vendor Data found.`, 400))
    }

    for (let i = 0; i < supplierData.length; i++) {
      supplierData[i].order_count = supplierData[i].vendorOrderDetails.length
      supplierData[i].TM_count = supplierData[i].vendorTMDetails.length
      supplierData[i].CP_count = supplierData[i].vendorCPDetails.length

      // delete supplierData[i].supplierOrderDetails
    }
    return supplierData
  }

  async buyerOrder(
    month: string | undefined,
    year: number | undefined,
    buyer_id: string | undefined,
    status: string | undefined,
    before: string | undefined,
    after: string | undefined,
    payment_status?: string,
    gateway_transaction_id?: string,
    is_export?: boolean,
    start_date?: string,
    end_date?: string,
    buyer_order_id?: string,
    buyer_name?: string

  ) {
    let Orderquery: { [k: string]: any } = {}
    // If year is passed, then check with first and last date of given year.
    if (!isNullOrUndefined(year)) {
      Orderquery.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }
    // If month and year passed, then find first and last day of given month and year then check.
    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      Orderquery.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    const sort = { created_at: 1 }
    if (!isNullOrUndefined(before)) {
      Orderquery.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after)) {
      Orderquery.created_at = { $gt: new Date(after) }
      sort.created_at = -1
    }

    if (!isNullOrUndefined(status)) {
      Orderquery.order_status = status
    }

    if (!isNullOrUndefined(payment_status)) {
      Orderquery.payment_status = payment_status
    }
    if (!isNullOrUndefined(gateway_transaction_id)) {
      Orderquery.gateway_transaction_id = gateway_transaction_id
    }
    if (!isNullOrUndefined(buyer_id)) {
      Orderquery.user_id = new ObjectId(buyer_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      Orderquery.order_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      Orderquery.order_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      Orderquery.order_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    if(!isNullOrUndefined(buyer_name)){
      let buyerData = await BuyerUserModel.findOne({
        full_name : buyer_name
      })
      if(!isNullOrUndefined(buyerData)){
        Orderquery.user_id = new ObjectId(buyerData._id)
      }else {
        Orderquery.user_id = ""
      }
    }

    if (!isNullOrUndefined(buyer_order_id)) {
      Orderquery.display_id = buyer_order_id
    }


    if (!isNullOrUndefined(is_export) && is_export === true) {
      return await OrderModel.find(Orderquery).sort({ created_at: -1 })
    }

    const aggregateOrderArr = [
      { $match: Orderquery },
      {
        $sort : {created_at : -1}
      },
      { $limit: 10 },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "site",
          localField: "site_id",
          foreignField: "_id",
          as: "site",
        },
      },
      {
        $unwind: "$site",
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
        },
      },
      {
        $project: {
          _id: 1,
          display_id: 1,
          user_id: 1,
          order_status: 1,
          payment_status: 1,
          selling_price: 1,
          margin_price: 1,
          gst_amount: 1,
          TM_price: 1,
          CP_price: 1,
          delivery_location: 1,
          payment_attempt: 1,
          payment_mode: 1,
          base_amount: 1,
          logistics_amount: 1,
          margin_amount: 1,
          coupon_amount: 1,
          total_amount: 1,
          site_id: 1,
          site_name: 1,
          company_name: "$site.company_name",
          person_name: "$site.person_name",
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          created_at: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          mobile_number: "$site.mobile_number",
          alt_mobile_number: "$site.alt_mobile_number",
          "invoice._id": true,
          "buyer._id": true,
          "buyer.full_name": true,
          "buyer.company_name": true,
        },
      },
    ]

    const buyerOrderData = await OrderModel.aggregate(aggregateOrderArr)
    // if (sort.created_at === 1) {
    //   buyerOrderData.reverse()
    // }
    return Promise.resolve(buyerOrderData)
  }

  async buyerOrderById(order_id: string, user_id: string | undefined) {
    const Orderquery: { [k: string]: any } = {}
    Orderquery._id = new ObjectId(order_id)

    const aggregateOrderArr = [
      { $match: Orderquery },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "site",
          localField: "site_id",
          foreignField: "_id",
          as: "site",
        },
      },
      {
        $unwind: "$site",
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
        },
      },
      {
        $project: {
          _id: 1,
          display_id: 1,
          user_id: 1,
          order_status: 1,
          payment_status: 1,
          selling_price: 1,
          margin_price: 1,
          gst_amount: 1,
          TM_price: 1,
          CP_price: 1,
          delivery_location: 1,
          payment_attempt: 1,
          payment_mode: 1,
          base_amount: 1,
          logistics_amount: 1,
          margin_amount: 1,
          coupon_amount: 1,
          total_amount: 1,
          site_id: 1,
          site_name: 1,
          company_name: "$site.company_name",
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          created_at: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          mobile_number: "$site.mobile_number",
          alt_mobile_number: "$site.alt_mobile_number",
          "invoice._id": true,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]

    const orderData = await OrderModel.aggregate(aggregateOrderArr)
    if (orderData.length <= 0) {
      return Promise.reject(new InvalidInput(`We could not find order details for this order`, 400))
    }

    const orderItemQuery: { [k: string]: any } = {}
    orderItemQuery.order_id = new ObjectId(order_id)

    const aggregateArr = [
      { $match: orderItemQuery },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "vendor_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      // {
      //   $lookup: {
      //     from: "product_sub_category",
      //     localField: "product_sub_category_id",
      //     foreignField: "_id",
      //     as: "product_sub_category",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$product_sub_category",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      {
        $project: {
          display_item_id: 1,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          design_mix_id: 1,
          cement_quantity: 1,
          sand_quantity: 1,
          aggregate1_quantity: 1,
          aggregate2_quantity: 1,
          fly_ash_quantity: 1,
          admix_quantity: 1,
          water_quantity: 1,
          vendor_id: 1,
          buyer_id: 1,
          order_id: 1,
          quantity: 1,
          revised_quantity: 1,
          cement_per_kg_rate: 1,
          sand_per_kg_rate: 1,
          aggregate1_per_kg_rate: 1,
          aggregate2_per_kg_rate: 1,
          fly_ash_per_kg_rate: 1,
          admix_per_kg_rate: 1,
          water_per_ltr_rate: 1,
          cement_price: 1,
          sand_price: 1,
          aggreagte1_price: 1,
          aggreagte2_price: 1,
          fly_ash_price: 1,
          admix_price: 1,
          water_price: 1,
          with_TM: 1,
          with_CP: 1,
          distance: 1,
          unit_price: 1,
          selling_price: 1,
          margin_price: 1,
          TM_price: 1,
          CP_price: 1,
          gst_type: 1, // IGST/ SGST,CGST
          igst_rate: 1,
          cgst_rate: 1,
          sgst_rate: 1,
          igst_amount: 1,
          sgst_amount: 1,
          cgst_amount: 1,
          item_status: 1,
          created_at: 1,
          created_by_id: 1,
          created_by_type: 1,
          payment_status: 1,
          address_id: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          location: 1,
          address_type: 1,
          business_name: 1,
          city_name: 1,
          state_name: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "vendor.mobile_number": 1,
          "vendor.email": 1,
          vendor_media: 1,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const orderItemData = await OrderItemModel.aggregate(aggregateArr)
    return Promise.resolve({
      orderData: orderData[0],
      orderItemData,
    })
  }
  async vendorOrder(
    month: string | undefined,
    year: number | undefined,
    product_id: string | undefined,
    supplier_id: string | undefined,
    buyer_id: string | undefined,
    status: string | undefined,
    before: string | undefined,
    after: string | undefined,
    is_export: boolean | undefined,
    start_date: string,
    end_date: string
  ) {
    let Orderquery: { [k: string]: any } = {}
    const limit = 10
    //let OrderItemQuery: { [k: string]: any } = {}
    // If year is passed, then check with first and last date of given year.
    if (!isNullOrUndefined(year)) {
      Orderquery.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }
    // If month and year passed, then find first and last day of given month and year then check.
    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      Orderquery.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    const sort = { created_at: 1 }
    if (!isNullOrUndefined(before)) {
      Orderquery.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after)) {
      Orderquery.created_at = { $gt: new Date(after) }
      sort.created_at = -1
    }

    if (!isNullOrUndefined(status)) {
      Orderquery.order_status = status
    }

    if (!isNullOrUndefined(supplier_id)) {
     // Orderquery.user_id = new ObjectId(supplier_id)
      Orderquery.$or = [
        {
          user_id: new ObjectId(supplier_id),
        },
        {
          sub_vendor_id: new ObjectId(supplier_id),
        },
        {
          master_vendor_id: new ObjectId(supplier_id),
        },
      ]
   
    }

    if (!isNullOrUndefined(buyer_id)) {
      Orderquery.buyer_id = new ObjectId(buyer_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      Orderquery.vendor_order_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      Orderquery.vendor_order_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      Orderquery.vendor_order_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    console.log("Orderquery", Orderquery)
    const aggregateArr: any[] = [
      { $match: Orderquery },
      { $sort: sort },
      { $limit: limit },
      //{ $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },     
 
      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          unit_price: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "vendor._id" : true,
          "vendor.full_name": true,
          "vendor.company_name" : true,
          "vendor.mobile_number" : true,
          // order: true,
          // gateway_transaction_id : "$order.gateway_transaction_id",
          // "delivery_address.site_id": "$order.site_id",
          // "delivery_address.site_name": "$order.site_name",
          // "delivery_address.address_line1": "$order.address_line1",
          // "delivery_address.address_line2": "$order.address_line2",
          // "delivery_address.state_id": "$order.state_id",
          // "delivery_address.city_id": "$order.city_id",
          // "delivery_address.pincode": "$order.pincode",
          // "delivery_address.location": "$order.delivery_location",
          // "delivery_address.created_at": "$order.created_at",
          // "delivery_address.state_name": "$stateData.state_name",
          // "delivery_address.city_name": "$cityData.city_name",

        },
      },
    ]

    if (isNullOrUndefined(is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      vendorOrderData.reverse()
    }
    return Promise.resolve(vendorOrderData)
  }

  async vendorOrderById(order_id: string, supplier_id?: string) {
    const Orderquery: { [k: string]: any } = {}
    Orderquery._id = order_id
    if (!isNullOrUndefined(supplier_id)) {
      Orderquery.user_id = new ObjectId(supplier_id)
    }

    const aggregateArr = [
      { $match: Orderquery },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "pickup_address",
        },
      },
      {
        $unwind: { path: "$pickup_address" },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_display_id",
          foreignField: "display_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "order.state_id",
          foreignField: "_id",
          as: "stateData",
        },
      },
      {
        $unwind: { path: "$stateData" },
      },
      {
        $lookup: {
          from: "city",
          localField: "order.city_id",
          foreignField: "_id",
          as: "cityData",
        },
      },
      {
        $unwind: { path: "$cityData" },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "user_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      // {
      //   $lookup: {
      //     from: "order_item",
      //     localField: "buyer_order_item_id",
      //     foreignField: "_id",
      //     as: "orderItem",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$orderItem",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },

      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          unit_price: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          vendor_media: true,
          "vendor._id": true,
          "vendor.full_name": true,
          "vendor.company_name": true,
          "vendor.mobile_number": true,
          "vendor.landline_number": true,
          "vendor.email": true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          "pickup_address.state_name": "$state_name",
          "pickup_address.city_name": "$city_name",
          "pickup_address.line1": true,
          "pickup_address.line2": true,
          "pickup_address.pincode": true,
          "pickup_address.location": true,
          "delivery_address.site_id": "$order.site_id",
          "delivery_address.site_name": "$order.site_name",
          "delivery_address.address_line1": "$order.address_line1",
          "delivery_address.address_line2": "$order.address_line2",
          "delivery_address.state_id": "$order.state_id",
          "delivery_address.city_id": "$order.city_id",
          "delivery_address.pincode": "$order.pincode",
          "delivery_address.location": "$order.delivery_location",
          "delivery_address.created_at": "$order.created_at",
          "delivery_address.state_name": "$stateData.state_name",
          "delivery_address.city_name": "$cityData.city_name",
          //orderItem: true,
        },
      },
    ]

    const supplierOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (supplierOrderData.length <= 0) {
      return Promise.reject(
        new InvalidInput(`Sorry, we could not find any  Suppplier order details.`, 400)
      )
    }
    return Promise.resolve(supplierOrderData[0])
    // let Items = []
    // if (
    //   !isNullOrUndefined(supplierOrderData) &&
    //   !isNullOrUndefined(supplierOrderData[0].orderItem) &&
    //   supplierOrderData[0].orderItem.length > 0
    // ) {
    //   for (let j = 0; j < supplierOrderData[0].orderItem.length; j++) {
    //     if (supplierOrderData[0].orderItem[j].vendor_id.equals(supplier_id)) {
    //       Items.push(supplierOrderData[0].orderItem[j])
    //     }
    //   }
    // }
    // supplierOrderData[0].orderItem = Items
    // if (
    //   !isNullOrUndefined(supplierOrderData) &&
    //   !isNullOrUndefined(supplierOrderData[0].orderItem) &&
    //   supplierOrderData[0].orderItem.length > 0
    // ) {
    //   for (let i = 0; i < supplierOrderData[0].orderItem.length; i++) {
    //     const data = await AggregateSandCategoryModel.findById(
    //       supplierOrderData[0].orderItem[i].product_category_id
    //     )
    //     if (!isNullOrUndefined(data)) {
    //       supplierOrderData[0].orderItem[i].product_category_name =
    //         data.category_name
    //       supplierOrderData[0].orderItem[i].product_category_image =
    //         data.image_url
    //       supplierOrderData[0].orderItem[i].thumbnail_url = data.thumbnail_url
    //     }

    //     const subcatData = await AggregateSandSubCategoryModel.findById(
    //       supplierOrderData[0].orderItem[i].product_sub_category_id
    //     )
    //     if (!isNullOrUndefined(subcatData)) {
    //       supplierOrderData[0].orderItem[i].product_sub_category_name =
    //         subcatData.sub_category_name
    //       supplierOrderData[0].orderItem[i].product_sub_category_image =
    //         subcatData.image_url
    //       supplierOrderData[0].orderItem[i].product_sub_category_thumbnail_url =
    //         subcatData.thumbnail_url
    //     }
    //   }
    // }

    // const orderTrackData = await OrderTrackModel.find({
    //   supplier_order_id: order_id,
    // })
    // supplierOrderData[0].orderTrackData = orderTrackData
    // for (let i = 0; i < supplierOrderData[0].orderTrackData.length; i++) {
    //   const vehicleData = await VehicleModel.findById(
    //     supplierOrderData[0].orderTrackData[i].vehicle_id
    //   )

    //   if (!isNullOrUndefined(vehicleData)) {
    //     supplierOrderData[0].orderTrackData[i].vehicle_rc_number =
    //       vehicleData.vehicle_rc_number

    //     const vehicleCategoryData = await VehicleCategoryeModel.findById(
    //       vehicleData.vehicle_category_id
    //     )

    //     if (!isNullOrUndefined(vehicleCategoryData)) {
    //       console.log("vehicleCategoryData", vehicleCategoryData)
    //       supplierOrderData[0].orderTrackData[i].category_name =
    //         vehicleCategoryData.category_name
    //     }
    //     const driverInfo = await DriverInfoModel.findById(
    //       vehicleData.driver1_id
    //     )
    //     if (!isNullOrUndefined(driverInfo)) {
    //       supplierOrderData[0].orderTrackData[i].driver_name =
    //         driverInfo.driver_name
    //       supplierOrderData[0].orderTrackData[i].driver_mobile_number =
    //         driverInfo.driver_mobile_number
    //     }
    //   }
    // }
  }

  async getProductList(
    before: string | undefined,
    after: string | undefined,
    supplier_id: string | undefined,
    product_category_id: string | undefined,
    product_sub_category_id: string | undefined
  ) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(supplier_id)) {
      query.user_id = new ObjectId(supplier_id)
    }

    if (!isNullOrUndefined(product_category_id)) {
      query.product_category_id = new ObjectId(product_category_id)
    }

    if (!isNullOrUndefined(product_sub_category_id)) {
      query.product_sub_category_id = new ObjectId(product_sub_category_id)
    }

    return ProductModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 40,
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: {
          path: "$productSubCategory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          self_logistics: 1,
          "productCategory._id": 1,
          "productSubCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }

  async getDesignMixture(searchParam: IGetDesignMixture) {
    const query: { [k: string]: any } = {}
    const limit = 10
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.user_id)) {
      query.vendor_id = searchParam.user_id
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = new ObjectId(searchParam.grade_id) 
    }

    if (!isNullOrUndefined(searchParam.company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: searchParam.company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          }
        ]
      } else {
        query.vendor_id = ""
      }
    }

    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.design_mix_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.design_mix_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.design_mix_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      //  { $limit: 10 },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
        },
      },

      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          "concrete_grade._id": true,
          "concrete_grade.name": true,
          "vendor._id": true,
          "vendor.full_name": true,
          "vendor.company_name": true,
        },
      },
    ]

    if (isNullOrUndefined(searchParam.is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      designMixData.reverse()
    }
    return Promise.resolve(designMixData)
  }

  async getDesignMixtureDetails(designMixtureId: string, searchParam: any) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(designMixtureId),
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = searchParam.grade_id
    }
    if (!isNullOrUndefined(searchParam.user_id)) {
      query.vendor_id = searchParam.user_id
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "_id",
          foreignField: "design_mix_id",
          as: "designMixVariantDetails",
        },
      },
      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          designMixVariantDetails: true,
        },
      },
    ]

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)
    if (designMixData.length > 0) {
      for (
        let i = 0;
        i < designMixData[0].designMixVariantDetails.length;
        i++
      ) {
        const concreteGradeDoc = await ConcreteGradeModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].grade_id,
        }).lean()
        if (!isNullOrUndefined(concreteGradeDoc)) {
          designMixData[0].designMixVariantDetails[i].concrete_grade_name =
            concreteGradeDoc.name
        }

        const cementBrandDoc = await CementBrandModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].cement_brand_id,
        }).lean()
        if (!isNullOrUndefined(cementBrandDoc)) {
          designMixData[0].designMixVariantDetails[i].cement_brand_name =
            cementBrandDoc.name
        }

        const sandSourceDoc = await SandSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].sand_source_id,
        }).lean()
        if (!isNullOrUndefined(sandSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].sand_source_name =
            sandSourceDoc.sand_source_name
        }

        const aggregateSourceDoc = await AggregateSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].aggregate_source_id,
        }).lean()
        if (!isNullOrUndefined(aggregateSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].aggregate_source_name =
            aggregateSourceDoc.aggregate_source_name
        }

        const flyAshSourceDoc = await FlyAshSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].fly_ash_source_id,
        }).lean()
        if (!isNullOrUndefined(flyAshSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].fly_ash_source_name =
            flyAshSourceDoc.fly_ash_source_name
        }

        const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id:
              designMixData[0].designMixVariantDetails[i]
                .aggregate1_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate1SubCatDoc)) {
          designMixData[0].designMixVariantDetails[
            i
          ].aggregate1_sub_category_name = aggregate1SubCatDoc.sub_category_name
        }

        const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id:
              designMixData[0].designMixVariantDetails[i]
                .aggregate2_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate2SubCatDoc)) {
          designMixData[0].designMixVariantDetails[
            i
          ].aggregate2_sub_category_name = aggregate2SubCatDoc.sub_category_name
        }

        const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].ad_mixture_brand_id,
        }).lean()
        if (!isNullOrUndefined(adMixtureBrandDoc)) {
          designMixData[0].designMixVariantDetails[i].admix_brand_name =
            adMixtureBrandDoc.name
        }

        const addressDoc = await AddressModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].address_id,
        }).lean()
        if (!isNullOrUndefined(addressDoc)) {
          designMixData[0].designMixVariantDetails[i].address_line1 =
            addressDoc.line1
          designMixData[0].designMixVariantDetails[i].address_line2 =
            addressDoc.line2
          designMixData[0].designMixVariantDetails[i].pincode =
            addressDoc.pincode
          designMixData[0].designMixVariantDetails[i].location =
            addressDoc.location

          const cityDoc = await CityModel.findById({
            _id: addressDoc.city_id,
          }).lean()

          if (!isNullOrUndefined(cityDoc)) {
            designMixData[0].designMixVariantDetails[i].city_name =
              cityDoc.city_name
          }

          const stateDoc = await StateModel.findById({
            _id: addressDoc.state_id,
          }).lean()

          if (!isNullOrUndefined(stateDoc)) {
            designMixData[0].designMixVariantDetails[i].state_name =
              stateDoc.state_name
          }
        }

        const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
          _id:
            designMixData[0].designMixVariantDetails[i].ad_mixture_category_id,
        }).lean()
        if (!isNullOrUndefined(adMixtureCatDoc)) {
          designMixData[0].designMixVariantDetails[i].admix_category_name =
            adMixtureCatDoc.category_name

          designMixData[0].designMixVariantDetails[i].admixture_type =
            adMixtureCatDoc.admixture_type
        }

        const cementGradeDoc = await CementGradeModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].cement_grade_id,
        }).lean()
        if (!isNullOrUndefined(cementGradeDoc)) {
          designMixData[0].designMixVariantDetails[i].cement_grade_name =
            cementGradeDoc.name
        }
      }

      return Promise.resolve(designMixData[0])
    }
    return Promise.reject(new InvalidInput(`No RMC Supplier found near the selected location.`, 400))
  }

  async getAdmixBrand(search?: string, before?: string, after?: string, start_date?: string, end_date?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.admix_brand_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.admix_brand_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.admix_brand_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    return AdmixtureBrandModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getCategories(
    search?: string,
    before?: string,
    after?: string,
    brand_id?: string,
    start_date?: string,
    end_date?: string
  ) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(brand_id) && brand_id !== "") {
      query.brand_id = new ObjectId(brand_id)
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.admix_cat_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.admix_cat_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.admix_cat_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    const aggregateArr = [
      { $match: query },
      { $sort: { created_at: -1 } },
      { $limit: 10 },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          category_name: 1,
          admixture_type: 1,
          created_by: 1,
          created_at: 1,
          updated_at: 1,
          updated_by_id: 1,
          brand_id: "$admixture_brand._id",
          admixture_brand_name: "$admixture_brand.name",
        },
      },
    ]

    const admixCatData = await AdmixtureCategoryModel.aggregate(aggregateArr)
    if (isNullOrUndefined(admixCatData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }
    return Promise.resolve(admixCatData)
  }

  async getAggSandCategories(
    search?: string,
    before?: string,
    after?: string,
    start_date?: string,
    end_date?: string
  ){
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.agg_sand_cat_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.agg_sand_cat_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.agg_sand_cat_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    return AggregateSandCategoryModel.find(query)
      .sort({ created_at: -1 })
      .limit(10)
  }

  async getAggSandSubCategories(
    category_id: string,
    search?: string,
    before?: string,
    after?: string,
    sub_category_name?: string,
    margin_rate_id?: any,
    start_date?: string,
    end_date?: string
  ) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(sub_category_name) && sub_category_name !== "") {
      query.sub_category_name = sub_category_name
    }
    if (!isNullOrUndefined(margin_rate_id)) {
      query.margin_rate_id = new ObjectId(margin_rate_id)
    }

    if (!isNullOrUndefined(category_id)) {
      query.category_id = new ObjectId(category_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.agg_sand_sub_cat_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.agg_sand_sub_cat_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.agg_sand_sub_cat_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    console.log(query)
    return AggregateSandSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $unwind: {
          path: "$gst_slab",
        },
      },

      {
        $lookup: {
          from: "margin_rate",
          localField: "margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: {
          path: "$margin_rate",
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "aggregate_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_category",
        },
      },
      {
        $project: {
          sub_category_name: 1,
          category_id: 1,
          "aggregate_sand_category._id": 1,
          "aggregate_sand_category.category_name": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          sequence: 1,
          min_quantity: 1,
          selling_unit: 1,
          image_url: 1,
          thumbnail_url: 1,
          created_at: 1,
          created_by: 1,
        },
      },
    ])
  }

  async aggsourceList(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.region_id)) {
      query.region_id = new ObjectId(searchParam.region_id)  
    }
    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.agg_source_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.agg_source_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.agg_source_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      { $match: query },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          aggregate_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await AggregateSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async getCementBrand(search?: string, before?: string, after?: string, start_date?: string, end_date?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.cement_brand_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.cement_brand_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.cement_brand_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    return CementBrandModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getCementgrade(search?: string, before?: string, after?: string, start_date?: string, end_date?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.cement_grade_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.cement_grade_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.cement_grade_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    return CementGradeModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getCg(search?: string, before?: string, after?: string, start_date?: string, end_date?: string) {
    let query: { [k: string]: any } = {}
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lte: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gte: new Date(after) }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.cg_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.cg_date = { $lte: end_date }
    }
    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.cg_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    return ConcreteGradeModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  // async getConcretePump(user_id: string, searchParam: IGetConcretePumpList) {
  //   const query: { [k: string]: any } = {}

  //   //query.user_id = new ObjectId(user_id)

  //   let limit: number = 10
  //   if (!isNullOrUndefined(searchParam.limit_count)) {
  //     limit = searchParam.limit_count
  //   }

  //   if (!isNullOrUndefined(searchParam.is_active)) {
  //     query.is_active = searchParam.is_active
  //   }

  //   if (!isNullOrUndefined(searchParam.concrete_pump_category_id)) {
  //     query.concrete_pump_category_id = new ObjectId(
  //       searchParam.concrete_pump_category_id
  //     )
  //   }

  //   if (!isNullOrUndefined(searchParam.user_id)) {
  //     query.user_id = searchParam.user_id
  //   }

  //   const sort = { created_at: -1 }
  //   if (!isNullOrUndefined(searchParam.before)) {
  //     query.created_at = { $lt: new Date(searchParam.before) }
  //   }
  //   if (!isNullOrUndefined(searchParam.after)) {
  //     query.created_at = { $gt: new Date(searchParam.after) }
  //     sort.created_at = 1
  //   }

  //   if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
  //     query.CP_date = { $gte: searchParam.start_date }
  //   }
  //   if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
  //     query.CP_date = { $lte: searchParam.end_date }
  //   }
  //   if (
  //     !isNullOrUndefined(searchParam.start_date) &&
  //     searchParam.start_date !== "" &&
  //     !isNullOrUndefined(searchParam.end_date) &&
  //     searchParam.end_date !== ""
  //   ) {
  //     query.CP_date = {
  //       $gte: searchParam.start_date,
  //       $lte: searchParam.end_date,
  //     }
  //   }

  //   const aggregateArr = [
  //     {
  //       $match: query,
  //     },
  //     {
  //       $sort: { created_at: -1 },
  //     },
  //     {
  //       $limit: limit,
  //     },

  //     {
  //       $lookup: {
  //         from: "concrete_pump_category",
  //         localField: "concrete_pump_category_id",
  //         foreignField: "_id",
  //         as: "concrete_pump_category",
  //       },
  //     },
  //     {
  //       $unwind: {
  //         path: "$concrete_pump_category",
  //         preserveNullAndEmptyArrays: true,
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "vendor",
  //         localField: "user_id",
  //         foreignField: "_id",
  //         as: "vendor",
  //       },
  //     },
  //     {
  //       $unwind: {
  //         path: "$vendor",
  //         preserveNullAndEmptyArrays: true,
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "operator_info",
  //         localField: "operator_id",
  //         foreignField: "_id",
  //         as: "operator_info",
  //       },
  //     },
  //     {
  //       $unwind: {
  //         path: "$operator_info",
  //         preserveNullAndEmptyArrays: true,
  //       },
  //     },

  //     {
  //       $project: {
  //         _id: 1,
  //         concrete_pump_category_id: 1,
  //         company_name: 1,
  //         concrete_pump_model: 1,
  //         manufacture_year: 1,
  //         pipe_connection: 1,
  //         concrete_pump_capacity: 1,
  //         is_Operator: 1,
  //         operator_id: 1,
  //         is_helper: 1,
  //         transportation_charge: 1,
  //         concrete_pump_image_url: 1,
  //         concrete_pump_price: 1,
  //         created_at: 1,
  //         updated_at: 1,
  //         "concrete_pump_category.category_name": 1,
  //         "concrete_pump_category.is_active": 1,
  //         "vendor._id": 1,
  //         "vendor.full_name": 1,
  //         "vendor.company_name": 1,
  //         "operator_info._id": 1,
  //         "operator_info.operator_name": 1,
  //         "operator_info.operator_mobile_number": 1,
  //       },
  //     },
  //   ]
  //   const data = await ConcretePumpModel.aggregate(aggregateArr)
  //   return Promise.resolve(data)
  // }

  async listAllConcretePumpCategory() {
    return ConcretePumpCategoryModel.find()
  }

  async getCouponList(searchParam: IGetCouponDetails) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(searchParam.is_deleted)) {
      query.is_deleted = searchParam.is_deleted
    }
    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }
   
    if (!isNullOrUndefined(searchParam.discount_type)) {
      query.discount_type = searchParam.discount_type
    }
    if (!isNullOrUndefined(searchParam.buyer_id)) {
      query.buyer_ids = new ObjectId(searchParam.buyer_id)
    }
    // if (!isNullOrUndefined(searchParam.supplier_id)) {
    //   query.supplier_ids = new ObjectId(searchParam.supplier_id)
    // }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.code)) {
      query.code = searchParam.code
    }
    if (!isNullOrUndefined(searchParam.discount_value)) {
      query.discount_value =  parseInt(searchParam.discount_value)
    }
    if (!isNullOrUndefined(searchParam.start_date)) {
      query.start_date =  { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date)) {
      query.end_date =  { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      !isNullOrUndefined(searchParam.end_date)
    ) {
      query.created_at = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }
    if (!isNullOrUndefined(searchParam.min_order) && 
    !isNullOrUndefined(searchParam.min_order_range_value) && 
    searchParam.min_order_range_value === "less") {
      query.min_order =  { $lte: parseInt(searchParam.min_order) }
    }
    if (!isNullOrUndefined(searchParam.min_order) && 
    !isNullOrUndefined(searchParam.min_order_range_value) &&
    searchParam.min_order_range_value === "more")  {
      query.min_order =  { $gte: parseInt(searchParam.min_order) }
    }
    if (!isNullOrUndefined(searchParam.max_discount) 
    && !isNullOrUndefined(searchParam.max_discount_range_value) &&
    searchParam.max_discount_range_value === "less") {
      query.max_discount =  { $lte: parseInt(searchParam.max_discount) }
    }
    if (!isNullOrUndefined(searchParam.max_discount) && 
    !isNullOrUndefined(searchParam.max_discount_range_value) &&
    searchParam.max_discount_range_value === "more") {
      query.max_discount =  { $gte: parseInt(searchParam.max_discount) }
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $project: {
          discount_type: true,
          discount_value: true,
          min_order: true,
          max_discount: true,
          start_date: true,
          end_date: true,
          code: true,
          created_at: true,
          info: true,
          tnc: true,
          supplier_id: true,
          buyer_ids: true,
          is_deleted: true,
          is_active: true,
          unique_use: true,
          max_usage: true,
        },
      },
    ]

    const couponData = await CouponModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      couponData.reverse()
    }
    return Promise.resolve(couponData)
  }

  async getSupportTicketByAdmin(
    user_id: string,
    searchParam: IGetSupportTicketByAdmin
  ) {
    let query: { [k: string]: any } = {}
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.support_ticket_status)) {
      query.support_ticket_status = searchParam.support_ticket_status
    }
    if (!isNullOrUndefined(searchParam.client_id)) {
      query.client_id = new ObjectId(searchParam.client_id)
    }
    if (!isNullOrUndefined(searchParam.severity)) {
      query.severity = searchParam.severity
    }
    if (!isNullOrUndefined(searchParam.subject)) {
      query.subject = searchParam.subject
    }
    if(!isNullOrUndefined(searchParam.ticket_id)){
      query.ticket_id = searchParam.ticket_id
    }
    if(!isNullOrUndefined(searchParam.client_type)){
      query.client_type = searchParam.client_type
    }

    if(!isNullOrUndefined(searchParam.buyer_name)){
      let buyerData = await BuyerUserModel.findOne({
        full_name : searchParam.buyer_name
      })
      if(!isNullOrUndefined(buyerData)){
        query.client_id = new ObjectId(buyerData._id)
      }else {
        query.client_id = ""
      }
    }

    if (!isNullOrUndefined(searchParam.company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: searchParam.company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
          {
            client_id: new ObjectId(user_id1),
          },
        ]
      } else {
        query.client_id = ""
      }
    }

    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.ticket_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.ticket_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.ticket_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "client_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "client_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "logistics_user",
          localField: "client_id",
          foreignField: "_id",
          as: "logistics_user",
        },
      },
      {
        $unwind: { path: "$logistics_user", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          question_type: true,
          severity: true,
          subject: true,
          description: true,
          attachments: true,
          support_ticket_status: true,
          order_id: true,
          created_at: true,
          created_by_id: true,
          created_by_type: true,
          client_id: true,
          client_type: true,
          "buyer.full_name": 1,
          "buyer.company_name": 1,
          "logistics_user.full_name": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1
        },
      },
    ]

    const ticketData = await SupportTicketModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      ticketData.reverse()
    }
    return Promise.resolve(ticketData)
  }

  async flyAshsourceList(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {}

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.region_id)) {
      query.region_id = new ObjectId(searchParam.region_id)  
    }
    
    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.fly_Ash_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.fly_Ash_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.fly_Ash_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      { $match: query },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          fly_ash_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await FlyAshSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async getGstSlab() {
    return await GstSlabModel.find()
  }

  async getMarginRateList() {
    return await MarginRateModel.find()
  }

  async SandsourceList(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {}

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.region_id)) {
      query.region_id = new ObjectId(searchParam.region_id)  
    }
    
    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.sand_source_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.sand_source_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.sand_source_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      { $match: query },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          sand_source_name: 1,
          region_id: 1,
          created_at: 1,
          created_by_id: 1,
          updated_at: 1,
          updated_by_id: 1,
          region_name: "$state.state_name",
        },
      },
    ]

    const sourceData = await SandSourceModel.aggregate(aggregateArr)
    if (isNullOrUndefined(sourceData)) {
      return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
    }

    return Promise.resolve(sourceData)
  }

  async getTM(user_id: string, searchParam: IGetTMList) {
    const query: { [k: string]: any } = {}

    // query.user_id = new ObjectId(user_id)

    let limit: number = 10
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.TM_sub_category_id)) {
      query.TM_sub_category_id = new ObjectId(searchParam.TM_sub_category_id)
    }

    if (!isNullOrUndefined(searchParam.is_gps_enabled)) {
      query.is_gps_enabled = searchParam.is_gps_enabled
    }

    if (!isNullOrUndefined(searchParam.is_insurance_active)) {
      query.is_insurance_active = searchParam.is_insurance_active
    }

    if (!isNullOrUndefined(searchParam.user_id)) {
      query.user_id = searchParam.user_id
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number
    }

    if (!isNullOrUndefined(searchParam.min_trip_price) && 
    !isNullOrUndefined(searchParam.min_trip_price_range_value) &&
    searchParam.min_trip_price_range_value === "less") {
      query.min_trip_price = {$lte : parseInt(searchParam.min_trip_price)}
    }

    if (!isNullOrUndefined(searchParam.min_trip_price) && 
    !isNullOrUndefined(searchParam.min_trip_price_range_value) &&
    searchParam.min_trip_price_range_value === "more") {
      query.min_trip_price = {$gte : parseInt(searchParam.min_trip_price)}
    }

    if (!isNullOrUndefined(searchParam.min_delivery_range) &&
    !isNullOrUndefined(searchParam.min_delivery_range_value) &&
    searchParam.min_delivery_range_value === "less" ) {
      query.delivery_range = {$lte: parseInt(searchParam.min_delivery_range)}
    }

    if (!isNullOrUndefined(searchParam.min_delivery_range) &&
    !isNullOrUndefined(searchParam.min_delivery_range_value) &&
    searchParam.min_delivery_range_value === "more" ) {
      query.delivery_range = {$gte: parseInt(searchParam.min_delivery_range)}
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number
    }

    if (!isNullOrUndefined(searchParam.company_name)) {
      let vendorData = await VendorUserModel.findOne({
        company_name: searchParam.company_name,
      })
      if (!isNullOrUndefined(vendorData)) {
        let user_id1 = vendorData._id
        query.$or = [
          {
            vendor_id: new ObjectId(user_id1),
          },
          {
            sub_vendor_id: new ObjectId(user_id1),
          },
          {
            master_vendor_id: new ObjectId(user_id1),
          },
        ]
      } else {
        query.vendor_id = ""
      }
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.start_date) && searchParam.start_date !== "") {
      query.TM_date = { $gte: searchParam.start_date }
    }
    if (!isNullOrUndefined(searchParam.end_date) && searchParam.end_date !== "") {
      query.TM_date = { $lte: searchParam.end_date }
    }
    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.TM_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    console.log("query", query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: {
          path: "$TM_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          business_name: "$address.business_name",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: 1,
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.min_load_capacity": 1,
          "TM_sub_category.max_load_capacity": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async listAllTMCategory() {
    return TMCategoryModel.find()
  }

  async listTMSubCategory(searchParam?: any) {
    let query: { [k: string]: any } = {}
 
    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.load_capacity)) {
      query.load_capacity = parseInt(searchParam.load_capacity)
    }
    console.log("query", query)
    return TMSubCategoryModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      // {
      //   $limit: 10,
      // },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: { path: "$TM_category", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          sub_category_name: 1,
          TM_category_id: 1,
          load_capacity: 1,
          weight_unit_code: 1,
          weight_unit: 1,
          created_at: 1,
          "TM_category.category_name": 1,
        },
      },
    ])
  }
  
}
