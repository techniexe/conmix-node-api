import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode, Severity } from "../../utilities/config"

export const buyerSearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    search:  Joi.string(),
    user_id:  Joi.string(),
    company_name:  Joi.string(),
    account_type:  Joi.string(),
  }),
}

export interface IBuyerSearchByAdmin {
  before?: string
  after?: string
  is_export?: boolean
  start_date?: string
  end_date?: string
  search?: string,
  user_id?: string,
  company_name?: string,
  account_type?: string
}

export interface IBuyerListingByAdminReq extends IAuthenticatedRequest {
  query: IBuyerSearchByAdmin
}

export const supplierSearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    search: Joi.string(),
    company_type: Joi.string(),
  }),
}

export interface ISupplierSearchByAdmin {
  before?: string
  after?: string
  is_export?: boolean
  start_date?: string
  end_date?: string
  search?: string
  company_type?: string

}

export interface ISupplierListingByAdminReq extends IAuthenticatedRequest {
  query: ISupplierSearchByAdmin
}

export const logisticsSearchByAdminReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
  }),
}

export interface ILogisticsSearchByAdmin {
  before?: string
  after?: string
}

export interface ILogiscticsListingByAdminReq extends IAuthenticatedRequest {
  query: ILogisticsSearchByAdmin
}

export const buyerOrderReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    product_id: Joi.string().regex(objectIdRegex),
    supplier_id: Joi.string().regex(objectIdRegex),
    buyer_id: Joi.string().regex(objectIdRegex),
    product_category_id: Joi.string().regex(objectIdRegex),
    product_sub_category_id: Joi.string().regex(objectIdRegex),
    status: Joi.string(),
    payment_status: Joi.string(),
    gateway_transaction_id: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    buyer_order_id: Joi.string(),
    buyer_name: Joi.string(),
  }),
}

export interface IGetBuyerOrderRequest {
  before?: string
  after?: string
  month?: string
  year?: number
  product_id?: string
  supplier_id?: string
  buyer_id?: string
  product_category_id?: string
  product_sub_category_id?: string
  status?: string
  payment_status?: string
  gateway_transaction_id?: string
  is_export?: boolean
  start_date: string
  end_date: string
  buyer_order_id: string,
  buyer_name: string
}

export interface IGetBuyerOrderRequest extends IAuthenticatedRequest {
  query: IGetBuyerOrderRequest
}

export const supplierOrderReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    product_id: Joi.string().regex(objectIdRegex),
    supplier_id: Joi.string().regex(objectIdRegex),
    buyer_id: Joi.string().regex(objectIdRegex),
    product_category_id: Joi.string().regex(objectIdRegex),
    product_sub_category_id: Joi.string().regex(objectIdRegex),
    status: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    
  }),
}

export interface IGetSupplierOrderRequest {
  before?: string
  after?: string
  month?: string
  year?: number
  product_id?: string
  supplier_id?: string
  buyer_id?: string
  product_category_id?: string
  product_sub_category_id?: string
  status?: string
  is_export?: boolean
  start_date: string
  end_date: string
}

export interface IGetSupplierOrderRequest extends IAuthenticatedRequest {
  query: IGetSupplierOrderRequest
}

export const logisticsOrderReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    product_id: Joi.string().regex(objectIdRegex),
    payment_status: Joi.string(),
  }),
}

export interface IGetLogisticsOrderRequest {
  before?: string
  after?: string
  month?: string
  year?: number
  product_id?: string
  payment_status?: string
}

export interface IGetLogisticsOrderRequest extends IAuthenticatedRequest {
  query: IGetLogisticsOrderRequest
}

export const getProductListSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    supplier_id: Joi.string().regex(objectIdRegex),
    product_category_id: Joi.string().regex(objectIdRegex),
    product_sub_category_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetProductListRequest {
  before?: string
  after?: string
  supplier_id?: string
  product_category_id?: string
  product_sub_category_id?: string
}

export interface IGetProductListRequest extends IAuthenticatedRequest {
  query: IGetProductListRequest
}

export interface IGetBuyerOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const buyerOrderByIdReqSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
}

export interface IGetSupplierOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const supplierOrderByIdReqSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
}

export interface IGetLogisticsOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const logisticsOrderByIdReqSchema: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
}

export interface IGetUserVehiclesDetails {
  before?: string
  after?: string
  logistics_id: string
  is_active?: boolean
  vehicle_category_id?: string
  vehicle_sub_category_id?: string
}

export interface IGetUserVehicleRequest extends IAuthenticatedRequest {
  query: IGetUserVehiclesDetails
}

export const getUserVehiclesSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    logistics_id: Joi.string().regex(objectIdRegex),
    is_active: Joi.boolean(),
    vehicle_category_id: Joi.string(),
    vehicle_sub_category_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetDesignMixture {
  user_id?: string
  grade_id?: string
  before?: string
  after?: string
  is_export?: boolean
  start_date?: string
  end_date?: string
  company_name?: string
}

export interface IGetDesignMixtureRequest extends IAuthenticatedRequest {
  query: IGetDesignMixture
}

export const getDesignMixtureSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    company_name: Joi.string(),
  }),
}

export interface IGetDesignMixtureDetailsRequest extends IAuthenticatedRequest {
  params: {
    designMixtureId: string
  }
}

export const getDesignMixtureDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    designMixtureId: Joi.string(),
  }),
}

export interface IGetConcretePumpList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  concrete_pump_category_id?: string
  user_id?: string
  start_date: string
  end_date: string
}

export interface IGetConcretePumpRequest extends IAuthenticatedRequest {
  query: IGetConcretePumpList
}

export const getConcretePumpSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    concrete_pump_category_id: Joi.string().regex(objectIdRegex),
    user_id: Joi.string().regex(objectIdRegex),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetCouponDetails {
  is_deleted?: boolean
  is_active?: boolean
  code?: string
  buyer_id?: string
  supplier_id?: string
  discount_type?: string
  before?: string
  after?: string
  discount_value?: any
  start_date?: Date
  end_date?: Date
  min_order?: any
  max_discount?: any
  min_order_range_value?: any
  max_discount_range_value?: any
}

export interface IGetCouponListRequest extends IAuthenticatedRequest {
  body: IGetCouponDetails
}

export const getCouponSchema: IRequestSchema = {
  query: Joi.object().keys({
    is_deleted: Joi.boolean(),
    buyer_id: Joi.string().regex(objectIdRegex),
    supplier_id: Joi.string().regex(objectIdRegex),
    discount_type: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    code: Joi.string(),
    discount_value: Joi.number(),
    start_date: Joi.date().iso(),
    end_date: Joi.date().iso(),
    min_order: Joi.number(),
    max_discount: Joi.number(),
    is_active: Joi.boolean(),
    min_order_range_value: Joi.string(),
    max_discount_range_value: Joi.string(),
  }),
}

export interface IGetSupportTicketByAdmin {
  support_ticket_status?: string
  before?: string
  after?: string
  client_id?: string
  subject?: string
  severity?: string
  ticket_id?: string
  client_type?: string
  buyer_name?: string
  company_name?: string
  start_date?: string
  end_date?: string
}

export interface IGetSupportTicketByAdminRequest extends IAuthenticatedRequest {
  query: IGetSupportTicketByAdmin
}

export const getSupportTicketByAdminSchema: IRequestSchema = {
  query: Joi.object().keys({
    support_ticket_status: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex),
    subject: Joi.string(),
    severity: Joi.string().valid(Severity),
    ticket_id: Joi.string(),
    client_type: Joi.string(),
    buyer_name: Joi.string(),
    company_name: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetTMList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  TM_category_id?: string
  TM_sub_category_id?: string
  is_gps_enabled?: boolean
  is_insurance_active?: boolean
  user_id?: string
  TM_rc_number?: string
  company_name?: string
  min_trip_price?: any
  min_delivery_range?: any
  min_delivery_range_value?: any
  min_trip_price_range_value?: any
  start_date?: string
  end_date?: string
}

export interface IGetTMRequest extends IAuthenticatedRequest {
  query: IGetTMList
}

export const getTMSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    TM_category_id: Joi.string(),
    TM_sub_category_id: Joi.string().regex(objectIdRegex),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
    user_id: Joi.string().regex(objectIdRegex),
    TM_rc_number: Joi.string(),
    company_name: Joi.string(),
    min_trip_price: Joi.string(),
    min_delivery_range : Joi.number(),
    min_delivery_range_value: Joi.string(),
    min_trip_price_range_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}