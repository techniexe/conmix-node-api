import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { IAuthenticatedRequest, verifyCustomToken } from "../../middleware/auth-token.middleware"
import { IRequestSchema, validate } from "../../middleware/joi.middleware"
import { AdminTypes } from "../admin.types"
import {
  buyerSearchByAdminReqSchema,
  IBuyerListingByAdminReq,
  supplierSearchByAdminReqSchema,
  ISupplierListingByAdminReq,
  buyerOrderReqSchema,
  IGetBuyerOrderRequest,
  supplierOrderReqSchema,
  IGetSupplierOrderRequest,
  buyerOrderByIdReqSchema,
  IGetBuyerOrderByIdRequest,
  IGetSupplierOrderByIdRequest,
  supplierOrderByIdReqSchema,
  getDesignMixtureSchema,
  IGetDesignMixtureRequest,
  getDesignMixtureDetailsSchema,
  IGetDesignMixtureDetailsRequest,
  getCouponSchema,
  IGetCouponListRequest,
  getSupportTicketByAdminSchema,
  getTMSchema,
  IGetTMRequest,
} from "./admin.report.schema"
import { AdminReportRepository } from "./admin.report.repository"
import { Response, NextFunction } from "express"

@injectable()
@controller("/report", verifyCustomToken("admin"))
export class AdminReportController {
  constructor(
    @inject(AdminTypes.AdminReportRepository)
    private reportRepo: AdminReportRepository
  ) {}

  // List buyer order and order items.
  @httpGet("/buyerorder/:orderId", validate(buyerOrderByIdReqSchema))
  async buyerOrderById(
    req: IGetBuyerOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.buyerOrderById(
        req.params.orderId,
        req.query.buyer_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  //  List supplier order and order items. [for getting item, supplier id required.]
  @httpGet("/vendorOrder/:orderId", validate(supplierOrderByIdReqSchema))
  async vendorOrderById(
    req: IGetSupplierOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.vendorOrderById(
        req.params.orderId,
        req.query.supplier_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/design_mix/:designMixtureId",
    validate(getDesignMixtureDetailsSchema)
  )
  async getDesignMixtureDetails(
    req: IGetDesignMixtureDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { designMixtureId } = req.params
      const data = await this.reportRepo.getDesignMixtureDetails(
        designMixtureId,
        req.query
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
/************************************************************ */

  @httpGet("/admix_brand")
  async getAdmixBrand(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, start_date, end_date } = req.query
    const data = await this.reportRepo.getAdmixBrand(search, before, after, start_date, end_date)
    res.json({ data })
  }

  @httpGet("/admix_types")
  async getCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, brand_id, start_date, end_date } = req.query
    const data = await this.reportRepo.getCategories(
      search,
      before,
      after,
      brand_id,
      start_date,
      end_date
    )
    res.json({ data })
  }

  @httpGet("/agg_sand_cat")
  async getAggSandCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after,start_date, end_date } = req.query
    const data = await this.reportRepo.getAggSandCategories(search, before, after, start_date, end_date)
    res.json({ data })
  }

  @httpGet("/agg_sand_subcategory")
  async getAggSandSubCategories(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { search, before, after, category_id, sub_category_name, margin_rate_id, start_date, end_date } = req.query
    const data = await this.reportRepo.getAggSandSubCategories(
      category_id,
      search,
      before,
      after,
      sub_category_name,
      margin_rate_id,
      start_date,
      end_date
    )
    res.json({ data })
  }

  @httpGet("/agg_source")
  async aggsourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.reportRepo.aggsourceList(req.user.uid, req.query)
    res.json({ data })
  }

    // List buyer order and order items.
  @httpGet("/buyerorder", validate(buyerOrderReqSchema))
  async buyerOrder(
    req: IGetBuyerOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.buyerOrder(
        req.query.month,
        req.query.year,
        req.query.buyer_id,
        req.query.status,
        req.query.before,
        req.query.after,
        req.query.payment_status,
        req.query.gateway_transaction_id,
        req.query.is_export,
        req.query.start_date,
        req.query.end_date,
        req.query.buyer_order_id,
        req.query.buyer_name      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

    // Buyer list with no of orders.
  @httpGet("/buyerlisting", validate(buyerSearchByAdminReqSchema))
  async buyerListing(req: IBuyerListingByAdminReq, res: Response) {
      const data = await this.reportRepo.buyerListing(req.query)
      res.json({ data })
  }

  @httpGet("/cement_brand")
  async getCementBrand(
      req: IAuthenticatedRequest,
      res: Response,
      next: NextFunction
    ) {
      const { search, before, after, start_date, end_date } = req.query
      const data = await this.reportRepo.getCementBrand(
        search,
        before,
        after,
        start_date,
        end_date
      )
      res.json({ data })
  }

  @httpGet("/cement_grade")
  async getCementgrade(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after, start_date, end_date } = req.query
    const data = await this.reportRepo.getCementgrade(search, before, after,  start_date,
      end_date)
    res.json({ data })
  }

  @httpGet("/concrete_grade")
  async getCg(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    const { search, before, after, start_date, end_date } = req.query
    const data = await this.reportRepo.getCg(search, before, after, start_date, end_date)

    console.log("data", data)
    res.json({ data })
  }

  // @httpGet("/concrete_pump", validate(getConcretePumpSchema))
  // async getConcretePump(
  //   req: IGetConcretePumpRequest,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   console.log(req.user.uid)
  //   const data = await this.reportRepo.getConcretePump(
  //     req.user.uid,
  //     req.query
  //   )
  //   res.json({ data })
  // }

  @httpGet("/concrete_pump_category")
  async listAllConcretePumpCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.reportRepo.listAllConcretePumpCategory()
    res.json({ data })
  }

  @httpGet("/coupon", validate(getCouponSchema))
  async getCouponList(
    req: IGetCouponListRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.getCouponList(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/support_ticket", validate(getSupportTicketByAdminSchema))
  async getSupportTicketByAdmin(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.reportRepo.getSupportTicketByAdmin(
        uid,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/flyAsh_source")
  async flyAshsourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.reportRepo.flyAshsourceList(req.user.uid, req.query)
    res.json({ data })
  }

  @httpGet("/gst_slab")
  async getGstSlab(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.getGstSlab()
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/margin_rate")
  async getMarginRateList(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.getMarginRateList()
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

    // Supplier list with no of orders.
  @httpGet("/vendorlisting", validate(supplierSearchByAdminReqSchema))
  async vendorListing(req: ISupplierListingByAdminReq, res: Response) {
    const data = await this.reportRepo.vendorListing(req.query)
    res.json({ data })
    }

  @httpGet("/sand_source")
  async SandsourceList(req: IAuthenticatedRequest, res: Response) {
    const data = await this.reportRepo.SandsourceList(req.user.uid, req.query)
    res.json({ data })
  }  

  @httpGet("/vendorOrder", validate(supplierOrderReqSchema))
  async vendorOrder(
    req: IGetSupplierOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.vendorOrder(
        req.query.month,
        req.query.year,
        req.query.product_id,
        req.query.supplier_id,
        req.query.buyer_id,
        req.query.status,
        req.query.before,
        req.query.after,
        req.query.is_export,
        req.query.start_date,
        req.query.end_date
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/designMixList", validate(getDesignMixtureSchema))
  async getDesignMixture(
    req: IGetDesignMixtureRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.reportRepo.getDesignMixture(req.query)
    res.json({ data })
  }

  @httpGet("/TM", validate(getTMSchema))
  async getTM(req: IGetTMRequest, res: Response, next: NextFunction) {
    console.log(req.user.uid)
    const data = await this.reportRepo.getTM(req.user.uid, req.query)
    res.json({ data })
  }

  @httpGet("/TM_category")
  async listAllTMCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.reportRepo.listAllTMCategory()
    res.json({ data })
  }

  @httpGet("/TM_subCategory")
  async listSubCategory(
    req: IRequestSchema,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.reportRepo.listTMSubCategory(req.query)
    res.json({ data })
  }
}
