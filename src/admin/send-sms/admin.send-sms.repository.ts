import { injectable, inject } from "inversify"

import { ISendSms } from "./admin.send-sms.req-schema"
import { AdminTypes } from "../admin.types"
import { TwilioService } from "../../utilities/twilio-service"

@injectable()
export class SendSmsRepository {
  constructor(
    @inject(AdminTypes.TwilioService) private twilioService: TwilioService
  ) {}
  async sendSms(smsData: ISendSms) {
    try {
      await this.twilioService.sendSMS(
        smsData.to_mobileNumber,
        smsData.text,
        smsData
      )
      return Promise.resolve()
    } catch (err) {
      return Promise.resolve()
    }
  }
}
