import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ISendSms {
  user_id?: string
  user_type?: string
  to_mobileNumber: string
  text: string
  sent_at: Date
  send_status: string
  send_error?: string
}

export interface ISendSmsReq extends IAuthenticatedRequest {
  body: ISendSms
}

export const sendSmsSchema: IRequestSchema = {
  body: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    user_type: Joi.string(),
    to_mobileNumber: Joi.string().required(),
    text: Joi.string().required(),
    send_status: Joi.string(),
    send_error: Joi.string(),
  }),
}
