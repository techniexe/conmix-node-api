import { controller, httpPost } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { sendSmsSchema, ISendSmsReq } from "./admin.send-sms.req-schema"
import { SendSmsRepository } from "./admin.send-sms.repository"

@injectable()
@controller("/send-sms", verifyCustomToken("admin"))
export class SendSmsController {
  constructor(
    @inject(AdminTypes.SendSmsRepository) private sendSmsRepo: SendSmsRepository
  ) {}

  @httpPost("", validate(sendSmsSchema))
  async sendSms(req: ISendSmsReq, res: Response, next: NextFunction) {
    try {
      await this.sendSmsRepo.sendSms(req.body)
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }
}
