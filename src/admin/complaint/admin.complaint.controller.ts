import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { validate } from "../../middleware/joi.middleware"
import { Response, NextFunction } from "express"
import { ComplaintRepository } from "./admin.complaint.repository"
import { AdminTypes } from "../admin.types"
import {
  IGetComplaintRequest,
  getComplaintSchema,
  editComplaintSchema,
  IEditComplaintRequest,
} from "./admin.complaint.req.schema"

@controller("/complaint", verifyCustomToken("admin"))
@injectable()
export class ComplaintController {
  constructor(
    @inject(AdminTypes.ComplaintRepository)
    private adminRepo: ComplaintRepository
  ) {}

  @httpGet("/", validate(getComplaintSchema))
  async getComplaints(
    req: IGetComplaintRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const {
        read_by_admin,
        user_id,
        sub_category_id,
        search,
        before,
        after,
        order_id,
      } = req.query
      const data = await this.adminRepo.getComplaints(
        read_by_admin,
        user_id,
        sub_category_id,
        search,
        before,
        after,
        order_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:complaintId", validate(editComplaintSchema))
  async editComplaint(
    req: IEditComplaintRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { complaintId } = req.params
      await this.adminRepo.editComplaint(
        req.user.uid,
        complaintId,
        req.body.authentication_code,
        req.body.read_by_admin
      )
      return res.json(202)
    } catch (err) {
      next(err)
    }
  }
}
