import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ComplaintModel } from "../../model/complaint.model"
import { InvalidInput } from "../../utilities/customError"
import { AuthModel } from "../../model/auth.model"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"

@injectable()
export class ComplaintRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async getComplaints(
    read_by_admin?: boolean,
    user_id?: string,
    sub_category_id?: string,
    search?: string,
    before?: string,
    after?: string,
    order_id?: string
  ) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(read_by_admin)) {
      query.read_by_admin = read_by_admin
    }
    if (!isNullOrUndefined(user_id) && user_id !== "") {
      query.user_id = user_id
    }
    if (!isNullOrUndefined(sub_category_id) && sub_category_id !== "") {
      query.sub_category_id = sub_category_id
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(order_id)) {
      query.order_id = order_id
    }
    return ComplaintModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: { path: "$buyer" },
      },

      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },

      {
        $project: {
          user_id: 1,
          order_id: 1,
          complaint_text: 1,
          category_id: 1,
          sub_category_id: 1,
          read_by_admin: 1,
          created_at: 1,
          read_unread_at: 1,
          "productSubCategory.sub_category_name": 1,
          "buyer.full_name": 1,
        },
      },
    ])
  }

  async editComplaint(
    user_id: string,
    complaintId: string,
    authentication_code: string,
    read_by_admin?: boolean
  ) {
    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }

    if (!isNullOrUndefined(read_by_admin)) {
      updt.$set.read_by_admin = read_by_admin
      updt.$set.read_unread_at = Date.now()
    }
    if (Object.keys(updt.$set).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const res = await ComplaintModel.updateOne({ _id: complaintId }, updt)

    if (res.n === 0) {
      const err1 = new InvalidInput("No matched record found.")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.READ_COMPLAINT_BY_ADMIN,
      "Admin read complaint."
    )

    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })

    return Promise.resolve()
  }
}
