import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetComplaintRequest extends IAuthenticatedRequest {
  query: {
    read_by_admin?: boolean
    user_id?: string
    sub_category_id?: string
    search?: string
    before?: string
    after?: string
    order_id?: string
  }
}

export const getComplaintSchema: IRequestSchema = {
  query: Joi.object().keys({
    read_by_admin: Joi.boolean().valid([true, false]),
    user_id: Joi.string().regex(objectIdRegex),
    sub_category_id: Joi.string().regex(objectIdRegex),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    order_id: Joi.string(),
  }),
}

export interface IEditComplaintRequest extends IAuthenticatedRequest {
  body: {
    read_by_admin: boolean
    authentication_code: string
  }
}

export const editComplaintSchema: IRequestSchema = {
  params: Joi.object().keys({
    complaintId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    read_by_admin: Joi.boolean().valid([true, false]),
    authentication_code: Joi.string().required(),
  }),
}
