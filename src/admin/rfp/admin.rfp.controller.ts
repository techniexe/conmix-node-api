import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { AdminTypes } from "../admin.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  getRfpSchema,
  IGetRfpDetailsReq,
  updateRfp,
  IUpdateRfpRequest,
} from "./admin.rfp.req-schema"
import { RfpRepository } from "./admin.rfp.repository"

@injectable()
@controller("/rfp", verifyCustomToken("admin"))
export class RfpController {
  constructor(
    @inject(AdminTypes.RfpRepository) private rfpRepo: RfpRepository
  ) {}

  @httpGet("/", validate(getRfpSchema))
  async getRfp(req: IGetRfpDetailsReq, res: Response, next: NextFunction) {
    try {
      const {
        user_id,
        contact_person_id,
        before,
        after,
        category_name,
        sub_category_name,
        status,
      } = req.query
      const data = await this.rfpRepo.getRfp(
        user_id,
        contact_person_id,
        before,
        after,
        category_name,
        sub_category_name,
        status
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:rfp_id", validate(updateRfp))
  async updateRfp(req: IUpdateRfpRequest, res: Response, next: NextFunction) {
    await this.rfpRepo.updateRfp(req.user.uid, req.params.rfp_id, req.body)
    res.sendStatus(201)
  }
}
