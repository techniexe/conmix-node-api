import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetRfpDetailsReq extends IAuthenticatedRequest {
  query: {
    user_id?: string
    contact_person_id?: string
    before?: string
    after?: string
    category_name?: string
    sub_category_name?: string
    status?: string
  }
}

export const getRfpSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    contact_person_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    category_name: Joi.string(),
    sub_category_name: Joi.string(),
    status: Joi.string().valid(["Unverified", "Verified"]),
  }),
}

export interface IUpdateRfp {
  status: string
}

export interface IUpdateRfpRequest extends IAuthenticatedRequest {
  body: IUpdateRfp
  params: {
    rfp_id: string
  }
}

export const updateRfp: IRequestSchema = {
  body: Joi.object().keys({
    status: Joi.string().valid(["Unverified", "Verified"]),
  }),
  params: Joi.object().keys({
    rfp_id: Joi.string().regex(objectIdRegex).required(),
  }),
}
