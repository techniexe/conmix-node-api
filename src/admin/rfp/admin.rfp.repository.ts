import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { RfpModel } from "../../model/rfp.model"
import { escapeRegExp } from "../../utilities/mongoose.utilities"
import { ObjectId } from "bson"
import { IUpdateRfp } from "./admin.rfp.req-schema"
import { InvalidInput } from "../../utilities/customError"
import { AdminTypes } from "../admin.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"

@injectable()
export class RfpRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}
  async getRfp(
    user_id?: string,
    contact_person_id?: string,
    before?: string,
    after?: string,
    category_name?: string,
    sub_category_name?: string,
    status?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(category_name)) {
      query.category_name = new RegExp(escapeRegExp(category_name), "i")
    }

    if (!isNullOrUndefined(sub_category_name)) {
      query.sub_category_name = new RegExp(escapeRegExp(sub_category_name), "i")
    }

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(contact_person_id)) {
      query.contact_person_id = new ObjectId(contact_person_id)
    }

    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }

    if (!isNullOrUndefined(status)) {
      query.status = status
    }

    return RfpModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { requested_at: 1 },
      },
      {
        $limit: 40,
      },

      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplierDetails",
        },
      },
      {
        $unwind: { path: "$supplierDetails", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "contact",
          localField: "contact_person_id",
          foreignField: "_id",
          as: "contactDetails",
        },
      },
      {
        $unwind: { path: "$contactDetails", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $project: {
          _id: 1,
          user_id: 1,
          category_name: 1,
          sub_category_name: 1,
          contact_person_id: 1,
          pickup_address_id: 1,
          description: 1,
          status: 1,
          requested_at: 1,
          "supplierDetails._id": 1,
          "supplierDetails.full_name": 1,
          "supplierDetails.mobile_number": 1,
          "supplierDetails.email": 1,
          "contactDetails.person_name": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }

  async updateRfp(user_id: string, rfp_id: string, rfpData: IUpdateRfp) {
    // const result = await AuthModel.findOne({
    //   code: orderData.authentication_code,
    // })
    // if (isNullOrUndefined(result)) {
    //   return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    // }

    const editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(rfpData.status)) {
      editDoc.status = rfpData.status
    }

    if (Object.keys(editDoc).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }
    const res = await RfpModel.findOneAndUpdate(
      { _id: rfp_id },
      { $set: editDoc }
    )
    if (res === null) {
      const err1 = new InvalidInput("No matched record found.")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_RFP_BY_ADMIN,
      `Admin Updated details of RFP id ${rfp_id}.`,
      res,
      editDoc
    )

    // Send sms and email to associated supplier for notify.

    return Promise.resolve()
  }
}
