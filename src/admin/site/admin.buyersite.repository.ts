import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { SiteModel } from "../../model/site.model"

@injectable()
export class BuyerSiteRepository {
  async getBuyerSitelist(
    user_id?: string,
    search?: string,
    before?: string,
    after?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query["user.user_id"] = user_id
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    console.log(query)

    return SiteModel.find(query).limit(10)
  }
}
