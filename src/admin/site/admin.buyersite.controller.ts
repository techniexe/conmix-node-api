import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminTypes } from "../admin.types"
import { NextFunction, Response } from "express"
import { BuyerSiteRepository } from "./admin.buyersite.repository"
import { validate } from "../../middleware/joi.middleware"
import { getBuyerSite, IGetBuyerSite } from "./admin.buyersite.req-schema"

@injectable()
@controller("/buyersitelist", verifyCustomToken("admin"))
export class BuyerSiteController {
  constructor(
    @inject(AdminTypes.BuyerSiteRepository)
    private buyerSiteRepo: BuyerSiteRepository
  ) {}

  @httpGet("/", validate(getBuyerSite))
  async getBuyerSitelist(
    req: IGetBuyerSite,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after, user_id } = req.query
      const data = await this.buyerSiteRepo.getBuyerSitelist(
        user_id,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      return next(err)
    }
  }
}
