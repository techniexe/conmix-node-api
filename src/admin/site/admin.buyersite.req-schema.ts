import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetBuyerSite extends IAuthenticatedRequest {
  query: {
    user_id?: string
    search?: string
    before?: string
    after?: string
  }
}

export const getBuyerSite: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}
