import { injectable, inject } from "inversify"
import { controller, httpGet, httpPatch } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { AdminDesignMixtureRepository } from "./admin.design_mix.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  getDesignMixtureSchema,
  IGetDesignMixtureRequest,
  getDesignMixtureDetailsSchema,
  IGetDesignMixtureDetailsRequest,
  EditdesignMixSchema,
  IEditDesignMixRequest,
  EditDesignMixVariantSchema,
  IEditDesignMixVariantRequest,
  getDesignMixtureVariantDetailsSchema,
  IGetDesignMixtureVariantDetailsRequest,
} from "./admin.design_mix.req-schema"

import { AdminTypes } from "../admin.types"

@injectable()
@controller("/design_mix", verifyCustomToken("admin"))
export class AdminDesignMixtureController {
  constructor(
    @inject(AdminTypes.AdminDesignMixtureRepository)
    private DesignMixRepo: AdminDesignMixtureRepository
  ) {}

  @httpGet("/", validate(getDesignMixtureSchema))
  async getDesignMixture(
    req: IGetDesignMixtureRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.DesignMixRepo.getDesignMixture(req.query)
      return res.json({ data })
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  @httpGet("/:designMixtureId", validate(getDesignMixtureDetailsSchema))
  async getDesignMixtureDetails(
    req: IGetDesignMixtureDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { designMixtureId } = req.params
      const data = await this.DesignMixRepo.getDesignMixtureDetails(
        designMixtureId
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   *  Edit  details.
   */
  @httpPatch("/:designMixtureId", validate(EditdesignMixSchema))
  async editDesignMixDetails(
    req: IEditDesignMixRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.DesignMixRepo.editDesignMixDetails(
        req.user.uid,
        req.params.designMixtureId,
        req.body
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  /**
   *  Edit  details.
   */
  @httpPatch(
    "/variant/:design_mix_variant_id",
    validate(EditDesignMixVariantSchema)
  )
  async editDesignMixVariantVariantDetails(
    req: IEditDesignMixVariantRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.DesignMixRepo.editDesignMixVariantDetails(
        req.user.uid,
        req.params.design_mix_variant_id,
        req.body
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  @httpGet(
    "/variant/:design_mix_variant_id",
    validate(getDesignMixtureVariantDetailsSchema)
  )
  async getDesignMixtureVariantDetails(
    req: IGetDesignMixtureVariantDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { design_mix_variant_id } = req.params
      const data = await this.DesignMixRepo.getDesignMixtureVariantDetails(
        design_mix_variant_id,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
