import { AddressModel } from "../../model/address.model"
import { SandSourceModel } from "../../model/sand_source.model"
import { injectable, inject } from "inversify"
import { CommonService } from "../../utilities/common.service"
import {
  IGetDesignMixture,
  IEditDesignMix,
  IEditDesignMixVariant,
} from "./admin.design_mix.req-schema"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { VendorUserModel } from "../../model/vendor.user.model"
import { DesignMixtureModel } from "../../model/design_mixture.model "
import { AccessTypes } from "../../model/access_logs.model"
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { ObjectId } from "bson"
import { CementBrandModel } from "../../model/cement_brand.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { CityModel, StateModel } from "../../model/region.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import { AdminTypes } from "../admin.types"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class AdminDesignMixtureRepository {
  constructor(
    @inject(AdminTypes.CommonService) private commonService: CommonService
  ) {}

  async getDesignMixture(searchParam: IGetDesignMixture) {
    const query: { [k: string]: any } = {}
    let sort = { created_at: -1 }
    const limit = 10
    if (!isNullOrUndefined(searchParam.export)) {
      if (!isNullOrUndefined(searchParam.grade_id)) {
        query.grade_id = new ObjectId(searchParam.grade_id)
      }

      if (!isNullOrUndefined(searchParam.company_name)) {
        let vendorData = await VendorUserModel.findOne({
          company_name: searchParam.company_name,
        })
        if (!isNullOrUndefined(vendorData)) {
          let user_id1 = vendorData._id
          query.$or = [
            {
              vendor_id: new ObjectId(user_id1),
            },
            {
              sub_vendor_id: new ObjectId(user_id1),
            },
            {
              master_vendor_id: new ObjectId(user_id1),
            }
          ]
        } else {
          query.vendor_id = ""
        }
      }

      if (!isNullOrUndefined(searchParam.user_id)) {
        query.vendor_id = searchParam.user_id
      }

      if (!isNullOrUndefined(searchParam.after)) {
        query.created_at = { $gte: new Date(searchParam.after) }
        sort.created_at = 1
      }
    } else {
      if (!isNullOrUndefined(searchParam.grade_id)) {
        query.grade_id = new ObjectId(searchParam.grade_id)
      }

      if (!isNullOrUndefined(searchParam.user_id)) {
        query.vendor_id = searchParam.user_id
      }

      if (!isNullOrUndefined(searchParam.company_name)) {
        let vendorData = await VendorUserModel.findOne({
          company_name: searchParam.company_name,
        })
        if (!isNullOrUndefined(vendorData)) {
          let user_id1 = vendorData._id
          query.$or = [
            {
              vendor_id: new ObjectId(user_id1),
            },
            {
              sub_vendor_id: new ObjectId(user_id1),
            },
            {
              master_vendor_id: new ObjectId(user_id1),
            }
          ]
        } else {
          query.vendor_id = ""
        }
      }
      
      if (!isNullOrUndefined(searchParam.before)) {
        query.created_at = { $lt: new Date(searchParam.before) }
      }
      if (!isNullOrUndefined(searchParam.after)) {
        query.created_at = { $gt: new Date(searchParam.after) }
        sort.created_at = 1
      }
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
        },
      },

      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          "concrete_grade._id": true,
          "concrete_grade.name": true,
          "vendor._id": true,
          "vendor.full_name": true,
          "vendor.company_name": true,
        },
      },
    ]
    if (isNullOrUndefined(searchParam.export)) {
      aggregateArr.push({ $limit: limit })
    }

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      designMixData.reverse()
    }
    return Promise.resolve(designMixData)
  }

  async getDesignMixtureDetails(designMixtureId: string) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(designMixtureId),
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "_id",
          foreignField: "design_mix_id",
          as: "designMixVariantDetails",
        },
      },

      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          designMixVariantDetails: true,
        },
      },
    ]

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)

    console.log(
      "designMixVariantDetails",
      designMixData[0].designMixVariantDetails
    )

    if (designMixData.length > 0) {
      for (
        let i = 0;
        i < designMixData[0].designMixVariantDetails.length;
        i++
      ) {
        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].grade_id
          )
        ) {
        const concreteGradeDoc = await ConcreteGradeModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].grade_id,
        }).lean()
        if (!isNullOrUndefined(concreteGradeDoc)) {
          designMixData[0].designMixVariantDetails[i].concrete_grade_name =
            concreteGradeDoc.name
        }
      }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].cement_brand_id
          )
        ) {
        const cementBrandDoc = await CementBrandModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].cement_brand_id,
        }).lean()
        if (!isNullOrUndefined(cementBrandDoc)) {
          designMixData[0].designMixVariantDetails[i].cement_brand_name =
            cementBrandDoc.name
        }
      }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].sand_source_id
          )
        ) {
        const sandSourceDoc = await SandSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].sand_source_id,
        }).lean()
        if (!isNullOrUndefined(sandSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].sand_source_name =
            sandSourceDoc.sand_source_name
        }
      }
        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].aggregate_source_id
          )
        ) {
          const aggregateSourceDoc = await AggregateSourceModel.findById({
            _id:
              designMixData[0].designMixVariantDetails[i].aggregate_source_id,
          }).lean()
          if (!isNullOrUndefined(aggregateSourceDoc)) {
            designMixData[0].designMixVariantDetails[i].aggregate_source_name =
              aggregateSourceDoc.aggregate_source_name
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].fly_ash_source_id
          )
        ) {
          const flyAshSourceDoc = await FlyAshSourceModel.findById({
            _id: designMixData[0].designMixVariantDetails[i].fly_ash_source_id,
          }).lean()
          if (!isNullOrUndefined(flyAshSourceDoc)) {
            designMixData[0].designMixVariantDetails[i].fly_ash_source_name =
              flyAshSourceDoc.fly_ash_source_name
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i]
              .aggregate1_sub_category_id
          )
        ) {
          const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById(
            {
              _id:
                designMixData[0].designMixVariantDetails[i]
                  .aggregate1_sub_category_id,
            }
          ).lean()
          if (!isNullOrUndefined(aggregate1SubCatDoc)) {
            designMixData[0].designMixVariantDetails[
              i
            ].aggregate1_sub_category_name =
              aggregate1SubCatDoc.sub_category_name
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i]
              .aggregate2_sub_category_id
          )
        ) {
          const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById(
            {
              _id:
                designMixData[0].designMixVariantDetails[i]
                  .aggregate2_sub_category_id,
            }
          ).lean()
          if (!isNullOrUndefined(aggregate2SubCatDoc)) {
            designMixData[0].designMixVariantDetails[
              i
            ].aggregate2_sub_category_name =
              aggregate2SubCatDoc.sub_category_name
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].ad_mixture_brand_id
          )
        ) {
          const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
            _id:
              designMixData[0].designMixVariantDetails[i].ad_mixture_brand_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureBrandDoc)) {
            designMixData[0].designMixVariantDetails[i].admix_brand_name =
              adMixtureBrandDoc.name
          }
        }
        const addressDoc = await AddressModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].address_id,
        }).lean()
        if (!isNullOrUndefined(addressDoc)) {
          designMixData[0].designMixVariantDetails[i].address_line1 =
            addressDoc.line1
          designMixData[0].designMixVariantDetails[i].address_line2 =
            addressDoc.line2
          designMixData[0].designMixVariantDetails[i].pincode =
            addressDoc.pincode
          designMixData[0].designMixVariantDetails[i].location =
            addressDoc.location

          const cityDoc = await CityModel.findById({
            _id: addressDoc.city_id,
          }).lean()

          if (!isNullOrUndefined(cityDoc)) {
            designMixData[0].designMixVariantDetails[i].city_name =
              cityDoc.city_name
          }

          const stateDoc = await StateModel.findById({
            _id: addressDoc.state_id,
          }).lean()

          if (!isNullOrUndefined(stateDoc)) {
            designMixData[0].designMixVariantDetails[i].state_name =
              stateDoc.state_name
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].ad_mixture_category_id
          )
        ) {
          const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
            _id:
              designMixData[0].designMixVariantDetails[i]
                .ad_mixture_category_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureCatDoc)) {
            designMixData[0].designMixVariantDetails[i].admix_category_name =
              adMixtureCatDoc.category_name

            designMixData[0].designMixVariantDetails[i].admixture_type =
              adMixtureCatDoc.admixture_type
          }
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].cement_grade_id
          )
        ) {
          const cementGradeDoc = await CementGradeModel.findById({
            _id: designMixData[0].designMixVariantDetails[i].cement_grade_id,
          }).lean()
          if (!isNullOrUndefined(cementGradeDoc)) {
            designMixData[0].designMixVariantDetails[i].cement_grade_name =
              cementGradeDoc.name
          }
        }

        const vendorUserDoc = await VendorUserModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].vendor_id,
        }).lean()
        if (!isNullOrUndefined(vendorUserDoc)) {
          designMixData[0].designMixVariantDetails[i].vendor_user_name =
            vendorUserDoc.full_name
        }
      }

      return Promise.resolve(designMixData[0])
    }
    return Promise.reject(new InvalidInput(`No RMC Supplier found near the selected location.`, 400))
  }

  async editDesignMixDetails(
    user_id: string,
    designMixtureId: string,
    designMixData: IEditDesignMix | any
  ) {
    const result = await AuthModel.findOne({
      code: designMixData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }

    try {
      const designMixCheck = await DesignMixtureModel.findOne({
        _id: designMixtureId,
        //  vendor_id: new ObjectId(user_id),
      })
      if (designMixCheck === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      let editDoc: { [k: string]: any } = {}
      const arrKeys = ["grade_id"]

      if (designMixData.variant.length > 0) {
        for (let i = 0; i < designMixData.variant.length; i++) {
          if (!isNullOrUndefined(designMixData.variant[i]._id)) {
            console.log("hereeeeeeee")
            let editvariantDoc: { [k: string]: any } = {}
            editvariantDoc.grade_id = designMixData.variant[i].grade_id
            editvariantDoc.product_name = designMixData.variant[i].product_name
            editvariantDoc.cement_brand_id =
              designMixData.variant[i].cement_brand_id
            editvariantDoc.cement_quantity =
              designMixData.variant[i].cement_quantity
            editvariantDoc.sand_source_id =
              designMixData.variant[i].sand_source_id
            editvariantDoc.sand_quantity =
              designMixData.variant[i].sand_quantity
            editvariantDoc.aggregate_source_id =
              designMixData.variant[i].aggregate_source_id
            editvariantDoc.aggregate1_sub_category_id =
              designMixData.variant[i].aggregate1_sub_category_id
            editvariantDoc.aggregate1_quantity =
              designMixData.variant[i].aggregate1_quantity
            editvariantDoc.aggregate2_sub_category_id =
              designMixData.variant[i].aggregate2_sub_category_id
            editvariantDoc.aggregate2_quantity =
              designMixData.variant[i].aggregate2_quantity
            editvariantDoc.fly_ash_source_id =
              designMixData.variant[i].fly_ash_source_id
            editvariantDoc.fly_ash_quantity =
              designMixData.variant[i].fly_ash_quantity
            editvariantDoc.ad_mixture_brand_id =
              designMixData.variant[i].ad_mixture_brand_id
            editvariantDoc.ad_mixture_quantity =
              designMixData.variant[i].ad_mixture_quantity
            editvariantDoc.water_quantity =
              designMixData.variant[i].water_quantity
            editvariantDoc.selling_price =
              designMixData.variant[i].selling_price
            editvariantDoc.is_availble = designMixData.variant[i].is_availble
            editvariantDoc.description = designMixData.variant[i].description
            editvariantDoc.is_custom = designMixData.variant[i].is_custom
            editvariantDoc.address_id = designMixData.variant[i].address_id
            editvariantDoc.cement_grade_id =
              designMixData.variant[i].cement_grade_id
            editvariantDoc.ad_mixture_category_id =
              designMixData.variant[i].ad_mixture_category_id

            await DesignMixVariantModel.findOneAndUpdate(
              { _id: new ObjectId(designMixData.variant[i]._id) },
              {
                $set: editvariantDoc,
              }
            )
          } else {
            let variantDoc: { [k: string]: any } = {}
            variantDoc.design_mix_id = designMixtureId
            variantDoc.grade_id = designMixData.variant[i].grade_id
            variantDoc.product_name = designMixData.variant[i].product_name
            variantDoc.cement_brand_id =
              designMixData.variant[i].cement_brand_id
            variantDoc.cement_quantity =
              designMixData.variant[i].cement_quantity
            variantDoc.sand_source_id = designMixData.variant[i].sand_source_id
            variantDoc.sand_quantity = designMixData.variant[i].sand_quantity
            variantDoc.aggregate_source_id =
              designMixData.variant[i].aggregate_source_id
            variantDoc.aggregate1_sub_category_id =
              designMixData.variant[i].aggregate1_sub_category_id
            variantDoc.aggregate1_quantity =
              designMixData.variant[i].aggregate1_quantity
            variantDoc.aggregate2_sub_category_id =
              designMixData.variant[i].aggregate2_sub_category_id
            variantDoc.aggregate2_quantity =
              designMixData.variant[i].aggregate2_quantity
            variantDoc.fly_ash_source_id =
              designMixData.variant[i].fly_ash_source_id
            variantDoc.fly_ash_quantity =
              designMixData.variant[i].fly_ash_quantity
            variantDoc.ad_mixture_brand_id =
              designMixData.variant[i].ad_mixture_brand_id
            variantDoc.ad_mixture_quantity =
              designMixData.variant[i].ad_mixture_quantity
            variantDoc.water_quantity = designMixData.variant[i].water_quantity
            variantDoc.selling_price = designMixData.variant[i].selling_price
            variantDoc.is_availble = designMixData.variant[i].is_availble
            variantDoc.description = designMixData.variant[i].description
            variantDoc.is_custom = designMixData.variant[i].is_custom
            variantDoc.address_id = designMixData.variant[i].address_id
            variantDoc.cement_grade_id =
              designMixData.variant[i].cement_grade_id
            variantDoc.ad_mixture_category_id =
              designMixData.variant[i].ad_mixture_category_id
            await new DesignMixVariantModel(variantDoc).save()
          }
        }
      }

      for (const [key, value] of Object.entries(designMixData)) {
        if (arrKeys.indexOf(key) > -1) {
          editDoc[key] = value
        }
      }

      if (Object.keys(editDoc).length < 1) {
        return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
      }

      editDoc.updated_at = Date.now()
      editDoc.updated_by_id = user_id
      const designMixDoc = await DesignMixtureModel.findOneAndUpdate(
        { _id: designMixtureId },
        { $set: editDoc },
        {
          new: true,
        }
      )
      if (designMixDoc === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "vendor",
        AccessTypes.EDIT_DESIGN_MIX_BY_ADMIN,
        `Admin updated details design mix of id ${designMixtureId} .`,
        designMixDoc,
        editDoc
      )
      await AuthModel.deleteOne({
        user_id,
        code: designMixData.authentication_code,
      })

      return Promise.resolve(designMixDoc)
   
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }

  async editDesignMixVariantDetails(
    user_id: string,
    design_mix_variant_id: string,
    designMixData: IEditDesignMixVariant
  ) {
    try {
      console.log("designMixData", designMixData)

      const result = await AuthModel.findOne({
        code: designMixData.authentication_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
      }

      const variantCheck = await DesignMixVariantModel.findOne({
        _id: design_mix_variant_id,
      })
      if (variantCheck === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      const designMixdoc = await DesignMixtureModel.findById({
        _id: variantCheck.design_mix_id,
        // vendor_id: new ObjectId(user_id),
      })

      if (designMixdoc === null) {
        return Promise.reject(new InvalidInput(`No RMC Supplier found near the selected location.`, 400))
      }

      let editDoc: { [k: string]: any } = {}

      const arrKeys = [
        "product_name",
        "cement_brand_id",
        "cement_quantity",
        "sand_source_id",
        "sand_quantity",
        "aggregate_source_id",
        "aggregate1_sub_category_id",
        "aggregate1_quantity",
        "aggregate2_sub_category_id",
        "aggregate2_quantity",
        "fly_ash_source_id",
        "fly_ash_quantity",
        "ad_mixture_brand_id",
        "ad_mixture_quantity",
        "water_quantity",
        "selling_price",
        "is_availble",
        "description",
        "is_custom",
        "address_id",
        "cement_grade_id",
        "ad_mixture_category_id",
      ]

      for (const [key, value] of Object.entries(designMixData)) {
        if (arrKeys.indexOf(key) > -1) {
          editDoc[key] = value
        }
      }

      if (Object.keys(editDoc).length < 1) {
        return Promise.reject(new InvalidInput(`Kindly enter a field for update`, 400))
      }

      editDoc.updated_at = Date.now()
      editDoc.updated_by_id = user_id

      const designMixDoc = await DesignMixVariantModel.findOneAndUpdate(
        { _id: new ObjectId(design_mix_variant_id) },
        { $set: editDoc },
        {
          new: true,
        }
      )
      if (designMixDoc === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "vendor",
        AccessTypes.EDIT_DESIGN_MIX_VARIANT_BY_ADMIN,
        `Vendor updated details design mix variant of id ${design_mix_variant_id} .`,
        designMixDoc,
        editDoc
      )

      await AuthModel.deleteOne({
        user_id,
        code: designMixData.authentication_code,
      })

      return Promise.resolve(designMixDoc)
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }
  async getDesignMixtureVariantDetails(
    design_mix_variant_id: string,
    user_id: string
  ) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(design_mix_variant_id),
      //vendor_id: new ObjectId(user_id),
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          design_mix_id: true,
          grade_id: true,
          vendor_id: true,
          cement_brand_id: true,
          sand_source_id: true,
          aggregate_source_id: true,
          fly_ash_source_id: true,
          aggregate1_sub_category_id: true,
          aggregate2_sub_category_id: true,
          ad_mixture_brand_id: true,
          product_name: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          ad_mixture_quantity: true,
          water_quantity: true,
          selling_price: true,
          is_availble: true,
          description: true,
          is_custom: true,
          address_id: true,
          ad_mixture_category_id: true,
          cement_grade_id: true,
          updated_by_id: true,
          updated_at: true,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
        },
      },
    ]

    const designMixVariantData = await DesignMixVariantModel.aggregate(
      aggregateArr
    )
    if (designMixVariantData.length > 0) {
      const concreteGradeDoc = await ConcreteGradeModel.findById({
        _id: designMixVariantData[0].grade_id,
      }).lean()
      if (!isNullOrUndefined(concreteGradeDoc)) {
        designMixVariantData[0].concrete_grade_name = concreteGradeDoc.name
      }

      const cementBrandDoc = await CementBrandModel.findById({
        _id: designMixVariantData[0].cement_brand_id,
      }).lean()
      if (!isNullOrUndefined(cementBrandDoc)) {
        designMixVariantData[0].cement_brand_name = cementBrandDoc.name
      }

      const sandSourceDoc = await SandSourceModel.findById({
        _id: designMixVariantData[0].sand_source_id,
      }).lean()
      if (!isNullOrUndefined(sandSourceDoc)) {
        designMixVariantData[0].sand_source_name =
          sandSourceDoc.sand_source_name
      }

      const aggregateSourceDoc = await AggregateSourceModel.findById({
        _id: designMixVariantData[0].aggregate_source_id,
      }).lean()
      if (!isNullOrUndefined(aggregateSourceDoc)) {
        designMixVariantData[0].aggregate_source_name =
          aggregateSourceDoc.aggregate_source_name
      }

      const flyAshSourceDoc = await FlyAshSourceModel.findById({
        _id: designMixVariantData[0].fly_ash_source_id,
      }).lean()
      if (!isNullOrUndefined(flyAshSourceDoc)) {
        designMixVariantData[0].fly_ash_source_name =
          flyAshSourceDoc.fly_ash_source_name
      }

      const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate1_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate1SubCatDoc)) {
        designMixVariantData[0].aggregate1_sub_category_name =
          aggregate1SubCatDoc.sub_category_name
      }

      const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate2_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate2SubCatDoc)) {
        designMixVariantData[0].aggregate2_sub_category_name =
          aggregate2SubCatDoc.sub_category_name
      }

      const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
        _id: designMixVariantData[0].ad_mixture_brand_id,
      }).lean()
      if (!isNullOrUndefined(adMixtureBrandDoc)) {
        designMixVariantData[0].admix_brand_name = adMixtureBrandDoc.name
      }

      const addressDoc = await AddressModel.findById({
        _id: designMixVariantData[0].address_id,
      }).lean()
      if (!isNullOrUndefined(addressDoc)) {
        designMixVariantData[0].address_line1 = addressDoc.line1
        designMixVariantData[0].address_line2 = addressDoc.line2
        designMixVariantData[0].pincode = addressDoc.pincode
        designMixVariantData[0].location = addressDoc.location

        const cityDoc = await CityModel.findById({
          _id: addressDoc.city_id,
        }).lean()

        if (!isNullOrUndefined(cityDoc)) {
          designMixVariantData[0].city_name = cityDoc.city_name
        }

        const stateDoc = await StateModel.findById({
          _id: addressDoc.state_id,
        }).lean()

        if (!isNullOrUndefined(stateDoc)) {
          designMixVariantData[0].state_name = stateDoc.state_name
        }
      }

      const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
        _id: designMixVariantData[0].ad_mixture_category_id,
      }).lean()
      if (!isNullOrUndefined(adMixtureCatDoc)) {
        designMixVariantData[0].admix_category_name =
          adMixtureCatDoc.category_name
      }

      const cementGradeDoc = await CementGradeModel.findById({
        _id: designMixVariantData[0].cement_grade_id,
      }).lean()
      if (!isNullOrUndefined(cementGradeDoc)) {
        designMixVariantData[0].cement_grade_name = cementGradeDoc.name
      }

      return Promise.resolve(designMixVariantData[0])
    }
    return Promise.reject(new InvalidInput(`No RMC Supplier found near the selected location.`, 400))
  }
}
