import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
export interface ISite {
  user_id?: string
  site_name: string
  company_name: string
  address_line1: string
  address_line2: string
  country_id: number
  state_id: string
  city_id: string
  sub_city_id: string
  pincode?: number
  person_name: string
  title?: string
  email: string
  mobile_number: string
  alt_mobile_number?: string
  whatsapp_number?: string
  landline_number?: string
  profile_pic?: string
  billing_address_id: string
  location: {
    type?: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
}
export interface IAddSiteRequest extends IAuthenticatedRequest {
  body: {
    site: ISite
  }
}

export interface IeditSite {
  site_name?: string
  company_name?: string
  address_line1?: string
  address_line2?: string
  country_id?: number
  state_id?: string
  city_id?: string
  sub_city_id?: string
  pincode?: number
  person_name?: string
  title?: string
  email?: string
  mobile_number?: string
  alt_mobile_number?: string
  whatsapp_number?: string
  landline_number?: string
  profile_pic?: string
  billing_address_id: string
  location: {
    type?: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
}

export interface IEditSiteRequest extends IAuthenticatedRequest {
  body: {
    site: IeditSite
  }
  params: {
    siteId: string
  }
}

export interface IGetSiteRequest extends IAuthenticatedRequest {
  params: {
    siteId: string
  }
}

export interface IVerifyMobilenumberRequest extends IAuthenticatedRequest {
  params: {
    mobileNumber: string
    code: string
  }
}

export interface IResendEmailRequest extends IAuthenticatedRequest {
  body: {
    email: string
  }
}

export interface IResendOTPRequest extends IAuthenticatedRequest {
  body: {
    mobileNumber: string
    mobileNumber_type: string
  }
}

export interface IDeleteSiteRequest extends IAuthenticatedRequest {
  params: {
    siteId: string
  }
}

export interface ICheckSiteRequest extends IAuthenticatedRequest {
  body: {
    site_name: string
  }
}
