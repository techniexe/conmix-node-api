import { injectable, inject } from "inversify"
import { ISite, IeditSite } from "./buyer.site.request-definations"
import { ISiteDoc, SiteModel } from "../../model/site.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { BuyerTypes } from "../buyer.types"
import { CommonService } from "../../utilities/common.service"
import { emailVerificationType, AuthModel } from "../../model/auth.model"
import { emailVerificationLinkURI } from "../../utilities/config"
import { MailService } from "../../utilities/mail-service"
import { logger } from "../../logger"
//import { BuyerAuthRepository } from "../auth/buyer.auth.repository"
//import { TwilioService } from "../../utilities/twilio-service"
import { AccessTypes } from "../../model/access_logs.model"
import { ObjectId } from "bson"
import { TestService } from "../../utilities/test.service"

@injectable()
export class SiteRepository {
  constructor(
    // @inject(BuyerTypes.BuyerAuthRepository)
    // private authRepo: BuyerAuthRepository,
    //@inject(BuyerTypes.TwilioService) private twilioService: TwilioService,
    @inject(BuyerTypes.CommonService) private commonService: CommonService,
    @inject(BuyerTypes.MailService) private mailService: MailService,
    @inject(BuyerTypes.TestService) private testService: TestService,
    ) {}
  async addSite(user_id: string, site: ISite): Promise<ISiteDoc> {
    site.user_id = user_id
    const newSite = new SiteModel(site)
    try {
      const newSiteDoc = await newSite.save()
      return Promise.resolve(newSiteDoc)
    } catch (e) {
      const msg =
        e.code === 11000 ? "Site name already exists." : "Sorry, we could not add this site"
      return Promise.reject(new InvalidInput(msg, 400))
    }
  }

  async checkSite(user_id: string, site_name: string) {
    const siteData = await SiteModel.findOne({
      user_id: new ObjectId(user_id),
      site_name,
    })
    if (!isNullOrUndefined(siteData)) {
      return Promise.reject(new InvalidInput(`Site name already exists.`, 400))
    } else {
      return Promise.resolve()
    }
  }
  async getSites(
    user_id: string,
    search?: string,
    before?: string,
    after?: string
  ): Promise<ISiteDoc[]> {
    let query: { [k: string]: any } = {
      user_id: new ObjectId(user_id),
      is_deleted: false,
    }

    console.log(user_id)
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "billingAddressDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "billingAddressDetails.state_id",
          foreignField: "_id",
          as: "billingAddressStateDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressStateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "billingAddressDetails.city_id",
          foreignField: "_id",
          as: "billingAddressCityDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressCityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          user_id: 1,
          site_name: 1,
          company_name: 1,
          address_line1: 1,
          address_line2: 1,
          whatsapp_number_verified: 1,
          _id: 1,
          country_id: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          person_name: 1,
          title: 1,
          email: 1,
          mobile_number: 1,
          landline_number: 1,
          profile_pic: 1,
          alt_mobile_number: 1,
          created_at: 1,
          location: 1,
          billing_address_id: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          "billingAddressDetails._id": 1,
          "billingAddressDetails.company_name": 1,
          "billingAddressDetails.line1": 1,
          "billingAddressDetails.line2": 1,
          "billingAddressDetails.pincode": 1,
          "billingAddressDetails.gst_number": 1,
          "billingAddressDetails.gst_image_url": 1,
          "billingAddressDetails.created_at": 1,
          "billingAddressStateDetails._id": "$stateDetails._id",
          "billingAddressStateDetails.state_name": "$stateDetails.state_name",
          "billingAddressStateDetails.country_id": "$stateDetails.country_id",
          "billingAddressStateDetails.country_code": "$stateDetails.country_code",
          "billingAddressStateDetails.country_name": "$stateDetails.country_name",
          "billingAddressStateDetails.created_at": "$stateDetails.created_at",
          "billingAddressCityDetails._id": "$cityDetails._id",
          "billingAddressCityDetails.city_name": "$cityDetails.city_name",
          "billingAddressCityDetails.location": "$cityDetails.location",
          "billingAddressCityDetails.state_id": "$cityDetails.state_id",
          "billingAddressCityDetails.created_at": "$cityDetails.created_at",
          "billingAddressCityDetails.country_code": "$cityDetails.country_code",
          "billingAddressCityDetails.country_id": "$cityDetails.country_id",
          "billingAddressCityDetails.country_name": "$cityDetails.country_name",
          "billingAddressCityDetails.state_name": "$cityDetails.state_name",
          "billingAddressCityDetails.popularity": "$cityDetails.popularity",
        },
      },
    ]

    const siteData = await SiteModel.aggregate(aggregateArr)
    return Promise.resolve(siteData)
  }

  async editSite(
    user_id: string,
    siteId: string,
    siteData: IeditSite
  ): Promise<any> {
    const flds = [
      "site_name",
      "company_name",
      "address_line1",
      "address_line2",
      "country_id",
      "state_id",
      "city_id",
      "pincode",
      "person_name",
      "title",
      "landline_number",
      "profile_pic",
      "location",
      "billing_address_id"
    ]
    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    for (const [key, val] of Object.entries(siteData)) {
      if (flds.indexOf(key) > -1) {
        updt.$set[key] = val
      }
    }
    if (Object.keys(updt.$set).length < 1) {
      return Promise.reject(new UnexpectedInput(`Kindly enter a field for update`, 400))
    }

    const siteCheck = await SiteModel.findOne({ _id: siteId })
    if (siteCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    if (
      !isNullOrUndefined(siteData.email) &&
      siteCheck.email !== siteData.email
    ) {
      updt.$set.email = siteData.email
      updt.$set.email_verified = false
    }

    if (
      !isNullOrUndefined(siteData.mobile_number) &&
      siteCheck.mobile_number !== siteData.mobile_number
    ) {
      updt.$set.mobile_number = siteData.mobile_number
      updt.$set.mobile_number_verified = false
    }

    if (
      !isNullOrUndefined(siteData.alt_mobile_number) &&
      siteCheck.alt_mobile_number !== siteData.alt_mobile_number
    ) {
      updt.$set.alt_mobile_number = siteData.alt_mobile_number
      updt.$set.alt_mobile_number_verified = false
    }

    if (
      !isNullOrUndefined(siteData.whatsapp_number) &&
      siteCheck.whatsapp_number !== siteData.whatsapp_number
    ) {
      updt.$set.whatsapp_number = siteData.whatsapp_number
      updt.$set.whatsapp_number_verified = false
    }
    const res = await SiteModel.updateOne(
      {
        _id: siteId,
        user_id,
      },
      updt
    )
    if (res.n !== 1) {
      return Promise.reject(
        new UnexpectedInput(`This site is no longer active `, 400)
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "buyer",
      AccessTypes.EDIT_SITE,
      "Site edited."
    )

    if (!isNullOrUndefined(updt.$set.email)) {
      const emailToken = await this.commonService.getEmailAuthToken(
        updt.$set.email,
        "buyer",
        user_id,
        emailVerificationType.SITE
      )
      const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
      try {
        await this.mailService.sendMailUsingTemplate(
          "email_verification",
          {
            subject: { name: siteCheck.person_name },
            html: { name: siteCheck.person_name, email: updt.$set.email, link },
          },
          { full_name: siteCheck.person_name, email: updt.$set.email }
        )
      } catch (e) {
        logger.fatal("Unable to send message:", e)
        return Promise.reject(
          new Error(`Unable to send email. Please try again later.`)
        )
      }
    }

    if (!isNullOrUndefined(updt.$set.mobile_number)) {
      // const authCode = await this.authRepo.getBuyerAuthCode(
      //   updt.$set.mobile_number,
      //   user_id
      // )
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   updt.$set.mobile_number,
      //   authCode.code,
      //   "sms"
      // )
    }

    if (!isNullOrUndefined(updt.$set.alt_mobile_number)) {
      // const authCode = await this.authRepo.getBuyerAuthCode(
      //   updt.$set.alt_mobile_number,
      //   user_id
      // )
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   updt.$set.alt_mobile_number,
      //   authCode.code,
      //   "sms"
      // )
    }

    if (!isNullOrUndefined(updt.$set.whatsapp_number)) {
      // const authCode = await this.authRepo.getBuyerAuthCode(
      //   updt.$set.whatsapp_number,
      //   user_id
      // )
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   updt.$set.whatsapp_number,
      //   authCode.code,
      //   "sms"
      // )
    }

    return Promise.resolve()
  }

  async deleteSite(user_id: string, siteId: string) {
    const updtRes = await SiteModel.updateOne(
      {
        _id: siteId,
        user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Delete failed as no record exists`, 400)
      )
    }
    return Promise.resolve()
  }
  async getSiteById(user_id: string, siteId: string): Promise<ISiteDoc | null> {
    let query: { [k: string]: any } = {
      user_id: new ObjectId(user_id),
      _id: new ObjectId(siteId),
      is_deleted: false,
    }

    const aggregateArr = [
      { $match: query },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 40,
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "billingAddressDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "billingAddressDetails.state_id",
          foreignField: "_id",
          as: "billingAddressStateDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressStateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "billingAddressDetails.city_id",
          foreignField: "_id",
          as: "billingAddressCityDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressCityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          user_id: 1,
          site_name: 1,
          company_name: 1,
          address_line1: 1,
          address_line2: 1,
          whatsapp_number_verified: 1,
          _id: 1,
          country_id: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          person_name: 1,
          title: 1,
          email: 1,
          mobile_number: 1,
          landline_number: 1,
          profile_pic: 1,
          alt_mobile_number: 1,
          created_at: 1,
          location: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          "billingAddressDetails._id": 1,
          "billingAddressDetails.company_name": 1,
          "billingAddressDetails.line1": 1,
          "billingAddressDetails.line2": 1,
          "billingAddressDetails.pincode": 1,
          "billingAddressDetails.gst_number": 1,
          "billingAddressDetails.gst_image_url": 1,
          "billingAddressDetails.created_at": 1,
          "billingAddressStateDetails._id": "$stateDetails._id",
          "billingAddressStateDetails.state_name": "$stateDetails.state_name",
          "billingAddressStateDetails.country_id": "$stateDetails.country_id",
          "billingAddressStateDetails.country_code": "$stateDetails.country_code",
          "billingAddressStateDetails.country_name": "$stateDetails.country_name",
          "billingAddressStateDetails.created_at": "$stateDetails.created_at",
          "billingAddressCityDetails._id": "$cityDetails._id",
          "billingAddressCityDetails.city_name": "$cityDetails.city_name",
          "billingAddressCityDetails.location": "$cityDetails.location",
          "billingAddressCityDetails.state_id": "$cityDetails.state_id",
          "billingAddressCityDetails.created_at": "$cityDetails.created_at",
          "billingAddressCityDetails.country_code": "$cityDetails.country_code",
          "billingAddressCityDetails.country_id": "$cityDetails.country_id",
          "billingAddressCityDetails.country_name": "$cityDetails.country_name",
          "billingAddressCityDetails.state_name": "$cityDetails.state_name",
          "billingAddressCityDetails.popularity": "$cityDetails.popularity",
        },
      },
    ]

    const siteData = await SiteModel.aggregate(aggregateArr)

    if (siteData.length <= 0) {
      return Promise.reject(new InvalidInput(`No site details could be found `, 400))
    }
    return Promise.resolve(siteData[0])
  }

  async verifyUserWithMobilenumber(
    user_id: string,
    mobileNumber: string,
    code: string
  ) {
    const userCheck = await AuthModel.findOne({
      user_id,
      user_type: "buyer",
      identifier: mobileNumber,
      code,
    })
    if (userCheck === null) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again `, 400))
    }

    return Promise.all([
      SiteModel.findOneAndUpdate(
        {
          user_id,
          mobile_number: mobileNumber,
        },
        { $set: { mobile_number_verified: true } }
      ),
      SiteModel.findOneAndUpdate(
        {
          user_id,
          alt_mobile_number: mobileNumber,
        },
        { $set: { alt_mobile_number_verified: true } }
      ),
      SiteModel.findOneAndUpdate(
        {
          user_id,
          whatsapp_number: mobileNumber,
        },
        { $set: { whatsapp_number_verified: true } }
      ),
      AuthModel.deleteOne({
        user_id,
        user_type: "buyer",
        identifier: mobileNumber,
      }),
    ])
  }

  async resendEmail(user_id: string, email: string) {
    const siteCheck = await SiteModel.findOne({
      user_id,
      email,
    })

    if (siteCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This Email is not registered.`, 400)
      )
    }

    if (siteCheck.email_verified === true) {
      return Promise.resolve("Email already verified.")
    }
    const emailToken = await this.commonService.getEmailAuthToken(
      email,
      "buyer",
      user_id,
      emailVerificationType.SITE
    )
    const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
    try {
      await this.mailService.sendMailUsingTemplate(
        "email_verification",
        {
          subject: { name: siteCheck.person_name },
          html: { name: siteCheck.person_name, email, link },
        },
        { full_name: siteCheck.person_name, email }
      )
      return Promise.resolve("Email sent.")
    } catch (e) {
      logger.fatal("Unable to send message:", e)
      return Promise.reject(
        new Error(`Unable to send email. Please try again later.`)
      )
    }
  }

  async resendOTP(
    user_id: string,
    mobileNumber: string,
    mobileNumber_type: string
  ) {

    const authData = await AuthModel.findOne(
      { identifier: mobileNumber, user_type: "buyer" }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let query: { [k: string]: any } = {
      user_id,
    }
    if (mobileNumber_type === "mobile_number") {
      query.mobile_number = mobileNumber
    } else if (mobileNumber_type === "alt_mobile_number") {
      query.alt_mobile_number = mobileNumber
    } else {
      query.whatsapp_number = mobileNumber
    }

    const siteCheck = await SiteModel.findOne(query)

    if (siteCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This mobileNumber is not registered.`, 400)
      )
    }

    if (
      (mobileNumber_type === "mobile_number" &&
        siteCheck.mobile_number_verified === true) ||
      (mobileNumber_type === "alt_mobile_number" &&
        siteCheck.alt_mobile_number_verified === true) ||
      (mobileNumber_type === "whatsapp_number" &&
        siteCheck.whatsapp_number_verified === true)
    ) {
      return Promise.resolve("mobileNumber already verified.")
    }

    let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.	
    let templateId = "1707163532596026621"
    await this.testService.sendSMS(mobileNumber, message, templateId)
    return Promise.resolve("OTP sent.")
  }
}
