import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
  httpDelete,
} from "inversify-express-utils"
import { BuyerTypes } from "../buyer.types"
import { SiteRepository } from "./buyer.site.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addsiteSchema,
  editSiteSchema,
  getSiteSchema,
  verifyMobilenumberSchema,
  resendEmailSchema,
  resendOTPSchema,
  deleteSiteSchema,
  checksiteSchema,
} from "./buyer.site.request-schema"
import {
  IAddSiteRequest,
  IEditSiteRequest,
  IGetSiteRequest,
  IVerifyMobilenumberRequest,
  IResendEmailRequest,
  IResendOTPRequest,
  IDeleteSiteRequest,
  ICheckSiteRequest,
} from "./buyer.site.request-definations"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { UnexpectedInput } from "../../utilities/customError"

@controller("/site", verifyCustomToken("buyer"))
@injectable()
export class SiteController {
  constructor(
    @inject(BuyerTypes.SiteRepository) private siteRepo: SiteRepository
  ) {}

  //Add site.
  @httpPost("/", validate(addsiteSchema))
  async addSite(req: IAddSiteRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    const { site } = req.body
    try {
      console.log("uid", uid)
      const { _id } = await this.siteRepo.addSite(uid, site)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added this site details. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  //Check site.
  @httpPost("/check", validate(checksiteSchema))
  async checkSite(req: ICheckSiteRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    const { site_name } = req.body
    try {
      console.log("uid", uid)
      await this.siteRepo.checkSite(uid, site_name)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  //Get site data.
  @httpGet("/")
  async getSite(req: IAuthenticatedRequest, res: Response, next: NextFunction) {
    try {
      const { uid } = req.user
      const { search, before, after } = req.query
      const data = await this.siteRepo.getSites(uid, search, before, after)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  //Edit site data.
  @httpPatch("/:siteId", validate(editSiteSchema))
  async editSite(req: IEditSiteRequest, res: Response, next: NextFunction) {
    try {
      await this.siteRepo.editSite(
        req.user.uid,
        req.params.siteId,
        req.body.site
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added this site details. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  //Delete site data.
  @httpDelete("/:siteId", validate(deleteSiteSchema))
  async deleteSite(req: IDeleteSiteRequest, res: Response, next: NextFunction) {
    try {
      await this.siteRepo.deleteSite(req.user.uid, req.params.siteId)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:siteId", validate(getSiteSchema))
  async getSiteById(req: IGetSiteRequest, res: Response, next: NextFunction) {
    try {
      const data = await this.siteRepo.getSiteById(
        req.user.uid,
        req.params.siteId
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPost(
    "/mobile/:mobileNumber/code/:code",
    validate(verifyMobilenumberSchema)
  )
  async verifyMobilenumber(
    req: IVerifyMobilenumberRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.siteRepo.verifyUserWithMobilenumber(
        req.user.uid,
        req.params.mobileNumber,
        req.params.code
      )
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendEmail", validate(resendEmailSchema))
  async resendEmail(
    req: IResendEmailRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const message = await this.siteRepo.resendEmail(
        req.user.uid,
        req.body.email
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendOTP", validate(resendOTPSchema))
  async resendOTP(req: IResendOTPRequest, res: Response, next: NextFunction) {
    try {
      const message = await this.siteRepo.resendOTP(
        req.user.uid,
        req.body.mobileNumber,
        req.body.mobileNumber_type
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }
}
