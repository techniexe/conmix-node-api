import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export const addsiteSchema: IRequestSchema = {
  body: Joi.object().keys({
    site: Joi.object()
      .keys({
        site_name: Joi.string().required(),
        company_name: Joi.string().required(),
        address_line1: Joi.string().required(),
        address_line2: Joi.string(),
        country_id: Joi.number().required(),
        state_id: Joi.string().required(),
        city_id: Joi.string().required(),
        pincode: Joi.number().min(6),
        person_name: Joi.string().required(),
        title: Joi.string(),
        email: Joi.string().required(),
        mobile_number: Joi.string().required(),
        alt_mobile_number: Joi.string(),
        whatsapp_number: Joi.string(),
        landline_number: Joi.string(),
        profile_pic: Joi.string(),
        billing_address_id: Joi.string().regex(objectIdRegex),
        location: Joi.object()
          .keys({
            type: Joi.string().default("Point"),
            coordinates: Joi.array()
              .ordered([
                Joi.number().min(-180).max(180).required(),
                Joi.number().min(-90).max(90).required(),
              ])
              .length(2)
              .required(),
          })
          .required(),
      })
      .required(),
  }),
}

export const editSiteSchema: IRequestSchema = {
  body: Joi.object().keys({
    site: Joi.object().keys({
      site_name: Joi.string(),
      company_name: Joi.string(),
      address_line1: Joi.string(),
      address_line2: Joi.string(),
      country_id: Joi.number(),
      state_id: Joi.string(),
      city_id: Joi.string(),
      pincode: Joi.number().min(6),
      person_name: Joi.string(),
      title: Joi.string(),
      email: Joi.string(),
      mobile_number: Joi.string(),
      alt_mobile_number: Joi.string(),
      whatsapp_number: Joi.string(),
      landline_number: Joi.string(),
      profile_pic: Joi.string(),
      billing_address_id: Joi.string().regex(objectIdRegex),
      location: Joi.object().keys({
        type: Joi.string().default("Point"),
        coordinates: Joi.array()
          .ordered([
            Joi.number().min(-180).max(180).required(),
            Joi.number().min(-90).max(90).required(),
          ])
          .length(2)
          .required(),
      }),
    }),
  }),
  params: Joi.object().keys({
    siteId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const getSiteSchema: IRequestSchema = {
  params: Joi.object().keys({
    siteId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const deleteSiteSchema: IRequestSchema = {
  params: Joi.object().keys({
    siteId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const verifyMobilenumberSchema: IRequestSchema = {
  params: Joi.object().keys({
    mobileNumber: Joi.string().required(),
    code: Joi.string().required(),
  }),
}

export const resendEmailSchema: IRequestSchema = {
  body: Joi.object().keys({
    email: Joi.string().required(),
  }),
}

export const resendOTPSchema: IRequestSchema = {
  body: Joi.object().keys({
    mobileNumber: Joi.string().required(),
    mobileNumber_type: Joi.string()
      .required()
      .valid(["mobile_number", "alt_mobile_number", "whatsapp_number"]),
  }),
}

export const checksiteSchema: IRequestSchema = {
  body: Joi.object().keys({
    site_name: Joi.string().required(),
  }),
}
