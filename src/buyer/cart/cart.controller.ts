import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { CartRepository } from "./cart.repository"
import {
  IAddToCartRequest,
  addToCartSchema,
  IRemoveCartItemRequest,
  removeCartItemSchema,
  getCartItemSchema,
  IGetCartItemRequest,
  addSiteToCart,
  IAddSiteToCart,
  getCouponSchema,
  IGetCouponListRequest,
  applyCouponSchema,
  IApplyCoupon,
  addBillingAddressToCart,
  IAddBillingAddressToCart,
} from "./cart.req.schema"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import { isNullOrUndefined } from "util"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { BuyerTypes } from "../buyer.types"

@controller("/cart", verifyCustomToken("buyer"))
@injectable()
export class CartController {
  constructor(
    @inject(BuyerTypes.CartRepository) private cartRepo: CartRepository
  ) {}
  @httpPost("/", validate(addToCartSchema))
  async addToCart(req: IAddToCartRequest, res: Response, next: NextFunction) {
    const {
      design_mix_id,
      custom_mix,
      quantity,
      state_id,
      cart_id,
      item_id,
      site_id,
      long,
      lat,
      with_CP,
      with_TM,
      delivery_date,
      end_date,
      TM_no,
      driver_name,
      driver_mobile,
      TM_operator_name,
      TM_operator_mobile_no,
    } = req.body
    let uid
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }
    const data = await this.cartRepo.addToCart(
      design_mix_id,
      custom_mix,
      quantity,
      state_id,
      cart_id,
      item_id,
      uid,
      site_id,
      long,
      lat,
      with_CP,
      with_TM,
      delivery_date,
      end_date,
      TM_no,
      driver_name,
      driver_mobile,
      TM_operator_name,
      TM_operator_mobile_no
    )
    res.json({ data })
  }
  @httpDelete("/item", validate(removeCartItemSchema))
  async removeItemsFromCart(
    req: IRemoveCartItemRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cartRepo.removeItemsFromCart(
        req.body.cart_id,
        req.body.item_id
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/address", validate(addSiteToCart))
  async addSiteToCart(req: IAddSiteToCart, res: Response) {
    if (isNullOrUndefined(req.user)) {
      return res
        .status(400)
        .json({ message: "Kindly login to add site details in cart" })
    }
    await this.cartRepo.addSiteToCart(req.user.uid, req.body.site_id)
    res.sendStatus(201)
  }

  @httpPost("/billing_address", validate(addBillingAddressToCart))
  async addBillingAddressToCart(req: IAddBillingAddressToCart, res: Response) {
    if (isNullOrUndefined(req.user)) {
      return res
        .status(400)
        .json({ message: "Kindly login to add billing address details in cart" })
    }
    await this.cartRepo.addBillingAddressToCart(req.user.uid, req.body.billing_address_id)
    res.sendStatus(201)
  }

  /**
   * Get coupon list.
   */
  @httpGet("/coupon", validate(getCouponSchema))
  async getCouponList(
    req: IGetCouponListRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.cartRepo.getCouponList(req.query)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/", validate(getCartItemSchema))
  @httpGet("/:cartId", validate(getCartItemSchema))
  async getCartItem(
    req: IGetCartItemRequest,
    res: Response,
    next: NextFunction
  ) {
    let uid
    if (!isNullOrUndefined(req.user) && req.user.user_type === "buyer") {
      uid = req.user.uid
    }
    const { cartId } = req.params
    const { state_id, long, lat } = req.query
    const data = await this.cartRepo.getCartItem(
      // with_TM,
      // with_CP,
      // delivery_date,
      cartId,
      uid,
      state_id,
      long,
      lat
    )
    res.json({ data })
  }

  /**
   * Apply coupon to cart.
   */
  @httpPost("/coupon", validate(applyCouponSchema))
  @httpPost("/coupon/:cartId", validate(applyCouponSchema))
  async applyCouponCode(req: IApplyCoupon, res: Response) {
    let uid
    if (!isNullOrUndefined(req.user)) {
      uid = req.user.uid
    }
    await this.cartRepo.applyCoupon(
      req.body.coupon_code,
      uid,
      req.params.cartId
    )
    res.sendStatus(201)
  }
}
