import { ObjectId } from "bson"
import * as Joi from "joi"
import { Request } from "express"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import {
  IOptionalAuthenticatedReq,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { ICustomMix } from "../../model/cart.model"

export interface IAddToCart {
  design_mix_id: string
  custom_mix: ICustomMix
  quantity: number
  state_id: ObjectId
  cart_id: string
  item_id: string
  user_id: string
  site_id: string
  long: number
  lat: number
  with_CP: boolean
  with_TM: boolean
  delivery_date: Date
  end_date: Date
  TM_no: string
  driver_name: string
  driver_mobile: string
  TM_operator_name: string
  TM_operator_mobile_no: string
}

export interface IAddToCartRequest extends IOptionalAuthenticatedReq {
  body: IAddToCart
}
export interface IRemoveCartItemRequest extends Request {
  body: {
    item_id: string
    cart_id: string
  }
  user: {
    uid?: string
    [key: string]: any
  }
}

export interface ICartItems {
  _id: string
  items: [
    {
      _id: string
      sub_category_id: string
      sub_category_name: string
      thumbnail_url: string
      quantity_units: string
      quantity: number
    }
  ]
}
export const addToCartSchema: IRequestSchema = {
  body: Joi.object().keys({
    design_mix_id: Joi.string().regex(objectIdRegex),
    custom_mix: Joi.object(),
    quantity: Joi.number().required(),
    state_id: Joi.string().regex(objectIdRegex),
    cart_id: Joi.string().regex(objectIdRegex),
    item_id: Joi.string().regex(objectIdRegex),
    user_id: Joi.string().regex(objectIdRegex),
    site_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    with_CP: Joi.boolean().required(),
    with_TM: Joi.boolean().required(),
    delivery_date: Joi.date().required(),
    end_date: Joi.date(),
    TM_no: Joi.string(),
    driver_name: Joi.string(),
    driver_mobile: Joi.string(),
    TM_operator_name: Joi.string(),
    TM_operator_mobile_no: Joi.string(),
  }),
}

export const removeCartItemSchema: IRequestSchema = {
  body: Joi.object().keys({
    item_id: Joi.string().regex(objectIdRegex).required(),
    cart_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IGetCartItemRequest extends IOptionalAuthenticatedReq {
  params: {
    cartId?: string
    [k: string]: any
  }
  query: {
    state_id?: string
    long?: number
    lat?: number
    // with_TM: boolean
    // with_CP: boolean
    // delivery_date?: Date
  }
}

export const getCartItemSchema: IRequestSchema = {
  params: Joi.object().keys({
    cartId: Joi.string().regex(objectIdRegex),
  }),
  query: Joi.object().keys({
    state_id: Joi.string().regex(objectIdRegex),
    long: Joi.number().min(-180).max(180),
    lat: Joi.number().min(-90).max(90),
    // with_TM: Joi.boolean().required(),
    // with_CP: Joi.boolean().required(),
    // delivery_date: Joi.date(),
  }),
}

export interface IApplyCoupon extends IOptionalAuthenticatedReq {
  params: {
    cartId?: string
    [k: string]: any
  }
  body: {
    coupon_code: string
  }
}

export const applyCouponSchema: IRequestSchema = {
  params: Joi.object().keys({
    cartId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    coupon_code: Joi.string().allow("").max(500).required(),
  }),
}

export interface IAddSiteToCart extends IOptionalAuthenticatedReq {
  body: {
    site_id: string
  }
}

export const addSiteToCart: IRequestSchema = {
  body: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IAddBillingAddressToCart extends IOptionalAuthenticatedReq {
  body: {
    billing_address_id: string
  }
}

export const addBillingAddressToCart: IRequestSchema = {
  body: Joi.object().keys({
    billing_address_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IGetCouponDetails {
  is_deleted?: boolean
  is_active?: boolean
  code?: string
  buyer_id?: string
  supplier_id?: string
  discount_type?: string
  before?: string
  after?: string
}

export interface IGetCouponListRequest extends IAuthenticatedRequest {
  body: IGetCouponDetails
}

export const getCouponSchema: IRequestSchema = {
  query: Joi.object().keys({
    is_deleted: Joi.boolean(),
    is_active: Joi.boolean(),
    code: Joi.string(),
    buyer_id: Joi.string().regex(objectIdRegex),
    supplier_id: Joi.string().regex(objectIdRegex),
    discount_type: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
  }),
}
