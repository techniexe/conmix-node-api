import { GstSlabModel } from "./../../model/gst_slab.model"
import { BuyerTypes } from "./../buyer.types"
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { ICustomMix } from "../../model/cart.model"
import { AddressModel } from "../../model/address.model"
import { deliveryRange, message, discountTypes } from "../../utilities/config"
import { injectable, inject } from "inversify"
import { CartModel, ICartDoc } from "../../model/cart.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { ObjectID } from "bson"
import { SiteModel, ISiteDoc } from "../../model/site.model"
import { ProductModel } from "../../model/product.model"
import { VehicleModel } from "../../model/vehicle.model"
import {
  getDistance,
  distanceFactor,
  number_format,
} from "../../utilities/distance.utilities"
import {
  maxVehicleToPickupDistance,
  deliveryRangeForVehicle,
} from "../../utilities/config"
import { getMarginRate } from "../../utilities/rate.utility"
import { ILocation } from "../../utilities/common.interfaces"
import { ObjectId } from "bson"
import { VehicleRateModel } from "../../model/vehicle_rate.model"
import { CommonService } from "../../utilities/common.service"
import { IGetCouponDetails } from "../../admin/coupon/admin.coupon.schema"
import { CouponModel } from "../../model/coupon.model"
import { OrderModel } from "../../model/order.model"
import { VendorAdMixtureSettingModel } from "../../model/vendor_ad_mix_setting.model"
import { BuyerBillingAddressModel } from "../../model/buyer_billing_address.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { FlyAshSourceQuantityModel } from "../../model/fly_ash_source_quantity.model"

interface IItem {
  _id?: string | ObjectId
  design_mix_id: string | ObjectID
  custom_mix: ICustomMix
  vendor_id: string | ObjectID
  quantity: number
  state_id: string | ObjectID
  with_CP: boolean
  with_TM: boolean
  delivery_date: Date
  end_date: Date
  TM_no?: string
  driver_name?: string
  driver_mobile?: string
  TM_operator_name?: string
  TM_operator_mobile_no?: string
}

// interface IViewCartItem {
//   _id: string | ObjectID
//   product_id: string | ObjectID
//   quantity: number
//   quantity_unit: string
//   base_price: number
//   margin_rate: number
//   margin_price: number
//   igst_rate: number
//   cgst_rate: number
//   sgst_rate: number
//   igst_price: number
//   cgst_price: number
//   sgst_price: number
//   total_amount: number
//   added_at: Date | string
//   dvlDistance: number
//   trip_price: number
//   subTotalAmount: number
//   coupon_amount: number
//   vehicle_data: {
//     count: number
//     vehicle_sub_category_id: string | ObjectID
//     weight_unit_code: string
//     load_quantity: number
//     trip_price: number
//   }[]
//   error: boolean
//   errorMessage: string
//   productInfo: IProductInfo
//   [k: string]: any
// }
// interface IProductInfo {
//   name: string
//   category_id: string | ObjectID
//   sub_category_id: string | ObjectID
//   quantity: number
//   minimum_order: number
//   unit_price: number
//   self_logistics: boolean
//   user_id: string | ObjectID
//   pickup_location: ILocation
//   calculated_distance: number
//   rating_count: number
//   rating_sum: number
//   supplier: {
//     _id: string | ObjectID
//     full_name: string
//   }
//   productCategory: {
//     _id: string | ObjectID
//     category_name: string
//     image_url: string
//     thumbnail_url: string
//   }
//   productSubCategory: {
//     _id: string | ObjectID
//     sub_category_name: string
//     image_url: string
//     thumbnail_url: string
//     gst_slab_id: string | ObjectID
//     margin_rate_id: string | ObjectID
//     min_quantity: number
//     selling_unit: string[]
//   }
//   gst_slab: {
//     _id: string | ObjectID
//     title: string
//     igst_rate: number
//     cgst_rate: number
//     sgst_rate: number
//   }
//   margin_rate: {
//     _id: string | ObjectID
//     title: string
//     slab: [
//       {
//         _id: string | ObjectID
//         upto: number
//         rate: number
//       }
//     ]
//   }
// }

// interface IVehicleData {
//   vehicle_sub_category_id: any
//   weight_unit_code: string
//   load_quantity: number
//   min_trip_price: number
//   per_metric_ton_per_km_rate: number
//   trip_price: number
// }

// interface IVehicleDataNew extends IVehicleData {
//   count: number
// }

interface ISiteInfo extends ISiteDoc {
  city_name: string
  country_code: string
  country_name: string
  state_name: string
}

@injectable()
export class CartRepository {
  constructor(
    @inject(BuyerTypes.CommonService) private commonService: CommonService
  ) {}

  private async addOrUpdateItemToCart(
    query: { _id?: string; user_id?: string; [k: string]: any },
    cartDoc: ICartDoc,
    item: IItem,
    updtObj: { $set?: any; [k: string]: any }
  ): Promise<any> {
    console.log("itemmmmm", item)
    console.log("cartDoc.items", cartDoc.items)
    for (let i = 0; i < cartDoc.items.length; i++) {
      const doc4 = cartDoc.items[i]
      console.log(doc4._id, "doc4.design_mix_id")
      console.log(item._id, "item.design_mix_id")
      if ("" + doc4.design_mix_id === item.design_mix_id) {
        query["items._id"] = doc4._id
        updtObj.$set = updtObj.$set || {}
        updtObj.$set["items.$"] = item
        const updtResult = await CartModel.findOneAndUpdate(query, updtObj)
        if (updtResult === null) {
          return Promise.reject(
            new InvalidInput(`Sorry, we coudln't update your item in cart`, 400)
          )
        }
        return Promise.resolve(updtResult._id)
      }
      if ("" + doc4._id === item._id) {
        query["items._id"] = doc4._id
        updtObj.$set = updtObj.$set || {}
        updtObj.$set["items.$"] = item
        const updtResult = await CartModel.findOneAndUpdate(query, updtObj)
        if (updtResult === null) {
          return Promise.reject(
            new InvalidInput(`Sorry, we coudln't update your item in cart`, 400)
          )
        }
        return Promise.resolve(updtResult._id)
      }
    }
    // no item matched push item on cart
    updtObj.$push = { items: item }
    const updtResult2 = await CartModel.findOneAndUpdate(query, updtObj)
    if (updtResult2 === null) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we coudln't update your item in
cart`,
          400
        )
      )
    }
    return Promise.resolve(updtResult2._id)
  }

  async getDeliverability(
    product_id: string | ObjectID,
    deliveryLocation: ILocation,
    quantity: number,
    isAddToCart?: boolean
  ) {
    const prdt = await ProductModel.findOne({
      _id: new ObjectId(product_id),
      // is_deleted: false,
      is_available: true,
      verified_by_admin: true,
      //quantity: { $gt: quantity },
      // pickup_location: {
      //   $near: {
      //     $geometry: deliveryLocation,
      //     $maxDistance: deliveryRangeForVehicle.max * 1000,
      //   },
      // },
    })
    if (prdt === null) {
      return Promise.reject(
        new InvalidInput(
          `The selected product is either out of stock or not yet verified by Conmix/Admin.`,
          400
        )
      )
    }

    if (isAddToCart === true) {
      if (prdt.is_available === false || prdt.quantity < quantity) {
        return Promise.reject(
          new InvalidInput(
            `Make sure your Order quantity doesn't exceed product quantity.`,
            400
          )
        )
      }
    }

    const distance =
      getDistance(
        prdt.pickup_location.coordinates,
        deliveryLocation.coordinates
      ) * distanceFactor

    console.log(prdt.pickup_location)
    console.log(distance)
    const vehicles = await this.getVehiclesInRange(
      prdt.pickup_location,
      distance
    )
    //console.log(`vehicles: `, vehicles)

    //////////////////????????????//////////////////////

    // const tripCosts = getAvgTripCosts(distance, vehicles)

    // console.log(
    //   "***************************tripCosts*****************************************",
    //   tripCosts
    // )
    // if (tripCosts.length === 0) {
    //   return Promise.reject(
    //     new InvalidInput(
    //       `Currently we can't deliver this product at your location. Please check again later.`,
    //       400
    //     )
    //   )
    // }
    return Promise.resolve(vehicles)
  }

  calculateDeliveryPossibility(vehicles: any, total: number) {
    //  console.log("final", vehicles)
    let trip_arr: any[] = []
    let trip_vehicle_arr: any[] = []
    //let suggestionHigh: any[] = []
    let remaining_quantity_arr: any[] = []
    let remaining_quantity_vehicle_arr: any[] = []
    let remianing_trip_vehicle_arr: any[] = []

    const respArr: {
      trip: number
      vehicle: any
      min_capacity_vehicle_value: number
      state_id: string | ObjectID
      total_quantity: number
    }[] = []

    // const counts = new Array(vehicles.length)
    // counts.fill(0)
    // let out: {
    //   suggestion: { low: number[]; high: number[] }
    //   counts: number[]
    // } = {
    //   suggestion: { low: [], high: [] },
    //   counts: counts.slice(),
    // }

    //step 1. multiple of any one
    for (let cnt = 0; cnt < vehicles.length; cnt++) {
      const trips = total / vehicles[cnt].vehicleSubCategory.max_load_capacity

      if (total % vehicles[cnt].vehicleSubCategory.max_load_capacity === 0) {
        // out.counts[cnt] = trips
        // return out

        respArr.push({
          trip: Math.floor(trips),
          vehicle: vehicles[cnt],
          min_capacity_vehicle_value: 0,
          state_id: vehicles[cnt].state_id,
          total_quantity: total,
        })

        console.log(" >>>>>>>>>>>>>> respArr1 >>>>>>>>>>>>>>>", respArr)

        return respArr
      }

      //    if (Math.floor(trips) > 0) {
      trip_arr.push(trips)
      trip_vehicle_arr.push(vehicles[cnt])
      // } else {
      ///continue
      //   }

      let remaining_quantity =
        total % vehicles[cnt].vehicleSubCategory.max_load_capacity

      if (remaining_quantity !== total) {
        remaining_quantity_arr.push(remaining_quantity)
        remaining_quantity_vehicle_arr.push(vehicles[cnt])
      }

      // const lowArr = counts.slice()
      // const highArr = counts.slice()

      //console.log("*******lowArr**********", lowArr)
      // console.log("*******highArr**********", highArr)

      //highArr[cnt] = Math.ceil(trips)

      //console.log("******* lowArr[cnt]**********", lowArr[cnt])

      //console.log("*******highArr[cnt]**********", highArr[cnt])

      //suggestionLow.push(lowArr)
      //suggestionHigh.push(highArr)
    }

    if (remaining_quantity_arr.length !== 0) {
      let remaining_quantity_arr_index = remaining_quantity_arr.indexOf(
        Math.max(...remaining_quantity_arr)
      )

      console.log(
        "************remaining_quantity_arr_index*************",
        remaining_quantity_arr_index
      )

      let trip_value = trip_arr[remaining_quantity_arr_index]

      // console.log("************trip_arr*************", trip_arr)
      // let trip_arr_index = trip_arr.indexOf(Math.max(...trip_arr))
      // console.log("************trip_arr_index*************", trip_arr_index)

      console.log(
        "++++++++++remaining_quantity_arr++++++++++++",
        remaining_quantity_arr
      )

      console.log(
        "++++++++++remaining_quantity_arr++++++++++++",
        remaining_quantity_arr
      )

      let remaining_quantity_min_arr_index = remaining_quantity_arr.indexOf(
        Math.min(...remaining_quantity_arr)
      )

      console.log(
        "************remaining_quantity_min_arr_index*************",
        remaining_quantity_min_arr_index
      )

      // let min_capacity_vehicle_value =
      //   remaining_quantity_vehicle_arr[remaining_quantity_min_arr_index]

      let min_capacity_vehicle_value =
        total - remaining_quantity_arr[remaining_quantity_min_arr_index]

      console.log("***total****", total)
      console.log(
        "************min_capacity_vehicle_value*************",
        min_capacity_vehicle_value
      )

      console.log("trip_value", trip_value)
      respArr.push({
        trip: Math.floor(trip_value),
        vehicle: trip_vehicle_arr[remaining_quantity_arr_index],
        // min_capacity_vehicle_value:
        //   remaining_quantity_vehicle_arr[remaining_quantity_min_arr_index]
        //     .vehicleSubCategory.max_load_capacity,
        min_capacity_vehicle_value,
        state_id:
          remaining_quantity_vehicle_arr[remaining_quantity_min_arr_index]
            .state_id,
        total_quantity: total,
      })

      console.log(
        "************remaining_quantity_arr*************",
        remaining_quantity_arr
      )

      // console.log(
      //   "++++++++++remaining_quantity_vehicle_arr++++++++++++",
      //   remaining_quantity_vehicle_arr
      // )

      let quantity = remaining_quantity_arr[remaining_quantity_arr_index]

      let remianing_trip_arr: any[] = []
      for (let j = 0; j < vehicles.length; j++) {
        let remianing_trip =
          quantity / vehicles[j].vehicleSubCategory.max_load_capacity
        // remianing_trip_arr.push(remianing_trip)

        // remianing_trip_vehicle_arr.push(vehicles[j])

        if (remianing_trip < 1) {
          remianing_trip_arr.push(remianing_trip)
          remianing_trip_vehicle_arr.push(vehicles[j])
        } else {
          continue
        }
      }

      let remianing_trip_arr_index = remianing_trip_arr.indexOf(
        Math.max(...remianing_trip_arr)
      )
      console.log(
        "************remianing_trip_arr_index*************",
        remianing_trip_arr_index
      )

      // console.log(
      //   "++++++++++remianing_trip_vehicle_arr++++++++++++",
      //   remianing_trip_vehicle_arr
      // )

      let remianing_trip_value = remianing_trip_arr[remianing_trip_arr_index]

      respArr.push({
        trip: Math.ceil(remianing_trip_value),
        vehicle: remianing_trip_vehicle_arr[remianing_trip_arr_index],
        min_capacity_vehicle_value:
          remaining_quantity_vehicle_arr[remaining_quantity_min_arr_index]
            .vehicleSubCategory.max_load_capacity,
        state_id:
          remaining_quantity_vehicle_arr[remaining_quantity_min_arr_index]
            .state_id,
        total_quantity: total,
      })

      let totalTrip = Math.floor(trip_value) + Math.ceil(remianing_trip_value)

      let final_vehicles = remianing_trip_vehicle_arr[remianing_trip_arr_index]

      console.log(">>>>>>>>> totalTrip >>>>>>>>>>>>", totalTrip)

      console.log(">>>>>>>>> final_vehicles >>>>>>>>>>>>", final_vehicles)

      console.log(" >>>>>>>>>>>>>> respArr2 >>>>>>>>>>>>>>>", respArr)

      return respArr
    } else {
      let trip_arr_index = trip_arr.indexOf(Math.max(...trip_arr))
      let trip = Math.ceil(trip_arr[trip_arr_index])
      let vehicle = trip_vehicle_arr[trip_arr_index]

      respArr.push({
        trip,
        vehicle,
        min_capacity_vehicle_value: 0,
        state_id: trip_vehicle_arr[trip_arr_index].state_id,
        total_quantity: total,
      })

      console.log("******* respArr ********", respArr)
      return respArr
    }

    //step 2. sum of any combination
    // for (let c = 0; c < vehicles.length; c++) {
    //   for (let d = 0; d < vehicles.length; d++) {
    //     const seq1Len = total / vehicles[c].load_quantity

    //     console.log(
    //       "*******seq1Lennnnnnnnnnnnnnnnnnnnnnnnnn**********",
    //       seq1Len
    //     )

    //     const seq2Len = total / vehicles[d].load_quantity

    //     console.log("*******seq2Len**********", seq2Len)

    //     for (let i = 1; i < seq1Len; i++) {
    //       for (let j = 1; j < seq2Len; j++) {
    //         const currentTotal =
    //           vehicles[c].load_quantity * i + vehicles[d].load_quantity * j

    //         console.log("*******currentTotal**********", currentTotal)

    //         if (currentTotal === total) {
    //           // Lets assume c and d will never equeal here
    //           out.counts[c] = i
    //           out.counts[d] = j
    //           return out
    //         }
    //         if (currentTotal < total) {
    //           const lowArr = counts.slice()

    //           console.log("*******lowArr**********", lowArr)

    //           // console.log("*******currentTotal**********", currentTotal)

    //           // console.log("*******c**********", c)
    //           // console.log("*******d**********", d)
    //           // console.log("*******i**********", i)
    //           // console.log("*******j**********", j)

    //           if (c === d) {
    //             lowArr[c] = i + j
    //           } else {
    //             lowArr[c] = i
    //             lowArr[d] = j
    //           }

    //           //  console.log("*******lowArr**********", lowArr)

    //           // suggestionLow.push(lowArr)

    //           console.log(
    //             "******* suggestionLow222222222222**********"
    //             //  suggestionLow
    //           )
    //           //  console.log("*******suggestionLow**********", suggestionLow)
    //         } else {
    //           const highArr = counts.slice()

    //           console.log("*******highArr**********", highArr)

    //           // console.log("*******c**********", c)
    //           // console.log("*******d**********", d)
    //           // console.log("*******i**********", i)
    //           // console.log("*******j**********", j)

    //           if (c === d) {
    //             highArr[c] = i + j
    //           } else {
    //             highArr[c] = i
    //             highArr[d] = j
    //           }
    //           suggestionHigh.push(highArr)
    //           console.log(
    //             "******* suggestionHigh222222222222**********",
    //             suggestionHigh
    //           )
    //           // console.log("*******suggestionHigh**********", suggestionHigh)
    //         }
    //       }
    //     }
    //   }
    // }

    // console.log("suggestionLow*FFFFFFFFFFFFF", suggestionLow)

    // console.log("suggestionHigh*FFFFFFFFFFFFFFFFFFF", suggestionHigh)

    // out.suggestion.low = suggestionHigh.reduce((r, a) => {
    //   let oldSum = 0
    //   let newSum = 0

    //   console.log("rrrrrrr", r)
    //   console.log("aaaaaaaaaaaaa", a)
    //   for (let i = 0; i < vehicles.length; i++) {
    //     // console.log("*******LOW**********")
    //     // console.log(
    //     //   "*******vehicles[i].load_quantity**********",
    //     //   vehicles[i].load_quantity
    //     // )

    //     // console.log("*******r[i]**********", r[i])
    //     // console.log("*******r[i]**********", a[i])

    //     oldSum += vehicles[i].load_quantity * r[i]
    //     newSum += vehicles[i].load_quantity * a[i]
    //   }
    //   return oldSum > newSum ? r : a
    // })
    // out.suggestion.high = suggestionHigh.reduce((r, a) => {
    //   let oldSum = 0
    //   let newSum = 0

    //   for (let i = 0; i < vehicles.length; i++) {
    //     // console.log("*******HIGH**********")
    //     // console.log(
    //     //   "*******vehicles[i].load_quantity**********",
    //     //   vehicles[i].load_quantity
    //     // )

    //     // console.log("*******r[i]**********", r[i])
    //     // console.log("*******r[i]**********", a[i])

    //     oldSum += vehicles[i].load_quantity * r[i]
    //     newSum += vehicles[i].load_quantity * a[i]
    //   }
    //   return oldSum < newSum ? r : a
    // })
    // return out
  }

  async calculateLogisticsPriceForBuyer(
    dvlPossibility: any,
    quantity: number,
    product_id: string | ObjectID,
    deliveryLocation: ILocation
  ) {
    console.log("product_id", product_id)
    console.log("quantity", quantity)
    let query: { [k: string]: any } = {
      _id: new ObjectId(product_id),
      is_available: true,
      verified_by_admin: true,
      // quantity: { $gt: quantity },
    }

    const aggregateArr = [
      { $match: query },

      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "margin_rate",
          localField: "productSubCategory.margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: { path: "$margin_rate" },
      },
      {
        $project: {
          name: 1,
          category_id: 1,
          sub_category_id: 1,
          quantity: 1,
          minimum_order: 1,
          unit_price: 1,
          self_logistics: 1,
          user_id: 1,
          pickup_location: 1,
          calculated_distance: 1,
          rating_count: 1,
          rating_sum: 1,
          rating: 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "productSubCategory.gst_slab_id": 1,
          "productSubCategory.margin_rate_id": 1,
          "productSubCategory.min_quantity": 1,
          "productSubCategory.selling_unit": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
        },
      },
    ]
    const products = await ProductModel.aggregate(aggregateArr)
    const prdt = products[0]
    console.log("prdt", prdt)
    // const prdt = await ProductModel.findOne({
    //   _id: new ObjectId(product_id),
    //   is_available: true,
    //   verified_by_admin: true,
    //   quantity: { $gt: quantity },
    // })
    // if (prdt === null) {
    //   return Promise.reject(
    //     new InvalidInput(
    //       `Currently this product is not available at your location. Please check again later.`,
    //       400
    //     )
    //   )
    // }

    // if (prdt.is_available === false || prdt.quantity < quantity) {
    //   return Promise.reject(
    //     new InvalidInput(`Currently this product is not available.`, 400)
    //   )
    // }
    let distance =
      getDistance(
        prdt.pickup_location.coordinates,
        deliveryLocation.coordinates
      ) * distanceFactor

    distance = number_format(distance, 2)

    console.log(prdt.pickup_location)
    console.log("ddddddddddddddddddddd", distance)

    let single_trip_price = 0
    let price = 0
    let per_metric_ton_per_km_rate = 0
    let total_capacity_value = 0
    let total_capacity = 0
    for (let i = 0; i < dvlPossibility.length; i++) {
      let rateDoc = await VehicleRateModel.findOne({
        state_id: new ObjectId(dvlPossibility[i].state_id),
      })

      if (!isNullOrUndefined(rateDoc)) {
        per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
      } else {
        return Promise.reject(
          new InvalidInput(
            `Rate for this product is unvailable for requested location.`,
            400
          )
        )
      }

      console.log(" dvlPossibility[i]", dvlPossibility[i])

      single_trip_price =
        dvlPossibility[i].vehicle.vehicleSubCategory.max_load_capacity *
        //dvlPossibility[i].trip *
        distance *
        per_metric_ton_per_km_rate

      console.log("before trip_price", single_trip_price)

      if (single_trip_price < dvlPossibility[i].vehicle.min_trip_price) {
        single_trip_price = dvlPossibility[i].vehicle.min_trip_price
      }
      let trip_price = single_trip_price * dvlPossibility[i].trip
      console.log("after trip_price", single_trip_price)

      price = price + trip_price

      total_capacity_value =
        dvlPossibility[i].vehicle.vehicleSubCategory.max_load_capacity *
        dvlPossibility[i].trip

      total_capacity += total_capacity_value
    }

    console.log("total_capacity", total_capacity)
    console.log("price", price)
    const baseCost = prdt.unit_price

    let total_trip_price =
      distance * per_metric_ton_per_km_rate * total_capacity

    total_trip_price = total_trip_price / quantity

    // Rate price.
    const rate_with_supplier_and_logistics = baseCost + total_trip_price

    const marginRateForSingleQuantity = getMarginRate(
      rate_with_supplier_and_logistics,
      prdt.margin_rate.slab,
      true
    )
    const marginAmountForSingleQuantity =
      (rate_with_supplier_and_logistics * marginRateForSingleQuantity) / 100

    let price_with_margin =
      rate_with_supplier_and_logistics + marginAmountForSingleQuantity

    // let price_with_margin_and_logistics_for_total_quantity = price

    const amount_with_supplier_and_logistics = baseCost * +quantity + price
    //const amount_with_supplier_and_logistics = baseCost * total_capacity + price

    const marginRate = getMarginRate(
      amount_with_supplier_and_logistics,
      prdt.margin_rate.slab
    )
    const marginAmount = (amount_with_supplier_and_logistics * marginRate) / 100

    let price_with_margin_and_logistics_for_total_quantity =
      amount_with_supplier_and_logistics + marginAmount
    console.log(
      "amount_with_supplier_and_logistics",
      amount_with_supplier_and_logistics
    )
    console.log("marginAmount", marginAmount)
    console.log("price_with_margin", price_with_margin)
    let data = {
      quantity,
      price,
      price_with_margin,
      price_with_margin_and_logistics_for_total_quantity,
    }
    return Promise.resolve(data)
  }

  /** add items to cart   */
  async addToCart(
    design_mix_id: string,
    custom_mix: ICustomMix,
    quantity: number,
    state_id: ObjectId,
    cart_id: string,
    item_id: string,
    user_id: string | undefined,
    site_id: string,
    long: number,
    lat: number,
    with_CP: boolean,
    with_TM: boolean,
    delivery_date: Date,
    end_date: Date,
    TM_no: string,
    driver_name: string,
    driver_mobile: string,
    TM_operator_name: string,
    TM_operator_mobile_no: string
  ): Promise<{ status: string; suggestion?: any; _id?: any }> {
    const suggestion: any[] = []

    let location = {
      type: "Point",
      coordinates: [0, 0],
    }
    let stateIdOfSite: any
    if (!isNullOrUndefined(site_id)) {
      const siteCheck = await SiteModel.findOne({
        _id: site_id,
        user_id,
      }).lean()
      if (siteCheck === null) {
        return Promise.reject(
          new InvalidInput(`Kindly enter valid site information.`, 400)
        )
      }
      stateIdOfSite = siteCheck.state_id
      location = siteCheck.location
    } else if (!isNullOrUndefined(long) && !isNullOrUndefined(lat)) {
      location.coordinates = [long, lat]
    } else {
      return Promise.reject(
        new InvalidInput(`Please enter a valid delivery location.`, 400)
      )
    }

    let vendor_id: any
    let address_id: any
    if (!isNullOrUndefined(design_mix_id)) {
      console.log("design_mix", design_mix_id)
      let designMixData = await DesignMixVariantModel.findOne({
        _id: new ObjectId(design_mix_id),
      })
      if (isNullOrUndefined(designMixData)) {
        return Promise.reject(
          new InvalidInput(
            `No RMC Supplier found near the selected location.`,
            400
          )
        )
      }
      vendor_id = designMixData.vendor_id
      address_id = designMixData.address_id

      const addressData = await AddressModel.findOne({
        _id: new ObjectId(address_id),
      })

      console.log("addressData", addressData)
      if (!isNullOrUndefined(addressData)) {
        let distance1 =
          getDistance(addressData.location.coordinates, location.coordinates) *
          distanceFactor

        designMixData.distance = distance1
      }

      console.log("designMixData", designMixData)

      const query: { [k: string]: any } = {}

      query.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let AdmixSettings = await VendorAdMixtureSettingModel.findOne(query)
      console.log("AdmixSettings", AdmixSettings)
      if (!isNullOrUndefined(AdmixSettings)) {
        for (let k = 0; k < AdmixSettings.settings.length; k++) {
          if (
            designMixData.distance > AdmixSettings.settings[k].min_km &&
            designMixData.distance < AdmixSettings.settings[k].max_km
          ) {
            console.log("heree11")
            console.log(
              " AdmixSettings.settings[k].price",
              AdmixSettings.settings[k].price
            )
            designMixData.ad_mixture1_brand_id =
              AdmixSettings.settings[k].ad_mixture1_brand_id
            designMixData.ad_mixture1_category_id =
              AdmixSettings.settings[k].ad_mixture1_category_id
            designMixData.ad_mixture2_brand_id =
              AdmixSettings.settings[k].ad_mixture2_brand_id
            designMixData.ad_mixture2_category_id =
              AdmixSettings.settings[k].ad_mixture2_category_id
            break
          }
        }

        if (
          !isNullOrUndefined(designMixData) &&
          (!isNullOrUndefined(designMixData.ad_mixture1_brand_id) ||
            !isNullOrUndefined(designMixData.ad_mixture1_brand_id)) &&
          (!isNullOrUndefined(designMixData.ad_mixture1_category_id) ||
            !isNullOrUndefined(designMixData.ad_mixture2_category_id))
        ) {
          console.log("Admixxxxxxxxxxxxxxxxxxx")
          let q: { [k: string]: any } = {
            address_id: designMixData.address_id,
            //  vendor_id: products[i].vendor._id,
          }

          q.$or = [
            {
              vendor_id: new ObjectId(designMixData.vendor_id),
            },
            {
              sub_vendor_id: new ObjectId(designMixData.vendor_id),
            },
            {
              master_vendor_id: new ObjectId(designMixData.vendor_id),
            },
          ]

          q.$or = [
            {
              brand_id: new ObjectId(designMixData.ad_mixture1_brand_id),
            },
            {
              brand_id: new ObjectId(designMixData.ad_mixture2_brand_id),
            },
          ]

          q.$or = [
            {
              category_id: new ObjectId(designMixData.ad_mixture1_category_id),
            },
            {
              category_id: new ObjectId(designMixData.ad_mixture2_category_id),
            },
          ]

          // Check for admix quantity.
          let admixData = await AdmixtureQuantityModel.findOne(q)
          console.log("admixData", admixData)

          if (
            !isNullOrUndefined(admixData) &&
            admixData.quantity < designMixData.ad_mixture_quantity * quantity
          ) {
            console.log("insuff. Admix qauntity")
            return Promise.reject(
              new InvalidInput(
                `Insufficient Admix quantity in this design mix.`,
                400
              )
            )
          }
        }
      }

      // Check for cement quantity.
      const cquery: { [k: string]: any } = {
        address_id: designMixData.address_id,
        brand_id: designMixData.cement_brand_id,
        grade_id: designMixData.cement_grade_id,
      }

      cquery.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let cqData = await CementQuantityModel.findOne(cquery)

      // let cqData = await CementQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   brand_id: designMixData.cement_brand_id,
      //   grade_id: designMixData.cement_grade_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   // master_vendor_id: designMixData.master_vendor_id,
      // })

      console.log("cqData", cqData)

      if (
        !isNullOrUndefined(cqData) &&
        cqData.quantity < designMixData.cement_quantity * quantity
      ) {
        console.log("insuff. Cement qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Cement quantity in this design mix.`,
            400
          )
        )
      }

      const squery: { [k: string]: any } = {
        address_id: designMixData.address_id,
        source_id: designMixData.sand_source_id,
      }

      squery.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let sqData = await SandSourceQuantityModel.findOne(squery)

      // // Check for sand quantity.
      // let sqData = await SandSourceQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   source_id: designMixData.sand_source_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   // master_vendor_id: designMixData.master_vendor_id,
      // })
      if (
        !isNullOrUndefined(sqData) &&
        sqData.quantity < designMixData.sand_quantity * quantity
      ) {
        console.log("insuff. Sand qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Sand quantity in this design mix.`,
            400
          )
        )
      }

      const aggquery: { [k: string]: any } = {
        address_id: designMixData.address_id,
        source_id: designMixData.aggregate_source_id,
        sub_cat_id: designMixData.aggregate1_sand_category_id,
      }

      aggquery.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let agg1Data = await AggregateSourceQuantityModel.findOne(aggquery)

      // // Check for agg1 quantity.
      // let agg1Data = await AggregateSourceQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   source_id: designMixData.aggregate_source_id,
      //   sub_cat_id: designMixData.aggregate1_sub_category_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   // master_vendor_id: designMixData.master_vendor_id,
      // })
      if (
        !isNullOrUndefined(agg1Data) &&
        agg1Data.quantity < designMixData.aggregate1_quantity * quantity
      ) {
        console.log("insuff. Agg1 qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Agg1 quantity in this design mix.`,
            400
          )
        )
      }

      const agg2query: { [k: string]: any } = {
        address_id: designMixData.address_id,
        source_id: designMixData.aggregate_source_id,
        sub_cat_id: designMixData.aggregate2_sand_category_id,
      }

      agg2query.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let agg2Data = await AggregateSourceQuantityModel.findOne(agg2query)
      // Check for agg2 quantity.
      // let agg2Data = await AggregateSourceQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   source_id: designMixData.aggregate_source_id,
      //   sub_cat_id: designMixData.aggregate2_sub_category_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   // master_vendor_id: designMixData.master_vendor_id,
      // })
      if (
        !isNullOrUndefined(agg2Data) &&
        agg2Data.quantity < designMixData.aggregate2_quantity * quantity
      ) {
        console.log("insuff. Agg2 qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Agg2 quantity in this design mix.`,
            400
          )
        )
      }

      // Check for admix quantity.
      // let admixData = await AdmixtureQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   brand_id: designMixData.ad_mixture_brand_id,
      //   category_id: designMixData.ad_mixture_category_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   // master_vendor_id: designMixData.master_vendor_id,
      // })
      // if (
      //   !isNullOrUndefined(admixData) &&
      //   admixData.quantity < designMixData.ad_mixture_quantity * quantity
      // ) {
      //   console.log("insuff. Admix qauntity")
      //   return Promise.reject(
      //     new InvalidInput(
      //       `Insufficient Admix quantity in this design mix.`,
      //       400
      //     )
      //   )
      // }

      const fquery: { [k: string]: any } = {
        address_id: designMixData.address_id,
        source_id: designMixData.fly_ash_source_id,
      }

      fquery.$or = [
        {
          vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(designMixData.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(designMixData.vendor_id),
        },
      ]

      let flyAshData = await FlyAshSourceQuantityModel.findOne(fquery)

      // Check for fly ash quantity.
      // let flyAshData = await FlyAshSourceQuantityModel.findOne({
      //   address_id: designMixData.address_id,
      //   source_id: designMixData.fly_ash_source_id,
      //   vendor_id: designMixData.vendor_id,
      //   sub_vendor_id: designMixData.sub_vendor_id,
      //   //  master_vendor_id: designMixData.master_vendor_id,
      // })
      if (
        !isNullOrUndefined(flyAshData) &&
        flyAshData.quantity < designMixData.fly_ash_quantity * quantity
      ) {
        console.log("insuff. Fly Ash qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Fly Ash quantity in this design mix.`,
            400
          )
        )
      }
    } else if (!isNullOrUndefined(custom_mix)) {
      console.log("Herrrrrreeeeeeeee")
      console.log("custom_mix", custom_mix)
      vendor_id = custom_mix.vendor_id
      address_id = custom_mix.address_id

       // Check for cement quantity.
       const cquery: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        brand_id: custom_mix.final_cement_brand_id,
        grade_id: custom_mix.final_cement_grade_id,
        vendor_id: custom_mix.vendor_id,
      }

      cquery.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let cqData = await CementQuantityModel.findOne(cquery)

      // Check for cement quantity.
      // let cqData = await CementQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   brand_id: custom_mix.final_cement_brand_id,
      //   grade_id: custom_mix.final_cement_grade_id,
      //   vendor_id: custom_mix.vendor_id,
      // })
      if (
        !isNullOrUndefined(cqData) &&
        cqData.quantity < custom_mix.cement_quantity * quantity
      ) {
        console.log("insuff. Cement qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Cement quantity in this custom mix.`,
            400
          )
        )
      }

      const squery: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        source_id: custom_mix.final_sand_source_id,
       // vendor_id: custom_mix.vendor_id,
      }

      squery.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let sqData = await SandSourceQuantityModel.findOne(squery)

      // Check for sand quantity.
      // let sqData = await SandSourceQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   source_id: custom_mix.final_sand_source_id,
      //   vendor_id: custom_mix.vendor_id,
      // })
      if (
        !isNullOrUndefined(sqData) &&
        sqData.quantity < custom_mix.sand_quantity * quantity
      ) {
        console.log("insuff. Sand qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Sand quantity in this custom mix.`,
            400
          )
        )
      }

      const aggquery: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        source_id: custom_mix.final_agg_source_id,
        sub_cat_id: custom_mix.final_aggregate1_sub_cat_id,
        //vendor_id: custom_mix.vendor_id,
      }

      aggquery.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let agg1Data = await AggregateSourceQuantityModel.findOne(aggquery)

      // Check for agg1 quantity.
      // let agg1Data = await AggregateSourceQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   source_id: custom_mix.final_agg_source_id,
      //   sub_cat_id: custom_mix.final_aggregate1_sub_cat_id,
      //   vendor_id: custom_mix.vendor_id,
      // })

      if (
        !isNullOrUndefined(agg1Data) &&
        agg1Data.quantity < custom_mix.aggregate1_quantity * quantity
      ) {
        console.log("insuff. Agg1 qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Agg1 quantity in this custom mix.`,
            400
          )
        )
      }

      const agg2query: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        source_id: custom_mix.final_agg_source_id,
        sub_cat_id: custom_mix.final_aggregate2_sub_cat_id,
       // vendor_id: custom_mix.vendor_id,
      }

      agg2query.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let agg2Data = await AggregateSourceQuantityModel.findOne(agg2query)

      // Check for agg2 quantity.
      // let agg2Data = await AggregateSourceQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   source_id: custom_mix.final_agg_source_id,
      //   sub_cat_id: custom_mix.final_aggregate2_sub_cat_id,
      //   vendor_id: custom_mix.vendor_id,
      // })
      if (
        !isNullOrUndefined(agg2Data) &&
        agg2Data.quantity < custom_mix.aggregate2_quantity * quantity
      ) {
        console.log("insuff. Agg2 qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Agg2 quantity in this custom mix.`,
            400
          )
        )
      }

      const admixquery: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        brand_id: custom_mix.final_admix_brand_id,
        category_id: custom_mix.final_admix_cat_id,
       // vendor_id: custom_mix.vendor_id,
      }

      admixquery.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let admixData = await AdmixtureQuantityModel.findOne(admixquery)

      // Check for admix quantity.
      // let admixData = await AdmixtureQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   brand_id: custom_mix.final_admix_brand_id,
      //   category_id: custom_mix.final_admix_cat_id,
      //   vendor_id: custom_mix.vendor_id,
      // })
      if (
        !isNullOrUndefined(admixData) &&
        admixData.quantity < custom_mix.admix_quantity * quantity
      ) {
        console.log("insuff. Admix qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Admix quantity in this custom mix.`,
            400
          )
        )
      }

      // Check for fly ash quantity.

      const fquery: { [k: string]: any } = {
        address_id: custom_mix.address_id,
        source_id: custom_mix.final_fly_ash_source_id,
        //vendor_id: custom_mix.vendor_id,
      }

      fquery.$or = [
        {
          vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
        {
          master_vendor_id: new ObjectId(custom_mix.vendor_id),
        },
      ]

      let flyAshData = await FlyAshSourceQuantityModel.findOne(fquery)

      // let flyAshData = await FlyAshSourceQuantityModel.findOne({
      //   address_id: custom_mix.address_id,
      //   source_id: custom_mix.final_fly_ash_source_id,
      //   vendor_id: custom_mix.vendor_id,
      // })
      if (
        !isNullOrUndefined(flyAshData) &&
        flyAshData.quantity < custom_mix.fly_ash_quantity * quantity
      ) {
        console.log("insuff. Fly Ash qauntity")
        return Promise.reject(
          new InvalidInput(
            `Insufficient Fly Ash quantity in this custom mix.`,
            400
          )
        )
      }
    }

    let distance = await this.checkDistance(address_id, location)
    console.log("**********distance***************", distance)
    console.log("**********vendor_id***************", vendor_id)

    const item: IItem = {
      _id: item_id,
      design_mix_id,
      custom_mix,
      vendor_id,
      quantity,
      state_id,
      with_CP,
      with_TM,
      delivery_date,
      end_date,
    }
    const newCartData = {
      user_id,
      items: [item],
      site_id,
      delivery_location: location,
      //  state_id,
      unique_id: Math.ceil(1000 + Math.random() * 9000),
    }

    /************CHECK FOR "TM"**************** */
    let TM_ids: any[] = []
    if (!isNullOrUndefined(with_TM) && with_TM === true) {
      const resp = await this.commonService.checkTMavailibity(
        vendor_id,
        delivery_date
      )
      console.log("TM_resp", resp)
      if (resp.with_TM === true && resp.msgForTM === message.AvailableTM) {
        TM_ids.push(vendor_id)
        console.log("TM_vendor_ids", TM_ids)
      }
      if (TM_ids.length <= 0) {
        suggestion.push({
          ErrMsgForTM: `Sorry, no Transit Mixer available on the selected date.`,
        })
      }
    } else if (!isNullOrUndefined(with_TM) && with_TM === false) {
      // if (!isNullOrUndefined(custom_mix)) {
      //   console.log("Heree")
      //   custom_mix.TM_no = TM_no
      //   custom_mix.driver_name = driver_name
      //   custom_mix.driver_mobile = driver_mobile
      //   custom_mix.TM_operator_name = TM_operator_name
      //   custom_mix.TM_operator_mobile_no = TM_operator_mobile_no
      // } else {
      item.TM_no = TM_no
      item.driver_name = driver_name
      item.driver_mobile = driver_mobile
      item.TM_operator_name = TM_operator_name
      item.TM_operator_mobile_no = TM_operator_mobile_no
      // }
    }

    /************CHECK FOR "CP"**************** */
    let CP_ids: any[] = []
    if (!isNullOrUndefined(with_CP) && with_CP === true) {
      const resp = await this.commonService.checkCPavailibity(
        vendor_id,
        delivery_date
      )
      console.log("TM_resp", resp)
      if (resp.with_CP === true && resp.msgForCP === message.AvailableCP) {
        CP_ids.push(vendor_id)
        console.log("TM_vendor_ids", CP_ids)
      }
      if (CP_ids.length <= 0) {
        suggestion.push({
          ErrMsgForCp: `Sorry, no Concrete pump available on the selected date.`,
        })
      }
    }

    console.log("********newCartData**************", newCartData)
    if (!isNullOrUndefined(user_id) && !isNullOrUndefined(cart_id)) {
      console.log("user_id", user_id)
      console.log("cart_id", cart_id)
      const userCartData = await CartModel.findOne({ user_id })
      const cartDataUsingId = await CartModel.findById(cart_id)
      //both are null on db. create new cart and return new cart id
      if (userCartData === null && cartDataUsingId === null) {
        const doc3 = await new CartModel(newCartData).save()
        return Promise.resolve({ status: "success", _id: doc3._id, suggestion })
      }
      //usercart is null but cart id has data.
      if (userCartData === null && cartDataUsingId !== null) {
        // cart id is associated with another user. creating new cart for this user.
        if (!isNullOrUndefined(cartDataUsingId.user_id)) {
          const rData1 = await new CartModel(newCartData).save()
          return Promise.resolve({
            status: "success",
            _id: rData1._id,
            suggestion,
          })
        }

        // else part. add item to this cart id and add user id to cart.
        await this.addOrUpdateItemToCart(
          //
          { _id: cart_id },
          cartDataUsingId,
          item,
          { $set: { user_id, site_id, state_id, suggestion } }
        )
        return Promise.resolve({ status: "success", _id: cart_id })
      }
      // user has cart but provided cart id is wrong. update to user's cart and return user's cart id
      if (userCartData !== null && cartDataUsingId === null) {
        const rData2 = await this.addOrUpdateItemToCart(
          { user_id },
          userCartData,
          item,
          {}
        )
        return Promise.resolve({ status: "success", _id: rData2 })
      }
      if (userCartData !== null && cartDataUsingId !== null) {
        console.log(
          "herrrrrrrrrrrrrrrrrrrrrrrrreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        )
        // both are same. only add item
        if (userCartData._id === cartDataUsingId._id) {
          await this.addOrUpdateItemToCart(
            { _id: cart_id },
            userCartData,
            item,
            {}
          )
          return Promise.resolve({
            status: "success",
            _id: cart_id,
            suggestion,
          })
        }
        //check if cart id is of different user. add items to user cart and return user's cart id
        if (!isNullOrUndefined(cartDataUsingId.user_id)) {
          const rData3 = await this.addOrUpdateItemToCart(
            { user_id },
            userCartData,
            item,
            {}
          )
          return Promise.resolve({ status: "success", _id: rData3 })
        }
        // cart id is not associated with any user. add all items of cart id to user cart and delete cart id
        cartDataUsingId.items.forEach(async (cDoc) => {
          console.log("cDoc", cDoc)
          await this.addOrUpdateItemToCart(
            { user_id },
            userCartData,
            {
              design_mix_id: cDoc.design_mix_id,
              custom_mix: cDoc.custom_mix,
              vendor_id,
              quantity,
              state_id,
              with_CP: cDoc.with_CP,
              with_TM: cDoc.with_TM,
              delivery_date: cDoc.delivery_date,
              end_date: cDoc.end_date,
            },
            {}
          )
        })
        await this.addOrUpdateItemToCart({ user_id }, userCartData, item, {})
        await CartModel.deleteOne({ _id: cart_id })
        return Promise.resolve({
          status: "success",
          _id: userCartData._id,
          suggestion,
        })
      }
      console.log("userid and cart id")
      await CartModel.updateOne(
        {
          _id: cart_id,
          "items.design_mix_id": design_mix_id,
          "items.custom_mix": custom_mix,
        },
        {
          $set: { user_id, site_id, state_id, "": item },
        },
        { upsert: true }
      )
      return Promise.resolve({ status: "success", _id: cart_id, suggestion })
    }

    if (!isNullOrUndefined(user_id)) {
      console.log("userid only")
      // here user is login and may be items on cart
      const userCartDoc = await CartModel.findOne({ user_id })

      console.log("userCartDoc", userCartDoc)
      if (userCartDoc === null) {
        // doesn't have cart. Create for new cart
        const doc3 = await new CartModel(newCartData).save()
        return Promise.resolve({ status: "success", _id: doc3._id, suggestion })
      }

      const rData4 = await this.addOrUpdateItemToCart(
        { user_id },
        userCartDoc,
        item,
        {}
      )
      // HEEEEEEEEEEE
      if (
        !isNullOrUndefined(site_id) &&
        !isNullOrUndefined(state_id) &&
        new ObjectId(state_id).equals(new ObjectId(stateIdOfSite))
      ) {
        await CartModel.findOneAndUpdate(
          { user_id: new ObjectId(user_id) },
          { $set: { site_id: new ObjectId(site_id) } }
        )
      } else {
        await CartModel.updateOne(
          { user_id: new ObjectId(user_id) },
          { $unset: { site_id: "" }, $set: { delivery_location: location } }
        )
      }
      return Promise.resolve({ status: "success", _id: rData4, suggestion })
    }
    if (!isNullOrUndefined(cart_id)) {
      // If user is not login but have items on cart
      console.log(
        "cart id ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
      )
      const docFound = await CartModel.findById(cart_id)
      if (docFound === null) {
        return Promise.reject(
          new InvalidInput(`Invalid cart Id as this cart no longer exist.`, 400)
        )
      }

      console.log("docFound", docFound)
      await this.addOrUpdateItemToCart({ _id: cart_id }, docFound, item, {})
      return Promise.resolve({ status: "success", _id: cart_id, suggestion })
    }
    console.log("without userid or cartid")
    // user is nither login nor have item on cart

    const { _id } = await new CartModel(newCartData).save()

    return Promise.resolve({ status: "success", _id, suggestion })
  }

  async checkDistance(address_id: string, location: ILocation) {
    let addressData = await AddressModel.findOne({
      _id: new ObjectId(address_id),
    })
    console.log("address_id", address_id)
    console.log("location", location)
    console.log("addressData", addressData)
    if (!isNullOrUndefined(addressData)) {
      //let design_mix_address_loc = addressData.location
      let distance =
        getDistance(addressData.location.coordinates, location.coordinates) *
        distanceFactor

      console.log("distance", distance)

      if (distance > deliveryRange.max * 1000) {
        return Promise.reject(
          new InvalidInput(
            `The delivery location entered is out of our service range.`,
            400
          )
        )
      }
      return distance
    } else {
      return Promise.reject(
        new InvalidInput(
          `No valid Address result found of design mix or custom mix.`,
          400
        )
      )
    }
  }

  /**Remove items from cart */
  async removeItemsFromCart(cart_id: string, item_id: string) {
    const updt = {
      $pull: { items: { _id: new ObjectID(item_id) } },
    }
    const updtRes = await CartModel.findOneAndUpdate(
      { _id: cart_id, "items._id": item_id },
      updt,
      { new: true }
    )
    console.log(`updtRes`, updtRes)
    if (updtRes === null) {
      return Promise.reject(new InvalidInput(`Cart is already empty`, 400))
    }
    if (updtRes.items.length === 0) {
      // removing empty cart
      await CartModel.deleteOne({ _id: cart_id })
    }
    return Promise.resolve()
  }

  async getCartItem(
    // with_TM: boolean,
    // with_CP: boolean,
    // delivery_date?: Date,
    cart_id?: string,
    user_id?: string,
    state_id?: string,
    long?: number,
    lat?: number
  ) {
    let location = {
      type: "Point",
      coordinates: [0, 0],
    }
    let query: { [k: string]: any } = {}
    if (isNullOrUndefined(cart_id) && isNullOrUndefined(user_id)) {
      return Promise.reject(
        new InvalidInput(`Kindly enter either user Id or cart Id`, 400)
      )
    }

    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }
    let userCartData = await CartModel.findOne(query)

    if (isNullOrUndefined(userCartData)) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }
    if (!isNullOrUndefined(cart_id)) {
      query._id = new ObjectId(cart_id)
    }

    if (!isNullOrUndefined(state_id)) {
      query["items.state_id"] = new ObjectId(state_id)
    }
    console.log("query", query)
    let data = await CartModel.findOne(query).lean()
    console.log("old data", data)
    if (data === null) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }

    if (
      isNullOrUndefined(data.site_id) &&
      !isNullOrUndefined(long) &&
      !isNullOrUndefined(lat)
    ) {
      location.coordinates = [long, lat]
      await CartModel.updateOne(
        { user_id: new ObjectId(user_id) },
        { $set: { delivery_location: location } }
      )
    }

    if (
      !isNullOrUndefined(data.site_id) &&
      !isNullOrUndefined(long) &&
      !isNullOrUndefined(lat)
    ) {
      location.coordinates = [long, lat]
      const siteCheck = await SiteModel.findOne({
        _id: data.site_id,
        user_id,
      }).lean()
      if (siteCheck === null) {
        return Promise.reject(
          new InvalidInput(`Kindly enter valid site information.`, 400)
        )
      }
      let stateIdOfSite = siteCheck.state_id
      console.log(
        new ObjectId(state_id).equals(new ObjectId(stateIdOfSite)),
        "heree"
      )
      if (
        new ObjectId(state_id).equals(new ObjectId(stateIdOfSite)) === false
      ) {
        await CartModel.updateOne(
          { user_id: new ObjectId(user_id) },
          { $unset: { site_id: "" }, $set: { delivery_location: location } }
        )
      }
    }

    console.log("^^^^^^^^^^^^^^^^^^^^^^^^^query^^^^^^^^^^^^^^^^^^^", query)
    let cartData: any = await CartModel.findOne(query).lean()
    if (cartData === null) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }
    if (
      isNullOrUndefined(cartData.site_id) &&
      isNullOrUndefined(long) &&
      isNullOrUndefined(lat)
    ) {
      location.coordinates = [
        cartData.delivery_location.coordinates[0],
        cartData.delivery_location.coordinates[1],
      ]
    }

    if (!isNullOrUndefined(cartData.site_id)) {
      cartData.siteInfo = await this.getSiteInfo(cartData.site_id)
      location.coordinates = [
        cartData.delivery_location.coordinates[0],
        cartData.delivery_location.coordinates[1],
      ]
    }

    if (!isNullOrUndefined(cartData.coupon_code_id)) {
      const couponInfo = await CouponModel.findOne({
        _id: cartData.coupon_code_id,
      })
      if (
        couponInfo === null ||
        couponInfo.is_deleted ||
        !couponInfo.is_active ||
        new Date(couponInfo.start_date).getTime() > Date.now()
      ) {
        cartData.couponError = "Kindly enter a valid coupon"
      } else if (new Date(couponInfo.end_date).getTime() < Date.now()) {
        cartData.couponError = "This Coupon is no longer available."
      } else if (couponInfo.buyer_ids.length > 0) {
        if (isNullOrUndefined(user_id)) {
          cartData.couponError = "Kindly login to apply the coupon"
        } else if (
          !couponInfo.buyer_ids.map((bId) => "" + bId).includes(user_id)
        ) {
          cartData.couponError = "Invalid Coupon Code"
        }
      } else if (
        couponInfo.max_usage !== -1 &&
        (isNullOrUndefined(couponInfo.remaining_usage) ||
          couponInfo.remaining_usage < 1)
      ) {
        cartData.couponError =
          "You cannot use this coupon as you've already used it multiple times"
      } else if (couponInfo.unique_use && !isNullOrUndefined(user_id)) {
        const usedOrder = await OrderModel.findOne({
          user_id,
          coupon_code_id: cartData.coupon_code_id,
        })
        if (!isNullOrUndefined(usedOrder)) {
          cartData.couponError = "You are allowed to use this coupon now"
        }
      }
      if (!isNullOrUndefined(couponInfo)) {
        cartData.couponInfo = couponInfo
      }
    }

    cartData.totalNoOfItems = userCartData.items.length
    cartData.selling_price = 0
    cartData.margin_price = 0
    cartData.TM_price = 0
    cartData.CP_price = 0
    cartData.total_CP_price = 0
    cartData.coupon_amount = 0
    cartData.cgst_price = 0
    cartData.sgst_price = 0
    cartData.igst_price = 0
    cartData.gst_price = 0
    cartData.total_amount = 0
    cartData.selling_price_With_Margin = 0
    // let couponApplied = false

    if (
      !isNullOrUndefined(cartData.site_id) &&
      isNullOrUndefined(state_id) &&
      isNullOrUndefined(long) &&
      isNullOrUndefined(lat)
    ) {
      const siteCheck = await SiteModel.findOne({
        _id: cartData.site_id,
        user_id,
      }).lean()
      if (siteCheck === null) {
        return Promise.reject(
          new InvalidInput(`Kindly enter valid site information.`, 400)
        )
      }
      let stateIdOfSite = siteCheck.state_id

      let items = cartData.items
      cartData.items = []
      for (let i = 0; i < items.length; i++) {
        if (items[i].state_id.equals(new ObjectId(stateIdOfSite))) {
          cartData.items.push(items[i])
        }
      }
    }

    // Find items thaty only belongs to specific state.
    if (!isNullOrUndefined(state_id)) {
      let items = cartData.items
      cartData.items = []
      for (let i = 0; i < items.length; i++) {
        if (items[i].state_id.equals(new ObjectId(state_id))) {
          cartData.items.push(items[i])
        }
      }
    }

    let with_TM: any
    let with_CP: any
    let delivery_date: any
    let end_date: any
    await Promise.all(
      cartData.items.map(async (item: any) => {
        let vendor_id: any
        let address_id: any
        let cement_quantity: any
        let sand_quantity: any
        let aggregate1_quantity: any
        let aggregate2_quantity: any
        let fly_ash_quantity: any
        let admix_quantity: any
        let water_quantity: any
        let final_admix_brand_id: any
        let final_admix_cat_id: any
        let final_fly_ash_source_id: any
        let final_aggregate2_sub_cat_id: any
        let final_agg_source_id: any
        let final_aggregate1_sub_cat_id: any
        let final_sand_source_id: any
        let final_cement_brand_id: any
        let final_cement_grade_id: any
        let concrete_grade_id: any
        //let selling_price: any

        let doc: any = {}

        console.log("itemmmmmmmmm", item)
        if (!isNullOrUndefined(item.design_mix_id)) {
          console.log("Design mix")
          let designMixData = await DesignMixVariantModel.findOne({
            _id: new ObjectId(item.design_mix_id),
          })
          if (isNullOrUndefined(designMixData)) {
            return Promise.reject(
              new InvalidInput(
                `No RMC Supplier found near the selected location.`,
                400
              )
            )
          }

          doc.vendor_id = designMixData.vendor_id
          doc.address_id = designMixData.address_id
          doc.cement_quantity = designMixData.cement_quantity
          doc.sand_quantity = designMixData.sand_quantity
          doc.aggregate1_quantity = designMixData.aggregate1_quantity
          doc.aggregate2_quantity = designMixData.aggregate2_quantity
          doc.fly_ash_quantity = designMixData.fly_ash_quantity
          doc.admix_quantity = designMixData.ad_mixture_quantity
          doc.water_quantity = designMixData.water_quantity
          doc.final_admix_brand_id = designMixData.ad_mixture_brand_id
          doc.final_admix_cat_id = designMixData.ad_mixture_category_id
          doc.final_fly_ash_source_id = designMixData.fly_ash_source_id
          doc.final_aggregate2_sub_cat_id =
            designMixData.aggregate2_sub_category_id
          doc.final_agg_source_id = designMixData.aggregate_source_id
          doc.final_aggregate1_sub_cat_id =
            designMixData.aggregate1_sub_category_id
          doc.final_sand_source_id = designMixData.sand_source_id
          doc.final_cement_brand_id = designMixData.cement_brand_id
          doc.final_cement_grade_id = designMixData.cement_grade_id
          doc.concrete_grade_id = designMixData.grade_id

          const addressData = await AddressModel.findById(doc.address_id)
          if (!isNullOrUndefined(addressData)) {
            console.log(
              "addressData.location.coordinates",
              addressData.location.coordinates
            )
            console.log("item", item)
            console.log("location.coordinates", location.coordinates)

            let distance =
              getDistance(
                addressData.location.coordinates,
                location.coordinates
              ) * distanceFactor

            console.log("designMixData", designMixData)
            const query1: { [k: string]: any } = {}

            query1.$or = [
              {
                vendor_id: new ObjectId(designMixData.vendor_id),
              },
              {
                vendor_id: new ObjectId(designMixData.sub_vendor_id),
              },
              {
                vendor_id: new ObjectId(designMixData.master_vendor_id),
              },
            ]

            // let data = await CementQuantityModel.findOne(query)

            let AdmixSettings = await VendorAdMixtureSettingModel.findOne(
              query1
            )

            console.log("AdmixSettings", AdmixSettings)
            if (!isNullOrUndefined(AdmixSettings)) {
              for (let k = 0; k < AdmixSettings.settings.length; k++) {
                if (
                  distance > AdmixSettings.settings[k].min_km &&
                  distance < AdmixSettings.settings[k].max_km
                ) {
                  designMixData.admix_price = AdmixSettings.settings[k].price
                  doc.ad_mixture1_brand_id =
                    AdmixSettings.settings[k].ad_mixture1_brand_id
                  doc.ad_mixture1_category_id =
                    AdmixSettings.settings[k].ad_mixture1_category_id
                  doc.ad_mixture2_brand_id =
                    AdmixSettings.settings[k].ad_mixture2_brand_id
                  doc.ad_mixture2_category_id =
                    AdmixSettings.settings[k].ad_mixture2_category_id
                    break
                } else {
                  console.log("Admixxxxxxxxxxxxxx", AdmixSettings)
                  designMixData.admix_price = 0
                }
              }
              if (designMixData.admix_price !== 0) {
                doc.admix_price =
                  designMixData.admix_price * designMixData.ad_mixture_quantity
                doc.selling_price =
                  designMixData.selling_price + doc.admix_price
              }
            } else {
              doc.admix_price = 0
            }
            console.log("doc.admix_priceee", doc.admix_price)
            doc.unit_price = designMixData.selling_price
            console.log(" doc.unit_price", doc.unit_price)
            doc.selling_price = designMixData.selling_price * item.quantity
            console.log(" doc.selling_price", doc.selling_price)
            doc.with_TM = item.with_TM
            doc.with_CP = item.with_CP
            doc.delivery_date = item.delivery_date
            doc.end_date = item.end_date

            console.log("responseeeeeee", doc)
            // doc.concrete_grade_id = concrete_grade_id

            /****Check CP PRICE***************** */
            if (!isNullOrUndefined(doc.with_CP) && doc.with_CP === true) {
              let cpData = await this.commonService.checkCPavailibity(
                doc.vendor_id,
                doc.delivery_date
              )

              console.log("cpData1111", cpData)

              if (item.with_CP === true && cpData.with_CP === false) {
                await CartModel.findOneAndUpdate(
                  {
                    _id: cart_id,
                    "items._id": new ObjectId(item._id),
                    "items.design_mix_id": new ObjectId(item.design_mix_id),
                  },
                  {
                    $set: {
                      "items.$.with_CP": false,
                    },
                  }
                )
                doc.ErrMsgForCp = `Sorry, no Concrete pump available on the selected date.`
                doc.CP_price = 0
              }
              if (
                cpData.with_CP === true &&
                cpData.msgForCP === message.AvailableCP
              ) {
                doc.with_CP = item.with_CP
                doc.CP_price = cpData.CP_price
              } else {
                doc.ErrMsgForCp = `Sorry, no Concrete pump available on the selected date.`
              }
            } else {
              doc.CP_price = 0
            }

            /**********Check TM PRICE */

            if (!isNullOrUndefined(doc.with_TM) && doc.with_TM === true) {
              let tmData = await this.commonService.checkTMavailibity(
                doc.vendor_id,
                doc.delivery_date
              )

              console.log("tmData", tmData)

              if (item.with_TM === true && tmData.with_TM === false) {
                await CartModel.findOneAndUpdate(
                  {
                    _id: cart_id,
                    "items._id": new ObjectId(item._id),
                    "items.design_mix_id": new ObjectId(item.design_mix_id),
                  },
                  {
                    $set: {
                      "items.$.with_TM": false,
                      "items.$.with_CP": false,
                    },
                  }
                )
                doc.ErrMsgForTM = `Sorry, no Transit Mixer available on the selected date.`
                doc.TM_price = 0
                doc.TM_price_for_unit = 0
              }
              if (
                tmData.with_TM === true &&
                tmData.msgForTM === message.AvailableTM
              ) {
                doc.with_TM = item.with_TM
                doc.TM_price = tmData.TM_price * distance * item.quantity
                doc.TM_price_for_unit = tmData.TM_price * distance
              } else {
                doc.ErrMsgForTM = `Sorry, no Transit Mixer available on the selected date.`
              }
            } else {
              doc.TM_price = 0
              doc.TM_price_for_unit = 0
            }
          } else {
            return Promise.reject(
              new InvalidInput(`No Associated address data found...!`, 400)
            )
          }
          console.log("doc.unit_price", doc.unit_price)
          console.log("doc.TM_price_for_unit", doc.TM_price_for_unit)
          console.log("doc.CP_price", doc.CP_price)

          doc.unit_price = doc.unit_price + doc.TM_price_for_unit + doc.CP_price

          console.log("doc.unit_price", doc.unit_price)
        } else if (!isNullOrUndefined(item.custom_mix)) {
          console.log("Custom mix")
          vendor_id = item.custom_mix.vendor_id
          address_id = item.custom_mix.address_id
          cement_quantity = item.custom_mix.cement_quantity
          sand_quantity = item.custom_mix.sand_quantity
          aggregate1_quantity = item.custom_mix.aggregate1_quantity
          aggregate2_quantity = item.custom_mix.aggregate2_quantity
          fly_ash_quantity = item.custom_mix.fly_ash_quantity
          admix_quantity = item.custom_mix.admix_quantity
          water_quantity = item.custom_mix.water_quantity
          final_admix_brand_id = item.custom_mix.final_admix_brand_id
          final_admix_cat_id = item.custom_mix.final_admix_cat_id
          final_fly_ash_source_id = item.custom_mix.final_fly_ash_source_id
          final_aggregate2_sub_cat_id =
            item.custom_mix.final_aggregate2_sub_cat_id
          final_agg_source_id = item.custom_mix.final_agg_source_id
          final_aggregate1_sub_cat_id =
            item.custom_mix.final_aggregate1_sub_cat_id
          final_sand_source_id = item.custom_mix.final_sand_source_id
          final_cement_brand_id = item.custom_mix.final_cement_brand_id
          final_cement_grade_id = item.custom_mix.final_cement_grade_id
          concrete_grade_id = item.custom_mix.concrete_grade_id
          with_TM = item.with_TM
          with_CP = item.with_CP
          delivery_date = item.delivery_date
          end_date = item.end_date

          let suggestion: any[] = []
          /************CHECK FOR "TM"**************** */
          let TM_ids: any[] = []

          if (
            !isNullOrUndefined(with_TM) &&
            with_TM === true &&
            !isNullOrUndefined(delivery_date)
          ) {
            const resp2 = await this.commonService.checkTMavailibity(
              vendor_id,
              delivery_date
            )

            if (item.with_TM === true && resp2.with_TM === false) {
              await CartModel.findOneAndUpdate(
                {
                  _id: cart_id,
                  "items._id": new ObjectId(item._id),
                },
                {
                  $set: {
                    "items.$.with_TM": false,
                    "items.$.with_CP": false,
                  },
                }
              )
              suggestion.push({
                ErrMsgForTM: `Sorry, no Transit Mixer available on the selected date.`,
              })
            }
            console.log("TM_resp", resp2)
            if (
              resp2.with_TM === true &&
              resp2.msgForTM === message.AvailableTM
            ) {
              TM_ids.push(vendor_id)
              console.log("TM_vendor_ids", TM_ids)
            }
            if (TM_ids.length <= 0) {
              suggestion.push({
                ErrMsgForTM: `Sorry, no Transit Mixer available on the selected date.`,
              })
            }
          }

          /************CHECK FOR "CP"**************** */
          let CP_ids: any[] = []
          if (!isNullOrUndefined(with_CP) && with_CP === true) {
            const resp1 = await this.commonService.checkCPavailibity(
              vendor_id,
              delivery_date
            )

            if (item.with_CP === true && resp1.with_CP === false) {
              await CartModel.findOneAndUpdate(
                {
                  _id: cart_id,
                  "items._id": new ObjectId(item._id),
                },
                {
                  $set: {
                    "items.$.with_CP": false,
                  },
                }
              )
              suggestion.push({
                ErrMsgForCp: `Sorry, no Concrete pump available on the selected date.`,
              })
            }

            console.log("CP_resp", resp1)
            if (
              resp1.with_CP === true &&
              resp1.msgForCP === message.AvailableCP
            ) {
              CP_ids.push(vendor_id)
              console.log("CP_vendor_ids", CP_ids)
            }
            if (CP_ids.length <= 0) {
              suggestion.push({
                ErrMsgForCp: `Sorry, no Concrete pump available on the selected date.`,
              })
            }
          }
          item.suggestion = suggestion
        }
        if (!isNullOrUndefined(item.custom_mix)) {
          // try {
          let req: any = {
            final_admix_brand_id,
            final_admix_cat_id,
            final_fly_ash_source_id,
            final_aggregate2_sub_cat_id,
            final_agg_source_id,
            final_aggregate1_sub_cat_id,
            final_sand_source_id,
            final_cement_brand_id,
            final_cement_grade_id,
          }

          console.log("reqqqqqq", req)
          doc = await this.commonService.findPerKgRate(
            req,
            vendor_id,
            address_id,
            cement_quantity,
            sand_quantity,
            aggregate1_quantity,
            aggregate2_quantity,
            fly_ash_quantity,
            admix_quantity,
            water_quantity,
            with_CP,
            with_TM,
            delivery_date,
            end_date,
            location,
            item.quantity
          )
        }
        console.log("responseeeeeee111", doc)
        let resp = await this.commonService.getDetails([doc])

        console.log("final", resp)
        if (!isNullOrUndefined(item.design_mix_id)) {
          concrete_grade_id = doc.concrete_grade_id
        }
        concrete_grade_id = resp[0].concrete_grade_id
        item.concrete_grade_id = concrete_grade_id
        item.final_admix_brand_id = resp[0].final_admix_brand_id
        item.final_admix_cat_id = resp[0].final_admix_cat_id
        item.final_fly_ash_source_id = resp[0].final_fly_ash_source_id
        item.final_aggregate2_sub_cat_id = resp[0].final_aggregate2_sub_cat_id
        item.final_agg_source_id = resp[0].final_agg_source_id
        item.final_aggregate1_sub_cat_id = resp[0].final_aggregate1_sub_cat_id
        item.final_sand_source_id = resp[0].final_sand_source_id
        item.final_cement_brand_id = resp[0].final_cement_brand_id
        item.final_cement_grade_id = resp[0].final_cement_grade_id
        item.vendor_id = resp[0].vendor_id
        item.address_id = resp[0].address_id
        item.cement_quantity = resp[0].cement_quantity
        item.sand_quantity = resp[0].sand_quantity
        item.aggregate1_quantity = resp[0].aggregate1_quantity
        item.aggregate2_quantity = resp[0].aggregate2_quantity
        item.fly_ash_quantity = resp[0].fly_ash_quantity
        item.admix_quantity = resp[0].admix_quantity
        item.water_quantity = resp[0].water_quantity
        item.cement_per_kg_rate = resp[0].cement_per_kg_rate
        item.sand_per_kg_rate = resp[0].sand_per_kg_rate
        item.aggregate1_per_kg_rate = resp[0].aggregate1_per_kg_rate
        item.aggregate2_per_kg_rate = resp[0].aggregate2_per_kg_rate
        item.fly_ash_per_kg_rate = resp[0].fly_ash_per_kg_rate
        item.admix_per_kg_rate = resp[0].admix_per_kg_rate
        item.water_per_ltr_rate = resp[0].water_per_ltr_rate
        item.cement_price = resp[0].cement_price
        item.sand_price = resp[0].sand_price
        item.aggreagte1_price = resp[0].aggreagte1_price
        item.aggreagte2_price = resp[0].aggreagte2_price
        item.fly_ash_price = resp[0].fly_ash_price
        item.admix_price = resp[0].admix_price
        item.water_price = resp[0].water_price
        item.with_TM = resp[0].with_TM
        item.with_CP = resp[0].with_CP
        item.distance = resp[0].distance
        item.concrete_grade_name = resp[0].concrete_grade_name
        item.cement_grade_name = resp[0].cement_grade_name
        item.cement_brand_name = resp[0].cement_brand_name
        item.sand_source_name = resp[0].sand_source_name
        item.aggregate1_sub_category_name = resp[0].aggregate1_sub_category_name
        item.aggregate2_sub_category_name = resp[0].aggregate2_sub_category_name
        item.aggregate_source_name = resp[0].aggregate_source_name
        item.fly_ash_source_name = resp[0].fly_ash_source_name
        item.admix_category_name = resp[0].admix_category_name
        item.admix_brand_name = resp[0].admix_brand_name
        item.vendor_media = resp[0].vendor_media
        item.vendor_name = resp[0].vendor_name
        item.company_name = resp[0].company_name
        item.cement_per_kg_rate = resp[0].cement_per_kg_rate
        item.sand_per_kg_rate = resp[0].sand_per_kg_rate
        item.aggregate1_per_kg_rate = resp[0].aggregate1_per_kg_rate
        item.aggregate2_per_kg_rate = resp[0].aggregate2_per_kg_rate
        item.fly_ash_per_kg_rate = resp[0].fly_ash_per_kg_rate
        item.admix_per_kg_rate = resp[0].admix_per_kg_rate
        item.water_per_ltr_rate = resp[0].water_per_ltr_rate
        item.cement_price = resp[0].cement_price
        item.sand_price = resp[0].sand_price
        item.aggreagte1_price = resp[0].aggreagte1_price
        item.aggreagte2_price = resp[0].aggreagte2_price
        item.fly_ash_price = resp[0].fly_ash_price
        item.admix_price = resp[0].admix_price
        item.water_price = resp[0].water_price
        item.with_TM = resp[0].with_TM
        item.with_CP = resp[0].with_CP
        item.distance = resp[0].distance
        item.vendor_name = resp[0].vendor_name
        item.concrete_grade_name = resp[0].concrete_grade_name
        item.cement_grade_name = resp[0].cement_grade_name
        item.cement_brand_name = resp[0].cement_brand_name
        item.sand_source_name = resp[0].sand_source_name
        item.aggregate1_sub_category_name = resp[0].aggregate1_sub_category_name
        item.aggregate2_sub_category_name = resp[0].aggregate2_sub_category_name
        item.aggregate_source_name = resp[0].aggregate_source_name
        item.fly_ash_source_name = resp[0].fly_ash_source_name
        item.admix_category_name = resp[0].admix_category_name
        item.admix_brand_name = resp[0].admix_brand_name
        item.ad_mixture1_brand_id = resp[0].ad_mixture1_brand_id
        item.ad_mixture1_category_id = resp[0].ad_mixture1_category_id
        item.ad_mixture2_brand_id = resp[0].ad_mixture2_brand_id
        item.ad_mixture2_category_id = resp[0].ad_mixture2_category_id

        item.TM_price = resp[0].TM_price
        item.TM_price_for_unit = resp[0].TM_price_for_unit
        item.CP_price = resp[0].CP_price
        item.unit_price = resp[0].unit_price
        item.total_CP_price = resp[0].CP_price * item.quantity

        item.TM_price = item.TM_price.toFixed(2)
        item.TM_price = parseFloat(item.TM_price)

        item.CP_price = item.CP_price.toFixed(2)
        item.CP_price = parseFloat(item.CP_price)

        item.total_CP_price = resp[0].CP_price * item.quantity
        item.total_CP_price = item.total_CP_price.toFixed(2)
        item.total_CP_price = parseFloat(item.total_CP_price)

        item.unit_price = item.unit_price.toFixed(2)
        item.unit_price = parseFloat(item.unit_price)
        console.log("item.unit_price", item.unit_price)
        item.selling_price = resp[0].selling_price
        item.selling_price = item.selling_price.toFixed(2)
        item.selling_price = parseFloat(item.selling_price)
        console.log("item.selling_price", item.selling_price)
        item.margin_price = resp[0].margin_price

        // item.unit_price_With_TM_CP =
        //   item.unit_price + item.TM_price_for_unit + item.CP_price

        console.log("item.TM_price", item.TM_price)
        console.log("item.CP_price", item.CP_price)
        item.selling_price_With_TM_CP =
          item.selling_price + item.TM_price + item.total_CP_price

        console.log(
          "item.selling_price_With_TM_CP",
          item.selling_price_With_TM_CP
        )

        item.selling_price_With_TM_CP = item.selling_price_With_TM_CP.toFixed(2)
        item.selling_price_With_TM_CP = parseFloat(
          item.selling_price_With_TM_CP
        )

        /**********Check Margin PRICE */
        let margin_rate_for_selling: any = await this.commonService.getMarginRate(
          item.selling_price_With_TM_CP,
          true
        )

        margin_rate_for_selling = margin_rate_for_selling.toFixed(2)
        margin_rate_for_selling = parseFloat(margin_rate_for_selling)

        item.margin_price_for_selling =
          (item.selling_price_With_TM_CP * margin_rate_for_selling) / 100

        item.margin_price_for_selling = item.margin_price_for_selling.toFixed(2)
        item.margin_price_for_selling = parseFloat(
          item.margin_price_for_selling
        )

        let margin_rate_for_unit: any = await this.commonService.getMarginRate(
          item.unit_price,
          true
        )

        console.log("item.unit_price", item.unit_price)
        console.log("item.margin_rate_for_unit", margin_rate_for_unit)

        margin_rate_for_unit = margin_rate_for_unit.toFixed(2)
        margin_rate_for_unit = parseFloat(margin_rate_for_unit)

        item.margin_price_for_unit =
          (item.unit_price * margin_rate_for_unit) / 100

        item.margin_price_for_unit = item.margin_price_for_unit.toFixed(2)
        item.margin_price_for_unit = parseFloat(item.margin_price_for_unit)

        item.unit_price_With_Margin =
          item.unit_price + item.margin_price_for_unit

        console.log(
          "Before =====  item.unit_price_With_Margin",
          item.unit_price_With_Margin
        )

        // item.final_price1 = await this.commonService.transcationFeesCalc(
        //   item.unit_price_With_Margin
        // )

        // console.log(
        //   "After =====  item.unit_price_With_Margin",
        //   item.final_price1
        // )

        // item.unit_price_With_Margin = item.final_price1

        item.selling_price_With_Margin =
          item.selling_price_With_TM_CP + item.margin_price_for_selling

        console.log(
          "Before =====  item.selling_price_With_Margin",
          item.selling_price_With_Margin
        )

        item.final_price = await this.commonService.transcationFeesCalc(
          item.selling_price_With_Margin
        )

        console.log(
          "After =====  item.selling_price_With_Margin",
          item.final_price
        )

        item.selling_price_With_Margin = item.final_price.toFixed(2)
        item.selling_price_With_Margin = parseFloat(
          item.selling_price_With_Margin
        )

        console.log("selling_price_With_Margin", item.selling_price_With_Margin)

        item.unit_price_With_Margin =
          item.selling_price_With_Margin / item.quantity

        item.unit_price_With_Margin = item.unit_price_With_Margin.toFixed(2)
        item.unit_price_With_Margin = parseFloat(item.unit_price_With_Margin)

        console.log("item", item)
        console.log("item.quantiy", item.quantity)
        console.log("unit_price_With_Margin", item.unit_price_With_Margin)

        item.selling_price_With_Margin =
          item.unit_price_With_Margin * item.quantity

        item.selling_price_With_Margin = item.selling_price_With_Margin.toFixed(
          2
        )
        item.selling_price_With_Margin = parseFloat(
          item.selling_price_With_Margin
        )

        item.final_price = item.final_price.toFixed(2)
        item.final_price = parseFloat(item.final_price)

        item.subTotalAmount = item.final_price

        item.subTotalAmount = item.subTotalAmount.toFixed(2)
        item.subTotalAmount = parseFloat(item.subTotalAmount)

        // Need to change in future, Add GST .

        cartData.coupon_amount = 0

        // console.log(
        //   "couponApcartData.selling_price_With_Margin",
        //   cartData.selling_price_With_Margin
        // )
        // console.log(
        //   "ccartData.couponInfo.min_order",
        //   cartData.couponInfo.min_order
        // )
        console.log("cartData.couponInfo", cartData.couponInfo)
        console.log("item.subTotal", item.subTotalAmount)
        // if (
        //   // !couponApplied &&
        //   !isNullOrUndefined(cartData.couponInfo) &&
        //   item.subTotalAmount > cartData.couponInfo.min_order
        // ) {
        //   console.log("hereeeeeeeddddddddddddddddddddddde")
        //   console.log("subTotalAmount", item.subTotalAmount)
        //   if (cartData.couponInfo.discount_type === discountTypes.fix) {
        //     cartData.coupon_amount = Math.min(
        //       item.subTotalAmount,
        //       cartData.couponInfo.discount_value,
        //       cartData.couponInfo.max_discount
        //     )
        //     cartData.coupon_per = cartData.couponInfo.discount_value * 100/item.subTotalAmount
        //     cartData.coupon_per = cartData.coupon_per.toFixed(2)
        //     cartData.coupon_per = parseFloat(cartData.coupon_per)
        //     item.subTotalAmount -= cartData.coupon_amount
        //   } else {
        //     cartData.coupon_amount = Math.min(
        //       item.subTotalAmount,
        //       cartData.couponInfo.max_discount,
        //       (item.subTotalAmount * cartData.couponInfo.discount_value) / 100
        //     )
        //     cartData.coupon_per = cartData.couponInfo.discount_value
        //     item.subTotalAmount -= cartData.coupon_amount
        //   }

        //   cartData.coupon_amount = cartData.coupon_amount.toFixed(2)
        //   cartData.coupon_amount = parseFloat(cartData.coupon_amount)

        //   //  cartData.coupon_amount = item.coupon_amount
        // }

        // cartData.coupon_amount = cartData.coupon_amount.toFixed(2)
        // cartData.coupon_amount = parseFloat(cartData.coupon_amount)

        // const gst_slab = await GstSlabModel.findOne()
        // console.log("gst", gst_slab)
        // if (!isNullOrUndefined(gst_slab)) {
        //   let cgst_rate = gst_slab.cgst_rate
        //   let sgst_rate = gst_slab.sgst_rate
        //   let igst_rate = gst_slab.igst_rate

        //   let cgst_price: any = (item.subTotalAmount * cgst_rate) / 100
        //   cgst_price = cgst_price.toFixed(2)
        //   cgst_price = parseFloat(cgst_price)

        //   let sgst_price: any = (item.subTotalAmount * sgst_rate) / 100
        //   sgst_price = sgst_price.toFixed(2)
        //   sgst_price = parseFloat(sgst_price)

        //   let igst_price: any = (item.subTotalAmount * igst_rate) / 100
        //   igst_price = igst_price.toFixed(2)
        //   igst_price = parseFloat(igst_price)

        //   let gst_price = cgst_price + sgst_price
        //   item.gst_price = gst_price
        //   console.log("gst_price", gst_price)

        //   if (!isNullOrUndefined(cartData.billing_address_id)) {
        //     console.log("heree")
        //     const billingAddressInfo = await BuyerBillingAddressModel.findOne({
        //       _id: new ObjectId(cartData.billing_address_id),
        //     })
        //     cartData.billingAddressInfo = await this.getBillingAddressInfo(
        //       cartData.billing_address_id
        //     )

        //     console.log(
        //       "cartData.billingAddressInfo",
        //       cartData.billingAddressInfo
        //     )
        //     if (!isNullOrUndefined(billingAddressInfo)) {
        //       if (cartData.billingAddressInfo.state_name === "Gujarat") {
        //         item.cgst_price = cgst_price
        //         item.sgst_price = sgst_price
        //         item.igst_price = 0
        //         item.gst_price = item.cgst_price + item.sgst_price
        //       } else {
        //         item.cgst_price = 0
        //         item.sgst_price = 0
        //         item.igst_price = igst_price
        //         item.gst_price = item.igst_price
        //       }
        //     }
        //   } else {
        //     item.cgst_price = cgst_price
        //     item.sgst_price = sgst_price
        //     item.igst_price = 0
        //     item.gst_price = item.cgst_price + item.sgst_price
        //   }
        //   console.log("item.gst_price", item.gst_price)
        //   item.gst_price = item.gst_price.toFixed(2)
        //   item.gst_price = parseFloat(item.gst_price)

        //   item.total_amount = item.subTotalAmount + item.gst_price

        //   item.total_amount = item.total_amount.toFixed(2)
        //   item.total_amount = parseFloat(item.total_amount)

        //   console.log("item.total_amount", item.total_amount)
        //   console.log("item.subTotalAmount", item.subTotalAmount)
        //   console.log("item.gst_price", item.gst_price)
        // }

        //delete item.custom_mix
        cartData.selling_price += item.selling_price

        cartData.selling_price = cartData.selling_price.toFixed(2)
        cartData.selling_price = parseFloat(cartData.selling_price)

        //cartData.margin_price += item.margin_price
        cartData.TM_price += item.TM_price
        cartData.TM_price = cartData.TM_price.toFixed(2)
        cartData.TM_price = parseFloat(cartData.TM_price)

        cartData.CP_price += item.CP_price
        cartData.CP_price = cartData.CP_price.toFixed(2)
        cartData.CP_price = parseFloat(cartData.CP_price)

        cartData.total_CP_price += item.total_CP_price
        cartData.total_CP_price = cartData.total_CP_price.toFixed(2)
        cartData.total_CP_price = parseFloat(cartData.total_CP_price)

        cartData.selling_price_With_Margin += item.selling_price_With_Margin

        if (
          // !couponApplied &&
          !isNullOrUndefined(cartData.couponInfo) &&
          cartData.selling_price_With_Margin > cartData.couponInfo.min_order
        ) {
          console.log("hereeeeeeeddddddddddddddddddddddde")
          console.log("subTotalAmount", item.subTotalAmount)
          if (cartData.couponInfo.discount_type === discountTypes.fix) {
            cartData.coupon_amount = Math.min(
              cartData.selling_price_With_Margin,
              cartData.couponInfo.discount_value,
              cartData.couponInfo.max_discount
            )
            cartData.coupon_per =
              (cartData.couponInfo.discount_value * 100) /
              cartData.selling_price_With_Margin
            cartData.coupon_per = cartData.coupon_per.toFixed(2)
            cartData.coupon_per = parseFloat(cartData.coupon_per)
            //  item.subTotalAmount -= cartData.coupon_amount
          } else {
            cartData.coupon_amount = Math.min(
              cartData.selling_price_With_Margin,
              cartData.couponInfo.max_discount,
              (cartData.selling_price_With_Margin *
                cartData.couponInfo.discount_value) /
                100
            )
            cartData.coupon_per = cartData.couponInfo.discount_value
            // item.subTotalAmount -= cartData.coupon_amount
          }

          cartData.coupon_amount = cartData.coupon_amount.toFixed(2)
          cartData.coupon_amount = parseFloat(cartData.coupon_amount)

          //  cartData.coupon_amount = item.coupon_amount
        }

        cartData.selling_price_With_Margin = cartData.selling_price_With_Margin.toFixed(
          2
        )
        cartData.selling_price_With_Margin = parseFloat(
          cartData.selling_price_With_Margin
        )
        console.log(
          "cartData.selling_price_With_Margin",
          cartData.selling_price_With_Margin
        )

        cartData.amount =
          cartData.selling_price_With_Margin - cartData.coupon_amount
        cartData.amount = cartData.amount.toFixed(2)
        cartData.amount = parseFloat(cartData.amount)

        const gst_slab = await GstSlabModel.findOne()
        console.log("gst", gst_slab)
        if (!isNullOrUndefined(gst_slab)) {
          let cgst_rate = gst_slab.cgst_rate
          let sgst_rate = gst_slab.sgst_rate
          let igst_rate = gst_slab.igst_rate

          let cgst_price: any = (cartData.amount * cgst_rate) / 100
          cgst_price = cgst_price.toFixed(2)
          cgst_price = parseFloat(cgst_price)

          let sgst_price: any = (cartData.amount * sgst_rate) / 100
          sgst_price = sgst_price.toFixed(2)
          sgst_price = parseFloat(sgst_price)

          let igst_price: any = (cartData.amount * igst_rate) / 100
          igst_price = igst_price.toFixed(2)
          igst_price = parseFloat(igst_price)

          let gst_price = cgst_price + sgst_price
          cartData.gst_price = gst_price
          console.log("gst_price", gst_price)

          if (!isNullOrUndefined(cartData.billing_address_id)) {
            console.log("heree")
            const billingAddressInfo = await BuyerBillingAddressModel.findOne({
              _id: new ObjectId(cartData.billing_address_id),
            })
            cartData.billingAddressInfo = await this.getBillingAddressInfo(
              cartData.billing_address_id
            )

            console.log(
              "cartData.billingAddressInfo",
              cartData.billingAddressInfo
            )
            if (!isNullOrUndefined(billingAddressInfo)) {
              if (cartData.billingAddressInfo.state_name === "Gujarat") {
                cartData.cgst_price = cgst_price
                cartData.sgst_price = sgst_price
                cartData.igst_price = 0
                cartData.gst_price = cartData.cgst_price + cartData.sgst_price
              } else {
                cartData.cgst_price = 0
                cartData.sgst_price = 0
                cartData.igst_price = igst_price
                cartData.gst_price = cartData.igst_price
              }
            }
          } else {
            cartData.cgst_price = cgst_price
            cartData.sgst_price = sgst_price
            cartData.igst_price = 0
            cartData.gst_price = cartData.cgst_price + cartData.sgst_price
          }
          console.log("cartData.gst_price", cartData.gst_price)

          cartData.cgst_price = cartData.cgst_price.toFixed(2)
          cartData.cgst_price = parseFloat(cartData.cgst_price)

          cartData.sgst_price = cartData.sgst_price.toFixed(2)
          cartData.sgst_price = parseFloat(cartData.sgst_price)

          cartData.igst_price = cartData.igst_price.toFixed(2)
          cartData.igst_price = parseFloat(cartData.igst_price)

          cartData.gst_price = cartData.gst_price.toFixed(2)
          cartData.gst_price = parseFloat(cartData.gst_price)

          // item.total_amount = item.subTotalAmount + item.gst_price

          // item.total_amount = item.total_amount.toFixed(2)
          // item.total_amount = parseFloat(item.total_amount)

          // console.log("item.total_amount", item.total_amount)
          // console.log("item.subTotalAmount", item.subTotalAmount)
          // console.log("item.gst_price", item.gst_price)
        }

        // cartData.cgst_price += item.cgst_price
        // cartData.cgst_price = cartData.cgst_price.toFixed(2)
        // cartData.cgst_price = parseFloat(cartData.cgst_price)

        // cartData.sgst_price += item.sgst_price
        // cartData.sgst_price = cartData.sgst_price.toFixed(2)
        // cartData.sgst_price = parseFloat(cartData.sgst_price)

        // cartData.igst_price += item.igst_price
        // cartData.igst_price = cartData.igst_price.toFixed(2)
        // cartData.igst_price = parseFloat(cartData.igst_price)

        // cartData.gst_price += item.gst_price
        // cartData.gst_price = cartData.gst_price.toFixed(2)
        // cartData.gst_price = parseFloat(cartData.gst_price)

        cartData.total_amount = cartData.amount + cartData.gst_price

        cartData.total_amount = cartData.total_amount.toFixed(2)
        cartData.total_amount = parseFloat(cartData.total_amount)

        console.log("cartData.total_amount", cartData.total_amount)

        // console.log(
        //   "couponApcartData.selling_price_With_Margin",
        //   cartData.selling_price_With_Margin
        // )
        // console.log(
        //   "ccartData.couponInfo.min_order",
        //   cartData.couponInfo.min_order
        // )

        // if (
        //   // !couponApplied &&
        //   !isNullOrUndefined(cartData.couponInfo) &&
        //   cartData.selling_price_With_Margin > cartData.couponInfo.min_order
        // ) {
        //   console.log("hereeeeeeee")
        //   console.log("subTotalAmount", item.subTotalAmount)
        //   if (cartData.couponInfo.discount_type === discountTypes.fix) {
        //     cartData.coupon_amount = Math.min(
        //       cartData.selling_price_With_Margin,
        //       cartData.couponInfo.discount_value,
        //       cartData.couponInfo.max_discount
        //     )

        //     cartData.selling_price_With_Margin -= cartData.coupon_amount
        //   } else {
        //     console.log("Percentage")
        //     console.log(
        //       "cartData.selling_price_With_Margin",
        //       cartData.selling_price_With_Margin
        //     )
        //     console.log(
        //       "cartData.couponInfo.max_discount",
        //       cartData.couponInfo.max_discount
        //     )
        //     console.log(
        //       "cartData.couponInfo.discount_value",
        //       cartData.couponInfo.discount_value
        //     )
        //     cartData.coupon_amount = Math.min(
        //       cartData.selling_price_With_Margin,
        //       cartData.couponInfo.max_discount,
        //       (cartData.selling_price_With_Margin *
        //         cartData.couponInfo.discount_value) /
        //         100
        //     )
        //     console.log("cartData.coupon_amount", cartData.coupon_amount)
        //   }

        //   cartData.coupon_amount = cartData.coupon_amount.toFixed(2)
        //   cartData.coupon_amount = parseFloat(cartData.coupon_amount)
        //   //cartData.coupon_amount = item.coupon_amount
        // }

        // cartData.coupon_amount = cartData.coupon_amount.toFixed(2)
        // cartData.coupon_amount = parseFloat(cartData.coupon_amount)

        // cartData.total_amount = cartData.total_amount - cartData.coupon_amount

        // cartData.total_amount = cartData.total_amount.toFixed(2)
        // cartData.total_amount = parseFloat(cartData.total_amount)
        // cartData.margin_price += item.margin_price
        // cartData.total_amount += item.total_amount
        // cartData.sub_total_amount += item.subTotalAmount
        // } catch (err) {
        //   item.error = true
        //   item.errorMessage = err.message
        //   console.log(`[ERROR] unable to get dvl info. ${err.message}`)
        //   return item
        // }

        console.log("cartData", cartData)
      })
    )
    return cartData
  }

  async getProductInRange(
    product_id: string | ObjectID,
    deliveryLocation: ILocation
  ) {
    console.log(product_id, "pppppppppppppp")
    console.log(deliveryLocation, "ddddddddddddddd")
    const aggregateArr = [
      {
        $geoNear: {
          near: deliveryLocation,
          distanceField: "calculated_distance",
          distanceMultiplier: 0.001,
          maxDistance: deliveryRangeForVehicle.max * 1000, // km in meter
          spherical: true,
          query: {
            _id: new ObjectID(product_id),
            // is_deleted: false,
            is_available: true,
            verified_by_admin: true,
            quantity: { $gt: 0 },
          },
        },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$supplier" },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "productSubCategory.gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $lookup: {
          from: "margin_rate",
          localField: "productSubCategory.margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: { path: "$gst_slab" },
      },
      {
        $unwind: { path: "$margin_rate" },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $project: {
          name: 1,
          category_id: 1,
          sub_category_id: 1,
          quantity: 1,
          minimum_order: 1,
          unit_price: 1,
          self_logistics: 1,
          user_id: 1,
          pickup_location: 1,
          calculated_distance: 1,
          rating_count: 1,
          rating_sum: 1,
          rating: 1,
          pickup_address_id: 1,
          "supplier._id": 1,
          "supplier.full_name": 1,
          "productCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "productSubCategory.gst_slab_id": 1,
          "productSubCategory.margin_rate_id": 1,
          "productSubCategory.min_quantity": 1,
          "productSubCategory.selling_unit": 1,
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
          "address.state_id": "$state._id",
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
        },
      },
    ]

    const products = await ProductModel.aggregate(aggregateArr)
    console.log(`products:`, products)
    if (products.length < 1) {
      return Promise.reject(
        new InvalidInput(`No such product was  found `, 400)
      )
    }
    return products[0]
  }
  private async getVehiclesInRange(
    pickupLocation: ILocation,
    pickupToDvldistance: number
  ): Promise<
    {
      vehicle_category_id: string | ObjectID
      vehicle_sub_category_id: string | ObjectID
      per_metric_ton_per_km_rate: number
      min_trip_price: number
      delivery_range: number
      pickup_location: ILocation
      calculated_distance: number
      vehicleSubCategory: {
        _id: string | ObjectID
        sub_category_name: string
        min_load_capacity: number
        max_load_capacity: number
        weight_unit: string
        weight_unit_code: string
      }
    }[]
  > {
    console.log("pickupToDvldistance", pickupToDvldistance)
    const aggregateArr: any[] = [
      {
        $geoNear: {
          near: pickupLocation,
          distanceField: "calculated_distance",
          distanceMultiplier: 0.001,
          maxDistance: maxVehicleToPickupDistance * 1000, //   <distance in meters>
          spherical: true,
          query: {
            is_active: true,
            delivery_range: { $gte: pickupToDvldistance },
          },
        },
      },
      // { $limit: 10 },
      {
        $lookup: {
          from: "vehicle_sub_category",
          localField: "vehicle_sub_category_id",
          foreignField: "_id",
          as: "vehicleSubCategory",
        },
      },
      {
        $unwind: "$vehicleSubCategory",
      },
      {
        $project: {
          vehicle_category_id: 1,
          vehicle_sub_category_id: 1,
          // per_metric_ton_per_km_rate: 1,
          min_trip_price: 1,
          delivery_range: 1,
          pickup_location: 1,
          calculated_distance: 1,
          state_id: 1,
          "vehicleSubCategory._id": 1,
          "vehicleSubCategory.sub_category_name": 1,
          "vehicleSubCategory.min_load_capacity": 1,
          "vehicleSubCategory.max_load_capacity": 1,
          "vehicleSubCategory.weight_unit": 1,
          "vehicleSubCategory.weight_unit_code": 1,
        },
      },
    ]

    const vehicles = await VehicleModel.aggregate(aggregateArr)

    if (vehicles.length <= 0) {
      return Promise.reject(
        new InvalidInput(
          `Oops! No vehicle available for delivery of your order`,
          400
        )
      )
    }

    for (let i = 0; i < vehicles.length; i++) {
      let rateDoc = await VehicleRateModel.findOne({
        state_id: new ObjectId(vehicles[i].state_id),
      })
      if (!isNullOrUndefined(rateDoc)) {
        vehicles[i].per_metric_ton_per_km_rate =
          rateDoc.per_metric_ton_per_km_rate
      } else {
        return Promise.reject(
          new InvalidInput(
            `Rate for this product is unvailable for requested location.`,
            400
          )
        )
      }
    }
    return Promise.resolve(vehicles)
  }

  async addSiteToCart(user_id: string, site_id: string) {
    const siteInfo = await SiteModel.findOne({ _id: site_id, user_id })
    if (isNullOrUndefined(siteInfo)) {
      return Promise.reject(new InvalidInput(`Invalid site info`, 400))
    }
    const cartInfo = await CartModel.findOne({ user_id })
    if (isNullOrUndefined(cartInfo)) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }
    return CartModel.updateOne(
      { user_id },
      { $set: { site_id, delivery_location: siteInfo.location } }
    )
  }

  async addBillingAddressToCart(user_id: string, billing_address_id: string) {
    const billingAddressInfo = await BuyerBillingAddressModel.findOne({
      _id: billing_address_id,
      "user.user_id": user_id,
    })
    if (isNullOrUndefined(billingAddressInfo)) {
      return Promise.reject(
        new InvalidInput(`Invalid billing address info`, 400)
      )
    }
    const cartInfo = await CartModel.findOne({ user_id })
    if (isNullOrUndefined(cartInfo)) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }
    return CartModel.updateOne({ user_id }, { $set: { billing_address_id } })
  }

  async getSiteInfo(site_id: string | ObjectID): Promise<ISiteInfo> {
    const sites = await SiteModel.aggregate([
      { $match: { _id: new ObjectID(site_id) } },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $project: {
          location: 1,
          email_verified: 1,
          mobile_number_verified: 1,
          alt_mobile_number_verified: 1,
          whatsapp_number_verified: 1,
          _id: 1,
          site_name: 1,
          company_name: 1,
          address_line1: 1,
          address_line2: 1,
          country_id: 1,
          state_id: 1,
          city_id: 1,
          sub_city_id: 1,
          person_name: 1,
          title: 1,
          email: 1,
          mobile_number: 1,
          landline_number: 1,
          profile_pic: 1,
          alt_mobile_number: 1,
          created_at: 1,
          pincode: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
        },
      },
    ])
    if (sites.length === 0) {
      return Promise.reject(
        new InvalidInput(`Please enter a valid delivery address`, 400)
      )
    }
    return sites[0]
  }

  async getBillingAddressInfo(
    billing_address_id: string | ObjectID
  ): Promise<ISiteInfo> {
    const address = await BuyerBillingAddressModel.aggregate([
      { $match: { _id: new ObjectID(billing_address_id) } },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $project: {
          user: 1,
          company_name: 1,
          full_name: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          gst_number: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
        },
      },
    ])
    if (address.length === 0) {
      return Promise.reject(
        new InvalidInput(`Please enter a valid delivery address`, 400)
      )
    }
    return address[0]
  }

  async getCouponList(searchParam: IGetCouponDetails) {
    const query: { [k: string]: any } = {
      start_date: { $lte: new Date() },
      end_date: { $gte: new Date() },
      is_deleted: false,
      is_active: true,
      remaining_usage: { $gte: 1 },
      // buyer_ids: { $exists: false },
    }
    if (!isNullOrUndefined(searchParam.is_deleted)) {
      query.is_deleted = searchParam.is_deleted
    }
    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }
    if (!isNullOrUndefined(searchParam.code)) {
      query.code = searchParam.code
    }
    if (!isNullOrUndefined(searchParam.discount_type)) {
      query.discount_type = searchParam.discount_type
    }
    if (!isNullOrUndefined(searchParam.buyer_id)) {
      query.$or = []
      query.$or.push({
        buyer_ids: new ObjectId(searchParam.buyer_id),
      })

      query.$or.push({
        buyer_ids: { $exists: false },
      })
    }
    if (!isNullOrUndefined(searchParam.supplier_id)) {
      query.supplier_id = new ObjectId(searchParam.supplier_id)
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $project: {
          discount_type: true,
          discount_value: true,
          min_order: true,
          max_discount: true,
          start_date: true,
          end_date: true,
          code: true,
          created_at: true,
          info: true,
          tnc: true,
          supplier_id: true,
          buyer_id: true,
          is_deleted: true,
          is_active: true,
          max_usage: true,
          unique_use: true,
        },
      },
    ]

    const couponData = await CouponModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      couponData.reverse()
    }
    return Promise.resolve(couponData)
  }

  async applyCoupon(couponCode: string, user_id?: string, cart_id?: string) {
    let cartData = await this.getCartItem(cart_id, user_id)
    const cartQuery: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      cartQuery.user_id = user_id
    }
    if (!isNullOrUndefined(cart_id)) {
      cartQuery._id = cart_id
    }
    const cartInfo = await CartModel.findOne(cartQuery)
    if (cartInfo === null) {
      return Promise.reject(
        new InvalidInput(`Your cart is currently empty.`, 400)
      )
    }
    if (couponCode === "") {
      return CartModel.updateOne(cartQuery, { $unset: { coupon_code_id: "" } })
    }
    const couponInfo = await CouponModel.findOne({
      code: couponCode,
      start_date: { $lte: new Date() },
      end_date: { $gte: new Date() },
      is_deleted: false,
      is_active: true,
    })
    if (couponInfo === null) {
      return Promise.reject(new InvalidInput("No such coupon exists", 400))
    }

    console.log("cartInfo", cartInfo)
    console.log("cartData", cartData)

    if (cartData.selling_price_With_Margin < couponInfo.min_order) {
      return Promise.reject(
        new InvalidInput(
          "This coupon cannot be applied as the amount in your cart is insufficient.",
          400
        )
      )
    }
    if (
      !isNullOrUndefined(couponInfo.max_discount) &&
      couponInfo.max_discount > 0 &&
      !isNullOrUndefined(couponInfo.remaining_usage) &&
      couponInfo.remaining_usage < 1
    ) {
      return Promise.reject(
        new InvalidInput(
          "You cannot use this coupon as you've already used it multiple times",
          400
        )
      )
    }
    return CartModel.updateOne(cartQuery, {
      $set: { coupon_code_id: couponInfo._id },
    })
  }
}
