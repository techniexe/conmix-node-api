import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { BuyerTypes } from "../buyer.types"
import { BuyerProductRepository } from "./buyer.product.repository"
import { IGetProduct } from "./buyer.product.req.schema"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"

@injectable()
@controller("/product", verifyCustomToken("buyer"))
export class BuyerProductController {
  constructor(
    @inject(BuyerTypes.BuyerProductRepository)
    private buyerProduct: BuyerProductRepository
  ) {}
  @httpGet("/")
  async getProduct(req: IGetProduct, res: Response, next: NextFunction) {
    const data = await this.buyerProduct.getProduct(req.user.uid, req.query)
    res.json({ data })
  }

  // @httpGet("/:productId", validate(GetProductDetailsSchema))
  // async getProductDetails(req: IGetProductDeatilsRequest, res: Response) {
  //   console.log(req.query)
  //   const data = await this.buyerProduct.getProductDetails(
  //     req.params.productId,
  //     req.query,
  //     req.user.uid
  //   )
  //   res.json({ data })
  // }
}
