import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export interface IGetProductDetails {
  site_id?: string
  sub_city?: string
  long?: number
  lat?: number
}

export interface IProductQuery {
  search?: string
  before?: string
  after?: string
  categories?: [string]
  sub_categories?: [string]
  order_by?: string
}

export interface IGetProduct extends IAuthenticatedRequest {
  query: IProductQuery
}

export const getProduct: IRequestSchema = {
  query: Joi.object().keys({
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    sub_categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    order_by: Joi.string(), //comma separated fields. ex- name:desc,category_id,sub_category_id
  }),
}

// export interface IGetProductDetails {
//   site_id?: string
//   long?: number
//   lat?: number
// }

// export interface IGetProductDeatilsRequest extends IAuthenticatedRequest {
//   query: IGetProductDetails
//   params: {
//     productId: string
//   }
// }

// export const GetProductDetailsSchema: IRequestSchema = {
//   query: Joi.object().keys({
//     site_id: Joi.string().regex(objectIdRegex),
//     long: Joi.number().min(-180).max(180),
//     lat: Joi.number().min(-90).max(90),
//   }),
//   params: Joi.object().keys({
//     productId: Joi.string().regex(objectIdRegex),
//   }),
// }
