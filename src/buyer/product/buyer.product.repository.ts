import { injectable } from "inversify"
import { ProductModel } from "../../model/product.model"
import { IProductQuery } from "./buyer.product.req.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ObjectId } from "bson"
// import { InvalidInput } from "../../utilities/customError"
// import { SiteModel } from "../../model/site.model"
// import {
//   deliveryRangeForVehicle,
//   maxVehicleToPickupDistance,
//   GstTypes,
// } from "../../utilities/config"
// import { getDistance, distanceFactor } from "../../utilities/distance.utilities"
// import { VehicleModel } from "../../model/vehicle.model"
// import { getAvgTripCosts, getMarginRate } from "../../utilities/rate.utility"

@injectable()
export class BuyerProductRepository {
  async getProduct(user_id: string, reqQuery: IProductQuery) {
    const arrSortFld = ["name", "category_id", "sub_category_id"]
    const query: { [k: string]: any } = {
      // is_deleted: { $in: [null, false] },
      is_available: true,
      verified_by_admin: true,
    }

    const sort: { [k: string]: any } = {}
    const {
      search,
      before,
      after,
      categories,
      sub_categories,
      order_by,
    } = reqQuery
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(categories)) {
      query.category_id = { $in: categories.map((oid) => new ObjectId(oid)) }
    }
    if (!isNullOrUndefined(sub_categories)) {
      query.sub_category_id = {
        $in: sub_categories.map((oid) => new ObjectId(oid)),
      }
    }
    if (!isNullOrUndefined(order_by)) {
      order_by.split(",").reduce((obj, str) => {
        const arrSplt = str.split(":")
        if (arrSortFld.indexOf(arrSplt[0]) < 0) {
          return obj
        }
        return Object.assign(obj, {
          [arrSplt[0]]: arrSplt[1] === "desc" ? -1 : 1,
        })
      }, sort)
    }

    const aggregateArr = [
      { $match: query },
      { $limit: 10 },
      // { $group: {
      //   _id: "$sub_category_id",
      //   unit_price_min: { $min: "$unit_price" },
      //   unit_price_max: { $max: "$unit_price" },
      //   stock: { $sum: "$quantity" },
      // }
      //  },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "product_sub_category",
        },
      },
      {
        $unwind: {
          path: "$product_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          name: 1,
          product_sub_category: 1,
        },
      },
    ]

    // aggregateArr.push({
    //   $group: {
    //     _id: "$sub_category_id",
    //     unit_price_min: { $min: "$unit_price" },
    //     unit_price_max: { $max: "$unit_price" },
    //     stock: { $sum: "$quantity" },
    //   },
    // })

    // if (Object.keys(sort).length > 0) {
    //   aggregateArr.push({ $sort: sort })
    // }

    const ProductData = await ProductModel.aggregate(aggregateArr)
    return Promise.resolve(ProductData)

    // console.log(query)
    // const aggregatePipe = []
    // aggregatePipe.push({ $match: query })
    // if (Object.keys(sort).length > 0) {
    //   aggregatePipe.push({ $sort: sort })
    // }
    // aggregatePipe.push({
    //   $group: {
    //     _id: "$sub_category_id",
    //     unit_price_min: { $min: "$unit_price" },
    //     unit_price_max: { $max: "$unit_price" },
    //     stock: { $sum: "$quantity" },
    //   },
    // })
    // aggregatePipe.push({ $limit: 40 })
    // return await ProductModel.aggregate(aggregatePipe)
    //   // .lookup({
    //   //   from: "product_sub_category",
    //   //   localField: "_id",
    //   //   foreignField: "_id",
    //   //   as: "product_sub_category",
    //   // })
    //   // .unwind({
    //   //   path: "$product_sub_category",
    //   //   preserveNullAndEmptyArrays: true,
    //   // })
    //   // .project({
    //   //   product_sub_category: 1,
    //   //   _id: 1,
    //   //   name: 1
    //   // })
  }

  // async getProductDetails(
  //   product_id: string,
  //   query: IGetProductDetails,
  //   user_id: string
  // ) {
  //   const requesterUid = new ObjectId(user_id)

  //   let { sub_city } = query
  //   let dvlLocation = { type: "Point", coordinates: [0, 0] }
  //   if (
  //     isNullOrUndefined(query.site_id) &&
  //     isNullOrUndefined(sub_city) &&
  //     (isNullOrUndefined(query.long) || isNullOrUndefined(query.long))
  //   ) {
  //     return Promise.reject(
  //       new InvalidInput(
  //         `either Site id, sub city or location is required`,
  //         400
  //       )
  //     )
  //   }

  //   console.log("user_id", user_id)
  //   if (!isNullOrUndefined(query.site_id)) {
  //     //getting city id
  //     const siteQuery = { _id: new ObjectId(query.site_id), user_id }

  //     console.log("siteQuery", siteQuery)
  //     const siteData = await SiteModel.findOne(siteQuery)

  //     console.log("siteData", siteData)
  //     if (siteData === null) {
  //       return Promise.reject(
  //         new InvalidInput(`Invalid site reference provided`, 400)
  //       )
  //     }
  //     // sub_city = "" + siteData.sub_city_id
  //     dvlLocation = siteData.location
  //   }

  //   // console.log("sub_city", sub_city)
  //   // if (!isNullOrUndefined(sub_city)) {
  //   //   console.log("Hereeee")
  //   //   const subCityData = await SubCityModel.findById(sub_city)
  //   //   if (subCityData === null) {
  //   //     return Promise.reject(
  //   //       new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400)
  //   //     )
  //   //   }
  //   //   dvlLocation = subCityData.location
  //   // }
  //   if (!isNullOrUndefined(query.long) && !isNullOrUndefined(query.lat)) {
  //     dvlLocation.coordinates = [query.long, query.lat]
  //   }
  //   if (dvlLocation.coordinates[0] === 0) {
  //     return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
  //   }
  //   console.log("dvlLocation: ", dvlLocation)
  //   const aggregateArr: any[] = [
  //     {
  //       $geoNear: {
  //         near: dvlLocation,
  //         distanceField: "calculated_distance",
  //         maxDistance: deliveryRangeForVehicle.max * 1000, // km in meter
  //         spherical: true,
  //         query: {
  //           _id: new ObjectId(product_id),
  //           // is_deleted: false,
  //           is_available: true,
  //           verified_by_admin: true,
  //           quantity: { $gt: 0 },
  //         },
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "supplier",
  //         localField: "user_id",
  //         foreignField: "_id",
  //         as: "supplier",
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "product_category",
  //         localField: "category_id",
  //         foreignField: "_id",
  //         as: "productCategory",
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "product_sub_category",
  //         localField: "sub_category_id",
  //         foreignField: "_id",
  //         as: "productSubCategory",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$supplier" },
  //     },
  //     {
  //       $unwind: { path: "$productCategory" },
  //     },
  //     {
  //       $unwind: { path: "$productSubCategory" },
  //     },
  //     {
  //       $lookup: {
  //         from: "gst_slab",
  //         localField: "productSubCategory.gst_slab_id",
  //         foreignField: "_id",
  //         as: "gst_slab",
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "margin_rate",
  //         localField: "productSubCategory.margin_rate_id",
  //         foreignField: "_id",
  //         as: "margin_rate",
  //       },
  //     },
  //     {
  //       $lookup: {
  //         from: "address",
  //         localField: "pickup_address_id",
  //         foreignField: "_id",
  //         as: "address",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
  //     },
  //     {
  //       $unwind: { path: "$gst_slab" },
  //     },
  //     {
  //       $unwind: { path: "$margin_rate" },
  //     },
  //     {
  //       $lookup: {
  //         from: "source",
  //         localField: "address.source_id",
  //         foreignField: "_id",
  //         as: "source",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$source" },
  //     },
  //     {
  //       $lookup: {
  //         from: "state",
  //         localField: "address.state_id",
  //         foreignField: "_id",
  //         as: "state",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
  //     },
  //     {
  //       $lookup: {
  //         from: "city",
  //         localField: "address.city_id",
  //         foreignField: "_id",
  //         as: "city",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
  //     },
  //     {
  //       $lookup: {
  //         from: "wishlist",
  //         let: {
  //           requesterUid,
  //           product_id: "$_id",
  //         },
  //         pipeline: [
  //           {
  //             $match: {
  //               $expr: {
  //                 $and: [
  //                   {
  //                     $eq: ["$user_id", "$$requesterUid"],
  //                   },
  //                   {
  //                     $eq: ["$product_id", "$$product_id"],
  //                   },
  //                 ],
  //               },
  //             },
  //           },
  //         ],
  //         as: "wishlist",
  //       },
  //     },
  //     {
  //       $project: {
  //         name: 1,
  //         category_id: 1,
  //         sub_category_id: 1,
  //         quantity: 1,
  //         minimum_order: 1,
  //         unit_price: 1,
  //         self_logistics: 1,
  //         user_id: 1,
  //         pickup_location: 1,
  //         calculated_distance: 1,
  //         rating_count: 1,
  //         rating_sum: 1,
  //         "supplier._id": 1,
  //         "supplier.full_name": 1,
  //         "productCategory._id": 1,
  //         "productCategory.category_name": 1,
  //         "productCategory.image_url": 1,
  //         "productCategory.thumbnail_url": 1,
  //         "productSubCategory._id": 1,
  //         "productSubCategory.sub_category_name": 1,
  //         "productSubCategory.image_url": 1,
  //         "productSubCategory.thumbnail_url": 1,
  //         "productSubCategory.gst_slab_id": 1,
  //         "productSubCategory.margin_rate_id": 1,
  //         "productSubCategory.min_quantity": 1,
  //         "productSubCategory.selling_unit": 1,
  //         "gst_slab._id": 1,
  //         "gst_slab.title": 1,
  //         "gst_slab.igst_rate": 1,
  //         "gst_slab.cgst_rate": 1,
  //         "gst_slab.sgst_rate": 1,
  //         "margin_rate._id": 1,
  //         "margin_rate.title": 1,
  //         "margin_rate.slab": 1,
  //         "address.line1": 1,
  //         "address.line2": 1,
  //         "address.state_name": "$state.state_name",
  //         "address.city_name": "$city.city_name",
  //         "address.source_name": "$source.source_name",
  //         is_wishlisted: { $gt: [{ $size: "$wishlist" }, 0] },
  //       },
  //     },
  //   ]

  //   const products = await ProductModel.aggregate(aggregateArr)
  //   console.log(`Found products:`, JSON.stringify(products))
  //   const outProducts: any[] = []
  //   for (let j = 0; j < products.length; j++) {
  //     const nPrdt = products[j]
  //     console.log(`Finding price for product ${nPrdt._id} ${nPrdt.name}`)
  //     if (nPrdt.self_logistics === false) {
  //       nPrdt.distance =
  //         getDistance(
  //           dvlLocation.coordinates,
  //           nPrdt.pickup_location.coordinates
  //         ) * distanceFactor
  //       console.log(`nPrdt.distance:${nPrdt.distance}`)
  //       const vehicles = await VehicleModel.aggregate([
  //         {
  //           $geoNear: {
  //             near: nPrdt.pickup_location,
  //             distanceField: "calculated_distance",
  //             maxDistance: maxVehicleToPickupDistance * 1000, //   <distance in meters>
  //             spherical: true,
  //             query: {
  //               is_active: true,
  //               delivery_range: { $gte: nPrdt.distance },
  //             },
  //           },
  //         },
  //         { $limit: 10 },
  //         {
  //           $lookup: {
  //             from: "vehicle_sub_category",
  //             localField: "vehicle_sub_category_id",
  //             foreignField: "_id",
  //             as: "vehicleSubCategory",
  //           },
  //         },
  //         {
  //           $unwind: "$vehicleSubCategory",
  //         },
  //         {
  //           $project: {
  //             vehicle_category_id: 1,
  //             vehicle_sub_category_id: 1,
  //             per_metric_ton_per_km_rate: 1,
  //             min_trip_price: 1,
  //             delivery_range: 1,
  //             pickup_location: 1,
  //             calculated_distance: 1,
  //             "vehicleSubCategory._id": 1,
  //             "vehicleSubCategory.sub_category_name": 1,
  //             "vehicleSubCategory.min_load_capacity": 1,
  //             "vehicleSubCategory.max_load_capacity": 1,
  //             "vehicleSubCategory.weight_unit": 1,
  //             "vehicleSubCategory.weight_unit_code": 1,
  //           },
  //         },
  //       ])
  //       // console.log(`vehicles: `, vehicles)
  //       const avlVehicles = getAvgTripCosts(nPrdt.distance, vehicles)

  //       nPrdt.vehicles = []
  //       for (let i = 0; i < avlVehicles.length; i++) {
  //         const v = avlVehicles[i]
  //         const minQuantity = v.vehicleSubCategory.min_load_capacity
  //         const baseCost = nPrdt.unit_price * minQuantity
  //         const marginRate = getMarginRate(baseCost, nPrdt.margin_rate.slab)
  //         const marginAmont = (baseCost * marginRate) / 100
  //         const subTotalAmount = baseCost + marginAmont + v.logistics_price
  //         const gstAmount = (subTotalAmount * nPrdt.gst_slab.igst_rate) / 100
  //         const totalAmount = subTotalAmount + gstAmount
  //         v.totalAmount = totalAmount
  //         nPrdt.vehicles.push(v)
  //       }
  //     } else {
  //       const baseCostZ =
  //         (nPrdt.unit_price * (nPrdt.productSubCategory.margin_rate + 100)) /
  //         100
  //       nPrdt.gst_type = GstTypes.INTERSTATE
  //       nPrdt.igst_amount =
  //         (baseCostZ * nPrdt.productSubCategory.igst_rate) / 100
  //       nPrdt.total_amount = baseCostZ + nPrdt.igst_amount
  //     }
  //     outProducts.push(nPrdt)
  //   }
  //   return outProducts
  // }
}
