import { injectable, inject } from "inversify"
import { controller, httpGet, httpPost } from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { BuyerTypes } from "../buyer.types"
import {
  getOrder,
  IGetOrderRequest,
  IGetOrderByIdRequest,
  getOrderById,
  placeOrderSchema,
  IPlaceOrderRequest,
  ICancelOrderItemRequest,
  cancelOrderItemSchema,
  addOrderPartSchema,
  IAddOrderPartRequest,
  checkTMAvailbilitySchema,
  ICheckTMAvailbilityRequest,
} from "./buyer.order.req-schema"
import { OrderRepository } from "./buyer.order.repository"

@injectable()
@controller("/order", verifyCustomToken("buyer"))
export class OrderController {
  constructor(
    @inject(BuyerTypes.OrderRepository) private orderRepo: OrderRepository
  ) {}

  @httpPost("/CheckTMAvailbility", validate(checkTMAvailbilitySchema))
  async CheckTMAvailbility(
    req: ICheckTMAvailbilityRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.orderRepo.CheckTMAvailbility(
      req.user.uid,
      req.body.start_time,
      req.body.end_time,
      req.body.vendor_id
    )
    res.send({ data })
  }

  @httpPost("/")
  @httpPost("/:cartId", validate(placeOrderSchema))
  async placeOrder(req: IPlaceOrderRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    const { cartId } = req.params

    const data = await this.orderRepo.placeOrder(
      uid,
      cartId,
      req.body.gateway_transaction_id
    )
    res.json({ data })
  }

  @httpGet("/", validate(getOrder))
  async getOrder(req: IGetOrderRequest, res: Response, next: NextFunction) {
    const {
      before,
      after,
      order_status,
      payment_status,
      gateway_transaction_id,
      site_id,
      month,
      year,
      date,
      total_amount,
      total_amount_range,
      start_date,
      end_date,
    } = req.query

    const data = await this.orderRepo.getOrder(
      req.user.uid,
      before,
      after,
      order_status,
      payment_status,
      gateway_transaction_id,
      site_id,
      month,
      year,
      date,
      total_amount,
      total_amount_range,
      start_date,
      end_date
    )
    res.json({ data })
  }

  @httpGet("/:orderId", validate(getOrderById))
  async getOrderById(
    req: IGetOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.orderRepo.getOrderById(
        req.user.uid,
        req.params.orderId
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/:order_item_id/cancel/:order_id", validate(cancelOrderItemSchema))
  async cancelOrderItem(
    req: ICancelOrderItemRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.cancelOrderItem(
        req.user.uid,
        req.params.order_item_id,
        req.params.order_id,
        req.body.gateway_transaction_id
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/:order_id/:order_item_id", validate(addOrderPartSchema))
  async addOrderPart(
    req: IAddOrderPartRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.orderRepo.addOrderPart(
      req.user.uid,
      req.params.order_id,
      req.params.order_item_id,
      req.body
    )
    res.sendStatus(201)
  }
}
