import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { OrderModel, OrderStatus } from "../../model/order.model"

import { InvalidInput } from "../../utilities/customError"
import { SiteModel } from "../../model/site.model"
import {
  PaymentStatus,
  OrderItemStatus,
  UserType,
  razor_key,
  razor_secret,
  monthsCode,
} from "../../utilities/config"
import { BuyerTypes } from "../buyer.types"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { CartRepository } from "../cart/cart.repository"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { OrderItemModel } from "../../model/order_item.model"
import { ObjectId } from "bson"
import { OrderTrackModel } from "../../model/order_track.model"
import {
  LogisticsOrderStatus,
  LogisticsOrderModel,
} from "../../model/logistics.order.model"
import {
  SupplierOrderStatus,
  VendorOrderModel,
} from "../../model/vendor.order.model"
import { AddressModel } from "../../model/address.model"
import { CartModel } from "../../model/cart.model"
import { ReviewModel } from "../../model/review.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import {
  ClientNotificationType,
  AdminNotificationType,
  VendorNotificationType,
} from "../../model/notification.model"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { PayMethodModel } from "../../model/pay_method.model"
import { IAddOrderPart } from "./buyer.order.req-schema"
// import { CommonService } from "../../utilities/common.service"
import { TM_unModel } from "../../model/TM_un.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { CPOrderTrackModel } from "../../model/CP_order_track.model"
import { BuyerBillingAddressModel } from "../../model/buyer_billing_address.model"
import { TestService } from "../../utilities/test.service"
import { VendorUserModel } from "../../model/vendor.user.model"
import { MailGunService } from "../../utilities/mailgun.service"
import { client_mail_template } from "../../utilities/client_email_tempalte"
import { admin_mail_template } from "../../utilities/admin_email_template"
import { vendor_mail_template } from "../../utilities/vendor_email_template"


const Razorpay = require("razorpay")

let rzp = new Razorpay({
  key_id: razor_key, // your `KEY_ID`
  key_secret: razor_secret, // your `KEY_SECRET`
})

@injectable()
export class OrderRepository {
  constructor(
    @inject(BuyerTypes.UniqueidService)
    private uniqueIdService: UniqueidService,
    @inject(BuyerTypes.CartRepository) private cartRepo: CartRepository,
    // @inject(BuyerTypes.SnsService) private snsService: SnsService,
    @inject(BuyerTypes.TestService) private testService: TestService,
    @inject(BuyerTypes.NotificationUtility)
    private notUtil: NotificationUtility,
    @inject(BuyerTypes.mailgunService) private mailgunService: MailGunService // @inject(BuyerTypes.CommonService) private commonService: CommonService
  ) {}

  async placeOrder(
    user_id: string,
    cart_id?: string,
    gateway_transaction_id?: string
  ) {
    try {
      let cartData = await this.cartRepo.getCartItem(cart_id, user_id)

      console.log(
        " %%%%%%%%%%%%%%%%%%%%%%%%%%%%% cartData %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
        cartData
      )

      let userData = await BuyerUserModel.findOne({
        _id: new ObjectId(user_id),
      })
      let pay_method_name: any
      if (!isNullOrUndefined(userData)) {
        let payData = await PayMethodModel.findOne({
          _id: new ObjectId(userData.pay_method_id),
        })

        if (!isNullOrUndefined(payData)) {
          pay_method_name = payData.name
        }
      }

      let orderData: { [k: string]: any } = {
        _id: new ObjectId(),
        display_id: await this.uniqueIdService.getUniqueOrderid(),
        user_id,
        order_status: OrderStatus.PLACED,
        payment_mode: pay_method_name,
        payment_status: PaymentStatus.UNPAID,
        unit_price: cartData.unit_price,
        selling_price: cartData.selling_price,
        margin_price: cartData.margin_price,
        total_amount: cartData.total_amount,
        TM_price: cartData.TM_price,
        CP_price: cartData.CP_price,
        total_CP_price: cartData.total_CP_price,
        coupon_amount: cartData.coupon_amount,
        cgst_price: cartData.cgst_price,
        sgst_price: cartData.sgst_price,
        igst_price: cartData.igst_price,
        gst_price: cartData.gst_price,
        site_id: cartData.site_id,
        delivery_location: cartData.delivery_location,
        gateway_transaction_id,
        unique_id: cartData.unique_id,
        selling_price_With_Margin: cartData.selling_price_With_Margin,
        billing_address_id: cartData.billingAddressInfo._id,
        coupon_per: cartData.coupon_per,
        amount: cartData.amount,
      }
      if (isNullOrUndefined(cartData.site_id)) {
        return Promise.reject(
          new InvalidInput(
            `Kindly enter delivery site to place your order`,
            400
          )
        )
      }
      const siteInfo = await SiteModel.findById(cartData.site_id).lean()
      if (siteInfo === null) {
        return Promise.reject(
          new InvalidInput(`Site reference couldn't be found`, 400)
        )
      }

      let clientData = await BuyerUserModel.findOne({
        _id: new ObjectId(user_id),
      })

      let client_name: any
      let mobile_number: any
      let email: any
      if (!isNullOrUndefined(clientData)) {
        mobile_number = clientData.mobile_number
        email = clientData.email
        if (clientData.account_type === "Individual") {
          client_name = clientData.full_name
        } else {
          client_name = clientData.company_name
        }
      }

      const adminUserData = await AdminUserModel.findOne({
        admin_type: "superAdmin",
      })

      if (isNullOrUndefined(adminUserData)) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      await rzp.payments
        .fetch(gateway_transaction_id)
        .then((data: any) => {
          console.log("dataaaaaa", data)
          orderData.fee = data.fee / 100
          orderData.tax = data.tax / 100
        })
        .catch((error: any) => {
          let message = `Hi ${client_name}, payment attempt by you for an Order ID ${orderData.display_id} has failed. Kindly retry. - conmix`
          let templateId = "1707163732922904474"
          this.testService.sendSMS(mobile_number, message, templateId)

          let payment_failed_html = client_mail_template.payment_failed
          payment_failed_html = payment_failed_html.replace(
            "{{client_name}}",
            client_name
          )
          var data11 = {
            from: "no-reply@conmate.in",
            to: email,
            subject: "Conmix - Payment Failed",
            html: payment_failed_html,
          }
          this.mailgunService.sendEmail(data11)

          let payment_failed_admin_html = admin_mail_template.payment_failed
          payment_failed_admin_html = payment_failed_admin_html.replace(
            "{{client_name}}",
            client_name
          )
          var data_failed = {
            from: "no-reply@conmate.in",
            to: adminUserData.email,
            subject: "Conmix - Payment Failed",
            html: payment_failed_html,
          }
          this.mailgunService.sendEmail(data_failed)

          return Promise.reject(new InvalidInput(error))
        })

      orderData.site_name = siteInfo.site_name
      orderData.address_line1 = siteInfo.address_line1
      orderData.address_line2 = siteInfo.address_line2
      orderData.state_id = siteInfo.state_id
      orderData.city_id = siteInfo.city_id
      orderData.sub_city_id = siteInfo.sub_city_id
      orderData.pincode = siteInfo.pincode

      // let message = `Hi ${client_name}, thanks for using Conmix. You have successfully placed an order with an Order ID ${orderData.display_id} for your site ${orderData.site_name}. Kindly login to Conmix App to track your order online.`
      // let templateId = "1707163729963397741"
      // await this.testService.sendSMS(mobile_number, message, templateId)

      // let site_message = `Hi ${client_name}, thanks for using Conmix. You have successfully placed an order with an Order ID ${orderData.display_id} for your site ${orderData.site_name}. Kindly login to Conmix App to track your order online.`
      // let site_templateId = "1707163729963397741"
      // await this.testService.sendSMS(siteInfo.mobile_number, site_message, site_templateId)

      const billingAddressInfo = await BuyerBillingAddressModel.findById(
        cartData.billing_address_id
      ).lean()
      if (billingAddressInfo === null) {
        return Promise.reject(
          new InvalidInput(`Billing address reference couldn't be found`, 400)
        )
      }

      orderData.billing_address_full_name = billingAddressInfo.full_name
      orderData.billing_address_company_name = billingAddressInfo.company_name
      orderData.billing_address_line1 = billingAddressInfo.line1
      orderData.billing_address_line2 = billingAddressInfo.line2
      orderData.billing_address_state_id = billingAddressInfo.state_id
      orderData.billing_address_city_id = billingAddressInfo.city_id
      orderData.billing_address_pincode = billingAddressInfo.pincode
      orderData.billing_address_gst_number = billingAddressInfo.gst_number

      const orderItems: any[] = []
      for (let i = 0; i < cartData.items.length; i++) {
        const item = cartData.items[i]

        console.log(
          "%%%%%%%%%%%%%%%%%%% item %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
          item
        )
        if (item.error) {
          return Promise.reject(
            new InvalidInput(
              `Sorry, your order couldn't be placed${item.errorMessage}`
            )
          )
        }
        //   //let orderItem: any = {}

        let orderItem: { [k: string]: any } = {
          display_item_id: await this.uniqueIdService.getUniqueOrderItemId(),
          design_mix_id: item.design_mix_id,
          buyer_id: user_id,
          order_id: orderData._id,
          quantity: item.quantity,
          cement_per_kg_rate: item.cement_per_kg_rate,
          sand_per_kg_rate: item.sand_per_kg_rate,
          aggregate1_per_kg_rate: item.aggregate1_per_kg_rate,
          aggregate2_per_kg_rate: item.aggregate2_per_kg_rate,
          fly_ash_per_kg_rate: item.fly_ash_per_kg_rate,
          admix_per_kg_rate: item.admix_per_kg_rate,
          water_per_ltr_rate: item.water_per_ltr_rate,
          cement_price: item.cement_price,
          sand_price: item.sand_price,
          aggreagte1_price: item.aggreagte1_price,
          aggreagte2_price: item.aggreagte2_price,
          fly_ash_price: item.fly_ash_price,
          admix_price: item.admix_price,
          water_price: item.water_price,
          with_TM: item.with_TM,
          with_CP: item.with_CP,
          distance: item.distance,
          unit_price: item.unit_price,
          selling_price: item.selling_price,
          margin_price: item.margin_price_for_selling,
          selling_price_With_Margin: item.selling_price_With_Margin,
          unit_price_With_Margin: item.unit_price_With_Margin,
          TM_price: item.TM_price,
          CP_price: item.CP_price,
          total_CP_price: item.total_CP_price,
          cgst_rate: item.cgst_rate,
          sgst_rate: item.sgst_rate,
          sgst_price: item.sgst_price,
          cgst_price: item.cgst_price,
          gst_price: item.gst_price,
          total_amount: item.total_amount,
          delivery_date: item.delivery_date,
          end_date: item.end_date,
          item_status: OrderItemStatus.PLACED,
          remaining_quantity: item.quantity,
          temp_quantity: item.quantity,
          part_quantity: item.quantity,
          TM_no: item.TM_no,
          driver_name: item.driver_name,
          driver_mobile: item.driver_mobile,
          TM_operator_name: item.TM_operator_name,
          TM_operator_mobile_no: item.TM_operator_mobile_no,
        }

        if (!isNullOrUndefined(item.design_mix_id)) {
          orderItem.vendor_id = item.vendor_id
          orderItem.address_id = item.address_id
          orderItem.concrete_grade_id = item.concrete_grade_id
          orderItem.admix_brand_id = item.final_admix_brand_id
          orderItem.admix_cat_id = item.final_admix_cat_id
          orderItem.fly_ash_source_id = item.final_fly_ash_source_id
          orderItem.aggregate2_sub_cat_id = item.final_aggregate2_sub_cat_id
          orderItem.agg_source_id = item.final_agg_source_id
          orderItem.aggregate1_sub_cat_id = item.final_aggregate1_sub_cat_id
          orderItem.sand_source_id = item.final_sand_source_id
          orderItem.cement_brand_id = item.final_cement_brand_id
          orderItem.cement_grade_id = item.final_cement_grade_id
          orderItem.cement_quantity = item.cement_quantity
          orderItem.sand_quantity = item.sand_quantity
          orderItem.aggregate1_quantity = item.aggregate1_quantity
          orderItem.aggregate2_quantity = item.aggregate2_quantity
          orderItem.fly_ash_quantity = item.fly_ash_quantity
          orderItem.admix_quantity = item.admix_quantity
          orderItem.water_quantity = item.water_quantity
          orderItem.ad_mixture1_brand_id = item.ad_mixture1_brand_id
          orderItem.ad_mixture1_category_id = item.ad_mixture1_category_id
          orderItem.ad_mixture2_brand_id = item.ad_mixture2_brand_id
          orderItem.ad_mixture2_category_id = item.ad_mixture2_category_id
        } else {
          orderItem.vendor_id = item.custom_mix.vendor_id
          orderItem.address_id = item.custom_mix.address_id
          orderItem.concrete_grade_id = item.custom_mix.concrete_grade_id
          orderItem.admix_brand_id = item.custom_mix.final_admix_brand_id
          orderItem.admix_cat_id = item.custom_mix.final_admix_cat_id
          orderItem.fly_ash_source_id = item.custom_mix.final_fly_ash_source_id
          orderItem.aggregate2_sub_cat_id =
            item.custom_mix.final_aggregate2_sub_cat_id
          orderItem.agg_source_id = item.custom_mix.final_agg_source_id
          orderItem.aggregate1_sub_cat_id =
            item.custom_mix.final_aggregate1_sub_cat_id
          orderItem.sand_source_id = item.custom_mix.final_sand_source_id
          orderItem.cement_brand_id = item.custom_mix.final_cement_brand_id
          orderItem.cement_grade_id = item.custom_mix.final_cement_grade_id
          orderItem.cement_quantity = item.custom_mix.cement_quantity
          orderItem.sand_quantity = item.custom_mix.sand_quantity
          orderItem.aggregate1_quantity = item.custom_mix.aggregate1_quantity
          orderItem.aggregate2_quantity = item.custom_mix.aggregate2_quantity
          orderItem.fly_ash_quantity = item.custom_mix.fly_ash_quantity
          orderItem.admix_quantity = item.custom_mix.admix_quantity
          orderItem.water_quantity = item.custom_mix.water_quantity
        }

        let addressData = await AddressModel.findById({
          _id: new ObjectId(orderItem.address_id),
        }).exec()

        console.log("addressData", addressData)
        if (!isNullOrUndefined(addressData)) {
          orderItem.line1 = addressData.line1
          orderItem.line2 = addressData.line2
          orderItem.state_id = addressData.state_id
          orderItem.city_id = addressData.city_id
          orderItem.pincode = addressData.pincode
          orderItem.location = addressData.location
          orderItem.address_type = addressData.address_type
          orderItem.business_name = addressData.business_name
          orderItem.city_name = addressData.stateDetails.state_name
          orderItem.state_name = addressData.cityDetails.city_name
          orderItem.sub_vendor_id = addressData.sub_vendor_id
          orderItem.master_vendor_id = addressData.user.user_id
          // orderItem.region_id = addressData.region_id
          // orderItem.source_id = addressData.source_id
          // let data = await CityModel.findOne({
          //   _id: new ObjectId(addressData.city_id),
          // })

          // console.log("data", data)
          // if (!isNullOrUndefined(data)) {
          //   orderItem.city_name = data.city_name
          //   orderItem.state_name = data.state_name
          // }
        }

        orderItems.push(orderItem)
         }

      try {
        console.log(`adding orderData: `, orderData)
        //await OrderModel.create([orderData], { session })
        await OrderModel.create([orderData])
        // console.log(`adding orderItems: `, orderItems)
        // await OrderItemModel.create(orderItems, {
        //   session,
        // })

        await OrderItemModel.create(orderItems)
        //  session.commitTransaction()
        console.log(`New order created`)

        // const adminUserDoc = await AdminUserModel.findOne({
        //   admin_type: "superAdmin",
        // })

        // if (isNullOrUndefined(adminUserDoc)) {
        //   return Promise.reject(
        //     new InvalidInput(`No Admin data were found `, 400)
        //   )
        // }
        // let siteData = await SiteModel.findOne({
        //   _id: new ObjectId(orderData.site_id),
        // })
        // let site_name: any
        // if (!isNullOrUndefined(siteData)) {
        //   site_name = siteData.site_name
        // }

        // let message1 = `Hi ${client_name}, thanks for using Conmix. You have successfully placed an order for ${"product"} with ${"vendor"} with an Order ID ${
        //   orderData._id
        // } for your site ${site_name}. Kindly login to Conmix App to track your order online.`

        let new_order_html = client_mail_template.new_order
        new_order_html = new_order_html.replace("{{client_name}}", client_name)
        new_order_html = new_order_html.replace(
          "{{orderData.display_id}}",
          orderData.display_id
        )
        new_order_html = new_order_html.replace(
          "{{orderData.site_name}}",
          orderData.site_name
        )
        var data = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - New Order",
          html: new_order_html,
        }
        await this.mailgunService.sendEmail(data)

        let message = `Hi ${client_name}, thanks for using Conmix. You have successfully placed an order with an Order ID ${orderData.display_id} for your site ${orderData.site_name}. Kindly login to Conmix App to track your order online.`
        let templateId = "1707163729963397741"
        await this.testService.sendSMS(mobile_number, message, templateId)

        let pay_msg = `Hi ${client_name}, Thank You. We have received the payment for your order having an Order ID ${orderData.display_id}. - conmix `
        let pay_templateId = "1707163732921036782"
        await this.testService.sendSMS(mobile_number, pay_msg, pay_templateId)

        let payment_successful_html = client_mail_template.payment_successfully
        payment_successful_html = payment_successful_html.replace(
          "{{client_name}}",
          client_name
        )
        payment_successful_html = payment_successful_html.replace(
          "{{orderData.display_id}}",
          orderData.display_id
        )
        var data11 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Payment Successfully",
          html: payment_successful_html,
        }
        await this.mailgunService.sendEmail(data11)

        // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        },
        {
          admin_type : AdminRoles.admin_accounts
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i< adminUserDocs.length; i++){
        let payment_successful_admin_html =
        admin_mail_template.payment_successfully
        payment_successful_admin_html = payment_successful_admin_html.replace(
        "{{orderData.display_id}}",
        orderData.display_id
      )
      var data_admin = {
        from: "no-reply@conmate.in",
        to: adminUserDocs[i].email,
        subject: "Conmix - Payment Successfully",
        html: payment_successful_admin_html,
      }
      await this.mailgunService.sendEmail(data_admin)
      }
 

        let site_message = `Hi ${siteInfo.person_name}, thanks for using Conmix. You have successfully placed an order with an Order ID ${orderData.display_id} for your site ${orderData.site_name}. Kindly login to Conmix App to track your order online.`
        let site_templateId = "1707163729963397741"
        await this.testService.sendSMS(
          siteInfo.mobile_number,
          site_message,
          site_templateId
        )
        let site_email = siteInfo.email

        let new_order_html_site = client_mail_template.new_order
        new_order_html_site = new_order_html_site.replace(
          "{{client_name}}",
          siteInfo.person_name
        )
        new_order_html_site = new_order_html_site.replace(
          "{{orderData.display_id}}",
          orderData.display_id
        )
        new_order_html_site = new_order_html_site.replace(
          "{{orderData.site_name}}",
          orderData.site_name
        )
        var data1 = {
          from: "no-reply@conmate.in",
          to: site_email,
          subject: "Conmix - New Order",
          html: new_order_html_site,
        }
        await this.mailgunService.sendEmail(data1)
      } catch (err) {
        // session.abortTransaction()
        console.log(`Sorry, we couldn't create your order  ${err.message}`)
        return Promise.reject(
          new InvalidInput(
            `Sorry, We could not process your order ${err.message}`,
            400
          )
        )
      }
      await CartModel.deleteOne({ user_id })

      const buyerUserDoc = await BuyerUserModel.findById(user_id)
      if (isNullOrUndefined(buyerUserDoc)) {
        return Promise.reject(
          new InvalidInput(`No Buyer data were found `, 400)
        )
      }
      console.log(`Notificationnnnnnnnnnnnnnnnnnnnnn`)
      // notification to Buyer for place order.
      for (let j = 0; j < orderItems.length; j++) {
        console.log("herree__itemmmmmmmmmmmmmmm", orderItems)
        console.log("herree__itemmmmmmmmmmmmmmm", orderItems[j].display_item_id)
        await this.notUtil.addNotificationForBuyer({
          to_user_type: UserType.BUYER,
          to_user_id: buyerUserDoc._id,
          notification_type: ClientNotificationType.orderPlaced,
          order_id: orderData._id,
          order_item_id: orderItems[j].display_item_id,
          site_id: orderData.site_id,
        })
      }

      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i< adminUserDocs.length; i ++){

        // Send notification.
        await this.notUtil.addNotificationForAdmin({
          to_user_type: UserType.ADMIN,
          to_user_id: adminUserDocs[i]._id,
          notification_type: AdminNotificationType.orderPlaced,
          order_id: orderData._id,
          client_id: orderData.user_id,
          site_id: orderData.site_id,
        })

         // Send Email.
        let new_order_html_admin = admin_mail_template.new_order
        new_order_html_admin = new_order_html_admin.replace(
          "{{adminUserDoc.full_name}}",
          adminUserDocs[i].full_name
        )
        new_order_html_admin = new_order_html_admin.replace(
          "{{orderData.display_id}}",
          orderData.display_id
        )
        new_order_html_admin = new_order_html_admin.replace(
          "{{client_name}}",
          client_name
        )
        var data0 = {
          from: "no-reply@conmate.in",
          to: adminUserDocs[i].email,
          subject: "Conmix - New Order",
          html: new_order_html_admin,
        }
        await this.mailgunService.sendEmail(data0)
      }
      
      return Promise.resolve({
        _id: orderData._id,
        display_id: orderData.display_id,
      })
    } catch (err) {
      console.log(`Sorry, we couldn't create your order  ${err.message}`)
      return Promise.reject(
        new InvalidInput(
          `Sorry, we couldn't create your order  ${err.message}`,
          400
        )
      )
    }
  }

  async getOrder(
    user_id?: string,
    before?: string,
    after?: string,
    order_status?: string,
    payment_status?: string,
    gateway_transaction_id?: string,
    site_id?: string,
    month?: string,
    year?: number,
    date?: Date,
    total_amount?: any,
    total_amount_range?: string,
    start_date?: string,
    end_date?: string
  ) {
    const query: { [k: string]: any } = {}

    query.user_id = user_id

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(order_status)) {
      query.order_status = order_status
    }
    if (!isNullOrUndefined(payment_status)) {
      query.payment_status = payment_status
    }
    if (!isNullOrUndefined(gateway_transaction_id)) {
      query.gateway_transaction_id = gateway_transaction_id
    }
    if (!isNullOrUndefined(site_id)) {
      query.site_id = site_id
    }

    if (!isNullOrUndefined(total_amount) && 
    !isNullOrUndefined(total_amount_range) && 
    total_amount_range === "less") {
      query.total_amount =  { $lte: parseInt(total_amount) }
    }
    if (!isNullOrUndefined(total_amount) && 
    !isNullOrUndefined(total_amount_range) &&
    total_amount_range === "more")  {
      query.total_amount =  { $gte: parseInt(total_amount) }
    }
console.log("queery", query)
    if (!isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }

    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    if (!isNullOrUndefined(date)) {
      var d1 = new Date(date)
      console.log("d1", d1)
      let next_day = new Date(d1.setDate(d1.getDate() + 1))
      console.log("date", date)
      console.log("next_day", next_day)
      query.created_at = {
        $gte: date,
        $lte: next_day,
      }
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.order_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.order_date = { $lte: end_date }
    }

    return OrderModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async getOrderById(user_id: string, orderId: string) {
    const orderQuery: { [k: string]: any } = {}

    console.log("user_id", user_id)
    orderQuery._id = new ObjectId(orderId)
    orderQuery.user_id = new ObjectId(user_id)
    const aggregateOrderArr = [
      { $match: orderQuery },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "site",
          localField: "site_id",
          foreignField: "_id",
          as: "site",
        },
      },
      {
        $unwind: "$site",
      },
      {
        $lookup: {
          from: "buyer_billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "buyer_billing_address",
        },
      },
      {
        $unwind: {
          path: "$buyer_billing_address",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "buyer_billing_address.city_id",
          foreignField: "_id",
          as: "billing_city",
        },
      },
      {
        $unwind: "$billing_city",
      },
      {
        $project: {
          _id: 1,
          display_id: 1,
          user_id: 1,
          order_status: 1,
          payment_status: 1,
          selling_price: 1,
          margin_price: 1,
          gst_amount: 1,
          TM_price: 1,
          CP_price: 1,
          delivery_location: 1,
          payment_attempt: 1,
          payment_mode: 1,
          base_amount: 1,
          logistics_amount: 1,
          margin_amount: 1,
          coupon_amount: 1,
          cgst_price: 1,
          sgst_price: 1,
          igst_price: 1,
          gst_price: 1,
          total_amount: 1,
          site_id: 1,
          site_name: 1,
          company_name: "$site.company_name",
          address_line1: 1,
          address_line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          created_at: 1,
          gateway_transaction_id: 1,
          city_name: "$city.city_name",
          country_code: "$city.country_code",
          country_name: "$city.country_name",
          state_name: "$city.state_name",
          mobile_number: "$site.mobile_number",
          alt_mobile_number: "$site.alt_mobile_number",
          fee: 1,
          tax: 1,
          selling_price_With_Margin: 1,
          total_CP_price: 1,
          "invoice._id": true,
          "buyer._id": true,
          "buyer.full_name": true,
          billing_address_id: 1,
          buyer_billing_address: 1,
          buyer_billing_address_city_name: "$billing_city.city_name",
          buyer_billing_address_state_name: "$billing_city.state_name",
          amount: true,
          // "billing_address._id": true,
          // "billing_address.company_name": true,
          // "billing_address.line1": true,
          // "billing_address.line2": true,
          // "billing_address.state_id": true,
          // "billing_address.city_id": true,
          // "billing_address.pincode": true,
        },
      },
    ]
    const orderData = await OrderModel.aggregate(aggregateOrderArr)
    if (orderData.length <= 0) {
      return Promise.reject(
        new InvalidInput(`We could not find order details for this order`, 400)
      )
    }
    const orderItemQuery: { [k: string]: any } = {}
    orderItemQuery.order_id = new ObjectId(orderId)
    orderItemQuery.buyer_id = new ObjectId(user_id)
    console.log(orderItemQuery)
    const aggregateArr = [
      { $match: orderItemQuery },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "vendor_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order",
          localField: "order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item_part",
          localField: "order_id",
          foreignField: "buyer_order_id",
          as: "orderItemPartData",
        },
      },
      {
        $project: {
          display_item_id: 1,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "order.gateway_transaction_id": 1,
          cement_quantity: 1,
          sand_quantity: 1,
          aggregate1_quantity: 1,
          aggregate2_quantity: 1,
          fly_ash_quantity: 1,
          admix_quantity: 1,
          water_quantity: 1,
          vendor_id: 1,
          buyer_id: 1,
          order_id: 1,
          quantity: 1,
          revised_quantity: 1,
          cement_per_kg_rate: 1,
          sand_per_kg_rate: 1,
          aggregate1_per_kg_rate: 1,
          aggregate2_per_kg_rate: 1,
          fly_ash_per_kg_rate: 1,
          admix_per_kg_rate: 1,
          water_per_ltr_rate: 1,
          cement_price: 1,
          sand_price: 1,
          aggreagte1_price: 1,
          aggreagte2_price: 1,
          fly_ash_price: 1,
          admix_price: 1,
          water_price: 1,
          with_TM: 1,
          with_CP: 1,
          distance: 1,
          unit_price: 1,
          selling_price: 1,
          margin_price: 1,
          TM_price: 1,
          CP_price: 1,
          sgst_rate: 1,
          cgst_price: 1,
          gst_price: 1,
          coupon_amount: 1,
          item_status: 1,
          created_at: 1,
          created_by_id: 1,
          created_by_type: 1,
          payment_status: 1,
          address_id: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          location: 1,
          address_type: 1,
          business_name: 1,
          city_name: 1,
          state_name: 1,
          vendor_media: 1,
          remaining_quantity: 1,
          temp_quantity: 1,
          pickup_quantity: 1,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          design_mix_id: true,
          "vendor._id": 1,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
          TM_no: true,
          driver_name: true,
          driver_mobile: true,
          TM_operator_name: true,
          TM_operator_mobile_no: true,
          selling_price_With_Margin: true,
          unit_price_With_Margin: true,
          delivery_date: true,
          end_date: true,
          part_quantity: true,
          orderItemPartData: true,
          total_CP_price: 1,
        },
      },
    ]
    const orderItemData = await OrderItemModel.aggregate(aggregateArr)

    for (let i = 0; i < orderItemData.length; i++) {
      const data = await OrderItemModel.findOne({
        buyer_id: new ObjectId(user_id),
        vendor_id: new ObjectId(orderItemData[i].vendor_id),
        // product_id: new ObjectId(orderItemData[i].product._id),
        item_status: { $eq: OrderItemStatus.DELIVERED },
      })

      let write_review: boolean = false
      if (!isNullOrUndefined(data)) {
        write_review = true
      }
      orderItemData[i].write_review = write_review

      const review = await ReviewModel.findOne({
        user_id: new ObjectId(user_id),
        vendor_id: new ObjectId(orderItemData[i].vendor_id),
      })

      if (!isNullOrUndefined(review)) {
        orderItemData[i].review = review
      }

      for (let j = 0; j < orderItemData[i].orderItemPartData.length; j++) {
        let TMtrack_details = await OrderTrackModel.find({
          order_item_part_id: new ObjectId(
            orderItemData[i].orderItemPartData[j]._id
          ),
        })
        orderItemData[i].orderItemPartData[j].TMtrack_details = TMtrack_details

        let CPtrack_details = await CPOrderTrackModel.find({
          order_item_part_id: new ObjectId(
            orderItemData[i].orderItemPartData[j]._id
          ),
        })
        orderItemData[i].orderItemPartData[j].CPtrack_details = CPtrack_details
      }
    }

    return Promise.resolve({
      orderData: orderData[0],
      orderItemData,
    })
  }

  async cancelOrderItem(
    user_id: string,
    order_item_id: string,
    order_id: string,
    gateway_transaction_id: string
  ) {
    //Find buyer order.
    const data = await OrderTrackModel.findOne({
      order_item_id: new ObjectId(order_item_id),
      user_id,
      buyer_order_id: new ObjectId(order_id),
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `You cannot cancel this order item as it has been picked-up for delivery`,
          400
        )
      )
    }

    await OrderTrackModel.updateOne(
      {
        buyer_order_id: new ObjectId(order_id),
        order_item_id: new ObjectId(order_item_id),
        user_id,
      },
      {
        $set: {
          event_status: LogisticsOrderStatus.CANCELLED,
          cancelled_at: Date.now(),
        },
      }
    )

    let orderItemDoc = await OrderItemModel.findOneAndUpdate(
      { _id: new ObjectId(order_item_id) },
      {
        $set: {
          item_status: OrderItemStatus.CANCELLED,
          cancelled_at: Date.now(),
        },
      }
    )

    if (isNullOrUndefined(orderItemDoc)) {
      return Promise.reject(
        new InvalidInput(`No Order item doc were found `, 400)
      )
    }
    // const adminUserDoc = await AdminUserModel.findOne({
    //   admin_type: "superAdmin",
    // })

    // if (isNullOrUndefined(adminUserDoc)) {
    //   return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    // }

    let buyerUserDoc = await BuyerUserModel.findOne({
      _id: new ObjectId(user_id),
    })

    let client_name: any
    let mobile_number: any
    let email: any
    if (!isNullOrUndefined(buyerUserDoc)) {
      mobile_number = buyerUserDoc.mobile_number
      email = buyerUserDoc.email
      if (buyerUserDoc.account_type === "Individual") {
        client_name = buyerUserDoc.full_name
      } else {
        client_name = buyerUserDoc.company_name
      }
    }


    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(order_id),
    })
    let display_id: any
    let site_id: any
    if (!isNullOrUndefined(buyerOrderDoc)) {
      display_id = buyerOrderDoc.display_id
      site_id = buyerOrderDoc.site_id
    }

    let vendorOrderDoc = await VendorOrderModel.findOneAndUpdate(
      {
        buyer_order_id: new ObjectId(order_id),
        buyer_order_item_id: new ObjectId(order_item_id),
      },
      {
        $set: {
          order_status: SupplierOrderStatus.CANCELLED,
          cancelled_at: Date.now(),
        },
      }
    )
    if (isNullOrUndefined(vendorOrderDoc)) {
      return Promise.reject(
        new InvalidInput(`No Vendor Order Doc were Found.`, 400)
      )
    }

    //let master_vendor_name: any
    let master_vendor_mobile_number: any
    let master_vendor_company_name: any
    let master_vendor_email: any
    let mastervendorData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderDoc.master_vendor_id),
    })
    if (!isNullOrUndefined(mastervendorData)) {
      //master_vendor_name = mastervendorData.full_name
      master_vendor_mobile_number = mastervendorData.mobile_number
      master_vendor_company_name = mastervendorData.company_name
      master_vendor_email = mastervendorData.email
    }

    // let sub_vendor_name: any
    let sub_vendor_mobile_number: any
    let sub_vendor_company_name: any
    let sub_vendor_email: any
    let subvendorData = await VendorUserModel.findOne({
      _id: new ObjectId(vendorOrderDoc.sub_vendor_id),
    })
    if (!isNullOrUndefined(subvendorData)) {
      // sub_vendor_name = subvendorData.full_name
      sub_vendor_mobile_number = subvendorData.mobile_number
      sub_vendor_company_name = subvendorData.company_name
      sub_vendor_email = subvendorData.email
    }

    let site_Info = await SiteModel.findOne({
      _id: new ObjectId(site_id),
    })
    //let site_name: any
    let site_mobile_number: any
    let site_person_name: any
    let site_email: any
    if (!isNullOrUndefined(site_Info)) {
      //site_name = site_Info.site_name
      site_mobile_number = site_Info.mobile_number
      site_person_name = site_Info.person_name
      site_email = site_Info.email
    }

    // let message = `Hi ${client_name}, you have rejected the material delivered to your site ${site_name} for the product id ${orderItemDoc.display_item_id} having an Order ID ${display_id}. - conmix`
    let message = `Hi ${client_name}, you have cancelled the product id ${orderItemDoc.display_item_id} which was to be supplied by RMC Supplier ${sub_vendor_company_name} having an Order ID ${display_id}. Your order is thus partially cancelled. - conmix`
    let templateId = "1707163732931326735"
    await this.testService.sendSMS(mobile_number, message, templateId)

    let product_cancelled_html = client_mail_template.product_cancelled
    product_cancelled_html = product_cancelled_html.replace(
      "{{client_name}}",
      client_name
    )
    product_cancelled_html = product_cancelled_html.replace(
      "{{orderItemDoc.display_item_id}}",
      orderItemDoc.display_item_id
    )
    product_cancelled_html = product_cancelled_html.replace(
      "{{sub_vendor_company_name}}",
      sub_vendor_company_name
    )
    product_cancelled_html = product_cancelled_html.replace(
      "{{display_id}}",
      display_id
    )
    var data1 = {
      from: "no-reply@conmate.in",
      to: email,
      subject: "Conmix - Product Cancelled",
      html: product_cancelled_html,
    }
    await this.mailgunService.sendEmail(data1)

    let site_message = `Hi ${site_person_name}, you have cancelled the product id ${orderItemDoc.display_item_id} which was to be supplied by RMC Supplier ${sub_vendor_company_name} having an Order ID ${display_id}. Your order is thus partially cancelled. - conmix`
    let site_templateId = "1707163732931326735"
    await this.testService.sendSMS(
      site_mobile_number,
      site_message,
      site_templateId
    )

    let product_cancelled_site_html = client_mail_template.product_cancelled
    product_cancelled_site_html = product_cancelled_site_html.replace(
      "{{client_name}}",
      site_person_name
    )
    product_cancelled_site_html = product_cancelled_site_html.replace(
      "{{orderItemDoc.display_item_id}}",
      orderItemDoc.display_item_id
    )
    product_cancelled_site_html = product_cancelled_site_html.replace(
      "{{sub_vendor_company_name}}",
      sub_vendor_company_name
    )
    product_cancelled_site_html = product_cancelled_site_html.replace(
      "{{display_id}}",
      display_id
    )
    var data1 = {
      from: "no-reply@conmate.in",
      to: site_email,
      subject: "Conmix - Product Cancelled",
      html: product_cancelled_site_html,
    }
    await this.mailgunService.sendEmail(data1)

    let refund_message1 = `Hi ${client_name}, your payment refund for the cancelled product id ${orderItemDoc.display_item_id} having an Order ID ${display_id} will be processed as per our Cancellation Policy. Kindly refer to the policy and contact Conmix Customer Care for more information.`
    let refund_templateId1 = "1707163730022020433"
    await this.testService.sendSMS(
      mobile_number,
      refund_message1,
      refund_templateId1
    )

    let order_cancelled_buyer_refund_html =
      client_mail_template.order_cancelled_buyer_payment_refund
    order_cancelled_buyer_refund_html = order_cancelled_buyer_refund_html.replace(
      "{{client_name}}",
      client_name
    )
    order_cancelled_buyer_refund_html = order_cancelled_buyer_refund_html.replace(
      "{{orderItemDoc.display_item_id}}",
      orderItemDoc.display_item_id
    )
    order_cancelled_buyer_refund_html = order_cancelled_buyer_refund_html.replace(
      "{{display_id}}",
      display_id
    )
    var data1 = {
      from: "no-reply@conmate.in",
      to: email,
      subject: "Conmix - Cancelled order payment refund",
      html: order_cancelled_buyer_refund_html,
    }
    await this.mailgunService.sendEmail(data1)

    let refund_message1_site = `Hi ${site_person_name}, your payment refund for the cancelled product id ${orderItemDoc.display_item_id} having an Order ID ${display_id} will be processed as per our Cancellation Policy. Kindly refer to the policy and contact Conmix Customer Care for more information.`
    let refund_templateId1_site = "1707163730022020433"
    await this.testService.sendSMS(
      site_mobile_number,
      refund_message1_site,
      refund_templateId1_site
    )

    let order_cancelled_buyer_refund_site_html =
    client_mail_template.order_cancelled_buyer_payment_refund
    order_cancelled_buyer_refund_site_html = order_cancelled_buyer_refund_site_html.replace(
    "{{client_name}}",
    site_person_name
  )
  order_cancelled_buyer_refund_site_html = order_cancelled_buyer_refund_site_html.replace(
    "{{orderItemDoc.display_item_id}}",
    orderItemDoc.display_item_id
  )
  order_cancelled_buyer_refund_site_html = order_cancelled_buyer_refund_site_html.replace(
    "{{display_id}}",
    display_id
  )
  var data1 = {
    from: "no-reply@conmate.in",
    to: site_email,
    subject: "Conmix - Cancelled order payment refund",
    html: order_cancelled_buyer_refund_site_html,
  }
  await this.mailgunService.sendEmail(data1)

      // Check Admin Type.
      const re_adminQuery1: { [k: string]: any } = {}
      re_adminQuery1.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        },
        {
          admin_type : AdminRoles.admin_accounts
        }
      ]
      const re_adminUserDocs1 = await AdminUserModel.find(re_adminQuery1)
      if (re_adminUserDocs1.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }
      for(let i = 0 ; i> re_adminUserDocs1.length ; i ++){ 
        let order_cancelled_buyer_refund_admin_html =
        admin_mail_template.order_cancelled_buyer_payment_refund_admin
        order_cancelled_buyer_refund_admin_html = order_cancelled_buyer_refund_admin_html.replace(
        "{{client_name}}",
        client_name
      )
      order_cancelled_buyer_refund_admin_html = order_cancelled_buyer_refund_admin_html.replace(
        "{{orderItemDoc.display_item_id}}",
        orderItemDoc.display_item_id
      )
      order_cancelled_buyer_refund_admin_html = order_cancelled_buyer_refund_admin_html.replace(
        "{{display_id}}",
        display_id
      )
      var data11 = {
        from: "no-reply@conmate.in",
        to: re_adminUserDocs1[i].email,
        subject: "Conmix - Cancelled order payment refund",
        html: order_cancelled_buyer_refund_admin_html,
      }
      await this.mailgunService.sendEmail(data11)
      }


    // let vendor_message1 = `Hi ${vendor_name}, your client ${client_name} has rejected the material order for Product Id ${orderItemDoc.display_item_id} having an Order ID ${display_id}. Kindly do not supply Poor Quality Material. You have been warned for this action from the client. - conmix`
    // let vendor_templateId1 = "1707163732952588028"
    // await this.testService.sendSMS(vendor_mobile_number, vendor_message1, vendor_templateId1)

    // let m_vendor_message = `Hi ${master_vendor_company_name}, your client ${client_name} has cancelled the order for Product Id ${orderItemDoc.display_item_id} having an Order ID ${vendorOrderDoc._id}. - conmix`
    // let m_vendor_templateId = "1707163732954309880"
    // await this.testService.sendSMS(
    //   master_vendor_mobile_number,
    //   m_vendor_message,
    //   m_vendor_templateId
    // )

    // let m_vendor_message = `Hi ${master_vendor_company_name}, your client ${client_name} has cancelled the order for Product Id ${orderItemDoc.display_item_id} having an Order ID ${vendorOrderDoc._id}. - conmix`
    let m_vendor_message = `Hi ${master_vendor_company_name}, your client ${client_name} has cancelled the order for Product Id ${orderItemDoc.display_item_id} having an Order ID ${vendorOrderDoc._id}. - conmix`
    let m_vendor_templateId = "1707164146056337625"
    await this.testService.sendSMS(
      master_vendor_mobile_number,
      m_vendor_message,
      m_vendor_templateId
    )


    
    // Check Admin Type.
    const adminQuery1: { [k: string]: any } = {}
    adminQuery1.$or = [
      {
        admin_type : AdminRoles.superAdmin
      },
      {
        admin_type : AdminRoles.admin_manager
      },
      {
        admin_type : AdminRoles.admin_sales
      }
    ]
    const adminUserDocs1 = await AdminUserModel.find(adminQuery1)
    if (adminUserDocs1.length < 0) {
      return Promise.reject(
        new InvalidInput(`No Admin data were found `, 400)
      )
    }

    for(let i = 0 ; i> adminUserDocs1.length ; i ++){
      let order_cancelled_by_buyer_admin_html =
      admin_mail_template.order_cancelled_by_buyer

      order_cancelled_by_buyer_admin_html = order_cancelled_by_buyer_admin_html.replace(
        "{{adminUserDoc.full_name}}",
        adminUserDocs1[i].full_name
      )
      order_cancelled_by_buyer_admin_html = order_cancelled_by_buyer_admin_html.replace(
      "{{vendor_name}}",
      master_vendor_company_name
    )
    order_cancelled_by_buyer_admin_html = order_cancelled_by_buyer_admin_html.replace(
      "{{client_name}}",
      client_name
    )
    order_cancelled_by_buyer_admin_html = order_cancelled_by_buyer_admin_html.replace(
      "{{orderItemDoc.display_item_id}}",
      orderItemDoc.display_item_id
    )
    order_cancelled_by_buyer_admin_html = order_cancelled_by_buyer_admin_html.replace(
      "{{vendorOrderDoc._id}}",
      vendorOrderDoc._id
    )
    var data_admin = {
      from: "no-reply@conmate.in",
      to: adminUserDocs1[i].email,
      subject: "Conmix - Order cancelled",
      html: order_cancelled_by_buyer_admin_html,
    }
    await this.mailgunService.sendEmail(data_admin)
    }

    let s_vendor_message = `Hi ${sub_vendor_company_name}, your client ${client_name} has cancelled the order for Product Id ${orderItemDoc.display_item_id} having an Order ID ${vendorOrderDoc._id}. - conmix`
    let s_vendor_templateId = "1707164146056337625"
    await this.testService.sendSMS(
      sub_vendor_mobile_number,
      s_vendor_message,
      s_vendor_templateId
    )

    let order_cancelled_by_buyer_sub_vendor_html =
      vendor_mail_template.order_cancelled_by_buyer
    order_cancelled_by_buyer_sub_vendor_html = order_cancelled_by_buyer_sub_vendor_html.replace(
      "{{vendor_name}}",
      sub_vendor_company_name
    )
    order_cancelled_by_buyer_sub_vendor_html = order_cancelled_by_buyer_sub_vendor_html.replace(
      "{{client_name}}",
      client_name
    )
    order_cancelled_by_buyer_sub_vendor_html = order_cancelled_by_buyer_sub_vendor_html.replace(
      "{{orderItemDoc.display_item_id}}",
      orderItemDoc.display_item_id
    )
    order_cancelled_by_buyer_sub_vendor_html = order_cancelled_by_buyer_sub_vendor_html.replace(
      "{{vendorOrderDoc._id}}",
      vendorOrderDoc._id
    )
    var data_supplier = {
      from: "no-reply@conmate.in",
      to: sub_vendor_email,
      subject: "Conmix - Order cancelled",
      html: order_cancelled_by_buyer_sub_vendor_html,
    }
    await this.mailgunService.sendEmail(data_supplier)


    let order_cancelled_by_buyer_supplier_html =
    vendor_mail_template.order_cancelled_by_buyer
  order_cancelled_by_buyer_supplier_html = order_cancelled_by_buyer_supplier_html.replace(
    "{{vendor_name}}",
    master_vendor_company_name
  )
  order_cancelled_by_buyer_supplier_html = order_cancelled_by_buyer_supplier_html.replace(
    "{{client_name}}",
    client_name
  )
  order_cancelled_by_buyer_supplier_html = order_cancelled_by_buyer_supplier_html.replace(
    "{{orderItemDoc.display_item_id}}",
    orderItemDoc.display_item_id
  )
  order_cancelled_by_buyer_supplier_html = order_cancelled_by_buyer_supplier_html.replace(
    "{{vendorOrderDoc._id}}",
    vendorOrderDoc._id
  )
  var data_supplier = {
    from: "no-reply@conmate.in",
    to: master_vendor_email,
    subject: "Conmix - Order cancelled",
    html: order_cancelled_by_buyer_supplier_html,
  }
  await this.mailgunService.sendEmail(data_supplier)

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: orderItemDoc.buyer_id,
      notification_type: ClientNotificationType.partiallyOrderCancelled,
      order_id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id: vendorOrderDoc.user_id,
    })

    // Check Admin Type.
    const adminQuery: { [k: string]: any } = {}
    adminQuery.$or = [
      {
        admin_type : AdminRoles.superAdmin
      },
      {
        admin_type : AdminRoles.admin_manager
      },
      {
        admin_type : AdminRoles.admin_sales
      }
    ]
    const adminUserDocs = await AdminUserModel.find(adminQuery)
    if (adminUserDocs.length < 0) {
      return Promise.reject(
        new InvalidInput(`No Admin data were found `, 400)
      )
    }
for(let i = 0 ; i< adminUserDocs.length; i++){
  await this.notUtil.addNotificationForAdmin({
    to_user_type: UserType.ADMIN,
    to_user_id: adminUserDocs[i]._id,
    notification_type: AdminNotificationType.partiallyOrderCancelled,
    order_id,
    order_item_id: orderItemDoc.display_item_id,
    vendor_id: vendorOrderDoc.user_id,
    client_id: orderItemDoc.buyer_id,
  })
}


    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.master_vendor_id,
      notification_type: VendorNotificationType.orderCancelled,
      vendor_order_id: vendorOrderDoc._id,
      order_id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id: vendorOrderDoc.user_id,
      client_id: orderItemDoc.buyer_id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.sub_vendor_id,
      notification_type: VendorNotificationType.orderCancelled,
      vendor_order_id: vendorOrderDoc._id,
      order_id,
      order_item_id: orderItemDoc.display_item_id,
      client_id: orderItemDoc.buyer_id,
    })

    await LogisticsOrderModel.findOneAndUpdate(
      {
        buyer_order_id: new ObjectId(order_id),
        buyer_order_item_id: new ObjectId(order_item_id),
      },
      {
        $set: {
          delivery_status: LogisticsOrderStatus.CANCELLED,
          cancelled_at: Date.now(),
        },
      }
    )

    let doc = await OrderItemModel.find({
      order_id: new ObjectId(order_id),
      item_status: { $ne: OrderItemStatus.CANCELLED },
    })

    const resp: { [k: string]: any } = {}

    let amount: any = orderItemDoc.total_amount * 100
    await rzp.payments
      .refund(gateway_transaction_id, {
        amount: parseInt(amount),
      })
      .then((data1: any) => {
        console.log("data1", data1)
        resp.refund_id = data1.id
        // success
      })
      .catch((error: any) => {
        console.error(error)
        // error
      })

    await OrderItemModel.findOneAndUpdate(
      { _id: new ObjectId(order_item_id) },
      {
        $set: {
          refund_id: resp.refund_id,
        },
      }
    )

    console.log("doc", doc)
    if (doc.length <= 0) {
      await OrderModel.findOneAndUpdate(
        {
          _id: new ObjectId(order_id),
        },
        {
          $set: {
            order_status: OrderStatus.CANCELLED,
            cancelled_at: Date.now(),
          },
        }
      )

      // let message1 = `Hi ${client_name}, you have cancelled an order with Order ID ${display_id}. You may login to your Conmix account to place a new order.`
      // // Order cancelled.
      // let templateId1 = "1707163577166063544"
      // await this.testService.sendSMS(mobile_number, message1, templateId1)

      let message1 = `Hi ${client_name}, you have cancelled an entire order having an Order ID ${display_id}. You may now login to your Conmix account to place a new order.`
      // Order cancelled.
      let templateId1 = "1707163730001971730"
      await this.testService.sendSMS(mobile_number, message1, templateId1)

      let order_cancelled_by_buyer_mail_html =
      client_mail_template.order_cancelled_by_buyer
    order_cancelled_by_buyer_mail_html = order_cancelled_by_buyer_mail_html.replace(
      "{{client_name}}",
      client_name
    )
    order_cancelled_by_buyer_mail_html = order_cancelled_by_buyer_mail_html.replace(
      "{{display_id}}",
      display_id
    )
    var data1 = {
      from: "no-reply@conmate.in",
      to: email,
      subject: "Conmix - Order cancelled",
      html: order_cancelled_by_buyer_mail_html,
    }
    await this.mailgunService.sendEmail(data1)

      let site_message1 = `Hi ${site_person_name}, you have cancelled an entire order having an Order ID ${display_id}. You may now login to your Conmix account to place a new order.`
      // Order cancelled.
      let site_templateId1 = "1707163730001971730"
      await this.testService.sendSMS(
        site_mobile_number,
        site_message1,
        site_templateId1
      )

      let order_cancelled_by_buyer_site_mail_html =
      client_mail_template.order_cancelled_by_buyer
      order_cancelled_by_buyer_site_mail_html = order_cancelled_by_buyer_site_mail_html.replace(
      "{{site_person_name}}",
      client_name
    )
    order_cancelled_by_buyer_site_mail_html = order_cancelled_by_buyer_site_mail_html.replace(
      "{{display_id}}",
      display_id
    )
    var data1 = {
      from: "no-reply@conmate.in",
      to: email,
      subject: "Conmix - Order cancelled",
      html: order_cancelled_by_buyer_site_mail_html,
    }
    await this.mailgunService.sendEmail(data1)

      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: orderItemDoc.buyer_id,
        notification_type: ClientNotificationType.orderCancelled,
        order_id,
        order_item_id: orderItemDoc.display_item_id,
        vendor_id: vendorOrderDoc.user_id,
      })

      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i < adminUserDocs.length ; i++){
        await this.notUtil.addNotificationForAdmin({
          to_user_type: UserType.ADMIN,
          to_user_id: adminUserDocs[i]._id,
          notification_type: AdminNotificationType.orderCancelled,
          order_id,
          order_item_id: orderItemDoc.display_item_id,
          vendor_id: vendorOrderDoc.user_id,
          client_id: orderItemDoc.buyer_id,
        })

        let order_cancelled_by_buyer_admin_mail_html =
        admin_mail_template.order_cancelled_by_buyer_admin
      order_cancelled_by_buyer_admin_mail_html = order_cancelled_by_buyer_admin_mail_html.replace(
        "{{client_name}}",
        client_name
      )
      order_cancelled_by_buyer_admin_mail_html = order_cancelled_by_buyer_admin_mail_html.replace(
        "{{display_id}}",
        display_id
      )
      var doc1 = {
        from: "no-reply@conmate.in",
        to: adminUserDocs[i].email,
        subject: "Conmix - Order cancelled",
        html: order_cancelled_by_buyer_admin_mail_html,
      }
      await this.mailgunService.sendEmail(doc1)
      }




      // // Send notofication to Supplier.
      // await this.notUtil.addNotificationForSupplier({
      //   to_user_type: UserType.VENDOR,
      //   to_user_id: vendorOrderDoc.user_id,
      //   notification_type: VendorNotificationType.orderCancelled,
      //   vendor_order_id: vendorOrderDoc._id,
      //   vendor_id: vendorOrderDoc.user_id,
      //   order_id,
      //   order_item_id: orderItemDoc.display_item_id,
      //   client_id: orderItemDoc.buyer_id,
      // })

      // await this.notUtil.addNotificationForSupplier({
      //   to_user_type: UserType.VENDOR,
      //   to_user_id: vendorOrderDoc.sub_vendor_id,
      //   notification_type: VendorNotificationType.orderCancelled,
      //   vendor_order_id: vendorOrderDoc._id,
      //   vendor_id: vendorOrderDoc.user_id,
      //   order_id,
      //   order_item_id: orderItemDoc.display_item_id,
      //   client_id: orderItemDoc.buyer_id,
      // })
    }

    return Promise.resolve()
  }

  async addOrderPart(
    user_id: string,
    orderId: string,
    orderItemId: string,
    partData: IAddOrderPart
  ) {
    let orderItemData = await OrderItemModel.findOne({
      _id: new ObjectId(orderItemId),
    })

    if (isNullOrUndefined(orderItemData)) {
      return Promise.reject(
        new InvalidInput(`No Order item doc were found `, 400)
      )
    }

    if (orderItemData.part_quantity < partData.assigned_quantity) {
      return Promise.reject(
        new InvalidInput(`You assigned all your quantity.`, 400)
      )
    }

    let buyerUserDoc = await BuyerUserModel.findOne({
      _id: new ObjectId(user_id),
    })

    if (isNullOrUndefined(buyerUserDoc)) {
      return Promise.reject(
        new InvalidInput(`No Buyer order doc were found `, 400)
      )
    }

    let client_name: any
    let client_mobile_number: any
    if (!isNullOrUndefined(buyerUserDoc)) {
      client_mobile_number = buyerUserDoc.mobile_number
      if (buyerUserDoc.account_type === "Individual") {
        client_name = buyerUserDoc.full_name
      } else {
        client_name = buyerUserDoc.company_name
      }
    }

    partData.buyer_order_id = orderId
    partData.buyer_id = user_id
    partData.temp_quantity = partData.assigned_quantity
    partData.vendor_id = orderItemData.vendor_id
    partData.order_item_id = orderItemId

    await new OrderItemPartModel(partData).save()

    await OrderItemModel.updateOne(
      { _id: new ObjectId(orderItemId) },
      {
        $set: {
          part_quantity:
            orderItemData.part_quantity - partData.assigned_quantity,
        },
      }
    )

    let data = await OrderItemModel.findOne({
      _id: new ObjectId(orderItemId),
    })

    if (isNullOrUndefined(data)) {
      return Promise.reject(new InvalidInput(`No data available`, 400))
    }
    let orderItem = await OrderItemModel.findOne({
      _id: new ObjectId(orderItemId),
    })
    if (isNullOrUndefined(orderItem)) {
      return Promise.reject(new InvalidInput(`No item data available`, 400))
    }

    let vendorOrderDoc = await VendorOrderModel.findOne({
      buyer_id: new ObjectId(user_id),
      buyer_order_id: new ObjectId(orderId),
      buyer_order_item_id: new ObjectId(orderItemId),
    })

    if (isNullOrUndefined(vendorOrderDoc)) {
      return Promise.reject(
        new InvalidInput(`No vendor order data available`, 400)
      )
    }

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderItemData.master_vendor_id,
      notification_type: VendorNotificationType.assignQtyByBuyer,
      order_id: orderId,
      order_item_id: orderItem.display_item_id,
      revised_quantity: orderItem.revised_quantity,
      client_id: buyerUserDoc._id,
      assigned_quantity: partData.assigned_quantity,
      vendor_order_id: vendorOrderDoc._id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderItemData.sub_vendor_id,
      notification_type: VendorNotificationType.assignQtyByBuyer,
      order_id: orderId,
      order_item_id: orderItem.display_item_id,
      revised_quantity: orderItem.revised_quantity,
      client_id: buyerUserDoc._id,
      assigned_quantity: partData.assigned_quantity,
      vendor_order_id: vendorOrderDoc._id,
    })
    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let order_display_id: any
    let orderInfo = await OrderModel.findOne({
      _id: new ObjectId(orderId),
    })
    if (!isNullOrUndefined(orderInfo)) {
      order_display_id = orderInfo.display_id
      let siteInfo = await SiteModel.findOne({
        _id: new ObjectId(orderInfo.site_id),
      })

      if (!isNullOrUndefined(siteInfo)) {
        site_name = siteInfo.site_name
        site_person_name = siteInfo.person_name
        site_mobile_number = siteInfo.mobile_number
      }
    }

    let mastervendorDoc = await VendorUserModel.findOne({
      _id: new ObjectId(orderItemData.master_vendor_id),
    })

    if (isNullOrUndefined(mastervendorDoc)) {
      return Promise.reject(
        new InvalidInput(`No vendor user data available`, 400)
      )
    }

    //let master_vendor_name = mastervendorDoc.full_name
    let master_mobile_number = mastervendorDoc.mobile_number
    let master_vendor_company_name = mastervendorDoc.company_name

    let subvendorDoc = await VendorUserModel.findOne({
      _id: new ObjectId(orderItemData.sub_vendor_id),
    })

    if (isNullOrUndefined(subvendorDoc)) {
      return Promise.reject(
        new InvalidInput(`No vendor user data available`, 400)
      )
    }

    //let sub_vendor_name = subvendorDoc.full_name
    let sub_mobile_number = subvendorDoc.mobile_number
    let sub_vendor_company_name = subvendorDoc.company_name

    let message = `Hi ${client_name}, you have assigned RMC Qty ${partData.assigned_quantity} cum for your ${site_name} site with product id ${orderItem.display_item_id} having an order id ${order_display_id}. You may now login to your Conmix account to place a new order.`
    // Assigned qty
    let templateId = "1707163774001394109"
    await this.testService.sendSMS(client_mobile_number, message, templateId)

    let site_message = `Hi ${site_person_name}, you have assigned RMC Qty ${partData.assigned_quantity} cum for your ${site_name} site with product id ${orderItem.display_item_id} having an order id ${order_display_id}. You may now login to your Conmix account to place a new order.`
    // Assigned qty
    let site_templateId = "1707163774001394109"
    await this.testService.sendSMS(
      site_mobile_number,
      site_message,
      site_templateId
    )

    let m_vendor_message = `Hi ${master_vendor_company_name} your client ${client_name} has assigned the RMC Qty ${partData.assigned_quantity} cum for the product id ${orderItem.display_item_id} having an order id ${order_display_id} for delivery at its ${site_name} site - conmix.`
    let m_vendor_templateId = "1707163810576193483"
    await this.testService.sendSMS(
      master_mobile_number,
      m_vendor_message,
      m_vendor_templateId
    )

    let s_vendor_message = `Hi ${sub_vendor_company_name} your client ${client_name} has assigned the RMC Qty ${partData.assigned_quantity} cum for the product id ${orderItem.display_item_id} having an order id ${order_display_id} for delivery at its ${site_name} site - conmix.`
    let s_vendor_templateId = "1707163810576193483"
    await this.testService.sendSMS(
      sub_mobile_number,
      s_vendor_message,
      s_vendor_templateId
    )

    if (data.part_quantity < 3 && data.part_quantity > 0) {
      await OrderItemModel.updateOne(
        { _id: new ObjectId(orderItemId) },
        {
          $set: {
            revised_quantity: data.quantity - data.part_quantity,
            part_quantity: 0,
            remaining_quantity: data.remaining_quantity - data.part_quantity,
          },
        }
      )

      await VendorOrderModel.updateOne(
        { buyer_order_item_id: new ObjectId(orderItemId) },
        {
          $set: {
            revised_quantity: data.quantity - data.part_quantity,
          },
        }
      )

      let orderItem = await OrderItemModel.findOne({
        _id: new ObjectId(orderItemId),
      })
      if (isNullOrUndefined(orderItem)) {
        return Promise.reject(new InvalidInput(`No item data available`, 400))
      }
      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: buyerUserDoc._id,
        notification_type: ClientNotificationType.orderShortClose,
        order_id: orderId,
        order_item_id: orderItem.display_item_id,
        revised_quantity: orderItem.revised_quantity,
        vendor_id: orderItemData.vendor_id,
        vendor_order_id: vendorOrderDoc._id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: orderItemData.master_vendor_id,
        notification_type: VendorNotificationType.orderShortClose,
        order_id: orderId,
        order_item_id: orderItem.display_item_id,
        revised_quantity: orderItem.revised_quantity,
        client_id: buyerUserDoc._id,
        vendor_order_id: vendorOrderDoc._id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: orderItemData.sub_vendor_id,
        notification_type: VendorNotificationType.orderShortClose,
        order_id: orderId,
        order_item_id: orderItem.display_item_id,
        revised_quantity: orderItem.revised_quantity,
        client_id: buyerUserDoc._id,
        vendor_order_id: vendorOrderDoc._id,
      })

     // Check Admin Type.
     const adminQuery: { [k: string]: any } = {}
     adminQuery.$or = [
       {
         admin_type : AdminRoles.admin_manager
       },
       {
         admin_type : AdminRoles.admin_sales
       },
     ]
     const adminUserDocs = await AdminUserModel.find(adminQuery)
     if (adminUserDocs.length < 0) {
       return Promise.reject(
         new InvalidInput(`No Admin data were found `, 400)
       )
     }

     for(let i = 0 ; i< adminUserDocs.length; i ++){  
      await this.notUtil.addNotificationForAdmin({
        to_user_type: UserType.ADMIN,
        to_user_id: adminUserDocs[i]._id,
        notification_type: AdminNotificationType.orderShortClose,
        order_id: orderId,
        order_item_id: orderItem.display_item_id,
        revised_quantity: orderItem.revised_quantity,
        client_id: buyerUserDoc._id,
        vendor_id: orderItemData.vendor_id,
        vendor_order_id: vendorOrderDoc._id,
      })
    }

      let message = `Hi ${client_name}, your product id ${orderItem.display_item_id} having an order id ${order_display_id} has been shortclosed by the RMC supplier ${sub_vendor_company_name} as the balance quantity is less than 3 cum. - conmix`
      // Assigned qty
      let templateId = "1707163843637840299"
      await this.testService.sendSMS(client_mobile_number, message, templateId)

      let site_message = `Hi ${site_person_name}, your product id ${orderItem.display_item_id} having an order id ${order_display_id} has been shortclosed by the RMC supplier ${sub_vendor_company_name} as the balance quantity is less than 3 cum. - conmix`
      // Assigned qty
      let site_templateId = "1707163843637840299"
      await this.testService.sendSMS(
        site_mobile_number,
        site_message,
        site_templateId
      )

      let m_vendor_message = `Hi ${master_vendor_company_name}, your supply order with product id ${orderItem.display_item_id} having an order id ${vendorOrderDoc._id} for your client ${client_name} has been shortclosed by the Admin as the balance quantity is less than 3 cum. - conmix`
      let m_vendor_templateId = "1707163843644549739"
      await this.testService.sendSMS(
        master_mobile_number,
        m_vendor_message,
        m_vendor_templateId
      )

      let s_vendor_message = `Hi ${sub_vendor_company_name}, your supply order with product id ${orderItem.display_item_id} having an order id ${vendorOrderDoc._id} for your client ${client_name} has been shortclosed by the Admin as the balance quantity is less than 3 cum. - conmix`
      let s_vendor_templateId = "1707163843644549739"
      await this.testService.sendSMS(
        sub_mobile_number,
        s_vendor_message,
        s_vendor_templateId
      )
    }

    return Promise.resolve()
  }

  async CheckTMAvailbility(
    buyer_id: string,
    start_time: string,
    end_time: string,
    vendor_id: string
  ) {
    let unavailbleData = await TM_unModel.find({
      vendor_id,
    })

    console.log("unavailbleData", unavailbleData)
    let temp: any[] = []
    if (unavailbleData.length > 0) {
      // time of first timespan
      for (let k = 0; k < unavailbleData.length; k++) {
        var x = new Date(start_time).getTime()
        var y = new Date(end_time).getTime()
        // time of second timespan
        var a = new Date(unavailbleData[k].start_time).getTime()
        var b = new Date(unavailbleData[k].end_time).getTime()
        if (
          Math.min(x, y) < Math.max(a, b) &&
          Math.max(x, y) > Math.min(a, b)
        ) {
          console.log("heree")
          temp.push(x)
        }
      }
      console.log("temp", temp.length)
      if (temp.length > 0) {
        return Promise.reject(
          new InvalidInput(
            `Sorry, your request for Transit Mixture is not available on selected date and time.`,
            400
          )
        )
      } else {
        return true
      }
    } else {
      return true
    }
  }
}
