import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode } from "../../utilities/config"

export interface IGetOrder {
  user_id?: string
  before?: string
  after?: string
  order_status?: string
  payment_status?: string
  gateway_transaction_id?: string
  site_id?: string
  month?: string
  year?: number
  date: Date
  total_amount?: number
  total_amount_range: string
  start_date?: string
  end_date?: string
}

export interface IGetOrderRequest extends IAuthenticatedRequest {
  query: IGetOrder
}
export interface IPlaceOrderRequest extends IAuthenticatedRequest {
  params: {
    cartId?: string
    gateway_transaction_id: string
    [k: string]: any
  }
  // query: {
  //   with_TM: boolean
  //   with_CP: boolean
  //   delivery_date: Date
  // }
}

export const getOrder: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    order_status: Joi.string(),
    payment_status: Joi.string(),
    gateway_transaction_id: Joi.string(),
    site_id: Joi.string().regex(objectIdRegex),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
    total_amount: Joi.number(),
    total_amount_range: Joi.string(),
  }),
}

export interface IGetOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const getOrderById: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
}

export const placeOrderSchema: IRequestSchema = {
  params: Joi.object().keys({
    cartId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    gateway_transaction_id: Joi.string(),
  }),
  // query: Joi.object().keys({
  //   with_TM: Joi.boolean().required(),
  //   with_CP: Joi.boolean().required(),
  //   delivery_date: Joi.date().required(),
  // }),
}

export interface ICancelOrderItemRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    order_id: string
  }
}

export const cancelOrderItemSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex).required(),
    order_id: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    gateway_transaction_id: Joi.string(),
  }),
}

export interface IAddOrderPart {
  assigned_quantity: number
  assigned_date: Date
  start_time: string
  end_time: string
  [k: string]: any
}

export interface IAddOrderPartRequest extends IAuthenticatedRequest {
  body: IAddOrderPart
  params: {
    order_id: string
    order_item_id: string
  }
}

export const addOrderPartSchema: IRequestSchema = {
  body: Joi.object().keys({
    assigned_quantity: Joi.number().required(),
    // assigned_date: Joi.date().iso().required(),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
  }),
  params: Joi.object().keys({
    order_id: Joi.string().required(),
    order_item_id: Joi.string().required().regex(objectIdRegex),
  }),
}

export interface ICheckTMAvailbility {
  start_time: string
  end_time: string
  [k: string]: any
}

export interface ICheckTMAvailbilityRequest extends IAuthenticatedRequest {
  body: ICheckTMAvailbility
}

export const checkTMAvailbilitySchema: IRequestSchema = {
  body: Joi.object().keys({
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
    vendor_id: Joi.string().required().regex(objectIdRegex),
  }),
}
