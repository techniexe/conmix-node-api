import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvoiceModel } from "../../model/invoice.model"
import { ObjectId } from "bson"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { logger } from "../../logger"
import { MailService } from "../../utilities/mail-service"
import { BuyerTypes } from "../buyer.types"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
export class InvoiceRepository {
  constructor(
    @inject(BuyerTypes.MailService) private mailService: MailService
  ) {}

  async getInvoice(
    user_id: string,
    orderId: string,
    site_id?: string,
    product_subcategory_id?: string,
    start_date?: string,
    end_date?: string
  ) {
    const query: { [k: string]: any } = {}

    query.user_id = user_id
    query.order_id = orderId

    if (!isNullOrUndefined(site_id)) {
      query.site_id = site_id
    }
    if (!isNullOrUndefined(product_subcategory_id)) {
      query["items.sub_category_id"] = new ObjectId(product_subcategory_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.created_at = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.created_at = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.created_at = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    return await InvoiceModel.find(query).sort({ created_at: -1 }).limit(10)
  }

  async sentInvoiceMail(user_id: string, invoiceId: string) {
    const userData = await BuyerUserModel.findOne({ _id: user_id })
    const invoiceData = await InvoiceModel.findOne({
      _id: invoiceId,
    })

    if (isNullOrUndefined(userData)) {
      const err = new UnexpectedInput(`User could not be found`)
      err.httpStatusCode = 400
      return Promise.reject(err)
      //return Promise.reject(new Error(`User could not be found`))
    }
    if (isNullOrUndefined(invoiceData)) {
      const err = new UnexpectedInput(`Invoice could not be found `)
      err.httpStatusCode = 400
      return Promise.reject(err)
      // return Promise.reject(new Error(`Invoice could not be found `))
    }
    if (isNullOrUndefined(userData.email)) {
      const err = new UnexpectedInput(`We couldn't find the email Id you entered `)
      err.httpStatusCode = 400
      return Promise.reject(err)
      //return Promise.reject(new Error(`We couldn't find the email Id you entered `))
    }

    let templateData = {
      html: {
        from_user: "from_user:  " + `Admin`,
        from_address:
          "from_address:  " +
          `104, Sumel-­2, S.G.Highway Road, Beside Gurudwara Temple, Thaltej, Ahmedabad, Gujarat 380054`,
        from_phone_number: "from_phone_number:  " + `(079) 123-456`,
        from_email: "from_email:  " + `info@text.com`,
        to_user: "to_user:  " + userData.full_name,
        to_address:
          "to_address:  " +
          `${invoiceData.address_line1}
          ${invoiceData.address_line2}
          ${invoiceData.stateDetails.state_name}
          ${invoiceData.cityDetails.city_name}
          ${invoiceData.subCityDetails.sub_city_name}`,
        to_phone_number: "to_phone_number:  " + userData.mobile_number,
        to_email: "to_email:  " + userData.email,
        orderId: "orderId:  " + invoiceData.order_id,
        gstNo: "gstNo:  " + userData.gst_number,
        PanNo: "PanNo:  " + userData.pan_number,
        orderDate: "OrderDate:  " + invoiceData.created_at,
        items: invoiceData.items,
        totalPrice: "TotalPrice:   " + invoiceData.total_price,
      },
    }
    const emailTemplt = await this.mailService.getRenderedTemplate(
      "new_invoice",
      templateData
    )
    let mailData = emailTemplt.parsedHtml
    let stream = await this.mailService.generatepdf(mailData)

    try {
      await this.mailService.sendMailUsingTemplate(
        "new_invoice",
        templateData,
        { full_name: userData.full_name, email: userData.email },
        { filename: `invoice.pdf`, content: stream }
      )
    } catch (e) {
      logger.fatal("Unable to send message:", e)
      return Promise.reject(
        new Error(`Unable to send email. Please try again later.`)
      )
    }
  }
}
