import { injectable, inject } from "inversify"
import { controller, httpGet, httpPost } from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { BuyerTypes } from "../buyer.types"
import {
  IGetInvoiceRequest,
  getInvoice,
  sentInvoiceMail,
  ISentInvoiceMailRequest,
} from "./buyer.invoice.req-schema"
import { InvoiceRepository } from "./buyer.invoice.repository"

@injectable()
@controller("/invoice", verifyCustomToken("buyer"))
export class InvoiceController {
  constructor(
    @inject(BuyerTypes.InvoiceRepository) private invoiceRepo: InvoiceRepository
  ) {}

  @httpGet("/:orderId", validate(getInvoice))
  async getInvoice(req: IGetInvoiceRequest, res: Response, next: NextFunction) {
    try {
      const { orderId } = req.params
      const {
        site_id,
        product_subcategory_id,
        start_date,
        end_date,
      } = req.query

      const data = await this.invoiceRepo.getInvoice(
        req.user.uid,
        orderId,
        site_id,
        product_subcategory_id,
        start_date,
        end_date
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/:InvoiceId", validate(sentInvoiceMail))
  async sentInvoiceMail(
    req: ISentInvoiceMailRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.invoiceRepo.sentInvoiceMail(
      req.user.uid,
      req.params.InvoiceId
    )
    res.json({ data })
    try {
      //
    } catch (err) {
      next(err)
    }
  }
}
