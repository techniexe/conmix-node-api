import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetInvoice {
  site_id?: string
  product_subcategory_id?: string
  start_date?: string
  end_date?: string
}

export interface IGetInvoiceRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
  query: IGetInvoice
}

export const getInvoice: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
  query: Joi.object().keys({
    site_id: Joi.string().regex(objectIdRegex),
    product_subcategory_id: Joi.string().regex(objectIdRegex),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface ISentInvoiceMailRequest extends IAuthenticatedRequest {
  params: {
    InvoiceId: string
  }
}

export const sentInvoiceMail: IRequestSchema = {
  params: Joi.object().keys({
    InvoiceId: Joi.string(),
  }),
}
