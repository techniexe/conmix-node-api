import { injectable } from "inversify"
import { IWishlist } from "./buyer.wishlist.request-definations"
import { WishlistModel, IWishlistDoc } from "../../model/wishlist.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { minOrder } from "../../utilities/config"
import { getMarginRate } from "../../utilities/rate.utility"
import { getDistance, distanceFactor } from "../../utilities/distance.utilities"
import { VehicleRateModel } from "../../model/vehicle_rate.model"

@injectable()
export class WishlistRepository {
  async addToWishlist(user_id: string, wishlistData: IWishlist) {
    wishlistData.user_id = user_id
    if (
      !isNullOrUndefined(wishlistData.user_id) &&
      !isNullOrUndefined(wishlistData.product_id)
    ) {
      await WishlistModel.updateOne(
        {
          user_id: wishlistData.user_id,
          product_id: wishlistData.product_id,
        },
        {
          $inc: { quantity: wishlistData.quantity },
          $set: { created_at: Date.now() },
        },
        { upsert: true }
      )
      return Promise.resolve()
    }

    wishlistData.created_at = new Date()
    const newWishlist = new WishlistModel(wishlistData)
    const newwishlsitDoc = await newWishlist.save()
    return Promise.resolve(newwishlsitDoc._id)
  }

  async getWishlist(
    user_id: string,
    state_id: string,
    long: number,
    lat: number
  ): Promise<IWishlistDoc[] | null> {
    let query: { [k: string]: any } = { user_id: new ObjectId(user_id) }
    const sort = { created_at: -1 }

    let dvlLocation = { type: "Point", coordinates: [0, 0] }
    if (!isNullOrUndefined(long) && !isNullOrUndefined(lat)) {
      dvlLocation.coordinates = [long, lat]
    }
    if (dvlLocation.coordinates[0] === 0) {
      return Promise.reject(new InvalidInput(`Sorry, location reference could not be found or no longer exists `, 400))
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "product",
          localField: "product_id",
          foreignField: "_id",
          as: "product",
        },
      },
      {
        $unwind: { path: "$product", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "supplier",
          localField: "product.user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier" },
      },
      {
        $lookup: {
          from: "product_category",
          localField: "product.category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "product.sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "address",
          localField: "product.pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $lookup: {
          from: "gst_slab",
          localField: "productSubCategory.gst_slab_id",
          foreignField: "_id",
          as: "gst_slab",
        },
      },
      {
        $lookup: {
          from: "margin_rate",
          localField: "productSubCategory.margin_rate_id",
          foreignField: "_id",
          as: "margin_rate",
        },
      },
      {
        $unwind: { path: "$gst_slab" },
      },
      {
        $unwind: { path: "$margin_rate" },
      },
      {
        $project: {
          _id: 1,
          product_id: 1,
          quantity: 1,
          created_at: 1,
          "product._id": 1,
          "product.name": 1,
          "product.media": 1,
          "product.category_id": 1,
          "product.sub_category_id": 1,
          "product.quantity": 1,
          "product.unit_price": 1,
          "product.rating_sum": 1,
          "product.rating_count": 1,
          "product.rating": 1,
          "product.pickup_location": 1,
          "product.calculated_distance": 1,
          "supplier._id": 1,
          "supplier.full_name": 1,
          "productCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "productSubCategory.selling_unit": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "gst_slab._id": 1,
          "gst_slab.title": 1,
          "gst_slab.igst_rate": 1,
          "gst_slab.cgst_rate": 1,
          "gst_slab.sgst_rate": 1,
          "margin_rate._id": 1,
          "margin_rate.title": 1,
          "margin_rate.slab": 1,
        },
      },
    ]
    const wishlistData = await WishlistModel.aggregate(aggregateArr)

    for (let i = 0; i < wishlistData.length; i++) {
      let distance =
        getDistance(
          dvlLocation.coordinates,
          wishlistData[i].product.pickup_location.coordinates
        ) * distanceFactor

      let rateDoc = await VehicleRateModel.findOne({
        state_id: new ObjectId(state_id),
      })

      if (!isNullOrUndefined(rateDoc)) {
        let per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
        wishlistData[i].product.logistics_price =
          distance * per_metric_ton_per_km_rate
      }
      // } else {
      //   return Promise.reject(
      //     new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
      //   )
      // }

      const baseCost = wishlistData[i].product.unit_price
      const marginRate = getMarginRate(
        baseCost,
        wishlistData[i].margin_rate.slab,
        true
      )
      const marginAmount = (baseCost * marginRate) / 100
      /** For refrence. */
      wishlistData[i].product.marginAmount = marginAmount
      /** This value is from config. */
      wishlistData[i].product.minimum_order = minOrder
      wishlistData[i].product.price_with_margin_and_logistics =
        wishlistData[i].product.unit_price +
        marginAmount +
        wishlistData[i].product.logistics_price
    }

    if (wishlistData.length < 0) {
      return Promise.reject(new InvalidInput(`No Wishlist data found.`, 400))
    }

    return Promise.resolve(wishlistData)
  }

  async removeFromWishlist(product_id: string, user_id: string) {
    const status = await WishlistModel.deleteOne({
      product_id: new ObjectId(product_id),
      user_id: new ObjectId(user_id),
    })

    console.log(product_id)
    console.log(user_id)
    console.log(status)
    if (status.n !== 1) {
      return Promise.reject(new InvalidInput(`No Wishlist data found.`, 400))
    }

    //return Promise.resolve()
  }

  async editWishlist(user_id: string, wishlisted_id: string, quantity: number) {
    const res = await WishlistModel.updateOne(
      {
        user_id,
        _id: wishlisted_id,
      },
      {
        $set: { quantity },
      }
    )
    if (res.n !== 1) {
      const err = new UnexpectedInput(`This subCategory doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    return Promise.resolve()
  }
}
