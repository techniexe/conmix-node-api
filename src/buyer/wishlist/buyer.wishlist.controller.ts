import {
  controller,
  httpPost,
  httpGet,
  httpDelete,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { BuyerTypes } from "../buyer.types"
import { WishlistRepository } from "./buyer.wishlist.repository"
import { validate } from "../../middleware/joi.middleware"
import { Response, NextFunction } from "express"
import {
  addWishlistSchema,
  editWishlistSchema,
  deleteWishlistSchema,
} from "./buyer.wishlist.request-schema"
import {
  IAddWishlistRequest,
  IRemoveFromWishlistReq,
  IEditWishlistReq,
} from "./buyer.wishlist.request-definations"

@controller("/wishlist", verifyCustomToken("buyer"))
@injectable()
export class WishlistController {
  constructor(
    @inject(BuyerTypes.WishlistRepository)
    private wishlistRepo: WishlistRepository
  ) {}

  @httpPost("/", validate(addWishlistSchema))
  async addToWishlist(
    req: IAddWishlistRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.wishlistRepo.addToWishlist(uid, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getWishlist(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { state_id, long, lat } = req.query
      const data = await this.wishlistRepo.getWishlist(uid, state_id, long, lat)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpDelete("/:product_id", validate(deleteWishlistSchema))
  async removeFromWishlist(
    req: IRemoveFromWishlistReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.wishlistRepo.removeFromWishlist(
        req.params.product_id,
        req.user.uid
      )
      res.sendStatus(204)
    } catch (err) {
      // if (err.message === "doesn't exists") {
      //   return res.status(422).json({
      //     error: {
      //       message: "Invalid Request",
      //       details: [{ path: "wishlistedId", message: err.message }],
      //     },
      //   })
      // }
      next(err)
    }
  }

  @httpPatch("/:wishlistedId", validate(editWishlistSchema))
  async editWishlist(req: IEditWishlistReq, res: Response, next: NextFunction) {
    try {
      await this.wishlistRepo.editWishlist(
        req.user.uid,
        req.params.wishlistedId,
        req.body.quantity
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
