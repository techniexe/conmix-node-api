import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export const addWishlistSchema: IRequestSchema = {
  body: Joi.object().keys({
    product_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export const editWishlistSchema: IRequestSchema = {
  body: Joi.object().keys({
    quantity: Joi.number().min(1).required(),
  }),
  params: Joi.object().keys({
    wishlistedId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const deleteWishlistSchema: IRequestSchema = {
  params: Joi.object().keys({
    product_id: Joi.string().regex(objectIdRegex).required(),
  }),
}
