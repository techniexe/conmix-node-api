import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IWishlist {
  user_id?: string
  product_id: string
  quantity: number
  created_at: Date
}

export interface IEditWishlist {
  quantity: number
}

export interface IAddWishlistRequest extends IAuthenticatedRequest {
  body: IWishlist
}

export interface IRemoveFromWishlistReq extends IAuthenticatedRequest {
  params: { product_id: string }
}

export interface IEditWishlistReq extends IAuthenticatedRequest {
  params: { wishlistedId: string }
  body: IEditWishlist
}
