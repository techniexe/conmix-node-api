import "reflect-metadata"
import { Container } from "inversify"
import { TYPE } from "inversify-express-utils"
import { BuyerTypes } from "./buyer.types"
import { SiteController } from "./site/buyer.site.controller"
import { SiteRepository } from "./site/buyer.site.repository"
import { BuyerUserController } from "./user/buyer.user.controller"
import { BuyerUserRepository } from "./user/buyer.user.repository"
import { BuyerAuthRepository } from "./auth/buyer.auth.repository"
import { BuyerAuthController } from "./auth/buyer.auth.controller"
import { TwilioService } from "../utilities/twilio-service"
import { MailService } from "../utilities/mail-service"
import { BuyerProductController } from "./product/buyer.product.controller"
import { BuyerProductRepository } from "./product/buyer.product.repository"
import { WishlistController } from "./wishlist/buyer.wishlist.controller"
import { WishlistRepository } from "./wishlist/buyer.wishlist.repository"
import { CardController } from "./card/buyer.card.controller"
import { CardRepository } from "./card/buyer.card.repository"
//import { EmailTemplateRepository } from "../admin/email-template/email.template.repository"
import { CommonService } from "../utilities/common.service"
import { ComplaintController } from "./complaint/buyer.complaint.controller"
import { ComplaintRepository } from "./complaint/buyer.complaint.repository"
import { EmailTemplateRepository } from "../admin/email-template/email.template.repository"
import { OrderController } from "./order/buyer.order.controller"
import { OrderRepository } from "./order/buyer.order.repository"
import { InvoiceController } from "./invoice/buyer.invoice.controller"
import { InvoiceRepository } from "./invoice/buyer.invoice.repository"
import { OrderTrackController } from "./order_track/buyer.order_track.controller"
import { OrderTrackRepository } from "./order_track/buyer.order_track.repository"
import { UniqueidService } from "../utilities/uniqueid-service"
//import { CartRepository } from "../common/cart/cart.repository"
import { SnsService } from "../utilities/sns.service"
import { SupportTicketController } from "./support_ticket/buyer.support_ticket.controller"
import { SupportTicketRepository } from "./support_ticket/buyer.support_ticket.repository"
import { NotificationController } from "./notification/buyer.notification.controller"
import { NotificationRepository } from "./notification/buyer.notification.repository"
import { NotificationUtility } from "../utilities/notification.utility"
import { FcmTokenUtility } from "../utilities/fcm-token.utilities"
import { FcmTokensRepository } from "./fcm-tokens/fcm-tokens.repository"
import { FcmTokensController } from "./fcm-tokens/fcm-tokens.controller"
import { CartController } from "./cart/cart.controller"
import { CartRepository } from "./cart/cart.repository"
import { ReviewController } from "./review/buyer.review.controller"
import { ReviewRepository } from "./review/buyer.review.repository"
import { BillingAddressController } from "./billing_address/buyer.billing_address.controller"
import { BillingAddressRepository } from "./billing_address/buyer.billing_address.repository"
import { TestService } from "../utilities/test.service"
import { MailGunService } from "../utilities/mailgun.service"
// import { BuyerSourceController } from "./source/buyer.source.controller"
// import { BuyerSourceRepository } from "./source/buyer.source.repository"

export const buyerContainer = new Container()

buyerContainer
  .bind(BuyerTypes.TwilioService)
  .to(TwilioService)
  .inSingletonScope()

buyerContainer.bind(BuyerTypes.MailService).to(MailService).inSingletonScope()

buyerContainer.bind(BuyerTypes.mailgunService).to(MailGunService).inSingletonScope()

buyerContainer
  .bind(BuyerTypes.TestService)
  .to(TestService)
  .inSingletonScope()

buyerContainer
  .bind(BuyerTypes.EmailTemplateRepository)
  .to(EmailTemplateRepository)
  .inSingletonScope()

buyerContainer
  .bind(BuyerTypes.CommonService)
  .to(CommonService)
  .inSingletonScope()

buyerContainer
  .bind(BuyerTypes.UniqueidService)
  .to(UniqueidService)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(SiteController)
  .whenTargetNamed(BuyerTypes.SiteController)

buyerContainer
  .bind(BuyerTypes.SiteRepository)
  .to(SiteRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(BuyerUserController)
  .whenTargetNamed(BuyerTypes.BuyerUserController)

buyerContainer
  .bind(BuyerTypes.BuyerUserRepository)
  .to(BuyerUserRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(BuyerAuthController)
  .whenTargetNamed(BuyerTypes.BuyerAuthController)
buyerContainer
  .bind(BuyerTypes.BuyerAuthRepository)
  .to(BuyerAuthRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(BuyerProductController)
  .whenTargetNamed(BuyerTypes.BuyerProductController)
buyerContainer
  .bind(BuyerTypes.BuyerProductRepository)
  .to(BuyerProductRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(WishlistController)
  .whenTargetNamed(BuyerTypes.WishlistController)
buyerContainer
  .bind(BuyerTypes.WishlistRepository)
  .to(WishlistRepository)
  .inSingletonScope()
buyerContainer
  .bind(TYPE.Controller)
  .to(CardController)
  .whenTargetNamed(BuyerTypes.CardController)
buyerContainer
  .bind(BuyerTypes.CardRepository)
  .to(CardRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(ComplaintController)
  .whenTargetNamed(BuyerTypes.ComplaintController)
buyerContainer
  .bind(BuyerTypes.ComplaintRepository)
  .to(ComplaintRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(OrderController)
  .whenTargetNamed(BuyerTypes.OrderController)
buyerContainer
  .bind(BuyerTypes.OrderRepository)
  .to(OrderRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(InvoiceController)
  .whenTargetNamed(BuyerTypes.InvoiceController)
buyerContainer
  .bind(BuyerTypes.InvoiceRepository)
  .to(InvoiceRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(OrderTrackController)
  .whenTargetNamed(BuyerTypes.OrderTrackController)

buyerContainer
  .bind(BuyerTypes.OrderTrackRepository)
  .to(OrderTrackRepository)
  .inSingletonScope()

// buyerContainer
//   .bind(BuyerTypes.CartRepository)
//   .to(CartRepository)
//   .inSingletonScope()

buyerContainer.bind(BuyerTypes.SnsService).to(SnsService).inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(SupportTicketController)
  .whenTargetNamed(BuyerTypes.SupportTicketController)
buyerContainer
  .bind(BuyerTypes.SupportTicketRepository)
  .to(SupportTicketRepository)
  .inSingletonScope()

buyerContainer
  .bind(BuyerTypes.FcmTokenUtility)
  .to(FcmTokenUtility)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(NotificationController)
  .whenTargetNamed(BuyerTypes.NotificationController)
buyerContainer
  .bind(BuyerTypes.NotificationRepository)
  .to(NotificationRepository)
  .inSingletonScope()
buyerContainer
  .bind(BuyerTypes.NotificationUtility)
  .to(NotificationUtility)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(FcmTokensController)
  .whenTargetNamed(BuyerTypes.FcmTokensController)
buyerContainer
  .bind(BuyerTypes.FcmTokensRepository)
  .to(FcmTokensRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(CartController)
  .whenTargetNamed(BuyerTypes.CartController)
buyerContainer
  .bind(BuyerTypes.CartRepository)
  .to(CartRepository)
  .inSingletonScope()

buyerContainer
  .bind(TYPE.Controller)
  .to(ReviewController)
  .whenTargetNamed(BuyerTypes.ReviewController)
buyerContainer
  .bind(BuyerTypes.ReviewRepository)
  .to(ReviewRepository)
  .inSingletonScope()

  buyerContainer
  .bind(TYPE.Controller)
  .to(BillingAddressController)
  .whenTargetNamed(BuyerTypes.BillingAddressController)

  buyerContainer
  .bind(BuyerTypes.BillingAddressRepository)
  .to(BillingAddressRepository)
  .inSingletonScope()

// buyerContainer
//   .bind(TYPE.Controller)
//   .to(BuyerSourceController)
//   .whenTargetNamed(BuyerTypes.BuyerSourceController)

// buyerContainer
//   .bind(BuyerTypes.BuyerSourceRepository)
//   .to(BuyerSourceRepository)
//   .inSingletonScope()
