import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { BuyerTypes } from "../buyer.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import {
  createSupportTicketSchema,
  ICreateSupportTicketRequest,
  changeSupportTicketStatusSchema,
  IChangeSupportTicketRequest,
  replySupportTicketSchema,
  IReplySupportTicketRequest,
  getSupportTicketSchema,
  getSupportTicketInfoSchema,
  IGetSupportTicketInfoRequest,
  getReplyOfSupportTicketSchema,
  IGetReplyOfTicketRequest,
} from "./buyer.support_ticket.schema"
import { SupportTicketRepository } from "./buyer.support_ticket.repository"

@injectable()
@controller("/support_ticket", verifyCustomToken("buyer"))
export class SupportTicketController {
  constructor(
    @inject(BuyerTypes.SupportTicketRepository)
    private supportTicketRepo: SupportTicketRepository
  ) {}

  /**
   * Create Support ticket.
   */
  @httpPost("/", validate(createSupportTicketSchema))
  async createSupportTicket(
    req: ICreateSupportTicketRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.supportTicketRepo.createSupportTicket(uid, req.body, req.files)
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /**
 *  Change support ticket status.
 */
  @httpPatch("/:ticketId", validate(changeSupportTicketStatusSchema))
  async changeSupportTicketStatus(
    req: IChangeSupportTicketRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { ticketId } = req.params
      const { uid } = req.user
      await this.supportTicketRepo.changeSupportTicketStatus(
        uid,
        ticketId,
        req.body
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /**
   * Reply of support ticket.
   */
  @httpPost("/reply", validate(replySupportTicketSchema))
  async replySupportTicket(
    req: IReplySupportTicketRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.supportTicketRepo.replySupportTicket(uid, req.body, req.files)
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /**
 * Get listing of support ticket.
 */
  @httpGet("/", validate(getSupportTicketSchema))
  async getSupportTicket(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.supportTicketRepo.getSupportTicket(uid, req.query)
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Get Info of support ticket.
   */
  @httpGet("/:ticketId", validate(getSupportTicketInfoSchema))
  async getSupportTicketInfo(
    req: IGetSupportTicketInfoRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.supportTicketRepo.getSupportTicketInfo(
        uid,
        req.params.ticketId
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/reply/:ticketId", validate(getReplyOfSupportTicketSchema))
  async getReplyListOfTicket(
    req: IGetReplyOfTicketRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { ticketId } = req.params
      const data = await this.supportTicketRepo.getReplyListOfTicket(
        uid,
        ticketId,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
