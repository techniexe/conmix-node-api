import { QuestionTypes, Severity } from "./../../utilities/config"
import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { ObjectId } from "bson"
import { UploadedFile } from "express-fileupload"

export interface ICreateSupportTicket {
  question_type: string
  severity: string
  subject: string
  description: string
  support_ticket_status: string
  order_id: string
  client_id: string | ObjectId
  client_type: string
  created_by_id: string | ObjectId
  created_by_type: string
  files: {
    attachments: [UploadedFile]
  }
}

export const createSupportTicketSchema: IRequestSchema = {
  body: Joi.object().keys({
    question_type: Joi.string().valid(QuestionTypes).required(),
    severity: Joi.string().valid(Severity).required(),
    subject: Joi.string().required(),
    description: Joi.string().required(),
    order_id: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex),
    client_type: Joi.string().valid("Buyer").required(),
  }),
}

export interface ICreateSupportTicketRequest extends IAuthenticatedRequest {
  body: ICreateSupportTicket
}

export interface IChangeSupportTicketStatus {
  support_ticket_status: string
}

export const changeSupportTicketStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    support_ticket_status: Joi.string()
      .allow("OPEN", "INPROCESS", "CLOSED", "SOLVED")
      .required(),
  }),
}

export interface IChangeSupportTicketRequest extends IAuthenticatedRequest {
  body: IChangeSupportTicketStatus
}

export interface IReplySupportTicket {
  ticket_id: string
  reply_by_id: string | ObjectId
  reply_by_type: string
  comment: string
  files: {
    attachments: [UploadedFile]
  }
  support_ticket_status?: string
}

export const replySupportTicketSchema: IRequestSchema = {
  body: Joi.object().keys({
    ticket_id: Joi.string().required(),
    comment: Joi.string().required(),
    support_ticket_status: Joi.string().allow("CLOSED", "SOLVED"),
  }),
}

export interface IReplySupportTicketRequest extends IAuthenticatedRequest {
  body: IReplySupportTicket
}

export interface IGetSupportTicket {
  support_ticket_status?: string
  before?: string
  after?: string
  client_id?: string
  subject?: string
  severity?: string
  ticket_id?: string
}

export interface IGetSupportTicketRequest extends IAuthenticatedRequest {
  query: IGetSupportTicket
}

export const getSupportTicketSchema: IRequestSchema = {
  query: Joi.object().keys({
    support_ticket_status: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex),
    subject: Joi.string(),
    severity: Joi.string().valid(Severity),
    ticket_id: Joi.string(),
  }),
}

export interface IGetSupportTicketInfo {
  ticketId: string
  [k: string]: any
}

export interface IGetSupportTicketInfoRequest extends IAuthenticatedRequest {
  params: IGetSupportTicketInfo
}

export const getSupportTicketInfoSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string(),
  }),
}

export interface IGetReplyOfSupportTicketInfo {
  before?: string
  after?: string
}

export interface IGetReplyOfTicketRequest extends IAuthenticatedRequest {
  query: IGetReplyOfSupportTicketInfo
}

export const getReplyOfSupportTicketSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
  }),
}
