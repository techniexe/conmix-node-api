// import { IAuthenticatedRequest } from "./../../middleware/auth-token.middleware"
// import { inject, injectable } from "inversify"
// import { controller, httpGet } from "inversify-express-utils"
// import { verifyCustomToken } from "../../middleware/auth-token.middleware"
// import { BuyerTypes } from "../buyer.types"
// import { Response } from "express-serve-static-core"
// import { BuyerSourceRepository } from "./buyer.source.repository"

// @injectable()
// @controller("/source", verifyCustomToken("buyer"))
// export class BuyerSourceController {
//   constructor(
//     @inject(BuyerTypes.BuyerSourceRepository)
//     private sourceRepo: BuyerSourceRepository
//   ) {}

//   @httpGet("/")
//   async sourceList(req: IAuthenticatedRequest, res: Response) {
//     const data = await this.sourceRepo.sourceList()
//     res.json({ data })
//   }
// }
