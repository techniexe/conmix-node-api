// import { injectable } from "inversify"

// import { isNullOrUndefined } from "../../utilities/type-guards"
// import { InvalidInput } from "../../utilities/customError"

// @injectable()
// export class BuyerSourceRepository {
//   async sourceList() {
//     const query: { [k: string]: any } = {}
//     const aggregateArr = [
//       { $match: query },
//       {
//         $lookup: {
//           from: "state",
//           localField: "region_id",
//           foreignField: "_id",
//           as: "state",
//         },
//       },
//       {
//         $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
//       },
//       {
//         $project: {
//           _id: 1,
//           source_name: 1,
//           region_id: 1,
//           created_at: 1,
//           created_by_id: 1,
//           updated_at: 1,
//           updated_by_id: 1,
//           //"region._id": 1,
//           region_name: "$state.state_name",
//         },
//       },
//     ]

//     const sourceData = await SourceModel.aggregate(aggregateArr)
//     if (isNullOrUndefined(sourceData)) {
//       return Promise.reject(new InvalidInput(`Sorry, we could not find any details related to this product`, 400))
//     }

//     return Promise.resolve(sourceData)
//   }
// }
