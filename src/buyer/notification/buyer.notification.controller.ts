import { BuyerTypes } from "./../buyer.types"
import { injectable, inject } from "inversify"
import { controller, httpGet, httpPatch } from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import {
  getNotificationSchema,
  IGetNotificationsRequest,
  setNotificationAsSeen,
  ISetNotificationAsSeenRequest,
} from "./buyer.notification.schema"
import { Response, NextFunction } from "express"
import { NotificationRepository } from "./buyer.notification.repository"

@injectable()
@controller("/notification", verifyCustomToken("buyer"))
export class NotificationController {
  constructor(
    @inject(BuyerTypes.NotificationRepository)
    private notificationRepo: NotificationRepository
  ) {}
  @httpGet("/", validate(getNotificationSchema))
  async getUserNotification(
    req: IGetNotificationsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { before, after } = req.query
    const notifications = await this.notificationRepo.getNotifications(
      req.user.uid,
      before,
      after,
      4
    )
    return res.json({ data: { notifications } })
  }
  @httpGet("/all", validate(getNotificationSchema))
  async getUserAllNotification(req: IGetNotificationsRequest, res: Response) {
    const { before, after, max_notification_type } = req.query
    const notifications = await this.notificationRepo.getNotifications(
      req.user.uid,
      before,
      after,
      max_notification_type
    )
    return res.json({ data: { notifications } })
  }

  @httpGet("/count")
  async getNotificationCount(req: IAuthenticatedRequest, res: Response) {
    const {
      notification_count,
      unseen_message_count,
    } = await this.notificationRepo.getNotificationCount(req.user.uid)
    return res.json({ data: { notification_count, unseen_message_count } })
  }
  @httpPatch("/:notificationId", validate(setNotificationAsSeen))
  async setNotificationAsSeen(
    req: ISetNotificationAsSeenRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.notificationRepo.setNotificationAsSeen(
      req.user.uid,
      req.params.notificationId
    )
    return res.sendStatus(201)
  }

  @httpPatch("/")
  async setAllNotificationsAsSeen(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.notificationRepo.setAllNotificationsAsSeen(req.user.uid)
    return res.sendStatus(201)
  }
}
