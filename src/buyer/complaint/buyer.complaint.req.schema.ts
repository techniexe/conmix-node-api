import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { complaintTypes } from "../../utilities/config"

export interface IAddComplaint {
  user_id?: string
  order_id: string
  complaint_text: string
  complaint_type: string
  category_id: string
  sub_category_id: string
  read_by_admin: boolean
}

export interface IAddComplaintRequest extends IAuthenticatedRequest {
  body: {
    complaint: IAddComplaint
  }
}

export const addComplaintSchema: IRequestSchema = {
  body: Joi.object().keys({
    complaint: Joi.object().keys({
      order_id: Joi.string().regex(objectIdRegex).required(),
      complaint_text: Joi.string().required(),
      complaint_type: Joi.string().required().valid(complaintTypes),
      category_id: Joi.string().regex(objectIdRegex),
      sub_category_id: Joi.string().regex(objectIdRegex),
    }),
  }),
}
