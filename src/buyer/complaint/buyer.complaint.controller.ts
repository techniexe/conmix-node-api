import { controller, httpPost } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { BuyerTypes } from "../buyer.types"
import { validate } from "../../middleware/joi.middleware"
import {
  addComplaintSchema,
  IAddComplaintRequest,
} from "./buyer.complaint.req.schema"
import { Response, NextFunction } from "express"
import { ComplaintRepository } from "./buyer.complaint.repository"

@controller("/complaint", verifyCustomToken("buyer"))
@injectable()
export class ComplaintController {
  constructor(
    @inject(BuyerTypes.ComplaintRepository)
    private complaintRepo: ComplaintRepository
  ) {}

  @httpPost("/", validate(addComplaintSchema))
  async addComplaint(
    req: IAddComplaintRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      console.log(req.user.uid)
      const { uid } = req.user
      await this.complaintRepo.addComplaint(uid, req.body.complaint)
      return res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }
}
