import { isNullOrUndefined } from "util"
import { injectable, inject } from "inversify"
import { IAddComplaint } from "./buyer.complaint.req.schema"
import { ComplaintModel } from "../../model/complaint.model"
import { MailService } from "../../utilities/mail-service"
import { AdminUserModel } from "../../model/admin.user.model"
import { BuyerTypes } from "../buyer.types"
import { InvalidInput } from "../../utilities/customError"
import { BuyerUserModel } from "../../model/buyer.user.model"

@injectable()
export class ComplaintRepository {
  constructor(
    @inject(BuyerTypes.MailService) private mailService: MailService
  ) {}
  async addComplaint(user_id: string, complaintData: IAddComplaint) {
    if (isNullOrUndefined(complaintData)) {
      return Promise.reject(new InvalidInput(`No Data for update.`, 400))
    }
    complaintData.user_id = user_id
    const complaint = new ComplaintModel(complaintData)
    const complaintDoc = await complaint.save()

    const AdminuserDoc = await AdminUserModel.findOne({ admin_type: "support" })

    if (AdminuserDoc === null) {
      console.log("Admin with the entered details doesn't exist ")
      return Promise.resolve()
    }

    const BuyeruserDoc = await BuyerUserModel.findById({ _id: user_id })
    if (BuyeruserDoc === null) {
      return Promise.reject(
        new InvalidInput(`We couldn't find any user with this email Id/mobile no.`, 400)
      )
    }

    try {
      await this.mailService.sendMailUsingTemplate(
        "product_complaint",
        {
          html: {
            Details: `<br> "userId": ${user_id} <br> "orderId": ${
              complaintData.order_id
            } <br> "complaint_text": ${complaintData.complaint_text}
            <br> "complaint_type": ${
              complaintData.complaint_type
            } <br>"category_id": ${
              complaintData.category_id || ""
            } <br> "sub_category_id": ${complaintData.sub_category_id || ""} `,
          },
        },
        {
          full_name: BuyeruserDoc.full_name,
          email: BuyeruserDoc.email,
        }
      )
    } catch (e) {
      console.log("error on sending mail", e)
      return Promise.resolve()
    }

    return Promise.resolve(complaintDoc)
  }
}
