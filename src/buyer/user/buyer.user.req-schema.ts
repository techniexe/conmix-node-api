import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
const FullNameRegex = /^[a-z. ]*$/i

export const createBuyerUserSchema: IRequestSchema = {
  query: Joi.object().keys({
    signup_type: Joi.string().valid(["mobile", "email"]).required(),
  }),
  body: Joi.object().keys({
    account_type: Joi.string().valid(["Builder", "Contractor", "Individual"]),
    company_type: Joi.string().allow(""),
    company_name: Joi.string().allow(""),
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .required()
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      }),
    mobile_number: Joi.string(),
    email: Joi.string().email(),
    password: Joi.string().min(6).max(20).required(),
    landline_number: Joi.string(),
    // identity_image_url: Joi.string(),
    gst_number: Joi.string().allow(""),
    pan_number: Joi.string().length(10),
    pay_method_id: Joi.string().regex(objectIdRegex),
  }),
}

export const editBuyerUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    account_type: Joi.string().valid(["Builder", "Contractor", "Individual"]),
    company_type: Joi.string().allow(""),
    company_name: Joi.string().allow(""),
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      }),
    password: Joi.string().min(6).max(20),
    email: Joi.string().email(),
    mobile_number: Joi.string(),
    landline_number: Joi.string(),
    // identity_image_url: Joi.string(),
    gst_number: Joi.string().allow(""),
    pan_number: Joi.string().length(10),
    pay_method_id: Joi.string().regex(objectIdRegex),
   // authentication_code: Joi.string().required(),
    old_mobile_code: Joi.string(),
    new_mobile_code: Joi.string(),
    old_email_code: Joi.string(),
    new_email_code: Joi.string(),
  }),
}

export const verifyMobilenumberSchema: IRequestSchema = {
  params: Joi.object().keys({
    mobileNumber: Joi.string().required(),
    code: Joi.string().required(),
  }),
}

export const resendEmailSchema: IRequestSchema = {
  body: Joi.object().keys({
    email: Joi.string().required(),
  }),
}

export const resendOTPSchema: IRequestSchema = {
  body: Joi.object().keys({
    mobileNumber: Joi.string().required(),
  }),
}

export const changePasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    old_password: Joi.string().min(6).max(20).required(),
    password: Joi.string().min(6).max(20).required(),
    authentication_code: Joi.string().required(),
  }),
}

export const forgotPasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    password: Joi.string().min(6).max(20).required()
  }),
}


export const requestOTPForUpdateProfileSchema: IRequestSchema = {
  body: Joi.object().keys({
    old_mobile_no: Joi.string(),
    new_mobile_no: Joi.string(),
    old_email: Joi.string(),
    new_email: Joi.string(),
  }),
}