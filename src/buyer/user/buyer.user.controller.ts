import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { BuyerTypes } from "../buyer.types"
import { BuyerUserRepository } from "./buyer.user.repository"
import {
  ICreateBuyerProfileRequest,
  IEditBuyerProfileRequest,
  IVerifyMobilenumberRequest,
  IResendEmailRequest,
  IResendOTPRequest,
  IChangePasswordRequest,
  IForgotPasswordRequest,
} from "./buyer.user.req-defination"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  createBuyerUserSchema,
  editBuyerUserSchema,
  verifyMobilenumberSchema,
  resendEmailSchema,
  resendOTPSchema,
  changePasswordSchema,
  forgotPasswordSchema,
  requestOTPForUpdateProfileSchema,
} from "./buyer.user.req-schema"
import { createJWTToken } from "../../utilities/jwt.utilities"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/users", verifyCustomToken("buyer"))
export class BuyerUserController {
  constructor(
    @inject(BuyerTypes.BuyerUserRepository)
    private buyerUserRepo: BuyerUserRepository
  ) {}

  /** If user is login then this service may be used to change his password */
  @httpPatch("/changePassword", validate(changePasswordSchema))
  async changePassword(
    req: IChangePasswordRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.buyerUserRepo.changePassword(
      req.user.uid,
      req.body.old_password,
      req.body.password,
      req.body.authentication_code
    )
    res.sendStatus(201)
  }

    /** Forgot Password. */
    @httpPatch("/forgotPassword", validate(forgotPasswordSchema))
    async forgotPassword(
      req: IForgotPasswordRequest,
      res: Response,
      next: NextFunction
    ) {
      try {
        await this.buyerUserRepo.forgotPassword(req.user.uid, req.body.password)
        res.sendStatus(201)
      } catch (err) {
        return next(err)
      }
    }

  @httpPost("/", validate(createBuyerUserSchema))
  async createProfile(
    req: ICreateBuyerProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const stat = await this.buyerUserRepo.createUser(
        req.user.uid,
        req.body,
        req.query.signup_type,
        req.files
      )
      const jwtObj = {
        user_type: "buyer",
        full_name: stat.full_name,
        uid: stat._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")

      console.log("hereeeeee222")
      console.log("respp", customToken)
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      let msg = `Sorry, account for this user could not be created.`
      if (err.code === 11000) {
        msg = `User with this email or mobile is already registered.`
      }
      // if (err.message === "User already registered.") {
      //   return res.status(422).json({
      //     error: {
      //       message: "Invalid Request",
      //       details: [{ path: "token", message: err.message }],
      //     },
      //   })
      // }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)

      // err.httpStatusCode = 400
      // return next(err)
    }
  }

  @httpPatch("/", validate(editBuyerUserSchema))
  async editProfile(
    req: IEditBuyerProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const stat = await this.buyerUserRepo.editUser(
        req.user.uid,
        req.body,
        req.files
      )
      const jwtObj = {
        user_type: "buyer",
        full_name: stat.full_name,
        uid: stat._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      return next(err)
    }
  }

  @httpGet("/profile")
  async getUserDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const user = await this.buyerUserRepo.getUserProfile(req.user.uid)
      if (user === null) {
        return res
          .status(404)
          .json({ error: { message: `Sorry we did not find any user with this details` } })
      }
      return res.json({ data: user })
    } catch (err) {
      next(err)
    }
  }

  @httpPost(
    "/mobile/:mobileNumber/code/:code",
    validate(verifyMobilenumberSchema)
  )
  async verifyMobilenumber(
    req: IVerifyMobilenumberRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.buyerUserRepo.verifyUserWithMobilenumber(
        req.user.uid,
        req.params.mobileNumber,
        req.params.code
      )
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendEmail", validate(resendEmailSchema))
  async resendEmail(
    req: IResendEmailRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const message = await this.buyerUserRepo.resendEmail(
        req.user.uid,
        req.body.email
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendOTP", validate(resendOTPSchema))
  async resendOTP(req: IResendOTPRequest, res: Response, next: NextFunction) {
    try {
      const message = await this.buyerUserRepo.resendOTP(
        req.user.uid,
        req.body.mobileNumber
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/requestOTP")
  async requestOTP(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.buyerUserRepo.requestOTP(req.user.uid, req.query)
      return res.send({ data: data.code })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/requestOTPForUpdateProfile", validate(requestOTPForUpdateProfileSchema))
  async requestOTPForUpdateProfile(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.buyerUserRepo.requestOTPForUpdateProfile(req.user.uid, req.body)
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
