import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface ICreateBuyerUserDetails {
  full_name: string
  email: string
  image?: string
  signup_type: string
}

export interface ICreateBuyerProfile {
  full_name: string
  mobile_number: string
  email?: string
  account_type: string // personal/company
  password: string
  landline_number?: string
  signup_type: string //facebook/gmail/mobile
  identity_image_url: string
  gst_number: string
  pan_number: string
  pay_method_id: string
}

export interface ICreateBuyerProfileRequest extends IAuthenticatedRequest {
  query: {
    signup_type: string
  }
  body: ICreateBuyerProfile
  files: {
    identity_image: UploadedFile
  }
}

export interface IEditBuyerProfile {
  full_name?: string
  password?: string
  email?: string
  mobile_number?: string
  landline_number?: string
  identity_image_url: string
  gst_number: string
  pan_number: string
  pay_method_id: string
  authentication_code: string
  new_mobile_code: string
  new_email_code: string
  old_mobile_code: string
  old_email_code: string
}
export interface IEditBuyerProfileRequest extends IAuthenticatedRequest {
  body: IEditBuyerProfile
  files: {
    identity_image: UploadedFile
  }
}

export interface IVerifyMobilenumberRequest extends IAuthenticatedRequest {
  params: {
    mobileNumber: string
    code: string
  }
}

export interface IResendEmailRequest extends IAuthenticatedRequest {
  body: {
    email: string
  }
}

export interface IResendOTPRequest extends IAuthenticatedRequest {
  body: {
    mobileNumber: string
  }
}

export interface IChangePasswordRequest extends IAuthenticatedRequest {
  body: {
    old_password: string
    password: string
    authentication_code: string
  }
}


export interface IForgotPasswordRequest extends IAuthenticatedRequest {
  body: {
    password: string
  }
}