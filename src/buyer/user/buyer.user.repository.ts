import { injectable, inject } from "inversify"

import {
  ICreateBuyerUserDetails,
  ICreateBuyerProfile,
  IEditBuyerProfile,
} from "./buyer.user.req-defination"
import { isNullOrUndefined } from "../../utilities/type-guards"
import * as bcrypt from "bcryptjs"
import { AuthModel, emailVerificationType } from "../../model/auth.model"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { BuyerTypes } from "../buyer.types"
// import { BuyerAuthRepository } from "../auth/buyer.auth.repository"
// import { TwilioService } from "../../utilities/twilio-service"
import { MailService } from "../../utilities/mail-service"
import { emailVerificationLinkURI, awsConfig } from "../../utilities/config"
import { CommonService } from "../../utilities/common.service"
import { logger } from "../../logger"
import { AccessTypes } from "../../model/access_logs.model"
import { UploadedFile } from "express-fileupload"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { BuyerUserModel, IBuyerUserDoc } from "../../model/buyer.user.model"
import { TestService } from "../../utilities/test.service"
import { SiteModel } from "../../model/site.model"
import { ObjectId } from "bson"
import { MailGunService } from "../../utilities/mailgun.service"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { client_mail_template } from "../../utilities/client_email_tempalte"
import { VendorUserModel } from "../../model/vendor.user.model"
import { AdminUserModel } from "../../model/admin.user.model"
const SALT_WORK_FACTOR = 10

/**
 * Ways in which a user can be identified
 */
export enum UserIdentifiers {
  mobile_number,
  email,
}
export function getUserIdentifierType(identifier: string): UserIdentifiers {
  if (identifier.length === 0) {
    throw new Error("Identifier cannot be of length zero.")
  }
  if (identifier.charAt(0) === "+") {
    return UserIdentifiers.mobile_number
  }
  return UserIdentifiers.email
}

@injectable()
export class BuyerUserRepository {
  constructor(
    @inject(BuyerTypes.UniqueidService)
    private uniqueIdService: UniqueidService,
    // @inject(BuyerTypes.BuyerAuthRepository)
    // private authRepo: BuyerAuthRepository,
    // @inject(BuyerTypes.TwilioService) private twilioService: TwilioService,
    @inject(BuyerTypes.TestService) private testService: TestService,
    @inject(BuyerTypes.CommonService) private commonService: CommonService,
    @inject(BuyerTypes.MailService) private mailService: MailService,
    @inject(BuyerTypes.mailgunService) private mailgunService: MailGunService
  ) {}
  /**
   * Create new User using (third party auth) gmail or facebook
   * @param user
   */
  async createAndGetUser(
    userInfo: ICreateBuyerUserDetails
  ): Promise<IBuyerUserDoc> {
    const userCheck = await BuyerUserModel.findOne({ email: userInfo.email })
    if (userCheck !== null) {
      return Promise.resolve(userCheck)
    }
    const userDoc = {
      user_id: await this.uniqueIdService.getUniqueBuyerid(),
      email: userInfo.email,
      full_name: userInfo.full_name,
      image: userInfo.image,
      thumbnail: userInfo.image,
      signup_type: userInfo.signup_type,
      email_verified: true,
    }
    const user = new BuyerUserModel(userDoc)
    return user.save()
  }

  async getUserFromDbUsing(
    identifierType: UserIdentifiers,
    value: string
  ): Promise<IBuyerUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = value
    return BuyerUserModel.findOne(query).exec()
  }

  async getUserWithPassword(
    identifierType: UserIdentifiers,
    identifierValue: string,
    password: string
  ): Promise<IBuyerUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = identifierValue
    console.log("query:", query)
    const doc = await BuyerUserModel.findOne(query).exec()

    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.resolve(null)
    }
    if (await bcrypt.compare(password, doc.password)) {
      return Promise.resolve(doc)
    }
    return Promise.resolve(null)
  }

  /**method used for creating fresh user with mobile number token */
  async createUser(
    uid: string,
    createBody: ICreateBuyerProfile,
    signup_type: string,
    fileData: {
      identity_image: UploadedFile
    }
  ): Promise<IBuyerUserDoc> {
    console.log("createBody", createBody)
    let createDoc: { [k: string]: any } = {
      user_id: await this.uniqueIdService.getUniqueBuyerid(),
      _id: uid,
      full_name: createBody.full_name.trim(),
      password: await bcrypt.hash(createBody.password, SALT_WORK_FACTOR),
      signup_type,
    }
    //let tokenData: any
    const oldData = await BuyerUserModel.findOne({ _id: uid }).exec()

    console.log("oldData", oldData)
    if (oldData !== null) {
      return Promise.reject(new Error("User already registered."))
    }

    // if (signup_type === "email") {
    //   tokenData = await AuthEmailModel.findOne({ _id: uid }).exec()

    //   console.log("tokenData", tokenData)

    //   if (tokenData === null) {
    //     return Promise.reject(
    //       new Error("Unable to validate session. Please verify email again")
    //     )
    //   }
    // } else {
    //   tokenData = await AuthModel.findOne({ _id: uid }).exec()
    //   if (tokenData === null) {
    //     return Promise.reject(
    //       new Error("Unable to validate session. Please verify mobile again")
    //     )
    //   }
    // }

    // Validate user's identifier
    // if (
    //   (signup_type === "mobile" &&
    //     getUserIdentifierType(tokenData.identifier) !==
    //       UserIdentifiers.mobile_number) ||
    //   (signup_type === "email" &&
    //     getUserIdentifierType(tokenData.email) !== UserIdentifiers.email)
    // ) {
    //   return Promise.reject(new Error("Invalid request"))
    // }

    console.log("Hereee111")

    const arrKeys = [
      "email",
      "account_type",
      "mobile_number",
      "landline_number",
      "identity_image_url",
      "gst_number",
      "pan_number",
      "company_name",
      "company_type",
      "pay_method_id",
    ]
    for (const [key, value] of Object.entries(createBody)) {
      if (arrKeys.indexOf(key) > -1) {
        createDoc[key] = value
      }
    }
    if (signup_type === "mobile") {
      //   createDoc.mobile_number = tokenData.identifier
      createDoc.mobile_verified = true
    } else if (signup_type === "email") {
      // createDoc.email = tokenData.email
      createDoc.email_verified = true
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.identity_image) &&
      !isNullOrUndefined(fileData.identity_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/buyer_user`
      let objectName = "" + createDoc._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.identity_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.identity_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.identity_image.data,
        fileData.identity_image.mimetype
      )
      createDoc.identity_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    console.log("Hereee222")

    if(!isNullOrUndefined(createBody.email)){
      let vendorUserDoc = await VendorUserModel.findOne({
        email: createBody.email
      })
      if(!isNullOrUndefined(vendorUserDoc)){
        return Promise.reject(new Error(`${createBody.email} is already associated with another vendor account.`))
      }
    
      let adminUserDoc = await AdminUserModel.findOne({
        email: createBody.email
      })
      if(!isNullOrUndefined(adminUserDoc)){
        return Promise.reject(new Error(`${createBody.email} is already associated with another admin account.`,))
      }
    }


    if(!isNullOrUndefined(createBody.mobile_number)){
      let vendorUserDoc = await VendorUserModel.findOne({
        mobile_number: createBody.mobile_number
      })
      if(!isNullOrUndefined(vendorUserDoc)){
        return Promise.reject(new Error(`${createBody.mobile_number} is already associated with another vendor account.`))
      }
    
      let adminUserDoc = await AdminUserModel.findOne({
        mobile_number: createBody.mobile_number
      })
      if(!isNullOrUndefined(adminUserDoc)){
        return Promise.reject(new Error(`${createBody.mobile_number} is already associated with another admin account.`,))
      }
    }


    const userDoc = await new BuyerUserModel(createDoc).save()
    let client_name: any
    if (createDoc.account_type === "Individual") {
      client_name = createDoc.full_name
    } else {
      client_name = createDoc.company_name
    }
    if (!isNullOrUndefined(createBody.email)) {
      //email has been changed. Send verification link to user
      // const emailToken = await this.commonService.getEmailAuthToken(
      //   createBody.email,
      //   "buyer",
      //   uid,
      //   emailVerificationType.PROFILE
      // )
      // const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
      // try {
      //   await this.mailService.sendMailUsingTemplate(
      //     "email_verification",
      //     {
      //       subject: { name: userDoc.full_name },
      //       html: { name: userDoc.full_name, email: createBody.email, link },
      //     },
      //     { full_name: userDoc.full_name, email: createBody.email }
      //   )
      // } catch (e) {
      //   logger.fatal("Unable to send message:", e)
      //   // return Promise.reject(
      //   //   new Error(`Unable to send email. Please try again later.`)
      //   // )
      // }
      // SEND MAIL.
      let mail_html = client_mail_template.welcome
      mail_html = mail_html.replace("{{client_name}}", client_name)
      var data = {
        from: "no-reply@conmate.in",
        to: createBody.email,
        subject: "Conmix - Welcome",
        html: mail_html
      }
      await this.mailgunService.sendEmail(data)
    }

    if (!isNullOrUndefined(createBody.mobile_number)) {
      // const authCode = await this.authRepo.getBuyerAuthCode(
      //   createBody.mobile_number,
      //   uid
      // )
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   createBody.mobile_number,
      //   authCode.code,
      //   "sms"
      // )
    }

    let message = `Hi, welcome to Conmix - an advanced mobile and web application for Ready Mix Concrete Solutions. Thank you for the registration. Order your Custom RMC and track the order to plan the pour. Login Now !!`
    let templateId = "1707163729941169129"
    await this.testService.sendSMS(
      createBody.mobile_number,
      message,
      templateId
    )

    await AuthModel.deleteOne({ _id: uid })
    return userDoc
  }

  async editUser(
    uid: string,
    editBody: IEditBuyerProfile,
    fileData: {
      identity_image: UploadedFile
    }
  ): Promise<IBuyerUserDoc> {
    const userCheck = await BuyerUserModel.findOne({ _id: uid })
    if (userCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    // const result = await AuthModel.findOne({
    //   code: editBody.authentication_code,
    // })
    // if (isNullOrUndefined(result)) {
    //   return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    // }

    if (!isNullOrUndefined(editBody.new_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.new_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.email)) {
      const numberCheck = await BuyerUserModel.findOne({
        email: editBody.email,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(uid) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other client.`,
            400
          )
        )
      }

      const vendorDoc = await VendorUserModel.findOne({
        email: editBody.email
      })
      if(!isNullOrUndefined(vendorDoc)){
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other vendor.`,
            400
          )
        )
      }

      const adminDoc = await AdminUserModel.findOne({
        email: editBody.email
      })
      if(!isNullOrUndefined(adminDoc)){
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other admin.`,
            400
          )
        )
      }
    }

    if (!isNullOrUndefined(editBody.mobile_number)) {
      const numberCheck = await BuyerUserModel.findOne({
        mobile_number: editBody.mobile_number,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(uid) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other client.`,
            400
          )
        )
      }

      const vendorDoc = await VendorUserModel.findOne({
        mobile_number: editBody.mobile_number
      })
      if(!isNullOrUndefined(vendorDoc)){
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other vendor.`,
            400
          )
        )
      }

      const adminDoc = await AdminUserModel.findOne({
        mobile_number: editBody.mobile_number
      })
      console.log("adminDoc" ,adminDoc)
      if(!isNullOrUndefined(adminDoc)){
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other admin.`,
            400
          )
        )
      }
    }


    console.log("fileDATAAAAAA", fileData)
    let editDoc: { [k: string]: any } = {}
    const arrKeys = [
      "account_type",
      "company_type",
      "company_name",
      "full_name",
      "landline_number",
      "identity_image_url",
      "gst_number",
      "pan_number",
      "pay_method_id",
    ]
    for (const [key, value] of Object.entries(editBody)) {
      if (arrKeys.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }

    let client_name: any
    if (!isNullOrUndefined(userCheck)) {
      if (userCheck.account_type === "Individual") {
        client_name = userCheck.full_name
      } else {
        client_name = userCheck.company_name
      }
    }
    let pwd_updated_mail_html = client_mail_template.password_updated_successfully
    pwd_updated_mail_html = pwd_updated_mail_html.replace("{{client_name}}", client_name)
    if (!isNullOrUndefined(editBody.password)) {
      var data = {
        from: "no-reply@conmate.in",
        to: userCheck.email,
        subject: "Conmix - Password Update Successful",
        html: pwd_updated_mail_html,
      }
      await this.mailgunService.sendEmail(data)

      let message = `Hi. You have successfully updated the login password for Conmix.`
      // Password Update Successful.
      let templateId = "1707163635326085233"
      await this.testService.sendSMS(
        userCheck.mobile_number,
        message,
        templateId
      )

      let siteData = await SiteModel.find({ user_id: new ObjectId(uid) })
      if (siteData.length > 0) {
        for (let i = 0; i < siteData.length; i++) {
          let site_message = `Hi, ${siteData[i].person_name}, your login password has been updated. Kindly contact ${client_name}. - conmix`
          // Password Update Successful.
          let site_templateId = "1707163775555810802"
          await this.testService.sendSMS(
            siteData[i].mobile_number,
            site_message,
            site_templateId
          )
          let pwd_updated_mail_html_for_site = client_mail_template.password_updated_successfully_site
          pwd_updated_mail_html_for_site = pwd_updated_mail_html_for_site.replace("{{site_person_name}}", siteData[i].company_name)
          pwd_updated_mail_html_for_site = pwd_updated_mail_html_for_site.replace("{{client_name}}", client_name)
          var data = {
            from: "no-reply@conmate.in",
            to: siteData[i].email,
            subject: "Conmix - Password Update Successful",
            html: pwd_updated_mail_html_for_site,
          }
          await this.mailgunService.sendEmail(data)
        }
      }
      editDoc.password = await bcrypt.hash(editBody.password, SALT_WORK_FACTOR)
    }

    let mobile_number: any
    if (!isNullOrUndefined(editBody.mobile_number)) {
      mobile_number = editBody.mobile_number
    } else {
      mobile_number = userCheck.mobile_number
    }
    let email: any
    if (!isNullOrUndefined(editBody.email)) {
      email = editBody.email
    } else {
      email = userCheck.email
    }
    if (!isNullOrUndefined(editBody.email)) {
      editDoc.email = editBody.email

      if (userCheck.email !== editBody.email) {
        editDoc.email_verified = false

        let message = `Hi ${client_name}, you have successfully updated your email id to ${editBody.email}. Your earlier registered email id. was ${userCheck.email}. - conmix`
        // mobile no update
        let templateId = "1707163894391191264"
        await this.testService.sendSMS(mobile_number, message, templateId)

        var data1 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Email Update Successful",
          html: `Dear ${client_name},<br>
          You have successfully updated your email id to ${editBody.email}. Your earlier registered email id. was ${userCheck.email}.
          <br>  <br> <br>
          Regards,<br>
          Conmate E-Commerce Pvt. Ltd.`,
        }
        await this.mailgunService.sendEmail(data1)
      }
    }

    if (!isNullOrUndefined(editBody.mobile_number)) {
      editDoc.mobile_number = editBody.mobile_number

      if (userCheck.mobile_number !== editBody.mobile_number) {
        editDoc.mobile_verified = false

        let message = `Hi ${client_name}, you have successfully updated your Mobile No. to ${editBody.mobile_number}. Your earlier registered Mobile no. was ${userCheck.mobile_number}. - conmix`
        // mobile no update
        let templateId = "1707163894384517270"
        await this.testService.sendSMS(mobile_number, message, templateId)

        var data11 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Mobile number Update Successful",
          html: `Dear ${client_name},<br>
          You have successfully updated your Mobile No. to ${editBody.mobile_number}. Your earlier registered Mobile no. was 'Previous Mobile No.'
          <br>  <br> <br>

          Regards,<br>
          Conmate E-Commerce Pvt. Ltd.`,
        }
        await this.mailgunService.sendEmail(data11)
      }
    }

    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(
        new InvalidInput(`Kindly enter a field for update`, 400)
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.identity_image) &&
      !isNullOrUndefined(fileData.identity_image.data)
    ) {
      console.log("hereeeeee")
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/buyer_user`
      // let objectName = "" + userCheck._id

      let objectName = `${uid}_${Math.random()}`

      // let objectName = `${uid}`
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.identity_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.identity_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.identity_image.data,
        fileData.identity_image.mimetype
      )
      editDoc.identity_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }
    console.log("uid", uid)
    console.log("editDoccccccc", editDoc)

  

    const userDoc = await BuyerUserModel.findOneAndUpdate(
      { _id: uid },
      { $set: editDoc },
      {
        new: true,
      }
    )
    if (userDoc === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_email_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_email_code,
    })

    await this.commonService.addActivityLogs(
      uid,
      "buyer",
      AccessTypes.EDIT_USER,
      "User information updated."
    )

    if (!isNullOrUndefined(editDoc.email)) {
      // //email has been changed. Send verification link to user
      // const emailToken = await this.commonService.getEmailAuthToken(
      //   editDoc.email,
      //   "buyer",
      //   uid,
      //   emailVerificationType.PROFILE
      // )
      // const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
      // try {
      //   await this.mailService.sendMailUsingTemplate(
      //     "email_verification",
      //     {
      //       subject: { name: userDoc.full_name },
      //       html: { name: userDoc.full_name, email: editDoc.email, link },
      //     },
      //     { full_name: userDoc.full_name, email: editDoc.email }
      //   )
      // } catch (e) {
      //   logger.fatal("Unable to send message:", e)
      //   return Promise.reject(
      //     new Error(`Unable to send email. Please try again later.`)
      //   )
      // }
    }
    if (!isNullOrUndefined(editDoc.mobile_number)) {
      // const authCode = await this.authRepo.getBuyerAuthCode(
      //   editDoc.mobile_number,
      //   uid
      // )
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   editDoc.mobile_number,
      //   authCode.code,
      //   "sms"
      // )
    }
    return Promise.resolve(userDoc)
  }
  async getUserProfile(uid: string): Promise<IBuyerUserDoc | null> {
    return BuyerUserModel.findById(uid)
  }

  async verifyUserWithMobilenumber(
    user_id: string,
    mobileNumber: string,
    code: string
  ) {
    const userCheck = await AuthModel.findOne({
      user_id,
      user_type: "buyer",
      identifier: mobileNumber,
      code,
    })

    if (userCheck === null) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again `, 400)
      )
    }

    return Promise.all([
      BuyerUserModel.findOneAndUpdate(
        {
          _id: user_id,
          mobile_number: mobileNumber,
        },
        { $set: { mobile_verified: true } }
      ),
      AuthModel.deleteOne({
        user_id,
        user_type: "buyer",
        identifier: mobileNumber,
      }),
    ])
  }

  async resendEmail(user_id: string, email: string) {
    const userCheck = await BuyerUserModel.findOne({
      _id: user_id,
      email,
    })

    if (userCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This Email is not registered.`, 400)
      )
    }

    if (userCheck.email_verified === true) {
      return Promise.resolve("Email already verified.")
    }
    const emailToken = await this.commonService.getEmailAuthToken(
      email,
      "buyer",
      user_id,
      emailVerificationType.PROFILE
    )
    const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
    try {
      await this.mailService.sendMailUsingTemplate(
        "email_verification",
        {
          subject: { name: userCheck.full_name },
          html: { name: userCheck.full_name, email, link },
        },
        { full_name: userCheck.full_name, email }
      )
      return Promise.resolve("Email sent.")
    } catch (e) {
      logger.fatal("Unable to send message:", e)
      return Promise.reject(
        new Error(`Unable to send email. Please try again later.`)
      )
    }
  }

  async resendOTP(user_id: string, mobileNumber: string) {
    const authData = await AuthModel.findOne({
      identifier: mobileNumber,
      user_type: "buyer",
    }).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    const userCheck = await BuyerUserModel.findOne({
      _id: user_id,
      mobile_number: mobileNumber,
    })

    if (userCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This Mobile is not registered.`, 400)
      )
    }

    if (userCheck.mobile_verified === true) {
      return Promise.resolve("This mobile already verified.")
    }

    let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(mobileNumber, message, templateId)
    return Promise.resolve("OTP sent.")
  }

  async changePassword(
    uid: string,
    old_password: string,
    password: string,
    authentication_code: string
  ): Promise<any> {
    console.log(uid)

    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const doc = await BuyerUserModel.findById(uid)

    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }
    let client_name: any
    if (!isNullOrUndefined(doc)) {
      if (doc.account_type === "Individual") {
        client_name = doc.full_name
      } else {
        client_name = doc.company_name
      }
    }

    if (await bcrypt.compare(old_password, doc.password)) {

      let pwd_updated_mail_html = client_mail_template.password_updated_successfully
      pwd_updated_mail_html = pwd_updated_mail_html.replace("{{client_name}}", client_name)
      var data = {
        from: "no-reply@conmate.in",
        to: doc.email,
        subject: "Conmix - Password Update Successful",
        html: pwd_updated_mail_html,
      }
      await this.mailgunService.sendEmail(data)

      let message = `Hi. You have successfully updated the login password for Conmix.`
      // Password Update Successful.
      let templateId = "1707163635326085233"
      await this.testService.sendSMS(doc.mobile_number, message, templateId)

      let siteData = await SiteModel.find({ user_id: new ObjectId(uid) })
      if (siteData.length > 0) {
        for (let i = 0; i < siteData.length; i++) {
          
          let pwd_updated_mail_html_for_site = client_mail_template.password_updated_successfully_site
          pwd_updated_mail_html_for_site = pwd_updated_mail_html_for_site.replace("{{site_person_name}}", siteData[i].company_name)
          pwd_updated_mail_html_for_site = pwd_updated_mail_html_for_site.replace("{{client_name}}", client_name)
          var data = {
            from: "no-reply@conmate.in",
            to: siteData[i].email,
            subject: "Conmix - Password Update Successful",
            html: pwd_updated_mail_html_for_site,
          }
          await this.mailgunService.sendEmail(data)

          //let site_message = `Hi, ${siteData[i].person_name}, your login password has been updated. Kindly contact ${client_name}`
          let site_message = `Hi, ${siteData[i].person_name}, your login password has been updated. Kindly contact ${client_name}. - conmix`
          // Password Update Successful.
          let site_templateId = "1707163775555810802"
          await this.testService.sendSMS(
            siteData[i].mobile_number,
            site_message,
            site_templateId
          )
        }
      }
      return BuyerUserModel.updateOne(
        { _id: uid },
        { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
      )
    }

    await this.commonService.addActivityLogs(
      uid,
      "buyer",
      AccessTypes.CHANGE_PASSWORD_BY_BUYER,
      `Buyer Changed password with id ${uid}.`,
      old_password,
      password
    )

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: authentication_code,
    })

    return Promise.reject(
      new InvalidInput(
        `The New password and  Confirm password do not match `,
        400
      )
    )
  }

  async requestOTP(user_id: string, reqData: any) {
    const userDoc = await BuyerUserModel.findById(user_id)

    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }

    if(!isNullOrUndefined(reqData.old_password) && !isNullOrUndefined(reqData.password)){
      if (await bcrypt.compare(reqData.old_password, userDoc.password)) {
        const code = generateRandomNumber()

        const authData = await AuthModel.findOneAndUpdate(
          {
            identifier: userDoc.mobile_number,
            user_id,
          },
          {
            $set: { code, created_at: Date.now() },
          },
          { upsert: true, new: true }
        ).exec()
        if (authData === null) {
          return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
        }
       
        let message = `"${code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
        // Request OTP.
        let templateId = "1707163645095255064"
        await this.testService.sendSMS(userDoc.mobile_number, message, templateId)
        return Promise.resolve(authData)
      } else {
        return Promise.reject(
          new InvalidInput(
            `The old password not matched with existing password. `,
            400
          )
        )
      }
    }
    const code = generateRandomNumber()

    const authData = await AuthModel.findOneAndUpdate(
      {
        identifier: userDoc.mobile_number,
        user_id,
      },
      {
        $set: { code, created_at: Date.now() },
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    // await this.twilioService.sendCode(
    //   userDoc.mobile_number,
    //   authData.code,
    //   "sms"
    // )

    let message = `"${code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Request OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(userDoc.mobile_number, message, templateId)

    return Promise.resolve(authData)
  }

  async forgotPassword(uid: string, password: string) {
    console.log(uid)

    const doc = await BuyerUserModel.findById(uid)

    if (isNullOrUndefined(doc)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }

    await BuyerUserModel.updateOne(
      { _id: uid },
      { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
    )

    let message = `Hi. You have successfully updated the login password for Conmix.`
    // Password Update Successful.
    let templateId = "1707163729957458233"
    await this.testService.sendSMS(doc.mobile_number, message, templateId)

    let client_name: any
    if (!isNullOrUndefined(doc)) {
      if (doc.account_type === "Individual") {
        client_name = doc.full_name
      } else {
        client_name = doc.company_name
      }
    }

    let pwd_updated_mail_html = client_mail_template.password_updated_successfully
    pwd_updated_mail_html = pwd_updated_mail_html.replace("{{client_name}}", client_name)
    var data = {
      from: "no-reply@conmate.in",
      to: doc.email,
      subject: "Conmix - Password Update Successful",
      html: pwd_updated_mail_html,
    }
    await this.mailgunService.sendEmail(data)

    await this.commonService.addActivityLogs(
      uid,
      "admin",
      AccessTypes.FORGOT_PASSWORD_BY_BUYER,
      `Admin Forgot password with id ${uid}.`,
      doc.password,
      password
    )
  }

  async requestOTPForUpdateProfile(user_id: string, reqData: any) {
    const userDoc = await BuyerUserModel.findById(user_id)
    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }

    let client_name: any
    if (!isNullOrUndefined(userDoc)) {
      if (userDoc.account_type === "Individual") {
        client_name = userDoc.full_name
      } else {
        client_name = userDoc.company_name
      }
    }


    let oldmobileauthData: any
    if (!isNullOrUndefined(reqData.old_mobile_no)) {
      const code = generateRandomNumber()
      oldmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Request OTP.
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(reqData.old_mobile_no, message, templateId)
    }

    let newmobileauthData: any
    if (!isNullOrUndefined(reqData.new_mobile_no)) {


      const numberCheck = await BuyerUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered`, 400)
        )
      }

      const vendorDoc = await VendorUserModel.findOne({
        mobile_number: reqData.new_mobile_no
      })
      if(!isNullOrUndefined(vendorDoc)){
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other vendor.`,
            400
          )
        )
      }

      const adminDoc = await AdminUserModel.findOne({
        mobile_number: reqData.new_mobile_no
      })
      console.log("adminDoc" ,adminDoc)
      if(!isNullOrUndefined(adminDoc)){
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other admin.`,
            400
          )
        )
      }

      const code = generateRandomNumber()
      newmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Request OTP.
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(reqData.new_mobile_no, message, templateId)
    }
    let oldemailauthData: any
    if (!isNullOrUndefined(reqData.old_email)) {
      const code: any = generateRandomNumber()
      oldemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }
      let otp_html = client_mail_template.one_time_pwd
      otp_html = otp_html.replace("{{client_name}}", client_name)
      otp_html = otp_html.replace("{{code}}", code)
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.old_email,
        subject: "Conmix - One Time Password",
        html: otp_html,
      }
      await this.mailgunService.sendEmail(data)
    }
    let newemailauthData: any
    if (!isNullOrUndefined(reqData.new_email)) {
      const numberCheck = await BuyerUserModel.findOne({
        email: reqData.new_email,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(`This email is already registered`, 400)
        )
      }

      const vendorDoc = await VendorUserModel.findOne({
        email: reqData.new_email
      })
      if(!isNullOrUndefined(vendorDoc)){
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other vendor.`,
            400
          )
        )
      }

      const adminDoc = await AdminUserModel.findOne({
        email: reqData.new_email
      })
      if(!isNullOrUndefined(adminDoc)){
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other admin.`,
            400
          )
        )
      }



      const code:any = generateRandomNumber()
      newemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let otp_html = client_mail_template.one_time_pwd
      otp_html = otp_html.replace("{{code}}", code)
      otp_html = otp_html.replace("{{client_name}}", client_name)
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.new_email,
        subject: "Conmix - One Time Password",
        html: otp_html,
      }
      await this.mailgunService.sendEmail(data)
    }
    // const userDoc = await BuyerUserModel.findById(user_id)
    // if (isNullOrUndefined(userDoc)) {
    //   return Promise.reject(new InvalidInput(`No document could be found`, 400))
    // }
    // //const code = generateRandomNumber()
    // const authData = await AuthModel.findOneAndUpdate(
    //   {
    //     identifier: userDoc.mobile_number,
    //     user_id,
    //   },
    //   {
    //     $set: { code, created_at: Date.now() },
    //   },
    //   { upsert: true, new: true }
    // ).exec()
    // if (authData === null) {
    //   return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    // }

    // let message = `"${code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // // Request OTP.
    // let templateId = "1707163645095255064"
    // await this.testService.sendSMS(userDoc.mobile_number, message, templateId)
    return Promise.resolve({
      oldmobileauthData,
      newmobileauthData,
      oldemailauthData,
      newemailauthData,
    })
  }
}
