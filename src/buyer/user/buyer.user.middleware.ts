import { NextFunction, RequestHandler, Response, Request } from "express"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { AdminUserModel } from "../../model/admin.user.model"

export const assertThatMobileNumberNotRegistered: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { mobileNumber } = req.params
  // mobileNumber is compulsory
  if (isNullOrUndefined(mobileNumber)) {
    return res.status(422).json({
      message: "Invalid Request.",
      details: [
        {
          path: "mobileNumber",
          description: "mobile number is required.",
        },
      ],
    })
  }

  let vendorUserDoc = await VendorUserModel.findOne({
    mobile_number: mobileNumber
  })
  if(!isNullOrUndefined(vendorUserDoc)){
    return res.status(400).json({
      error: {
        message: `${mobileNumber} is already associated with another vendor account.`,
      },
    })
  }

  let adminUserDoc = await AdminUserModel.findOne({
    mobile_number: mobileNumber
  })
  if(!isNullOrUndefined(adminUserDoc)){
    return res.status(400).json({
      error: {
        message: `${mobileNumber} is already associated with another admin account.`,
      },
    })
  }

  const check = await BuyerUserModel.findOne({
    mobile_number: mobileNumber,
  }).exec()
  if (check === null) {
    return next()
  }
  return res.status(400).json({
    error: {
      message: `${mobileNumber} is already associated with another account.`,
    },
  })
}

export const assertThatEmailNotRegistered: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email } = req.params
  // email is compulsory
  if (isNullOrUndefined(email)) {
    return res.status(422).json({
      message: "Invalid Request.",
      details: [
        {
          path: "email",
          description: "email is required.",
        },
      ],
    })
  }

  let vendorUserDoc = await VendorUserModel.findOne({
    email: email
  })
  if(!isNullOrUndefined(vendorUserDoc)){
    return res.status(400).json({
      error: {
        message: `${email} is already associated with another vendor account.`,
      },
    })
  }

  let adminUserDoc = await AdminUserModel.findOne({
    email: email
  })
  if(!isNullOrUndefined(adminUserDoc)){
    return res.status(400).json({
      error: {
        message: `${email} is already associated with another admin account.`,
      },
    })
  }

  const check = await BuyerUserModel.findOne({
    email,
  }).exec()
  if (check === null) {
    return next()
  }
  return res.status(400).json({
    error: {
      message: `${email} is already associated with another account.`,
    },
  })
}
