import { inject, injectable } from "inversify"
import { AuthModel, IAuthDoc } from "../../model/auth.model"
import { client_mail_template } from "../../utilities/client_email_tempalte"
//import { generateRandomNumber } from "../../utilities/generate-random-number"
import { InvalidInput } from "../../utilities/customError"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { MailGunService } from "../../utilities/mailgun.service"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { BuyerTypes } from "../buyer.types"
//import { BuyerTypes } from "../buyer.types"
// import { CommonService } from "../../utilities/common.service"
//import { MailService } from "../../utilities/mail-service"
//import { logger } from "../../logger"

@injectable()
export class BuyerAuthRepository {
  constructor(
    @inject(BuyerTypes.mailgunService) private mailgunService: MailGunService
   // @inject(BuyerTypes.MailService) private mailService: MailService
  ) {}
  async getBuyerAuthCode(
    identifier: string,
    user_id?: string
  ): Promise<IAuthDoc> {
     const code = generateRandomNumber()
    const $setOnInsert: { [k: string]: any } = {
      identifier,
      user_type: "buyer",
      code,
    }
    if (!isNullOrUndefined(user_id)) {
      $setOnInsert.user_id = user_id
    }
    const authData = await AuthModel.findOneAndUpdate(
      { identifier, user_type: "buyer" },
      {
        $set: { created_at: Date.now() },
        $setOnInsert,
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }
    console.log("authData", authData)
    return Promise.resolve(authData)
  }
  async checkVerificationCodeWithDb(
    identifier: string,
    user_type: string,
    code: string
  ): Promise<IAuthDoc | null> {
    return AuthModel.findOne({ identifier, code, user_type }).exec()
  }
  async sendEmailforRegistration(identifier: string, user_id?: string) {
    const code = generateRandomNumber()
    const $setOnInsert: { [k: string]: any } = {
      identifier,
      user_type: "buyer",
      code,
    }
    if (!isNullOrUndefined(user_id)) {
      $setOnInsert.user_id = user_id
    }
    const authData = await AuthModel.findOneAndUpdate(
      { identifier, user_type: "buyer" },
      {
        $set: { created_at: Date.now() },
        $setOnInsert,
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let mail_html = client_mail_template.registration_otp
    mail_html = mail_html.replace("{{code}}", authData.code)
    var data = {
      from: "no-reply@conmate.in",
      to: identifier,
      subject: 'Conmix - Registration One Time Password',
      html : mail_html
    }
   await this.mailgunService.sendEmail(data)
  }
}
