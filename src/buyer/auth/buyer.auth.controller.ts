import { controller, httpPost, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import * as Joi from "joi"
import * as request from "request"
import { BuyerTypes } from "../buyer.types"
import { NextFunction, Response } from "express"
import { verifySessionToken } from "../../middleware/auth-token.middleware"
import {
  getFBCustomTokenSchema,
  getGoogleCustomTokenSchema,
  IGetAuthenticationCodeOnMobileRequest,
  verifyAuthenticationCodeSchema,
  IVerifyAuthenticationCodeForMobileRequest,
  loginUserSchema,
  ILoginUserRequest,
  IGetEmailRegistrationRequest,
  EmailRegistrationSchema,
} from "./buyer.auth.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  BuyerUserRepository,
  getUserIdentifierType,
  // UserIdentifiers,
} from "../user/buyer.user.repository"
import { createJWTToken } from "../../utilities/jwt.utilities"
import {
  assertThatMobileNumberNotRegistered,
  assertThatEmailNotRegistered,
} from "../user/buyer.user.middleware"
//import { TwilioService } from "../../utilities/twilio-service"
import { BuyerAuthRepository } from "./buyer.auth.repository"
import { validate } from "../../middleware/joi.middleware"
import { TestService } from "../../utilities/test.service"
import { MailGunService } from "../../utilities/mailgun.service"
import { client_mail_template } from "../../utilities/client_email_tempalte"

@controller("/auth", verifySessionToken)
@injectable()
export class BuyerAuthController {
  constructor(
    @inject(BuyerTypes.BuyerUserRepository)
    private buyerUserRepo: BuyerUserRepository,
    @inject(BuyerTypes.mailgunService) private mailgunService: MailGunService,
    @inject(BuyerTypes.TestService) private testService: TestService,
    @inject(BuyerTypes.BuyerAuthRepository)
    private authRepo: BuyerAuthRepository,
    
  ) {}

  @httpPost("/facebook/tokens/:token")
  async facebookUserJwt(req: any, res: Response, next: NextFunction) {
    const { token } = req.params
    request.get(
      `https://graph.facebook.com/v2.11/me?access_token=${token}&fields=id,name,email&format=json&method=get`,
      undefined,
      async (error, resp, body) => {
        if (!isNullOrUndefined(error)) {
          return res.status(400).json({
            error: { message: "Error in Facebook Sign in." },
          })
        }
        let result = Joi.validate(body, getFBCustomTokenSchema)
        if (!isNullOrUndefined(result.error)) {
          return res.status(400).json({
            error: {
              message: "Error in Facebook Sign in.",
              details: [{ path: "token", message: "invalid token." }],
            },
          })
        }
        const bodyObj = JSON.parse(body)
        const { email, name: full_name } = bodyObj
        const userInfo = await this.buyerUserRepo.createAndGetUser({
          email,
          full_name,
          signup_type: "facebook",
        })
        const jwtObj = {
          user_type: "buyer",
          full_name,
          uid: userInfo._id,
          email_verified: true,
        }
        const customToken = await createJWTToken(jwtObj, "30d")
        res.status(200).json({ data: { customToken } })
      }
    )
  }
  @httpPost("/google/tokens/:token")
  async googleUserJwt(req: any, res: Response, next: NextFunction) {
    const { token } = req.params
    request.get(
      `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`,
      undefined,
      async (error, resp, body) => {
        if (!isNullOrUndefined(error)) {
          return res.status(400).json({
            error: { message: "Error in Google Sign in." },
          })
        }
        let result = Joi.validate(body, getGoogleCustomTokenSchema, {
          abortEarly: false,
          allowUnknown: true,
        })
        if (!isNullOrUndefined(result.error)) {
          return res.status(400).json({
            error: {
              message: "Error in Google Sign in.",
              details: [{ path: "token", message: "Invalid token." }],
            },
          })
        }
        const { email, exp, name: full_name, picture: image } = JSON.parse(body)
        if (exp < Date.now() / 1000) {
          return res.status(400).json({
            error: {
              message: "Error in Google Sign in.",
              details: [{ path: "token", message: "Token expired." }],
            },
          })
        }
        const userInfo = await this.buyerUserRepo.createAndGetUser({
          email,
          full_name,
          image,
          signup_type: "google",
        })
        const jwtObj = {
          user_type: "buyer",
          full_name,
          uid: userInfo._id,
        }
        const customToken = await createJWTToken(jwtObj, "30d")
        res.status(200).json({ data: { customToken } })
      }
    )
  }

  /**
   * Used to get auth code(OTP) on mobile number during Signup.
   * @param req
   * @param res
   * @param next
   */
  @httpGet("/mobiles/:mobileNumber", assertThatMobileNumberNotRegistered)
  async getAuthenticationCodeOnMobile(
    req: IGetAuthenticationCodeOnMobileRequest,
    res: Response,
    next: NextFunction
  ) {
    const { mobileNumber } = req.params
    // const { method = "sms" } = req.query
    try {
      const auth = await this.authRepo.getBuyerAuthCode(mobileNumber)
      console.log("autgdgdgdgdgdgh", auth)
      if (auth === null) {
        return res
          .status(400)
          .json({
            error: { message: "We are unable to send OTP to your mobile no." },
          })
      }

      let message = `${auth.code} is your OTP for registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Registration OTP.	
      let templateId = "1707163532596026621"
      await this.testService.sendSMS(mobileNumber, message, templateId)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /** Verify mobile number to get registration token (User should not registered on our database) */
  @httpPost(
    "/mobiles/:mobileNumber/code/:code",
    validate(verifyAuthenticationCodeSchema)
  )
  async verifyAuthenticationCodeForMobile(
    req: IVerifyAuthenticationCodeForMobileRequest,
    res: Response,
    next: NextFunction
  ) {
    let { mobileNumber, code } = req.params
    const codeRow = await this.authRepo.checkVerificationCodeWithDb(
      mobileNumber,
      "buyer",
      code
    )
    if (isNullOrUndefined(codeRow)) {
      return res
        .status(400)
        .json({
          error: { message: "You entered a wrong OTP. Kindly try again." },
        })
    }
    const jwtObj = {
      user_type: "buyer",
      full_name: "",
      uid: codeRow._id,
    }
    const customToken = await createJWTToken(jwtObj, "30d")
    console.log("hereeeeee111")
    console.log("respp", customToken)

    res.status(200).json({ data: { customToken } })
  }

  /**
   * Get auth code for a user that exists in the database.
   * This api will be called when a user with a mobile number
   * has forgotten his/her password.
   * @param req Express Authenticated Request
   * @param res Express Response
   * @param next Express Next Function
   */
  @httpGet("/users/:identifier")
  async resetPasswordReq(req: any, res: Response, next: NextFunction) {
    const { identifier } = req.params
    //const { method = "sms" } = req.query

    try {
      const identifierType = getUserIdentifierType(identifier)
      const user = await this.buyerUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (user === null) {
        return res.status(404).json({
          error: {
            message: `Sorry. No user found with this email Id/mobile no.`,
          },
        })
      }

      if (isNullOrUndefined(user.mobile_number)) {
        return res.status(400).json({
          error: {
            message:
              "This User is not registered with entered mobile number. Password recovery request is Invalid",
          },
        })
      }      
      const auth = await this.authRepo.getBuyerAuthCode(identifier)
      if (auth === null) {
        return next("We are unable to send OTP to your mobile no.")
      }

      let client_name: any
      // let mobile_number: any
      // let email: any
      if (!isNullOrUndefined(user)) {
        // mobile_number = user.mobile_number
        // email = user.email
        if (user.account_type === "Individual") {
          client_name = user.full_name
        } else {
          client_name = user.company_name
        }
      }
      let forgotpwd_html = client_mail_template.forgot_password_otp
      forgotpwd_html = forgotpwd_html.replace("{{client_name}}", client_name)
      forgotpwd_html = forgotpwd_html.replace("{{code}}", auth.code)
      var data = {
        from: "no-reply@conmate.in",
        to: user.email,
        subject: 'Conmix - Forgot Password One Time Password',
        html : forgotpwd_html
      }
     await this.mailgunService.sendEmail(data)

      let message = `"${auth.code}" is your OTP to Reset Password at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Reset password.
      let templateId = "1707163697744114693"
      await this.testService.sendSMS(user.mobile_number, message, templateId)
      // await this.twilioService.sendCode(user.mobile_number, auth.code, method)
      return res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /**
   * This API is called when the user with mobile number
   * wants to verify the code sent to him/her. (Forgot password/ for registered user only)
   * @param req
   * @param res
   * @param next
   */
  @httpPost("/users/:identifier/codes/:code")
  async verifyAuthCodeForUser(req: any, res: Response, next: NextFunction) {
    const { identifier, code } = req.params

    try {
      const identifierType = getUserIdentifierType(identifier)

      const user = await this.buyerUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (isNullOrUndefined(user)) {
        return res.status(400).json({
          error: {
            message: `Sorry. No user found with this email Id/mobile no.`,
          },
        })
      }

      const codeRow = await this.authRepo.checkVerificationCodeWithDb(
        identifier,
        "buyer",
        code
      )

      if (isNullOrUndefined(codeRow)) {
        return res
          .status(400)
          .json({
            error: { message: "You entered the wrong OTP. Kindly try again" },
          })
      }
      const jwtObj = {
        user_type: "buyer",
        full_name: user.full_name,
        uid: user._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")

      res.status(200).json({ data: { customToken } })
    } catch (err) {
      next(err)
    }
  }
  /** User login with identifier */
  @httpPost("/:identifier", validate(loginUserSchema))
  async loginUserJwt(
    req: ILoginUserRequest,
    res: Response,
    next: NextFunction
  ) {
    const { identifier } = req.params
    const { password } = req.body

    try {
      const identifierType = getUserIdentifierType(identifier)
      const errorMsg = `Kindly enter valid email/mobile no. and password.`
      const user = await this.buyerUserRepo.getUserWithPassword(
        identifierType,
        identifier,
        password
      )

      if (user === null) {
        return res.status(400).json({ error: { message: errorMsg } })
      }
      const jwtObj = {
        user_type: "buyer",
        full_name: user.full_name,
        uid: user._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Used to get email during Signup.
   * @param req
   * @param res
   * @param next
   */
  @httpGet(
    "/email/:email",
    validate(EmailRegistrationSchema),
    assertThatEmailNotRegistered
  )
  async sendEmailforRegistration(
    req: IGetEmailRegistrationRequest,
    res: Response,
    next: NextFunction
  ) {
    const { email } = req.params
    try {
      const message = await this.authRepo.sendEmailforRegistration(email)
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }
}
