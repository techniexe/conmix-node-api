import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ICard {
  user_id: string
  card_number: string
  card_name: string
  expiry_month: number
  expiry_year: number
}

export interface IAddCardDetailsRequest extends IAuthenticatedRequest {
  body: {
    card: ICard
  }
}
