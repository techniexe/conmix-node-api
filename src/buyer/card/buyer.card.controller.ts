import { injectable, inject } from "inversify"
import { controller, httpPost, httpGet } from "inversify-express-utils"
import { BuyerTypes } from "../buyer.types"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { CardRepository } from "./buyer.card.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import { addCardDetailsSchema } from "./buyer.card.request-schema"
import { IAddCardDetailsRequest } from "./buyer.card.request-definations"

@controller("/card", verifyCustomToken("buyer"))
@injectable()
export class CardController {
  constructor(
    @inject(BuyerTypes.CardRepository) private cardRepo: CardRepository
  ) {}

  @httpPost("/", validate(addCardDetailsSchema))
  async addCardDetails(
    req: IAddCardDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    const { card } = req.body

    try {
      const { _id } = await this.cardRepo.addCardDetails(uid, card)
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getCardDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.cardRepo.getCardDetails(uid)

      return res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
