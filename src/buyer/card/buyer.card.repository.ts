import { injectable } from "inversify"
import { ICard } from "./buyer.card.request-definations"
import { CardModel, ICardDoc } from "../../model/card.model"

@injectable()
export class CardRepository {
  async addCardDetails(user_id: string, card: ICard) {
    card.user_id = user_id
    const newCard = new CardModel(card)
    const newcardDoc = await newCard.save()
    return Promise.resolve(newcardDoc)
  }

  async getCardDetails(user_id: string): Promise<ICardDoc[] | null> {
    let query: { [k: string]: any } = { user_id }
    return CardModel.find(query)
  }
}
