import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"

export const addCardDetailsSchema: IRequestSchema = {
  body: Joi.object().keys({
    card: Joi.object()
      .keys({
        card_number: Joi.string().required(),
        card_name: Joi.string().required(),
        expiry_month: Joi.string()
          .required()
          .valid([
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
          ]),
        expiry_year: Joi.number().required(),
      })
      .required(),
  }),
}
