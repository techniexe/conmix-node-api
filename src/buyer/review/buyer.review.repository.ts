import { injectable } from "inversify"
import { IAddVendorReview } from "./buyer.review.req-schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { VendorUserModel } from "../../model/vendor.user.model"
import { InvalidInput } from "../../utilities/customError"
import { ReviewModel } from "../../model/review.model"
import { ObjectId } from "bson"
import { OrderItemModel } from "../../model/order_item.model"
import { OrderItemStatus } from "../../utilities/config"

@injectable()
export class ReviewRepository {
  async addVendorReview(user_id: string, reviewData: IAddVendorReview) {
    if (!isNullOrUndefined(reviewData.rating)) {
      await VendorUserModel.findOneAndUpdate(
        { _id: reviewData.vendor_id },
        { $inc: { rating_count: 1, rating_sum: reviewData.rating } }
      ).exec()
    }

    const vendorData = await VendorUserModel.findById(reviewData.vendor_id)

    if (isNullOrUndefined(vendorData)) {
      return Promise.reject(new InvalidInput(`No Vendor record could be found`, 400))
    }

    reviewData.user_id = user_id
    const vendorreview = new ReviewModel(reviewData)
    const vendorreviewDoc = await vendorreview.save()

    let rating_sum = vendorData.rating_sum
    let rating_count = vendorData.rating_count
    await VendorUserModel.updateOne(
      { _id: reviewData.vendor_id },
      { $set: { rating: rating_sum / rating_count } }
    ).exec()

    return Promise.resolve(vendorreviewDoc)
  }

  async getVendorReview(
    user_id: string,
    vendor_id: string,
    before?: string,
    after?: string
  ) {
    console.log("user_id", user_id)
    console.log("vendor_id", vendor_id)

    const query: { [k: string]: any } = {
      status: "published",
    }
    query.vendor_id = new ObjectId(vendor_id)

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
      sort.created_at = 1
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "buyer",
          localField: "user_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: 1,
          "buyer._id": 1,
          "buyer.full_name": 1,
          "vendor.full_name": 1,
          //sub_category_id: 1,
          vendor_id: 1,
          review_text: 1,
          rating: 1,
          status: 1,
          created_at: 1,
          modirated_at: 1,
        },
      },
    ]

    const reviews = await ReviewModel.aggregate(aggregateArr).limit(40)

    if (sort.created_at === 1) {
      reviews.reverse()
    }

    const data = await OrderItemModel.findOne({
      buyer_id: new ObjectId(user_id),
      vendor_id: new ObjectId(vendor_id),
      item_status: { $eq: OrderItemStatus.DELIVERED },
    })

    let write_review: boolean = false
    if (!isNullOrUndefined(data)) {
      write_review = true
    }
    return { write_review, reviews }
  }
}
