import { BuyerTypes } from "./../buyer.types"
import { controller, httpPost, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { ReviewRepository } from "./buyer.review.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addVendorReviewSchema,
  IAddVendorReviewRequest,
  GetVendorreviewSchema,
  IGetVendorreview,
} from "./buyer.review.req-schema"

@controller("/review", verifyCustomToken("buyer"))
@injectable()
export class ReviewController {
  constructor(
    @inject(BuyerTypes.ReviewRepository)
    private reviewRepo: ReviewRepository
  ) {}

  //Add vendor review.
  @httpPost("/", validate(addVendorReviewSchema))
  async addVendorReview(
    req: IAddVendorReviewRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { _id } = await this.reviewRepo.addVendorReview(uid, req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  //Get vendor review.
  @httpGet("/getVendorReview/:vendor_id", validate(GetVendorreviewSchema))
  async getVendorReview(
    req: IGetVendorreview,
    res: Response,
    next: NextFunction
  ) {
    const { vendor_id } = req.params
    const { before, after } = req.query
    try {
      const data = await this.reviewRepo.getVendorReview(
        req.user.uid,
        vendor_id,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
