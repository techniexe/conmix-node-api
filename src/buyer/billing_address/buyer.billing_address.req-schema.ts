import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IBillingAddress {
    full_name: string
    company_name: string
    line1: string
    line2?: string
    state_id: string
    city_id: string
    pincode: number
    gst_number: string
  }
  export interface IBillingddressRequest extends IAuthenticatedRequest {
      body: IBillingAddress
    }

  export const billingAddressSchema: IRequestSchema = {
      body: Joi.object().keys({
        full_name: Joi.string(),
        company_name: Joi.string().required(),
        line1: Joi.string().required(),
        line2: Joi.string(),
        state_id: Joi.string().required(),
        city_id: Joi.string().required(),
        pincode: Joi.number().min(6).required(),
        gst_number: Joi.string(),
      }),
    }

    export interface IEditBillingAddress {
        full_name: string
        company_name: string
        line1: string
        line2?: string
        state_id: string
        city_id: string
        pincode: number
        gst_number: string
      }
      export const editbillingAddressSchema: IRequestSchema = {
        params: Joi.object().keys({
          addressId: Joi.string().regex(objectIdRegex),
        }),
        body: Joi.object().keys({
          full_name: Joi.string(),
          company_name: Joi.string().required(),
          line1: Joi.string().required(),
          line2: Joi.string(),
          state_id: Joi.string().required(),
          city_id: Joi.string().required(),
          pincode: Joi.number().min(6).required(),
          gst_number: Joi.string(),
        }),
      }

      export interface IEditBillingAddressRequest extends IAuthenticatedRequest {
        params: {
          addressId: string
        }
        body: IEditBillingAddress
      }
      
      export const deletebillingAddressSchema: IRequestSchema = {
        params: Joi.object().keys({
          addressId: Joi.string().regex(objectIdRegex).required(),
        }),
      }
      export interface IDeleteBillingAddressRequest extends IAuthenticatedRequest {
        params: {
          addressId: string
        }
      }
      
