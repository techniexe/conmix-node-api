import { inject, injectable } from "inversify"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { CommonService } from "../../utilities/common.service"
import { BuyerTypes } from "../buyer.types"
import {
  IBillingAddress,
  IEditBillingAddress,
} from "./buyer.billing_address.req-schema"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { BuyerBillingAddressModel } from "../../model/buyer_billing_address.model"
import { CartModel } from "../../model/cart.model"

@injectable()
export class BillingAddressRepository {
  constructor(
    @inject(BuyerTypes.CommonService) private commonService: CommonService
  ) {}
  async addBillingAddress(user_id: string, addressData: IBillingAddress) {
    let buyerUserDoc = await BuyerUserModel.findOne({
      _id: new ObjectId(user_id),
    })
    if (isNullOrUndefined(buyerUserDoc)) {
      return Promise.reject(new InvalidInput(`No Buyer user Doc Found.`, 400))
    }
    const newDoc: any = {
      _id: new ObjectId(),
      full_name: addressData.full_name,
      company_name: addressData.company_name,
      line1: addressData.line1,
      line2: addressData.line2,
      state_id: addressData.state_id,
      city_id: addressData.city_id,
      pincode: addressData.pincode,
      gst_number: addressData.gst_number,
      user: {
        user_type: "buyer",
        user_id,
      },
    }
    const newBillingAddress = new BuyerBillingAddressModel(newDoc)
    const newBillingAddressDoc = await newBillingAddress.save()
    await this.commonService.addActivityLogs(
      user_id,
      "buyer",
      AccessTypes.ADD_BILLING_ADDRESS_BY_BUYER,
      `Buyer added billing address of id ${newBillingAddressDoc._id} .`
    )
    return Promise.resolve(newBillingAddressDoc)
  }

  async getAddress(
    user_id: string,
    search?: string,
    before?: string,
    after?: string
  ) {
    let query: { [k: string]: any } = {
      "user.user_id": new ObjectId(user_id),
      "user.user_type": "buyer",
      is_deleted: false
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    console.log(query)
    return BuyerBillingAddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      // {
      //   $limit: 10,
      // },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "user.user_id",
          foreignField: "_id",
          as: "buyerDetails",
        },
      },
      {
        $unwind: { path: "$buyerDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          full_name: 1,
          company_name: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          gst_number: 1,
          gst_image_url: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          created_at: 1,
          updated_at: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "buyerDetails.account_type": 1,
        },
      },
    ])
    // return AddressModel.find(query).limit(40)
  }

  async editBillingAddress(
    user_id: string,
    addressId: string,
    addressData: IEditBillingAddress
  ): Promise<any> {
    const fields = [
      "full_name",
      "company_name",
      "line1",
      "line2",
      "state_id",
      "city_id",
      "pincode",
      "gst_number",
    ]
    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(addressData)) {
      if (fields.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    console.log("addressDataaaaaaaaaaaaaaaaaaaaaaaaa", addressData)
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const res = await BuyerBillingAddressModel.findOneAndUpdate(
      {
        _id: addressId,
        "user.user_id": user_id,
        "user.user_type": "buyer",
      },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This address doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "buyer",
      AccessTypes.UPDATE_BILLING_ADDRESS_BY_BUYER,
      `Buyer updated details Of billing Address of id ${addressId} .`,
      res,
      editDoc
    )
    return Promise.resolve()
  }

  async deleteBillingAddress(user_id: string, addressId: string) {
    const updtRes = await BuyerBillingAddressModel.updateOne(
      {
        _id: addressId,
        "user.user_id" : user_id
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Delete failed as no record exists`, 400)
      )
    }

    await CartModel.updateOne(
      {
        user_id: new ObjectId(user_id),
        billing_address_id : new ObjectId(addressId)
      },
      { $unset: { billing_address_id: "" } }
    )
    return Promise.resolve()
  }
}
