import { NextFunction, Response } from "express"
import { inject, injectable } from "inversify"
import { controller, httpDelete, httpGet, httpPatch, httpPost } from "inversify-express-utils"
import { IAuthenticatedRequest, verifyCustomToken } from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { BuyerTypes } from "../buyer.types"
import { BillingAddressRepository } from "./buyer.billing_address.repository"
import { billingAddressSchema, deletebillingAddressSchema, editbillingAddressSchema, IBillingddressRequest, IDeleteBillingAddressRequest, IEditBillingAddressRequest } from "./buyer.billing_address.req-schema"

@injectable()
@controller("/billing_address", verifyCustomToken("buyer"))
export class BillingAddressController {
    constructor(
        @inject(BuyerTypes.BillingAddressRepository)
        private billingaddressRepo: BillingAddressRepository
      ) {}
      @httpPost("/", validate(billingAddressSchema))
      async addBillingAddress(req: IBillingddressRequest, res: Response, next: NextFunction) {
        try {
          const { _id } = await this.billingaddressRepo.addBillingAddress(req.user.uid, req.body)
          res.json({ data: { _id } })
        } catch (err) {
          next(err)
        }
      }

      @httpGet("/")
      async getAddress(
        req: IAuthenticatedRequest,
        res: Response,
        next: NextFunction
      ) {
        try {
          const { uid } = req.user
          const { search, before, after } = req.query
          const data = await this.billingaddressRepo.getAddress(uid, search, before, after)
          res.json({ data })
        } catch (err) {
          next(err)
        }
      }

      @httpPatch("/:addressId", validate(editbillingAddressSchema))
      async editBillingAddress(
        req: IEditBillingAddressRequest,
        res: Response,
        next: NextFunction
      ) {
        try {
          await this.billingaddressRepo.editBillingAddress(
            req.user.uid,
            req.params.addressId,
            req.body
          )
          res.sendStatus(202)
        } catch (err) {
          next(err)
        }
      }

      @httpDelete("/:addressId", validate(deletebillingAddressSchema))
      async deleteBillingAddress(
        req: IDeleteBillingAddressRequest,
        res: Response,
        next: NextFunction
      ) {
        try {
          await this.billingaddressRepo.deleteBillingAddress(
            req.user.uid,
            req.params.addressId
          )
          res.sendStatus(202)
        } catch (err) {
          next(err)
        }
      }
}