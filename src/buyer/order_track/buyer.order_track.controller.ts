import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { BuyerTypes } from "../buyer.types"
import { OrderTrackRepository } from "./buyer.order_track.repository"
import {
  IGetTrackedOrderRequest,
  getTrackedOrderItemSchema,
  getOrderTrackSchema,
  IGetOrderTrackRequest,
  getOrderItemTrackSchema,
  IGetOrderItemTrackRequest,
  IGetOrderItemCPTrackRequest,
  getOrderItemCPTrackSchema,
} from "./buyer.order_track.req-schema"

@injectable()
@controller("/order_track", verifyCustomToken("buyer"))
export class OrderTrackController {
  constructor(
    @inject(BuyerTypes.OrderTrackRepository)
    private orderTrackRepo: OrderTrackRepository
  ) {}

  @httpGet("/orderItem/:order_id", validate(getTrackedOrderItemSchema))
  async getTrackedItem(
    req: IGetTrackedOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_id } = req.params
      const data = await this.orderTrackRepo.getTrackedItem(
        req.user.uid,
        order_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  // get tracking details for item. [Used for web.]
  // @httpGet("/CP/:order_item_part_id", validate(getOrderItemCPTrackSchema))
  @httpGet("/CP/:order_item_part_id", validate(getOrderItemCPTrackSchema))
  async getCPTrack(
    req: IGetOrderItemCPTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_part_id } = req.params

      const data = await this.orderTrackRepo.getCPTrack(order_item_part_id)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:order_item_id/:tracking_id", validate(getOrderTrackSchema))
  async getOrderTrack(
    req: IGetOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id, tracking_id } = req.params

      const data = await this.orderTrackRepo.getOrderTrack(
        order_item_id,
        tracking_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  // get tracking details for item. [Used for web.]
  @httpGet("/:order_item_id", validate(getOrderItemTrackSchema))
  async getOrderItemTrack(
    req: IGetOrderItemTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id } = req.params

      const data = await this.orderTrackRepo.getOrderItemTrack(order_item_id)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
