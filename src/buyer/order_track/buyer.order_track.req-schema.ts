import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IGetOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    tracking_id: string
  }
}

export const getOrderTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
    tracking_id: Joi.string(),
  }),
}

export interface IGetTrackedOrderRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
  }
}

export const getTrackedOrderItemSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetOrderItemTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
  }
}

export const getOrderItemTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
  }),
}

export const getCPTrackedOrderItemSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetOrderItemCPTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_part_id: string
  }
}

export const getOrderItemCPTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_part_id: Joi.string().regex(objectIdRegex),
  }),
}
