import { OrderModel } from "./../../model/order.model"
import { injectable } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"

import { OrderTrackModel } from "../../model/order_track.model"
import { CPOrderTrackModel } from "../../model/CP_order_track.model"
import { ObjectId } from "bson"
import { LogisticsOrderStatus } from "../../model/logistics.order.model"

// import { OrderTrackModel } from "../../model/order_track.model"
// import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class OrderTrackRepository {
  async getTrackedItem(user_id: string, order_id: string) {
    console.log("order_id", order_id)
    console.log("user_id", user_id)
    const buyerOrderData = await OrderModel.findOne({
      _id: new ObjectId(order_id),
      user_id,
    })

    if (isNullOrUndefined(buyerOrderData)) {
      return Promise.reject(new InvalidInput(`No Order could be found`, 400))
    }

    const query: { [k: string]: any } = {
      buyer_order_id: new ObjectId(order_id),
      event_status: LogisticsOrderStatus.TM_ASSIGNED,
      // assigned_at: { $gte: new Date(Date.now()) },
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      // {
      //   $lookup: {
      //     from: "design_mix_variant",
      //     localField: "orderItemDetails.design_mix_id",
      //     foreignField: "_id",
      //     as: "designMixDetails",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$designMixDetails",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "design_mix_variant",
          localField: "orderItemDetails.design_mix_id",
          foreignField: "_id",
          as: "designMixDetails",
        },
      },
      {
        $unwind: {
          path: "$designMixDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TMDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TMDetails.TM_category_id",
          foreignField: "_id",
          as: "TMCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TMDetails.TM_sub_category_id",
          foreignField: "_id",
          as: "TMSubCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMSubCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "TMDetails.driver1_id",
          foreignField: "_id",
          as: "TMDriverDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDriverDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "designMixDetails.grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "designMixDetails.cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "designMixDetails.sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "designMixDetails.aggregate_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "designMixDetails.aggregate1_sub_category_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "designMixDetails.aggregate2_sub_category_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "designMixDetails.fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "designMixDetails.ad_mixture_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "cement_grade",
          localField: "designMixDetails.cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "designMixDetails.ad_mixture_category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          order_item_id: true,
          pickup_quantity: true,
          event_status: true,
          assigned_at: true,
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          vendor_order_id: "$vendorOrderDetails._id",
          designMixDetails: true,
          //invoice_url: true,
          "pickup_address_info.pickup_location":
            "$pickup_address_info.location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.pincode": true,
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sand_category._id": 1,
          "aggregate1_sand_category.sub_category_name": 1,
          "aggregate2_sand_category._id": 1,
          "aggregate2_sand_category.sub_category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          "TMDetails._id": 1,
          "TMDetails.TM_rc_number": 1,
          "TMCategoryDetails._id": 1,
          "TMCategoryDetails.category_name": 1,
          "TMSubCategoryDetails._id": 1,
          "TMSubCategoryDetails.sub_category_name": 1,
          "TMDriverDetails._id": 1,
          "TMDriverDetails.driver_name": 1,
          "TMDriverDetails.driver_mobile_number": 1,
          TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true
        },
      },
    ]

    const orderTrackData = await OrderTrackModel.aggregate(aggregateArr)
    if (orderTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(`Your request for tracking is invalid as we couldn't find Order item`, 400)
      )
    }

    return Promise.resolve(orderTrackData)
  }
  async getOrderTrack(order_item_id: string, tracking_id: string) {
    const query: { [k: string]: any } = {
      order_item_id: new ObjectId(order_item_id),
      _id: new ObjectId(tracking_id),
      // assigned_at: { $gte: new Date(Date.now()) },
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TMDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TMDetails.TM_category_id",
          foreignField: "_id",
          as: "TMCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TMDetails.TM_sub_category_id",
          foreignField: "_id",
          as: "TMSubCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMSubCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "TMDetails.driver1_id",
          foreignField: "_id",
          as: "TMDriverDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDriverDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "design_mix_variant",
          localField: "orderItemDetails.design_mix_id",
          foreignField: "_id",
          as: "designMixDetails",
        },
      },
      {
        $unwind: {
          path: "$designMixDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "orderItemDetails.concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "orderItemDetails.cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "orderItemDetails.sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "orderItemDetails.agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "orderItemDetails.aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "orderItemDetails.aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "orderItemDetails.fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "orderItemDetails.admix_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "cement_grade",
          localField: "orderItemDetails.cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "orderItemDetails.admix_cat_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          buyer_order_id: true,
          order_item_id: true,
          TM_id: true,
          pickup_quantity: true,
          royality_quantity: true,
          reasonForDelay: true,
          delayTime: true,
          event_status: true,
          assigned_at: true,
          pickedup_at: true,
          delivered_at: true,
          delayed_at: true,
          designMixDetails: true,
          invoice_url: true,
          design_mix_id: "$orderItemDetails.design_mix_id",
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          processed_at: "$order.processed_at",
          //TM_rc_number: "$TMDetails.TM_rc_number",
          TM_category: "$TMCategoryDetails.category_name",
          TM_sub_category: "$TMSubCategoryDetails.sub_category_name",
          driver_name: "$TMDriverDetails.driver_name",
          driver_mobile_number: "$TMDriverDetails.driver_mobile_number",
          concrete_grade_name: "$concrete_grade.name",
          cement_brand_name: "$cement_brand.name",
          sand_source_name: "$sand_source.sand_source_name",
          aggregate_source_name: "$aggregate_source.aggregate_source_name",
          aggregate1_sub_category_name:
            "$aggregate1_sand_category.sub_category_name",
          aggregate2_sub_category_name:
            "$aggregate2_sand_category.sub_category_name",
          fly_ash_source_name: "$fly_ash_source.fly_ash_source_name",
          admixture_brand_name: "$admixture_brand.name",
          cement_grade_name: "$cement_grade.name",
          admixture_category_name: "$admixture_category.category_name",
          qube_test_report_7days: true,
          qube_test_report_28days: true,
          TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true
        },
      },
    ]

    const orderTrackData = await OrderTrackModel.aggregate(aggregateArr)
    if (orderTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(`Your request for tracking is invalid as we couldn't find Order item`, 400)
      )
    }

    return Promise.resolve(orderTrackData[0])
  }
  async getOrderItemTrack(order_item_id: string) {
    const query: { [k: string]: any } = {
      order_item_id: new ObjectId(order_item_id),
    }
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TMDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TMDetails.TM_category_id",
          foreignField: "_id",
          as: "TMCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TMDetails.TM_sub_category_id",
          foreignField: "_id",
          as: "TMSubCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMSubCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "TMDetails.driver1_id",
          foreignField: "_id",
          as: "TMDriverDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDriverDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "design_mix_variant",
          localField: "orderItemDetails.design_mix_id",
          foreignField: "_id",
          as: "designMixDetails",
        },
      },
      {
        $unwind: {
          path: "$designMixDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "designMixDetails.grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "designMixDetails.cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "designMixDetails.sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "designMixDetails.aggregate_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "designMixDetails.aggregate1_sub_category_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "designMixDetails.aggregate2_sub_category_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "designMixDetails.fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "designMixDetails.ad_mixture_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "cement_grade",
          localField: "designMixDetails.cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "designMixDetails.ad_mixture_category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          buyer_order_id: true,
          order_item_id: true,
          TM_id: true,
          pickup_quantity: true,
          royality_quantity: true,
          reasonForDelay: true,
          delayTime: true,
          event_status: true,
          assigned_at: true,
          pickedup_at: true,
          delivered_at: true,
          delayed_at: true,
          designMixDetails: true,
          qube_test_report_7days: true,
          qube_test_report_28days: true,
          design_mix_id: "$orderItemDetails.design_mix_id",
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          processed_at: "$order.processed_at",
          TM_rc_number: "$TMDetails.TM_rc_number",
          TM_category: "$TMCategoryDetails.category_name",
          TM_sub_category: "$TMSubCategoryDetails.sub_category_name",
          driver_name: "$TMDriverDetails.driver_name",
          driver_mobile_number: "$TMDriverDetails.driver_mobile_number",
          concrete_grade_name: "$concrete_grade.name",
          cement_brand_name: "$cement_brand.name",
          sand_source_name: "$sand_source.sand_source_name",
          aggregate_source_name: "$aggregate_source.aggregate_source_name",
          aggregate1_sub_category_name:
            "$aggregate1_sand_category.sub_category_name",
          aggregate2_sub_category_name:
            "$aggregate2_sand_category.sub_category_name",
          fly_ash_source_name: "$fly_ash_source.fly_ash_source_name",
          admixture_brand_name: "$admixture_brand.name",
          cement_grade_name: "$cement_grade.name",
          admixture_category_name: "$admixture_category.category_name",
         // TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true
       
        },
      },
    ]

    const orderTrackData = await OrderTrackModel.aggregate(aggregateArr)
    if (orderTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(`Your request for tracking is invalid as we couldn't find Order item`, 400)
      )
    }

    return Promise.resolve(orderTrackData)
  }

  async getCPTrack(order_item_part_id: string) {
    const query: { [k: string]: any } = {
      order_item_part_id: new ObjectId(order_item_part_id),
    }
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "concrete_pump",
          localField: "CP_id",
          foreignField: "_id",
          as: "CPDetails",
        },
      },
      {
        $unwind: {
          path: "$CPDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "operator_info",
          localField: "CPDetails.operator_id",
          foreignField: "_id",
          as: "CPOperatorDetails",
        },
      },
      {
        $unwind: {
          path: "$CPOperatorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          buyer_order_id: true,
          order_item_id: true,
          CP_id: true,
          reasonForDelay: true,
          delayTime: true,
          event_status: true,
          assigned_at: true,
          pickedup_at: true,
          delivered_at: true,
          delayed_at: true,
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          processed_at: "$order.processed_at",
          serial_number: "$CPDetails.serial_number",
          CP_company_name: "$CPDetails.CP_company_name",
          concrete_pump_model: "$CPDetails.concrete_pump_model",
          manufacture_year: "$CPDetails.manufacture_year",
          pipe_connection: "$CPDetails.pipe_connection",
          concrete_pump_capacity: "$CPDetails.concrete_pump_capacity",
          is_Operator: "$CPDetails.is_Operator",
          operator_name: "$CPOperatorDetails.operator_name",
          operator_mobile_number: "$CPOperatorDetails.operator_mobile_number",
          CP_serial_number: true,
          CP_category_name: true,
          CP_operator_name: true,
          CP_operator_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
          
        },
      },
    ]

    const orderCPTrackData = await CPOrderTrackModel.aggregate(aggregateArr)
    if (orderCPTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(`Your request for tracking is invalid as we couldn't find Order item`, 400)
      )
    }

    return Promise.resolve(orderCPTrackData[0])
  }
  //   const query: { [k: string]: any } = {}
  //   if (!isNullOrUndefined(item_id)) {
  //     query.item_id = item_id
  //   }
  //   if (!isNullOrUndefined(order_id)) {
  //     query.order_id = order_id
  //   }
  //   if (!isNullOrUndefined(logistics_order_id)) {
  //     query.logistics_order_id = logistics_order_id
  //   }
  //   if (!isNullOrUndefined(before) && before !== "") {
  //     query.created_at = { $lt: before }
  //   }
  //   if (!isNullOrUndefined(after) && after !== "") {
  //     query.created_at = { $gt: after }
  //   }
  //   if (!isNullOrUndefined(user_id)) {
  //     query.added_by_id = user_id
  //   }
  //   if (!isNullOrUndefined(user_type)) {
  //     query.added_by_type = user_type
  //   }
  //   return OrderTrackModel.find(query).sort({ created_at: -1 }).limit(40)
  // }
}
