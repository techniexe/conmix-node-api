import { injectable } from "inversify"
import { IFcmTokenDetails } from "./fcm-tokens.request-schema"

import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"

import { InvalidInput } from "../../utilities/customError"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { FcmTokensModel } from "../../model/fcm-tokens.model"

@injectable()
export class FcmTokensRepository {
  async addUpdateToken(user_id: string, tokenData: IFcmTokenDetails) {
    console.log("tokenData", tokenData)
    const setUpdt: { [k: string]: any } = {
      token: tokenData.token,
      platform: tokenData.platform,
      updated_at: Date.now(),
    }
    if (!isNullOrUndefined(tokenData.apn_token)) {
      setUpdt.apn_token = tokenData.apn_token
    }
    const userCheck = await BuyerUserModel.findOne({ _id: user_id })
    if (isNullOrUndefined(userCheck)) {
      return Promise.reject(new InvalidInput(`User could not be found`, 400))
    }

    await FcmTokensModel.findOneAndUpdate(
      { user_id, device_id: tokenData.deviceID },
      {
        $set: setUpdt,
        $setOnInsert: {
          created_at: Date.now(),
        },
      },
      { upsert: true }
    ).exec()
    return Promise.resolve()
  }

  async deleteByDeviceID(user_id: string, device_id: string): Promise<any> {
    return FcmTokensModel.deleteOne({
      user_id,
      device_id,
    })
  }

  async removeExpiredTokens(user_id: string | ObjectId, tokens: string[]) {
    return Promise.all(
      tokens.map((token) => FcmTokensModel.deleteOne({ user_id, token }))
    )
  }
  async handleFailedAPNsToken(
    apnResp: {
      device: string
      error?: Error
      status?: string
      response?: {
        reason: string
        timestamp?: string
      }
    }[],
    tokenId: ObjectId | string
  ) {
    apnResp.map((r) => {
      console.log(
        `APNs Failed. status: ${r.status}, reason: ${
          r.response && r.response.reason
        }`,
        r.status
      )
    })
    // return Promise.resolve()
    if (
      apnResp.length > 0 &&
      apnResp[0].status &&
      ["400", "410"].includes(apnResp[0].status)
    ) {
      console.log(`Deleting failed APN tokens: _id: ${tokenId}`)
      return FcmTokensModel.deleteOne({ _id: tokenId })
    }
    return Promise.resolve()
  }
}
