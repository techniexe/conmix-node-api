import { IRequestSchema } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import * as Joi from "joi"
import { Platforms } from "../../utilities/config"

export interface IFcmTokenDetails {
  token: string
  deviceID: string
  platform: Platforms
  apn_token?: string
}

export interface IAddTokenRequest extends IAuthenticatedRequest {
  body: IFcmTokenDetails
}

export const addTokenSchema: IRequestSchema = {
  body: Joi.object().keys({
    token: Joi.string().required(),
    deviceID: Joi.string().required(),
    platform: Joi.string()
      .valid(...Object.values(Platforms))
      .required(),
    apn_token: Joi.string(),
  }),
}

export interface IToken extends IFcmTokenDetails {
  uid: string
}

export interface IDeleteTokenRequest extends IAuthenticatedRequest {
  params: {
    deviceID: string
  }
}
