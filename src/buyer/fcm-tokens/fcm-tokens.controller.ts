import { injectable, inject } from "inversify"
import { httpPost, controller, httpDelete } from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"

import { NextFunction, Response } from "express"

import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import {
  addTokenSchema,
  IAddTokenRequest,
  IDeleteTokenRequest,
} from "./fcm-tokens.request-schema"
import { FcmTokensRepository } from "./fcm-tokens.repository"
import { BuyerTypes } from "../buyer.types"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/fcm-tokens", verifyCustomToken("buyer"))
export class FcmTokensController {
  constructor(
    @inject(BuyerTypes.FcmTokensRepository)
    private fcmTokensRepo: FcmTokensRepository
  ) {}

  @httpPost("/", validate(addTokenSchema))
  async addUpdateToken(
    req: IAddTokenRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user

    try {
      await this.fcmTokensRepo.addUpdateToken(uid, req.body)
      res.sendStatus(201)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added details of this token. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpDelete("/:deviceID")
  async deleteTokenForDevice(
    req: IDeleteTokenRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    const { deviceID } = req.params

    try {
      await this.fcmTokensRepo.deleteByDeviceID(uid, deviceID)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
}
