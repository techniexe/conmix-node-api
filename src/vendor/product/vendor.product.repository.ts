import { injectable, inject } from "inversify"
import { IProduct, IEditProduct } from "./vendor.product.req-schema"
import { ProductModel, IProductDoc } from "../../model/product.model"
import {
  AggregateSandSubCategoryModel,
  AggregateSandCategoryModel,
} from "../../model/aggregate_sand_category.model"
import { AddressModel } from "../../model/address.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { SnsService } from "../../utilities/sns.service"
import { EventType, adminEmails, awsConfig } from "../../utilities/config"
import { VendorUserModel } from "../../model/vendor.user.model"
import { ContactModel } from "../../model/contact.model"
import { UploadedFile } from "express-fileupload"
import {
  putObject,
  getS3MediaURL,
  getS3DynamicCdnURL,
} from "../../utilities/s3.utilities"
import { ObjectID, ObjectId } from "bson"

@injectable()
export class SupplierProductRepository {
  constructor(
    @inject(VendorTypes.SnsService) private snsService: SnsService,
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addProduct(
    user_id: string,
    productBody: IProduct,
    fileData?: {
      media?: [UploadedFile]
    }
  ) {
    console.log(user_id)
    productBody.user_id = user_id
    const subCatCheck = await AggregateSandSubCategoryModel.findOne({
      _id: productBody.sub_category_id,
      category_id: productBody.category_id,
    })

    const addrCheck = await AddressModel.findOne({
      "user.user_id": user_id,
      "user.user_type": "supplier",
      _id: productBody.pickup_address_id,
    })

    let errMsg: any[] = []
    if (subCatCheck === null) {
      errMsg.push(`Invalid category or sub-category`)
    }
    if (addrCheck === null) {
      errMsg.push(`Kindly enter a valid Address`)
    }

    const uniqueCheck = await ProductModel.findOne({
      sub_category_id: productBody.sub_category_id,
      pickup_address_id: productBody.pickup_address_id,
    })

    if (!isNullOrUndefined(uniqueCheck)) {
      errMsg.push(`The sub category was already added with selected pick up address`)
    }

    if (errMsg.length > 0) {
      return Promise.reject(new InvalidInput(errMsg.join(", "), 400))
    }

    if (!isNullOrUndefined(addrCheck)) {
      productBody.pickup_location = addrCheck.location
      productBody.source_id = addrCheck.source_id
      productBody.region_id = addrCheck.region_id
    }

    const supplierUserDoc = await VendorUserModel.findById(user_id)
    if (isNullOrUndefined(supplierUserDoc)) {
      return Promise.reject(new InvalidInput(`No Supplier data found`, 400))
    }

    //find product category doc.
    const productCategoryDoc = await AggregateSandCategoryModel.findById(
      productBody.category_id
    ).lean()
    if (isNullOrUndefined(productCategoryDoc)) {
      return Promise.reject(
        new InvalidInput(`Sorry, we could not find any doucuments for product in this category`, 400)
      )
    }
    //find product sub category doc.
    const productsubCategoryDoc = await AggregateSandSubCategoryModel.findById(
      productBody.sub_category_id
    ).lean()
    if (isNullOrUndefined(productsubCategoryDoc)) {
      return Promise.reject(
        new InvalidInput(`Sorry we could not find any doucuments for product in this subcategory`, 400)
      )
    }

    //find product contact person doc.
    const productcontactPersonDoc = await ContactModel.findById(
      productBody.contact_person_id
    ).lean()
    if (isNullOrUndefined(productcontactPersonDoc)) {
      console.log("Sorry we could not find any doucuments for product contact person.")
    }

    const contact_person_name = isNullOrUndefined(productcontactPersonDoc)
      ? supplierUserDoc.full_name
      : productcontactPersonDoc.person_name

    const contact_person_number = isNullOrUndefined(productcontactPersonDoc)
      ? supplierUserDoc.mobile_number
      : productcontactPersonDoc.mobile_number

    let mediaArr: {
      image_url: string
      thumbnail_url: string
      thumbnail_low_url: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/product`

    productBody._id = new ObjectID()
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.media) &&
      Array.isArray(fileData.media)
    ) {
      console.log("here")
      await Promise.all(
        fileData.media.map(async (media) => {
          let objectName = `${productBody._id}_${Math.random()}` 

          if (
            ["image/jpg", "image/jpeg", "image/png"].includes(media.mimetype) &&
            /\.jpe?g$/i.test(media.name)
          ) {
            objectName += ".jpg"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg and mp4 supported`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            media.data,
            media.mimetype
          )
          mediaArr.push({
            image_url: getS3MediaURL(`${objectDir}/${objectName}`),
            thumbnail_url: getS3DynamicCdnURL(
              `${objectDir}/w_400/${objectName}`
            ),
            thumbnail_low_url: getS3DynamicCdnURL(
              `${objectDir}/w_100,q_10/${objectName}`
            ),
          })
          return Promise.resolve()
        })
      )
      if (mediaArr.length > 0) {
        productBody.media = mediaArr
      }
    }

    const productDoc = await new ProductModel(productBody).save()

    // send sms to admin.
    await this.snsService.publishMessage({
      event_type: EventType.SEND_SMS_USING_TEMPLATE,
      event_data: {
        to: adminEmails[0].mobile_number,
        template_type: "add_product",
        templateData: {
          product_name: productDoc.name,
          quantity: productDoc.quantity,
          unit_price: productDoc.unit_price,
          product_category_name: productCategoryDoc.category_name,
          product_subCategory_name: productsubCategoryDoc.sub_category_name,
          product_contact_person_name: contact_person_name,
          product_conatct_person_mobile_number: contact_person_number,
        },
      },
    })

    // send mail to admin.
    await this.snsService.publishMessage({
      event_type: EventType.SEND_MAIL_USING_TEMPLATE,
      event_data: {
        template_name: "add_product",
        templateData: {
          subject: "add_product",
          html: {
            product_name: productDoc.name,
            quantity: productDoc.quantity,
            unit_price: productDoc.unit_price,
            product_category_name: productCategoryDoc.category_name,
            product_subCategory_name: productsubCategoryDoc.sub_category_name,
            product_contact_person_name: contact_person_name,
            product_conatct_person_mobile_number: contact_person_number,
          },
        },
        receiverInfo: {
          full_name: adminEmails[0].name,
          email: adminEmails[0].email,
        },
      },
    })

    await this.commonService.addActivityLogs(
      user_id,
      "supplier",
      AccessTypes.ADD_PRODUCT_BY_VENDOR,
      `Supplier added product of id ${productDoc._id} .`
    )

    return Promise.resolve(productDoc)
  }

  async addProductMedia(
    user_id: string,
    productId: string,
    fileData?: {
      media?: [UploadedFile]
    }
  ) {
    console.log("fileData", fileData)
    if (isNullOrUndefined(fileData)) {
      return Promise.reject(new InvalidInput(`No media found to update`, 400))
    }
    const productCheck = await ProductModel.findOne({
      _id: productId,
    })
    if (productCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }
    const updtObj: { [k: string]: any } = {}
    let mediaArr: {
      image_url: string
      thumbnail_url: string
      thumbnail_low_url: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/product`
    if (!isNullOrUndefined(fileData.media) && Array.isArray(fileData.media)) {
      await Promise.all(
        fileData.media.map(async (media) => {
          let objectName = `${productId}_${Math.random()}`

          if (
            ["image/jpg", "image/jpeg", "image/png"].includes(media.mimetype) &&
            /\.jpe?g$/i.test(media.name)
          ) {
            objectName += ".jpg"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg and mp4 supported`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            media.data,
            media.mimetype
          )
          mediaArr.push({
            image_url: getS3MediaURL(`${objectDir}/${objectName}`),
            thumbnail_url: getS3DynamicCdnURL(
              `${objectDir}/w_400/${objectName}`
            ),
            thumbnail_low_url: getS3DynamicCdnURL(
              `${objectDir}/w_100,q_10/${objectName}`
            ),
          })
          return Promise.resolve()
        })
      )
      if (mediaArr.length > 0) {
        updtObj.$push = {
          media: { $each: mediaArr },
        }
      }
    }

    if (Object.keys(updtObj).length === 0) {
      return Promise.reject(
        new InvalidInput(`Media not found on request payload`, 400)
      )
    }
    updtObj.$set = updtObj.$set || {}
    updtObj.updated_at = Date.now()
    updtObj.updated_by_id = user_id
    await ProductModel.updateOne({ _id: productId }, updtObj)
    return Promise.resolve()
  }

  async editProduct(
    productId: string,
    user_id: string,
    editBody: IEditProduct
  ): Promise<any> {
    const updt: { [k: string]: any } = {}
    const arrKyes = [
      "name",
      // "minimum_order",
      "unit_price",
      "self_logistics",
      "contact_person_id",
      "is_available",
      "pickup_address_id",
    ]

    for (const [key, value] of Object.entries(editBody)) {
      if (arrKyes.indexOf(key) > -1) {
        updt[key] = value
      }
    }
    if (!isNullOrUndefined(editBody.pickup_address_id)) {
      const addrCheck = await AddressModel.findOne({
        "user.user_id": user_id,
        "user.user_type": "supplier",
        _id: editBody.pickup_address_id,
      })

      if (!isNullOrUndefined(addrCheck)) {
        updt.pickup_location = addrCheck.location
        updt.source_id = addrCheck.source_id
        updt.region_id = addrCheck.region_id
      }
    }

    if (Object.keys(updt).length < 1) {
      return Promise.reject(new InvalidInput("Kindly enter a field for update", 400))
    }
    const res = await ProductModel.findOneAndUpdate(
      { _id: productId, user_id },
      { $set: updt }
    )
    if (res === null) {
      return Promise.reject(new InvalidInput("No matched record found.", 400))
    }

    await this.commonService.addActivityLogs(
      user_id,
      "supplier",
      AccessTypes.EDIT_PRODUCT_BY_VENDOR,
      `Supplier updated details product of id ${productId} .`,
      res,
      updt
    )

    return Promise.resolve()
  }
  async deleteProduct(productId: string, user_id: string) {
    const res = await ProductModel.updateOne(
      { _id: productId, user_id },
      { $set: { is_deleted: true, deleted_at: Date.now() } }
    )
    if (res.n === 0) {
      return Promise.reject(new InvalidInput("No matched record found.", 400))
    }

    await this.commonService.addActivityLogs(
      user_id,
      "supplier",
      AccessTypes.DELETE_PRODUCT_BY_VENDOR,
      `Supplier removed coupon of id ${productId} .`
    )

    return Promise.resolve()
  }

  async getSupplierProduct(
    user_id: string,
    search?: string,
    before?: string,
    after?: string,
    category?: [string],
    sub_category?: [string],
    deleted?: string,
    is_available?: boolean
  ): Promise<IProductDoc[]> {
    const query: { [k: string]: any } = {
      user_id: new ObjectId(user_id),
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(category)) {
      query.category_id = { $in: category }
    }
    if (!isNullOrUndefined(sub_category)) {
      query.sub_category_id = { $in: sub_category }
    }
    if (!isNullOrUndefined(is_available)) {
      query.is_available = is_available
    }
    // if (deleted === "yes") {
    //   query.is_deleted = true
    // } else if (deleted === "no") {
    //   query.is_deleted = { $in: [false, null] }
    // }

    console.log(query)
    return ProductModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier" },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address" },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state" },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city" },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }

  async increaseQuantity(
    productId: string,
    user_id: string,
    incVal: number
  ): Promise<any> {
    if (incVal <= -1) {
      const checkQnty = await ProductModel.findOne({ _id: productId, user_id })
      if (checkQnty === null) {
        return Promise.reject(new InvalidInput("No matched record found.", 400))
      }
      if (checkQnty.quantity < incVal) {
        return Promise.reject(
          new InvalidInput("The added quantity is insufficient and cannot be decreased.", 400)
        )
      }
    }
    const res = await ProductModel.updateOne(
      { _id: productId, user_id },
      { $inc: { quantity: incVal }, $set: { stock_updated_at: new Date() } }
    )
    if (res.n === 0) {
      return Promise.reject(new InvalidInput("No matched record found.", 400))
    }
    return Promise.resolve()
  }

  async deleteProductMedia(
    user_id: string,
    product_id: string,
    media_ids: [string]
  ) {
    const updtRes = await ProductModel.updateOne(
      { _id: product_id },
      {
        $pull: { media: { _id: { $in: media_ids } } },
        $set: {
          updated_at: Date.now(),
          updated_by_id: user_id,
        },
      }
    )
    if (updtRes.n === 0) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }
    return Promise.resolve()
  }
}
