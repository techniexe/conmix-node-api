import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"
import { minOrder } from "../../utilities/config"

export interface IProduct {
  user_id?: string
  name: string
  category_id: string
  sub_category_id: string
  quantity: number
  minimum_order: number
  unit_price: number
  pickup_address_id: string
  self_logistics: boolean
  pickup_location?: {
    coordinates: number[]
  }
  contact_person_id: string
  is_available: boolean
  files: {
    media: [UploadedFile]
  }
  [k: string]: any
}
export interface IAddProductRequest extends IAuthenticatedRequest {
  body: IProduct
}

export const addProductMediaSchema: IRequestSchema = {
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IAddProductMediaReq extends IAuthenticatedRequest {
  params: {
    productId: string
  }
  files: {
    media: [UploadedFile]
  }
}

export interface IEditProduct {
  user_id?: string
  name?: string
  // minimum_order?: number
  pickup_address_id: string
  unit_price?: number
  self_logistics?: boolean
  contact_person_id?: string
  is_available?: boolean
  verified_by_admin?: boolean
}
export interface IEditProductReq extends IAuthenticatedRequest {
  params: { productId: string }
  body: IEditProduct
}
export interface IDeleteProductReq extends IAuthenticatedRequest {
  params: { productId: string }
}

export interface IGetSupplierProduct extends IAuthenticatedRequest {
  query: {
    search?: string
    before?: string
    after?: string
    categories?: [string]
    sub_categories?: [string]
    deleted?: string
    is_available?: boolean
  }
}

export const addProductSchema: IRequestSchema = {
  body: Joi.object().keys({
    category_id: Joi.string().regex(objectIdRegex).required(),
    sub_category_id: Joi.string().regex(objectIdRegex).required(),
    name: Joi.string().min(3).max(100),
    unit_price: Joi.number().min(0).required(),
    quantity: Joi.number().min(0).required(),
    minimum_order: Joi.number().min(minOrder).required(),
    pickup_address_id: Joi.string().regex(objectIdRegex).required(),
    self_logistics: Joi.boolean().default(false),
    contact_person_id: Joi.string().regex(objectIdRegex).required(),
    is_available: Joi.boolean().required(),
  }),
}

export const editProductSchema: IRequestSchema = {
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    name: Joi.string().min(3).max(100),
    // minimum_order: Joi.number().min(20),
    pickup_address_id: Joi.string().regex(objectIdRegex),
    unit_price: Joi.number().min(0),
    self_logistics: Joi.boolean(),
    contact_person_id: Joi.string().regex(objectIdRegex),
    is_available: Joi.boolean(),
    verified_by_admin: Joi.boolean(),
  }),
}

export const deleteProductSchema: IRequestSchema = {
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}

export const getSupplierProduct: IRequestSchema = {
  query: Joi.object().keys({
    search: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    sub_categories: Joi.array().items(Joi.string().regex(objectIdRegex)),
    deleted: Joi.string().valid(["yes", "no", "both"]).default("no"),
    is_available: Joi.boolean(),
  }),
}

export interface IncreaseQuantityRequest extends IAuthenticatedRequest {
  params: { productId: string }
  body: {
    incVal: number
  }
}

export const increaseQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    incVal: Joi.number(),
  }),
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IDeleteProductMedia extends IAuthenticatedRequest {
  params: {
    productId: string
  }
  body: {
    mediaIds: [string]
  }
}

export const deleteProductMediaSchema: IRequestSchema = {
  params: Joi.object().keys({
    productId: Joi.string().regex(objectIdRegex).required(),
  }),
  body: Joi.object().keys({
    mediaIds: Joi.array().items(Joi.string().regex(objectIdRegex)).required(),
  }),
}
