import {
  httpGet,
  controller,
  httpPost,
  httpPatch,
  httpDelete,
} from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import {
  addProductSchema,
  IAddProductRequest,
  editProductSchema,
  IEditProductReq,
  deleteProductSchema,
  IDeleteProductReq,
  IGetSupplierProduct,
  getSupplierProduct,
  increaseQuantitySchema,
  IncreaseQuantityRequest,
  addProductMediaSchema,
  IAddProductMediaReq,
  IDeleteProductMedia,
  deleteProductMediaSchema,
} from "./vendor.product.req-schema"
import { NextFunction, Response } from "express-serve-static-core"
import { VendorTypes } from "../vendor.types"
import { SupplierProductRepository } from "./vendor.product.repository"
import { UnexpectedInput } from "../../utilities/customError"
@controller("/product", verifyCustomToken("supplier"))
@injectable()
export class SupplierProductController {
  constructor(
    @inject(VendorTypes.SupplierProductRepositroy)
    private productRepo: SupplierProductRepository
  ) {}
  @httpPost("/", validate(addProductSchema))
  async addProduct(req: IAddProductRequest, res: Response, next: NextFunction) {
    try {
      await this.productRepo.addProduct(req.user.uid, req.body, req.files)
      res.sendStatus(201)
    } catch (error) {
      let msg = error.message
      if (error.code === 11000) {
        msg = "This product is already exists on your account."
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  /**
   *  Add Product media.
   */
  @httpPatch("/media/:productId", validate(addProductMediaSchema))
  async addProductMedia(
    req: IAddProductMediaReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.productRepo.addProductMedia(
        req.user.uid,
        req.params.productId,
        req.files
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  @httpPatch("/:productId", validate(editProductSchema))
  async updateProduct(req: IEditProductReq, res: Response, next: NextFunction) {
    try {
      await this.productRepo.editProduct(
        req.params.productId,
        req.user.uid,
        req.body
      )
      res.sendStatus(202)
    } catch (error) {
      next(error)
    }
  }

  @httpDelete("/:productId", validate(deleteProductSchema))
  async deleteProduct(
    req: IDeleteProductReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.productRepo.deleteProduct(req.params.productId, req.user.uid)
      res.sendStatus(201)
    } catch (error) {
      next(error)
    }
  }
  @httpGet("/", validate(getSupplierProduct)) // get his own product
  async getlist(req: IGetSupplierProduct, res: Response, next: NextFunction) {
    const {
      search,
      before,
      after,
      categories,
      sub_categories,
      deleted,
      is_available,
    } = req.query
    const data = await this.productRepo.getSupplierProduct(
      req.user.uid,
      search,
      before,
      after,
      categories,
      sub_categories,
      deleted,
      is_available
    )
    res.json({ data })
  }

  @httpPatch("/quantity/:productId", validate(increaseQuantitySchema))
  async increaseQuantity(
    req: IncreaseQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.productRepo.increaseQuantity(
        req.params.productId,
        req.user.uid,
        req.body.incVal
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  /**
   * Delete Product media
   */
  @httpDelete("/media/:productId", validate(deleteProductMediaSchema))
  async deleteProductMedia(req: IDeleteProductMedia, res: Response) {
    await this.productRepo.deleteProductMedia(
      req.user.uid,
      req.params.productId,
      req.body.mediaIds
    )
    res.sendStatus(204)
  }
}
