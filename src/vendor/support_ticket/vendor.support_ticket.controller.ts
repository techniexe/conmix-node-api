import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  //httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { SupportTicketRepository } from "./vendor.support_ticket.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import {
  createSupportTicketByVendorSchema,
  ICreateSupportTicketByVendorRequest,
  // changeSupportTicketStatusBySupplierSchema,
  // IChangeSupportTicketBySupplierRequest,
  IReplySupportTicketByVendorRequest,
  replySupportTicketByVendorSchema,
  getSupportTicketByVendorSchema,
  getSupportTicketInfoByVendorSchema,
  IGetSupportTicketInfoByVendorRequest,
  getReplyOfSupportTicketByVendorSchema,
  IGetReplyOfTicketByVendorRequest,
} from "./vendor.support_ticket.schema"

@injectable()
@controller("/support_ticket", verifyCustomToken("vendor"))
export class SupportTicketController {
  constructor(
    @inject(VendorTypes.SupportTicketRepository)
    private supportTicketRepo: SupportTicketRepository
  ) {}

  /**
   * Create Support ticket.
   */
  @httpPost("/", validate(createSupportTicketByVendorSchema))
  async createSupportTicketByVendor(
    req: ICreateSupportTicketByVendorRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.supportTicketRepo.createSupportTicketByVendor(
        uid,
        req.body,
        req.files
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /**
   * Reply of support ticket.
   */
  @httpPost("/reply", validate(replySupportTicketByVendorSchema))
  async replySupportTicketByVendor(
    req: IReplySupportTicketByVendorRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      await this.supportTicketRepo.replySupportTicketByVendor(
        uid,
        req.body,
        req.files
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /**
   * Get listing of support ticket.
   */
  @httpGet("/", validate(getSupportTicketByVendorSchema))
  async getSupportTicketByVendor(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.supportTicketRepo.getSupportTicketByVendor(
        uid,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   * Get Info of support ticket.
   */
  @httpGet("/:ticketId", validate(getSupportTicketInfoByVendorSchema))
  async getSupportTicketInfo(
    req: IGetSupportTicketInfoByVendorRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const data = await this.supportTicketRepo.getSupportTicketInfoByVendor(
        uid,
        req.params.ticketId
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/reply/:ticketId", validate(getReplyOfSupportTicketByVendorSchema))
  async getReplyListOfTicket(
    req: IGetReplyOfTicketByVendorRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { ticketId } = req.params
      const data = await this.supportTicketRepo.getReplyListOfTicketByVendor(
        uid,
        ticketId,
        req.query
      )
      res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   *  Change support ticket status.
   */
  // @httpPatch("/:ticketId", validate(changeSupportTicketStatusBySupplierSchema))
  // async changeSupportTicketStatusBySupplier(
  //   req: IChangeSupportTicketBySupplierRequest,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   try {
  //     const { ticketId } = req.params
  //     const { uid } = req.user
  //     await this.supportTicketRepo.changeSupportTicketStatusBySupplier(
  //       uid,
  //       ticketId,
  //       req.body
  //     )
  //     res.sendStatus(204)
  //   } catch (err) {
  //     next(err)
  //   }
  // }
}
