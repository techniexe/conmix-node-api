import { QuestionTypes, Severity } from "../../utilities/config"
import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { ObjectId } from "bson"
import { UploadedFile } from "express-fileupload"

export interface ICreateSupportTicketByVendor {
  question_type: string
  severity: string
  subject: string
  description?: string
  support_ticket_status: string
  order_id?: string
  client_id: string | ObjectId
  client_type: string
  created_by_id: string | ObjectId
  created_by_type: string
  files: {
    attachments: [UploadedFile]
  }
  master_vendor_id: string
}

export const createSupportTicketByVendorSchema: IRequestSchema = {
  body: Joi.object().keys({
    question_type: Joi.string().valid(QuestionTypes).required(),
    severity: Joi.string().valid(Severity).required(),
    subject: Joi.string().required(),
    description: Joi.string().required(),
    order_id: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex).required(),
    client_type: Joi.string().valid("Vendor").required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface ICreateSupportTicketByVendorRequest
  extends IAuthenticatedRequest {
  body: ICreateSupportTicketByVendor
}

export interface IChangeSupportTicketStatus {
  support_ticket_status: string
}

export const changeSupportTicketStatusByVendorSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    support_ticket_status: Joi.string()
      .allow("OPEN", "INPROCESS", "CLOSED", "SOLVED")
      .required(),
  }),
}

export interface IChangeSupportTicketByVendorRequest
  extends IAuthenticatedRequest {
  body: IChangeSupportTicketStatus
}

export interface IReplySupportTicketByVendor {
  ticket_id: string
  reply_by_id: string | ObjectId
  reply_by_type: string
  comment: string
  files: {
    attachement: [UploadedFile]
  }
  support_ticket_status?: string
  master_vendor_id: string | ObjectId
}

export const replySupportTicketByVendorSchema: IRequestSchema = {
  body: Joi.object().keys({
    ticket_id: Joi.string().required(),
    comment: Joi.string().required(),
    support_ticket_status: Joi.string().allow("CLOSED", "SOLVED"),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IReplySupportTicketByVendorRequest
  extends IAuthenticatedRequest {
  body: IReplySupportTicketByVendor
}

export interface IGetSupportTicketByVendor {
  support_ticket_status?: string
  before?: string
  after?: string
  client_id?: string
  subject?: string
  severity?: string
  ticket_id?: string
}

export interface IGetSupportTicketByVendorRequest
  extends IAuthenticatedRequest {
  query: IGetSupportTicketByVendor
}

export const getSupportTicketByVendorSchema: IRequestSchema = {
  query: Joi.object().keys({
    support_ticket_status: Joi.string(),
    before: Joi.string(),
    after: Joi.string(),
    client_id: Joi.string().regex(objectIdRegex),
    subject: Joi.string(),
    severity: Joi.string().valid(Severity),
    ticket_id: Joi.string(),
  }),
}

export interface IGetSupportTicketInfoByVendor {
  ticketId: string
  [k: string]: any
}

export interface IGetSupportTicketInfoByVendorRequest
  extends IAuthenticatedRequest {
  params: IGetSupportTicketInfoByVendor
}

export const getSupportTicketInfoByVendorSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string(),
  }),
}

export interface IGetReplyOfSupportTicketInfoByVendor {
  before?: string
  after?: string
}

export interface IGetReplyOfTicketByVendorRequest
  extends IAuthenticatedRequest {
  query: IGetReplyOfSupportTicketInfoByVendor
}

export const getReplyOfSupportTicketByVendorSchema: IRequestSchema = {
  params: Joi.object().keys({
    ticketId: Joi.string().required(),
  }),
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
  }),
}
