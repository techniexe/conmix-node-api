import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { UniqueidService } from "../../utilities/uniqueid-service"
import {
  ICreateSupportTicketByVendor,
  //IChangeSupportTicketStatus,
  IReplySupportTicketByVendor,
  IGetSupportTicketByVendor,
  IGetReplyOfSupportTicketInfoByVendor,
} from "./vendor.support_ticket.schema"
import { UploadedFile } from "express-fileupload"
import {
  UserType,
  awsConfig,
  SupportTicketStatus,
} from "../../utilities/config"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { SupportTicketModel } from "../../model/support_ticket.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { SupportTicketReplyModel } from "../../model/support_ticket_reply_model"
import { ObjectId } from "bson"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import { AdminNotificationType } from "../../model/notification.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class SupportTicketRepository {
  private uniqueId: UniqueidService
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService,
    @inject(VendorTypes.NotificationUtility)
    private notUtil: NotificationUtility
    ) {
    this.uniqueId = new UniqueidService()
  }

  async createSupportTicketByVendor(
    user_id: string,
    supportTicketData: ICreateSupportTicketByVendor,
    fileData?: {
      attachments?: [UploadedFile]
    }
  ) {
    let doc: { [k: string]: any } = {}

    doc.created_by_id = user_id
    doc.created_by_type = UserType.VENDOR
    doc.client_id = supportTicketData.client_id
    doc.client_type = supportTicketData.client_type
    doc.question_type = supportTicketData.question_type
    doc.severity = supportTicketData.severity
    doc.subject = supportTicketData.subject
    doc.description = supportTicketData.description
    doc.master_vendor_id = supportTicketData.master_vendor_id
    doc.ticket_id = await this.uniqueId.getUniqueTicketid()
    if (!isNullOrUndefined(supportTicketData.order_id)) {
      doc.order_id = supportTicketData.order_id
    }

    let mediaArr: {
      url: string
      type: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/support_ticket/attachments`

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments) &&
      Array.isArray(fileData.attachments)
    ) {
      await Promise.all(
        fileData.attachments.map(async (attachments) => {
          let objectName = `${user_id}_${Math.random()}`
          let media_type = "image"
          if (
            attachments.mimetype === "video/mp4" &&
            /\.mp4$/i.test(attachments.name)
          ) {
            objectName += ".mp4"
            media_type = "video"
          } else if (
            ["image/jpg", "image/jpeg", "image/png"].includes(
              attachments.mimetype
            ) &&
            /\.jpe?g$/i.test(attachments.name)
          ) {
            objectName += ".jpg"
            media_type = "image"
          } else if (
            ["application/msword"].includes(attachments.mimetype) &&
            /\.doc$/i.test(attachments.name)
          ) {
            objectName += ".doc"
            media_type = "msword"
          } else if (
            [
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            ].includes(attachments.mimetype) &&
            /\.docx$/i.test(attachments.name)
          ) {
            objectName += ".docx"
            media_type = "msword"
          } else if (
            attachments.mimetype === "application/pdf" &&
            /\.pdf$/i.test(attachments.name)
          ) {
            objectName += ".pdf"
            media_type = "pdf"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            attachments.data,
            attachments.mimetype
          )
          mediaArr.push({
            url: getS3MediaURL(`${objectDir}/${objectName}`),
            type: media_type,
          })
          if (mediaArr.length > 0) {
            doc.attachments = mediaArr
          }
          console.log(mediaArr)
          return Promise.resolve()
        })
      )
    }
    const supportTicketDoc = new SupportTicketModel(doc)
    const data = await supportTicketDoc.save()

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_SUPPORT_TICKET_BY_VENDOR,
      `Vendor added support_tikcet of id ${data._id} .`
    )

    let vendorUserDoc = await VendorUserModel.findById(user_id)
    if (isNullOrUndefined(vendorUserDoc)) {
      return Promise.reject(new InvalidInput(`Vendor doc is not found.`, 400))
    }

   // Check Admin Type.
   const adminQuery: { [k: string]: any } = {}
   adminQuery.$or = [
     {
       admin_type : AdminRoles.superAdmin
     },
     {
       admin_type : AdminRoles.admin_manager
     },
     {
       admin_type : AdminRoles.admin_customer_care
     },
     {
       admin_type : AdminRoles.admin_grievances
     }
   ]
   const adminUserDocs = await AdminUserModel.find(adminQuery)
   if (adminUserDocs.length < 0) {
     return Promise.reject(
       new InvalidInput(`No Admin data were found `, 400)
     )
   }
   for(let i = 0 ; i< adminUserDocs.length; i ++){ 
    await this.notUtil.addNotificationForAdmin({
      to_user_type: UserType.ADMIN,
      to_user_id: adminUserDocs[i]._id,
      notification_type: AdminNotificationType.createSupportTicketByVendor,
      ticket_id: supportTicketDoc._id,
      vendor_id: user_id,
    })
   }


    return Promise.resolve(data)
  }

  async replySupportTicketByVendor(
    user_id: string,
    supportTicketData: IReplySupportTicketByVendor,
    fileData?: {
      attachments?: [UploadedFile]
    }
  ) {
    let doc: { [k: string]: any } = {}

 

    let query: { [k: string]: any } = {ticket_id: supportTicketData.ticket_id}
    query.$or = [
      {
        client_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    const ticketData = await SupportTicketModel.findOne(query)  
    if (isNullOrUndefined(ticketData)) {
      return Promise.reject(
        new InvalidInput(`You cannot reply to the ticket `, 400)
      )
    }

    doc.ticket_id = supportTicketData.ticket_id
    doc.reply_by_id = user_id
    doc.reply_by_type = UserType.VENDOR
    doc.comment = supportTicketData.comment
    doc.master_vendor_id = supportTicketData.master_vendor_id

    let mediaArr: {
      url: string
      type: string
    }[] = []
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/support_ticket/attachments`

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.attachments) &&
      Array.isArray(fileData.attachments)
    ) {
      await Promise.all(
        fileData.attachments.map(async (attachments) => {
          let objectName = `${user_id}_${Math.random()}`
          let media_type = "image"
          if (
            attachments.mimetype === "video/mp4" &&
            /\.mp4$/i.test(attachments.name)
          ) {
            objectName += ".mp4"
            media_type = "video"
          } else if (
            ["image/jpg", "image/jpeg", "image/png"].includes(
              attachments.mimetype
            ) &&
            /\.jpe?g$/i.test(attachments.name)
          ) {
            objectName += ".jpg"
            media_type = "image"
          } else if (
            ["application/msword"].includes(attachments.mimetype) &&
            /\.doc$/i.test(attachments.name)
          ) {
            objectName += ".doc"
            media_type = "msword"
          } else if (
            [
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            ].includes(attachments.mimetype) &&
            /\.docx$/i.test(attachments.name)
          ) {
            objectName += ".docx"
            media_type = "msword"
          } else if (
            attachments.mimetype === "application/pdf" &&
            /\.pdf$/i.test(attachments.name)
          ) {
            objectName += ".pdf"
            media_type = "pdf"
          } else {
            return Promise.reject(
              new InvalidInput(
                `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
                400
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            attachments.data,
            attachments.mimetype
          )
          mediaArr.push({
            url: getS3MediaURL(`${objectDir}/${objectName}`),
            type: media_type,
          })
          if (mediaArr.length > 0) {
            doc.attachments = mediaArr
          }
          console.log(mediaArr)
          return Promise.resolve()
        })
      )
    }
    const supportTicketDoc = new SupportTicketReplyModel(doc)
    const data = await supportTicketDoc.save()

    // Check Admin Type.
    const adminQuery: { [k: string]: any } = {}
    adminQuery.$or = [
      {
        admin_type : AdminRoles.admin_manager
      },
      {
        admin_type : AdminRoles.admin_customer_care
      },
      {
        admin_type : AdminRoles.admin_grievances
      }
    ]
    const adminUserDocs = await AdminUserModel.find(adminQuery)
    if (adminUserDocs.length < 0) {
      return Promise.reject(
        new InvalidInput(`No Admin data were found `, 400)
      )
    }

    for(let i = 0 ; i< adminUserDocs.length; i ++){ 
      await this.notUtil.addNotificationForAdmin({
        to_user_type: UserType.ADMIN,
        to_user_id: adminUserDocs[i]._id,
        notification_type: AdminNotificationType.replyOfSupportTicketByVendor,
        ticket_id: ticketData._id,
        vendor_id: user_id,
      })
    }


    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.REPLY_OF_SUPPORT_TICKET_BY_VENDOR,
      `Vendor replied of support_ticket of id ${data._id} .`
    )

    // Update ticket status. If existing status is CLOSED OR SOLVED then update it to OPEN else whatever user passes it updates.

    if (!isNullOrUndefined(supportTicketData.support_ticket_status)) {
      await SupportTicketModel.updateOne(
        { ticket_id: supportTicketData.ticket_id },
        {
          $set: {
            support_ticket_status: supportTicketData.support_ticket_status,
            updated_by_id: user_id,
            updated_by_type: UserType.VENDOR,
          },
        }
      )
    } else if (
      isNullOrUndefined(supportTicketData.support_ticket_status) &&
      // (ticketData.support_ticket_status === SupportTicketStatus.SOLVED ||
      ticketData.support_ticket_status === SupportTicketStatus.CLOSED
    ) {
      await SupportTicketModel.updateOne(
        { ticket_id: supportTicketData.ticket_id },
        {
          $set: {
            support_ticket_status: SupportTicketStatus.OPEN,
            updated_by_id: user_id,
            updated_by_type: UserType.VENDOR,
          },
        }
      )
    }
    return Promise.resolve(data)
  }

  async getSupportTicketByVendor(
    user_id: string,
    searchParam: IGetSupportTicketByVendor
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        client_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        reply_by_id: new ObjectId(user_id),
      },
    ]

   // let query: { [k: string]: any } = { client_id: new ObjectId(user_id) }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.support_ticket_status)) {
      query.support_ticket_status = searchParam.support_ticket_status
    }

    if (!isNullOrUndefined(searchParam.severity)) {
      query.severity = searchParam.severity
    }
    if (!isNullOrUndefined(searchParam.subject)) {
      query.subject = searchParam.subject
    }

    if(!isNullOrUndefined(searchParam.ticket_id)){
      query.ticket_id = searchParam.ticket_id
    }

    const aggregateArr = [
      { $match: query },

      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "vendor",
          localField: "client_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          question_type: true,
          severity: true,
          subject: true,
          description: true,
          attachments: true,
          support_ticket_status: true,
          order_id: true,
          created_at: true,
          created_by_id: true,
          created_by_type: true,
          client_id: true,
          client_type: true,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
        },
      },
    ]

    const ticketData = await SupportTicketModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      ticketData.reverse()
    }
    return Promise.resolve(ticketData)
  }

  async getSupportTicketInfoByVendor(user_id: string, ticket_id: string) {
    // Find ticket data.
console.log("user_id", user_id)
    let query: { [k: string]: any } = {ticket_id}
    query.$or = [
      {
        client_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        reply_by_id: new ObjectId(user_id),
      },
    ]

    // let query: { [k: string]: any } = {
    //   ticket_id,
    //   client_id: new ObjectId(user_id),
    // }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "vendor",
          localField: "client_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: { path: "$vendor", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          ticket_id: true,
          question_type: true,
          severity: true,
          subject: true,
          description: true,
          attachments: true,
          support_ticket_status: true,
          order_id: true,
          created_at: true,
          created_by_id: true,
          created_by_type: true,
          client_id: true,
          client_type: true,
          "vendor.full_name": 1,
          "vendor.company_name": 1,
        },
      },
    ]

    const ticketData = await SupportTicketModel.aggregate(aggregateArr)

    // const ticketData = await SupportTicketModel.find({
    //   ticket_id,
    //  client_id: new ObjectId(user_id),
    // }).select({
    //   ticket_id: 1,
    //   question_type: 1,
    //   severity: 1,
    //   subject: 1,
    //   description: 1,
    //   attachments: 1,
    //   support_ticket_status: 1,
    //   order_id: 1,
    //   created_at: 1,
    // })

    if (ticketData.length < 0) {
      return Promise.reject(new InvalidInput(`Support ticket data was not found `, 400))
    }
    // Find ticket reply data.
    const ticketReplyData = await SupportTicketReplyModel.find({
      ticket_id,
    }).sort({created_at: -1})
    return Promise.resolve({
      ticketData,
      ticketReplyData,
    })
  }

  async getReplyListOfTicketByVendor(
    user_id: string,
    ticket_id: string,
    searchParam: IGetReplyOfSupportTicketInfoByVendor
  ) {

    let query: { [k: string]: any } = {ticket_id}
    query.$or = [
      {
        client_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        reply_by_id: new ObjectId(user_id),
      },
    ]

    // let query: { [k: string]: any } = {
    //   // reply_by_id: new ObjectId(user_id),
    //   ticket_id,
    // }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before) && searchParam.before !== "") {
      query.created_at = { $lt: searchParam.before }
    }
    if (!isNullOrUndefined(searchParam.after) && searchParam.after !== "") {
      query.created_at = { $gt: searchParam.after }
      sort.created_at = 1
    }

    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $project: {
          ticket_id: true,
          comment: true,
          attachments: true,
          created_at: true,
          reply_by_id: true,
          reply_by_type: true,
        },
      },
    ]

    const ticketData = await SupportTicketReplyModel.aggregate(aggregateArr)

    if (isNullOrUndefined(ticketData)) {
      return Promise.reject(new InvalidInput(`No Ticket Doc found.`, 400))
    }
    if (sort.created_at === 1) {
      ticketData.reverse()
    }
    return Promise.resolve(ticketData)
  }

  // async changeSupportTicketStatusBySupplier(
  //   user_id: string,
  //   ticketId: string,
  //   supportTicketStatusData: IChangeSupportTicketStatus
  // ) {
  //   const ticketData = await SupportTicketModel.findOne({
  //     ticket_id: ticketId,
  //   })

  //   if (isNullOrUndefined(ticketData)) {
  //     return Promise.reject(new InvalidInput(`No support ticket record found.`, 400))
  //   }

  //   let ticket_status = ticketData.support_ticket_status
  //   let status = ""

  //   // If status is OPEN then it update to CLOSED, INPROCESS and SOLVED.
  //   if (
  //     ticket_status === SupportTicketStatus.OPEN &&
  //     (supportTicketStatusData.support_ticket_status ===
  //       SupportTicketStatus.INPROCESS ||
  //       supportTicketStatusData.support_ticket_status ===
  //         SupportTicketStatus.CLOSED ||
  //       supportTicketStatusData.support_ticket_status ===
  //         SupportTicketStatus.SOLVED)
  //   ) {
  //     status = supportTicketStatusData.support_ticket_status

  //     // If status is INPROCESS then it update to CLOSED and SOLVED.
  //   } else if (
  //     ticket_status === SupportTicketStatus.INPROCESS &&
  //     (supportTicketStatusData.support_ticket_status ===
  //       SupportTicketStatus.CLOSED ||
  //       supportTicketStatusData.support_ticket_status ===
  //         SupportTicketStatus.SOLVED)
  //   ) {
  //     status = supportTicketStatusData.support_ticket_status

  //     // If status is CLOSED then it update to OPEN.
  //   } else if (
  //     ticket_status === SupportTicketStatus.CLOSED &&
  //     supportTicketStatusData.support_ticket_status === SupportTicketStatus.OPEN
  //   ) {
  //     status = supportTicketStatusData.support_ticket_status

  //     // If status is SOLVED then it update to OPEN.
  //   } else if (
  //     ticket_status === SupportTicketStatus.SOLVED &&
  //     supportTicketStatusData.support_ticket_status === SupportTicketStatus.OPEN
  //   ) {
  //     status = supportTicketStatusData.support_ticket_status
  //   } else {
  //     return Promise.reject(
  //       new InvalidInput(
  //         ` Support ticket status couldn't be updated as support ticket  is ${supportTicketStatusData.support_ticket_status}`,
  //         400
  //       )
  //     )
  //   }
  //   await SupportTicketModel.updateOne(
  //     { ticket_id: ticketId },
  //     {
  //       $set: {
  //         support_ticket_status: status,
  //         updated_by_id: user_id,
  //         updated_by_type: UserType.VENDOR,
  //       },
  //     }
  //   )

  //   await this.commonService.addActivityLogs(
  //     user_id,
  //     "vendor",
  //     AccessTypes.UPDATE_SUPPORT_TICKET_STATUS_BY_VENDOR,
  //     `Supplier Updated details of support ticket id ${ticketId}.`,
  //     ticketData.support_ticket_status,
  //     status
  //   )

  //   return Promise.resolve()
  // }
}
