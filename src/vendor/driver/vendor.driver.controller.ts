import { IAuthenticatedRequest } from "./../../middleware/auth-token.middleware"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { inject, injectable } from "inversify"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { UnexpectedInput } from "../../utilities/customError"
import { DriverRepository } from "./vendor.driver.repository"
import { VendorTypes } from "../vendor.types"
import {
  addDriverDetailsSchema,
  IAddDriverDetailsRequest,
  editDriverRequestSchema,
  IEditDriverRequest,
  getDriverDetailsRequest,
  IGetDriverDetailsRequest,
} from "./vendor.driver.req-schema"

@injectable()
@controller("/driver", verifyCustomToken("vendor"))
export class DriverController {
  constructor(
    @inject(VendorTypes.DriverRepository)
    private driverRepo: DriverRepository
  ) {}

  @httpPost("/", validate(addDriverDetailsSchema))
  async addDriverDetails(
    req: IAddDriverDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      await this.driverRepo.addDriverDetails(uid, req.body, req.files)
      return res.sendStatus(200)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added details of this driver. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpPatch("/:driver_id", validate(editDriverRequestSchema))
  async editDriver(req: IEditDriverRequest, res: Response, next: NextFunction) {
    try{
      await this.driverRepo.editDriver(
        req.user.uid,
        req.params.driver_id,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added details of this driver. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  
  }

  @httpGet("/")
  async getDriverList(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.driverRepo.getDriverList(req.user.uid,req.query)
    res.json({ data })
  }

  @httpGet("/:driver_id", validate(getDriverDetailsRequest))
  async getDriverDetails(
    req: IGetDriverDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.driverRepo.getDriverDetails(
      req.user.uid,
      req.params.driver_id
    )
    res.json({ data })
  }
}
