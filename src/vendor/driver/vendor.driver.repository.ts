import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { UploadedFile } from "express-fileupload"
import { DriverInfoModel } from "../../model/driver.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorTypes } from "../vendor.types"
import { IDriver, IEditDriver } from "./vendor.driver.req-schema"

@injectable()
export class DriverRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addDriverDetails(
    user_id: string,
    driverData: IDriver,
    fileData: { driver_pic: UploadedFile }
  ) {
    // if(!isNullOrUndefined(driverData.master_vendor_id)){
    //   driverData.user = {user_id}
    // }

    if (!isNullOrUndefined(driverData.driver_mobile_number)) {
      const numberCheck = await DriverInfoModel.findOne({
        driver_mobile_number: driverData.driver_mobile_number,
      })

      if (!isNullOrUndefined(numberCheck)) {
        return Promise.reject(
          new InvalidInput(
            `This mobile no. is already associated with other driver.`,
            400
          )
        )
      }
    }

    driverData.user = { user_id }
    driverData.user.user_type = "vendor"
    const newDriver = new DriverInfoModel(driverData)
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_pic) &&
      !isNullOrUndefined(fileData.driver_pic.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/driver`
      let objectName = "" + "dr" + newDriver._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_pic.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_pic.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_pic.data,
        fileData.driver_pic.mimetype
      )
      newDriver.driver_pic = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const newdriverDoc = await newDriver.save()

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_DRIVERDETAILS_BY_VENDOR,
      `Vendor added driver details of id ${newdriverDoc._id} .`
    )

    return Promise.resolve(newdriverDoc)
  }

  async editDriver(
    user_id: string,
    driver_id: string,
    editData: IEditDriver,
    fileData: {
      driver_pic: UploadedFile
    }
  ): Promise<any> {
    const editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(editData.driver_mobile_number)) {
      const numberCheck = await DriverInfoModel.findOne({
        driver_mobile_number: editData.driver_mobile_number,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(driver_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This mobile no. is already associated with other driver.`,
            400
          )
        )
      }
    }


    const edtFld = [
      "driver_name",
      "driver_mobile_number",
      "driver_alt_mobile_number",
      "driver_whatsapp_number",
      "sub_vendor_id",
      "master_vendor_id",
    ]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_pic) &&
      !isNullOrUndefined(fileData.driver_pic.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/driver`
      let objectName = "" + driver_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_pic.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_pic.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_pic.data,
        fileData.driver_pic.mimetype
      )
      editDoc.driver_pic = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const edtRes = await DriverInfoModel.findOneAndUpdate(
      {
        _id: driver_id,
        // "user.user_id": user_id,
      },
      { $set: editDoc }
    )
    if (edtRes === null) {
      return Promise.reject(
        new InvalidInput(`Invalid request as no vehicle details found`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_DRIVERDETAILS_BY_VENDOR,
      `Vendor updated details driver of id ${driver_id} .`,
      edtRes,
      editDoc
    )

    return Promise.resolve()
  }

  async getDriverList(user_id: string, searchParam: any) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    if (!isNullOrUndefined(searchParam.sub_vendor_id)) {
      query.sub_vendor_id = new ObjectId(searchParam.sub_vendor_id) 
    }

    return DriverInfoModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorDetails._id",
          foreignField: "sub_vendor_id",
          as: "addressDetails",
        },
      },
      {
        $unwind: { path: "$addressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "addressDetails.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "addressDetails.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          driver_name: 1,
          driver_mobile_number: 1,
          driver_alt_mobile_number: 1,
          driver_whatsapp_number: 1,
          driver_pic: 1,
          created_at: 1,
          sub_vendor_id: 1,
          master_vendor_id: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
         // addressDetails: 1,
            "addressDetails.user": 1,
            "addressDetails.state_id": 1,
            "addressDetails.city_id": 1,
            "addressDetails.address_type": 1,
            "addressDetails.business_name": 1,
            "addressDetails.region_id": 1,
            "addressDetails.source_id": 1,
            "addressDetails.created_at": 1,
            "addressDetails.updated_at": 1,
            "addressDetails.sub_vendor_id": 1,
            "addressDetails.master_vendor_id": 1,
            "addressDetails.billing_address_id": 1,
            "addressDetails.is_verified": 1,
            "addressDetails.is_accepted": 1,
            "addressDetails.is_blocked": 1,
            "addressDetails.plant_address_date": 1,
            "addressDetails.line1": 1,
            "addressDetails.line2": 1,
            "addressDetails.state_name": "$state.state_name",
            "addressDetails.city_name": "$city.city_name",
            "addressDetails.source_name": "$source.source_name",
            "addressDetails.pincode": 1,
            "addressDetails.location": 1,
        },
      },
    ])
    // return DriverInfoModel.find({ query })
  }

  async getDriverDetails(user_id: string, driver_id: string) {
    return DriverInfoModel.findOne({
      "user.user_id": user_id,
      _id: new ObjectId(driver_id),
    })
  }
}
