import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface IDriver {
  user: {
    user_id?: string
    user_type?: string
  }
  driver_name: string
  driver_mobile_number: string
  driver_alt_mobile_number: string
  driver_whatsapp_number: string
  driver_pic: string
  sub_vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddDriverDetailsRequest extends IAuthenticatedRequest {
  body: IDriver
  files: {
    driver_pic: UploadedFile
  }
}

export const addDriverDetailsSchema: IRequestSchema = {
  body: Joi.object().keys({
    driver_name: Joi.string().required(),
    driver_mobile_number: Joi.string().required(),
    driver_alt_mobile_number: Joi.string(),
    driver_whatsapp_number: Joi.string(),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export const editDriverRequestSchema: IRequestSchema = {
  body: Joi.object().keys({
    driver_name: Joi.string(),
    driver_mobile_number: Joi.string(),
    driver_alt_mobile_number: Joi.string(),
    driver_whatsapp_number: Joi.string(),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    driver_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IEditDriver {
  driver_name: string
  driver_mobile_number: string
  driver_alt_mobile_number: string
  driver_whatsapp_number: string
  driver_pic: string
  sub_vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditDriverRequest extends IAuthenticatedRequest {
  params: {
    driver_id: string
  }
  files: {
    driver_pic: UploadedFile
  }
  body: IEditDriver
}

export const getDriverDetailsRequest: IRequestSchema = {
  params: Joi.object().keys({
    driver_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetDriverDetailsRequest extends IAuthenticatedRequest {
  params: {
    driver_id: string
  }
}
