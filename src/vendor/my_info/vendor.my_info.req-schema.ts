import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface IRequestOTPRequest extends IAuthenticatedRequest {
  params: {
    mobile_number: string
  }
}

export const requestOTPSchema: IRequestSchema = {
  params: Joi.object().keys({
    mobile_number: Joi.string().required(),
  }),
}

export interface IVerifyMobilenumberRequest extends IAuthenticatedRequest {
  params: {
    mobile_number: string
    code: string
  }
}

export const verifyMobilenumberSchema: IRequestSchema = {
  params: Joi.object().keys({
    mobile_number: Joi.string().required(),
    code: Joi.string().required(),
  }),
}

export interface IAddMyInfo {
  first_name: string
  last_name: string
  number: number
  [k: string]: any
}

export interface IAddMyInfoRequest extends IAuthenticatedRequest {
  body: IAddMyInfo
}

export const addMyInfoSchema: IRequestSchema = {
  body: Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    number: Joi.number().required(),
  }),
}

export interface IGetMyInfo {
  [k: string]: any
}

export interface IGetMyInfoRequest extends IAuthenticatedRequest {
  body: IGetMyInfo
}

export const getMyInfoSchema: IRequestSchema = {}

export interface IAddTMtoOrderTrack {
  TM_id: string
  pickup_quantity: number
  assigned_at: Date
  start_time: string
  end_time: string
  order_item_part_id: string
  [k: string]: any
}

export interface IAddTMtoOrderTrackRequest extends IAuthenticatedRequest {
  body: IAddTMtoOrderTrack
  params: {
    order_id: string
    order_item_id: string
  }
}

export const addTMtoOrderTrackSchema: IRequestSchema = {
  body: Joi.object().keys({
    TM_id: Joi.string().required().regex(objectIdRegex),
    pickup_quantity: Joi.number().required(),
    assigned_at: Joi.date().iso().required(),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
    order_item_part_id: Joi.string().required().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    order_id: Joi.string().required(),
    order_item_id: Joi.string().required().regex(objectIdRegex),
  }),
}

export interface ICheckCPStatusRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
  }
}

export const checkCPStatusSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().required(),
  }),
}

export interface IAddCPtoOrderTrack {
  CP_id: string
  assigned_at: Date
  [k: string]: any
}

export interface IAddCPtoOrderTrackRequest extends IAuthenticatedRequest {
  body: IAddCPtoOrderTrack
  params: {
    order_id: string
    order_item_id: string
  }
}

export const addCPtoOrderTrackSchema: IRequestSchema = {
  body: Joi.object().keys({
    CP_id: Joi.string().required().regex(objectIdRegex),
    assigned_at: Joi.date().iso().required(),
  }),
  params: Joi.object().keys({
    order_id: Joi.string().required(),
    order_item_id: Joi.string().required().regex(objectIdRegex),
  }),
}

export interface IGetOrderByTMIdRequest extends IAuthenticatedRequest {
  params: {
    TMId: string
  }
  query: {
    current_date: Date
  }
}

export interface IGetOrderByCPIdRequest extends IAuthenticatedRequest {
  params: {
    CPId: string
  }
  query: {
    current_date: Date
  }
}

export interface IUpdateOrder {
  //pickup_quantity: string
  royality_quantity: string
  order_status: string
  reject_reason: string
}

export interface IUpdateOrderRequest extends IAuthenticatedRequest {
  params: {
    track_id: string
    order_id: string
    TMId: string
  }
  body: IUpdateOrder
}

export const updateOrderSchema: IRequestSchema = {
  body: Joi.object().keys({
    // pickup_quantity: Joi.number(),
    royality_quantity: Joi.number(),
    order_status: Joi.string().valid("ACCEPTED", "REJECTED"),
    reject_reason: Joi.string(),
  }),
}

export interface IUpdateCPOrderTrack {
  order_status: string
  reject_reason: string
}

export interface IUpdateCPOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    track_id: string
    order_id: string
    CPId: string
  }
  body: IUpdateOrder
}

export const updateCPOrderTrackSchema: IRequestSchema = {
  body: Joi.object().keys({
    order_status: Joi.string().valid("ACCEPTED", "REJECTED"),
    reject_reason: Joi.string(),
  }),
}

export interface IAddOrderAsPickupRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
    TMId: string
    track_id: string
  }
}

export const addOrderAsPickupSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().required(),
    TMId: Joi.string().required().regex(objectIdRegex),
    track_id: Joi.string().required().regex(objectIdRegex),
  }),
}

export interface IAddCPOrderAsPickupRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
    CPId: string
    track_id: string
  }
}

export const addCPOrderAsPickupSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().required(),
    CPId: Joi.string().required().regex(objectIdRegex),
    track_id: Joi.string().required().regex(objectIdRegex),
  }),
}

export interface IUploadDocumentRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
    TMId: string
    track_id: string
  }
  files: {
    royalty_pass_image: UploadedFile
    way_slip_image: UploadedFile
    challan_image: UploadedFile
    invoice_image: UploadedFile
    supplier_pickup_signature: UploadedFile
    driver_pickup_signature: UploadedFile
    buyer_drop_signature: UploadedFile
    driver_drop_signature: UploadedFile
  }
}

export interface IUploadCPDocumentRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
    CPId: string
    track_id: string
  }
  files: {
    challan_image: UploadedFile
    supplier_pickup_signature: UploadedFile
    driver_pickup_signature: UploadedFile
    buyer_drop_signature: UploadedFile
    driver_drop_signature: UploadedFile
  }
}

export interface IGetOrderDetailsRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
    track_id: string
  }
}

export const orderDelayedSchema: IRequestSchema = {
  body: Joi.object().keys({
    reasonForDelay: Joi.string().required(),
    delayTime: Joi.string().required(),
  }),
  params: Joi.object().keys({
    track_id: Joi.string().regex(objectIdRegex).required(),
    orderId: Joi.string().required(),
    TMId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IOrderDelayedRequest extends IAuthenticatedRequest {
  body: {
    reasonForDelay: string
    delayTime: string
  }
  params: {
    orderId: string
    TMId: string
    track_id: string
  }
}

export const CPorderDelayedSchema: IRequestSchema = {
  body: Joi.object().keys({
    reasonForDelay: Joi.string().required(),
    delayTime: Joi.string().required(),
  }),
  params: Joi.object().keys({
    track_id: Joi.string().regex(objectIdRegex).required(),
    orderId: Joi.string().required(),
    CPId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface ICPOrderDelayedRequest extends IAuthenticatedRequest {
  body: {
    reasonForDelay: string
    delayTime: string
  }
  params: {
    orderId: string
    CPId: string
    track_id: string
  }
}

export const orderDeliveredSchema: IRequestSchema = {
  body: Joi.object().keys({
    // buyer_signature: Joi.string(),
    // buyer_id_card: Joi.string(),
    // buyer_user_photo: Joi.string(),
    // order_item_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    TMId: Joi.string().regex(objectIdRegex).required(),
    orderId: Joi.string().required(),
    track_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IOrderDeliveredRequest extends IAuthenticatedRequest {
  body: {
    // buyer_signature: string
    // buyer_id_card: string
    // buyer_user_photo: string
    //order_item_id: string
  }
  params: {
    orderId: string
    TMId: string
    track_id: string
  }
}

export const CPorderDeliveredSchema: IRequestSchema = {
  params: Joi.object().keys({
    CPId: Joi.string().regex(objectIdRegex).required(),
    orderId: Joi.string().required(),
    track_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface ICPOrderDeliveredRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
    CPId: string
    track_id: string
  }
}

export interface IGetVendorOrderTrackRequest extends IAuthenticatedRequest {
  params: {
    order_item_id: string
    tracking_id: string
  }
}

export const getVendorOrderTrackSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_item_id: Joi.string().regex(objectIdRegex),
    tracking_id: Joi.string(),
  }),
}
