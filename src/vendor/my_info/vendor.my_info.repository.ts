import { injectable } from "inversify"
//import { generateRandomNumber } from "../../utilities/generate-random-number"

import { IAddMyInfo, IGetMyInfo } from "./vendor.my_info.req-schema"
import { ObjectId } from "bson"
import { MyInfoModel } from "../../model/my_info.model"

@injectable()
export class VendorMyInfoRepository {
  // async addTMtoOrderTrack(
  //   vendor_id: string,
  //   order_id: string,
  //   order_item_id: string,
  //   trackData: IAddTMtoOrderTrack
  // ) {
  //   var myobj = { name: "Ajeet Kumar", age: "28", address: "Delhi" }

  //   const data = await MyInfoModel.insertOne(myobj)
  //   console.log(query)
  //   console.log("dataaaaaaaaaa", data)

  //   return Promise.resolve()
  // }

  async addMyInfo(uid: string, reqBody: IAddMyInfo) {
    reqBody.user_id = uid
    reqBody._id = new ObjectId()

    console.log("Here")
    return new MyInfoModel(reqBody).save()
  }

  async getMyInfo(uid: string, searchParam: IGetMyInfo) {
    const query: { [k: string]: any } = {}

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $project: {
          _id: true,
          first_name: true,
          last_name: true,
          number: true,
        },
      },
    ]

    const data = await MyInfoModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
