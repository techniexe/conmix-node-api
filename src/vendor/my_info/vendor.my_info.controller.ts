import { injectable, inject } from "inversify"
import { controller, httpPost, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorMyInfoRepository } from "./vendor.my_info.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  IAddMyInfoRequest,
  addMyInfoSchema,
  IGetMyInfoRequest,
  getMyInfoSchema,
} from "./vendor.my_info.req-schema"
import { NextFunction, Response } from "express"

@injectable()
@controller("/my_info", verifyCustomToken("vendor"))
export class VendorMyInfoController {
  constructor(
    @inject(VendorTypes.VendorMyInfoRepository)
    private myInfoRepo: VendorMyInfoRepository
  ) {}

  //add TM to order track.
  @httpPost("/add_my_info", validate(addMyInfoSchema))
  async addMyInfo(req: IAddMyInfoRequest, res: Response, next: NextFunction) {
    try {
      await this.myInfoRepo.addMyInfo(req.user.uid, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/", validate(getMyInfoSchema))
  async getConcretePump(
    req: IGetMyInfoRequest,
    res: Response,
    next: NextFunction
  ) {
    console.log(req.user.uid)
    const data = await this.myInfoRepo.getMyInfo(req.user.uid, req.query)
    res.json({ data })
  }
}
