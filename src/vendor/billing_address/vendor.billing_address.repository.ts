import { inject, injectable } from "inversify"
import { ObjectId } from "bson"
import {
  IBillingAddress,
  IEditBillingAddress,
} from "./vendor.billing_address.req-schema"
import { VendorUserModel } from "../../model/vendor.user.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { BillingAddressModel } from "../../model/billing_address.model"
import { AccessTypes } from "../../model/access_logs.model"
import { UploadedFile } from "express-fileupload"
import { getS3MediaURL, putObject } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { CartModel } from "../../model/cart.model"
@injectable()
export class BillingAddressRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addBillingAddress(
    user_id: string,
    addressData: IBillingAddress,
    fileData: { gst_image: UploadedFile }
  ) {
    let vendorUserDoc = await VendorUserModel.findOne({
      _id: new ObjectId(user_id),
    })
    if (isNullOrUndefined(vendorUserDoc)) {
      return Promise.reject(new InvalidInput(`No Vendor user Doc Found.`, 400))
    }

    if(!isNullOrUndefined(addressData.company_name)){
      let data = await BillingAddressModel.findOne({
        company_name : addressData.company_name
      })
  
      if(!isNullOrUndefined(data)){
        return Promise.reject(new Error("Billing Address already registered with same company name."))
      }
    }

    if(!isNullOrUndefined(addressData.gst_number)){
      let data = await BillingAddressModel.findOne({
        gst_number : addressData.gst_number
      })
  
      if(!isNullOrUndefined(data)){
        return Promise.reject(new Error("Billing Address already registered with same gst number."))
      }
    }
  
    const newDoc: any = {
      _id: new ObjectId(),
      company_name: addressData.company_name,
      line1: addressData.line1,
      line2: addressData.line2,
      state_id: addressData.state_id,
      city_id: addressData.city_id,
      pincode: addressData.pincode,
      gst_number: addressData.gst_number,
      user: {
        user_type: "vendor",
        user_id,
      },
    }
console.log("fileData", fileData)
    if (
      !isNullOrUndefined(fileData.gst_image) &&
      !isNullOrUndefined(fileData.gst_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/gst_image`
      let objectName = "" + newDoc._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.gst_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.gst_image.name)
      ) {
        objectName += ".jpg"
      }
      else if (
        ["application/pdf", "image/pdf"].includes(
          fileData.gst_image.mimetype
        ) &&
        /\.pdf$/i.test(fileData.gst_image.name)
      ) {
        objectName += ".pdf"
      }
      else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg and pdf supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.gst_image.data,
        fileData.gst_image.mimetype
      )
      newDoc.gst_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
      // admixBrandData.thumbnail_url = getS3DynamicCdnURL(
      //   `${objectDir}/w_400/${objectName}`
      // )
    }
    const newBillingAddress = new BillingAddressModel(newDoc)
    const newBillingAddressDoc = await newBillingAddress.save()
    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_BILLING_ADDRESS_BY_VENDOR,
      `Vendor added billing address of id ${newBillingAddressDoc._id} .`
    )
    return Promise.resolve(newBillingAddressDoc)
  }

  async getAddress(
    user_id: string,
    search?: string,
    before?: string,
    after?: string,
    city_id?: any
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
        is_deleted: false,
      },
      {
        sub_vendor_id: new ObjectId(user_id),
        is_deleted: false,
      },
    ]
    // let query: { [k: string]: any } = {
    //   "user.user_id": new ObjectId(user_id),
    //   "user.user_type": "vendor",
    // }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(city_id) && city_id !== "") {
      query.city_id = new ObjectId(city_id)
    }

    console.log(query)
    return BillingAddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      // {
      //   $limit: 10,
      // },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "_id",
          foreignField: "billing_address_id",
          as: "AddressDetails",
        },
      },
      // {
      //   $unwind: { path: "$AddressDetails", preserveNullAndEmptyArrays: true },
      // },
      {
        $project: {
          _id: 1,
          company_name: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          gst_number: 1,
          gst_image_url: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          created_at: 1,
          updated_at: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          AddressDetails: 1,
        },
      },
    ])
    // return AddressModel.find(query).limit(40)
  }

  async editBillingAddress(
    user_id: string,
    addressId: string,
    addressData: IEditBillingAddress,
    fileData: { gst_image: UploadedFile }
  ): Promise<any> {

    if (!isNullOrUndefined(addressData.company_name)) {
      const numberCheck = await BillingAddressModel.findOne({
        company_name: addressData.company_name,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(addressId) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This company name is already associated with other billing address.`,
            400
          )
        )
      }
    }


    if (!isNullOrUndefined(addressData.gst_number)) {
      const numberCheck = await BillingAddressModel.findOne({
        gst_number: addressData.gst_number,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(addressId) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This gst number is already associated with other billing address.`,
            400
          )
        )
      }
    }

    const fields = [
      "company_name",
      "line1",
      "line2",
      "state_id",
      "city_id",
      "pincode",
      "gst_number",
    ]
    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(addressData)) {
      if (fields.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    console.log("addressDataaaaaaaaaaaaaaaaaaaaaaaaa", addressData)
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.gst_image) &&
      !isNullOrUndefined(fileData.gst_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/gst_image`
      let objectName = `${addressId}` + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.gst_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.gst_image.name)
      ) {
        objectName += ".jpg"
      }    else if (
        ["application/pdf", "image/pdf"].includes(
          fileData.gst_image.mimetype
        ) &&
        /\.pdf$/i.test(fileData.gst_image.name)
      ) {
        objectName += ".pdf"
      }
      else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg and pdf supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.gst_image.data,
        fileData.gst_image.mimetype
      )
      editDoc.gst_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const res = await BillingAddressModel.findOneAndUpdate(
      {
        _id: addressId,
        "user.user_id": user_id,
        "user.user_type": "vendor",
      },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This address doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.UPDATE_BILLING_ADDRESS_BY_VENDOR,
      `Vendor updated details Of billing Address of id ${addressId} .`,
      res,
      editDoc
    )
    return Promise.resolve()
  }

  async deleteBillingAddress(user_id: string, addressId: string) {
    const updtRes = await BillingAddressModel.updateOne(
      {
        _id: addressId,
        "user.user_id": user_id,
      },
      {
        $set: {
          is_deleted: true,
          deleted_at: Date.now(),
        },
      }
    )
    if (updtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Delete failed as no record exists`, 400)
      )
    }

    await CartModel.updateOne(
      {
        user_id: new ObjectId(user_id),
        billing_address_id: new ObjectId(addressId),
      },
      { $unset: { billing_address_id: "" } }
    )
    return Promise.resolve()
  }
}
