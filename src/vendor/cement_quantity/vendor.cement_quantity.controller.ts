import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { CementQuantityRepository } from "./vendor.cement_quantity.repository"
import {
  addCementQuantitySchema,
  IAddCementQuantityRequest,
  editCementQuantitySchema,
  IEditCementQuantityRequest,
  removeCementQuantitySchema,
  IRemoveCementQuantityRequest,
  getCementQuantitySchema,
  IGetCementQuantityRequest,
} from "./vendor.cement_quantity.req-schema"

@injectable()
@controller("/cement_quantity", verifyCustomToken("vendor"))
export class CementQuantityController {
  constructor(
    @inject(VendorTypes.CementQuantityRepository)
    private cementRateRepo: CementQuantityRepository
  ) {}

  @httpPost("/", validate(addCementQuantitySchema))
  async addCementQuantity(
    req: IAddCementQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.cementRateRepo.addCementQuantity(uid, req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:CementQuantityId", validate(editCementQuantitySchema))
  async editCementQuantity(
    req: IEditCementQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.cementRateRepo.editCementQuantity(
      req.user.uid,
      req.params.CementQuantityId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete("/:CementQuantityId", validate(removeCementQuantitySchema))
  async removeCementQuantity(
    req: IRemoveCementQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementRateRepo.removeCementQuantity(
        req.params.CementQuantityId,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getCementQuantitySchema))
  async List(req: IGetCementQuantityRequest, res: Response) {
    const data = await this.cementRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
