import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ICementQuantity {
  address_id: string
  brand_id: string
  grade_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddCementQuantityRequest extends IAuthenticatedRequest {
  body: ICementQuantity
}

export const addCementQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex).required(),
    grade_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export interface IEditCementQuantity {
  address_id: string
  brand_id: string
  grade_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditCementQuantityRequest extends IAuthenticatedRequest {
  params: {
    CementQuantityId: string
  }
  body: IEditCementQuantity
}

export const editCementQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number().min(1),
    authentication_code: Joi.string().required(),
  }),

  params: Joi.object().keys({
    CementQuantityId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveCementQuantityRequest extends IAuthenticatedRequest {
  params: {
    CementQuantityId: string
  }
}

export const removeCementQuantitySchema: IRequestSchema = {
  params: Joi.object().keys({
    CementQuantityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetCementQuantityList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  grade_id?: string
  quantity?: any
  quantity_value?: string
}

export interface IGetCementQuantityRequest extends IAuthenticatedRequest {
  query: IGetCementQuantityList
}

export const getCementQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
  }),
}
