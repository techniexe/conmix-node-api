import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface IConcretePump {
  user_id?: string
  company_name: string
  concrete_pump_model: number
  manufacture_year: number
  pipe_connection: number
  concrete_pump_capacity: number
  is_Operator: boolean
  operator_id: number
  is_helper: boolean
  transportation_charge: number
  concrete_pump_price: number
  serial_number: string
  address_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditConcretePump {
  company_name: string
  concrete_pump_model: number
  manufacture_year: number
  pipe_connection: number
  concrete_pump_capacity: number
  is_Operator: boolean
  operator_id: number
  is_helper: boolean
  transportation_charge: number
  concrete_pump_price: number
  serial_number: string
  address_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddConcretePumpRequest extends IAuthenticatedRequest {
  body: IConcretePump
  params: {
    ConcretePumpCategoryId: string
  }
  files: {
    concrete_pump_image: UploadedFile
  }
}

export interface IEditConcretePumpRequest extends IAuthenticatedRequest {
  params: {
    ConcretePumpId: string
  }
  files: {
    concrete_pump_image: UploadedFile
  }
  body: IEditConcretePump
}

export const addConcretePumpSchema: IRequestSchema = {
  body: Joi.object().keys({
    company_name: Joi.string().required(),
    concrete_pump_model: Joi.string().required(),
    manufacture_year: Joi.number().max(9999).required(),
    pipe_connection: Joi.number().required(),
    concrete_pump_capacity: Joi.number().required(),
    is_Operator: Joi.boolean().valid([true, false]).required(),
    operator_id: Joi.string().regex(objectIdRegex),
    is_helper: Joi.boolean().valid([true, false]).required(),
    transportation_charge: Joi.number().required(),
    concrete_pump_price: Joi.number().required(),
    serial_number: Joi.string().required(),
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    ConcretePumpCategoryId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const editConcretePumpRequestSchema: IRequestSchema = {
  body: Joi.object().keys({
    company_name: Joi.string(),
    concrete_pump_model: Joi.string(),
    manufacture_year: Joi.number().max(9999),
    pipe_connection: Joi.number(),
    concrete_pump_capacity: Joi.number(),
    is_Operator: Joi.boolean().valid([true, false]),
    operator_id: Joi.string().regex(objectIdRegex),
    is_helper: Joi.boolean().valid([true, false]),
    transportation_charge: Joi.number(),
    concrete_pump_price: Joi.number(),
    is_active: Joi.boolean(),
    serial_number: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),

  params: Joi.object().keys({
    ConcretePumpId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetConcretePumpList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  concrete_pump_category_id?: string
  serial_number?: string
  address_id?: string
}

export interface IGetConcretePumpRequest extends IAuthenticatedRequest {
  query: IGetConcretePumpList
}

export const getConcretePumpSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    concrete_pump_category_id: Joi.string().regex(objectIdRegex),
    serial_number: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetConcretePumpDetailsRequest extends IAuthenticatedRequest {
  params: {
    ConcretePumpId: string
  }
}

export const getConcretePumpDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    ConcretePumpId: Joi.string().regex(objectIdRegex),
  }),
}
