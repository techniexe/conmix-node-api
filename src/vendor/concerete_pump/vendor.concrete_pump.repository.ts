import { VendorTypes } from "./../vendor.types"
import { injectable, inject } from "inversify"

import { ConcretePumpModel } from "../../model/concrete_pump.model"
import {
  IConcretePump,
  IEditConcretePump,
  IGetConcretePumpList,
} from "./vendor.concrete_pump.req-schema"
import { ObjectId } from "bson"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { OperatorInfoModel } from "../../model/operator.model"
import { AddressModel } from "../../model/address.model"

@injectable()
export class VendorConcretePumpRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  // async listAllConcretePumpCategory() {
  //   return ConcretePumpCategoryModel.find()
  // }
  async listOperator(user_id: string) {
    return OperatorInfoModel.find({ "user.user_id": user_id })
  }
  async addConcretePump(
    uid: string,
    ConcretePumpCategoryId: string,
    reqBody: IConcretePump,
    fileData: {
      concrete_pump_image: UploadedFile
    }
  ) {
    reqBody.user_id = uid
    reqBody.concrete_pump_category_id = ConcretePumpCategoryId
    reqBody._id = new ObjectId()

    let data = await ConcretePumpModel.findOne({
      serial_number: reqBody.serial_number,
    })
    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Concrete pump already exists with same serial number`,
          400
        )
      )
    }

    let sub_vendor_id: any
    if (!isNullOrUndefined(reqBody.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(reqBody.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
    }
    reqBody.sub_vendor_id = sub_vendor_id


    if (!isNullOrUndefined(reqBody.operator_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(reqBody.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let opData = await OperatorInfoModel.findOne({
        _id: new ObjectId(reqBody.operator_id),
      })

      if (!isNullOrUndefined(opData) &&
      opData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This operator is not belongs to selected address.`,
            400
          )
        )
      }
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.concrete_pump_image) &&
      !isNullOrUndefined(fileData.concrete_pump_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/Concrete_pump`
      let objectName = "" + reqBody._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.concrete_pump_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.concrete_pump_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.concrete_pump_image.data,
        fileData.concrete_pump_image.mimetype
      )
      reqBody.concrete_pump_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.ADD_CONCRETE_PUMP,
      "Concrete Pump information Added."
    )

    console.log("Here")
    return new ConcretePumpModel(reqBody).save()
  }

  async editConcretePump(
    user_id: string,
    ConcretePumpId: string,
    editData: IEditConcretePump,
    fileData: {
      concrete_pump_image: UploadedFile
    }
  ): Promise<any> {
    const editObj: { $set: { [K: string]: any } } = { $set: {} }
    const edtFld = [
      "company_name",
      "concrete_pump_model",
      "manufacture_year",
      "pipe_connection",
      "concrete_pump_capacity",
      "is_Operator",
      "operator_id",
      "is_helper",
      "transportation_charge",
      "concrete_pump_price",
      "is_active",
      "serial_number",
      "address_id",
      "master_vendor_id",
    ]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editObj.$set[key] = val
      }
    }
    if (Object.keys(editObj.$set).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    const check = await ConcretePumpModel.findOne({ _id: new ObjectId(ConcretePumpId) })
    if (check === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    if (!isNullOrUndefined(editData.serial_number)) {
      const numberCheck = await ConcretePumpModel.findOne({
        serial_number: editData.serial_number,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(ConcretePumpId) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `Same serial number already exists with another concrete pump.`,
            400
          )
        )
      }
    }

    // if (!isNullOrUndefined(editData.serial_number) &&
    // check.serial_number !== editData.serial_number) {
    //   editObj.$set.serial_number = editData.serial_number
    // }

    let sub_vendor_id: any
    if (!isNullOrUndefined(editData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      editObj.$set.sub_vendor_id = sub_vendor_id
    }

    if (!isNullOrUndefined(editData.operator_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let opData = await OperatorInfoModel.findOne({
        _id: new ObjectId(editData.operator_id),
      })

      if (!isNullOrUndefined(opData) &&
      opData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This operator is not belongs to selected address.`,
            400
          )
        )
      }
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.concrete_pump_image) &&
      !isNullOrUndefined(fileData.concrete_pump_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM`
      let objectName = "" + ConcretePumpId + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.concrete_pump_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.concrete_pump_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.concrete_pump_image.data,
        fileData.concrete_pump_image.mimetype
      )
      editObj.$set.concrete_pump_image = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    const edtRes = await ConcretePumpModel.updateOne(
      {
        _id: ConcretePumpId,
        // user_id,
      },
      editObj
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(
          `Invalid request as Transit Mixer has not been added`,
          400
        )
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_CONCRETE_PUMP,
      "Concrete Pump information Updated."
    )

    return Promise.resolve()
  }
  async getConcretePump(user_id: string, searchParam: IGetConcretePumpList) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    let limit: number = 10
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.serial_number)) {
      query.serial_number = searchParam.serial_number
    }

    if (!isNullOrUndefined(searchParam.concrete_pump_category_id)) {
      query.concrete_pump_category_id = new ObjectId(
        searchParam.concrete_pump_category_id
      )
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "concrete_pump_category_id",
          foreignField: "_id",
          as: "concrete_pump_category",
        },
      },
      {
        $unwind: {
          path: "$concrete_pump_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "operator_info",
          localField: "operator_id",
          foreignField: "_id",
          as: "operator_info",
        },
      },
      {
        $unwind: {
          path: "$operator_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          concrete_pump_category_id: 1,
          company_name: 1,
          concrete_pump_model: 1,
          manufacture_year: 1,
          pipe_connection: 1,
          concrete_pump_capacity: 1,
          is_Operator: 1,
          operator_id: 1,
          is_helper: 1,
          transportation_charge: 1,
          concrete_pump_price: 1,
          concrete_pump_image_url: 1,
          created_at: 1,
          updated_at: 1,
          serial_number: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          business_name: "$address.business_name",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: "$address.pincode",
          "concrete_pump_category.category_name": 1,
          "concrete_pump_category.is_active": 1,
          "operator_info._id": 1,
          "operator_info.operator_name": 1,
          "operator_info.operator_mobile_number": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await ConcretePumpModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getConcretePumpDetails(user_id: string, ConcretePumpId: string) {
    const query: { [k: string]: any } = {}
    query._id = new ObjectId(ConcretePumpId)

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    // query.user_id = new ObjectId(user_id)

    console.log(query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "concrete_pump_category_id",
          foreignField: "_id",
          as: "concrete_pump_category",
        },
      },
      {
        $unwind: {
          path: "$concrete_pump_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          concrete_pump_category_id: 1,
          company_name: 1,
          concrete_pump_model: 1,
          manufacture_year: 1,
          pipe_connection: 1,
          concrete_pump_price: 1,
          concrete_pump_capacity: 1,
          is_Operator: 1,
          operator_id: 1,
          is_helper: 1,
          transportation_charge: 1,
          concrete_pump_image_url: 1,
          created_at: 1,
          updated_at: 1,
          serial_number: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: 1,
          "concrete_pump_category.category_name": 1,
          "concrete_pump_category.is_active": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]

    const data = await ConcretePumpModel.aggregate(aggregateArr)
    return Promise.resolve(data[0])
  }
}
