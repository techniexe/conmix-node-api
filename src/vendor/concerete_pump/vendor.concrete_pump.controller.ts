import { injectable, inject } from "inversify"
import {
  controller,
  httpGet,
  httpPost,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorConcretePumpRepository } from "./vendor.concrete_pump.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addConcretePumpSchema,
  IAddConcretePumpRequest,
  editConcretePumpRequestSchema,
  IEditConcretePumpRequest,
  getConcretePumpSchema,
  IGetConcretePumpRequest,
  getConcretePumpDetailsSchema,
  IGetConcretePumpDetailsRequest,
} from "./vendor.concrete_pump.req-schema"

@injectable()
@controller("/concrete_pump", verifyCustomToken("vendor"))
export class VendorConcretePumpController {
  constructor(
    @inject(VendorTypes.VendorConcretePumpRepository)
    private ConcretePumpRepo: VendorConcretePumpRepository
  ) {}

  // @httpGet("/categories")
  // async listAllConcretePumpCategory(
  //   req: IRequestSchema,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   const data = await this.ConcretePumpRepo.listAllConcretePumpCategory()
  //   res.json({ data })
  // }

  @httpGet("/operator")
  async listOperator(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.listOperator(req.user.uid)
    res.json({ data })
  }

  @httpPost("/:ConcretePumpCategoryId", validate(addConcretePumpSchema))
  async addConcretePump(
    req: IAddConcretePumpRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { ConcretePumpCategoryId } = req.params
      await this.ConcretePumpRepo.addConcretePump(
        uid,
        ConcretePumpCategoryId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:ConcretePumpId", validate(editConcretePumpRequestSchema))
  async editConcretePump(
    req: IEditConcretePumpRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.ConcretePumpRepo.editConcretePump(
      req.user.uid,
      req.params.ConcretePumpId,
      req.body,
      req.files
    )
    res.sendStatus(202)
  }

  @httpGet("/", validate(getConcretePumpSchema))
  async getConcretePump(
    req: IGetConcretePumpRequest,
    res: Response,
    next: NextFunction
  ) {
    console.log(req.user.uid)
    const data = await this.ConcretePumpRepo.getConcretePump(
      req.user.uid,
      req.query
    )
    res.json({ data })
  }
  @httpGet("/:ConcretePumpId", validate(getConcretePumpDetailsSchema))
  async getConcretePumpDetails(
    req: IGetConcretePumpDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.ConcretePumpRepo.getConcretePumpDetails(
      req.user.uid,
      req.params.ConcretePumpId
    )
    res.json({ data })
  }
}
