import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../../middleware/auth-token.middleware"
import {
  IRequestSchema,
  objectIdRegex,
} from "../../../middleware/joi.middleware"

export interface ICPUn {
  CPId: string
  // unavailable_at: string
  start_time: string
  end_time: string
  master_vendor_id: string
  sub_vendor_id: string
  [k: string]: any
}

export interface IAddCPUnRequest extends IAuthenticatedRequest {
  body: ICPUn
  params: {
    CPId: string
    start_time: string
    end_time: string
  }
}

export const addCPUnSchema: IRequestSchema = {
  body: Joi.object().keys({
    // unavailable_at: Joi.date().required(),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    CPId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IEditCPUn {
  // unavailable_at: string
  start_time: string
  end_time: string
  master_vendor_id: string
  sub_vendor_id: string
  [k: string]: any
}

export interface IEditCPUnRequest extends IAuthenticatedRequest {
  params: {
    id: string
  }
  body: IEditCPUn
}

export const editCPUnSchema: IRequestSchema = {
  body: Joi.object().keys({
    unavailable_at: Joi.date(),
    start_time: Joi.string(),
    end_time: Joi.string(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const listCPunSchema: IRequestSchema = {
  params: Joi.object().keys({
    CPId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IListUnRequest extends IAuthenticatedRequest {
  params: {
    CPId: string
  }
}

export interface IDeleteCPUnRequest extends IAuthenticatedRequest {
  params: {
    id: string
  }
}