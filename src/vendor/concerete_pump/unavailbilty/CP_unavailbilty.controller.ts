import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../../middleware/auth-token.middleware"
import { VendorTypes } from "../../vendor.types"
import { VendorCPunavailbiltyRepository } from "./CP_unavailbilty.repository"
import { NextFunction, Response } from "express"
import {
  IListUnRequest,
  IAddCPUnRequest,
  addCPUnSchema,
  editCPUnSchema,
  IEditCPUnRequest,
  listCPunSchema,
  IDeleteCPUnRequest,
} from "./CP_unavailbilty.req-schema"
import { validate } from "../../../middleware/joi.middleware"

@injectable()
@controller("/CP_unavailbilty", verifyCustomToken("vendor"))
export class VendorCPunavailbiltyController {
  constructor(
    @inject(VendorTypes.VendorCPunavailbiltyRepository)
    private CPunavailbiltyRepo: VendorCPunavailbiltyRepository
  ) {}

  @httpPost("/:CPId", validate(addCPUnSchema))
  async add(req: IAddCPUnRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    try {
      const { CPId } = req.params
      await this.CPunavailbiltyRepo.add(uid, CPId, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:id", validate(editCPUnSchema))
  async edit(req: IEditCPUnRequest, res: Response, next: NextFunction) {
    await this.CPunavailbiltyRepo.edit(req.user.uid, req.params.id, req.body)
    res.sendStatus(204)
  }

  @httpGet("/:CPId", validate(listCPunSchema))
  async get(req: IListUnRequest, res: Response, next: NextFunction) {
    console.log(req.user.uid)
    const data = await this.CPunavailbiltyRepo.get(
      req.user.uid,
      req.params.CPId
    )
    res.json({ data })
  }

  @httpDelete("/:id")
  async delete(req: IDeleteCPUnRequest, res: Response, next: NextFunction) {
    await this.CPunavailbiltyRepo.delete(req.user.uid, req.params.id)
    res.sendStatus(204)
  }
}
