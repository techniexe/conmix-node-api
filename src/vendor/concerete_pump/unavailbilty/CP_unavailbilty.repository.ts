import { ObjectId } from "bson"
import { injectable, inject } from "inversify"

import { AccessTypes } from "../../../model/access_logs.model"

import { VendorTypes } from "../../vendor.types"
import { CommonService } from "../../../utilities/common.service"
import { InvalidInput } from "../../../utilities/customError"
import { ICPUn, IEditCPUn } from "./CP_unavailbilty.req-schema"
import { CP_unModel } from "../../../model/CP_un.model"

@injectable()
export class VendorCPunavailbiltyRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async add(uid: string, CPId: string, reqBody: ICPUn) {
    reqBody.vendor_id = uid
    reqBody.CPId = CPId

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.ADD_CP_UN,
      "CP Unavailablity information Added."
    )
    console.log("Here")
    return new CP_unModel(reqBody).save()
  }

  async edit(user_id: string, id: string, editData: IEditCPUn): Promise<any> {
    const editObj: { $set: { [K: string]: any } } = { $set: {} }
    const edtFld = ["unavailable_at", "start_time", "end_time", "sub_vendor_id", "master_vendor_id"]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editObj.$set[key] = val
      }
    }
    if (Object.keys(editObj.$set).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    const edtRes = await CP_unModel.updateOne(
      {
        _id: new ObjectId(id),
        //vendor_id: new ObjectId(user_id),
      },
      editObj
    )
    if (edtRes.n !== 1) {
      return Promise.reject(new InvalidInput(`Invalid request as Transit Mixer has not been added`, 400))
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_CP_UN,
      "CP Unavailablity information Updated."
    )

    return Promise.resolve()
  }

  async get(vendor_id: string, CPId: string) {
    // const data = await CP_unModel.find({
    //   CPId: new ObjectId(CPId),
    //   vendor_id: new ObjectId(vendor_id),
    // })

    // return Promise.resolve(data)

    const CPUNquery: { [k: string]: any } = {  CPId: new ObjectId(CPId)}

    CPUNquery.$or = [
        {
          vendor_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let data = await CP_unModel.find(CPUNquery)

    return Promise.resolve(data)
  }

  async delete(user_id: string, id: string){
    return CP_unModel.findOneAndRemove({
      _id: new ObjectId(id)
    })
  }
}
