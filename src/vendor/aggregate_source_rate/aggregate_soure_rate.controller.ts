import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AggregateSourceRateRepository } from "./aggregate_soure_rate.repository"
import {
  addAggregateSourceRateSchema,
  IAddAggregateSourceRateRequest,
  IEditAggregateSourceRateRequest,
  editAggregateSourceRateSchema,
  IRemoveAggregateSourceRateRequest,
  removeAggregateSourceRateSchema,
  IGetAggregateSourceRateRequest,
  getAggregateSourceRateSchema,
} from "./aggregate_soure_rate.req-schema"

@injectable()
@controller("/aggregate_source_rate", verifyCustomToken("vendor"))
export class AggregateSourceRateController {
  constructor(
    @inject(VendorTypes.AggregateSourceRateRepository)
    private aggregateSourceRateRepo: AggregateSourceRateRepository
  ) {}

  @httpPost("/", validate(addAggregateSourceRateSchema))
  async addAggregateSourceRate(
    req: IAddAggregateSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.aggregateSourceRateRepo.addAggregateSourceRate(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:AggregateSourceRateId", validate(editAggregateSourceRateSchema))
  async editAggregateSourceRate(
    req: IEditAggregateSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.aggregateSourceRateRepo.editAggregateSourceRate(
      req.user.uid,
      req.params.AggregateSourceRateId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete(
    "/:AggregateSourceRateId",
    validate(removeAggregateSourceRateSchema)
  )
  async removeAggregateSourceRate(
    req: IRemoveAggregateSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.aggregateSourceRateRepo.removeAggregateSourceRate(
        req.params.AggregateSourceRateId
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getAggregateSourceRateSchema))
  async List(req: IGetAggregateSourceRateRequest, res: Response) {
    const data = await this.aggregateSourceRateRepo.List(
      req.user.uid,
      req.query
    )
    res.json({ data })
  }
}
