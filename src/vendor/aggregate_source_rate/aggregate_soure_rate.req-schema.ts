import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAggregateSourceRate {
  address_id: string
  source_id: string
  sub_cat_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddAggregateSourceRateRequest extends IAuthenticatedRequest {
  body: IAggregateSourceRate
}

export const addAggregateSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    source_id: Joi.string().regex(objectIdRegex).required(),
    sub_cat_id: Joi.string().regex(objectIdRegex).required(),
    per_kg_rate: Joi.number().min(1).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IEditAggregateSourceRate {
  address_id: string
  source_id: string
  sub_cat_id: string
  per_kg_rate: number
  vendor_id: string
  [k: string]: any
}

export interface IEditAggregateSourceRateRequest extends IAuthenticatedRequest {
  params: {
    AggregateSourceRateId: string
  }
  body: IEditAggregateSourceRate
}

export const editAggregateSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    sub_cat_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number().min(1),
  }),

  params: Joi.object().keys({
    AggregateSourceRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveAggregateSourceRateRequest
  extends IAuthenticatedRequest {
  params: {
    AggregateSourceRateId: string
  }
}

export const removeAggregateSourceRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    AggregateSourceRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetAggregateSourceRateList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  sub_cat_id?: string
  per_kg_rate?: any
  per_kg_rate_value?: string
 
}

export interface IGetAggregateSourceRateRequest extends IAuthenticatedRequest {
  query: IGetAggregateSourceRateList
}

export const getAggregateSourceRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    sub_cat_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number(),
    per_kg_rate_value: Joi.string(),
  }),
}
