import { ObjectId } from "bson"
import { inject, injectable } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"

import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { AggregateSourceRateModel } from "../../model/aggregate_source_rate.model"
import {
  IAggregateSourceRate,
  IEditAggregateSourceRate,
  IGetAggregateSourceRateList,
} from "./aggregate_soure_rate.req-schema"
import { AddressModel } from "../../model/address.model"

@injectable()
export class AggregateSourceRateRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addAggregateSourceRate(
    vendor_id: string,
    AggregateSourceRateData: IAggregateSourceRate
  ) {
    const data = await AggregateSourceRateModel.findOne({
      vendor_id,
      address_id: AggregateSourceRateData.address_id,
      source_id: AggregateSourceRateData.source_id,
      sub_cat_id: AggregateSourceRateData.sub_cat_id,
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Aggregate rate already exists for this source at selected site address.`,
          400
        )
      )
    }
    AggregateSourceRateData.vendor_id = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(AggregateSourceRateData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(AggregateSourceRateData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      }
      AggregateSourceRateData.sub_vendor_id = sub_vendor_id

    const newAggregateSourceRate = new AggregateSourceRateModel(
      AggregateSourceRateData
    )
    const newaggregateSandRateDoc = await newAggregateSourceRate.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_AGGREGATE_SOURCE_RATE_BY_VENDOR,
      `Vendor added aggreagte rate of id ${newaggregateSandRateDoc._id} .`
    )

    return Promise.resolve(newaggregateSandRateDoc)
  }

  async editAggregateSourceRate(
    vendor_id: string,
    AggregateSourceRateId: string,
    editData: IEditAggregateSourceRate
  ): Promise<any> {

    const data = await AggregateSourceRateModel.findOne({
      vendor_id,
      address_id: editData.address_id,
      source_id: editData.source_id,
      sub_cat_id: editData.sub_cat_id,
    })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(AggregateSourceRateId) === false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Aggregate rate already exists for this source at selected site address.`,
          400
        )
      )
    }

    const editDoc: { [k: string]: any } = {}
    const edtFld = ["address_id", "source_id", "sub_cat_id", "per_kg_rate", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id


    let sub_vendor_id: any
    if(!isNullOrUndefined(editData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(editData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    const edtRes = await AggregateSourceRateModel.updateOne(
      {
        _id: new ObjectId(AggregateSourceRateId),
       // vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`No Aggreagte source rate found to edit`, 400)
      )
    }
    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_AGGREGATE_SOURCE_RATE_BY_VENDOR,
      "aggreagte rate information Updated."
    )

    return Promise.resolve()
  }
  async removeAggregateSourceRate(AggregateSourceRateId: string) {
    const status = await AggregateSourceRateModel.deleteOne({
      _id: new ObjectId(AggregateSourceRateId),
    })
    if (status.n !== 1) {
      throw new Error("Aggregate source rate has not been added.")
    }
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetAggregateSourceRateList) {
    const query: { [k: string]: any } = {}

    // query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (!isNullOrUndefined(searchParam.sub_cat_id)) {
      query.sub_cat_id = new ObjectId(searchParam.sub_cat_id)
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "less") {
      query.per_kg_rate = {$lte: parseInt(searchParam.per_kg_rate)}
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "more") {
      query.per_kg_rate = {$gte: parseInt(searchParam.per_kg_rate)}
    }

    
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "sub_cat_id",
          foreignField: "_id",
          as: "aggregate_sand_sub_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate_sand_sub_category._id": 1,
          "aggregate_sand_sub_category.sub_category_name": 1,
          per_kg_rate: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await AggregateSourceRateModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
