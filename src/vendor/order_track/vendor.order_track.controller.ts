import { injectable, inject } from "inversify"
import {
  controller,
  httpGet,
  httpPost,
  httpPatch,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorOrderTrackRepository } from "./vendor.order_track.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  requestOTPSchema,
  IRequestOTPRequest,
  verifyMobilenumberSchema,
  IVerifyMobilenumberRequest,
  IGetOrderByTMIdRequest,
  updateOrderSchema,
  IUpdateOrderRequest,
  addOrderAsPickupSchema,
  IAddOrderAsPickupRequest,
  IUploadDocumentRequest,
  IGetOrderDetailsRequest,
  orderDelayedSchema,
  IOrderDelayedRequest,
  orderDeliveredSchema,
  IOrderDeliveredRequest,
  getVendorOrderTrackSchema,
  IGetVendorOrderTrackRequest,
  IAddTMtoOrderTrackRequest,
  addTMtoOrderTrackSchema,
  addCPtoOrderTrackSchema,
  IAddCPtoOrderTrackRequest,
  IGetOrderByCPIdRequest,
  updateCPOrderTrackSchema,
  IUpdateCPOrderTrackRequest,
  addCPOrderAsPickupSchema,
  IAddCPOrderAsPickupRequest,
  IUploadCPDocumentRequest,
  CPorderDelayedSchema,
  ICPOrderDelayedRequest,
  CPorderDeliveredSchema,
  ICPOrderDeliveredRequest,
  checkCPStatusSchema,
  ICheckCPStatusRequest,
  IGetVendorOrderCPTrackRequest,
  getVendorOrderCPTrackSchema,
  uploadInvoiceSchema,
  uploadVendorInvoiceSchema,
} from "./vendor.order_track.req-schema"
import { NextFunction, Response } from "express"

@injectable()
@controller("/order_track", verifyCustomToken("vendor"))
// @controller("/order_track")
export class VendorOrderTrackController {
  constructor(
    @inject(VendorTypes.VendorOrderTrackRepository)
    private orderTrackRepo: VendorOrderTrackRepository
  ) {}

  @httpPost("/uploadInvoice", validate(uploadInvoiceSchema))
  async uploadInvoice(req: any, res: Response, next: NextFunction) {
    try {
      await this.orderTrackRepo.uploadInvoice(req.user.uid, req.body)
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/uploadVendorInvoice", validate(uploadVendorInvoiceSchema))
  async uploadVendorInvoice(req: any, res: Response, next: NextFunction) {
    try {
      await this.orderTrackRepo.uploadVendorInvoice(req.user.uid, req.body)
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/requestOTP/:mobile_number", validate(requestOTPSchema))
  async requestOTP(req: IRequestOTPRequest, res: Response, next: NextFunction) {
    console.log("Hereee")
    try {
      const data = await this.orderTrackRepo.requestOTP(
        req.user.uid,
        req.params.mobile_number
      )
      return res.send({ code: data.code })
    } catch (err) {
      next(err)
    }
  }

  @httpPost(
    "/mobile/:mobile_number/code/:code",
    validate(verifyMobilenumberSchema)
  )
  async verifyMobilenumber(
    req: IVerifyMobilenumberRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.verifyUserWithMobilenumber(
        req.user.uid,
        req.params.mobile_number,
        req.params.code
      )
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  //add TM to order track.
  @httpPost("/:order_id/:order_item_id", validate(addTMtoOrderTrackSchema))
  async addTMtoOrderTrack(
    req: IAddTMtoOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.addTMtoOrderTrack(
        req.user.uid,
        req.params.order_id,
        req.params.order_item_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  //add CP to order track.
  @httpPost("/CP/:order_id/:order_item_id", validate(addCPtoOrderTrackSchema))
  async addCPtoOrderTrack(
    req: IAddCPtoOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.addCPtoOrderTrack(
        req.user.uid,
        req.params.order_id,
        req.params.order_item_id,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/CP/track/:order_item_part_id",
    validate(getVendorOrderCPTrackSchema)
  )
  async getVendorOrderCPTrack(
    req: IGetVendorOrderCPTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_part_id } = req.params
      const data = await this.orderTrackRepo.getVendorOrderCPTrack(
        order_item_part_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  //Get order by TMId, whose status = "TM_ASSIGNED".
  @httpGet("/:TMId")
  async getOrderByTMId(
    req: IGetOrderByTMIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { TMId } = req.params
      let { current_date } = req.query
      const order = await this.orderTrackRepo.getOrderByTMId(
        TMId,
        req.user.uid,
        current_date
      )
      res.json({ order })
    } catch (err) {
      next(err)
    }
  }

  //Get order by CPId, whose status = "CP_ASSIGNED".
  @httpGet("/CP/:CPId")
  async getOrderByCPId(
    req: IGetOrderByCPIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { CPId } = req.params
      let { current_date } = req.query
      const order = await this.orderTrackRepo.getOrderByCPId(
        CPId,
        req.user.uid,
        current_date
      )
      res.json({ order })
    } catch (err) {
      next(err)
    }
  }

  // Update order quantity and status.
  @httpPatch("/:track_id/:TMId/:order_id", validate(updateOrderSchema))
  async updateOrder(
    req: IUpdateOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.updateOrder(
        req.user.uid,
        req.params.track_id,
        req.params.order_id,
        req.params.TMId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Update CP order track status.
  @httpPatch(
    "/CP/:track_id/:CPId/:order_id",
    validate(updateCPOrderTrackSchema)
  )
  async updateCPOrderTrack(
    req: IUpdateCPOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderTrackRepo.updateCPOrderTrack(
        req.user.uid,
        req.params.track_id,
        req.params.order_id,
        req.params.CPId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
  // Add CP status.
  @httpGet("/checkCPStatus/:order_id", validate(checkCPStatusSchema))
  async checkCPStatus(
    req: ICheckCPStatusRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id } = req.params
      await this.orderTrackRepo.checkCPStatus(order_id, req.user.uid)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Add order in PICKUP status.
  @httpPost(
    "/:track_id/:TMId/:order_id/pickup",
    validate(addOrderAsPickupSchema)
  )
  async addOrderAsPickup(
    req: IAddOrderAsPickupRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id } = req.params
      await this.orderTrackRepo.addOrderAsPickup(
        order_id,
        req.params.track_id,
        req.params.TMId,
        req.user.uid
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Add order in PICKUP status.
  @httpPost(
    "/CP/:track_id/:CPId/:order_id/pickup",
    validate(addCPOrderAsPickupSchema)
  )
  async addCPOrderAsPickup(
    req: IAddCPOrderAsPickupRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id } = req.params
      await this.orderTrackRepo.addCPOrderAsPickup(
        order_id,
        req.params.track_id,
        req.params.CPId,
        req.user.uid
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Upload Order Track document.
  @httpPost("/:track_id/:TMId/:order_id/uploadDoc")
  async uploadDocument(
    req: IUploadDocumentRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id, TMId, track_id } = req.params
      await this.orderTrackRepo.uploadDocument(
        order_id,
        track_id,
        req.user.uid,
        TMId,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Upload CP Track document.
  @httpPost("/CP/:track_id/:CPId/:order_id/uploadDoc")
  async uploadCPDocument(
    req: IUploadCPDocumentRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id, CPId, track_id } = req.params
      await this.orderTrackRepo.uploadCPDocument(
        order_id,
        track_id,
        req.user.uid,
        CPId,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  //Order Track details.
  @httpGet("/OrderDetails/:order_id/:track_id")
  async getOrderDetailsById(
    req: IGetOrderDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id, track_id } = req.params
      const order = await this.orderTrackRepo.getOrderDetailsById(
        track_id,
        order_id,
        req.user.uid
      )
      res.json({ order })
    } catch (err) {
      next(err)
    }
  }

  // Upload CP Order Track details.
  @httpGet("/CP/OrderDetails/:order_id/:track_id")
  async getCPOrderTrackDetailsById(
    req: IGetOrderDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      let { order_id, track_id } = req.params
      const order = await this.orderTrackRepo.getCPOrderTrackDetailsById(
        track_id,
        order_id,
        req.user.uid
      )
      res.json({ order })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/:TMId/:orderId/:track_id/delay", validate(orderDelayedSchema))
  async orderDelayed(
    req: IOrderDelayedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { reasonForDelay, delayTime } = req.body
      const { orderId, TMId, track_id } = req.params
      await this.orderTrackRepo.orderDelayed(
        req.user.uid,
        reasonForDelay,
        delayTime,
        orderId,
        TMId,
        track_id
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // CP order delayed
  @httpPost(
    "/CP/:CPId/:orderId/:track_id/delay",
    validate(CPorderDelayedSchema)
  )
  async CPorderDelayed(
    req: ICPOrderDelayedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { reasonForDelay, delayTime } = req.body
      const { orderId, CPId, track_id } = req.params
      await this.orderTrackRepo.CPorderDelayed(
        req.user.uid,
        reasonForDelay,
        delayTime,
        orderId,
        CPId,
        track_id
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // when order delivered.
  @httpPost("/:TMId/:orderId/:track_id/deliver", validate(orderDeliveredSchema))
  async orderDelivered(
    req: IOrderDeliveredRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { TMId, orderId, track_id } = req.params
      await this.orderTrackRepo.orderDelivered(
        req.user.uid,
        TMId,
        orderId,
        track_id
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // when CP order delivered.
  @httpPost(
    "/CP/:CPId/:orderId/:track_id/deliver",
    validate(CPorderDeliveredSchema)
  )
  async CPorderDelivered(
    req: ICPOrderDeliveredRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { CPId, orderId, track_id } = req.params
      await this.orderTrackRepo.CPorderDelivered(
        req.user.uid,
        CPId,
        orderId,
        track_id
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:order_item_id/:tracking_id", validate(getVendorOrderTrackSchema))
  async getVendorOrderTrack(
    req: IGetVendorOrderTrackRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { order_item_id, tracking_id } = req.params
      const data = await this.orderTrackRepo.getVendorOrderTrack(
        order_item_id,
        tracking_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
