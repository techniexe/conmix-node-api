import { injectable, inject } from "inversify"
//import { generateRandomNumber } from "../../utilities/generate-random-number"
import { AuthModel } from "../../model/auth.model"
import { InvalidInput } from "../../utilities/customError"
import { VendorTypes } from "../vendor.types"
//import { TwilioService } from "../../utilities/twilio-service"
import {
  IUpdateOrder,
  IAddTMtoOrderTrack,
  IAddCPtoOrderTrack,
  IUpdateCPOrderTrack,
  IUploadInvoice,
  IUploadVendorInvoice,
} from "./vendor.order_track.req-schema"
import { ObjectId } from "bson"
import { TrackStatus, OrderTrackModel } from "../../model/order_track.model"
import { OrderItemModel } from "../../model/order_item.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  TMCategoryModel,
  TMModel,
  TMSubCategoryModel,
} from "../../model/TM.model"
import {
  VendorOrderModel,
  SupplierOrderStatus,
} from "../../model/vendor.order.model"
import { OrderItemStatus, awsConfig, UserType } from "../../utilities/config"
import { OrderModel } from "../../model/order.model"
import { UploadedFile } from "express-fileupload"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { VendorUserModel } from "../../model/vendor.user.model"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import {
  ClientNotificationType,
  VendorNotificationType,
  AdminNotificationType,
} from "../../model/notification.model"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import {
  ConcretePumpCategoryModel,
  ConcretePumpModel,
} from "../../model/concrete_pump.model"
import { CPOrderTrackModel } from "../../model/CP_order_track.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { CommonService } from "../../utilities/common.service"
import { DriverInfoModel } from "../../model/driver.model"
import { OperatorInfoModel } from "../../model/operator.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { FlyAshSourceQuantityModel } from "../../model/fly_ash_source_quantity.model"
import { SiteModel } from "../../model/site.model"
import { TestService } from "../../utilities/test.service"
import { MailGunService } from "../../utilities/mailgun.service"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { client_mail_template } from "../../utilities/client_email_tempalte"
import { admin_mail_template } from "../../utilities/admin_email_template"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
import { AddressModel } from "../../model/address.model"
const fs = require("fs")

@injectable()
export class VendorOrderTrackRepository {
  constructor(
    // @inject(VendorTypes.TwilioService) private twilioService: TwilioService,
    @inject(VendorTypes.CommonService) private commonService: CommonService,
    @inject(VendorTypes.NotificationUtility)
    private notUtil: NotificationUtility,
    @inject(VendorTypes.TestService) private testService: TestService,
    @inject(VendorTypes.mailgunService) private mailgunService: MailGunService
  ) {}

  async requestOTP(user_id: string, mobile_number: string) {

    console.log("user_id", user_id)
    console.log("mobile_number", mobile_number)
    const code = generateRandomNumber()
    const authData = await AuthModel.findOneAndUpdate(
      {
        identifier: mobile_number,
        user_id,
      },
      {
        $set: { code, created_at: Date.now() },
      },
      { upsert: true, new: true }
    ).exec()

    console.log("authData", authData)
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(mobile_number, message, templateId)

    //await this.twilioService.sendCode(mobile_number, authData.code, "sms")
    return Promise.resolve(authData)
  }

  async verifyUserWithMobilenumber(
    user_id: string,
    mobile_number: string,
    code: string
  ) {
    const userCheck = await AuthModel.findOne({
      user_id,
      identifier: mobile_number,
      code,
    })

    if (userCheck === null) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again `, 400)
      )
    }

    await AuthModel.deleteOne({
      user_id,
      identifier: mobile_number,
    })
    return Promise.resolve()
  }

  async addTMtoOrderTrack(
    vendor_id: string,
    order_id: string,
    order_item_id: string,
    trackData: IAddTMtoOrderTrack
  ) {
    const data1 = await TMModel.findOne({
      _id: new ObjectId(trackData.TM_id),
    })

    // Check TM belongs to Vendor or not.
    if (isNullOrUndefined(data1)) {
      return Promise.reject(
        new InvalidInput(`TM not associated with this vendor.`, 400)
      )
    }

    const vorderData = await VendorOrderModel.findOne({
      _id: order_id,
    })

    // Check vendor order exists or not.
    if (isNullOrUndefined(vorderData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find Vendor order for the entered order Id.`,
          400
        )
      )
    }
  
    if (
      data1.sub_vendor_id.equals(new ObjectId(vorderData.sub_vendor_id)) ===
      false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find TM belongs to this vendor.`,
          400
        )
      )
    }

    let vendorData = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })

    // Check for capacity.
    if (
      !isNullOrUndefined(vendorData) &&
      !isNullOrUndefined(trackData.assigned_at)
    ) {
      let vendor_capacity =
        vendorData.no_of_plants *
        vendorData.plant_capacity_per_hour *
        vendorData.no_of_operation_hour

      let vendorOrderData: any = await VendorOrderModel.find({
        user_id: new ObjectId(vendor_id),
        delivery_date: new Date(trackData.assigned_at),
      })
      console.log("vendor_capacity", vendor_capacity)
      console.log("vendorOrderData", vendorOrderData)

      if (!isNullOrUndefined(vendorOrderData)) {
        var day_capacity = 0
        for (var j = 0; j < vendorOrderData.length; j++) {
          day_capacity += vendorOrderData[j].quantity
        }
        console.log("day_capacity", day_capacity)

        if (day_capacity > vendor_capacity) {
          return Promise.reject(
            new InvalidInput(
              `TM cannot be assigned as you've reached per day order capacity limit.`,
              400
            )
          )
        }
      }
    }


    const query: { [k: string]: any } = {
      vendor_user_id: new ObjectId(vendor_id),
      TM_id: new ObjectId(trackData.TM_id),
      assigned_at: trackData.assigned_at,
    }

    query.$or = [
      {
        event_status: TrackStatus.TM_ASSIGNED,
      },
      {
        event_status: TrackStatus.PICKUP,
      },
      {
        event_status: TrackStatus.DELAYED,
      },
    ]

    const aggregateArr = [
      { $match: query },
      {
        $project: {
          _id: true,
          remaining_quantity: true,
          pickup_quantity: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          vendor_order_id: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const data = await OrderTrackModel.aggregate(aggregateArr)
    if (data.length > 0) {
      return Promise.reject(
        new InvalidInput(
          `This TM is engaged for order with id #${data[0].vendor_order_id}. current status of order is ${data[0].event_status}.`,
          400
        )
      )
    }

    // Check order_item quantity.
    const OrderItemData = await OrderItemModel.findById(order_item_id)
    if (isNullOrUndefined(OrderItemData)) {
      return Promise.reject(
        new InvalidInput(`No item found with request id.`, 400)
      )
    }

    const VendorOrderData = await VendorOrderModel.findOne({
      _id: order_id,
      buyer_order_item_id: order_item_id,
    })

    // Check vendor order exists or not.
    if (isNullOrUndefined(VendorOrderData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find Vendor order for the entered order Id.`,
          400
        )
      )
    }

    const TMData = await TMModel.findOne({
      _id: new ObjectId(trackData.TM_id),
    })

    // Check TM belongs to Vendor or not.
    if (isNullOrUndefined(TMData)) {
      return Promise.reject(
        new InvalidInput(`TM not associated with this vendor.`, 400)
      )
    }

    const TMCatData = await TMCategoryModel.findOne({
      _id: new ObjectId(TMData.TM_category_id),
    })

    if (isNullOrUndefined(TMCatData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any data related to TM category.`,
          400
        )
      )
    }

    trackData.TM_category_name = TMCatData.category_name
    trackData.TM_rc_number = TMData.TM_rc_number

    let doc = await TMSubCategoryModel.findOne({
      _id: new ObjectId(TMData.TM_sub_category_id),
    })
    // Check TM capacity greater than pickup quantity.
    if (!isNullOrUndefined(doc)) {
      if (doc.load_capacity < trackData.pickup_quantity) {
        return Promise.reject(
          new InvalidInput(`TM capacity is less than pickup capacity.`, 400)
        )
      }
      trackData.TM_sub_category_name = doc.sub_category_name
    }

    let driver1Data = await DriverInfoModel.findOne({
      _id: new ObjectId(TMData.driver1_id),
    })

    if (isNullOrUndefined(driver1Data)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any data for this Driver.`,
          400
        )
      )
    }
    trackData.TM_driver1_name = driver1Data.driver_name
    trackData.TM_driver1_mobile_number = driver1Data.driver_mobile_number
    let driver2Data = await DriverInfoModel.findOne({
      _id: new ObjectId(TMData.driver2_id),
    })
    if (isNullOrUndefined(driver2Data)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any data for this Driver.`,
          400
        )
      )
    }
    trackData.TM_driver2_name = driver2Data.driver_name
    trackData.TM_driver2_mobile_number = driver2Data.driver_mobile_number
    let order_item_quantity = OrderItemData.quantity
    let remaining_quantity = OrderItemData.remaining_quantity
    let pickup_quantity = trackData.pickup_quantity

    if (
      order_item_quantity >= pickup_quantity &&
      remaining_quantity >= pickup_quantity
    ) {
      await VendorOrderModel.updateOne(
        { _id: order_id, buyer_order_item_id: order_item_id },
        {
          $set: {
            order_status: TrackStatus.TM_ASSIGNED,
          },
        }
      )

      trackData.buyer_order_id = VendorOrderData.buyer_order_id
      trackData.order_item_id = order_item_id
      trackData.logistics_order_id = order_id
      trackData.remaining_quantity =
        OrderItemData.temp_quantity - trackData.pickup_quantity
      trackData.event_status = TrackStatus.TM_ASSIGNED
      trackData.vendor_user_id = vendor_id
      trackData.vendor_order_id = order_id
      trackData.sub_vendor_id = VendorOrderData.sub_vendor_id
      trackData.master_vendor_id = VendorOrderData.master_vendor_id
      await new OrderTrackModel(trackData).save()

      await OrderItemModel.updateOne(
        { _id: order_item_id },
        {
          $set: {
            item_status: OrderItemStatus.TM_ASSIGNED,
            temp_quantity:
              OrderItemData.temp_quantity - trackData.pickup_quantity,
            assigned_at: trackData.assigned_at,
          },
        }
      )
      const OrderItemPartData = await OrderItemPartModel.findById(
        trackData.order_item_part_id
      )

      if (isNullOrUndefined(OrderItemPartData)) {
        return Promise.reject(
          new InvalidInput(`No Match order part data found.`, 400)
        )
      }

    
      if (!isNullOrUndefined(OrderItemPartData)) {
        await OrderItemPartModel.updateOne(
          { _id: new ObjectId(trackData.order_item_part_id) },
          {
            $set: {
              temp_quantity:
                OrderItemPartData.temp_quantity - trackData.pickup_quantity,
            },
          }
        )
      }

      let buyerUserData = await BuyerUserModel.findOne({
        _id: new ObjectId(VendorOrderData.buyer_id),
      })
  
      let site_name: any
      let person_name: any
      let site_mobile_number: any
      let site_email: any
      let client_name: any
      if (!isNullOrUndefined(buyerUserData)) {
        if (buyerUserData.account_type === "Individual") {
          client_name = buyerUserData.full_name
        } else {
          client_name = buyerUserData.company_name
        }
      }

      let vendorUserData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor_id),
      })

      let vendor_mobile_number: any
      let vendor_company_name: any
      let vendor_email: any
      if (!isNullOrUndefined(vendorUserData)) {
        vendor_mobile_number = vendorUserData.mobile_number
        vendor_company_name = vendorUserData.company_name
        vendor_email = vendorUserData.email
      }

      let buyerOrderDoc = await OrderModel.findOne({
        _id: new ObjectId(VendorOrderData.buyer_order_id),
      })
      if (!isNullOrUndefined(buyerOrderDoc)) {
        let siteData = await SiteModel.findOne({
          _id: new ObjectId(buyerOrderDoc.site_id),
        })
        if (!isNullOrUndefined(siteData)) {
          site_name = siteData.site_name
          person_name = siteData.person_name
          site_mobile_number = siteData.mobile_number
          site_email = siteData.email
        }
      }

      // // send sms/email to client for TM assigned.
      // let message = `Hi ${client_name}, kindly note that the RMC supplier ${vendor_name} has assigned a TM for your order qty ${trackData.pickup_quantity} (${VendorOrderData.buyer_order_display_id}) for your site ${site_name}. You can track your order online.`
      // // TM assigned.
      // let templateId = "1707163577232974144"
      // await this.testService.sendSMS(mobile_number, message, templateId)

      // send sms/email to client for TM assigned.
      let site_message = `Hi ${person_name}, kindly note that the RMC supplier ${vendor_company_name} has assigned a TM for ${trackData.pickup_quantity} Cum qty of your product id ${VendorOrderData.buyer_order_display_item_id} having an Order ID ${VendorOrderData.buyer_order_display_id} for your site ${site_name}. You can track your order online. - conmix`
      // TM assigned.
      let site_templateId = "1707163732924552445"
      await this.testService.sendSMS(
        site_mobile_number,
        site_message,
        site_templateId
      )

        let tm_assigned_html = client_mail_template.tm_assigned
        tm_assigned_html = tm_assigned_html.replace("{{person_name}}", person_name)
        tm_assigned_html = tm_assigned_html.replace("{{vendor_company_name}}", vendor_company_name)
        tm_assigned_html = tm_assigned_html.replace("{{trackData.pickup_quantity}}", trackData.pickup_quantity)
        tm_assigned_html = tm_assigned_html.replace("{{VendorOrderData.buyer_order_display_item_id}}", VendorOrderData.buyer_order_display_item_id)
        tm_assigned_html = tm_assigned_html.replace("{{VendorOrderData.buyer_order_display_id}}", VendorOrderData.buyer_order_display_id)
        tm_assigned_html = tm_assigned_html.replace("{{site_name}}", site_name)
        var data11 = {
          from: "no-reply@conmate.in",
          to: site_email,
          subject: "Conmix - Transit Mixer Assign",
          html: tm_assigned_html,
        }
        await this.mailgunService.sendEmail(data11)

      // Send notofication to client.
      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: VendorOrderData.buyer_id,
        notification_type: ClientNotificationType.TMAssigned,
        order_id: VendorOrderData.buyer_order_id,
        order_item_id: OrderItemData.display_item_id,
        vendor_id,
      })

      let vendor_message = `Hi ${vendor_company_name}, you have assigned a TM for ${trackData.pickup_quantity} Cum qty for Product Id ${VendorOrderData.buyer_order_display_item_id} having an Order ID ${VendorOrderData._id} for your Client ${client_name} to be delivered to its ${site_name} site. - conmix`
      let vendor_templateId = "1707163732950059644"
      await this.testService.sendSMS(
        vendor_mobile_number,
        vendor_message,
        vendor_templateId
      )

      let tm_assigned_vendor_html = vendor_mail_template.tm_assigned
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{vendor_company_name}}", vendor_company_name)
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{trackData.pickup_quantity}}", trackData.pickup_quantity)
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{VendorOrderData.buyer_order_display_item_id}}", VendorOrderData.buyer_order_display_item_id)
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{VendorOrderData._id}}", VendorOrderData._id)
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{client_name}}", client_name)
      tm_assigned_vendor_html = tm_assigned_vendor_html.replace("{{site_name}}", site_name)
      var data_vendor = {
        from: "no-reply@conmate.in",
        to: vendor_email,
        subject: "Conmix - Transit Mixer Assign",
        html: tm_assigned_vendor_html,
      }
      await this.mailgunService.sendEmail(data_vendor)



      // Send notofication to Supplier.
      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: VendorOrderData.master_vendor_id,
        notification_type: VendorNotificationType.TMAssigned,
        vendor_order_id: VendorOrderData._id,
        vendor_id,
        order_id: VendorOrderData.buyer_order_id,
        order_item_id: OrderItemData.display_item_id,
        client_id: VendorOrderData.buyer_id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: VendorOrderData.sub_vendor_id,
        notification_type: VendorNotificationType.TMAssigned,
        vendor_order_id: VendorOrderData._id,
        vendor_id,
        order_id: VendorOrderData.buyer_order_id,
        order_item_id: OrderItemData.display_item_id,
        client_id: VendorOrderData.buyer_id,
      })


         // Check Admin Type.
         const adminQuery: { [k: string]: any } = {}
         adminQuery.$or = [
           {
             admin_type : AdminRoles.admin_manager
           },
           {
             admin_type : AdminRoles.admin_sales
           }
         ]
         const adminUserDocs = await AdminUserModel.find(adminQuery)
         if (adminUserDocs.length < 0) {
           return Promise.reject(
             new InvalidInput(`No Admin data were found `, 400)
           )
         }
          
         for(let i = 0 ; i< adminUserDocs.length; i ++){  
        // Send notification to Admin.
        await this.notUtil.addNotificationForAdmin({
        to_user_type: UserType.ADMIN,
        to_user_id: adminUserDocs[i]._id,
        notification_type: AdminNotificationType.TMAssigned,
        vendor_order_id: VendorOrderData._id,
        vendor_id,
        order_id: VendorOrderData.buyer_order_id,
        order_item_id: OrderItemData.display_item_id,
        client_id: VendorOrderData.buyer_id,
      })
         }  
    
 
    } else {
      return Promise.reject(
        new InvalidInput(
          `No Sufficient quantity for assigned truck to this item`,
          400
        )
      )
    }

    return Promise.resolve()
  }

  async addCPtoOrderTrack(
    vendor_id: string,
    order_id: string,
    order_item_id: string,
    trackData: IAddCPtoOrderTrack
  ) {

    const pumpdata = await ConcretePumpModel.findOne({
      _id: new ObjectId(trackData.CP_id),
    })

    // Check TM belongs to Vendor or not.
    if (isNullOrUndefined(pumpdata)) {
      return Promise.reject(
        new InvalidInput(`CP not associated with this vendor.`, 400)
      )
    }

    const vorderData = await VendorOrderModel.findOne({
      _id: order_id,
    })

    // Check vendor order exists or not.
    if (isNullOrUndefined(vorderData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find Vendor order for the entered order Id.`,
          400
        )
      )
    }

    if (
      pumpdata.sub_vendor_id.equals(new ObjectId(vorderData.sub_vendor_id)) ===
      false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find CP belongs to this vendor.`,
          400
        )
      )
    }

    if (trackData.is_already_delivered) {
      const track_query: { [k: string]: any } = {
        CP_id: new ObjectId(trackData.CP_id),
        vendor_order_id: order_id,
        order_item_id: new ObjectId(order_item_id),
        event_status: TrackStatus.DELIVERED,
      }

      const track_aggregateArr = [
        { $match: track_query },
        {
          $project: {
            _id: true,
            buyer_order_id: true,
            order_item_id: true,
            CP_id: true,
            vendor_order_id: true,
            vendor_user_id: true,
            reasonForDelay: true,
            delayTime: true,
            created_at: true,
            assigned_at: true,
            pickedup_at: true,
            cancelled_at: true,
            delivered_at: true,
            delayed_at: true,
            challan_image_url: true,
            supplier_pickup_signature: true,
            driver_pickup_signature: true,
            buyer_drop_signature: true,
            driver_drop_signature: true,
            qube_test_report_7days: true,
            qube_test_report_28days: true,
            event_status: true,
            start_time: true,
            end_time: true,
            order_item_part_id: true,
            sub_vendor_id: true,
            master_vendor_id: true,
          },
        },
      ]
      const track_data = await CPOrderTrackModel.aggregate(track_aggregateArr)

      if (track_data.length > 0) {
        trackData.buyer_order_id = track_data[0].buyer_order_id
        trackData.order_item_id = track_data[0].order_item_id
        trackData.CP_id = track_data[0].CP_id
        trackData.vendor_order_id = track_data[0].vendor_order_id
        trackData.vendor_user_id = track_data[0].vendor_user_id
        trackData.reasonForDelay = track_data[0].reasonForDelay
        trackData.delayTime = track_data[0].delayTime
        trackData.assigned_at = track_data[0].assigned_at
        trackData.pickedup_at = track_data[0].pickedup_at
        trackData.cancelled_at = track_data[0].cancelled_at
        trackData.delivered_at = track_data[0].delivered_at
        trackData.delayed_at = track_data[0].delayed_at
        trackData.challan_image_url = track_data[0].challan_image_url
        trackData.supplier_pickup_signature =
          track_data[0].supplier_pickup_signature
        trackData.driver_pickup_signature =
          track_data[0].driver_pickup_signature
        trackData.buyer_drop_signature = track_data[0].buyer_drop_signature
        trackData.driver_drop_signature = track_data[0].driver_drop_signature
        trackData.qube_test_report_7days = trackData[0].qube_test_report_7days
        trackData.qube_test_report_28days = trackData[0].qube_test_report_28days
        trackData.event_status = track_data[0].event_status
        trackData.start_time = track_data[0].start_time
        trackData.end_time = track_data[0].end_time
        trackData.order_item_part_id = track_data[0].order_item_part_id
      } else {
        return Promise.reject(
          new InvalidInput(
            `This Concrete pump has not been delivered yet.`,
            400
          )
        )
      }
    }

    const query: { [k: string]: any } = {
      CP_id: new ObjectId(trackData.CP_id),
      assigned_at: trackData.assigned_at,
      vendor_order_id: { $ne: order_id },
      order_item_id: { $ne: order_item_id },
    }

    query.$or = [
      {
        event_status: TrackStatus.CP_ASSIGNED,
      },
      {
        event_status: TrackStatus.PICKUP,
      },
      {
        event_status: TrackStatus.DELAYED,
      },
    ]

    const aggregateArr = [
      { $match: query },
      {
        $project: {
          _id: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          vendor_order_id: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const data = await CPOrderTrackModel.aggregate(aggregateArr)
    if (data.length > 0) {
      return Promise.reject(
        new InvalidInput(
          `This CP is engaged for order with id #${data[0].vendor_order_id}. current status of order is ${data[0].event_status}.`,
          400
        )
      )
    }

    const VendorOrderData = await VendorOrderModel.findOne({
      _id: order_id,
      buyer_order_item_id: order_item_id,
    })

    // Check vendor order exists or not.
    if (isNullOrUndefined(VendorOrderData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry we could not find Vendor order for the entered order Id.`,
          400
        )
      )
    }

    const CPData = await ConcretePumpModel.findOne({
      _id: new ObjectId(trackData.CP_id),
    })

    // Check TM belongs to Vendor or not.
    if (isNullOrUndefined(CPData)) {
      return Promise.reject(
        new InvalidInput(`CP not associated with this vendor.`, 400)
      )
    }

    const CPCatData = await ConcretePumpCategoryModel.findOne({
      _id: new ObjectId(CPData.concrete_pump_category_id),
    })

    // Check TM belongs to Vendor or not.
    if (isNullOrUndefined(CPCatData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any data related to CP category`,
          400
        )
      )
    }

    const CPOperatorData = await OperatorInfoModel.findOne({
      _id: new ObjectId(CPData.operator_id),
    })

    // Check TM belongs to VeCP_operator_namendor or not.
    if (isNullOrUndefined(CPOperatorData)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any data related to CP Operator `,
          400
        )
      )
    }



    trackData.CP_serial_number = CPData.serial_number
    trackData.CP_category_name = CPCatData.category_name
    trackData.CP_operator_mobile_number = CPOperatorData.operator_mobile_number
    trackData.CP_operator_name = CPOperatorData.operator_name

    if (trackData.is_already_delivered !== true) {
      trackData.buyer_order_id = VendorOrderData.buyer_order_id
      trackData.order_item_id = order_item_id
      trackData.logistics_order_id = order_id
      trackData.event_status = TrackStatus.CP_ASSIGNED
      trackData.vendor_user_id = vendor_id
      trackData.vendor_order_id = order_id
      trackData.CP_serial_number = CPData.serial_number
      trackData.CP_category_name = CPCatData.category_name
      trackData.CP_operator_mobile_number =
        CPOperatorData.operator_mobile_number
      trackData.CP_operator_name = CPOperatorData.operator_name
    }
    trackData.sub_vendor_id = VendorOrderData.sub_vendor_id
    trackData.master_vendor_id = VendorOrderData.master_vendor_id
    await new CPOrderTrackModel(trackData).save()

    let buyerUserData = await BuyerUserModel.findOne({
      _id: new ObjectId(VendorOrderData.buyer_id),
    })
  

    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let site_email: any
    let client_name: any
    if (!isNullOrUndefined(buyerUserData)) {
      if (buyerUserData.account_type === "Individual") {
        client_name = buyerUserData.full_name
      } else {
        client_name = buyerUserData.company_name
      }
    }

    let subvendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(VendorOrderData.sub_vendor_id),
    })

    let s_vendor_mobile_number: any
    let s_vendor_company_name: any
    let s_vendor_email: any
    if (!isNullOrUndefined(subvendorUserData)) {
      s_vendor_mobile_number = subvendorUserData.mobile_number
      s_vendor_company_name = subvendorUserData.company_name
      s_vendor_email = subvendorUserData.email
    }

    let mastervendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(VendorOrderData.master_vendor_id)
    })

    let m_vendor_mobile_number: any
    let m_vendor_company_name: any
    let m_vendor_email: any
    if (!isNullOrUndefined(mastervendorUserData)) {
      m_vendor_mobile_number = mastervendorUserData.mobile_number
      m_vendor_company_name = mastervendorUserData.company_name
      m_vendor_email = mastervendorUserData.email
    }

    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(VendorOrderData.buyer_order_id),
    })
    if (!isNullOrUndefined(buyerOrderDoc)) {
      let siteData = await SiteModel.findOne({
        _id: new ObjectId(buyerOrderDoc.site_id),
      })
      if (!isNullOrUndefined(siteData)) {
        site_name = siteData.site_name
        site_person_name = siteData.person_name
        site_mobile_number = siteData.mobile_number
        site_email = siteData.email
      }
    }

    // send sms/email to client for CP assigned.
    // let message = `Hi ${client_name}, kindly note that the RMC supplier ${vendor_name} has assigned a Concrete Pump (CP) for your Order with an ${VendorOrderData.buyer_order_display_id} for your site ${site_name}`
    // // CP assigned.
    // let templateId = "1707163577243467240"
    // await this.testService.sendSMS(mobile_number, message, templateId)

    // let message = `Hi ${site_person_name}, kindly note that the RMC supplier ${vendor_name} has assigned a CP for ${trackData.pickup_quantity} Cum qty of your product id ${VendorOrderData.buyer_order_display_item_id} having an Order ID ${VendorOrderData.buyer_order_display_id} for your site ${site_name}. You can track your order online. - conmix`
    let message = `Hi ${site_person_name}, CP has been assigned to your product id ${VendorOrderData.buyer_order_display_item_id} having an order id ${VendorOrderData.buyer_order_display_id} by RMC Supplier ${s_vendor_company_name}. -conmix`
    // CP assigned.
    let templateId = "1707163775564509794"
    await this.testService.sendSMS(site_mobile_number, message, templateId)

    let cp_assigned_html = client_mail_template.cp_assigned
    cp_assigned_html = cp_assigned_html.replace("{{site_person_name}}", site_person_name)
    cp_assigned_html = cp_assigned_html.replace("{{vendor_company_name}}", s_vendor_company_name)
    cp_assigned_html = cp_assigned_html.replace("{{trackData.pickup_quantity}}", trackData.pickup_quantity)
    cp_assigned_html = cp_assigned_html.replace("{{VendorOrderData.buyer_order_display_item_id}}", VendorOrderData.buyer_order_display_item_id)
    cp_assigned_html = cp_assigned_html.replace("{{VendorOrderData.buyer_order_display_id}}", VendorOrderData.buyer_order_display_id)
    cp_assigned_html = cp_assigned_html.replace("{{site_name}}", site_name)
    var data11 = {
      from: "no-reply@conmate.in",
      to: site_email,
      subject: "Conmix - Concrete Pump Assign",
      html: cp_assigned_html,
    }
    await this.mailgunService.sendEmail(data11)



    //let vendor_message = `Hi ${vendor_name}, you have assigned a CP for 'Product Qty Value' Cum qty for Product Id ${VendorOrderData.buyer_order_display_item_id} having an Order ID ${VendorOrderData.buyer_order_display_id} for your Client ${client_name} to be delivered to its ${site_name} site.`
    let m_vendor_message = `Hi ${m_vendor_company_name}, you have assigned a CP for the product id ${VendorOrderData.buyer_order_display_item_id} having an order id ${VendorOrderData._id} for your client ${client_name} -conmix`
    // CP assigned.
    let m_vendor_templateId = "1707163792753849815"
    await this.testService.sendSMS(
      m_vendor_mobile_number,
      m_vendor_message,
      m_vendor_templateId
    )

    let s_vendor_message = `Hi ${s_vendor_company_name}, you have assigned a CP for the product id ${VendorOrderData.buyer_order_display_item_id} having an order id ${VendorOrderData._id} for your client ${client_name} -conmix`
    // CP assigned.
    let s_vendor_templateId = "1707163792753849815"
    await this.testService.sendSMS(
      s_vendor_mobile_number,
      s_vendor_message,
      s_vendor_templateId
    )

    let cp_assigned_vendor_html = vendor_mail_template.cp_assigned
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{vendor_company_name}}", m_vendor_company_name)
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{trackData.pickup_quantity}}", trackData.pickup_quantity)
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{VendorOrderData.buyer_order_display_item_id}}", VendorOrderData.buyer_order_display_item_id)
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{VendorOrderData._id}}", VendorOrderData._id)
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{client_name}}", client_name)
    cp_assigned_vendor_html = cp_assigned_vendor_html.replace("{{site_name}}", site_name)
    var data_vendor = {
      from: "no-reply@conmate.in",
      to: m_vendor_email,
      subject: "Conmix - Concrete Pump Assign",
      html: cp_assigned_vendor_html,
    }
    await this.mailgunService.sendEmail(data_vendor)

    let cp_assigned_sub_vendor_html = vendor_mail_template.cp_assigned
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{vendor_company_name}}", s_vendor_company_name)
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{trackData.pickup_quantity}}", trackData.pickup_quantity)
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{VendorOrderData.buyer_order_display_item_id}}", VendorOrderData.buyer_order_display_item_id)
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{VendorOrderData._id}}", VendorOrderData._id)
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{client_name}}", client_name)
    cp_assigned_sub_vendor_html = cp_assigned_sub_vendor_html.replace("{{site_name}}", site_name)
    var data_vendor = {
      from: "no-reply@conmate.in",
      to: s_vendor_email,
      subject: "Conmix - Concrete Pump Assign",
      html: cp_assigned_sub_vendor_html,
    }
    await this.mailgunService.sendEmail(data_vendor)

    // Send notofication to client.
    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: VendorOrderData.buyer_id,
      notification_type: ClientNotificationType.CPAssigned,
      order_id: VendorOrderData.buyer_order_id,
      order_item_id: VendorOrderData.buyer_order_display_item_id,
      vendor_id,
    })

    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: VendorOrderData.master_vendor_id,
      notification_type: VendorNotificationType.CPAssigned,
      vendor_order_id: VendorOrderData._id,
      order_id: VendorOrderData.buyer_order_id,
      order_item_id: VendorOrderData.buyer_order_display_item_id,
      vendor_id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: VendorOrderData.sub_vendor_id,
      notification_type: VendorNotificationType.CPAssigned,
      vendor_order_id: VendorOrderData._id,
      order_id: VendorOrderData.buyer_order_id,
      order_item_id: VendorOrderData.buyer_order_display_item_id,
      vendor_id,
      client_id: VendorOrderData.buyer_id,
    })

         // Check Admin Type.
         const adminQuery: { [k: string]: any } = {}
         adminQuery.$or = [
           {
             admin_type : AdminRoles.admin_manager
           },
           {
             admin_type : AdminRoles.admin_sales
           }
         ]
         const adminUserDocs = await AdminUserModel.find(adminQuery)
         if (adminUserDocs.length < 0) {
           return Promise.reject(
             new InvalidInput(`No Admin data were found `, 400)
           )
         }

    for(let i = 0 ; i< adminUserDocs.length; i ++){  
    // Send notification to Admin.
    await this.notUtil.addNotificationForAdmin({
      to_user_type: UserType.ADMIN,
      to_user_id: adminUserDocs[i]._id,
      notification_type: AdminNotificationType.CPAssigned,
      order_id: VendorOrderData.buyer_order_id,
      order_item_id: VendorOrderData.buyer_order_display_item_id,
      vendor_id,
      client_id: VendorOrderData.buyer_id,
    })
         }     


    return Promise.resolve()
  }

  async getOrderByTMId(TMId: string, vendor_id: string, current_date: Date) {
    console.log("vendorid", vendor_id)

    const query: { [k: string]: any } = {
      //vendor_user_id: new ObjectId(vendor_id),
      TM_id: new ObjectId(TMId),
      // assigned_at: current_date,
    }
    query.$or = [
      {
        event_status: TrackStatus.TM_ASSIGNED,
      },
      {
        event_status: TrackStatus.PICKUP,
      },
      {
        event_status: TrackStatus.DELAYED,
      },
    ]

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TM_info",
        },
      },
      {
        $unwind: {
          path: "$TM_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "driver_info",
          localField: "TM_info.driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_info.TM_category_id",
          foreignField: "_id",
          as: "TM_category_info",
        },
      },
      {
        $unwind: {
          path: "$TM_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_info.TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category_info",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendor_order_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_order_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "vendor_order_info.buyer_id",
          foreignField: "_id",
          as: "buyer_info",
        },
      },
      {
        $unwind: {
          path: "$buyer_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "vendor_order_info.design_mix_id",
          foreignField: "_id",
          as: "design_mix_variant_info",
        },
      },
      {
        $unwind: {
          path: "$design_mix_variant_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendor_order_info.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order_info",
        },
      },
      {
        $unwind: {
          path: "$order_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "order_item_info",
        },
      },
      {
        $unwind: {
          path: "$order_item_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order_info.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: true,
          remaining_quantity: true,
          pickup_quantity: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          "vendor_order_info.quantity": true,
          "vendor_order_info._id": true,
          "vendor_order_info.created_at": true,
          "vendor_order_info.address_id": true,
          "TM_info._id": true,
          "TM_info.TM_category_id": true,
          "TM_info.TM_sub_category_id": true,
          "TM_info.TM_rc_number": true,
          "TM_info.TM_image_url": true,
          "TM_category_info._id": true,
          "TM_category_info.category_name": true,
          "TM_sub_category_info._id": true,
          "TM_sub_category_info.sub_category_name": true,
          "TM_sub_category_info.load_capacity": true,
          "buyer_info._id": true,
          "buyer_info.full_name": true,
          "buyer_info.mobile_number": true,
          "buyer_info.company_name": true,
          "supplier_info._id": true,
          "supplier_info.full_name": true,
          "supplier_info.mobile_number": true,
          "supplier_info.company_name": true,
          "pickup_address_info.pickup_location":
            "$pickup_address_info.location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "delivery_address_info.pincode": true,
          "order_info._id": true,
          "order_item_info._id": true,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const vendorOrder = await OrderTrackModel.aggregate(aggregateArr)

    console.log("vendorOrder", vendorOrder)
    return Promise.resolve(vendorOrder)
  }

  async getOrderByCPId(CPId: string, vendor_id: string, current_date: Date) {
    const query: { [k: string]: any } = {
      // vendor_user_id: new ObjectId(vendor_id),
      CP_id: new ObjectId(CPId),
      //assigned_at: current_date,
    }
    query.$or = [
      {
        event_status: TrackStatus.CP_ASSIGNED,
      },
      {
        event_status: TrackStatus.PICKUP,
      },
      {
        event_status: TrackStatus.DELAYED,
      },
    ]

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "concrete_pump",
          localField: "CP_id",
          foreignField: "_id",
          as: "CP_info",
        },
      },
      {
        $unwind: {
          path: "$CP_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "CP_info.concrete_pump_category_id",
          foreignField: "_id",
          as: "CP_category_info",
        },
      },
      {
        $unwind: {
          path: "$CP_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendor_order_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_order_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "vendor_order_info.buyer_id",
          foreignField: "_id",
          as: "buyer_info",
        },
      },
      {
        $unwind: {
          path: "$buyer_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "vendor_order_info.design_mix_id",
          foreignField: "_id",
          as: "design_mix_variant_info",
        },
      },
      {
        $unwind: {
          path: "$design_mix_variant_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendor_order_info.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order_info",
        },
      },
      {
        $unwind: {
          path: "$order_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "order_item_info",
        },
      },
      {
        $unwind: {
          path: "$order_item_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order_info.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "operator_info",
          localField: "CP_info.operator_id",
          foreignField: "_id",
          as: "operator_info",
        },
      },
      {
        $unwind: { path: "$operator_info", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: true,
          remaining_quantity: true,
          pickup_quantity: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          "CP_info._id": true,
          "CP_info.concrete_pump_category_id": true,
          "CP_info.company_name": true,
          "CP_info.concrete_pump_model": true,
          "CP_info.manufacture_year": true,
          "CP_info.pipe_connection": true,
          "CP_info.concrete_pump_capacity": true,
          "CP_info.is_Operator": true,
          "CP_info.operator_id": true,
          "CP_info.is_helper": true,
          "CP_info.transportation_charge": true,
          "CP_info.concrete_pump_price": true,
          "CP_info.concrete_pump_image_url": true,
          "CP_info.serial_number": true,
          "CP_category_info.category_name": true,
          "CP_category_info.is_active": true,
          "vendor_order_info.quantity": true,
          "vendor_order_info._id": true,
          "vendor_order_info.created_at": true,
          "vendor_order_info.address_id": true,
          "buyer_info._id": true,
          "buyer_info.full_name": true,
          "buyer_info.mobile_number": true,
          "buyer_info.company_name": true,
          "supplier_info._id": true,
          "supplier_info.full_name": true,
          "supplier_info.mobile_number": true,
          "supplier_info.company_name": true,
          "pickup_address_info.pickup_location":
            "$pickup_address_info.location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "delivery_address_info.pincode": true,
          "order_info._id": true,
          "order_item_info._id": true,
          "operator_info.operator_name": 1,
          "operator_info.operator_mobile_number": 1,
          CP_serial_number: true,
          CP_category_name: true,
          CP_operator_name: true,
          CP_operator_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const vendorOrder = await CPOrderTrackModel.aggregate(aggregateArr)

    console.log("vendorOrder", vendorOrder)
    return Promise.resolve(vendorOrder)
  }

  async updateOrder(
    vendor_id: string,
    track_id: string,
    order_id: string,
    TM_id: string,
    quantityData: IUpdateOrder
  ) {

    // Find order in tracking
    const orderTrackDoc = await OrderTrackModel.findById(track_id)
    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }

    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    if (!isNullOrUndefined(quantityData.order_status)) {
      updt.$set.event_status = quantityData.order_status
    }

    if (!isNullOrUndefined(quantityData.royality_quantity)) {
      updt.$set.royality_quantity = quantityData.royality_quantity
    }

    if (!isNullOrUndefined(quantityData.reject_reason)) {
      updt.$set.reasonForReject = quantityData.reject_reason
    }

    if (Object.keys(updt.$set).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await OrderTrackModel.updateOne({ _id: new ObjectId(track_id) }, updt)

    if (quantityData.order_status === "REJECTED") {
      const data = await OrderItemModel.find({
        order_id: orderTrackDoc.buyer_order_id,
        _id: orderTrackDoc.order_item_id,
        item_status: { $ne: OrderItemStatus.REJECTED },
      })

      const OrderItemData = await OrderItemModel.findOne({
        _id: new ObjectId(orderTrackDoc.order_item_id),
      })

      if (isNullOrUndefined(OrderItemData)) {
        return Promise.reject(
          new InvalidInput(`No Order item doc were found `, 400)
        )
      }

      let buyerUserDoc = await BuyerUserModel.findOne({
        _id: new ObjectId(OrderItemData.buyer_id),
      })

      let client_name: any
      let mobile_number: any
      let email: any
      if (!isNullOrUndefined(buyerUserDoc)) {
        mobile_number = buyerUserDoc.mobile_number
        email = buyerUserDoc.email
        if (buyerUserDoc.account_type === "Individual") {
          client_name = buyerUserDoc.full_name
        } else {
          client_name = buyerUserDoc.company_name
        }
      }

      let buyerOrderDoc = await OrderModel.findOne({
        _id: new ObjectId(OrderItemData.order_id),
      })
      let display_id: any
      let site_id: any
      if (!isNullOrUndefined(buyerOrderDoc)) {
        display_id = buyerOrderDoc.display_id
        site_id = buyerOrderDoc.site_id
      }

      let vendorOrderDoc = await VendorOrderModel.findOne({
        buyer_order_item_id: orderTrackDoc.order_item_id,
      })

      if (isNullOrUndefined(vendorOrderDoc)) {
        return Promise.reject(
          new InvalidInput(`No Vendor Order Doc were Found.`, 400)
        )
      }

      // const adminUserDoc = await AdminUserModel.findOne({
      //   admin_type: "superAdmin",
      // })
  
      // if (isNullOrUndefined(adminUserDoc)) {
      //   return Promise.reject(
      //     new InvalidInput(`No Admin data were found `, 400)
      //   )
      // }

      let vendor_order_id = vendorOrderDoc._id
      let site_Info = await SiteModel.findOne({
        _id: new ObjectId(site_id),
      })
      let site_name: any
      let site_mobile_number: any
      let site_person_name: any
      let site_email: any
      if (!isNullOrUndefined(site_Info)) {
        site_name = site_Info.site_name
        site_mobile_number = site_Info.mobile_number
        site_person_name = site_Info.person_name
        site_email = site_Info.email
      }


      let m_vendor_mobile_number: any
      let master_vendor_company_name: any
      let m_vendor_email: any
      let vendorUserData = await VendorUserModel.findOne({
        _id: new ObjectId(vendorOrderDoc.master_vendor_id),
      })
      if (!isNullOrUndefined(vendorUserData)) {
        m_vendor_mobile_number = vendorUserData.mobile_number
        master_vendor_company_name = vendorUserData.company_name
        m_vendor_email = vendorUserData.email
      }

      let s_vendor_mobile_number: any
      let sub_vendor_company_name: any
      let s_vendor_email: any
      let subvendorUserData = await VendorUserModel.findOne({
        _id: new ObjectId(vendorOrderDoc.sub_vendor_id),
      })
      if (!isNullOrUndefined(subvendorUserData)) {
        s_vendor_mobile_number = subvendorUserData.mobile_number
        sub_vendor_company_name = subvendorUserData.company_name
        s_vendor_email = subvendorUserData.email
      }

      if (data.length > 0) {
        let vendorData = await VendorUserModel.findOne({
          _id: new ObjectId(vendor_id),
        })
        if (!isNullOrUndefined(vendorData) && vendorData.rejected_order >= 3) {
          await VendorUserModel.updateOne(
            { _id: new ObjectId(vendor_id) },
            {
              $set: {
                is_blocked: true,
              },
            }
          )

          await AddressModel.updateOne(
            { sub_vendor_id: new ObjectId(vendor_id) },
            {
              $set: {
                is_blocked: true,
              },
            }
          )
          let message = `Hi ${master_vendor_company_name}, this is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.`
          let templateId = "1707163731316863833"
          await this.testService.sendSMS(
            m_vendor_mobile_number,
            message,
            templateId
          )

          let s_message = `Hi ${sub_vendor_company_name}, this is to inform you that your account has been blocked at Conmix as per our Vendor/Supplier/Partner Policy. Kindly refer to the Policy and you may contact Conmix Customer Care.`
          let s_templateId = "1707163731316863833"
          await this.testService.sendSMS(
            s_vendor_mobile_number,
            s_message,
            s_templateId
          )
        } else {
          await VendorUserModel.updateOne(
            { _id: new ObjectId(vendor_id) },
            { $inc: { rejected_order: 1 } }
          )

          let message = `Hi ${master_vendor_company_name}, you have received the warning for not complying to the Conmix Vendor/Supplier/Partner Policy for the Product Id ${OrderItemData.display_item_id} having an Order ID ${vendor_order_id}. Your account will be blocked and upon 3 nos. of such warnings and your dues shall not be paid. - conmix`
          let templateId = "1707163893952687788"
          await this.testService.sendSMS(
            m_vendor_mobile_number,
            message,
            templateId
          )

          let warning_html = vendor_mail_template.warning
          warning_html = warning_html.replace("{{vendor_company_name}}", master_vendor_company_name)
          warning_html = warning_html.replace("{{client_name}}", client_name)
          warning_html = warning_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
          warning_html = warning_html.replace("{{vendor_order_id}}", vendor_order_id)
          var m_data1 = {
            from: "no-reply@conmate.in",
            to: m_vendor_email,
            subject: "Conmix - Warning",
            html: warning_html,
          }
          await this.mailgunService.sendEmail(m_data1)

          let s_message = `Hi ${sub_vendor_company_name}, you have received the warning for not complying to the Conmix Vendor/Supplier/Partner Policy for the Product Id ${OrderItemData.display_item_id} having an Order ID ${display_id}. Your account will be blocked and upon 3 nos. of such warnings and your dues shall not be paid. - conmix`
          let s_templateId = "1707163893952687788"
          await this.testService.sendSMS(
            s_vendor_mobile_number,
            s_message,
            s_templateId
          )

          let warning_s_vendor_html = vendor_mail_template.warning
          warning_s_vendor_html = warning_s_vendor_html.replace("{{vendor_company_name}}", sub_vendor_company_name)
          warning_s_vendor_html = warning_s_vendor_html.replace("{{client_name}}", client_name)
          warning_s_vendor_html = warning_s_vendor_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
          warning_s_vendor_html = warning_s_vendor_html.replace("{{vendor_order_id}}", vendor_order_id)
          var s_data1 = {
            from: "no-reply@conmate.in",
            to: s_vendor_email,
            subject: "Conmix - Warning",
            html: warning_html,
          }
          await this.mailgunService.sendEmail(s_data1)

          // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }
      
      for(let i = 0; i < adminUserDocs.length ; i ++){
        let warning_admin_html = admin_mail_template.warning
        warning_admin_html = warning_admin_html.replace("{{adminUserDoc.full_name}}", adminUserDocs[i].full_name)
        warning_admin_html = warning_admin_html.replace("{{vendor_company_name}}", master_vendor_company_name)
        warning_admin_html = warning_admin_html.replace("{{client_name}}", client_name)
        warning_admin_html = warning_admin_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        warning_admin_html = warning_admin_html.replace("{{vendor_order_id}}", vendor_order_id)
        var admin_data1 = {
          from: "no-reply@conmate.in",
          to: adminUserDocs[i].email,
          subject: "Conmix - Warning",
          html: warning_admin_html,
        }
        await this.mailgunService.sendEmail(admin_data1)
      }
        }

        //update supplier order status also.
        const vendorOrderDoc = await VendorOrderModel.findOneAndUpdate(
          {
            buyer_order_item_id: orderTrackDoc.order_item_id,
          },
          {
            $set: {
              order_status: SupplierOrderStatus.REJECTED,
            },
          }
        )

        if (isNullOrUndefined(vendorOrderDoc)) {
          return Promise.reject(
            new InvalidInput(`No Vendor Order Doc were Found.`, 400)
          )
        }

        await OrderItemModel.updateOne(
          { _id: orderTrackDoc.order_item_id },
          {
            $set: {
              item_status: OrderItemStatus.REJECTED,
              temp_quantity:
                OrderItemData.temp_quantity + orderTrackDoc.pickup_quantity,
            },
          }
        )

     

        let message = `Hi ${client_name}, you have rejected the material delivered to your site ${site_name} for the product id ${OrderItemData.display_item_id} having an Order ID ${display_id}. - conmix`
        let templateId = "1707163732919272821"
        await this.testService.sendSMS(mobile_number, message, templateId)


        let material_rejected_from_buyer_side_html = client_mail_template.material_rejected_buyer_side
        material_rejected_from_buyer_side_html = material_rejected_from_buyer_side_html.replace("{{client_name}}", client_name)
        material_rejected_from_buyer_side_html = material_rejected_from_buyer_side_html.replace("{{site_name}}", site_name)
        material_rejected_from_buyer_side_html = material_rejected_from_buyer_side_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_side_html = material_rejected_from_buyer_side_html.replace("{{display_id}}", display_id)
        material_rejected_from_buyer_side_html = material_rejected_from_buyer_side_html.replace("{{master_vendor_company_name}}", master_vendor_company_name)
        var data1 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Material Rejected",
          html: material_rejected_from_buyer_side_html,
        }
        await this.mailgunService.sendEmail(data1)

        let site_message = `Hi ${site_person_name}, you have rejected the material delivered to your site ${site_name} for the product id ${OrderItemData.display_item_id} having an Order ID ${display_id}. - conmix`
        let site_templateId = "1707163732919272821"
        await this.testService.sendSMS(
          site_mobile_number,
          site_message,
          site_templateId
        )

        let material_rejected_from_buyer_side_site_html = client_mail_template.material_rejected_buyer_side
        material_rejected_from_buyer_side_site_html = material_rejected_from_buyer_side_site_html.replace("{{client_name}}", site_person_name)
        material_rejected_from_buyer_side_site_html = material_rejected_from_buyer_side_site_html.replace("{{site_name}}", site_name)
        material_rejected_from_buyer_side_site_html = material_rejected_from_buyer_side_site_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_side_site_html = material_rejected_from_buyer_side_site_html.replace("{{display_id}}", display_id)
        material_rejected_from_buyer_side_site_html = material_rejected_from_buyer_side_site_html.replace("{{master_vendor_company_name}}", master_vendor_company_name)
        var data1 = {
          from: "no-reply@conmate.in",
          to: site_email,
          subject: "Conmix - Material Rejected",
          html: material_rejected_from_buyer_side_site_html,
        }
        await this.mailgunService.sendEmail(data1)

      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }
      for(let i = 0 ; i< adminUserDocs.length; i ++){  

        let material_rejected_from_buyer_side_admin_html = admin_mail_template.material_rejected_buyer_side_admin
        material_rejected_from_buyer_side_admin_html = material_rejected_from_buyer_side_admin_html.replace("{{client_name}}", client_name)
        material_rejected_from_buyer_side_admin_html = material_rejected_from_buyer_side_admin_html.replace("{{site_name}}", site_name)
        material_rejected_from_buyer_side_admin_html = material_rejected_from_buyer_side_admin_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_side_admin_html = material_rejected_from_buyer_side_admin_html.replace("{{display_id}}", display_id)
        material_rejected_from_buyer_side_admin_html = material_rejected_from_buyer_side_admin_html.replace("{{master_vendor_company_name}}", master_vendor_company_name)
        var data0 = {
          from: "no-reply@conmate.in",
          to: adminUserDocs[i].email,
          subject: "Conmix - Material Rejected",
          html: material_rejected_from_buyer_side_admin_html,
        }
        await this.mailgunService.sendEmail(data0)   
        
        await this.notUtil.addNotificationForAdmin({
          to_user_type: UserType.ADMIN,
          to_user_id: adminUserDocs[i]._id,
          notification_type: AdminNotificationType.orderItemReject,
          order_item_id: OrderItemData.display_item_id,
          order_id: orderTrackDoc.buyer_order_id,
          vendor_id,
          vendor_order_id: order_id,
          client_id: vendorOrderDoc.buyer_id,
        })
      }

        let material_rejected_from_buyer_side_supplier_html = vendor_mail_template.material_rejected_buyer_side
        material_rejected_from_buyer_side_supplier_html = material_rejected_from_buyer_side_supplier_html.replace("{{vendor_name}}", master_vendor_company_name)
        material_rejected_from_buyer_side_supplier_html = material_rejected_from_buyer_side_supplier_html.replace("{{client_name}}", site_person_name)
        material_rejected_from_buyer_side_supplier_html = material_rejected_from_buyer_side_supplier_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_side_supplier_html = material_rejected_from_buyer_side_supplier_html.replace("{{display_id}}", display_id)
       
        var data_master = {
          from: "no-reply@conmate.in",
          to: m_vendor_email,
          subject: "Conmix - Material Rejected",
          html: material_rejected_from_buyer_side_supplier_html,
        }
        await this.mailgunService.sendEmail(data_master)

        let material_rejected_from_buyer_side_sub_vendor_html = vendor_mail_template.material_rejected_buyer_side
        material_rejected_from_buyer_side_sub_vendor_html = material_rejected_from_buyer_side_sub_vendor_html.replace("{{vendor_name}}", sub_vendor_company_name)
        material_rejected_from_buyer_side_sub_vendor_html = material_rejected_from_buyer_side_sub_vendor_html.replace("{{client_name}}", site_person_name)
        material_rejected_from_buyer_side_sub_vendor_html = material_rejected_from_buyer_side_sub_vendor_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_side_sub_vendor_html = material_rejected_from_buyer_side_sub_vendor_html.replace("{{display_id}}", display_id)
       
        var data_master = {
          from: "no-reply@conmate.in",
          to: s_vendor_email,
          subject: "Conmix - Material Rejected",
          html: material_rejected_from_buyer_side_supplier_html,
        }
        await this.mailgunService.sendEmail(data_master)


        
        let refund_message = `Hi ${client_name}, your payment refund for the rejected material supplied by RMC Supplier ${sub_vendor_company_name} for the product id ${OrderItemData.display_item_id} having an Order ID ${display_id} will be processed as per our Material Rejection and Refund Policy. Kindly refer to the policy and contact Conmix Customer Care for more information.`
        let refund_templateId = "1707163730043909945"
        await this.testService.sendSMS(
          mobile_number,
          refund_message,
          refund_templateId
        )

        let material_rejected_from_buyer_refund_html = client_mail_template.material_rejected_buyer_side_payment_refund
        material_rejected_from_buyer_refund_html = material_rejected_from_buyer_refund_html.replace("{{client_name}}", client_name)
        material_rejected_from_buyer_refund_html = material_rejected_from_buyer_refund_html.replace("{{sub_vendor_company_name}}", sub_vendor_company_name)
        material_rejected_from_buyer_refund_html = material_rejected_from_buyer_refund_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_refund_html = material_rejected_from_buyer_refund_html.replace("{{display_id}}", display_id)
        var data1 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Payment Refund",
          html: material_rejected_from_buyer_refund_html,
        }
        await this.mailgunService.sendEmail(data1)

        let refund_site_message = `Hi ${site_person_name}, your payment refund for the rejected material supplied by RMC Supplier ${sub_vendor_company_name} for the product id ${OrderItemData.display_item_id} having an Order ID ${display_id} will be processed as per our Material Rejection and Refund Policy. Kindly refer to the policy and contact Conmix Customer Care for more information.`
        let refund_site_templateId = "1707163730043909945"
        await this.testService.sendSMS(
          site_mobile_number,
          refund_site_message,
          refund_site_templateId
        )

        let material_rejected_from_buyer_site_refund_html = client_mail_template.material_rejected_buyer_side_payment_refund
        material_rejected_from_buyer_site_refund_html = material_rejected_from_buyer_site_refund_html.replace("{{client_name}}", site_person_name)
        material_rejected_from_buyer_site_refund_html = material_rejected_from_buyer_site_refund_html.replace("{{sub_vendor_company_name}}", sub_vendor_company_name)
        material_rejected_from_buyer_site_refund_html = material_rejected_from_buyer_site_refund_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_site_refund_html = material_rejected_from_buyer_site_refund_html.replace("{{display_id}}", display_id)
        var data1 = {
          from: "no-reply@conmate.in",
          to: site_email,
          subject: "Conmix - Payment Refund",
          html: material_rejected_from_buyer_site_refund_html,
        }
        await this.mailgunService.sendEmail(data1)

        

          // Check Admin Type.
      const re_adminQuery: { [k: string]: any } = {}
      re_adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        },
        {
          admin_type : AdminRoles.admin_accounts
        }
      ]
      const re_adminUserDocs = await AdminUserModel.find(re_adminQuery)
      if (re_adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }
      for(let i = 0 ; i< re_adminUserDocs.length; i ++){ 
        let material_rejected_from_buyer_refund_admin_html = admin_mail_template.material_rejected_buyer_side_payment_refund_admin
        material_rejected_from_buyer_refund_admin_html = material_rejected_from_buyer_refund_admin_html.replace("{{client_name}}", client_name)
        material_rejected_from_buyer_refund_admin_html = material_rejected_from_buyer_refund_admin_html.replace("{{sub_vendor_company_name}}", sub_vendor_company_name)
        material_rejected_from_buyer_refund_admin_html = material_rejected_from_buyer_refund_admin_html.replace("{{OrderItemData.display_item_id}}", OrderItemData.display_item_id)
        material_rejected_from_buyer_refund_admin_html = material_rejected_from_buyer_refund_admin_html.replace("{{display_id}}", display_id)
        var refund_data = {
          from: "no-reply@conmate.in",
          to: re_adminUserDocs[i].email,
          subject: "Conmix - Payment Refund",
          html: material_rejected_from_buyer_refund_admin_html,
        }
        await this.mailgunService.sendEmail(refund_data)
      }

        let m_vendor_message = `Hi ${master_vendor_company_name}, your client ${client_name} has rejected the material order for Product Id ${OrderItemData.display_item_id} having an Order ID ${vendorOrderDoc._id}. Kindly do not supply Poor Quality Material. You have been warned for this action from the client. - conmix`
        let m_vendor_templateId = "1707163732952588028"
        await this.testService.sendSMS(
          m_vendor_mobile_number,
          m_vendor_message,
          m_vendor_templateId
        )

        let s_vendor_message = `Hi ${sub_vendor_company_name}, your client ${client_name} has rejected the material order for Product Id ${OrderItemData.display_item_id} having an Order ID ${vendorOrderDoc._id}. Kindly do not supply Poor Quality Material. You have been warned for this action from the client. - conmix`
        let s_vendor_templateId = "1707163732952588028"
        await this.testService.sendSMS(
          s_vendor_mobile_number,
          s_vendor_message,
          s_vendor_templateId
        )

        await this.notUtil.addNotificationForBuyer({
          to_user_type: UserType.BUYER,
          to_user_id: vendorOrderDoc.buyer_id,
          notification_type: ClientNotificationType.orderItemReject,
          order_item_id: OrderItemData.display_item_id,
          order_id: orderTrackDoc.buyer_order_id,
          vendor_id,
          vendor_order_id: order_id,
        })

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: vendorOrderDoc.master_vendor_id,
          notification_type: VendorNotificationType.orderItemReject,
          order_item_id: OrderItemData.display_item_id,
          order_id: orderTrackDoc.buyer_order_id,
          vendor_id,
          vendor_order_id: order_id,
          client_id: vendorOrderDoc.buyer_id,
        })

        await this.notUtil.addNotificationForSupplier({
          to_user_type: UserType.VENDOR,
          to_user_id: vendorOrderDoc.sub_vendor_id,
          notification_type: VendorNotificationType.orderItemReject,
          order_item_id: OrderItemData.display_item_id,
          order_id: orderTrackDoc.buyer_order_id,
          vendor_id,
          vendor_order_id: order_id,
          client_id: vendorOrderDoc.buyer_id,
        })

     
      }

      const doc = await VendorOrderModel.find({
        buyer_order_id: orderTrackDoc.buyer_order_id,
        order_status: { $ne: SupplierOrderStatus.REJECTED },
      })

      const orderTrackData = await OrderTrackModel.find({
        buyer_order_id: orderTrackDoc.buyer_order_id,
        event_status: { $ne: SupplierOrderStatus.REJECTED },
      })
      console.log(doc.length)
      console.log(orderTrackData.length)
      if (doc.length < 1 && orderTrackData.length < 1) {
        //   update buyer order status.
        const orderDoc = await OrderModel.findOneAndUpdate(
          {
            _id: orderTrackDoc.buyer_order_id,
          },
          {
            $set: {
              order_status: SupplierOrderStatus.REJECTED,
            },
          }
        )

        if (isNullOrUndefined(orderDoc)) {
          return Promise.reject(new InvalidInput(`No data was found `, 400))
        }

        await OrderItemModel.updateOne(
          { _id: orderTrackDoc.order_item_id },
          {
            $set: {
              item_status: OrderItemStatus.REJECTED,
            },
          }
        )

        // await this.notUtil.addNotificationForBuyer({
        //   to_user_type: UserType.BUYER,
        //   to_user_id: orderDoc.user_id,
        //   notification_type: ClientNotificationType.orderReject,
        //   order_item_id: orderTrackDoc.order_item_id,
        //   order_id: orderTrackDoc.buyer_order_id,
        //   vendor_id,
        // })
      }
    }
    return Promise.resolve()
  }

  async updateCPOrderTrack(
    vendor_id: string,
    track_id: string,
    order_id: string,
    CP_id: string,
    quantityData: IUpdateCPOrderTrack
  ) {
    const CPorderTrackDoc = await CPOrderTrackModel.findById(track_id)
    if (isNullOrUndefined(CPorderTrackDoc)) {
      return Promise.reject(new InvalidInput(`No CP Found For Tracking.`, 400))
    }

    const updt: { $set: { [k: string]: any } } = {
      $set: {},
    }
    console.log("track_id", track_id)
    console.log("order_id", order_id)
    console.log("vendor_id", vendor_id)
    console.log("CP_id", CP_id)

    if (!isNullOrUndefined(quantityData.order_status)) {
      updt.$set.event_status = quantityData.order_status
    }

    if (!isNullOrUndefined(quantityData.reject_reason)) {
      updt.$set.reasonForReject = quantityData.reject_reason
    }

    console.log("updt", updt)
    if (Object.keys(updt.$set).length < 1) {
      const err1 = new InvalidInput("Kindly enter a field for update")
      err1.httpStatusCode = 400
      return Promise.reject(err1)
    }

    await CPOrderTrackModel.updateOne({ _id: new ObjectId(track_id) }, updt)
    return Promise.resolve()
  }

  async checkCPStatus(order_id: string, vendor_id: string) {
    // Check CP delivered for that order or not.
    let CPOrderTrackDoc = await CPOrderTrackModel.findOne({
      vendor_order_id: order_id,
      // vendor_user_id: vendor_id,
    })

    if (!isNullOrUndefined(CPOrderTrackDoc)) {
      if (
        CPOrderTrackDoc.event_status === TrackStatus.CP_ASSIGNED ||
        CPOrderTrackDoc.event_status === TrackStatus.DELAYED
      ) {
        if (CPOrderTrackDoc.event_status === TrackStatus.CP_ASSIGNED) {
          CPOrderTrackDoc.event_status = "CP Assigned"
        }
        return Promise.reject(
          new InvalidInput(
            `Current Status of CP for this order is "${CPOrderTrackDoc.event_status}". So, TM can not load the material.`,
            400
          )
        )
      }
    } else {
      return Promise.resolve()
    }
  }
  async addOrderAsPickup(
    order_id: string,
    track_id: string,
    TM_id: string,
    vendor_id: string
  ) {

    // update vendor order status.
    let vendorOrderDoc = await VendorOrderModel.findOneAndUpdate(
      { _id: order_id },
      {
        $set: {
          order_status: SupplierOrderStatus.PICKUP,
          pickedup_at: Date.now(),
        },
      }
    )
    if (isNullOrUndefined(vendorOrderDoc)) {
      return Promise.reject(new InvalidInput(`No Vendor Data found.`, 400))
    }

    // update order track status.
    let orderTrackDoc = await OrderTrackModel.findOneAndUpdate(
      {
        _id: new ObjectId(track_id),
        vendor_order_id: order_id,
        TM_id,
      },
      {
        $set: {
          event_status: SupplierOrderStatus.PICKUP,
          pickedup_at: Date.now(),
        },
      }
    )

    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }

    // update order Item status.
    const itemDoc = await OrderItemModel.findOneAndUpdate(
      {
        _id: new ObjectId(orderTrackDoc.order_item_id),
      },
      {
        $set: {
          item_status: OrderItemStatus.PICKUP,
          pickedup_at: Date.now(),
        },
      }
    )

    if (isNullOrUndefined(itemDoc)) {
      return Promise.reject(new InvalidInput(`No Item Data found.`, 400))
    }

    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(orderTrackDoc.buyer_order_id),
    })

    if (isNullOrUndefined(buyerOrderDoc)) {
      return Promise.reject(new InvalidInput(`No Order Data found.`, 400))
    }

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: itemDoc.buyer_id,
      notification_type: ClientNotificationType.orderPickup,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
    })
    
        // Check Admin Type.
        const adminQuery: { [k: string]: any } = {}
        adminQuery.$or = [
          {
            admin_type : AdminRoles.admin_manager
          },
          {
            admin_type : AdminRoles.admin_sales
          }
        ]
        const adminUserDocs = await AdminUserModel.find(adminQuery)
        if (adminUserDocs.length < 0) {
          return Promise.reject(
            new InvalidInput(`No Admin data were found `, 400)
          )
        }  

        //Send notification to Admin.
        for(let i = 0 ; i< adminUserDocs.length; i ++){  
          await this.notUtil.addNotificationForAdmin({
            to_user_type: UserType.ADMIN,
            to_user_id: adminUserDocs[i]._id,
            notification_type: AdminNotificationType.orderPickup,
            order_id: orderTrackDoc.buyer_order_id,
            order_item_id: itemDoc.display_item_id,
            vendor_id,
            site_id: buyerOrderDoc.site_id,
            client_id: itemDoc.buyer_id,
          })
        }    


    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.master_vendor_id,
      notification_type: VendorNotificationType.orderPickup,
      vendor_order_id: vendorOrderDoc._id,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
      site_id: buyerOrderDoc.site_id,
      client_id: itemDoc.buyer_id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.sub_vendor_id,
      notification_type: VendorNotificationType.orderPickup,
      vendor_order_id: vendorOrderDoc._id,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
      site_id: buyerOrderDoc.site_id,
      client_id: itemDoc.buyer_id,
    })


    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let siteInfo = await SiteModel.findOne({
      _id: new ObjectId(buyerOrderDoc.site_id),
    })
    if (!isNullOrUndefined(siteInfo)) {
      site_name = siteInfo.site_name
      site_person_name = siteInfo.person_name
      site_mobile_number = siteInfo.mobile_number
    }

    let vendor_mobile_number: any
    let vendor_company_name: any
    let vendorInfo = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    if (!isNullOrUndefined(vendorInfo)) {
      vendor_mobile_number = vendorInfo.mobile_number
      vendor_company_name = vendorInfo.company_name
    }
  
    let message = `Hi ${site_person_name}, your order for product id ${itemDoc.display_item_id} having an order id ${buyerOrderDoc.display_id} has been picked up by RMC Supplier ${vendor_company_name}. - conmix`
    let templateId = "1707163842845713103"
    await this.testService.sendSMS(site_mobile_number, message, templateId)

    let vendor_message = `Hi ${vendor_company_name}, you have picked up the order with product id ${itemDoc.display_item_id} having an order id ${vendorOrderDoc._id} for your client ${site_person_name}. This Product has to be delivered at their ${site_name} site. - conmix`
    let vendor_templateId = "1707163842854100970"
    await this.testService.sendSMS(
      vendor_mobile_number,
      vendor_message,
      vendor_templateId
    )

    return Promise.resolve()
  }

  async addCPOrderAsPickup(
    order_id: string,
    track_id: string,
    CP_id: string,
    vendor_id: string
  ) {
    let CPorderTrackDoc = await CPOrderTrackModel.findOneAndUpdate(
      {
        _id: new ObjectId(track_id),
        vendor_order_id: order_id,
        //vendor_user_id: new ObjectId(vendor_id),
        CP_id,
      },
      {
        $set: {
          event_status: TrackStatus.PICKUP,
          pickedup_at: Date.now(),
        },
      }
    )

    if (isNullOrUndefined(CPorderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(`No CP Order Found For Tracking.`, 400)
      )
    }

    const itemDoc = await OrderItemModel.findOne({
      _id: new ObjectId(CPorderTrackDoc.order_item_id),
    })

    if (isNullOrUndefined(itemDoc)) {
      return Promise.reject(new InvalidInput(`No Item Data found.`, 400))
    }

    const orderDoc = await OrderModel.findOne({
      _id: itemDoc.order_id,
    })

    if (isNullOrUndefined(orderDoc)) {
      return Promise.reject(new InvalidInput(`No Order Data found.`, 400))
    }

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: itemDoc.buyer_id,
      notification_type: ClientNotificationType.CPPickup,
      order_id: CPorderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
      site_id: orderDoc.site_id,
    })
    let buyer_order_display_id: any
    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(CPorderTrackDoc.buyer_order_id),
    })
    if (!isNullOrUndefined(buyerOrderDoc)) {
      buyer_order_display_id = buyerOrderDoc.display_id
    }
    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let siteInfo = await SiteModel.findOne({
      _id: new ObjectId(orderDoc.site_id),
    })
    if (!isNullOrUndefined(siteInfo)) {
      site_name = siteInfo.site_name
      site_person_name = siteInfo.person_name
      site_mobile_number = siteInfo.mobile_number
    }
    // let vendor_name: any
    let s_vendor_mobile_number: any
    let s_vendor_company_name: any
    let subvendorInfo = await VendorUserModel.findOne({
      _id: new ObjectId(CPorderTrackDoc.sub_vendor_id),
    })
    if (!isNullOrUndefined(subvendorInfo)) {
      // vendor_name = vendorInfo.full_name
      s_vendor_mobile_number = subvendorInfo.s_vendor_mobile_number
      s_vendor_company_name = subvendorInfo.s_vendor_company_name
    }


        // // let vendor_name: any
        // let m_vendor_mobile_number: any
        // let m_vendor_company_name: any
        // let m_vendorInfo = await VendorUserModel.findOne({
        //   _id: new ObjectId(CPorderTrackDoc.master_vendor_id),
        // })
        // if (!isNullOrUndefined(m_vendorInfo)) {
        //   // vendor_name = vendorInfo.full_name
        //   m_vendor_mobile_number = m_vendorInfo.m_vendor_mobile_number
        //   m_vendor_company_name = m_vendorInfo.m_vendor_company_name
        // }

    // let message = `Hi ${site_person_name}, CP has been picked up by RMC supplier ${vendor_name} for delivery at your ${site_name} site for your product id ${itemDoc.display_item_id} having an order id ${buyer_order_display_id}. - conmix.`
    let message = `Hi ${site_person_name}, CP has been picked up by RMC supplier ${s_vendor_company_name} for delivery at your ${site_name} site for your product id ${itemDoc.display_item_id} having an order id ${buyer_order_display_id}. - conmix`
    let templateId = "1707163775571397995"
    await this.testService.sendSMS(site_mobile_number, message, templateId)

    let vendor_message = `Hi ${s_vendor_company_name}, you have picked up the CP to be delivered at ${site_name} site for your client ${site_person_name} for the product id ${itemDoc.display_item_id} having an order id ${CPorderTrackDoc.vendor_order_id}. - conmix.`
    // CP pickup.
    let vendor_templateId = "1707163810403261816"
    await this.testService.sendSMS(
       s_vendor_mobile_number,
      vendor_message,
      vendor_templateId
    )

    const adminUserDoc = await AdminUserModel.findOne({
      admin_type: "superAdmin",
    })

    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    }

    // await this.notUtil.addNotificationForAdmin({
    //   to_user_type: UserType.ADMIN,
    //   to_user_id: adminUserDoc._id,
    //   notification_type: AdminNotificationType.CPPickup,
    //   order_id: CPorderTrackDoc.buyer_order_id,
    //   order_item_id: itemDoc.display_item_id,
    //   vendor_id,
    //   client_id: itemDoc.buyer_id
    // })
    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: CPorderTrackDoc.master_vendor_id,
      notification_type: VendorNotificationType.CPPickup,
      vendor_order_id: CPorderTrackDoc.vendor_order_id,
      order_id: CPorderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
      client_id: itemDoc.buyer_id,
      site_id: orderDoc.site_id,
    })

    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: CPorderTrackDoc.sub_vendor_id,
      notification_type: VendorNotificationType.CPPickup,
      vendor_order_id: CPorderTrackDoc.vendor_order_id,
      order_id: CPorderTrackDoc.buyer_order_id,
      order_item_id: itemDoc.display_item_id,
      vendor_id,
      client_id: itemDoc.buyer_id,
      site_id: orderDoc.site_id,
    })

    // await this.notUtil.addNotificationForAdmin({
    //   to_user_type: UserType.ADMIN,
    //   to_user_id: adminUserDoc._id,
    //   notification_type: AdminNotificationType.orderPickup,
    //   order_id: CPorderTrackDoc.buyer_order_id,
    //   order_item_id: itemDoc.display_item_id,
    //   vendor_id,
    // })
    // // Send notofication to Supplier.
    // await this.notUtil.addNotificationForSupplier({
    //   to_user_type: UserType.VENDOR,
    //   to_user_id: CPorderTrackDoc.vendor_user_id,
    //   notification_type: VendorNotificationType.orderPickup,
    //   vendor_order_id: CPorderTrackDoc.vendor_order_id,
    //   vendor_id,
    // })

    return Promise.resolve()
  }
  async uploadDocument(
    order_id: string,
    track_id: string,
    vendor_id: string,
    TM_id: string,
    fileData: {
      royalty_pass_image: UploadedFile
      way_slip_image: UploadedFile
      challan_image: UploadedFile
      invoice_image: UploadedFile
      supplier_pickup_signature: UploadedFile
      driver_pickup_signature: UploadedFile
      buyer_drop_signature: UploadedFile
      driver_drop_signature: UploadedFile
      qube_test_report_7days: UploadedFile
      qube_test_report_28days: UploadedFile
    }
  ) {
    const editDoc: { [k: string]: any } = {}

    console.log("fileData", fileData)
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.royalty_pass_image) &&
      !isNullOrUndefined(fileData.royalty_pass_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.royalty_pass_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.royalty_pass_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.royalty_pass_image.data,
        fileData.royalty_pass_image.mimetype
      )
      editDoc.royalty_pass_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.way_slip_image) &&
      !isNullOrUndefined(fileData.way_slip_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "ws" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.way_slip_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.way_slip_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.royalty_pass_image.data,
        fileData.royalty_pass_image.mimetype
      )
      editDoc.way_slip_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.challan_image) &&
      !isNullOrUndefined(fileData.challan_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "ch" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.challan_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.challan_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.challan_image.data,
        fileData.challan_image.mimetype
      )
      editDoc.challan_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.supplier_pickup_signature) &&
      !isNullOrUndefined(fileData.supplier_pickup_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "sp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.supplier_pickup_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.supplier_pickup_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.supplier_pickup_signature.data,
        fileData.supplier_pickup_signature.mimetype
      )
      editDoc.supplier_pickup_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_pickup_signature) &&
      !isNullOrUndefined(fileData.driver_pickup_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "dp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_pickup_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_pickup_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_pickup_signature.data,
        fileData.driver_pickup_signature.mimetype
      )
      editDoc.driver_pickup_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.buyer_drop_signature) &&
      !isNullOrUndefined(fileData.buyer_drop_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "bd" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.buyer_drop_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.buyer_drop_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.buyer_drop_signature.data,
        fileData.buyer_drop_signature.mimetype
      )
      editDoc.buyer_drop_signature = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_drop_signature) &&
      !isNullOrUndefined(fileData.driver_drop_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "dd" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_drop_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_drop_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_drop_signature.data,
        fileData.driver_drop_signature.mimetype
      )
      editDoc.driver_drop_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.invoice_image) &&
      !isNullOrUndefined(fileData.invoice_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.invoice_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.invoice_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.invoice_image.data,
        fileData.invoice_image.mimetype
      )
      editDoc.invoice_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.qube_test_report_7days) &&
      !isNullOrUndefined(fileData.qube_test_report_7days.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.qube_test_report_7days.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.qube_test_report_7days.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.qube_test_report_7days.data,
        fileData.qube_test_report_7days.mimetype
      )
      editDoc.qube_test_report_7days = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.qube_test_report_28days) &&
      !isNullOrUndefined(fileData.qube_test_report_28days.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.qube_test_report_28days.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.qube_test_report_28days.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.qube_test_report_28days.data,
        fileData.qube_test_report_28days.mimetype
      )
      editDoc.qube_test_report_28days = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    const updtRes = await OrderTrackModel.updateOne(
      {
        _id: new ObjectId(track_id),
        vendor_order_id: order_id,
        // vendor_user_id: new ObjectId(vendor_id),
        TM_id,
      },
      { $set: editDoc }
    )
    if (updtRes.n !== 1) {
      return new InvalidInput(`Update failed as no record exists`, 400)
    }
  }

  async uploadCPDocument(
    order_id: string,
    track_id: string,
    vendor_id: string,
    CP_id: string,
    fileData: {
      challan_image: UploadedFile
      supplier_pickup_signature: UploadedFile
      driver_pickup_signature: UploadedFile
      buyer_drop_signature: UploadedFile
      driver_drop_signature: UploadedFile
      qube_test_report_7days: UploadedFile
      qube_test_report_28days: UploadedFile
    }
  ) {
    const editDoc: { [k: string]: any } = {}

    console.log("fileData", fileData)

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.challan_image) &&
      !isNullOrUndefined(fileData.challan_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "ch" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.challan_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.challan_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.challan_image.data,
        fileData.challan_image.mimetype
      )
      editDoc.challan_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.supplier_pickup_signature) &&
      !isNullOrUndefined(fileData.supplier_pickup_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "sp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.supplier_pickup_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.supplier_pickup_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.supplier_pickup_signature.data,
        fileData.supplier_pickup_signature.mimetype
      )
      editDoc.supplier_pickup_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_pickup_signature) &&
      !isNullOrUndefined(fileData.driver_pickup_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "dp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_pickup_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_pickup_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_pickup_signature.data,
        fileData.driver_pickup_signature.mimetype
      )
      editDoc.driver_pickup_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.buyer_drop_signature) &&
      !isNullOrUndefined(fileData.buyer_drop_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "bd" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.buyer_drop_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.buyer_drop_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.buyer_drop_signature.data,
        fileData.buyer_drop_signature.mimetype
      )
      editDoc.buyer_drop_signature = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.driver_drop_signature) &&
      !isNullOrUndefined(fileData.driver_drop_signature.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "dd" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.driver_drop_signature.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.driver_drop_signature.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.driver_drop_signature.data,
        fileData.driver_drop_signature.mimetype
      )
      editDoc.driver_drop_signature = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.qube_test_report_7days) &&
      !isNullOrUndefined(fileData.qube_test_report_7days.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.qube_test_report_7days.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.qube_test_report_7days.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.qube_test_report_7days.data,
        fileData.qube_test_report_7days.mimetype
      )
      editDoc.qube_test_report_7days = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.qube_test_report_28days) &&
      !isNullOrUndefined(fileData.qube_test_report_28days.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/order_track`
      let objectName = "" + "rp" + order_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.qube_test_report_28days.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.qube_test_report_28days.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.qube_test_report_28days.data,
        fileData.qube_test_report_28days.mimetype
      )
      editDoc.qube_test_report_28days = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    const updtRes = await CPOrderTrackModel.updateOne(
      {
        _id: new ObjectId(track_id),
        vendor_order_id: order_id,
        //vendor_user_id: new ObjectId(vendor_id),
        CP_id,
      },
      { $set: editDoc }
    )
    if (updtRes.n !== 1) {
      return new InvalidInput(`Update failed as no record exists`, 400)
    }
  }

  async getOrderDetailsById(
    track_id: string,
    order_id: string,
    vendor_id: string
  ) {
    console.log("user_id", vendor_id)
    const query: { [k: string]: any } = {
      _id: new ObjectId(track_id),
      vendor_order_id: order_id,
      //  vendor_user_id: new ObjectId(vendor_id),
    }

    query.$or = [
      {
        vendor_user_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    console.log("query", query)
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TM_info",
        },
      },
      {
        $unwind: {
          path: "$TM_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "driver_info",
          localField: "TM_info.driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_info.TM_category_id",
          foreignField: "_id",
          as: "TM_category_info",
        },
      },
      {
        $unwind: {
          path: "$TM_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_info.TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category_info",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendor_order_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_order_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "vendor_order_info.buyer_id",
          foreignField: "_id",
          as: "buyer_info",
        },
      },
      {
        $unwind: {
          path: "$buyer_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_order_info.user_id",
          foreignField: "_id",
          as: "vendor_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "vendor_order_info.design_mix_id",
          foreignField: "_id",
          as: "design_mix_variant_info",
        },
      },
      {
        $unwind: {
          path: "$design_mix_variant_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendor_order_info.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order_info",
        },
      },
      {
        $unwind: {
          path: "$order_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "order_item_info",
        },
      },
      {
        $unwind: {
          path: "$order_item_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order_info.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: true,
          remaining_quantity: true,
          pickup_quantity: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          royalty_pass_image_url: true,
          way_slip_image_url: true,
          challan_image_url: true,
          invoice_url: true,
          supplier_pickup_signature: true,
          driver_pickup_signature: true,
          buyer_drop_signature: true,
          driver_drop_signature: true,
          "vendor_order_info.quantity": true,
          "vendor_order_info._id": true,
          "vendor_order_info.created_at": true,
          "TM_info._id": true,
          "TM_info.TM_category_id": true,
          "TM_info.TM_sub_category_id": true,
          "TM_info.TM_rc_number": true,
          "TM_info.TM_image_url": true,
          "TM_category_info._id": true,
          "TM_category_info.category_name": true,
          "TM_sub_category_info._id": true,
          "TM_sub_category_info.sub_category_name": true,
          "TM_sub_category_info.load_capacity": true,
          "buyer_info._id": true,
          "buyer_info.full_name": true,
          "buyer_info.mobile_number": true,
          "buyer_info.company_name": true,
          "vendor_info._id": true,
          "vendor_info.full_name": true,
          "vendor_info.mobile_number": true,
          "vendor_info.company_name": true,
          "pickup_address_info.pickup_location":
            "$pickup_address_info.location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "delivery_address_info.pincode": true,
          "order_info._id": true,
          "order_item_info._id": true,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const orders = await OrderTrackModel.aggregate(aggregateArr)
    return Promise.resolve(orders[0])
  }

  async getCPOrderTrackDetailsById(
    track_id: string,
    order_id: string,
    vendor_id: string
  ) {
    console.log("user_id", vendor_id)
    const query: { [k: string]: any } = {
      _id: new ObjectId(track_id),
      vendor_order_id: order_id,
      //  vendor_user_id: new ObjectId(vendor_id),
    }

    console.log("query", query)
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "concrete_pump",
          localField: "CP_id",
          foreignField: "_id",
          as: "CP_info",
        },
      },
      {
        $unwind: {
          path: "$CP_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "CP_info.concrete_pump_category_id",
          foreignField: "_id",
          as: "CP_category_info",
        },
      },
      {
        $unwind: {
          path: "$CP_category_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendor_order_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_order_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "vendor_order_info.buyer_id",
          foreignField: "_id",
          as: "buyer_info",
        },
      },
      {
        $unwind: {
          path: "$buyer_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "vendor_order_info.user_id",
          foreignField: "_id",
          as: "vendor_info",
        },
      },
      {
        $unwind: {
          path: "$vendor_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "vendor_order_info.design_mix_id",
          foreignField: "_id",
          as: "design_mix_variant_info",
        },
      },
      {
        $unwind: {
          path: "$design_mix_variant_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendor_order_info.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order_info",
        },
      },
      {
        $unwind: {
          path: "$order_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "order_item_info",
        },
      },
      {
        $unwind: {
          path: "$order_item_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order_info.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "operator_info",
          localField: "CP_info.operator_id",
          foreignField: "_id",
          as: "operator_info",
        },
      },
      {
        $unwind: { path: "$operator_info", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: true,
          remaining_quantity: true,
          pickup_quantity: true,
          created_at: true,
          assigned_at: true,
          pickedup_at: true,
          event_status: true,
          royalty_pass_image_url: true,
          way_slip_image_url: true,
          challan_image_url: true,
          invoice_image_url: true,
          supplier_pickup_signature: true,
          driver_pickup_signature: true,
          buyer_drop_signature: true,
          driver_drop_signature: true,
          "vendor_order_info.quantity": true,
          "vendor_order_info._id": true,
          "vendor_order_info.created_at": true,
          "CP_info._id": true,
          "CP_info.concrete_pump_category_id": true,
          "CP_info.company_name": true,
          "CP_info.concrete_pump_model": true,
          "CP_info.manufacture_year": true,
          "CP_info.pipe_connection": true,
          "CP_info.concrete_pump_capacity": true,
          "CP_info.is_Operator": true,
          "CP_info.operator_id": true,
          "CP_info.is_helper": true,
          "CP_info.transportation_charge": true,
          "CP_info.concrete_pump_price": true,
          "CP_info.concrete_pump_image_url": true,
          "CP_info.serial_number": true,
          "CP_category_info.category_name": true,
          "CP_category_info.is_active": true,
          "buyer_info._id": true,
          "buyer_info.full_name": true,
          "buyer_info.mobile_number": true,
          "buyer_info.company_name": true,
          "vendor_info._id": true,
          "vendor_info.full_name": true,
          "vendor_info.mobile_number": true,
          "vendor_info.company_name": true,
          "pickup_address_info.pickup_location":
            "$pickup_address_info.location",
          "pickup_address_info._id": true,
          "pickup_address_info.line1": true,
          "pickup_address_info.line2": true,
          "pickup_address_info.city_name": "$city.city_name",
          "pickup_address_info.state_name": "$city.state_name",
          "pickup_address_info.pincode": true,
          "delivery_address_info.delivery_location":
            "$order_info.delivery_location",
          "delivery_address_info._id": true,
          "delivery_address_info.line1": "$delivery_address_info.address_line1",
          "delivery_address_info.line2": "$delivery_address_info.address_line2",
          "delivery_address_info.city_name": "$city.city_name",
          "delivery_address_info.state_name": "$city.state_name",
          "delivery_address_info.pincode": true,
          "order_info._id": true,
          "order_item_info._id": true,
          "operator_info.operator_name": 1,
          "operator_info.operator_mobile_number": 1,
          CP_serial_number: true,
          CP_category_name: true,
          CP_operator_name: true,
          CP_operator_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]
    const orders = await CPOrderTrackModel.aggregate(aggregateArr)
    return Promise.resolve(orders[0])
  }

  async orderDelayed(
    vendor_id: string,
    reasonForDelay: string,
    delayTime: string,
    vendor_order_id: string,
    TM_id: string,
    track_id: string
  ) {
    //find vendor order doc.
    const vendorOrderDoc = await VendorOrderModel.findOne({
      _id: vendor_order_id,
    })

    if (isNullOrUndefined(vendorOrderDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details asssociated with this Id.`,
          400
        )
      )
    }

    //find TM doc.
    const TMDoc = await TMModel.findById(TM_id)
    if (isNullOrUndefined(TMDoc)) {
      return Promise.reject(new InvalidInput(`No TM details found.`, 400))
    }

    //find vehicle sub cate doc.
    const TMsubCategoryDoc = await TMSubCategoryModel.findOne({
      _id: TMDoc.TM_sub_category_id,
    })
 
    if (isNullOrUndefined(TMsubCategoryDoc)) {
      return Promise.reject(
        new InvalidInput(`No matched TM sub category document found.`, 400)
      )
    }
    //find vendor user doc.
    const vendorUserDoc = await VendorUserModel.findById(vendorOrderDoc.user_id)
    if (isNullOrUndefined(vendorUserDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not  find Vendor for the entered order details`,
          400
        )
      )
    }

    //find buyer order doc.
    const buyerOrderDoc = await OrderModel.findOne({
      _id: vendorOrderDoc.buyer_order_id,
    })
    if (isNullOrUndefined(buyerOrderDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find Buyer order for the entered order Id`,
          400
        )
      )
    }

    //find buyer user doc.
    const buyerUserDoc = await BuyerUserModel.findById(buyerOrderDoc.user_id)
    if (isNullOrUndefined(buyerUserDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not any find Buyer for the entered order details.`,
          400
        )
      )
    }

    //update vendor order status
    await VendorOrderModel.findOneAndUpdate(
      {
        _id: vendor_order_id,
      },
      {
        $set: {
          order_status: SupplierOrderStatus.DELAYED,
          reasonForDelay,
          delayTime,
          delayed_at: Date.now(),
        },
      }
    )

    // update order track status

    /** Add in supplier order also as "DELAYED" */
    await OrderTrackModel.findOneAndUpdate(
      {
        _id: new ObjectId(track_id),
        vendor_order_id,
        TM_id,
      },
      {
        $set: {
          event_status: TrackStatus.DELAYED,
          reasonForDelay,
          delayTime,
          delayed_at: Date.now(),
        },
      }
    )

    let orderTrackDoc = await OrderTrackModel.findOne({
      _id: new ObjectId(track_id),
    })

    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }
    console.log("orderTrackDoc", orderTrackDoc)
    let orderItemDoc = await OrderItemModel.findOne({
      _id: orderTrackDoc.order_item_id,
    })

    if (isNullOrUndefined(orderItemDoc)) {
      return Promise.reject(new InvalidInput(`No Item found`, 400))
    }

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: vendorOrderDoc.buyer_id,
      notification_type: ClientNotificationType.orderDelay,
      order_id: orderTrackDoc.buyer_order_id,
      item_id: orderItemDoc._id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id,
    })

      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i< adminUserDocs.length; i ++){  
        await this.notUtil.addNotificationForAdmin({
          to_user_type: UserType.ADMIN,
          to_user_id: adminUserDocs[i]._id,
          notification_type: AdminNotificationType.orderDelay,
          order_id: orderTrackDoc.buyer_order_id,
          item_id: orderItemDoc._id,
          order_item_id: orderItemDoc.display_item_id,
          vendor_id,
        })
      }


    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.sub_vendor_id,
      notification_type: VendorNotificationType.orderDelay,
      order_id: orderTrackDoc.buyer_order_id,
      item_id: orderItemDoc._id,
      order_item_id: orderItemDoc.display_item_id,
      TM_id,
      vendor_order_id,
      vendor_id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: vendorOrderDoc.master_vendor_id,
      notification_type: VendorNotificationType.orderDelay,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      item_id: orderItemDoc._id,
      TM_id,
      vendor_order_id,
      vendor_id,
    })

    let buyerUserData1 = await BuyerUserModel.findOne({
      _id: new ObjectId(vendorOrderDoc.buyer_id),
    })

    let client_name1: any
    let mobile_number1: any
    if (!isNullOrUndefined(buyerUserData1)) {
      mobile_number1 = buyerUserData1.mobile_number
      if (buyerUserData1.account_type === "Individual") {
        client_name1 = buyerUserData1.full_name
      } else {
        client_name1 = buyerUserData1.company_name
      }
    }

    let vendorUserData1 = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })

    let vendor_mobile_number: any
    let vendor_company_name: any
    if (!isNullOrUndefined(vendorUserData1)) {
      vendor_mobile_number = vendorUserData1.mobile_number
      vendor_company_name = vendorUserData1.company_name
    }

    let siteData1 = await SiteModel.findOne({
      _id: new ObjectId(buyerOrderDoc.site_id),
    })

    let site_person_name1: any
    let site_mobile_number1: any
    if (!isNullOrUndefined(siteData1)) {
      site_person_name1 = siteData1.person_name
      site_mobile_number1 = siteData1.mobile_number
    }

    let message1 = `Hi ${client_name1}, your order for product id ${orderItemDoc.display_item_id} having an order id ${buyerOrderDoc._id} has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let templateId1 = "1707163842741982621"
    await this.testService.sendSMS(mobile_number1, message1, templateId1)

    let site_message1 = `Hi ${site_person_name1}, your order for product id ${orderItemDoc.display_item_id} having an order id ${buyerOrderDoc._id} has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let site_templateId1 = "1707163842741982621"
    await this.testService.sendSMS(
      site_mobile_number1,
      site_message1,
      site_templateId1
    )

    let vendor_message = `Hi ${vendor_company_name}, the supply of order product id ${orderItemDoc.display_item_id}, for your client ${client_name1}, having an order id (${vendor_order_id}), has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let vendor_templateId = "1707163842751994626"
    await this.testService.sendSMS(
      vendor_mobile_number,
      vendor_message,
      vendor_templateId
    )

    return Promise.resolve()
  }

  async CPorderDelayed(
    vendor_id: string,
    reasonForDelay: string,
    delayTime: string,
    vendor_order_id: string,
    CP_id: string,
    track_id: string
  ) {
    //find CP doc.
    const CPDoc = await ConcretePumpModel.findById(CP_id)
    if (isNullOrUndefined(CPDoc)) {
      return Promise.reject(new InvalidInput(`No CP details.`, 400))
    }
    /** Add in Tracking as "DELAYED" */
    await CPOrderTrackModel.findOneAndUpdate(
      {
        _id: new ObjectId(track_id),
        vendor_order_id,
        CP_id,
      },
      {
        $set: {
          event_status: TrackStatus.DELAYED,
          reasonForDelay,
          delayTime,
          delayed_at: Date.now(),
        },
      }
    )

    let orderTrackDoc = await CPOrderTrackModel.findOne({
      _id: new ObjectId(track_id),
    })
    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }

    let orderItemDoc = await OrderItemModel.findOne({
      _id: orderTrackDoc.order_item_id,
    })

    if (isNullOrUndefined(orderItemDoc)) {
      return Promise.reject(new InvalidInput(`No Item found`, 400))
    }

     // Check Admin Type.
     const adminQuery: { [k: string]: any } = {}
     adminQuery.$or = [
       {
         admin_type : AdminRoles.admin_manager
       },
       {
         admin_type : AdminRoles.admin_sales
       }
     ]
     const adminUserDocs = await AdminUserModel.find(adminQuery)
     if (adminUserDocs.length < 0) {
       return Promise.reject(
         new InvalidInput(`No Admin data were found `, 400)
       )
     }
     for(let i = 0 ; i< adminUserDocs.length; i ++){  
     await this.notUtil.addNotificationForAdmin({
      to_user_type: UserType.ADMIN,
      to_user_id: adminUserDocs[i]._id,
      notification_type: AdminNotificationType.CPDelay,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id,
      item_id: orderItemDoc._id,
    })
  }

    let buyer_order_display_id: any
    let site_name: any
    let site_person_name1: any
    let site_mobile_number1: any
    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(orderTrackDoc.buyer_order_id),
    })
    if (!isNullOrUndefined(buyerOrderDoc)) {
      buyer_order_display_id = buyerOrderDoc.display_id

      let siteData1 = await SiteModel.findOne({
        _id: new ObjectId(buyerOrderDoc.site_id),
      })

      if (!isNullOrUndefined(siteData1)) {
        site_name = siteData1.site_name
        site_person_name1 = siteData1.person_name
        site_mobile_number1 = siteData1.mobile_number
      }
    }

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: orderItemDoc.buyer_id,
      notification_type: ClientNotificationType.CPDelay,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id,
      item_id: orderItemDoc._id,
    })



    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderItemDoc.master_vendor_id,
      notification_type: VendorNotificationType.CPDelay,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      CP_id,
      vendor_order_id,
      vendor_id,
      item_id: orderItemDoc._id,
    })

    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderItemDoc.sub_vendor_id,
      notification_type: VendorNotificationType.CPDelay,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      CP_id,
      vendor_order_id,
      vendor_id,
      item_id: orderItemDoc._id,
    })

    let client_name1: any
    let mobile_number1: any
    let vendorOrderDoc = await VendorOrderModel.findOne({
      _id: vendor_order_id,
    })
    if (!isNullOrUndefined(vendorOrderDoc)) {
      let buyerUserData1 = await BuyerUserModel.findOne({
        _id: new ObjectId(vendorOrderDoc.buyer_id),
      })

      if (!isNullOrUndefined(buyerUserData1)) {
        mobile_number1 = buyerUserData1.mobile_number
        if (buyerUserData1.account_type === "Individual") {
          client_name1 = buyerUserData1.full_name
        } else {
          client_name1 = buyerUserData1.company_name
        }
      }
    }

    let vendorUserData1 = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    let vendor_mobile_number: any
    let vendor_company_name: any
    if (!isNullOrUndefined(vendorUserData1)) {
      vendor_mobile_number = vendorUserData1.mobile_number
      vendor_company_name = vendorUserData1.company_name
    }

    // CP Delay
    let message1 = `Hi ${client_name1}, delivery of CP assigned to your product id ${orderItemDoc.display_item_id} having an order id ${buyer_order_display_id} has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let templateId1 = "1707163842633864277"
    await this.testService.sendSMS(mobile_number1, message1, templateId1)

    let site_message1 = `Hi ${site_person_name1}, delivery of CP assigned to your product id ${orderItemDoc.display_item_id} having an order id ${buyer_order_display_id} has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let site_templateId1 = "1707163842633864277"
    await this.testService.sendSMS(
      site_mobile_number1,
      site_message1,
      site_templateId1
    )

    let vendor_message = `Hi ${vendor_company_name}, delivery of CP for your client ${client_name1} for their ${site_name} site for product id ${orderItemDoc.display_item_id} having an order id ${vendor_order_id} has been delayed by ${orderTrackDoc.delayTime} owing to ${orderTrackDoc.reasonForDelay}. - conmix`
    let vendor_templateId = "1707163842641220810"
    await this.testService.sendSMS(
      vendor_mobile_number,
      vendor_message,
      vendor_templateId
    )

    /** Add in supplier order also as  */
    // update order item status.
    // await OrderItemModel.findOneAndUpdate(
    //   {
    //     _id: new ObjectId(orderTrackDoc.order_item_id),
    //   },
    //   {
    //     $set: {
    //       item_status: OrderItemStatus.DELAY,
    //       delayed_at: Date.now(),
    //     },
    //   }
    // )

    // Send SMS to supplier. Send SMS to logistics, admin, buyer and admin to. Confirm after template data decided.
    // const payload = `Decide After template final.`
    // await this.twilioService.sendSMS(vendorUserDoc.mobile_number, payload, {
    //   to_mobileNumber: vendorUserDoc.mobile_number,
    //   text: payload,
    //   sent_at: new Date(),
    //   send_status: "sent",
    // })

    return Promise.resolve()
  }

  async orderDelivered(
    vendor_id: string,
    TM_id: string,
    vendor_order_id: string,
    track_id: string
  ) {
    // update order track status.
    let orderTrackDoc = await OrderTrackModel.findOne({
      _id: new ObjectId(track_id),
      vendor_order_id,
      // vendor_user_id: new ObjectId(vendor_id),
      TM_id: new ObjectId(TM_id),
    })

    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }

    let orderDoc = await OrderModel.findOne({
      _id: orderTrackDoc.buyer_order_id,
    })

    if (isNullOrUndefined(orderDoc)) {
      return Promise.reject(new InvalidInput(`No Item Found.`, 400))
    }

    console.log("orderDoc", orderDoc)

    await OrderTrackModel.updateOne(
      {
        _id: new ObjectId(track_id),
        vendor_order_id,
        // vendor_user_id: new ObjectId(vendor_id),
        TM_id: new ObjectId(TM_id),
      },
      {
        $set: {
          event_status: SupplierOrderStatus.DELIVERED,
          delivered_at: Date.now(),
        },
      }
    )

    // update order item status.
    let orderItemData = await OrderItemModel.findOne({
      _id: orderTrackDoc.order_item_id,
    })

    if (isNullOrUndefined(orderItemData)) {
      return Promise.reject(new InvalidInput(`No Order Item Found.`, 400))
    }

    await OrderItemModel.updateOne(
      { _id: orderTrackDoc.order_item_id },
      {
        $set: {
          // item_status: LogisticsOrderItemStatus.DELIVERED,
          // delivered_at: Date.now(),
          pickup_quantity:
            orderItemData.pickup_quantity + orderTrackDoc.pickup_quantity,
          remaining_quantity:
            orderItemData.remaining_quantity - orderTrackDoc.pickup_quantity,
        },
      }
    )
    const data = await OrderItemModel.find({
      order_id: orderTrackDoc.buyer_order_id,
      _id: orderTrackDoc.order_item_id,
      remaining_quantity: { $eq: 0 },
      item_status: { $ne: SupplierOrderStatus.DELIVERED },
    })
    let vendorOrderDoc: any
    console.log("data**************", data)

    // const adminUserDoc = await AdminUserModel.findOne({
    //   admin_type: "superAdmin",
    // })

    // if (isNullOrUndefined(adminUserDoc)) {
    //   return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    // }

    if (data.length > 0) {
      //update supplier order status also.
      vendorOrderDoc = await VendorOrderModel.findOneAndUpdate(
        {
          buyer_order_item_id: orderTrackDoc.order_item_id,
          // user_id: orderTrackDoc.vendor_user_id,
        },
        {
          $set: {
            order_status: SupplierOrderStatus.DELIVERED,
            delivered_at: Date.now(),
          },
        }
      )

      if (isNullOrUndefined(vendorOrderDoc)) {
        return Promise.reject(
          new InvalidInput(`No Vendor Order Doc were Found.`, 400)
        )
      }

      let orderItem = await OrderItemModel.findOneAndUpdate(
        { _id: orderTrackDoc.order_item_id },
        {
          $set: {
            item_status: SupplierOrderStatus.DELIVERED,
            delivered_at: Date.now(),
          },
        }
      )

      if (isNullOrUndefined(orderItem)) {
        return Promise.reject(new InvalidInput(`No Item Found.`, 400))
      }

      let buyerUserData1 = await BuyerUserModel.findOne({
        _id: new ObjectId(vendorOrderDoc.buyer_id),
      })
      let client_name1: any
      let mobile_number1: any
      let email1: any


      if (!isNullOrUndefined(buyerUserData1)) {
        mobile_number1 = buyerUserData1.mobile_number
        email1 = buyerUserData1.email
        if (buyerUserData1.account_type === "Individual") {
          client_name1 = buyerUserData1.full_name
        } else {
          client_name1 = buyerUserData1.company_name
        }
      }

      let m_vendorUserData1 = await VendorUserModel.findOne({
        _id: new ObjectId(orderItem.master_vendor_id),
      })
      //let vendor_name1: any
      let m_vendor_mobile_number: any
      let m_vendor_company_name: any
      let m_vendor_email: any
      if (!isNullOrUndefined(m_vendorUserData1)) {
        // vendor_name1 = vendorUserData1.full_name
        m_vendor_mobile_number = m_vendorUserData1.mobile_number
        m_vendor_company_name = m_vendorUserData1.company_name
        m_vendor_email = m_vendorUserData1.email
      }

      let s_vendorUserData1 = await VendorUserModel.findOne({
        _id: new ObjectId(orderItem.sub_vendor_id),
      })
      //let vendor_name1: any
      let s_vendor_mobile_number: any
      let s_vendor_company_name: any
      let s_vendor_email: any
      if (!isNullOrUndefined(s_vendorUserData1)) {
        // vendor_name1 = vendorUserData1.full_name
        s_vendor_mobile_number = s_vendorUserData1.mobile_number
        s_vendor_company_name = s_vendorUserData1.company_name
        s_vendor_email = s_vendorUserData1.email
      }

      let siteData1 = await SiteModel.findOne({
        _id: new ObjectId(orderDoc.site_id),
      })
      let site_name: any
      let site_person_name1: any
      let site_mobile_number1: any
      let site_email1: any
      if (!isNullOrUndefined(siteData1)) {
        site_name = siteData1.site_name
        site_person_name1 = siteData1.person_name
        site_mobile_number1 = siteData1.mobile_number
        site_email1 = siteData1.email
      }

      // product fully delievered.
      let message1 = `Hi ${client_name1}, your product id ${orderItem.display_item_id} having an Order ID ${orderDoc.display_id} for your site ${site_name} has been fully delivered, as per the purchase policy, and completed by RMC Supplier ${s_vendor_company_name}. Kindly share your feedback here 'weblink'. - conmix`
      let templateId1 = "1707163732915101352"
      await this.testService.sendSMS(mobile_number1, message1, templateId1)

      let order_delievered_html = client_mail_template.order_delieverd
      order_delievered_html = order_delievered_html.replace("{{client_name}}", client_name1)
      order_delievered_html = order_delievered_html.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_delievered_html = order_delievered_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
      order_delievered_html = order_delievered_html.replace("{{site_name}}", site_name)
      order_delievered_html = order_delievered_html.replace("{{vendor_company_name}}", s_vendor_company_name)
      var emaildata = {
        from: "no-reply@conmate.in",
        to: email1,
        subject: "Conmix - Order Delivered",
        html: order_delievered_html,
      }
      await this.mailgunService.sendEmail(emaildata)

      let site_message1 = `Hi ${site_person_name1}, your product id ${orderItem.display_item_id} having an Order ID ${orderDoc.display_id} for your site ${site_name} has been fully delivered, as per the purchase policy, and completed by RMC Supplier ${s_vendor_company_name}. Kindly share your feedback here 'weblink'. - conmix`
      let site_templateId1 = "1707163732915101352"
      await this.testService.sendSMS(
        site_mobile_number1,
        site_message1,
        site_templateId1
      )

      let order_delievered_html_for_site = client_mail_template.order_delieverd
      order_delievered_html_for_site = order_delievered_html_for_site.replace("{{client_name}}", site_person_name1)
      order_delievered_html_for_site = order_delievered_html_for_site.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_delievered_html_for_site = order_delievered_html_for_site.replace("{{orderDoc.display_id}}", orderDoc.display_id)
      order_delievered_html_for_site = order_delievered_html_for_site.replace("{{site_name}}", site_name)
      order_delievered_html_for_site = order_delievered_html_for_site.replace("{{vendor_company_name}}", s_vendor_company_name)
      var emaildata = {
        from: "no-reply@conmate.in",
        to: site_email1,
        subject: "Conmix - Order Delivered",
        html: order_delievered_html_for_site,
      }
      await this.mailgunService.sendEmail(emaildata)

      // product fully delievered for vendor.
      let v_msg = `Hi ${m_vendor_company_name}, you have now fully delivered the Product Id ${orderItem.display_item_id} having an Order ID ${vendorOrderDoc._id}. Kindly login to your Partner Account for more information. - conmix`
      let v_Id = "1707163784634477574"
      await this.testService.sendSMS(m_vendor_mobile_number, v_msg, v_Id)

      let v1_msg = `Hi ${s_vendor_company_name}, you have now fully delivered the Product Id ${orderItem.display_item_id} having an Order ID ${vendorOrderDoc._id}. Kindly login to your Partner Account for more information. - conmix`
      let v1_Id = "1707163784634477574"
      await this.testService.sendSMS(s_vendor_company_name, v1_msg, v1_Id)

      let order_delievered_html_for_vendor = vendor_mail_template.order_delivered
      order_delievered_html_for_vendor = order_delievered_html_for_vendor.replace("{{vendor_company_name}}", m_vendor_company_name)
      order_delievered_html_for_vendor = order_delievered_html_for_vendor.replace("{{client_name}}", client_name1)
      order_delievered_html_for_vendor = order_delievered_html_for_vendor.replace("{{site_name}}", site_name)
      order_delievered_html_for_vendor = order_delievered_html_for_vendor.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_delievered_html_for_vendor = order_delievered_html_for_vendor.replace("{{vendorOrderDoc._id}}", vendorOrderDoc._id)
      var emaildata1 = {
        from: "no-reply@conmate.in",
        to: m_vendor_email,
        subject: "Conmix - Order Delivered",
        html: order_delievered_html_for_vendor,
      }
      await this.mailgunService.sendEmail(emaildata1)

      let order_delievered_html_for_sub_vendor = vendor_mail_template.order_delivered
      order_delievered_html_for_sub_vendor = order_delievered_html_for_sub_vendor.replace("{{vendor_company_name}}", s_vendor_company_name)
      order_delievered_html_for_sub_vendor = order_delievered_html_for_sub_vendor.replace("{{client_name}}", client_name1)
      order_delievered_html_for_sub_vendor = order_delievered_html_for_sub_vendor.replace("{{site_name}}", site_name)
      order_delievered_html_for_sub_vendor = order_delievered_html_for_sub_vendor.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_delievered_html_for_sub_vendor = order_delievered_html_for_sub_vendor.replace("{{vendorOrderDoc._id}}", vendorOrderDoc._id)
      var emaildata1 = {
        from: "no-reply@conmate.in",
        to: s_vendor_email,
        subject: "Conmix - Order Delivered",
        html: order_delievered_html_for_vendor,
      }
      await this.mailgunService.sendEmail(emaildata1)

      // order fully delievered for vendor.
      let vendor_message = `Hi ${m_vendor_company_name}, you have fully delivered and completed the order for Product Id ${orderItem.display_item_id} having an Order ID ${vendorOrderDoc._id} as per the Vendor/Supplier/Partner Policy. Login to your Conmix Partner account for more details.`
      let vendor_templateId = "1707163731348721130"
      await this.testService.sendSMS(
        m_vendor_mobile_number,
        vendor_message,
        vendor_templateId
      )

      let s_vendor_message = `Hi ${s_vendor_company_name}, you have fully delivered and completed the order for Product Id ${orderItem.display_item_id} having an Order ID ${vendorOrderDoc._id} as per the Vendor/Supplier/Partner Policy. Login to your Conmix Partner account for more details.`
      let s_vendor_templateId = "1707163731348721130"
      await this.testService.sendSMS(
        s_vendor_mobile_number,
        s_vendor_message,
        s_vendor_templateId
      )

      let order_fully_delievered_html = vendor_mail_template.order_fully_delivered
      order_fully_delievered_html = order_fully_delievered_html.replace("{{vendor_company_name}}", m_vendor_company_name)
      order_fully_delievered_html = order_fully_delievered_html.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_fully_delievered_html = order_fully_delievered_html.replace("{{vendorOrderDoc._id}}", vendorOrderDoc._id)
      order_fully_delievered_html = order_fully_delievered_html.replace("{{client_name}}", client_name1)
      var emaildata11 = {
        from: "no-reply@conmate.in",
        to: m_vendor_email,
        subject: "Conmix - Order Delivered",
        html: order_fully_delievered_html,
      }
      await this.mailgunService.sendEmail(emaildata11)

      let order_fully_delievered_sub_html = vendor_mail_template.order_fully_delivered
      order_fully_delievered_sub_html = order_fully_delievered_sub_html.replace("{{vendor_company_name}}", s_vendor_company_name)
      order_fully_delievered_sub_html = order_fully_delievered_sub_html.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_fully_delievered_sub_html = order_fully_delievered_sub_html.replace("{{vendorOrderDoc._id}}", vendorOrderDoc._id)
      order_fully_delievered_sub_html = order_fully_delievered_sub_html.replace("{{client_name}}", client_name1)
      var emaildata11 = {
        from: "no-reply@conmate.in",
        to: s_vendor_email,
        subject: "Conmix - Order Delivered",
        html: order_fully_delievered_html,
      }
      await this.mailgunService.sendEmail(emaildata11)

      // let order_fully_delievered_client_html = client_mail_template.order_fully_delivered
      // order_fully_delievered_client_html = order_fully_delievered_client_html.replace("{{vendor_company_name}}", vendor_company_name)
      // order_fully_delievered_client_html = order_fully_delievered_client_html.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      // order_fully_delievered_client_html = order_fully_delievered_client_html.replace("{{vendorOrderDoc._id}}", vendorOrderDoc._id)
      // order_fully_delievered_client_html = order_fully_delievered_client_html.replace("{{client_name}}", client_name1)
      // var emaildata11 = {
      //   from: "no-reply@conmate.in",
      //   to: vendor_email,
      //   subject: "Conmix - Order Delivered",
      //   html: order_fully_delievered_client_html,
      // }
      // await this.mailgunService.sendEmail(emaildata11)


      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: vendorOrderDoc.buyer_id,
        notification_type: ClientNotificationType.fullyProductDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItem.display_item_id,
        vendor_id,
      })



      // Product fully Delivered.
      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        },
        {
          admin_type : AdminRoles.superAdmin
        },
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i < adminUserDocs.length; i++){
      await this.notUtil.addNotificationForAdmin({
        to_user_type: UserType.ADMIN,
        to_user_id: adminUserDocs[i]._id,
        notification_type: AdminNotificationType.fullyProductDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItem.display_item_id,
        vendor_id,
        client_id: vendorOrderDoc.buyer_id,
      })

      let order_delievered_html_admin = admin_mail_template.order_delivered
      order_delievered_html_admin = order_delievered_html_admin.replace("{{adminUserDoc.full_name}}", adminUserDocs[i].full_name)
      order_delievered_html_admin = order_delievered_html_admin.replace("{{orderItem.display_item_id}}", orderItem.display_item_id)
      order_delievered_html_admin = order_delievered_html_admin.replace("{{orderDoc.display_id}}", orderDoc.display_id)
      order_delievered_html_admin = order_delievered_html_admin.replace("{{client_name}}", client_name1)
      order_delievered_html_admin = order_delievered_html_admin.replace("{{site_name}}", site_name)
      order_delievered_html_admin = order_delievered_html_admin.replace("{{vendor_company_name}}", s_vendor_company_name)
      var admin_emaildata = {
        from: "no-reply@conmate.in",
        to: adminUserDocs[i].email,
        subject: "Conmix - Order Delivered",
        html: order_delievered_html_admin,
      }
      await this.mailgunService.sendEmail(admin_emaildata)
    }

      // Send notofication to Supplier.
      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: vendorOrderDoc.master_vendor_id,
        notification_type: VendorNotificationType.orderDelivered,
        vendor_order_id: vendorOrderDoc._id,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItem.display_item_id,
        vendor_id,
        client_id: vendorOrderDoc.buyer_id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: vendorOrderDoc.sub_vendor_id,
        notification_type: VendorNotificationType.orderDelivered,
        vendor_order_id: vendorOrderDoc._id,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItem.display_item_id,
        vendor_id,
        client_id: vendorOrderDoc.buyer_id,
      })
    } else {
      let vendorOrderData = await VendorOrderModel.findOne({
        buyer_order_item_id: orderTrackDoc.order_item_id,
        // user_id: orderTrackDoc.vendor_user_id,
      })
      console.log("vendorOrderData", vendorOrderData)
      if (isNullOrUndefined(vendorOrderData)) {
        return Promise.reject(
          new InvalidInput(`No Vendor Order Doc were Found.`, 400)
        )
      }

      let orderItemData1 = await OrderItemModel.findOne({
        _id: orderTrackDoc.order_item_id,
      })

      if (isNullOrUndefined(orderItemData1)) {
        return Promise.reject(new InvalidInput(`No Item Found.`, 400))
      }

      let buyerUserData1 = await BuyerUserModel.findOne({
        _id: new ObjectId(vendorOrderData.buyer_id),
      })
      let client_name1: any
      let mobile_number1: any
      let email1: any
      // if (!isNullOrUndefined(buyerUserData1)) {
      //   client_name1 = buyerUserData1.full_name
      //   mobile_number1 = buyerUserData1.mobile_number
      // }

      if (!isNullOrUndefined(buyerUserData1)) {
        mobile_number1 = buyerUserData1.mobile_number
        email1 = buyerUserData1.email
        if (buyerUserData1.account_type === "Individual") {
          client_name1 = buyerUserData1.full_name
        } else {
          client_name1 = buyerUserData1.company_name
        }
      }

      let m_vendorUserData1 = await VendorUserModel.findOne({
        _id: new ObjectId(orderItemData.master_vendor_id),
      })
      //let vendor_name1: any
      let m_vendor_mobile_number: any
      let m_vendor_company_name: any
      let m_vendor_email: any
      if (!isNullOrUndefined(m_vendorUserData1)) {
        // vendor_name1 = vendorUserData1.full_name
        m_vendor_mobile_number = m_vendorUserData1.mobile_number
        m_vendor_company_name = m_vendorUserData1.company_name
        m_vendor_email =  m_vendorUserData1.email
      }

      let s_vendorUserData1 = await VendorUserModel.findOne({
        _id: new ObjectId(orderItemData.sub_vendor_id),
      })
      //let vendor_name1: any
      let s_vendor_mobile_number: any
      let s_vendor_company_name: any
      let s_vendor_email: any
      if (!isNullOrUndefined(s_vendorUserData1)) {
        // vendor_name1 = vendorUserData1.full_name
        s_vendor_mobile_number = s_vendorUserData1.mobile_number
        s_vendor_company_name = s_vendorUserData1.company_name
        s_vendor_email =  s_vendorUserData1.email
      }


      let orderItem = await OrderItemModel.findOne({
        _id: orderTrackDoc.order_item_id,
      })

      if (isNullOrUndefined(orderItem)) {
        return Promise.reject(new InvalidInput(`No Item Found.`, 400))
      }

      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: vendorOrderData.buyer_id,
        notification_type: ClientNotificationType.partiallyProductDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItemData1.display_item_id,
        vendor_id,
        vendor_order_id: vendorOrderData._id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: vendorOrderData.master_vendor_id,
        notification_type: VendorNotificationType.partiallyProductDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItemData1.display_item_id,
        vendor_id,
        client_id: vendorOrderData.buyer_id,
        vendor_order_id: vendorOrderData._id,
      })

      await this.notUtil.addNotificationForSupplier({
        to_user_type: UserType.VENDOR,
        to_user_id: vendorOrderData.sub_vendor_id,
        notification_type: VendorNotificationType.partiallyProductDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItemData1.display_item_id,
        vendor_id,
        client_id: vendorOrderData.buyer_id,
        vendor_order_id: vendorOrderData._id,
      })



      let site_name1: any
      let site_person_name1: any
      let site_mobile_number1: any
      let site_email1: any
      let buyerOrderInfo = await OrderModel.findOne({
        _id: new ObjectId(orderTrackDoc.buyer_order_id),
      })

      if (!isNullOrUndefined(buyerOrderInfo)) {
        let siteInfo = await SiteModel.findOne({
          _id: new ObjectId(buyerOrderInfo.site_id),
        })
        if (!isNullOrUndefined(siteInfo)) {
          site_name1 = siteInfo.site_name
          site_person_name1 = siteInfo.person_name
          site_mobile_number1 = siteInfo.mobile_number
          site_email1 = siteInfo.email
        }
      }

            // Check Admin Type.
            const adminQuery: { [k: string]: any } = {}
            adminQuery.$or = [
              {
                admin_type : AdminRoles.admin_manager
              },
              {
                admin_type : AdminRoles.admin_sales
              },
              {
                admin_type : AdminRoles.superAdmin
              },
            ]
            const adminUserDocs = await AdminUserModel.find(adminQuery)
            if (adminUserDocs.length < 0) {
              return Promise.reject(
                new InvalidInput(`No Admin data were found `, 400)
              )
            }
      
            for(let i = 0 ; i < adminUserDocs.length; i++){
              await this.notUtil.addNotificationForAdmin({
                to_user_type: UserType.ADMIN,
                to_user_id: adminUserDocs[i]._id,
                notification_type: AdminNotificationType.partiallyProductDelivered,
                order_id: orderTrackDoc.buyer_order_id,
                order_item_id: orderItemData1.display_item_id,
                vendor_id,
                client_id: vendorOrderData.buyer_id,
                vendor_order_id: vendorOrderData._id,
              })
      
                  
              let product_partially_delivered_mail_admin_html = admin_mail_template.product_partially_delivered
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{adminUserDoc.full_name}}", adminUserDocs[i].full_name)
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{vendor_company_name}}", s_vendor_company_name)
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{orderItemData.display_item_id}}", orderItemData.display_item_id)
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{vendorOrderData._id}}", vendorOrderData._id)
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{client_name}}", client_name1)
              product_partially_delivered_mail_admin_html = product_partially_delivered_mail_admin_html.replace("{{site_name}}", site_name1)
              var v_data1 = {
                    from: "no-reply@conmate.in",
                    to: adminUserDocs[i].email,
                    subject: "Conmix - Product Delivered",
                    html: product_partially_delivered_mail_admin_html,
                  }
                  await this.mailgunService.sendEmail(v_data1)  
            }

      let message1 = `Hi ${client_name1}, your product id ${orderItemData.display_item_id} having an Order ID ${orderDoc.display_id} for your site ${site_name1} has been partially delivered by RMC Supplier ${s_vendor_company_name}. - conmix`
      let templateId1 = "1707163775562920482"
      await this.testService.sendSMS(mobile_number1, message1, templateId1)

      let product_partially_delivered_mail_html = client_mail_template.product_partially_delivered
      product_partially_delivered_mail_html = product_partially_delivered_mail_html.replace("{{client_name}}", client_name1)
      product_partially_delivered_mail_html = product_partially_delivered_mail_html.replace("{{orderItemData.display_item_id}}", orderItemData.display_item_id)
      product_partially_delivered_mail_html = product_partially_delivered_mail_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
      product_partially_delivered_mail_html = product_partially_delivered_mail_html.replace("{{site_name}}", site_name1)
      product_partially_delivered_mail_html = product_partially_delivered_mail_html.replace("{{vendor_company_name}}", s_vendor_company_name)
      var data1 = {
            from: "no-reply@conmate.in",
            to: email1,
            subject: "Conmix - Product Delivered",
            html: product_partially_delivered_mail_html,
          }
          await this.mailgunService.sendEmail(data1)

  
      let site_message1 = `Hi ${site_person_name1}, your product id ${orderItemData.display_item_id} having an Order ID ${orderDoc.display_id} for your site ${site_name1} has been partially delivered by RMC Supplier ${s_vendor_company_name}. - conmix.`
      let site_templateId1 = "1707163775562920482"
      await this.testService.sendSMS(
        site_mobile_number1,
        site_message1,
        site_templateId1
      )

      let product_partially_delivered_mail_site_html = client_mail_template.product_partially_delivered
      product_partially_delivered_mail_site_html = product_partially_delivered_mail_site_html.replace("{{client_name}}", site_person_name1)
      product_partially_delivered_mail_site_html = product_partially_delivered_mail_site_html.replace("{{orderItemData.display_item_id}}", orderItemData.display_item_id)
      product_partially_delivered_mail_site_html = product_partially_delivered_mail_site_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
      product_partially_delivered_mail_site_html = product_partially_delivered_mail_site_html.replace("{{site_name}}", site_name1)
      product_partially_delivered_mail_site_html = product_partially_delivered_mail_site_html.replace("{{vendor_company_name}}", s_vendor_company_name)
      var data1 = {
            from: "no-reply@conmate.in",
            to: site_email1,
            subject: "Conmix - Product Delivered",
            html: product_partially_delivered_mail_site_html,
          }
          await this.mailgunService.sendEmail(data1)

      // product partially delivered for vendor.
      let vendor_message = `Hi ${m_vendor_company_name}, you have partially delivered product id ${orderItemData.display_item_id} having an order id ${vendorOrderData._id} for your client ${client_name1} at their ${site_name1} site. - conmix.`
      let vendor_templateId = "1707163842468737420"
      await this.testService.sendSMS(
        m_vendor_mobile_number,
        vendor_message,
        vendor_templateId
      )

      let product_partially_delivered_mail_vendor_html = vendor_mail_template.product_partially_delivered
      product_partially_delivered_mail_vendor_html = product_partially_delivered_mail_vendor_html.replace("{{vendor_company_name}}", m_vendor_company_name)
      product_partially_delivered_mail_vendor_html = product_partially_delivered_mail_vendor_html.replace("{{orderItemData.display_item_id}}", orderItemData.display_item_id)
      product_partially_delivered_mail_vendor_html = product_partially_delivered_mail_vendor_html.replace("{{vendorOrderData._id}}", vendorOrderData._id)
      product_partially_delivered_mail_vendor_html = product_partially_delivered_mail_vendor_html.replace("{{client_name}}", client_name1)
      var m_data1 = {
            from: "no-reply@conmate.in",
            to: m_vendor_email,
            subject: "Conmix - Product Delivered",
            html: product_partially_delivered_mail_vendor_html,
          }
          await this.mailgunService.sendEmail(m_data1)

          let s_vendor_message = `Hi ${s_vendor_company_name}, you have partially delivered product id ${orderItemData.display_item_id} having an order id ${vendorOrderData._id} for your client ${client_name1} at their ${site_name1} site. - conmix.`
          let s_vendor_templateId = "1707163842468737420"
          await this.testService.sendSMS(
            s_vendor_mobile_number,
            s_vendor_message,
            s_vendor_templateId
          )

      let product_partially_delivered_mail_sub_vendor_html = vendor_mail_template.product_partially_delivered
      product_partially_delivered_mail_sub_vendor_html = product_partially_delivered_mail_sub_vendor_html.replace("{{vendor_company_name}}", s_vendor_company_name)
      product_partially_delivered_mail_sub_vendor_html = product_partially_delivered_mail_sub_vendor_html.replace("{{orderItemData.display_item_id}}", orderItemData.display_item_id)
      product_partially_delivered_mail_sub_vendor_html = product_partially_delivered_mail_sub_vendor_html.replace("{{vendorOrderData._id}}", vendorOrderData._id)
      product_partially_delivered_mail_sub_vendor_html = product_partially_delivered_mail_sub_vendor_html.replace("{{client_name}}", client_name1)
      var v_data11 = {
            from: "no-reply@conmate.in",
            to: s_vendor_email,
            subject: "Conmix - Product Delivered",
            html: product_partially_delivered_mail_sub_vendor_html,
          }
          await this.mailgunService.sendEmail(v_data11)    

    
    }

    const doc = await VendorOrderModel.find({
      buyer_order_id: orderTrackDoc.buyer_order_id,
      order_status: { $ne: SupplierOrderStatus.DELIVERED },
    })

    const orderTrackData = await OrderTrackModel.find({
      buyer_order_id: orderTrackDoc.buyer_order_id,
      event_status: { $ne: SupplierOrderStatus.DELIVERED },
    })

    console.log("doc**************", doc)

    console.log("orderTrackData*******************", orderTrackData)

    let orderItemDoc = await OrderItemModel.findOne({
      _id: orderTrackDoc.order_item_id,
    })

    if (isNullOrUndefined(orderItemDoc)) {
      return Promise.reject(new InvalidInput(`No Item Found.`, 400))
    }

    console.log("orderItemDoc", orderItemDoc)

    if (doc.length < 1 && orderTrackData.length < 1) {
      //   update buyer order status.
      const orderData = await OrderModel.findOneAndUpdate(
        {
          _id: orderTrackDoc.buyer_order_id,
        },
        {
          $set: {
            order_status: SupplierOrderStatus.DELIVERED,
            delivered_at: Date.now(),
          },
        }
      )
      if (isNullOrUndefined(orderData)) {
        return Promise.reject(new InvalidInput(`No data was found `, 400))
      }

      let orderItemData1 = await OrderItemModel.findOneAndUpdate(
        { _id: orderTrackDoc.order_item_id },
        {
          $set: {
            item_status: SupplierOrderStatus.DELIVERED,
            delivered_at: Date.now(),
          },
        }
      )

      if (isNullOrUndefined(orderItemData1)) {
        return Promise.reject(new InvalidInput(`No Item Found.`, 400))
      }

      console.log("orderItemData1", orderItemData1)

      vendorOrderDoc = await VendorOrderModel.findOne({
        buyer_order_item_id: orderTrackDoc.order_item_id,
        // user_id: orderTrackDoc.vendor_user_id,
      })

      console.log("vendorOrderDoc", vendorOrderDoc)
      let buyerUserData = await BuyerUserModel.findOne({
        _id: new ObjectId(vendorOrderDoc.buyer_id),
      })
      let client_name: any
      let mobile_number: any
      let email: any
      if (!isNullOrUndefined(buyerUserData)) {
        client_name = buyerUserData.full_name
        mobile_number = buyerUserData.mobile_number
        email = buyerUserData.email
      }

      let vendorUserData = await VendorUserModel.findOne({
        _id: new ObjectId(vendor_id),
      })
      //let vendor_name: any
      let vendor_company_name: any
      if (!isNullOrUndefined(vendorUserData)) {
        //vendor_name = vendorUserData.full_name
        vendor_company_name = vendorUserData.company_name
      }

      let siteData = await SiteModel.findOne({
        _id: new ObjectId(orderDoc.site_id),
      })
      let site_person_name: any
      let site_mobile_number: any
      let site_email: any
      if (!isNullOrUndefined(siteData)) {
        site_person_name = siteData.person_name
        site_mobile_number = siteData.mobile_number
        site_email = siteData.email
      }

      console.log("Fully order delivered")
    

    
      let message = `Hi ${client_name}, your order having an Order ID ${orderDoc.display_id} has been fully delivered and completed by RMC Supplier ${vendor_company_name}. - conmix`
      let templateId = "1707163775558291275"
      await this.testService.sendSMS(mobile_number, message, templateId)

      let order_fully_delivered_mail_html = client_mail_template.order_fully_delivered
      order_fully_delivered_mail_html = order_fully_delivered_mail_html.replace("{{client_name}}", client_name)
      order_fully_delivered_mail_html = order_fully_delivered_mail_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
        var data1 = {
            from: "no-reply@conmate.in",
            to: email,
            subject: "Conmix - Order Delivered",
            html: order_fully_delivered_mail_html,
          }
          await this.mailgunService.sendEmail(data1)
      
   
      let site_message = `Hi ${site_person_name}, your order having an Order ID ${orderDoc.display_id} has been fully delivered and completed by RMC Supplier ${vendor_company_name}. - conmix`
      let site_templateId = "1707163775558291275"
      await this.testService.sendSMS(
        site_mobile_number,
        site_message,
        site_templateId
      )

      let order_fully_delivered_mail_site_html = client_mail_template.order_fully_delivered
      order_fully_delivered_mail_site_html = order_fully_delivered_mail_site_html.replace("{{client_name}}", site_person_name)
      order_fully_delivered_mail_site_html = order_fully_delivered_mail_site_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
        var data1 = {
            from: "no-reply@conmate.in",
            to: site_email,
            subject: "Conmix - Order Delivered",
            html: order_fully_delivered_mail_site_html,
          }
          await this.mailgunService.sendEmail(data1)

        

      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: orderDoc.user_id,
        notification_type: ClientNotificationType.orderDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItemDoc.display_item_id,
        vendor_id,
      })

            // Check Admin Type.
            const adminQuery: { [k: string]: any } = {}
            adminQuery.$or = [
              {
                admin_type : AdminRoles.admin_manager
              },
              {
                admin_type : AdminRoles.admin_sales
              },
              {
                admin_type : AdminRoles.superAdmin
              },
            ]
            const adminUserDocs = await AdminUserModel.find(adminQuery)
            if (adminUserDocs.length < 0) {
              return Promise.reject(
                new InvalidInput(`No Admin data were found `, 400)
              )
            }
      
      for(let i = 0 ; i < adminUserDocs.length; i++){
      await this.notUtil.addNotificationForAdmin({
        to_user_type: UserType.ADMIN,
        to_user_id: adminUserDocs[i]._id,
        notification_type: AdminNotificationType.orderDelivered,
        order_id: orderTrackDoc.buyer_order_id,
        order_item_id: orderItemDoc.display_item_id,
        vendor_id,
        client_id: orderDoc.user_id,
      })

      let order_fully_delivered_mail_admin_html = admin_mail_template.order_fully_delivered
      order_fully_delivered_mail_admin_html = order_fully_delivered_mail_admin_html.replace("{{adminUserDoc.full_name}}", adminUserDocs[i].full_name)
      order_fully_delivered_mail_admin_html = order_fully_delivered_mail_admin_html.replace("{{client_name}}", client_name)
      order_fully_delivered_mail_admin_html = order_fully_delivered_mail_admin_html.replace("{{orderDoc.display_id}}", orderDoc.display_id)
        var data11 = {
            from: "no-reply@conmate.in",
            to: adminUserDocs[i].email,
            subject: "Conmix - Order Delivered",
            html: order_fully_delivered_mail_admin_html,
          }
          await this.mailgunService.sendEmail(data11)   
    }
    }

    console.log("orderItemDoc", orderItemDoc)
    // For cement
    if (!isNullOrUndefined(orderItemDoc.cement_quantity)) {
      let cement_total_qty: any
      let cement_existing_qty: any
      cement_total_qty = orderItemDoc.cement_quantity * orderItemDoc.quantity
      let cqData = await CementQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        brand_id: orderItemDoc.cement_brand_id,
        grade_id: orderItemDoc.cement_grade_id,
      })
      console.log("cqData", cqData)
      if (!isNullOrUndefined(cqData)) {
        cement_existing_qty = cqData.quantity

        let cement_final_qty = cement_existing_qty - cement_total_qty
        console.log("cement_existing_qty", cement_existing_qty)
        console.log("cement_total_qty", cement_total_qty)
        console.log("cement_final_qty", cement_final_qty)

        if (cement_final_qty < 0) {
          cement_final_qty = 0
        }
        await CementQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            brand_id: orderItemDoc.cement_brand_id,
            grade_id: orderItemDoc.cement_grade_id,
          },
          {
            $set: {
              quantity: cement_final_qty,
            },
          }
        )
      }
    }
    // For Sand
    if (!isNullOrUndefined(orderItemDoc.sand_quantity)) {
      let sand_total_qty: any
      let sand_existing_qty: any
      sand_total_qty = orderItemDoc.sand_quantity * orderItemDoc.quantity
      let sqData = await SandSourceQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        source_id: orderItemDoc.sand_source_id,
      })
      if (!isNullOrUndefined(sqData)) {
        sand_existing_qty = sqData.quantity

        let sand_final_qty = sand_existing_qty - sand_total_qty

        if (sand_final_qty < 0) {
          sand_final_qty = 0
        }

        await SandSourceQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            source_id: orderItemDoc.sand_source_id,
          },
          {
            $set: {
              quantity: sand_final_qty,
            },
          }
        )
      }
    }
    // For aggregate1
    if (!isNullOrUndefined(orderItemDoc.aggregate1_quantity)) {
      let agg1_total_qty: any
      let agg1_existing_qty: any
      agg1_total_qty = orderItemDoc.aggregate1_quantity * orderItemDoc.quantity
      console.log("agg1_total_qty", agg1_total_qty)
      let agg1Data = await AggregateSourceQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        sub_cat_id: orderItemDoc.aggregate1_sub_cat_id,
        source_id: orderItemDoc.agg_source_id,
      })
      console.log("agg1Data", agg1Data)
      if (!isNullOrUndefined(agg1Data)) {
        agg1_existing_qty = agg1Data.quantity

        let agg1_final_qty = agg1_existing_qty - agg1_total_qty

        if (agg1_final_qty < 0) {
          agg1_final_qty = 0
        }

        await AggregateSourceQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            sub_cat_id: orderItemDoc.aggregate1_sub_cat_id,
            source_id: orderItemDoc.agg_source_id,
          },
          {
            $set: {
              quantity: agg1_final_qty,
            },
          }
        )
      }
    }

    // For aggregate2
    if (!isNullOrUndefined(orderItemDoc.aggregate2_quantity)) {
      let agg2_total_qty: any
      let agg2_existing_qty: any
      agg2_total_qty = orderItemDoc.aggregate2_quantity * orderItemDoc.quantity
      let agg2Data = await AggregateSourceQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        sub_cat_id: orderItemDoc.aggregate2_sub_cat_id,
        source_id: orderItemDoc.agg_source_id,
      })
      console.log("agg2Data", agg2Data)
      if (!isNullOrUndefined(agg2Data)) {
        agg2_existing_qty = agg2Data.quantity

        let agg2_final_qty = agg2_existing_qty - agg2_total_qty

        if (agg2_final_qty < 0) {
          agg2_final_qty = 0
        }

        await AggregateSourceQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            sub_cat_id: orderItemDoc.aggregate2_sub_cat_id,
            source_id: orderItemDoc.agg_source_id,
          },
          {
            $set: {
              quantity: agg2_final_qty,
            },
          }
        )
      }
    }
    // For admix
    if (
      !isNullOrUndefined(orderItemDoc.admix_quantity) &&
      !isNullOrUndefined(orderItemDoc.ad_mixture1_category_id) &&
      !isNullOrUndefined(orderItemDoc.ad_mixture1_brand_id)
    ) {
      let admix1_total_qty: any
      let admix1_existing_qty: any
      admix1_total_qty = orderItemDoc.admix_quantity * orderItemDoc.quantity
      let admixData = await AdmixtureQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        brand_id: orderItemDoc.ad_mixture1_brand_id,
        category_id: orderItemDoc.ad_mixture1_category_id,
      })
      if (!isNullOrUndefined(admixData)) {
        admix1_existing_qty = admixData.quantity

        let admix1_final_qty = admix1_existing_qty - admix1_total_qty

        if (admix1_final_qty < 0) {
          admix1_final_qty = 0
        }

        await AdmixtureQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            brand_id: orderItemDoc.ad_mixture1_brand_id,
            category_id: orderItemDoc.ad_mixture1_category_id,
          },
          {
            $set: {
              quantity: admix1_final_qty,
            },
          }
        )
      }
    }

    if (
      !isNullOrUndefined(orderItemDoc.admix_quantity) &&
      !isNullOrUndefined(orderItemDoc.ad_mixture2_category_id) &&
      !isNullOrUndefined(orderItemDoc.ad_mixture2_brand_id)
    ) {
      let admix2_total_qty: any
      let admix2_existing_qty: any
      admix2_total_qty = orderItemDoc.admix_quantity * orderItemDoc.quantity
      let admixData = await AdmixtureQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        brand_id: orderItemDoc.ad_mixture2_brand_id,
        category_id: orderItemDoc.ad_mixture2_category_id,
      })
      if (!isNullOrUndefined(admixData)) {
        admix2_existing_qty = admixData.quantity

        let admix2_final_qty = admix2_existing_qty - admix2_total_qty

        if (admix2_final_qty < 0) {
          admix2_final_qty = 0
        }

        await AdmixtureQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            brand_id: orderItemDoc.ad_mixture2_brand_id,
            category_id: orderItemDoc.ad_mixture2_category_id,
          },
          {
            $set: {
              quantity: admix2_final_qty,
            },
          }
        )
      }
    }

    // For flyAsh.
    if (!isNullOrUndefined(orderItemDoc.fly_ash_quantity)) {
      let fly_ash_total_qty: any
      let fly_ash_existing_qty: any
      fly_ash_total_qty = orderItemDoc.fly_ash_quantity * orderItemDoc.quantity
      let flyAshData = await FlyAshSourceQuantityModel.findOne({
        address_id: orderItemDoc.address_id,
        sub_vendor_id: orderItemDoc.sub_vendor_id,
        source_id: orderItemDoc.fly_ash_source_id,
      })
      if (!isNullOrUndefined(flyAshData)) {
        fly_ash_existing_qty = flyAshData.quantity

        let fly_ash_final_qty = fly_ash_existing_qty - fly_ash_total_qty

        if (fly_ash_final_qty < 0) {
          fly_ash_final_qty = 0
        }

        await FlyAshSourceQuantityModel.findOneAndUpdate(
          {
            address_id: orderItemDoc.address_id,
            sub_vendor_id: orderItemDoc.sub_vendor_id,
            source_id: orderItemDoc.fly_ash_source_id,
          },
          {
            $set: {
              quantity: fly_ash_final_qty,
            },
          }
        )
      }
    }

    return Promise.resolve()
    // // send sms to supplier.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_SMS_USING_TEMPLATE,
    //   event_data: {
    //     to: supplierUserDoc.mobile_number,
    //     template_type: "after_deliver_success_sms_to_supplier",
    //     templateData: {
    //       vehicle_rc_number: vehicleDoc.vehicle_rc_number,
    //       vehicle_sub_category: vehiclesubCategoryDoc.sub_category_name,
    //       // driver_name: vehicleDoc.driver_name,
    //       // driver_mobile_number: vehicleDoc.driver_mobile_number,
    //       buyer_name: buyerUserDoc.full_name,
    //       buyer_mobile_number: buyerUserDoc.mobile_number,
    //       buyer_order_id: buyerOrderDoc._id,
    //     },
    //   },
    // })
    // // send sms to logistics.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_SMS_USING_TEMPLATE,
    //   event_data: {
    //     to: logisticsUserDoc.mobile_number,
    //     template_type: "after_deliver_sms_to_logistics",
    //     templateData: {
    //       supplier_name: supplierUserDoc.full_name,
    //       supplier_mobile_number: supplierUserDoc.mobile_number,
    //       buyer_name: buyerUserDoc.full_name,
    //       buyer_mobile_number: buyerUserDoc.mobile_number,
    //       buyer_order_id: buyerOrderDoc._id,
    //       vehicle_rc_number: vehicleDoc.vehicle_rc_number,
    //       vehicle_sub_category: vehiclesubCategoryDoc.sub_category_name,
    //       // driver_name: vehicleDoc.driver_name,
    //       // driver_mobile_number: vehicleDoc.driver_mobile_number,
    //     },
    //   },
    // })
    // // send sms to admin.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_SMS_USING_TEMPLATE,
    //   event_data: {
    //     to: adminEmails[0].mobile_number,
    //     template_type: "after_deliver_sms_to_admin",
    //     templateData: {
    //       supplier_name: supplierUserDoc.full_name,
    //       supplier_mobile_number: supplierUserDoc.mobile_number,
    //       buyer_name: buyerUserDoc.full_name,
    //       buyer_mobile_number: buyerUserDoc.mobile_number,
    //       buyer_order_id: buyerOrderDoc._id,
    //       vehicle_rc_number: vehicleDoc.vehicle_rc_number,
    //       vehicle_sub_category: vehiclesubCategoryDoc.sub_category_name,
    //       // driver_name: vehicleDoc.driver_name,
    //       // driver_mobile_number: vehicleDoc.driver_mobile_number,
    //     },
    //   },
    // })
    // // send sms to buyer.
    // await this.snsService.publishMessage({
    //   event_type: EventType.SEND_SMS_USING_TEMPLATE,
    //   event_data: {
    //     to: buyerUserDoc.mobile_number,
    //     template_type: "after_deliver_sms_to_buyer",
    //     templateData: {
    //       supplier_name: supplierUserDoc.full_name,
    //       supplier_mobile_number: supplierUserDoc.mobile_number,
    //       buyer_name: buyerUserDoc.full_name,
    //       buyer_order_id: buyerOrderDoc._id,
    //       vehicle_rc_number: vehicleDoc.vehicle_rc_number,
    //       vehicle_sub_category: vehiclesubCategoryDoc.sub_category_name,
    //       // driver_name: vehicleDoc.driver_name,
    //       // driver_mobile_number: vehicleDoc.driver_mobile_number,
    //     },
    //   },
    // })
  }

  async CPorderDelivered(
    vendor_id: string,
    CP_id: string,
    vendor_order_id: string,
    track_id: string
  ) {
    // update order track status.
    let orderTrackDoc = await CPOrderTrackModel.findOne({
      _id: new ObjectId(track_id),
      vendor_order_id,
      // vendor_user_id: new ObjectId(vendor_id),
      CP_id: new ObjectId(CP_id),
    })

    if (isNullOrUndefined(orderTrackDoc)) {
      return Promise.reject(
        new InvalidInput(
          `Sorry, we could not find any order with these details For Tracking.`,
          400
        )
      )
    }
    await CPOrderTrackModel.updateOne(
      {
        _id: new ObjectId(track_id),
        vendor_order_id,
        // vendor_user_id: new ObjectId(vendor_id),
        CP_id: new ObjectId(CP_id),
      },
      {
        $set: {
          event_status: SupplierOrderStatus.DELIVERED,
          delivered_at: Date.now(),
        },
      }
    )

    let orderItemDoc = await OrderItemModel.findOne({
      _id: orderTrackDoc.order_item_id,
    })

    if (isNullOrUndefined(orderItemDoc)) {
      return Promise.reject(new InvalidInput(`No Item found`, 400))
    }

    const adminUserDoc = await AdminUserModel.findOne({
      admin_type: "superAdmin",
    })

    if (isNullOrUndefined(adminUserDoc)) {
      return Promise.reject(new InvalidInput(`No Admin data were found `, 400))
    }

    const orderDoc = await OrderModel.findOne({
      _id: orderItemDoc.order_id,
    })

    if (isNullOrUndefined(orderDoc)) {
      return Promise.reject(new InvalidInput(`No Order Data found.`, 400))
    }

    await this.notUtil.addNotificationForBuyer({
      to_user_type: UserType.BUYER,
      to_user_id: orderItemDoc.buyer_id,
      notification_type: ClientNotificationType.CPDelivered,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      vendor_id,
      site_id: orderDoc.site_id,
    })

    let buyer_order_display_id: any
    let buyerOrderDoc = await OrderModel.findOne({
      _id: new ObjectId(orderTrackDoc.buyer_order_id),
    })
    if (!isNullOrUndefined(buyerOrderDoc)) {
      buyer_order_display_id = buyerOrderDoc.display_id
    }

    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let siteInfo = await SiteModel.findOne({
      _id: new ObjectId(orderDoc.site_id),
    })
    if (!isNullOrUndefined(siteInfo)) {
      site_name = siteInfo.site_name
      site_person_name = siteInfo.person_name
      site_mobile_number = siteInfo.mobile_number
    }
    let vendor_name: any
    let vendor_mobile_number: any
    let vendor_company_name: any
    let vendorInfo = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    if (!isNullOrUndefined(vendorInfo)) {
      vendor_name = vendorInfo.full_name
      vendor_mobile_number = vendorInfo.mobile_number
      vendor_company_name = vendorInfo.company_name
    }
    //let message = `Hi ${site_person_name}, CP has been delivered by RMC supplier ${vendor_name} at your ${site_name} site for your product id ${orderItemDoc.display_item_id} having an order id ${orderTrackDoc.buyer_order_id}.`
    let message = `Hi ${site_person_name}, CP has been delivered by RMC supplier ${vendor_name} at your ${site_name} site for your product id ${orderItemDoc.display_item_id} having an order id ${buyer_order_display_id}. - conmix`
    let templateId = "1707163775573913864"
    await this.testService.sendSMS(site_mobile_number, message, templateId)

    let vendor_message = `Hi ${vendor_company_name}, you have delivered the CP at ${site_name} site for your client ${site_person_name} for the product id ${orderItemDoc.display_item_id} having an order id ${vendor_order_id} - conmix.`
    let vendor_templateId = "1707163810433656131"
    await this.testService.sendSMS(
      vendor_mobile_number,
      vendor_message,
      vendor_templateId
    )

    //  await this.notUtil.addNotificationForAdmin({
    //   to_user_type: UserType.ADMIN,
    //   to_user_id: adminUserDoc._id,
    //   notification_type: AdminNotificationType.orderDelivered,
    //   order_id: orderTrackDoc.buyer_order_id,
    //   order_item_id: orderItemDoc.display_item_id,
    //   vendor_id,
    //   client_id :orderItemDoc.buyer_id,
    // })

    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderTrackDoc.master_vendor_id,
      notification_type: VendorNotificationType.CPDelivered,
      vendor_order_id,
      vendor_id,
      client_id: orderItemDoc.buyer_id,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      site_id: orderDoc.site_id,
    })

    // Send notofication to Supplier.
    await this.notUtil.addNotificationForSupplier({
      to_user_type: UserType.VENDOR,
      to_user_id: orderTrackDoc.sub_vendor_id,
      notification_type: VendorNotificationType.CPDelivered,
      vendor_order_id,
      vendor_id,
      client_id: orderItemDoc.buyer_id,
      order_id: orderTrackDoc.buyer_order_id,
      order_item_id: orderItemDoc.display_item_id,
      site_id: orderDoc.site_id,
    })

    return Promise.resolve()
  }

  async getVendorOrderTrack(order_item_id: string, tracking_id: string) {
    const query: { [k: string]: any } = {
      order_item_id: new ObjectId(order_item_id),
      _id: new ObjectId(tracking_id),
    }

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "TM",
          localField: "TM_id",
          foreignField: "_id",
          as: "TMDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TMDetails.TM_category_id",
          foreignField: "_id",
          as: "TMCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TMDetails.TM_sub_category_id",
          foreignField: "_id",
          as: "TMSubCategoryDetails",
        },
      },
      {
        $unwind: {
          path: "$TMSubCategoryDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "TMDetails.driver1_id",
          foreignField: "_id",
          as: "TMDriverDetails",
        },
      },
      {
        $unwind: {
          path: "$TMDriverDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "design_mix_variant",
          localField: "orderItemDetails.design_mix_id",
          foreignField: "_id",
          as: "designMixDetails",
        },
      },
      {
        $unwind: {
          path: "$designMixDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "orderItemDetails.concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: { path: "$concrete_grade", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "orderItemDetails.cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: { path: "$cement_brand", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "orderItemDetails.sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: { path: "$sand_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "aggregate_source",
          localField: "orderItemDetails.agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "orderItemDetails.aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "orderItemDetails.aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sand_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sand_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "orderItemDetails.fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "admixture_brand",
          localField: "orderItemDetails.admix_brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: { path: "$admixture_brand", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "cement_grade",
          localField: "orderItemDetails.cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "orderItemDetails.admix_cat_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          buyer_order_id: true,
          order_item_id: true,
          TM_id: true,
          pickup_quantity: true,
          royality_quantity: true,
          reasonForDelay: true,
          delayTime: true,
          event_status: true,
          assigned_at: true,
          pickedup_at: true,
          delivered_at: true,
          delayed_at: true,
          designMixDetails: true,
          vendor_order_id: "$vendorOrderDetails._id",
          design_mix_id: "$orderItemDetails.design_mix_id",
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          processed_at: "$order.processed_at",
          // TM_rc_number: "$TMDetails.TM_rc_number",
          TM_category: "$TMCategoryDetails.category_name",
          TM_sub_category: "$TMSubCategoryDetails.sub_category_name",
          driver_name: "$TMDriverDetails.driver_name",
          driver_mobile_number: "$TMDriverDetails.driver_mobile_number",
          concrete_grade_name: "$concrete_grade.name",
          cement_brand_name: "$cement_brand.name",
          sand_source_name: "$sand_source.sand_source_name",
          aggregate_source_name: "$aggregate_source.aggregate_source_name",
          aggregate1_sub_category_name:
            "$aggregate1_sand_category.sub_category_name",
          aggregate2_sub_category_name:
            "$aggregate2_sand_category.sub_category_name",
          fly_ash_source_name: "$fly_ash_source.fly_ash_source_name",
          admixture_brand_name: "$admixture_brand.name",
          cement_grade_name: "$cement_grade.name",
          admixture_category_name: "$admixture_category.category_name",
          TM_rc_number: true,
          TM_category_name: true,
          TM_sub_category_name: true,
          TM_driver1_name: true,
          TM_driver2_name: true,
          TM_driver1_mobile_number: true,
          TM_driver2_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]

    const orderTrackData = await OrderTrackModel.aggregate(aggregateArr)
    if (orderTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(
          `Your request for tracking is invalid as we couldn't find Order item`,
          400
        )
      )
    }

    return Promise.resolve(orderTrackData[0])
  }
  async getVendorOrderCPTrack(order_item_part_id: string) {
    const query: { [k: string]: any } = {
      order_item_part_id: new ObjectId(order_item_part_id),
    }
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_id",
          foreignField: "_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "order_item_id",
          foreignField: "_id",
          as: "orderItemDetails",
        },
      },
      {
        $unwind: {
          path: "$orderItemDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor_order",
          localField: "vendor_order_id",
          foreignField: "_id",
          as: "vendorOrderDetails",
        },
      },
      {
        $unwind: {
          path: "$vendorOrderDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorOrderDetails.address_id",
          foreignField: "_id",
          as: "pickup_address_info",
        },
      },
      {
        $unwind: {
          path: "$pickup_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "pickup_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "delivery_address_info",
        },
      },
      {
        $unwind: {
          path: "$delivery_address_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "delivery_address_info.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "concrete_pump",
          localField: "CP_id",
          foreignField: "_id",
          as: "CPDetails",
        },
      },
      {
        $unwind: {
          path: "$CPDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "operator_info",
          localField: "CPDetails.operator_id",
          foreignField: "_id",
          as: "CPOperatorDetails",
        },
      },
      {
        $unwind: {
          path: "$CPOperatorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          buyer_order_id: true,
          order_item_id: true,
          CP_id: true,
          reasonForDelay: true,
          delayTime: true,
          event_status: true,
          assigned_at: true,
          pickedup_at: true,
          delivered_at: true,
          delayed_at: true,
          display_id: "$order.display_id",
          created_at: "$order.created_at",
          order_status: "$order.order_status",
          processed_at: "$order.processed_at",
          CP_company_name: "$CPDetails.CP_company_name",
          concrete_pump_model: "$CPDetails.concrete_pump_model",
          manufacture_year: "$CPDetails.manufacture_year",
          pipe_connection: "$CPDetails.pipe_connection",
          concrete_pump_capacity: "$CPDetails.concrete_pump_capacity",
          is_Operator: "$CPDetails.is_Operator",
          operator_name: "$CPOperatorDetails.operator_name",
          operator_mobile_number: "$CPOperatorDetails.operator_mobile_number",
          CP_serial_number: true,
          CP_category_name: true,
          CP_operator_name: true,
          CP_operator_mobile_number: true,
          sub_vendor_id: true,
          master_vendor_id: true,
        },
      },
    ]

    const orderCPTrackData = await CPOrderTrackModel.aggregate(aggregateArr)
    if (orderCPTrackData.length < 0) {
      return Promise.reject(
        new InvalidInput(
          `Your request for tracking is invalid as we couldn't find Order item`,
          400
        )
      )
    }

    return Promise.resolve(orderCPTrackData[0])
  }

  // async getPDF() {
  //   console.log("Loading PDF in memory")
  //   try {
  //     const invoicePath = path.resolve("./latest_invoice.pdf")
  //     return await readFile(invoicePath)
  //   } catch (err) {
  //     return Promise.reject("Could not load PDF")
  //   }
  // }

  async uploadInvoice(vendor_id: string, data: IUploadInvoice) {
    console.log("vendor_id", vendor_id)
    let res = await this.commonService.generatePdf(data.order_id, data.track_id)
    let filename = res.Invoice_number

    console.log("filename", filename)
    const fileContent = fs.readFileSync(`./${filename}.pdf`)
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/order_track`
    let objectName =
      "" + "Invoice" + "_" + data.order_id + "_" + res.Invoice_number + ".pdf"

    const objectPath = `${objectDir}/${objectName}`
    await putObject(
      awsConfig.s3MediaBucket,
      objectPath,
      fileContent,
      fileContent.mimetype
    )
    let invoice_url = getS3MediaURL(`${objectDir}/${objectName}`)
    return await OrderTrackModel.updateOne(
      {
        _id: new ObjectId(data.track_id),
        vendor_order_id: data.order_id,
        // vendor_user_id: new ObjectId(vendor_id),
        TM_id: data.TM_id,
      },
      {
        $set: {
          invoice_url,
        },
      }
    )
    // console.log("invoice_url", invoice_url)
  }

  async uploadVendorInvoice(vendor_id: string, data: IUploadVendorInvoice) {
    console.log("vendor_id", vendor_id)
    let res = await this.commonService.generateVendorPdf(
      data.order_id,
      data.track_id
    )

    let filename = res.Invoice_number

    console.log("filename", filename)
    const fileContent = fs.readFileSync(`./${filename}.pdf`)
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/order_track`
    let objectName =
      "" + "Invoice" + "_" + data.order_id + "_" + res.Invoice_number + ".pdf"

    const objectPath = `${objectDir}/${objectName}`
    await putObject(
      awsConfig.s3MediaBucket,
      objectPath,
      fileContent,
      fileContent.mimetype
    )
    let vendor_invoice_url = getS3MediaURL(`${objectDir}/${objectName}`)
    return await OrderTrackModel.updateOne(
      {
        _id: new ObjectId(data.track_id),
        vendor_order_id: data.order_id,
        //  vendor_user_id: new ObjectId(vendor_id),
        TM_id: data.TM_id,
      },
      {
        $set: {
          vendor_invoice_url,
        },
      }
    )
  }
}
