import * as Mongoose from "mongoose"
import {
  NotificationModel,
  VendorNotificationType,
} from "../../model/notification.model"
import { injectable, inject } from "inversify"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import { VendorTypes } from "../vendor.types"
import { VendorUserModel } from "../../model/vendor.user.model"

export interface INewNotification {
  to_user_id: string | Mongoose.Types.ObjectId
  notification_type: VendorNotificationType
  from_user_type?: string
  to_user_type?: string
  data?: { [k: string]: any }
  [k: string]: any
}

@injectable()
export class NotificationRepository {
  constructor(
    @inject(VendorTypes.NotificationUtility)
    private notUtils: NotificationUtility
  ) {}

  async getNotifications(
    user_id: string,
    before?: string,
    after?: string,
    max_notification_type?: number
  ) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(max_notification_type)) {
      query.notification_type = { $lte: max_notification_type }
    }
    //else {
    //   query.notification_type = { $lte: 8 }
    // }
    console.log("user_id", user_id)
    query.to_user_id = new ObjectId(user_id)
    let sort = {
      created_at: -1,
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }
    //const requesterUid = new ObjectId(user_id)
    await VendorUserModel.updateOne(
      { _id: user_id },
      { $set: { unseen_message_count: 0 } }
    )

    console.log(query)
    const notifications = await NotificationModel.aggregate()
      .match(query)
      .sort(sort)
      .limit(10)
      .lookup({
        from: "vendor",
        localField: "to_user_id",
        foreignField: "_id",
        as: "to_user",
      })
      .lookup({
        from: "vendor_order",
        localField: "vendor_order_id",
        foreignField: "_id",
        as: "vendor_order",
      })
      .lookup({
        from: "order",
        localField: "order_id",
        foreignField: "_id",
        as: "order",
      })
      .lookup({
        from: "order_track",
        localField: "item_id",
        foreignField: "order_item_id",
        as: "order_track",
      })
      .lookup({
        from: "site",
        localField: "site_id",
        foreignField: "_id",
        as: "site",
      })
      .lookup({
        from: "CP_order_track",
        localField: "item_id",
        foreignField: "order_item_id",
        as: "CP_order_track",
      })
      .lookup({
        from: "buyer",
        localField: "client_id",
        foreignField: "_id",
        as: "client",
      })
      .lookup({
        from: "order_item",
        localField: "order_item_id",
        foreignField: "display_item_id",
        as: "order_item",
      })
      .lookup({
        from: "support_ticket",
        localField: "ticket_id",
        foreignField: "_id",
        as: "support_ticket",
      })
      // .lookup({
      //   from: "order_item",
      //   localField: "order_item_id",
      //   foreignField: "_id",
      //   as: "order_item",
      // })
      // .lookup({
      //   from: "post",
      //   localField: "post_id",
      //   foreignField: "_id",
      //   as: "post",
      // })
      // .lookup({
      //   from: "comments",
      //   localField: "comment_id",
      //   foreignField: "_id",
      //   as: "comments",
      // })
      // .lookup({
      //   from: "meetup",
      //   localField: "meetup_id",
      //   foreignField: "_id",
      //   as: "meetup",
      // })
      .unwind({
        path: "$to_user",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$vendor_order",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$order",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$order_track",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$site",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$client",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$order_item",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$CP_order_track",
        preserveNullAndEmptyArrays: true,
      })
      .unwind({
        path: "$support_ticket",
        preserveNullAndEmptyArrays: true,
      })
      // .unwind({
      //   path: "$order_item",
      //   preserveNullAndEmptyArrays: true,
      // })
      // .unwind({
      //   path: "$post",
      //   preserveNullAndEmptyArrays: true,
      // })
      // .unwind({
      //   path: "$comments",
      //   preserveNullAndEmptyArrays: true,
      // })
      // .unwind({
      //   path: "$meetup",
      //   preserveNullAndEmptyArrays: true,
      // })
      // .lookup({
      //   from: "user_follow",
      //   let: {
      //     requesterUid,
      //     user_id: "$from_user._id",
      //   },
      //   pipeline: [
      //     {
      //       $match: {
      //         $expr: {
      //           $and: [
      //             {
      //               $eq: ["$from_id", "$$requesterUid"],
      //             },
      //             {
      //               $eq: ["$to_id", "$$user_id"],
      //             },
      //           ],
      //         },
      //       },
      //     },
      //   ],
      //   as: "following",
      // })
      // .addFields({
      //   "from_user.is_following": { $gt: [{ $size: "$following" }, 0] },
      // })
      .project({
        notification_type: 1,
        created_at: 1,
        seen_on: 1,
        like_weight: 1,
        assigned_quantity: 1,
        // "from_user._id": 1,
        // "from_user.full_name": 1,
        // "from_user.handle_name": 1,
        // "from_user.thumbnail": 1,
        // "from_user.is_following": 1,
        "to_user._id": 1,
        "to_user.full_name": 1,
        "vendor_order._id": 1,
        "order_item._id": 1,
        "order_item.display_item_id": 1,
        "order._id": 1,
        "order.display_id": 1,
        "order_track._id": 1,
        "order_track.delayTime": 1,
        "order_track.reasonForDelay": 1,
        "site._id": 1,
        "site.site_name": 1,
        "client._id": 1,
        "client.account_type": 1,
        "client.company_name": 1,
        "client.full_name": 1,
        "CP_order_track._id": 1,
        "CP_order_track.delayTime": 1,
        "CP_order_track.reasonForDelay": 1,
        "support_ticket._id" : 1,
        "support_ticket.ticket_id" : 1,
        // "trail._id": 1,
        // "trail.collage_url": 1,
        // "trail.title": 1,

        // "post._id": 1,
        // "post.trail_id": 1,
        // "post.info": 1,
        // "post.media_type": 1,
        // "post.thumbnail": 1,
        // "post.mini_thumbnail": 1,
        // comment_id: 1,
        // comments: 1,
        // meetup: 1,
      })
    if (sort.created_at === 1) {
      notifications.reverse()
    }
    return Promise.resolve(notifications)
  }
  async setNotificationAsSeen(to_user_id: string, notification_id: string) {
    return NotificationModel.updateOne(
      { _id: notification_id, to_user_id },
      { $set: { seen_on: Date.now() } }
    )
  }

  async getNotificationCount(
    user_id: string
  ): Promise<{ unseen_message_count: number; notification_count: number }> {
    const userDoc = await VendorUserModel.findById(user_id, {
      notification_count: 1,
      unseen_message_count: 1,
    })
    if (userDoc === null) {
      return Promise.resolve({ unseen_message_count: 0, notification_count: 0 })
    }
    return Promise.resolve(userDoc)
  }

  async addNotification(notificationObj: INewNotification): Promise<any> {
    console.log("notificationObj", notificationObj)
    if (isNullOrUndefined(notificationObj.to_user_id)) {
      return Promise.resolve()
    }
    return Promise.all([
      BuyerUserModel.updateOne(
        { _id: notificationObj.to_user_id },
        { $inc: { notification_count: 1 } }
      ),
      new NotificationModel(notificationObj).save(),
      this.sendNotificationToSns(notificationObj),
    ])
  }

  async sendNotificationToSns(notificationObj: INewNotification): Promise<any> {
    console.log("sendddddddddddddd")
    const userData = await BuyerUserModel.findById(notificationObj.to_user_id, {
      notification_opted: 1,
      full_name: 1,
    })
    if (userData === null || userData.notification_opted === false) {
      return Promise.resolve() // User has not opted notification
    }
    let message = ""

    // const senderData = await BuyerUserModel.findById(
    //   notificationObj.to_user_id,
    //   {
    //     full_name: 1,
    //   }
    // )

    // console.log("senderData", senderData)
    // switch (notificationObj.notification_type) {
    //   case NotificationType.orderConfirmForBuyer:
    //     if (senderData === null) {
    //       return Promise.resolve()
    //     }
    //     message = `${senderData.full_name} your order has been booked.`
    //     break
    // }

    console.log("message******************", message)
    if (message === "") {
      console.log(`message is empty.`)
      return Promise.resolve()
    }
    return this.notUtils.send(message, notificationObj)
  }

  async setAllNotificationsAsSeen(user_id: string) {
    return NotificationModel.updateMany(
      { to_user_id: new ObjectId(user_id), seen_on: { $exists: false } },
      { $set: { seen_on: Date.now() } }
    )
  }
}
