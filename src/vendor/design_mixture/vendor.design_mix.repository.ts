import { AddressModel } from "./../../model/address.model"
import { SandSourceModel } from "./../../model/sand_source.model"
import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import {
  IAddDesignMix,
  IGetDesignMixture,
  IEditDesignMix,
  IEditDesignMixVariant,
  IAddDesignMixVariant,
} from "./vendor.design_mix.req-schema"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { VendorUserModel } from "../../model/vendor.user.model"
import { DesignMixtureModel } from "../../model/design_mixture.model "
import { AccessTypes } from "../../model/access_logs.model"
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { ObjectId } from "bson"
import { CementBrandModel } from "../../model/cement_brand.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { CityModel, StateModel } from "../../model/region.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { CementGradeModel } from "../../model/cement_grade.model"

@injectable()
export class VendorDesignMixtureRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addDesignMixture(user_id: string, designMixData: IAddDesignMix) {
    const doc = await ConcreteGradeModel.findById(designMixData.grade_id).lean()
    if (isNullOrUndefined(doc)) {
      return Promise.reject(
        new InvalidInput(`No Concrete Grade data found.`, 400)
      )
    }
    const data = await VendorUserModel.findById(user_id).lean()
    if (isNullOrUndefined(data)) {
      return Promise.reject(new InvalidInput(`No Vendor Data found.`, 400))
    }

    let sub_vendor_id: any
    if (!isNullOrUndefined(designMixData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(designMixData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
    }
    designMixData.sub_vendor_id = sub_vendor_id
    // const designMixdata = await DesignMixtureModel.findOne({
    //   grade_id: new ObjectId(designMixData.grade_id),
    //   vendor_id: new ObjectId(user_id),
    // })
    const query: { [k: string]: any } = {
      grade_id: new ObjectId(designMixData.grade_id),
    }
    query.$or = [
      {
        vendor_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
    ]

    const designMixdata = await DesignMixtureModel.findOne(query)

    console.log("designMixdata", designMixdata)
    if (!isNullOrUndefined(designMixdata)) {
      return Promise.resolve(designMixdata._id)
    }
    designMixData.vendor_id = user_id
    const newDesignMixDoc = await new DesignMixtureModel(designMixData).save()

    //await this.addAdmixVariant(newDesignMixDoc._id, designMixData, user_id)
    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_DESIGN_MIX_BY_VENDOR,
      `Vendor added design mix of id ${newDesignMixDoc._id} .`
    )

    return Promise.resolve(newDesignMixDoc._id)
  }

  async addDesignMixtureVariant(
    design_mix_id: string,
    user_id: string,
    designMixVariantData: IAddDesignMixVariant
  ) {
    designMixVariantData.design_mix_id = design_mix_id
    designMixVariantData.vendor_id = user_id

    let design_mixObj: any = []
    for (let j = 0; j < designMixVariantData.variant.length; j++) {
      console.log(designMixVariantData)
      console.log("user_id", user_id)
      console.log(designMixVariantData.variant[j].address_id)
      const query: { [k: string]: any } = {
        //  vendor_id: new ObjectId(user_id),
        _id: new ObjectId(designMixVariantData.variant[j].address_id),
        "user.user_type": "vendor",
      }

      query.$or = [
        {
          "user.user_id": new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
      ]

      const addrCheck = await AddressModel.findOne(query)

      if (isNullOrUndefined(addrCheck)) {
        return Promise.reject(
          new InvalidInput(`Kindly enter a valid Address.`, 400)
        )
      }

      let sub_vendor_id: any
      if (!isNullOrUndefined(designMixVariantData.variant[j].address_id)) {
        console.log("hereee")
        console.log(
          "designMixVariantData.variant[j].address_id",
          designMixVariantData.variant[j].address_id
        )
        let addressData = await AddressModel.findOne({
          _id: new ObjectId(designMixVariantData.variant[j].address_id),
        })

        console.log("addressData", addressData)
        if (!isNullOrUndefined(addressData)) {
          sub_vendor_id = addressData.sub_vendor_id
        }
      }
      designMixVariantData.variant[j].sub_vendor_id = sub_vendor_id
      const data = await DesignMixVariantModel.findOne({
        product_name: designMixVariantData.variant[j].product_name,
        vendor_id: new ObjectId(user_id),
      })
      console.log(
        "(designMixVariantData.variant[j]",
        designMixVariantData.variant[j]
      )

      if (isNullOrUndefined(data)) {
        designMixVariantData.variant[j].design_mix_id = design_mix_id
        designMixVariantData.variant[j].vendor_id = user_id
        designMixVariantData.variant[j].pickup_location = addrCheck.location
        design_mixObj.push(designMixVariantData.variant[j])
        await DesignMixVariantModel.insertMany(design_mixObj)
      } else {
        return Promise.reject(
          new InvalidInput(`This product name alredy exists with.`, 400)
        )
      }
      return Promise.resolve()
    }
    // let dat1a = Object.assign({}, design_mixObj)
    // console.log(design_mixObj)

    // await new DesignMixVariantModel(dat1a).save()
    // const design_mixObj = [...new Set(designMixVariantData.variant)].map(
    //   (dm_variant) => ({
    //     design_mix_id,
    //     grade_id: designMixVariantData.grade_id,
    //     product_name: dm_variant.product_name,
    //     cement_brand_id: dm_variant.cement_brand_id,
    //     cement_quantity: dm_variant.cement_quantity,
    //     sand_source_id: dm_variant.sand_source_id,
    //     sand_quantity: dm_variant.sand_quantity,
    //     aggregate_source_id: dm_variant.aggregate_source_id,
    //     aggregate1_sub_category_id: dm_variant.aggregate1_sub_category_id,
    //     aggregate1_quantity: dm_variant.aggregate1_quantity,
    //     aggregate2_sub_category_id: dm_variant.aggregate2_sub_category_id,
    //     aggregate2_quantity: dm_variant.aggregate2_quantity,
    //     fly_ash_source_id: dm_variant.fly_ash_source_id,
    //     fly_ash_quantity: dm_variant.fly_ash_quantity,
    //     ad_mixture_brand_id: dm_variant.ad_mixture_brand_id,
    //     ad_mixture_quantity: dm_variant.ad_mixture_quantity,
    //     water_quantity: dm_variant.water_quantity,
    //     selling_price: dm_variant.selling_price,
    //     is_availble: dm_variant.is_availble,
    //     description: dm_variant.description,
    //     is_custom: dm_variant.is_custom,
    //     vendor_id: user_id,
    //   })
    // )
  }

  async getDesignMixture(searchParam: IGetDesignMixture, user_id: string) {
    const query: { [k: string]: any } = {
      //  vendor_id: new ObjectId(user_id),
    }
    query.$or = [
      {
        vendor_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = searchParam.grade_id
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "addressDetails",
        },
      },
      {
        $unwind: {
          path: "$addressDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "_id",
          foreignField: "design_mix_id",
          as: "designMixVariantDetails",
        },
      },
      // {
      //   $unwind: {
      //     path: "$designMixVariantDetails",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          address_id: true,
          "concrete_grade._id": true,
          "concrete_grade.name": true,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
          "addressDetails._id": 1,
          "addressDetails.line1": 1,
          "addressDetails.line2": 1,
          "addressDetails.state_id": 1,
          "addressDetails.city_id": 1,
          "addressDetails.pincode": 1,
          "addressDetails.location": 1,
          "addressDetails.address_type": 1,
          "addressDetails.business_name": 1,
          "addressDetails.region_id": 1,
          "addressDetails.source_id": 1,
          "addressDetails.created_at": 1,
          "addressDetails.updated_at": 1,
          "addressDetails.sub_vendor_id": 1,
          "addressDetails.master_vendor_id": 1,
          "addressDetails.billing_address_id": 1,
          "addressDetails.is_verified": 1,
          "addressDetails.is_accepted": 1,
          "addressDetails.is_blocked": 1,
          designMixVariantDetails: 1,
        },
      },
    ]

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      designMixData.reverse()
    }
    return Promise.resolve(designMixData)
  }

  async getDesignMixtureDetails(designMixtureId: string, user_id: string) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(designMixtureId),
      // vendor_id: new ObjectId(user_id),
    }

    query.$or = [
      {
        vendor_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "_id",
          foreignField: "design_mix_id",
          as: "designMixVariantDetails",
        },
      },
      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          designMixVariantDetails: true,
        },
      },
    ]

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)
    if (designMixData.length > 0) {
      for (
        let i = 0;
        i < designMixData[0].designMixVariantDetails.length;
        i++
      ) {
        const concreteGradeDoc = await ConcreteGradeModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].grade_id,
        }).lean()
        if (!isNullOrUndefined(concreteGradeDoc)) {
          designMixData[0].designMixVariantDetails[i].concrete_grade_name =
            concreteGradeDoc.name
        }

        const cementBrandDoc = await CementBrandModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].cement_brand_id,
        }).lean()
        if (!isNullOrUndefined(cementBrandDoc)) {
          designMixData[0].designMixVariantDetails[i].cement_brand_name =
            cementBrandDoc.name
        }

        const sandSourceDoc = await SandSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].sand_source_id,
        }).lean()
        if (!isNullOrUndefined(sandSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].sand_source_name =
            sandSourceDoc.sand_source_name
        }

        const aggregateSourceDoc = await AggregateSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].aggregate_source_id,
        }).lean()
        if (!isNullOrUndefined(aggregateSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].aggregate_source_name =
            aggregateSourceDoc.aggregate_source_name
        }

        const flyAshSourceDoc = await FlyAshSourceModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].fly_ash_source_id,
        }).lean()
        if (!isNullOrUndefined(flyAshSourceDoc)) {
          designMixData[0].designMixVariantDetails[i].fly_ash_source_name =
            flyAshSourceDoc.fly_ash_source_name
        }

        const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id:
              designMixData[0].designMixVariantDetails[i]
                .aggregate1_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate1SubCatDoc)) {
          designMixData[0].designMixVariantDetails[
            i
          ].aggregate1_sub_category_name = aggregate1SubCatDoc.sub_category_name
        }

        const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id:
              designMixData[0].designMixVariantDetails[i]
                .aggregate2_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate2SubCatDoc)) {
          designMixData[0].designMixVariantDetails[
            i
          ].aggregate2_sub_category_name = aggregate2SubCatDoc.sub_category_name
        }

        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].ad_mixture_brand_id
          )
        ) {
          const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
            _id:
              designMixData[0].designMixVariantDetails[i].ad_mixture_brand_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureBrandDoc)) {
            designMixData[0].designMixVariantDetails[i].admix_brand_name =
              adMixtureBrandDoc.name
          }
        }

        const addressDoc = await AddressModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].address_id,
        }).lean()
        if (!isNullOrUndefined(addressDoc)) {
          designMixData[0].designMixVariantDetails[i].address_line1 =
            addressDoc.line1
          designMixData[0].designMixVariantDetails[i].address_line2 =
            addressDoc.line2
          designMixData[0].designMixVariantDetails[i].pincode =
            addressDoc.pincode
          designMixData[0].designMixVariantDetails[i].location =
            addressDoc.location

          const cityDoc = await CityModel.findById({
            _id: addressDoc.city_id,
          }).lean()

          if (!isNullOrUndefined(cityDoc)) {
            designMixData[0].designMixVariantDetails[i].city_name =
              cityDoc.city_name
          }

          const stateDoc = await StateModel.findById({
            _id: addressDoc.state_id,
          }).lean()

          if (!isNullOrUndefined(stateDoc)) {
            designMixData[0].designMixVariantDetails[i].state_name =
              stateDoc.state_name
          }
        }
        if (
          !isNullOrUndefined(
            designMixData[0].designMixVariantDetails[i].ad_mixture_category_id
          )
        ) {
          const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
            _id:
              designMixData[0].designMixVariantDetails[i]
                .ad_mixture_category_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureCatDoc)) {
            designMixData[0].designMixVariantDetails[i].admix_category_name =
              adMixtureCatDoc.category_name

            designMixData[0].designMixVariantDetails[i].admixture_type =
              adMixtureCatDoc.admixture_type
          }
        }

        const cementGradeDoc = await CementGradeModel.findById({
          _id: designMixData[0].designMixVariantDetails[i].cement_grade_id,
        }).lean()
        if (!isNullOrUndefined(cementGradeDoc)) {
          designMixData[0].designMixVariantDetails[i].cement_grade_name =
            cementGradeDoc.name
        }
      }

      return Promise.resolve(designMixData[0])
    }
    return Promise.reject(
      new InvalidInput(`No RMC Supplier found near the selected location.`, 400)
    )
  }

  async editDesignMixDetails(
    user_id: string,
    designMixtureId: string,
    designMixData: IEditDesignMix | any
  ) {
    try {
      const designMixCheck = await DesignMixtureModel.findOne({
        _id: designMixtureId,
        vendor_id: new ObjectId(user_id),
      })
      if (designMixCheck === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      let editDoc: { [k: string]: any } = {}
      const arrKeys = ["grade_id"]

      if (designMixData.variant.length > 0) {
        for (let i = 0; i < designMixData.variant.length; i++) {
          if (!isNullOrUndefined(designMixData.variant[i]._id)) {
            console.log("hereeeeeeee")
            let editvariantDoc: { [k: string]: any } = {}
            editvariantDoc.grade_id = designMixData.variant[i].grade_id
            editvariantDoc.product_name = designMixData.variant[i].product_name
            editvariantDoc.cement_brand_id =
              designMixData.variant[i].cement_brand_id
            editvariantDoc.cement_quantity =
              designMixData.variant[i].cement_quantity
            editvariantDoc.sand_source_id =
              designMixData.variant[i].sand_source_id
            editvariantDoc.sand_quantity =
              designMixData.variant[i].sand_quantity
            editvariantDoc.aggregate_source_id =
              designMixData.variant[i].aggregate_source_id
            editvariantDoc.aggregate1_sub_category_id =
              designMixData.variant[i].aggregate1_sub_category_id
            editvariantDoc.aggregate1_quantity =
              designMixData.variant[i].aggregate1_quantity
            editvariantDoc.aggregate2_sub_category_id =
              designMixData.variant[i].aggregate2_sub_category_id
            editvariantDoc.aggregate2_quantity =
              designMixData.variant[i].aggregate2_quantity
            editvariantDoc.fly_ash_source_id =
              designMixData.variant[i].fly_ash_source_id
            editvariantDoc.fly_ash_quantity =
              designMixData.variant[i].fly_ash_quantity
            if (
              !isNullOrUndefined(designMixData.variant[i].ad_mixture_brand_id)
            ) {
              editvariantDoc.ad_mixture_brand_id =
                designMixData.variant[i].ad_mixture_brand_id
            }

            editvariantDoc.ad_mixture_quantity =
              designMixData.variant[i].ad_mixture_quantity
            editvariantDoc.water_quantity =
              designMixData.variant[i].water_quantity
            editvariantDoc.selling_price =
              designMixData.variant[i].selling_price
            editvariantDoc.is_availble = designMixData.variant[i].is_availble
            editvariantDoc.description = designMixData.variant[i].description
            editvariantDoc.is_custom = designMixData.variant[i].is_custom
            editvariantDoc.address_id = designMixData.variant[i].address_id

            let sub_vendor_id: any
            if (!isNullOrUndefined(designMixData.variant[i].address_id)) {
              let addressData = await AddressModel.findOne({
                _id: new ObjectId(designMixData.variant[i].address_id),
              })
              if (!isNullOrUndefined(addressData)) {
                sub_vendor_id = addressData.sub_vendor_id
              }
              editvariantDoc.sub_vendor_id = sub_vendor_id
            }
            editvariantDoc.master_vendor_id =
              designMixData.variant[i].master_vendor_id
            editvariantDoc.cement_grade_id =
              designMixData.variant[i].cement_grade_id
            if (
              !isNullOrUndefined(
                designMixData.variant[i].ad_mixture_category_id
              )
            ) {
              editvariantDoc.ad_mixture_category_id =
                designMixData.variant[i].ad_mixture_category_id
            }

            await DesignMixVariantModel.findOneAndUpdate(
              { _id: new ObjectId(designMixData.variant[i]._id) },
              {
                $set: editvariantDoc,
              }
            )
          } else {
            let variantDoc: { [k: string]: any } = {}
            variantDoc.design_mix_id = designMixtureId
            variantDoc.grade_id = designMixData.variant[i].grade_id
            variantDoc.product_name = designMixData.variant[i].product_name
            variantDoc.cement_brand_id =
              designMixData.variant[i].cement_brand_id
            variantDoc.cement_quantity =
              designMixData.variant[i].cement_quantity
            variantDoc.sand_source_id = designMixData.variant[i].sand_source_id
            variantDoc.sand_quantity = designMixData.variant[i].sand_quantity
            variantDoc.aggregate_source_id =
              designMixData.variant[i].aggregate_source_id
            variantDoc.aggregate1_sub_category_id =
              designMixData.variant[i].aggregate1_sub_category_id
            variantDoc.aggregate1_quantity =
              designMixData.variant[i].aggregate1_quantity
            variantDoc.aggregate2_sub_category_id =
              designMixData.variant[i].aggregate2_sub_category_id
            variantDoc.aggregate2_quantity =
              designMixData.variant[i].aggregate2_quantity
            variantDoc.fly_ash_source_id =
              designMixData.variant[i].fly_ash_source_id
            variantDoc.fly_ash_quantity =
              designMixData.variant[i].fly_ash_quantity

            if (
              !isNullOrUndefined(designMixData.variant[i].ad_mixture_brand_id)
            ) {
              variantDoc.ad_mixture_brand_id =
                designMixData.variant[i].ad_mixture_brand_id
            }

            variantDoc.ad_mixture_quantity =
              designMixData.variant[i].ad_mixture_quantity
            variantDoc.water_quantity = designMixData.variant[i].water_quantity
            variantDoc.selling_price = designMixData.variant[i].selling_price
            variantDoc.is_availble = designMixData.variant[i].is_availble
            variantDoc.description = designMixData.variant[i].description
            variantDoc.is_custom = designMixData.variant[i].is_custom
            variantDoc.address_id = designMixData.variant[i].address_id

            let sub_vendor_id: any
            if (!isNullOrUndefined(designMixData.variant[i].address_id)) {
              let addressData = await AddressModel.findOne({
                _id: new ObjectId(designMixData.variant[i].address_id),
              })
              if (!isNullOrUndefined(addressData)) {
                sub_vendor_id = addressData.sub_vendor_id
              }
              variantDoc.sub_vendor_id = sub_vendor_id
            }
            variantDoc.master_vendor_id =
              designMixData.variant[i].master_vendor_id

            variantDoc.cement_grade_id =
              designMixData.variant[i].cement_grade_id

            if (
              !isNullOrUndefined(
                designMixData.variant[i].ad_mixture_category_id
              )
            ) {
              variantDoc.ad_mixture_category_id =
                designMixData.variant[i].ad_mixture_category_id
            }
            variantDoc.ad_mixture_category_id =
              designMixData.variant[i].ad_mixture_category_id
            await new DesignMixVariantModel(variantDoc).save()
          }
        }
      }

      for (const [key, value] of Object.entries(designMixData)) {
        if (arrKeys.indexOf(key) > -1) {
          editDoc[key] = value
        }
      }

      if (Object.keys(editDoc).length < 1) {
        return Promise.reject(
          new InvalidInput(`Kindly enter a field for update`, 400)
        )
      }

      editDoc.updated_at = Date.now()
      editDoc.updated_by_id = user_id
      const designMixDoc = await DesignMixtureModel.findOneAndUpdate(
        { _id: designMixtureId },
        { $set: editDoc },
        {
          new: true,
        }
      )
      if (designMixDoc === null) {
        return Promise.reject(
          new InvalidInput(`Update failed as no record exists`, 400)
        )
      }

      await this.commonService.addActivityLogs(
        user_id,
        "vendor",
        AccessTypes.EDIT_DESIGN_MIX_BY_VENDOR,
        `Vendor updated details design mix of id ${designMixtureId} .`,
        designMixDoc,
        editDoc
      )

      return Promise.resolve(designMixDoc)
    } catch (err) {
      console.log(err)
      return Promise.reject(err)
    }
  }

  async editDesignMixVariantDetails(
    user_id: string,
    design_mix_variant_id: string,
    designMixData: IEditDesignMixVariant
  ) {
    console.log("designMixData", designMixData)

    const variantCheck = await DesignMixVariantModel.findOne({
      _id: design_mix_variant_id,
    })
    if (variantCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    const designMixdoc = await DesignMixtureModel.findById({
      _id: variantCheck.design_mix_id,
      vendor_id: new ObjectId(user_id),
    })

    if (designMixdoc === null) {
      return Promise.reject(
        new InvalidInput(
          `No RMC Supplier found near the selected location. or  design Mix is not associated with this vendor`,
          400
        )
      )
    }

    let editDoc: { [k: string]: any } = {}

    const arrKeys = [
      "product_name",
      "cement_brand_id",
      "cement_quantity",
      "sand_source_id",
      "sand_quantity",
      "aggregate_source_id",
      "aggregate1_sub_category_id",
      "aggregate1_quantity",
      "aggregate2_sub_category_id",
      "aggregate2_quantity",
      "fly_ash_source_id",
      "fly_ash_quantity",
      "ad_mixture_brand_id",
      "ad_mixture_quantity",
      "water_quantity",
      "selling_price",
      "is_available",
      "description",
      "is_custom",
      "address_id",
      "cement_grade_id",
      "ad_mixture_category_id",
      "master_vendor_id",
    ]

    for (const [key, value] of Object.entries(designMixData)) {
      if (arrKeys.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }

    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(
        new InvalidInput(`Kindly enter a field for update`, 400)
      )
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by_id = user_id

    let sub_vendor_id: any
    if (!isNullOrUndefined(designMixData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(designMixData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    const designMixDoc = await DesignMixVariantModel.findOneAndUpdate(
      { _id: new ObjectId(design_mix_variant_id) },
      { $set: editDoc },
      {
        new: true,
      }
    )
    if (designMixDoc === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_DESIGN_MIX_VARIANT_BY_VENDOR,
      `Vendor updated details design mix variant of id ${design_mix_variant_id} .`,
      designMixDoc,
      editDoc
    )

    return Promise.resolve(designMixDoc)
  }
  async getDesignMixtureVariantDetails(
    design_mix_variant_id: string,
    user_id: string
  ) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(design_mix_variant_id),
      //vendor_id: new ObjectId(user_id),
    }

    query.$or = [
      {
        vendor_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    console.log(query)
    const aggregateArr = [
      { $match: query },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          design_mix_id: true,
          grade_id: true,
          cement_brand_id: true,
          sand_source_id: true,
          aggregate_source_id: true,
          fly_ash_source_id: true,
          aggregate1_sub_category_id: true,
          aggregate2_sub_category_id: true,
          ad_mixture_brand_id: true,
          product_name: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          ad_mixture_quantity: true,
          water_quantity: true,
          selling_price: true,
          is_available: true,
          description: true,
          is_custom: true,
          address_id: true,
          ad_mixture_category_id: true,
          cement_grade_id: true,
          updated_by_id: true,
          updated_at: true,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]

    const designMixVariantData = await DesignMixVariantModel.aggregate(
      aggregateArr
    )
    if (designMixVariantData.length > 0) {
      const concreteGradeDoc = await ConcreteGradeModel.findById({
        _id: designMixVariantData[0].grade_id,
      }).lean()
      if (!isNullOrUndefined(concreteGradeDoc)) {
        designMixVariantData[0].concrete_grade_name = concreteGradeDoc.name
      }

      const cementBrandDoc = await CementBrandModel.findById({
        _id: designMixVariantData[0].cement_brand_id,
      }).lean()
      if (!isNullOrUndefined(cementBrandDoc)) {
        designMixVariantData[0].cement_brand_name = cementBrandDoc.name
      }

      const sandSourceDoc = await SandSourceModel.findById({
        _id: designMixVariantData[0].sand_source_id,
      }).lean()
      if (!isNullOrUndefined(sandSourceDoc)) {
        designMixVariantData[0].sand_source_name =
          sandSourceDoc.sand_source_name
      }

      const aggregateSourceDoc = await AggregateSourceModel.findById({
        _id: designMixVariantData[0].aggregate_source_id,
      }).lean()
      if (!isNullOrUndefined(aggregateSourceDoc)) {
        designMixVariantData[0].aggregate_source_name =
          aggregateSourceDoc.aggregate_source_name
      }

      const flyAshSourceDoc = await FlyAshSourceModel.findById({
        _id: designMixVariantData[0].fly_ash_source_id,
      }).lean()
      if (!isNullOrUndefined(flyAshSourceDoc)) {
        designMixVariantData[0].fly_ash_source_name =
          flyAshSourceDoc.fly_ash_source_name
      }

      const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate1_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate1SubCatDoc)) {
        designMixVariantData[0].aggregate1_sub_category_name =
          aggregate1SubCatDoc.sub_category_name
      }

      const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate2_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate2SubCatDoc)) {
        designMixVariantData[0].aggregate2_sub_category_name =
          aggregate2SubCatDoc.sub_category_name
      }

      if (!isNullOrUndefined(designMixVariantData[0].ad_mixture_brand_id)) {
        const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
          _id: designMixVariantData[0].ad_mixture_brand_id,
        }).lean()
        if (!isNullOrUndefined(adMixtureBrandDoc)) {
          designMixVariantData[0].admix_brand_name = adMixtureBrandDoc.name
        }
      }

      const addressDoc = await AddressModel.findById({
        _id: designMixVariantData[0].address_id,
      }).lean()
      if (!isNullOrUndefined(addressDoc)) {
        designMixVariantData[0].address_line1 = addressDoc.line1
        designMixVariantData[0].address_line2 = addressDoc.line2
        designMixVariantData[0].pincode = addressDoc.pincode
        designMixVariantData[0].location = addressDoc.location

        const cityDoc = await CityModel.findById({
          _id: addressDoc.city_id,
        }).lean()

        if (!isNullOrUndefined(cityDoc)) {
          designMixVariantData[0].city_name = cityDoc.city_name
        }

        const stateDoc = await StateModel.findById({
          _id: addressDoc.state_id,
        }).lean()

        if (!isNullOrUndefined(stateDoc)) {
          designMixVariantData[0].state_name = stateDoc.state_name
        }
      }

      if (!isNullOrUndefined(designMixVariantData[0].ad_mixture_category_id)) {
        const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
          _id: designMixVariantData[0].ad_mixture_category_id,
        }).lean()
        if (!isNullOrUndefined(adMixtureCatDoc)) {
          designMixVariantData[0].admix_category_name =
            adMixtureCatDoc.category_name
        }
      }

      const cementGradeDoc = await CementGradeModel.findById({
        _id: designMixVariantData[0].cement_grade_id,
      }).lean()
      if (!isNullOrUndefined(cementGradeDoc)) {
        designMixVariantData[0].cement_grade_name = cementGradeDoc.name
      }

      return Promise.resolve(designMixVariantData[0])
    }
    return Promise.reject(
      new InvalidInput(`No RMC Supplier found near the selected location.`, 400)
    )
  }
}
