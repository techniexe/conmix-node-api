import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { objectIdRegex, IRequestSchema } from "../../middleware/joi.middleware"

export interface IAddDesignMix {
  grade_id: string
  vendor_id: string
  address_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddDesignMixtureRequest extends IAuthenticatedRequest {
  body: IAddDesignMix
}

export const AddDesignMixtureSchema: IRequestSchema = {
  body: Joi.object().keys({
    grade_id: Joi.string().regex(objectIdRegex).required(),
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IAddDesignMixVariant {
  variant: [
    {
      grade_id: string
      product_name: string
      cement_brand_id: string
      cement_quantity: number
      sand_source_id: string
      sand_quantity: number
      aggregate_source_id: string
      aggregate1_sub_category_id: string
      aggregate1_quantity: number
      aggregate2_sub_category_id: string
      aggregate2_quantity: number
      fly_ash_source_id: string
      fly_ash_quantity: number
      ad_mixture_brand_id: string
      ad_mixture_quantity: number
      water_quantity: number
      selling_price: number
      is_available: boolean
      description: string
      is_custom: boolean
      address_id: string
      cement_grade_id: string
      ad_mixture_category_id: string
      master_vendor_id: string
      [k: string]: any
    }
  ]

  vendor_id: string
  [k: string]: any
}

export interface IAddDesignMixtureVariantRequest extends IAuthenticatedRequest {
  body: IAddDesignMixVariant
}

export const AddDesignMixtureVariantSchema: IRequestSchema = {
  body: Joi.object().keys({
    variant: Joi.array()
      .items({
        grade_id: Joi.string().regex(objectIdRegex).required(),
        product_name: Joi.string().min(3).max(200).required(),
        cement_brand_id: Joi.string().regex(objectIdRegex).required(),
        cement_quantity: Joi.number().min(1).required(),
        sand_source_id: Joi.string().regex(objectIdRegex).required(),
        sand_quantity: Joi.number().min(1).required(),
        aggregate_source_id: Joi.string().regex(objectIdRegex).required(),
        aggregate1_sub_category_id: Joi.string()
          .regex(objectIdRegex)
          .required(),
        aggregate1_quantity: Joi.number().min(1).required(),
        aggregate2_sub_category_id: Joi.string()
          .regex(objectIdRegex)
          .required(),
        aggregate2_quantity: Joi.number().min(1).required(),
        fly_ash_source_id: Joi.string().regex(objectIdRegex),
        fly_ash_quantity: Joi.number().min(1),
        ad_mixture_brand_id: Joi.string().regex(objectIdRegex),
        ad_mixture_category_id: Joi.string().regex(objectIdRegex),
        ad_mixture_quantity: Joi.number().min(1),
        water_quantity: Joi.number().min(1).required(),
        selling_price: Joi.number().min(1).required(),
        is_available: Joi.boolean().required(),
        description: Joi.string().max(500).required(),
        address_id: Joi.string().regex(objectIdRegex).required(),
        cement_grade_id: Joi.string().regex(objectIdRegex).required(),
        is_custom: Joi.boolean(),
        master_vendor_id: Joi.string().regex(objectIdRegex),
      })
      .required(),
  }),
}

export interface IGetDesignMixture {
  user_id?: string
  grade_id?: string
  before?: string
  after?: string
  address_id?: string
}

export interface IGetDesignMixtureRequest extends IAuthenticatedRequest {
  query: IGetDesignMixture
}

export const getDesignMixtureSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetDesignMixtureDetailsRequest extends IAuthenticatedRequest {
  params: {
    designMixtureId: string
  }
}

export const getDesignMixtureDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    designMixtureId: Joi.string(),
  }),
}

export interface IEditDesignMix {
  grade_id: string
  variant: [
    {
      product_name: string
      cement_brand_id: string
      cement_quantity: number
      sand_source_id: string
      sand_quantity: number
      aggregate_source_id: string
      aggregate1_sub_category_id: string
      aggregate1_quantity: number
      aggregate2_sub_category_id: string
      aggregate2_quantity: number
      fly_ash_source_id: string
      fly_ash_quantity: number
      ad_mixture_brand_id: string
      ad_mixture_quantity: number
      water_quantity: number
      selling_price: number
      is_available: boolean
      description: string
      is_custom: boolean
      address_id: string
      cement_grade_id: string
      ad_mixture_category_id: string
      master_vendor_id: string
    }
  ]

  vendor_id: string
  [k: string]: any
}

export interface IEditDesignMixRequest extends IAuthenticatedRequest {
  params: {
    designMixtureId: string
  }
  body: IEditDesignMix
}

export const EditdesignMixSchema: IRequestSchema = {
  params: Joi.object().keys({
    designMixtureId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    grade_id: Joi.string().regex(objectIdRegex),
    variant: Joi.array().items({
      product_name: Joi.string().min(3).max(200),
      cement_brand_id: Joi.string().regex(objectIdRegex),
      cement_quantity: Joi.number().min(1),
      sand_source_id: Joi.string().regex(objectIdRegex),
      sand_quantity: Joi.number().min(1),
      aggregate_source_id: Joi.string().regex(objectIdRegex),
      aggregate1_sub_category_id: Joi.string().regex(objectIdRegex),
      aggregate1_quantity: Joi.number().min(1),
      aggregate2_sub_category_id: Joi.string().regex(objectIdRegex),
      aggregate2_quantity: Joi.number().min(1),
      fly_ash_source_id: Joi.string().regex(objectIdRegex),
      fly_ash_quantity: Joi.number().min(1),
      ad_mixture_brand_id: Joi.string().regex(objectIdRegex),
      ad_mixture_quantity: Joi.number().min(1),
      water_quantity: Joi.number().min(1),
      selling_price: Joi.number().min(1),
      is_available: Joi.boolean(),
      description: Joi.string().max(500),
      is_custom: Joi.boolean(),
      address_id: Joi.string().regex(objectIdRegex),
      cement_grade_id: Joi.string().regex(objectIdRegex),
      ad_mixture_category_id: Joi.string().regex(objectIdRegex),
      master_vendor_id: Joi.string().regex(objectIdRegex),
    }),
  }),
}

export interface IEditDesignMixVariant {
  product_name: string
  cement_brand_id: string
  cement_quantity: number
  sand_source_id: string
  sand_quantity: number
  aggregate_source_id: string
  aggregate1_sub_category_id: string
  aggregate1_quantity: number
  aggregate2_sub_category_id: string
  aggregate2_quantity: number
  fly_ash_source_id: string
  fly_ash_quantity: number
  ad_mixture_brand_id: string
  ad_mixture_quantity: number
  water_quantity: number
  selling_price: number
  is_available: boolean
  description: string
  is_custom: boolean
  address_id: string
  cement_grade_id: string
  ad_mixture_category_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditDesignMixVariantRequest extends IAuthenticatedRequest {
  params: {
    design_mix_variant_id: string
  }
  body: IEditDesignMixVariant
}

export const EditDesignMixVariantSchema: IRequestSchema = {
  params: Joi.object().keys({
    design_mix_variant_id: Joi.string().regex(objectIdRegex),
  }),

  body: Joi.object().keys({
    product_name: Joi.string().min(3).max(200),
    cement_brand_id: Joi.string().regex(objectIdRegex),
    cement_quantity: Joi.number().min(1),
    sand_source_id: Joi.string().regex(objectIdRegex),
    sand_quantity: Joi.number().min(1),
    aggregate_source_id: Joi.string().regex(objectIdRegex),
    aggregate1_sub_category_id: Joi.string().regex(objectIdRegex),
    aggregate1_quantity: Joi.number().min(1),
    aggregate2_sub_category_id: Joi.string().regex(objectIdRegex),
    aggregate2_quantity: Joi.number().min(1),
    fly_ash_source_id: Joi.string().regex(objectIdRegex),
    fly_ash_quantity: Joi.number().min(1),
    ad_mixture_brand_id: Joi.string().regex(objectIdRegex),
    ad_mixture_quantity: Joi.number().min(1),
    water_quantity: Joi.number().min(1),
    selling_price: Joi.number().min(1),
    is_available: Joi.boolean(),
    description: Joi.string().max(500),
    is_custom: Joi.boolean(),
    address_id: Joi.string().regex(objectIdRegex),
    cement_grade_id: Joi.string().regex(objectIdRegex),
    ad_mixture_category_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetDesignMixtureVariantDetailsRequest
  extends IAuthenticatedRequest {
  params: {
    design_mix_variant_id: string
  }
}

export const getDesignMixtureVariantDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    design_mix_variant_id: Joi.string().regex(objectIdRegex).required(),
  }),
}
