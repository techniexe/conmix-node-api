import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorDesignMixtureRepository } from "./vendor.design_mix.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  AddDesignMixtureSchema,
  IAddDesignMixtureRequest,
  getDesignMixtureSchema,
  IGetDesignMixtureRequest,
  getDesignMixtureDetailsSchema,
  IGetDesignMixtureDetailsRequest,
  EditdesignMixSchema,
  IEditDesignMixRequest,
  EditDesignMixVariantSchema,
  IEditDesignMixVariantRequest,
  getDesignMixtureVariantDetailsSchema,
  IGetDesignMixtureVariantDetailsRequest,
  AddDesignMixtureVariantSchema,
  IAddDesignMixtureVariantRequest,
} from "./vendor.design_mix.req-schema"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/design_mix", verifyCustomToken("vendor"))
export class VendorDesignMixtureController {
  constructor(
    @inject(VendorTypes.VendorDesignMixtureRepository)
    private DesignMixRepo: VendorDesignMixtureRepository
  ) {}

  @httpPost("/", validate(AddDesignMixtureSchema))
  async addDesignMixture(
    req: IAddDesignMixtureRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const _id = await this.DesignMixRepo.addDesignMixture(
        req.user.uid,
        req.body
      )
      return res.json({ _id })
    } catch (err) {
      // console.log(err.code)
      // let msg = err.message
      // if (err.code === 11000) {
      //   msg = `Same grade_id already exists. `
      // }
      // const err1 = new UnexpectedInput(msg)
      // err1.httpStatusCode = 400
      return next(err)
    }
  }

  @httpPost("/variant/:design_mix_id", validate(AddDesignMixtureVariantSchema))
  async addDesignMixtureVariant(
    req: IAddDesignMixtureVariantRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.DesignMixRepo.addDesignMixtureVariant(
        req.params.design_mix_id,
        req.user.uid,
        req.body
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err.code)
      let msg = err.message
      if (err.code === 11000) {
        msg = `Product name already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpGet("/", validate(getDesignMixtureSchema))
  async getDesignMixture(
    req: IGetDesignMixtureRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.DesignMixRepo.getDesignMixture(
        req.query,
        req.user.uid
      )
      return res.json({ data })
    } catch (err) {
      console.log(err)
      next(err)
    }
  }

  @httpGet("/:designMixtureId", validate(getDesignMixtureDetailsSchema))
  async getDesignMixtureDetails(
    req: IGetDesignMixtureDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { designMixtureId } = req.params
      const data = await this.DesignMixRepo.getDesignMixtureDetails(
        designMixtureId,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /**
   *  Edit  details.
   */
  @httpPatch("/:designMixtureId", validate(EditdesignMixSchema))
  async editDesignMixDetails(
    req: IEditDesignMixRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.DesignMixRepo.editDesignMixDetails(
        req.user.uid,
        req.params.designMixtureId,
        req.body
      )
      return res.sendStatus(200)
    }  catch (err) {
      console.log(err.code)
      let msg = err.message
      if (err.code === 11000) {
        msg = `Same grade_id already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  /**
   *  Edit  details.
   */
  @httpPatch(
    "/variant/:design_mix_variant_id",
    validate(EditDesignMixVariantSchema)
  )
  async editDesignMixVariantVariantDetails(
    req: IEditDesignMixVariantRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.DesignMixRepo.editDesignMixVariantDetails(
        req.user.uid,
        req.params.design_mix_variant_id,
        req.body
      )
      return res.sendStatus(200)
    } catch (err) {
      console.log(err.code)
      let msg = err.message
      if (err.code === 11000) {
        msg = `Product name already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpGet(
    "/variant/:design_mix_variant_id",
    validate(getDesignMixtureVariantDetailsSchema)
  )
  async getDesignMixtureVariantDetails(
    req: IGetDesignMixtureVariantDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { design_mix_variant_id } = req.params
      const data = await this.DesignMixRepo.getDesignMixtureVariantDetails(
        design_mix_variant_id,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
