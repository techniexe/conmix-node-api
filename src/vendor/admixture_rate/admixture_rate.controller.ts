import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AdmixtureRateRepository } from "./admixture_rate.repository"
import {
  addAdmixtureRateSchema,
  IAddAdmixtureRateRequest,
  editAdmixtureRateSchema,
  IEditAdmixtureRateRequest,
  IRemoveAdmixtureRateRequest,
  removeAdmixtureRateSchema,
  getAdmixtureRateSchema,
  IGetAdmixtureRateRequest,
} from "./admixture_rate.req-schema"

@injectable()
@controller("/admixture_rate", verifyCustomToken("vendor"))
export class AdmixtureRateController {
  constructor(
    @inject(VendorTypes.AdmixtureRateRepository)
    private admixtureRateRepo: AdmixtureRateRepository
  ) {}

  @httpPost("/", validate(addAdmixtureRateSchema))
  async addAdmixtureRate(
    req: IAddAdmixtureRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.admixtureRateRepo.addAdmixtureRate(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:AdmixtureRateId", validate(editAdmixtureRateSchema))
  async editAdmixtureRate(
    req: IEditAdmixtureRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.admixtureRateRepo.editAdmixtureRate(
      req.user.uid,
      req.params.AdmixtureRateId,
      req.body
    )
    res.sendStatus(204)
  }

  @httpDelete("/:AdmixtureRateId", validate(removeAdmixtureRateSchema))
  async removeAdmixtureRate(
    req: IRemoveAdmixtureRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.admixtureRateRepo.removeAdmixtureRate(
        req.params.AdmixtureRateId
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getAdmixtureRateSchema))
  async List(req: IGetAdmixtureRateRequest, res: Response) {
    const data = await this.admixtureRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
