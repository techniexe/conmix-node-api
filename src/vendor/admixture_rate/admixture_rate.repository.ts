import { ObjectId } from "bson"
import { inject, injectable } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"

import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { AdmixtureRateModel } from "../../model/admixture_rate.model"
import {
  IAdmixtureRate,
  IEditAdmixtureRate,
  IGetAdmixtureRateList,
} from "./admixture_rate.req-schema"
import { AddressModel } from "../../model/address.model"

@injectable()
export class AdmixtureRateRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addAdmixtureRate(vendor_id: string, AdmixtureRateData: IAdmixtureRate) {
    const data = await AdmixtureRateModel.findOne({
      vendor_id,
      address_id: AdmixtureRateData.address_id,
      brand_id: AdmixtureRateData.brand_id,
      category_id: AdmixtureRateData.category_id,
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Admixture rate already exists for this brand and category at selected site address.`,
          400
        )
      )
    }
    AdmixtureRateData.vendor_id = vendor_id
   
    let sub_vendor_id: any
    if(!isNullOrUndefined(AdmixtureRateData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(AdmixtureRateData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      }
      AdmixtureRateData.sub_vendor_id = sub_vendor_id
    const newAdmixtureRate = new AdmixtureRateModel(AdmixtureRateData)
    const newadmixtureDoc = await newAdmixtureRate.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_ADMIXTURE_RATE_BY_VENDOR,
      `Vendor added Admixture rate of id ${newadmixtureDoc._id} .`
    )

    return Promise.resolve(newadmixtureDoc)
  }

  async editAdmixtureRate(
    vendor_id: string,
    AdmixtureRateId: string,
    editData: IEditAdmixtureRate
  ): Promise<any> {
  
    const data = await AdmixtureRateModel.findOne({
      vendor_id,
      address_id: editData.address_id,
      brand_id: editData.brand_id,
      category_id: editData.category_id,
    })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(AdmixtureRateId) === false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Admixture rate already exists for this brand and category at selected site address.`,
          400
        )
      )
    }
  
    const editDoc: { [k: string]: any } = {}
    const edtFld = ["address_id", "brand_id", "category_id", "per_kg_rate", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(editData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(editData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }


    const edtRes = await AdmixtureRateModel.updateOne(
      {
        _id: new ObjectId(AdmixtureRateId),
       // vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Invalid request as no Admixture rate data found`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_ADMIXTURE_RATE_BY_VENDOR,
      "aggreagte rate information Updated."
    )

    return Promise.resolve()
  }
  async removeAdmixtureRate(AdmixtureRateId: string) {
    const status = await AdmixtureRateModel.deleteOne({
      _id: new ObjectId(AdmixtureRateId),
    })
    if (status.n !== 1) {
      throw new Error("Admixture Rate doesn't exists")
    }
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetAdmixtureRateList) {
    const query: { [k: string]: any } = {}

   // query.vendor_id = new ObjectId(vendor_id)

   query.$or = [
    {
      vendor_id: new ObjectId(vendor_id),
    },
    {
      sub_vendor_id: new ObjectId(vendor_id),
    },
    {
      master_vendor_id: new ObjectId(vendor_id),
    },
  ]


    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.brand_id)) {
      query.brand_id = new ObjectId(searchParam.brand_id)
    }

    if (!isNullOrUndefined(searchParam.category_id)) {
      query.category_id = new ObjectId(searchParam.category_id)
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "less") {
      query.per_kg_rate = {$lte: parseInt(searchParam.per_kg_rate)}
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "more") {
      query.per_kg_rate = {$gte: parseInt(searchParam.per_kg_rate)}
    }
    
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: {
          path: "$admixture_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          "admixture_category.admixture_type": 1,
          per_kg_rate: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await AdmixtureRateModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
