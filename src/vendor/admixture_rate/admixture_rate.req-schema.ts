import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAdmixtureRate {
  address_id: string
  brand_id: string
  category_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddAdmixtureRateRequest extends IAuthenticatedRequest {
  body: IAdmixtureRate
}

export const addAdmixtureRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex).required(),
    category_id: Joi.string().regex(objectIdRegex).required(),
    per_kg_rate: Joi.number().min(1).required(),
  }),
}

export interface IEditAdmixtureRate {
  address_id: string
  brand_id: string
  category_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditAdmixtureRateRequest extends IAuthenticatedRequest {
  params: {
    AdmixtureRateId: string
  }
  body: IEditAdmixtureRate
}

export const editAdmixtureRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    category_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number().min(1),
  }),

  params: Joi.object().keys({
    AdmixtureRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveAdmixtureRateRequest extends IAuthenticatedRequest {
  params: {
    AdmixtureRateId: string
  }
}

export const removeAdmixtureRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    AdmixtureRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetAdmixtureRateList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  category_id?: string
  per_kg_rate?: any
  per_kg_rate_value?: string
}

export interface IGetAdmixtureRateRequest extends IAuthenticatedRequest {
  query: IGetAdmixtureRateList
}

export const getAdmixtureRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    category_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number(),
    per_kg_rate_value: Joi.string(),
  }),
}
