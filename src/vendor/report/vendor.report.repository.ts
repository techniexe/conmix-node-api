import { injectable } from "inversify"
import { escapeRegExp, isNullOrUndefined } from "../../utilities/type-guards"
import { monthsCode } from "../../utilities/config"
import { ObjectId } from "bson"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { ProductModel } from "../../model/product.model"
import {
  IGetAdmixtureQuantityList,
  IGetAggregateSourceQuantityList,
  IGetCementQuantityList,
  IGetConcretePumpList,
  IGetDesignMixture,
  IGetFlyAshSourceQuantityList,
  IGetSandSourceQuantityList,
  IGetTMDetails,
} from "./vendor.report.req-schema"
import { DesignMixtureModel } from "../../model/design_mixture.model "
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { ConcreteGradeModel } from "../../model/concrete_grade.model"
import { CementBrandModel } from "../../model/cement_brand.model"
import { SandSourceModel } from "../../model/sand_source.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { FlyAshSourceModel } from "../../model/fly_ash.model"
import { AggregateSandSubCategoryModel } from "../../model/aggregate_sand_category.model"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { AddressModel } from "../../model/address.model"
import { CityModel, StateModel } from "../../model/region.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"
import { CementGradeModel } from "../../model/cement_grade.model"
import { InvalidInput } from "../../utilities/customError"
import { BillingAddressModel } from "../../model/billing_address.model"
import { DriverInfoModel } from "../../model/driver.model"
import { OperatorInfoModel } from "../../model/operator.model"
import { TMModel } from "../../model/TM.model"
import { ConcretePumpModel } from "../../model/concrete_pump.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { FlyAshSourceQuantityModel } from "../../model/fly_ash_source_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { BankInfoModel } from "../../model/bank.model"
import { BuyerUserModel } from "../../model/buyer.user.model"

@injectable()
export class SupplierReportRepository {
  async vendorOrder(
    user_id: string,
    before: string | undefined,
    after: string | undefined,
    month: string | undefined,
    year: number | undefined,
    design_mix_id: string | undefined,
    buyer_id: string | undefined,
    status: string | undefined,
    is_export: boolean | undefined,
    start_date: string,
    end_date: string,
    order_id: string,
    buyer_order_id: any,
    payment_status: string,
    buyer_name: any
  ) {
    let Orderquery: { [k: string]: any } = { user_id: new ObjectId(user_id) }
    const limit = 10
    // If year is passed, then check with first and last date of given year.
    if (!isNullOrUndefined(year)) {
      Orderquery.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }
    // If month and year passed, then find first and last day of given month and year then check.
    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      Orderquery.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    const sort = { created_at: 1 }
    if (!isNullOrUndefined(before)) {
      Orderquery.created_at = { $lt: new Date(before) }
    }

    if (!isNullOrUndefined(status)) {
      Orderquery.order_status = status
    }

    if (!isNullOrUndefined(after)) {
      Orderquery.created_at = { $gt: new Date(after) }
      sort.created_at = -1
    }

    if (!isNullOrUndefined(order_id)) {
      Orderquery._id = order_id
    }

    if (!isNullOrUndefined(buyer_order_id)) {
      Orderquery.buyer_order_display_id = buyer_order_id
    }

    if (!isNullOrUndefined(buyer_name)) {
      let buyerData = await BuyerUserModel.findOne({
        full_name: buyer_name,
      })
      if (!isNullOrUndefined(buyerData)) {
        Orderquery.buyer_id = new ObjectId(buyerData._id)
      } else {
        Orderquery.buyer_id = ""
      }
    }

    if (!isNullOrUndefined(payment_status)) {
      Orderquery.payment_status = payment_status
    }

    if (!isNullOrUndefined(design_mix_id)) {
      Orderquery.design_mix_id = new ObjectId(design_mix_id)
    }

    if (!isNullOrUndefined(buyer_id)) {
      Orderquery.buyer_id = new ObjectId(buyer_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      Orderquery.vendor_order_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      Orderquery.vendor_order_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      Orderquery.vendor_order_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    const aggregateArr: any[] = [
      { $match: Orderquery },
      { $sort: sort },
      //{ $limit: 40 },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_display_id",
          foreignField: "display_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "site",
          localField: "order.site_id",
          foreignField: "_id",
          as: "site",
        },
      },
      {
        $unwind: {
          path: "$site",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "order.state_id",
          foreignField: "_id",
          as: "stateData",
        },
      },
      {
        $unwind: {
          path: "$stateData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "order.city_id",
          foreignField: "_id",
          as: "cityData",
        },
      },
      {
        $unwind: {
          path: "$cityData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "buyer.company_name": true,
          "delivery_address.site_id": "$order.site_id",
          "delivery_address.site_name": "$order.site_name",
          "delivery_address.address_line1": "$order.address_line1",
          "delivery_address.address_line2": "$order.address_line2",
          "delivery_address.state_id": "$order.state_id",
          "delivery_address.city_id": "$order.city_id",
          "delivery_address.pincode": "$order.pincode",
          "delivery_address.location": "$order.delivery_location",
          "delivery_address.created_at": "$order.created_at",
          "delivery_address.state_name": "$stateData.state_name",
          "delivery_address.city_name": "$cityData.city_name",
          "site.site_name": true,
          "site.company_name": true,
          "site.person_name": true,
          "site.mobile_number": true,
          "site.alt_mobile_number": true,
        },
      },
    ]

    if (isNullOrUndefined(is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      vendorOrderData.reverse()
    }
    return Promise.resolve(vendorOrderData)
  }

  async getProductList(
    user_id: string,
    before: string | undefined,
    after: string | undefined,
    product_category_id: string | undefined,
    product_sub_category_id: string | undefined
  ) {
    const query: { [k: string]: any } = { user_id: new ObjectId(user_id) }

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(product_category_id)) {
      query.product_category_id = new ObjectId(product_category_id)
    }

    if (!isNullOrUndefined(product_sub_category_id)) {
      query.product_sub_category_id = new ObjectId(product_sub_category_id)
    }

    return ProductModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "supplier",
          localField: "user_id",
          foreignField: "_id",
          as: "supplier",
        },
      },
      {
        $unwind: { path: "$supplier", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: {
          path: "$productSubCategory",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source" },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          unit_price: 1,
          created_at: 1,
          verified_by_admin: 1,
          media: 1,
          minimum_order: 1,
          is_available: 1,
          self_logistics: 1,
          "productCategory._id": 1,
          "productSubCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "supplier.full_name": 1,
          "supplier.mobile_number": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.source_name": "$source.source_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }

  async getDesignMixList(searchParam: IGetDesignMixture, user_id: string) {
    const query: { [k: string]: any } = {
      vendor_id: new ObjectId(user_id),
    }
    const limit = 10

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = searchParam.grade_id
    }

    const aggregateArr: any[] = [
      { $match: query },
      { $sort: sort },
      // { $limit: 10 },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
        },
      },

      {
        $project: {
          _id: true,
          created_at: true,
          updated_by_id: true,
          updated_at: true,
          "concrete_grade._id": true,
          "concrete_grade.name": true,
        },
      },
    ]

    if (isNullOrUndefined(searchParam.is_export)) {
      aggregateArr.push({ $limit: limit })
    }

    const designMixData = await DesignMixtureModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      designMixData.reverse()
    }
    return Promise.resolve(designMixData)
  }

  async getDesignMixtureVariantDetails(
    design_mix_variant_id: string,
    user_id: string
  ) {
    const query: { [k: string]: any } = {
      _id: new ObjectId(design_mix_variant_id),
      vendor_id: new ObjectId(user_id),
    }
    console.log(query)
    const aggregateArr = [
      { $match: query },
      {
        $project: {
          _id: true,
          design_mix_id: true,
          grade_id: true,
          cement_brand_id: true,
          sand_source_id: true,
          aggregate_source_id: true,
          fly_ash_source_id: true,
          aggregate1_sub_category_id: true,
          aggregate2_sub_category_id: true,
          ad_mixture_brand_id: true,
          product_name: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          ad_mixture_quantity: true,
          water_quantity: true,
          selling_price: true,
          is_availble: true,
          description: true,
          is_custom: true,
          address_id: true,
          ad_mixture_category_id: true,
          cement_grade_id: true,
          updated_by_id: true,
          updated_at: true,
        },
      },
    ]

    const designMixVariantData = await DesignMixVariantModel.aggregate(
      aggregateArr
    )
    if (designMixVariantData.length > 0) {
      const concreteGradeDoc = await ConcreteGradeModel.findById({
        _id: designMixVariantData[0].grade_id,
      }).lean()
      if (!isNullOrUndefined(concreteGradeDoc)) {
        designMixVariantData[0].concrete_grade_name = concreteGradeDoc.name
      }

      const cementBrandDoc = await CementBrandModel.findById({
        _id: designMixVariantData[0].cement_brand_id,
      }).lean()
      if (!isNullOrUndefined(cementBrandDoc)) {
        designMixVariantData[0].cement_brand_name = cementBrandDoc.name
      }

      const sandSourceDoc = await SandSourceModel.findById({
        _id: designMixVariantData[0].sand_source_id,
      }).lean()
      if (!isNullOrUndefined(sandSourceDoc)) {
        designMixVariantData[0].sand_source_name =
          sandSourceDoc.sand_source_name
      }

      const aggregateSourceDoc = await AggregateSourceModel.findById({
        _id: designMixVariantData[0].aggregate_source_id,
      }).lean()
      if (!isNullOrUndefined(aggregateSourceDoc)) {
        designMixVariantData[0].aggregate_source_name =
          aggregateSourceDoc.aggregate_source_name
      }

      const flyAshSourceDoc = await FlyAshSourceModel.findById({
        _id: designMixVariantData[0].fly_ash_source_id,
      }).lean()
      if (!isNullOrUndefined(flyAshSourceDoc)) {
        designMixVariantData[0].fly_ash_source_name =
          flyAshSourceDoc.fly_ash_source_name
      }

      const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate1_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate1SubCatDoc)) {
        designMixVariantData[0].aggregate1_sub_category_name =
          aggregate1SubCatDoc.sub_category_name
      }

      const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById({
        _id: designMixVariantData[0].aggregate2_sub_category_id,
      }).lean()
      if (!isNullOrUndefined(aggregate2SubCatDoc)) {
        designMixVariantData[0].aggregate2_sub_category_name =
          aggregate2SubCatDoc.sub_category_name
      }

      const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
        _id: designMixVariantData[0].ad_mixture_brand_id,
      }).lean()
      if (!isNullOrUndefined(adMixtureBrandDoc)) {
        designMixVariantData[0].admix_brand_name = adMixtureBrandDoc.name
      }

      const addressDoc = await AddressModel.findById({
        _id: designMixVariantData[0].address_id,
      }).lean()
      if (!isNullOrUndefined(addressDoc)) {
        designMixVariantData[0].address_line1 = addressDoc.line1
        designMixVariantData[0].address_line2 = addressDoc.line2
        designMixVariantData[0].pincode = addressDoc.pincode
        designMixVariantData[0].location = addressDoc.location

        const cityDoc = await CityModel.findById({
          _id: addressDoc.city_id,
        }).lean()

        if (!isNullOrUndefined(cityDoc)) {
          designMixVariantData[0].city_name = cityDoc.city_name
        }

        const stateDoc = await StateModel.findById({
          _id: addressDoc.state_id,
        }).lean()

        if (!isNullOrUndefined(stateDoc)) {
          designMixVariantData[0].state_name = stateDoc.state_name
        }
      }

      const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
        _id: designMixVariantData[0].ad_mixture_category_id,
      }).lean()
      if (!isNullOrUndefined(adMixtureCatDoc)) {
        designMixVariantData[0].admix_category_name =
          adMixtureCatDoc.category_name
      }

      const cementGradeDoc = await CementGradeModel.findById({
        _id: designMixVariantData[0].cement_grade_id,
      }).lean()
      if (!isNullOrUndefined(cementGradeDoc)) {
        designMixVariantData[0].cement_grade_name = cementGradeDoc.name
      }

      return Promise.resolve(designMixVariantData[0])
    }
    return Promise.reject(
      new InvalidInput(`No RMC Supplier found near the selected location.`, 400)
    )
  }

  async vendorPlantAddressList(
    user_id: string,
    search?: string,
    before?: string,
    after?: string,
    city_id?: string,
    start_date?: string,
    end_date?: string
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
    ]
    if (!isNullOrUndefined(search) && search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(search)
      query.$or = [
        {
          business_name: { $regex, $options },
        },
      ]
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(city_id) && city_id !== "") {
      query.city_id = new ObjectId(city_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.plant_address_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.plant_address_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.plant_address_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    console.log(query)
    return AddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "source",
          localField: "source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "billingAddressDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "billingAddressDetails.state_id",
          foreignField: "_id",
          as: "billingAddressStateDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressStateDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "billingAddressDetails.city_id",
          foreignField: "_id",
          as: "billingAddressCityDetails",
        },
      },
      {
        $unwind: {
          path: "$billingAddressCityDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          location: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          address_type: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          business_name: 1,
          region_id: 1,
          region_name: "$state.state_name",
          source_id: 1,
          source_name: "$source.source_name",
          created_at: 1,
          billing_address_id: 1,
          is_verified: 1,
          is_blocked: 1,
          is_accepted: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "vendorDetails.no_of_plants": 1,
          "vendorDetails.plant_capacity_per_hour": 1,
          "vendorDetails.no_of_operation_hour": 1,
          "billingAddressDetails._id": 1,
          "billingAddressDetails.company_name": 1,
          "billingAddressDetails.line1": 1,
          "billingAddressDetails.line2": 1,
          "billingAddressDetails.pincode": 1,
          "billingAddressDetails.gst_number": 1,
          "billingAddressDetails.gst_image_url": 1,
          "billingAddressDetails.created_at": 1,
          "billingAddressStateDetails._id": "$stateDetails._id",
          "billingAddressStateDetails.state_name": "$stateDetails.state_name",
          "billingAddressStateDetails.country_id": "$stateDetails.country_id",
          "billingAddressStateDetails.country_code":
            "$stateDetails.country_code",
          "billingAddressStateDetails.country_name":
            "$stateDetails.country_name",
          "billingAddressStateDetails.created_at": "$stateDetails.created_at",
          "billingAddressCityDetails._id": "$cityDetails._id",
          "billingAddressCityDetails.city_name": "$cityDetails.city_name",
          "billingAddressCityDetails.location": "$cityDetails.location",
          "billingAddressCityDetails.state_id": "$cityDetails.state_id",
          "billingAddressCityDetails.created_at": "$cityDetails.created_at",
          "billingAddressCityDetails.country_code": "$cityDetails.country_code",
          "billingAddressCityDetails.country_id": "$cityDetails.country_id",
          "billingAddressCityDetails.country_name": "$cityDetails.country_name",
          "billingAddressCityDetails.state_name": "$cityDetails.state_name",
          "billingAddressCityDetails.popularity": "$cityDetails.popularity",
        },
      },
    ])
  }

  async vendorBillingAddressList(
    user_id: string,
    search?: string,
    before?: string,
    after?: string,
    city_id?: any,
    start_date?: string,
    end_date?: string
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
        is_deleted: false,
      },
      {
        sub_vendor_id: new ObjectId(user_id),
        is_deleted: false,
      },
    ]
    // let query: { [k: string]: any } = {
    //   "user.user_id": new ObjectId(user_id),
    //   "user.user_type": "vendor",
    // }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(city_id) && city_id !== "") {
      query.city_id = new ObjectId(city_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.billing_address_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.billing_address_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.billing_address_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }

    console.log(query)
    return BillingAddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      // {
      //   $limit: 10,
      // },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "_id",
          foreignField: "billing_address_id",
          as: "AddressDetails",
        },
      },
      // {
      //   $unwind: { path: "$AddressDetails", preserveNullAndEmptyArrays: true },
      // },
      {
        $project: {
          _id: 1,
          company_name: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          gst_number: 1,
          gst_image_url: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          created_at: 1,
          updated_at: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          AddressDetails: 1,
        },
      },
    ])
    // return AddressModel.find(query).limit(40)
  }

  async getDriverList(
    user_id: string,
    searchParam: any,
    start_date?: string,
    end_date?: string
  ) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    if (!isNullOrUndefined(searchParam.sub_vendor_id)) {
      query.sub_vendor_id = new ObjectId(searchParam.sub_vendor_id)
    }

    if (!isNullOrUndefined(start_date) && start_date !== "") {
      query.driver_date = { $gte: start_date }
    }
    if (!isNullOrUndefined(end_date) && end_date !== "") {
      query.driver_date = { $lte: end_date }
    }

    if (
      !isNullOrUndefined(start_date) &&
      start_date !== "" &&
      !isNullOrUndefined(end_date) &&
      end_date !== ""
    ) {
      query.driver_date = {
        $gte: start_date,
        $lte: end_date,
      }
    }
    return DriverInfoModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorDetails._id",
          foreignField: "sub_vendor_id",
          as: "addressDetails",
        },
      },
      {
        $unwind: { path: "$addressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "addressDetails.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "addressDetails.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          driver_name: 1,
          driver_mobile_number: 1,
          driver_alt_mobile_number: 1,
          driver_whatsapp_number: 1,
          driver_pic: 1,
          created_at: 1,
          sub_vendor_id: 1,
          master_vendor_id: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
          // addressDetails: 1,
          "addressDetails.user": 1,
          "addressDetails.state_id": 1,
          "addressDetails.city_id": 1,
          "addressDetails.address_type": 1,
          "addressDetails.business_name": 1,
          "addressDetails.region_id": 1,
          "addressDetails.source_id": 1,
          "addressDetails.created_at": 1,
          "addressDetails.updated_at": 1,
          "addressDetails.sub_vendor_id": 1,
          "addressDetails.master_vendor_id": 1,
          "addressDetails.billing_address_id": 1,
          "addressDetails.is_verified": 1,
          "addressDetails.is_accepted": 1,
          "addressDetails.is_blocked": 1,
          "addressDetails.plant_address_date": 1,
          "addressDetails.line1": 1,
          "addressDetails.line2": 1,
          "addressDetails.state_name": "$state.state_name",
          "addressDetails.city_name": "$city.city_name",
          "addressDetails.source_name": "$source.source_name",
          "addressDetails.pincode": 1,
          "addressDetails.location": 1,
        },
      },
    ])
    // return DriverInfoModel.find({ query })
  }

  async getOperatorList(user_id: string, searchParam: any) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    if (!isNullOrUndefined(searchParam.sub_vendor_id)) {
      query.sub_vendor_id = new ObjectId(searchParam.sub_vendor_id)
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.operator_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.operator_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.operator_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }
    return OperatorInfoModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorDetails._id",
          foreignField: "sub_vendor_id",
          as: "addressDetails",
        },
      },
      {
        $unwind: { path: "$addressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "addressDetails.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "addressDetails.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          operator_name: 1,
          operator_mobile_number: 1,
          operator_alt_mobile_number: 1,
          operator_whatsapp_number: 1,
          operator_pic: 1,
          created_at: 1,
          sub_vendor_id: 1,
          master_vendor_id: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
          //addressDetails: 1
          "addressDetails.user": 1,
          "addressDetails.state_id": 1,
          "addressDetails.city_id": 1,
          "addressDetails.address_type": 1,
          "addressDetails.business_name": 1,
          "addressDetails.region_id": 1,
          "addressDetails.source_id": 1,
          "addressDetails.created_at": 1,
          "addressDetails.updated_at": 1,
          "addressDetails.sub_vendor_id": 1,
          "addressDetails.master_vendor_id": 1,
          "addressDetails.billing_address_id": 1,
          "addressDetails.is_verified": 1,
          "addressDetails.is_accepted": 1,
          "addressDetails.is_blocked": 1,
          "addressDetails.plant_address_date": 1,
          "addressDetails.line1": 1,
          "addressDetails.line2": 1,
          "addressDetails.state_name": "$state.state_name",
          "addressDetails.city_name": "$city.city_name",
          "addressDetails.source_name": "$source.source_name",
          "addressDetails.pincode": 1,
          "addressDetails.location": 1,
        },
      },
    ])
    //  return OperatorInfoModel.find({ "user.user_id": user_id })
  }
  async getTM(user_id: string, searchParam: IGetTMDetails) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    // query.user_id = new ObjectId(user_id)

    let limit: number = 20
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.TM_sub_category_id)) {
      query.TM_sub_category_id = new ObjectId(searchParam.TM_sub_category_id)
    }

    if (!isNullOrUndefined(searchParam.is_gps_enabled)) {
      query.is_gps_enabled = searchParam.is_gps_enabled
    }

    if (!isNullOrUndefined(searchParam.is_insurance_active)) {
      query.is_insurance_active = searchParam.is_insurance_active
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.TM_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.TM_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.TM_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: {
          path: "$TM_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: "$address",
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: "$address.pincode",
          business_name: "$address.business_name",
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          pickup_location: 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.min_load_capacity": 1,
          "TM_sub_category.max_load_capacity": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getConcretePump(user_id: string, searchParam: IGetConcretePumpList) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    let limit: number = 10
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.serial_number)) {
      query.serial_number = searchParam.serial_number
    }

    if (!isNullOrUndefined(searchParam.concrete_pump_category_id)) {
      query.concrete_pump_category_id = new ObjectId(
        searchParam.concrete_pump_category_id
      )
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.CP_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.CP_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.CP_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "concrete_pump_category",
          localField: "concrete_pump_category_id",
          foreignField: "_id",
          as: "concrete_pump_category",
        },
      },
      {
        $unwind: {
          path: "$concrete_pump_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "operator_info",
          localField: "operator_id",
          foreignField: "_id",
          as: "operator_info",
        },
      },
      {
        $unwind: {
          path: "$operator_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          concrete_pump_category_id: 1,
          company_name: 1,
          concrete_pump_model: 1,
          manufacture_year: 1,
          pipe_connection: 1,
          concrete_pump_capacity: 1,
          is_Operator: 1,
          operator_id: 1,
          is_helper: 1,
          transportation_charge: 1,
          concrete_pump_price: 1,
          concrete_pump_image_url: 1,
          created_at: 1,
          updated_at: 1,
          serial_number: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          business_name: "$address.business_name",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: "$address.pincode",
          "concrete_pump_category.category_name": 1,
          "concrete_pump_category.is_active": 1,
          "operator_info._id": 1,
          "operator_info.operator_name": 1,
          "operator_info.operator_mobile_number": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await ConcretePumpModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getAdmixStock(
    vendor_id: string,
    searchParam: IGetAdmixtureQuantityList
  ) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    // query.vendor_id = new ObjectId(vendor_id)

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.brand_id)) {
      query.brand_id = new ObjectId(searchParam.brand_id)
    }

    if (!isNullOrUndefined(searchParam.category_id)) {
      query.category_id = new ObjectId(searchParam.category_id)
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "less"
    ) {
      query.quantity = { $lte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "more"
    ) {
      query.quantity = { $gte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.admix_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.admix_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.admix_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: {
          path: "$admixture_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,

          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          "admixture_category.admixture_type": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await AdmixtureQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getAggStock(
    vendor_id: string,
    searchParam: IGetAggregateSourceQuantityList
  ) {
    const query: { [k: string]: any } = {}

    //query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (!isNullOrUndefined(searchParam.sub_cat_id)) {
      query.sub_cat_id = new ObjectId(searchParam.sub_cat_id)
    }
    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "less"
    ) {
      query.quantity = { $lte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "more"
    ) {
      query.quantity = { $gte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.agg_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.agg_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.agg_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "sub_cat_id",
          foreignField: "_id",
          as: "aggregate_sand_sub_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate_sand_sub_category._id": 1,
          "aggregate_sand_sub_category.sub_category_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await AggregateSourceQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getCementStock(vendor_id: string, searchParam: IGetCementQuantityList) {
    const query: { [k: string]: any } = {}

    console.log("vendor_id", vendor_id)

    // query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.brand_id)) {
      query.brand_id = new ObjectId(searchParam.brand_id)
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = new ObjectId(searchParam.grade_id)
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "less"
    ) {
      query.quantity = { $lte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "more"
    ) {
      query.quantity = { $gte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.cement_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.cement_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.cement_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "cement_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.business_name": 1,
          "address.location": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await CementQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getFlyAshStock(
    vendor_id: string,
    searchParam: IGetFlyAshSourceQuantityList
  ) {
    const query: { [k: string]: any } = {}

    //  query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "less"
    ) {
      query.quantity = { $lte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "more"
    ) {
      query.quantity = { $gte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.flyAsh_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.flyAsh_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.flyAsh_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: {
          path: "$fly_ash_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await FlyAshSourceQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getSendStock(
    vendor_id: string,
    searchParam: IGetSandSourceQuantityList
  ) {
    const query: { [k: string]: any } = {}

    // query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "less"
    ) {
      query.quantity = { $lte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.quantity) &&
      !isNullOrUndefined(searchParam.quantity_value) &&
      searchParam.quantity_value === "more"
    ) {
      query.quantity = { $gte: parseInt(searchParam.quantity) }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.sand_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.sand_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.sand_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "sand_source",
          localField: "source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: {
          path: "$fly_ash_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await SandSourceQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }

  async getBankDetails(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {}
    query["user.user_id"] = user_id

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.created_at = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.created_at = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.created_at = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }
    return BankInfoModel.find(query)
  }

  async getDesignMixtureVariants(user_id: string, searchParam: any) {
    const query: { [k: string]: any } = {
      vendor_id: new ObjectId(user_id),
    }

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = new ObjectId(searchParam.grade_id)
    }

    if (!isNullOrUndefined(searchParam.cement_brand_id)) {
      query.cement_brand_id = new ObjectId(searchParam.cement_brand_id)
    }
    if (!isNullOrUndefined(searchParam.sand_source_id)) {
      query.sand_source_id = new ObjectId(searchParam.sand_source_id)
    }
    if (!isNullOrUndefined(searchParam.aggregate_source_id)) {
      query.aggregate_source_id = new ObjectId(searchParam.aggregate_source_id)
    }
    if (!isNullOrUndefined(searchParam.aggregate1_sub_category_id)) {
      query.aggregate1_sub_category_id = new ObjectId(
        searchParam.aggregate1_sub_category_id
      )
    }
    if (!isNullOrUndefined(searchParam.aggregate2_sub_category_id)) {
      query.aggregate2_sub_category_id = new ObjectId(
        searchParam.aggregate2_sub_category_id
      )
    }
    if (!isNullOrUndefined(searchParam.fly_ash_source_id)) {
      query.fly_ash_source_id = new ObjectId(searchParam.fly_ash_source_id)
    }
    if (!isNullOrUndefined(searchParam.ad_mixture_brand_id)) {
      query.ad_mixture_brand_id = new ObjectId(searchParam.ad_mixture_brand_id)
    }
    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }
    if (!isNullOrUndefined(searchParam.cement_grade_id)) {
      query.cement_grade_id = new ObjectId(searchParam.cement_grade_id)
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== ""
    ) {
      query.variant_date = { $gte: searchParam.start_date }
    }
    if (
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.variant_date = { $lte: searchParam.end_date }
    }

    if (
      !isNullOrUndefined(searchParam.start_date) &&
      searchParam.start_date !== "" &&
      !isNullOrUndefined(searchParam.end_date) &&
      searchParam.end_date !== ""
    ) {
      query.variant_date = {
        $gte: searchParam.start_date,
        $lte: searchParam.end_date,
      }
    }

    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      {
        $project: {
          _id: true,
          design_mix_id: true,
          grade_id: true,
          cement_brand_id: true,
          sand_source_id: true,
          aggregate_source_id: true,
          fly_ash_source_id: true,
          aggregate1_sub_category_id: true,
          aggregate2_sub_category_id: true,
          ad_mixture_brand_id: true,
          product_name: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          ad_mixture_quantity: true,
          water_quantity: true,
          selling_price: true,
          is_availble: true,
          description: true,
          is_custom: true,
          address_id: true,
          ad_mixture_category_id: true,
          cement_grade_id: true,
          updated_by_id: true,
          updated_at: true,
          created_at: true,
        },
      },
    ]
    const designMixVariantData = await DesignMixVariantModel.aggregate(
      aggregateArr
    )
    if (designMixVariantData.length > 0) {
      let resp: any[] = []
      for (let i = 0; i < designMixVariantData.length; i++) {
        const concreteGradeDoc = await ConcreteGradeModel.findById({
          _id: designMixVariantData[i].grade_id,
        }).lean()
        if (!isNullOrUndefined(concreteGradeDoc)) {
          designMixVariantData[i].concrete_grade_name = concreteGradeDoc.name
        }

        const cementBrandDoc = await CementBrandModel.findById({
          _id: designMixVariantData[i].cement_brand_id,
        }).lean()
        if (!isNullOrUndefined(cementBrandDoc)) {
          designMixVariantData[i].cement_brand_name = cementBrandDoc.name
        }

        const sandSourceDoc = await SandSourceModel.findById({
          _id: designMixVariantData[i].sand_source_id,
        }).lean()
        if (!isNullOrUndefined(sandSourceDoc)) {
          designMixVariantData[i].sand_source_name =
            sandSourceDoc.sand_source_name
        }

        const aggregateSourceDoc = await AggregateSourceModel.findById({
          _id: designMixVariantData[i].aggregate_source_id,
        }).lean()
        if (!isNullOrUndefined(aggregateSourceDoc)) {
          designMixVariantData[i].aggregate_source_name =
            aggregateSourceDoc.aggregate_source_name
        }

        if (!isNullOrUndefined(designMixVariantData[i].fly_ash_source_id)) {
          const flyAshSourceDoc = await FlyAshSourceModel.findById({
            _id: designMixVariantData[i].fly_ash_source_id,
          }).lean()
          if (!isNullOrUndefined(flyAshSourceDoc)) {
            designMixVariantData[i].fly_ash_source_name =
              flyAshSourceDoc.fly_ash_source_name
          }
        }

        const aggregate1SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id: designMixVariantData[i].aggregate1_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate1SubCatDoc)) {
          designMixVariantData[i].aggregate1_sub_category_name =
            aggregate1SubCatDoc.sub_category_name
        }

        const aggregate2SubCatDoc = await AggregateSandSubCategoryModel.findById(
          {
            _id: designMixVariantData[i].aggregate2_sub_category_id,
          }
        ).lean()
        if (!isNullOrUndefined(aggregate2SubCatDoc)) {
          designMixVariantData[i].aggregate2_sub_category_name =
            aggregate2SubCatDoc.sub_category_name
        }

        if (!isNullOrUndefined(designMixVariantData[i].ad_mixture_brand_id)) {
          const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
            _id: designMixVariantData[i].ad_mixture_brand_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureBrandDoc)) {
            designMixVariantData[i].admix_brand_name = adMixtureBrandDoc.name
          }
        }

        const addressDoc = await AddressModel.findById({
          _id: designMixVariantData[i].address_id,
        }).lean()
        if (!isNullOrUndefined(addressDoc)) {
          designMixVariantData[i].address_line1 = addressDoc.line1
          designMixVariantData[i].address_line2 = addressDoc.line2
          designMixVariantData[i].pincode = addressDoc.pincode
          designMixVariantData[i].location = addressDoc.location
          designMixVariantData[i].business_name = addressDoc.business_name

          const cityDoc = await CityModel.findById({
            _id: addressDoc.city_id,
          }).lean()

          if (!isNullOrUndefined(cityDoc)) {
            designMixVariantData[i].city_name = cityDoc.city_name
          }

          const stateDoc = await StateModel.findById({
            _id: addressDoc.state_id,
          }).lean()

          if (!isNullOrUndefined(stateDoc)) {
            designMixVariantData[i].state_name = stateDoc.state_name
          }
        }

        if (
          !isNullOrUndefined(designMixVariantData[i].ad_mixture_category_id)
        ) {
          const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
            _id: designMixVariantData[i].ad_mixture_category_id,
          }).lean()
          if (!isNullOrUndefined(adMixtureCatDoc)) {
            designMixVariantData[i].admix_category_name =
              adMixtureCatDoc.category_name
          }
        }

        const cementGradeDoc = await CementGradeModel.findById({
          _id: designMixVariantData[i].cement_grade_id,
        }).lean()
        if (!isNullOrUndefined(cementGradeDoc)) {
          designMixVariantData[i].cement_grade_name = cementGradeDoc.name
        }

        resp.push(designMixVariantData[i])
      }

      return Promise.resolve(resp)
    }
    return Promise.reject(
      new InvalidInput(`No RMC Supplier found near the selected location.`, 400)
    )
  }
}
