import { IAuthenticatedRequest, verifyCustomToken } from "../../middleware/auth-token.middleware"
import { controller, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import { validate } from "../../middleware/joi.middleware"
import {
  supplierOrderReqSchema,
  IGetSupplierOrderRequest,
  IGetDesignMixtureRequest,
  getDesignMixListSchema,
  getDesignMixtureVariantDetailsSchema,
  IGetDesignMixtureVariantDetailsRequest,
  getTMSchema,
  IGetTMRequest,
  getConcretePumpSchema,
  IGetConcretePumpRequest,
  getAdmixtureQuantitySchema,
  IGetAdmixtureQuantityRequest,
  getAggregateSourceQuantitySchema,
  IGetAggregateSourceQuantityRequest,
  getCementQuantitySchema,
  IGetCementQuantityRequest,
  getFlyAshSourceQuantitySchema,
  IGetFlyAshSourceQuantityRequest,
  getSandSourceQuantitySchema,
  IGetSandSourceQuantityRequest,
} from "./vendor.report.req-schema"
import { Response, NextFunction } from "express"
import { VendorTypes } from "../vendor.types"
import { SupplierReportRepository } from "./vendor.report.repository"

@injectable()
@controller("/report", verifyCustomToken("vendor"))
export class SupplierReportController {
  constructor(
    @inject(VendorTypes.SupplierReportRepository)
    private reportRepo: SupplierReportRepository
  ) {}

  //  List of supplier order and order items.
  @httpGet("/order", validate(supplierOrderReqSchema))
  async vendorOrder(
    req: IGetSupplierOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.vendorOrder(
        req.user.uid,
        req.query.before,
        req.query.after,
        req.query.month,
        req.query.year,
        req.query.design_mix_id,
        req.query.buyer_id,
        req.query.status,
        req.query.is_export,
        req.query.start_date,
        req.query.end_date,
        req.query.order_id,
        req.query.buyer_order_id,
        req.query.payment_status,
        req.query.buyer_name
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/designMixList", validate(getDesignMixListSchema))
  async getDesignMixList(
    req: IGetDesignMixtureRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.reportRepo.getDesignMixList(req.query, req.user.uid)
    res.json({ data })
  }

  @httpGet(
    "/design_mix/variant/:design_mix_variant_id",
    validate(getDesignMixtureVariantDetailsSchema)
  )
  async getDesignMixtureVariantDetails(
    req: IGetDesignMixtureVariantDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { design_mix_variant_id } = req.params
      const data = await this.reportRepo.getDesignMixtureVariantDetails(
        design_mix_variant_id,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }


  @httpGet("/address")
   async vendorPlantAddressList(
     req: IAuthenticatedRequest,
     res: Response,
     next: NextFunction
   ) {
     try {
      const { uid } = req.user
      const { search, before, after, city_id, start_date, end_date} = req.query
      const data = await this.reportRepo.vendorPlantAddressList(uid, search, before, after, city_id, start_date, end_date )
       res.json({ data })
     } catch (err) {
       next(err)
     }
   }

   @httpGet("/billing_address")
   async vendorBillingAddressList(
     req: IAuthenticatedRequest,
     res: Response,
     next: NextFunction
   ) {
     try {
      const { uid } = req.user
      const { search, before, after, city_id, start_date, end_date } = req.query
      const data = await this.reportRepo.vendorBillingAddressList(uid, search, before, after, city_id, start_date, end_date)
       res.json({ data })
     } catch (err) {
       next(err)
     }
   }

   @httpGet("/driver")
   async getDriverList(
     req: IAuthenticatedRequest,
     res: Response,
     next: NextFunction
   ) {
     const data = await this.reportRepo.getDriverList(req.user.uid,req.query, req.query.start_date, req.query.end_date)
     res.json({ data })
   }

   @httpGet("/operator")
   async getOperatorList(
     req: IAuthenticatedRequest,
     res: Response,
     next: NextFunction
   ) {
     const data = await this.reportRepo.getOperatorList(req.user.uid, req.query)
     res.json({ data })
   }

   @httpGet("/TM", validate(getTMSchema))
   async getTM(req: IGetTMRequest, res: Response, next: NextFunction) {
     console.log(req.user.uid)
     const data = await this.reportRepo.getTM(req.user.uid, req.query)
     res.json({ data })
   }

   @httpGet("/CP", validate(getConcretePumpSchema))
   async getConcretePump(
     req: IGetConcretePumpRequest,
     res: Response,
     next: NextFunction
   ) {
     console.log(req.user.uid)
     const data = await this.reportRepo.getConcretePump(
       req.user.uid,
       req.query
     )
     res.json({ data })
   }

   @httpGet("/admix_stock", validate(getAdmixtureQuantitySchema))
   async getAdmixStock(
     req: IGetAdmixtureQuantityRequest,
     res: Response,
     next: NextFunction
   ) {
     console.log(req.user.uid)
     const data = await this.reportRepo.getAdmixStock(
       req.user.uid,
       req.query
     )
     res.json({ data })
   }

   @httpGet("/agg_stock", validate(getAggregateSourceQuantitySchema))
   async getAggStock(req: IGetAggregateSourceQuantityRequest, res: Response) {
     const data = await this.reportRepo.getAggStock(
       req.user.uid,
       req.query
     )
     res.json({ data })
   }

   @httpGet("/cement_stock", validate(getCementQuantitySchema))
   async getCementStock(req: IGetCementQuantityRequest, res: Response) {
     const data = await this.reportRepo.getCementStock(req.user.uid, req.query)
     res.json({ data })
   }

   @httpGet("/flyAsh_stock", validate(getFlyAshSourceQuantitySchema))
   async getFlyAshStock(req: IGetFlyAshSourceQuantityRequest, res: Response) {
     const data = await this.reportRepo.getFlyAshStock(req.user.uid, req.query)
     res.json({ data })
   }

   @httpGet("/sand_stock", validate(getSandSourceQuantitySchema))
   async getSendStock(req: IGetSandSourceQuantityRequest, res: Response) {
     const data = await this.reportRepo.getSendStock(req.user.uid, req.query)
     res.json({ data })
   }

   @httpGet("/bank")
   async getBankDetails(
     req: IAuthenticatedRequest,
     res: Response,
     next: NextFunction
   ) {
     try {
       const data = await this.reportRepo.getBankDetails(req.user.uid, req.query)
       res.json({ data })
     } catch (err) {
       next(err)
     }
   }

   @httpGet("/design_mix_variant")
  async getDesignMixtureVariants(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.reportRepo.getDesignMixtureVariants(
        req.user.uid, 
        req.query
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
