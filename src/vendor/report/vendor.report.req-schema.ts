import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode } from "../../utilities/config"

export const supplierOrderReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    design_mix_id: Joi.string().regex(objectIdRegex),
    buyer_id: Joi.string().regex(objectIdRegex),
    product_category_id: Joi.string().regex(objectIdRegex),
    product_sub_category_id: Joi.string().regex(objectIdRegex),
    status: Joi.string(),
    is_export: Joi.boolean(),
    start_date: Joi.string(),
    end_date: Joi.string(),
    order_id: Joi.string(),
    buyer_order_id: Joi.string(),
    buyer_name: Joi.string(),
    payment_status: Joi.string(),
  }),
}

export interface IGetSupplierOrderRequest {
  before?: string
  after?: string
  month?: string
  year?: number
  design_mix_id?: string
  buyer_id?: string
  product_category_id?: string
  product_sub_category_id?: string
  status?: string
  is_export?: boolean
  start_date: string
  end_date: string
  order_id: string
  buyer_order_id: string
  buyer_name: string
  payment_status: string
}
export interface IGetSupplierOrderRequest extends IAuthenticatedRequest {
  query: IGetSupplierOrderRequest
}

export const getProductListSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    product_category_id: Joi.string().regex(objectIdRegex),
    product_sub_category_id: Joi.string().regex(objectIdRegex),
    is_export: Joi.boolean(),
  }),
}

export interface IGetProductListRequest {
  before?: string
  after?: string
  product_category_id?: string
  product_sub_category_id?: string
  is_export?: boolean
}

export interface IGetProductListRequest extends IAuthenticatedRequest {
  query: IGetProductListRequest
}

export interface IGetDesignMixture {
  user_id?: string
  grade_id?: string
  before?: string
  after?: string
  is_export?: boolean
}

export interface IGetDesignMixtureRequest extends IAuthenticatedRequest {
  query: IGetDesignMixture
}

export const getDesignMixListSchema: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    is_export: Joi.boolean(),
  }),
}

export interface IGetDesignMixtureVariantDetailsRequest
  extends IAuthenticatedRequest {
  params: {
    design_mix_variant_id: string
  }
}

export const getDesignMixtureVariantDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    design_mix_variant_id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IGetTMDetails {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  TM_category_id?: string
  TM_sub_category_id?: string
  is_gps_enabled?: boolean
  is_insurance_active?: boolean
  address_id?: string
  TM_rc_number?: string
  start_date?: string
  end_date?: string
}

export interface IGetTMRequest extends IAuthenticatedRequest {
  query: IGetTMDetails
}

export const getTMSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    TM_category_id: Joi.string(),
    TM_sub_category_id: Joi.string().regex(objectIdRegex),
    is_gps_enabled: Joi.boolean(),
    is_insurance_active: Joi.boolean(),
    address_id: Joi.string().regex(objectIdRegex),
    TM_rc_number: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),

  }),
}

export interface IGetConcretePumpList {
  before?: string
  after?: string
  limit_count?: number
  is_active?: boolean
  concrete_pump_category_id?: string
  serial_number?: string
  address_id?: string
  start_date?: string
  end_date?: string
}

export interface IGetConcretePumpRequest extends IAuthenticatedRequest {
  query: IGetConcretePumpList
}

export const getConcretePumpSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    limit_count: Joi.number().min(1).max(100),
    is_active: Joi.boolean(),
    concrete_pump_category_id: Joi.string().regex(objectIdRegex),
    serial_number: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}
export interface IGetAdmixtureQuantityList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  category_id?: string
  quantity?: any
  quantity_value?: string
  start_date?: string
  end_date?: string
}

export interface IGetAdmixtureQuantityRequest extends IAuthenticatedRequest {
  query: IGetAdmixtureQuantityList
}

export const getAdmixtureQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    category_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetAggregateSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  sub_cat_id?: string
  quantity?: any
  quantity_value?: string
  start_date?: string
  end_date?: string
}

export interface IGetAggregateSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetAggregateSourceQuantityList
}

export const getAggregateSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    sub_cat_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetCementQuantityList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  grade_id?: string
  quantity?: any
  quantity_value?: string
  start_date?: string
  end_date?: string
}

export interface IGetCementQuantityRequest extends IAuthenticatedRequest {
  query: IGetCementQuantityList
}

export const getCementQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetFlyAshSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  quantity?: any
  quantity_value?: string
  start_date?: string
  end_date?: string
}

export interface IGetFlyAshSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetFlyAshSourceQuantityList
}

export const getFlyAshSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}

export interface IGetSandSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  quantity?: any
  quantity_value?: string
  start_date?: string
  end_date?: string
}

export interface IGetSandSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetSandSourceQuantityList
}

export const getSandSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
    start_date: Joi.string(),
    end_date: Joi.string(),
  }),
}
export interface IGetVendorOrder {
  user_id?: string
  before?: string
  after?: string
  design_mix_id?: string
  payment_status?: string
  order_status?: string
  buyer_id?: string
  address_id?: string
  order_id?: string
  month?: string
  year?: number
  date: Date
  buyer_order_id: string
  buyer_name: string
}

export interface IGetVendorOrderRequest extends IAuthenticatedRequest {
  query: IGetVendorOrder
}

export const getVendorOrder: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    design_mix_id: Joi.string().regex(objectIdRegex),
    payment_status: Joi.string(),
    order_status: Joi.string(),
    buyer_id: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
    buyer_order_id: Joi.string(),
    buyer_name: Joi.string(),
  }),
}