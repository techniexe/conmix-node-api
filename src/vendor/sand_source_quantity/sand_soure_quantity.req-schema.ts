import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ISandSourceQuantity {
  address_id: string
  source_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddSandSourceQuantityRequest extends IAuthenticatedRequest {
  body: ISandSourceQuantity
}

export const addSandSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export interface IEditSandSourceQuantity {
  address_id: string
  source_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  authentication_code: string
  [k: string]: any
}

export interface IEditSandSourceQuantityRequest extends IAuthenticatedRequest {
  params: {
    SandSourceQuantityId: string
  }
  body: IEditSandSourceQuantity
}

export const editSandSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number().min(1),
    authentication_code: Joi.string().required(),
  }),

  params: Joi.object().keys({
    SandSourceQuantityId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveSandSourceQuantityRequest
  extends IAuthenticatedRequest {
  params: {
    SandSourceQuantityId: string
  }
}

export const removeSandSourceQuantitySchema: IRequestSchema = {
  params: Joi.object().keys({
    SandSourceQuantityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetSandSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  quantity?: any
  quantity_value?: string
}

export interface IGetSandSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetSandSourceQuantityList
}

export const getSandSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
  }),
}
