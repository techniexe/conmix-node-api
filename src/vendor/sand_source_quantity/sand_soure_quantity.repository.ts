import { ObjectId } from "bson"
import { inject, injectable } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"

import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import {
  ISandSourceQuantity,
  IEditSandSourceQuantity,
  IGetSandSourceQuantityList,
} from "./sand_soure_quantity.req-schema"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AddressModel } from "../../model/address.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { SandOSModel } from "../../model/sand_os.model"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class SandSourceQuantityRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addSandSourceQuantity(
    vendor_id: string,
    sandSourceRateData: ISandSourceQuantity
  ) {
    const query: { [k: string]: any } = {
      address_id: sandSourceRateData.address_id,
      source_id: sandSourceRateData.source_id,
    }

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    let data = await SandSourceQuantityModel.findOne(query)

    // const data = await SandSourceQuantityModel.findOne({
    //   vendor_id,
    //   address_id: sandSourceRateData.address_id,
    //   source_id: sandSourceRateData.source_id,
    // })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Sand stock already exists for this source at selected site address.`,
          400
        )
      )
    }
    sandSourceRateData.vendor_id = vendor_id
    let sub_vendor_id: any
    if (!isNullOrUndefined(sandSourceRateData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(sandSourceRateData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
    }
    sandSourceRateData.sub_vendor_id = sub_vendor_id
    const newSandSourceQuantity = new SandSourceQuantityModel(
      sandSourceRateData
    )
    const newsandSourceDoc = await newSandSourceQuantity.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_SAND_SOURCE_QUANTITY_BY_VENDOR,
      `Vendor added sand source quantity of id ${newsandSourceDoc._id} .`
    )

    return Promise.resolve(newsandSourceDoc)
  }

  async editSandSourceQuantity(
    vendor_id: string,
    SandSourceQuantityId: string,
    editData: IEditSandSourceQuantity
  ): Promise<any> {
    const editDoc: { [k: string]: any } = {}

    const result = await AuthModel.findOne({
      code: editData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const query: { [k: string]: any } = {
      address_id: editData.address_id,
      source_id: editData.source_id,
    }

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    let data = await SandSourceQuantityModel.findOne(query)

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(SandSourceQuantityId) === false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Sand stock already exists for this source at selected site address.`,
          400
        )
      )
    }

    // const data = await SandSourceQuantityModel.findOne({
    //   vendor_id,
    //   address_id: sandSourceRateData.address_id,
    //   source_id: sandSourceRateData.source_id,
    // })

    // if (!isNullOrUndefined(data)) {
    //   return Promise.reject(
    //     new InvalidInput(
    //       `Sand stock already exists for this source at selected site address.`,
    //       400
    //     )
    //   )
    // }

    const edtFld = ["address_id", "source_id", "quantity", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if (!isNullOrUndefined(editData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    if (!isNullOrUndefined(editData.quantity)) {
      let vendor_capacity: any
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(editDoc.sub_vendor_id),
      })
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }

      let qty: any = editData.quantity * vendor_capacity
      if (qty > 640 * vendor_capacity) {
        await SandOSModel.findOneAndRemove({
          address_id: editData.address_id,
          sub_vendor_id: editDoc.sub_vendor_id,
        })
      }
    }
    const edtRes = await SandSourceQuantityModel.updateOne(
      {
        _id: new ObjectId(SandSourceQuantityId),
        // vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`No Sand source quantity found to edit`, 400)
      )
    }

    await AuthModel.deleteOne({
      code: editData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_SAND_SOURCE_QUANTITY_BY_VENDOR,
      "Sand source quantity information Updated."
    )

    return Promise.resolve()
  }
  async removeSandSourceQuantity(
    SandSourceQuantityId: string,
    authentication_code: string
  ) {
    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const status = await SandSourceQuantityModel.deleteOne({
      _id: new ObjectId(SandSourceQuantityId),
    })
    if (status.n !== 1) {
      throw new Error("Sand source quantity has not been added")
    }

    await AuthModel.deleteOne({
      code: authentication_code,
    })

    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetSandSourceQuantityList) {
    const query: { [k: string]: any } = {}

    // query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (!isNullOrUndefined(searchParam.quantity) &&
    !isNullOrUndefined(searchParam.quantity_value) &&
    searchParam.quantity_value === "less") {
      query.quantity = {$lte: parseInt(searchParam.quantity)}
    }

    if (!isNullOrUndefined(searchParam.quantity) &&
    !isNullOrUndefined(searchParam.quantity_value) &&
    searchParam.quantity_value === "more") {
      query.quantity = {$gte: parseInt(searchParam.quantity)}
    }
    
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "sand_source",
          localField: "source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: {
          path: "$fly_ash_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await SandSourceQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
