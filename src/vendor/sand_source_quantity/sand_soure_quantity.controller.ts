import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { SandSourceQuantityRepository } from "./sand_soure_quantity.repository"
import {
  addSandSourceQuantitySchema,
  IAddSandSourceQuantityRequest,
  editSandSourceQuantitySchema,
  IEditSandSourceQuantityRequest,
  removeSandSourceQuantitySchema,
  IRemoveSandSourceQuantityRequest,
  getSandSourceQuantitySchema,
  IGetSandSourceQuantityRequest,
} from "./sand_soure_quantity.req-schema"

@injectable()
@controller("/sand_source_quantity", verifyCustomToken("vendor"))
export class SandSourceQuantityController {
  constructor(
    @inject(VendorTypes.SandSourceQuantityRepository)
    private sandSourceRateRepo: SandSourceQuantityRepository
  ) {}

  @httpPost("/", validate(addSandSourceQuantitySchema))
  async addSandSourceQuantity(
    req: IAddSandSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.sandSourceRateRepo.addSandSourceQuantity(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:SandSourceQuantityId", validate(editSandSourceQuantitySchema))
  async editSandSourceQuantity(
    req: IEditSandSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.sandSourceRateRepo.editSandSourceQuantity(
      req.user.uid,
      req.params.SandSourceQuantityId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete(
    "/:SandSourceQuantityId",
    validate(removeSandSourceQuantitySchema)
  )
  async removeSandSourceQuantity(
    req: IRemoveSandSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.sandSourceRateRepo.removeSandSourceQuantity(
        req.params.SandSourceQuantityId,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getSandSourceQuantitySchema))
  async List(req: IGetSandSourceQuantityRequest, res: Response) {
    const data = await this.sandSourceRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
