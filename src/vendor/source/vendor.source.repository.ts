import { injectable } from "inversify"
import { VendorUserModel } from "../../model/vendor.user.model"
import { AggregateSourceModel } from "../../model/aggregate_source.model"
import { ISourceList } from "./vendor.source.req.schema"
import { escapeRegExp } from "../../utilities/mongoose.utilities"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { StateModel } from "../../model/region.model"

@injectable()
export class SupplierSourceRepository {
  async sourceList(searchParam: ISourceList) {
    const query: { [k: string]: any } = {}

    if (!isNullOrUndefined(searchParam.source_name)) {
      query.source_name = new RegExp(escapeRegExp(searchParam.source_name), "i")
    }

    if (!isNullOrUndefined(searchParam.region_id)) {
      query.region_id = searchParam.region_id
    }

    return AggregateSourceModel.find(query).select({
      region_id: 1,
      source_name: 1,
      created_by_id: 1,
      created_at: 1,
      updated_by_id: 1,
      updated_at: 1,
    })
  }

  async getRegion(user_id: string) {
    let supplierData = await VendorUserModel.findById(user_id).select({
      region_served: 1,
    })
    if (
      !isNullOrUndefined(supplierData) &&
      supplierData.region_served.length > 0
    ) {
      let region_name: any[] = []
      for (let i = 0; i < supplierData.region_served.length; i++) {
        let doc = await StateModel.findById(supplierData.region_served[i])
        if (!isNullOrUndefined(doc)) {
          region_name.push(doc.state_name)
        }
      }
      supplierData.region_name = region_name
      return Promise.resolve(supplierData)
    }
  }
}
