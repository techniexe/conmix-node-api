import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { SupplierSourceRepository } from "./vendor.source.repository"
import { VendorTypes } from "../vendor.types"
import { Response } from "express"

import {
  sourceListReqSchema,
  ISourceListReq,
} from "./vendor.source.req.schema"
import { validate } from "../../middleware/joi.middleware"

@injectable()
@controller("/source", verifyCustomToken("supplier"))
export class SupplierSourceController {
  constructor(
    @inject(VendorTypes.SupplierSourceRepository)
    private sourceRepo: SupplierSourceRepository
  ) {}

  @httpGet("/", validate(sourceListReqSchema))
  async sourceList(req: ISourceListReq, res: Response) {
    const data = await this.sourceRepo.sourceList(req.query)
    res.json({ data })
  }

  @httpGet("/region")
  async getRegion(req: IAuthenticatedRequest, res: Response) {
    const data = await this.sourceRepo.getRegion(req.user.uid)
    res.json({ data })
  }
}
