import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ISourceList {
  source_name: string
  region_id: string
}

export const sourceListReqSchema: IRequestSchema = {
  query: Joi.object().keys({
    source_name: Joi.string(),
    region_id: Joi.string(),
  }),
}

export interface ISourceListReq extends IAuthenticatedRequest {
  body: ISourceList
}
