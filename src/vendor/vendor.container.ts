import "reflect-metadata"
import { Container } from "inversify"
import { TYPE } from "inversify-express-utils"
import { SupplierProductController } from "./product/vendor.product.controller"
import { VendorTypes } from "./vendor.types"
import { VendorUserController } from "./user/vendor.user.controller"
import { VendorUserRepository } from "./user/vendor.user.repository"
import { VendorAuthController } from "./auth/vendor.auth.controller"
import { VendorAuthRepository } from "./auth/vendor.auth.repository"
import { TwilioService } from "../utilities/twilio-service"
import { MailService } from "../utilities/mail-service"
import { AddressController } from "./address/vendor.address.controller"
import { AddressRepository } from "./address/vendor.address.repository"
import { SupplierProductRepository } from "./product/vendor.product.repository"
import { ContactpersonController } from "./contact-person/vendor.contact-person.controller"
import { ContactpersonRepository } from "./contact-person/vendor.contact-person.repository"
import { BankController } from "./bank/vendor.bank.controller"
import { BankRepository } from "./bank/vendor.bank.repository"
import { CommonService } from "../utilities/common.service"
import { OrderController } from "./order/vendor.order.controller"
import { OrderRepository } from "./order/vendor.order.repository"
import { DashboardController } from "./dashboard/vendor.dashboard.controller"
import { DashboardRepository } from "./dashboard/vendor.dashboard.repository"
import { SnsService } from "../utilities/sns.service"
import { SupportTicketController } from "./support_ticket/vendor.support_ticket.controller"
import { SupportTicketRepository } from "./support_ticket/vendor.support_ticket.repository"
import { UniqueidService } from "../utilities/uniqueid-service"
import { SupplierSourceController } from "./source/vendor.source.controller"
import { SupplierSourceRepository } from "./source/vendor.source.repository"
import { SupplierRfpController } from "./rfp/vendor.rfp.controller"
import { SupplierRfpRepository } from "./rfp/vendor.rfp.repository"
import { SupplierReportController } from "./report/vendor.report.controller"
import { SupplierReportRepository } from "./report/vendor.report.repository"
import { DriverController } from "./driver/vendor.driver.controller"
import { DriverRepository } from "./driver/vendor.driver.repository"
import { OperatorController } from "./operator/vendor.operator.controller"
import { OperatorRepository } from "./operator/vendor.operator.repository"
import { VendorTMController } from "./TM/vendor.TM.controller"
import { VendorTMRepository } from "./TM/vendor.TM.repository"
import { VendorConcretePumpController } from "./concerete_pump/vendor.concrete_pump.controller"
import { VendorConcretePumpRepository } from "./concerete_pump/vendor.concrete_pump.repository"
import { VendorDesignMixtureController } from "./design_mixture/vendor.design_mix.controller"
import { VendorDesignMixtureRepository } from "./design_mixture/vendor.design_mix.repository"
import { VendorMediaController } from "./media/vendor.media.controller"
import { VendorMediaRepository } from "./media/vendor.media.repository"
import { VendorSettingController } from "./setting/vendor.setting.controller"
import { VendorSettingRepository } from "./setting/vendor.setting.repository"
import { CementRateController } from "./cement_rate/vendor.cement_rate.controller"
import { CementRateRepository } from "./cement_rate/vendor.cement_rate.repository"
import { SandSourceRateController } from "./sand_source_rate/sand_soure_rate.controller"
import { SandSourceRateRepository } from "./sand_source_rate/sand_soure_rate.repository"
import { FlyAshSourceRateController } from "./fly_ash_source_rate/fly_ash_soure_rate.controller"
import { FlyAshSourceRateRepository } from "./fly_ash_source_rate/fly_ash_soure_rate.repository"
import { AggregateSourceRateController } from "./aggregate_source_rate/aggregate_soure_rate.controller"
import { AggregateSourceRateRepository } from "./aggregate_source_rate/aggregate_soure_rate.repository"
import { AdmixtureRateController } from "./admixture_rate/admixture_rate.controller"
import { AdmixtureRateRepository } from "./admixture_rate/admixture_rate.repository"
import { WaterRateController } from "./water_rate/vendor.water_rate.controller"
import { WaterRateRepository } from "./water_rate/vendor.water_rate.repository"
import { AdmixtureQuantityController } from "./admiture_quantity/admixture_quantity.controller"
import { AdmixtureQuantityRepository } from "./admiture_quantity/admixture_quantity.repository"
import { AggregateSourceQuantityController } from "./aggregate_source_quantity/aggregate_soure_quantity.controller"
import { AggregateSourceQuantityRepository } from "./aggregate_source_quantity/aggregate_soure_quantity.repository"
import { CementQuantityController } from "./cement_quantity/vendor.cement_quantity.controller"
import { CementQuantityRepository } from "./cement_quantity/vendor.cement_quantity.repository"
import { FlyAshSourceQuantityController } from "./fly_ash_source_quantity/fly_ash_soure_quantity.controller"
import { FlyAshSourceQuantityRepository } from "./fly_ash_source_quantity/fly_ash_soure_quantity.repository"
import { SandSourceQuantityController } from "./sand_source_quantity/sand_soure_quantity.controller"
import { SandSourceQuantityRepository } from "./sand_source_quantity/sand_soure_quantity.repository"
import { VendorTMunavailbiltyController } from "./TM/unavailbilty/TM_unavailbilty.controller"
import { VendorTMunavailbiltyRepository } from "./TM/unavailbilty/TM_unavailbilty.repository"
import { VendorCPunavailbiltyController } from "./concerete_pump/unavailbilty/CP_unavailbilty.controller"
import { VendorCPunavailbiltyRepository } from "./concerete_pump/unavailbilty/CP_unavailbilty.repository"
import { NotificationUtility } from "../utilities/notification.utility"
import { VendorOrderTrackController } from "./order_track/vendor.order_track.controller"
import { VendorOrderTrackRepository } from "./order_track/vendor.order_track.repository"
import { VendorSurpriseOrderController } from "./surprise_order/vendor.surprise_order.controller"
import { VendorSurpriserOrderRepository } from "./surprise_order/vendor.surprise_order.repository"
import { NotificationController } from "./notification/vendor.notification.controller"
import { NotificationRepository } from "./notification/vendor.notification.repository"
import { VendorAdMixSettingController } from "./admixture_setting/admix_setting.controller"
import { VendorAdMixSettingRepository } from "./admixture_setting/admix_setting.repository"
import { VendorMyInfoController } from "./my_info/vendor.my_info.controller"
import { VendorMyInfoRepository } from "./my_info/vendor.my_info.repository"
import { BillingAddressController } from "./billing_address/vendor.billing_address.controller"
import { BillingAddressRepository } from "./billing_address/vendor.billing_address.repository"
import { TestService } from "../utilities/test.service"
import { MailGunService } from "../utilities/mailgun.service"

export const vendorContainer = new Container()

vendorContainer
  .bind(VendorTypes.TwilioService)
  .to(TwilioService)
  .inSingletonScope()

vendorContainer.bind(VendorTypes.MailService).to(MailService).inSingletonScope()

vendorContainer
  .bind(VendorTypes.TestService)
  .to(TestService)
  .inSingletonScope()

vendorContainer
  .bind(VendorTypes.CommonService)
  .to(CommonService)
  .inSingletonScope()

vendorContainer
  .bind(VendorTypes.mailgunService)
  .to(MailGunService)
  .inSingletonScope()  

vendorContainer
  .bind(TYPE.Controller)
  .to(SupplierProductController)
  .whenTargetNamed(VendorTypes.SupplierProductController)

vendorContainer
  .bind(VendorTypes.SupplierProductRepositroy)
  .to(SupplierProductRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorUserController)
  .whenTargetNamed(VendorTypes.VendorUserController)

vendorContainer
  .bind(VendorTypes.VendorUserRepository)
  .to(VendorUserRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorAuthController)
  .whenTargetNamed(VendorTypes.VendorAuthController)
vendorContainer
  .bind(VendorTypes.VendorAuthRepository)
  .to(VendorAuthRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(AddressController)
  .whenTargetNamed(VendorTypes.AddressController)

vendorContainer
  .bind(VendorTypes.AddressRepository)
  .to(AddressRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(ContactpersonController)
  .whenTargetNamed(VendorTypes.ContactpersonController)

vendorContainer
  .bind(VendorTypes.ContactpersonRepository)
  .to(ContactpersonRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(BankController)
  .whenTargetNamed(VendorTypes.BankController)

vendorContainer
  .bind(VendorTypes.BankRepository)
  .to(BankRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(OrderController)
  .whenTargetNamed(VendorTypes.OrderController)

vendorContainer
  .bind(VendorTypes.OrderRepository)
  .to(OrderRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(DashboardController)
  .whenTargetNamed(VendorTypes.DashboardController)

vendorContainer
  .bind(VendorTypes.DashboardRepository)
  .to(DashboardRepository)
  .inSingletonScope()

vendorContainer.bind(VendorTypes.SnsService).to(SnsService).inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SupportTicketController)
  .whenTargetNamed(VendorTypes.SupportTicketController)
vendorContainer
  .bind(VendorTypes.SupportTicketRepository)
  .to(SupportTicketRepository)
  .inSingletonScope()

vendorContainer
  .bind(VendorTypes.UniqueidService)
  .to(UniqueidService)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SupplierSourceController)
  .whenTargetNamed(VendorTypes.SupplierSourceController)

vendorContainer
  .bind(VendorTypes.SupplierSourceRepository)
  .to(SupplierSourceRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SupplierRfpController)
  .whenTargetNamed(VendorTypes.SupplierRfpController)

vendorContainer
  .bind(VendorTypes.SupplierRfpRepository)
  .to(SupplierRfpRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SupplierReportController)
  .whenTargetNamed(VendorTypes.SupplierReportController)

vendorContainer
  .bind(VendorTypes.SupplierReportRepository)
  .to(SupplierReportRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(DriverController)
  .whenTargetNamed(VendorTypes.DriverController)

vendorContainer
  .bind(VendorTypes.DriverRepository)
  .to(DriverRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(OperatorController)
  .whenTargetNamed(VendorTypes.OperatorController)

vendorContainer
  .bind(VendorTypes.OperatorRepository)
  .to(OperatorRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorTMController)
  .whenTargetNamed(VendorTypes.VendorTMController)

vendorContainer
  .bind(VendorTypes.VendorTMRepository)
  .to(VendorTMRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorConcretePumpController)
  .whenTargetNamed(VendorTypes.VendorConcretePumpController)

vendorContainer
  .bind(VendorTypes.VendorConcretePumpRepository)
  .to(VendorConcretePumpRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorDesignMixtureController)
  .whenTargetNamed(VendorTypes.VendorDesignMixtureController)

vendorContainer
  .bind(VendorTypes.VendorDesignMixtureRepository)
  .to(VendorDesignMixtureRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorMediaController)
  .whenTargetNamed(VendorTypes.VendorMediaController)

vendorContainer
  .bind(VendorTypes.VendorMediaRepository)
  .to(VendorMediaRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorSettingController)
  .whenTargetNamed(VendorTypes.VendorSettingController)

vendorContainer
  .bind(VendorTypes.VendorSettingRepository)
  .to(VendorSettingRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(CementRateController)
  .whenTargetNamed(VendorTypes.CementRateController)

vendorContainer
  .bind(VendorTypes.CementRateRepository)
  .to(CementRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SandSourceRateController)
  .whenTargetNamed(VendorTypes.SandSourceRateController)

vendorContainer
  .bind(VendorTypes.SandSourceRateRepository)
  .to(SandSourceRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(FlyAshSourceRateController)
  .whenTargetNamed(VendorTypes.FlyAshSourceRateController)

vendorContainer
  .bind(VendorTypes.FlyAshSourceRateRepository)
  .to(FlyAshSourceRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(AggregateSourceRateController)
  .whenTargetNamed(VendorTypes.AggregateSourceRateController)

vendorContainer
  .bind(VendorTypes.AggregateSourceRateRepository)
  .to(AggregateSourceRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(AdmixtureRateController)
  .whenTargetNamed(VendorTypes.AdmixtureRateController)

vendorContainer
  .bind(VendorTypes.AdmixtureRateRepository)
  .to(AdmixtureRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(WaterRateController)
  .whenTargetNamed(VendorTypes.WaterRateController)

vendorContainer
  .bind(VendorTypes.WaterRateRepository)
  .to(WaterRateRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(AdmixtureQuantityController)
  .whenTargetNamed(VendorTypes.AdmixtureQuantityController)

vendorContainer
  .bind(VendorTypes.AdmixtureQuantityRepository)
  .to(AdmixtureQuantityRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(AggregateSourceQuantityController)
  .whenTargetNamed(VendorTypes.AggregateSourceQuantityController)

vendorContainer
  .bind(VendorTypes.AggregateSourceQuantityRepository)
  .to(AggregateSourceQuantityRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(CementQuantityController)
  .whenTargetNamed(VendorTypes.CementQuantityController)

vendorContainer
  .bind(VendorTypes.CementQuantityRepository)
  .to(CementQuantityRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(FlyAshSourceQuantityController)
  .whenTargetNamed(VendorTypes.FlyAshSourceQuantityController)

vendorContainer
  .bind(VendorTypes.FlyAshSourceQuantityRepository)
  .to(FlyAshSourceQuantityRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(SandSourceQuantityController)
  .whenTargetNamed(VendorTypes.SandSourceQuantityController)

vendorContainer
  .bind(VendorTypes.SandSourceQuantityRepository)
  .to(SandSourceQuantityRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorTMunavailbiltyController)
  .whenTargetNamed(VendorTypes.VendorTMunavailbiltyController)

vendorContainer
  .bind(VendorTypes.VendorTMunavailbiltyRepository)
  .to(VendorTMunavailbiltyRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorCPunavailbiltyController)
  .whenTargetNamed(VendorTypes.VendorCPunavailbiltyController)

vendorContainer
  .bind(VendorTypes.VendorCPunavailbiltyRepository)
  .to(VendorCPunavailbiltyRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorOrderTrackController)
  .whenTargetNamed(VendorTypes.VendorOrderTrackController)

vendorContainer
  .bind(VendorTypes.VendorOrderTrackRepository)
  .to(VendorOrderTrackRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorSurpriseOrderController)
  .whenTargetNamed(VendorTypes.VendorSurpriseOrderController)

vendorContainer
  .bind(VendorTypes.VendorSurpriserOrderRepository)
  .to(VendorSurpriserOrderRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorAdMixSettingController)
  .whenTargetNamed(VendorTypes.VendorAdMixSettingController)

vendorContainer
  .bind(VendorTypes.VendorAdMixSettingRepository)
  .to(VendorAdMixSettingRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(NotificationController)
  .whenTargetNamed(VendorTypes.NotificationController)

vendorContainer
  .bind(VendorTypes.NotificationRepository)
  .to(NotificationRepository)
  .inSingletonScope()

vendorContainer
  .bind(VendorTypes.NotificationUtility)
  .to(NotificationUtility)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(VendorMyInfoController)
  .whenTargetNamed(VendorTypes.VendorMyInfoController)

vendorContainer
  .bind(VendorTypes.VendorMyInfoRepository)
  .to(VendorMyInfoRepository)
  .inSingletonScope()

vendorContainer
  .bind(TYPE.Controller)
  .to(BillingAddressController)
  .whenTargetNamed(VendorTypes.BillingAddressController)

vendorContainer
  .bind(VendorTypes.BillingAddressRepository)
  .to(BillingAddressRepository)
  .inSingletonScope()

