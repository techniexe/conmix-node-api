import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { OperatorRepository } from "./vendor.operator.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addOperatorDetailsSchema,
  IAddOperatorDetailsRequest,
  editOperatorRequestSchema,
  IEditOperatorRequest,
  getOperatorDetailsRequest,
  IGetOperatorDetailsRequest,
} from "./vendor.operator.req-schema"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/operator", verifyCustomToken("vendor"))
export class OperatorController {
  constructor(
    @inject(VendorTypes.OperatorRepository)
    private OperatorRepo: OperatorRepository
  ) {}

  @httpPost("/", validate(addOperatorDetailsSchema))
  async addOperatorDetails(
    req: IAddOperatorDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      await this.OperatorRepo.addOperatorDetails(uid, req.body, req.files)
      return res.sendStatus(200)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This Operator details is already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpPatch("/:operator_id", validate(editOperatorRequestSchema))
  async editOperator(
    req: IEditOperatorRequest,
    res: Response,
    next: NextFunction
  ) {
    try{
      await this.OperatorRepo.editOperator(
        req.user.uid,
        req.params.operator_id,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This Operator details is already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }

  }

  @httpGet("/")
  async getOperatorList(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.OperatorRepo.getOperatorList(req.user.uid, req.query)
    res.json({ data })
  }

  @httpGet("/:operator_id", validate(getOperatorDetailsRequest))
  async getOperatorDetails(
    req: IGetOperatorDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.OperatorRepo.getOperatorDetails(
      req.user.uid,
      req.params.operator_id
    )
    res.json({ data })
  }
}
