import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface IOperator {
  user: {
    user_id?: string
    user_type?: string
  }
  operator_name: string
  operator_mobile_number: string
  operator_alt_mobile_number: string
  operator_whatsapp_number: string
  operator_pic: string
  sub_vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddOperatorDetailsRequest extends IAuthenticatedRequest {
  body: IOperator
  files: {
    operator_pic: UploadedFile
  }
}

export const addOperatorDetailsSchema: IRequestSchema = {
  body: Joi.object().keys({
    operator_name: Joi.string().required(),
    operator_mobile_number: Joi.string().required(),
    operator_alt_mobile_number: Joi.string(),
    operator_whatsapp_number: Joi.string(),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export const editOperatorRequestSchema: IRequestSchema = {
  body: Joi.object().keys({
    operator_name: Joi.string(),
    operator_mobile_number: Joi.string(),
    operator_alt_mobile_number: Joi.string(),
    operator_whatsapp_number: Joi.string(),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    operator_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IEditOperator {
  operator_name: string
  operator_mobile_number: string
  operator_alt_mobile_number: string
  operator_whatsapp_number: string
  operator_pic: string
  sub_vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditOperatorRequest extends IAuthenticatedRequest {
  params: {
    operator_id: string
  }
  files: {
    operator_pic: UploadedFile
  }
  body: IEditOperator
}

export const getOperatorDetailsRequest: IRequestSchema = {
  params: Joi.object().keys({
    operator_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetOperatorDetailsRequest extends IAuthenticatedRequest {
  params: {
    operator_id: string
  }
}
