import { OperatorInfoModel } from "./../../model/operator.model"
import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { IOperator, IEditOperator } from "./vendor.operator.req-schema"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { AccessTypes } from "../../model/access_logs.model"
import { ObjectId } from "bson"

@injectable()
export class OperatorRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addOperatorDetails(
    user_id: string,
    operatorData: IOperator,
    fileData: { operator_pic: UploadedFile }
  ) {

    if (!isNullOrUndefined(operatorData.operator_mobile_number)) {
      const numberCheck = await OperatorInfoModel.findOne({
        operator_mobile_number: operatorData.operator_mobile_number,
      })

      if (!isNullOrUndefined(numberCheck)) {
        return Promise.reject(
          new InvalidInput(
            `This mobile no. is already associated with other operator.`,
            400
          )
        )
      }
    }


    operatorData.user = { user_id }
    operatorData.user.user_type = "vendor"
    const newOperator = new OperatorInfoModel(operatorData)
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.operator_pic) &&
      !isNullOrUndefined(fileData.operator_pic.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/operator`
      let objectName = "" + newOperator._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.operator_pic.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.operator_pic.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.operator_pic.data,
        fileData.operator_pic.mimetype
      )
      newOperator.operator_pic = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const newOperatorDoc = await newOperator.save()

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_OPERATORDETAILS_BY_VENDOR,
      `Vendor added Operator details of id ${newOperatorDoc._id} .`
    )

    return Promise.resolve(newOperatorDoc)
  }

  async editOperator(
    user_id: string,
    operator_id: string,
    editData: IEditOperator,
    fileData: {
      operator_pic: UploadedFile
    }
  ): Promise<any> {
    const editDoc: { [k: string]: any } = {}

    if (!isNullOrUndefined(editData.operator_mobile_number)) {
      const numberCheck = await OperatorInfoModel.findOne({
        operator_mobile_number: editData.operator_mobile_number,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(operator_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This mobile no. is already associated with other operator.`,
            400
          )
        )
      }
    }


    const edtFld = [
      "operator_name",
      "operator_mobile_number",
      "operator_alt_mobile_number",
      "operator_whatsapp_number",
      "sub_vendor_id",
      "master_vendor_id"
    ]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.operator_pic) &&
      !isNullOrUndefined(fileData.operator_pic.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/operator`
      let objectName = "" + operator_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.operator_pic.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.operator_pic.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.operator_pic.data,
        fileData.operator_pic.mimetype
      )
      editDoc.operator_pic = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const edtRes = await OperatorInfoModel.findOneAndUpdate(
      {
        _id: operator_id,
        //"user.user_id": user_id,
      },
      { $set: editDoc }
    )
    if (edtRes === null) {
      return Promise.reject(
        new InvalidInput(`No Operator info found to edit`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_OPERATORDETAILS_BY_VENDOR,
      `Vendor updated details Operator of id ${operator_id} .`,
      edtRes,
      editDoc
    )

    return Promise.resolve()
  }

  async getOperatorList(user_id: string, searchParam: any) {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    if (!isNullOrUndefined(searchParam.sub_vendor_id)) {
      query.sub_vendor_id = new ObjectId(searchParam.sub_vendor_id) 
    }


    return OperatorInfoModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "address",
          localField: "vendorDetails._id",
          foreignField: "sub_vendor_id",
          as: "addressDetails",
        },
      },
      {
        $unwind: { path: "$addressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: { path: "$masterVendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "addressDetails.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "addressDetails.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          operator_name: 1,
          operator_mobile_number: 1,
          operator_alt_mobile_number: 1,
          operator_whatsapp_number: 1,
          operator_pic : 1,
          created_at: 1,
          sub_vendor_id: 1,
          master_vendor_id: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name" : 1,
          "vendorDetails.mobile_number" : 1,
          "vendorDetails.email" : 1,
          "vendorDetails.password" : 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name" : 1,
          "masterVendorDetails.mobile_number" : 1,
          "masterVendorDetails.email" : 1,
          "masterVendorDetails.password" : 1,
         // addressDetails: 1,
           "addressDetails.user": 1,
            "addressDetails.state_id": 1,
            "addressDetails.city_id": 1,
            "addressDetails.address_type": 1,
            "addressDetails.business_name": 1,
            "addressDetails.region_id": 1,
            "addressDetails.source_id": 1,
            "addressDetails.created_at": 1,
            "addressDetails.updated_at": 1,
            "addressDetails.sub_vendor_id": 1,
            "addressDetails.master_vendor_id": 1,
            "addressDetails.billing_address_id": 1,
            "addressDetails.is_verified": 1,
            "addressDetails.is_accepted": 1,
            "addressDetails.is_blocked": 1,
            "addressDetails.plant_address_date": 1,
            "addressDetails.line1": 1,
            "addressDetails.line2": 1,
            "addressDetails.state_name": "$state.state_name",
            "addressDetails.city_name": "$city.city_name",
            "addressDetails.source_name": "$source.source_name",
            "addressDetails.pincode": 1,
            "addressDetails.location": 1,
        },
      },
    ])
  //  return OperatorInfoModel.find({ "user.user_id": user_id })
  }

  async getOperatorDetails(user_id: string, operator_id: string) {
    return OperatorInfoModel.findOne({
      "user.user_id": user_id,
      _id: new ObjectId(operator_id),
    })
  }
}
