import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { AggregateSourceQuantityRepository } from "./aggregate_soure_quantity.repository"
import {
  addAggregateSourceQuantitySchema,
  IAddAggregateSourceQuantityRequest,
  editAggregateSourceQuantitySchema,
  IEditAggregateSourceQuantityRequest,
  removeAggregateSourceQuantitySchema,
  IRemoveAggregateSourceQuantityRequest,
  getAggregateSourceQuantitySchema,
  IGetAggregateSourceQuantityRequest,
} from "./aggregate_soure_quantity.req-schema"

@injectable()
@controller("/aggregate_source_quantity", verifyCustomToken("vendor"))
export class AggregateSourceQuantityController {
  constructor(
    @inject(VendorTypes.AggregateSourceQuantityRepository)
    private aggregateSourceQuantityRepo: AggregateSourceQuantityRepository
  ) {}

  @httpPost("/", validate(addAggregateSourceQuantitySchema))
  async addAggregateSourceQuantity(
    req: IAddAggregateSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const {
        _id,
      } = await this.aggregateSourceQuantityRepo.addAggregateSourceQuantity(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:AggregateSourceQuantityId",
    validate(editAggregateSourceQuantitySchema)
  )
  async editAggregateSourceQuantity(
    req: IEditAggregateSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.aggregateSourceQuantityRepo.editAggregateSourceQuantity(
      req.user.uid,
      req.params.AggregateSourceQuantityId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete(
    "/:AggregateSourceQuantityId",
    validate(removeAggregateSourceQuantitySchema)
  )
  async removeAggregateSourceQuantity(
    req: IRemoveAggregateSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.aggregateSourceQuantityRepo.removeAggregateSourceQuantity(
        req.params.AggregateSourceQuantityId,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getAggregateSourceQuantitySchema))
  async List(req: IGetAggregateSourceQuantityRequest, res: Response) {
    const data = await this.aggregateSourceQuantityRepo.List(
      req.user.uid,
      req.query
    )
    res.json({ data })
  }
}
