import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAggregateSourceQuantity {
  address_id: string
  source_id: string
  sub_cat_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddAggregateSourceQuantityRequest extends IAuthenticatedRequest {
  body: IAggregateSourceQuantity
}

export const addAggregateSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex).required(),
    sub_cat_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export interface IEditAggregateSourceQuantity {
  address_id: string
  source_id: string
  sub_cat_id: string
  quantity: number
  vendor_id: string
  [k: string]: any
}

export interface IEditAggregateSourceQuantityRequest extends IAuthenticatedRequest {
  params: {
    AggregateSourceQuantityId: string
  }
  body: IEditAggregateSourceQuantity
}

export const editAggregateSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    sub_cat_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number().min(1),
    authentication_code: Joi.string().required(),
  }),

  params: Joi.object().keys({
    AggregateSourceQuantityId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveAggregateSourceQuantityRequest
  extends IAuthenticatedRequest {
  params: {
    AggregateSourceQuantityId: string
  }
}

export const removeAggregateSourceQuantitySchema: IRequestSchema = {
  params: Joi.object().keys({
    AggregateSourceQuantityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetAggregateSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  sub_cat_id?: string
  quantity?: any
  quantity_value?: string
}

export interface IGetAggregateSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetAggregateSourceQuantityList
}

export const getAggregateSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    sub_cat_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
  }),
}
