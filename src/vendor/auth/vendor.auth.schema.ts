import { IJWTAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"

export interface IGetAuthenticationCodeOnMobileRequest
  extends IJWTAuthenticatedRequest {
  params: {
    mobileNumber: string
  }
  query: {
    method?: "sms" | "call"
  }
}
export interface IVerifyAuthenticationCodeForMobileRequest
  extends IGetAuthenticationCodeOnMobileRequest {
  params: {
    mobileNumber: string
    code: string
  }
}

export const verifyAuthenticationCodeSchema: IRequestSchema = {
  params: Joi.object().keys({
    code: Joi.string()
      .length(6)
      .required(),
    mobileNumber: Joi.string().required(),
  }),
}
export const getFBCustomTokenSchema = Joi.object().keys({
  id: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  cover: Joi.string(),
})

export const getGoogleCustomTokenSchema = Joi.object().keys({
  // These six fields are included in all Google ID Tokens.
  iss: Joi.string(), // "https://accounts.google.com",
  sub: Joi.string().required(), // "130769484474386276338",
  azp: Joi.string(), // "<id>.apps.googleusercontent.com",
  aud: Joi.string(), // "<id>.apps.googleusercontent.com",
  iat: Joi.string(), // "1433978353",
  exp: Joi.string().required(), // "1433981953",

  // These seven fields are only included when the user has granted the "profile" and
  // "email" OAuth scopes to the application.
  email: Joi.string().required(), // "testuser@gmail.com",
  email_verified: Joi.string(),
  name: Joi.string().required(), // "Test User",
  picture: Joi.string(),
  given_name: Joi.string(),
  family_name: Joi.string(),
  locale: Joi.string(),
})

export const loginUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    password: Joi.string().required(),
  }),
}

export interface ILoginUserRequest extends IJWTAuthenticatedRequest {
  params: {
    identifier: string
  }
  body: {
    password: string
  }
}

export const EmailRegistrationSchema: IRequestSchema = {
  params: Joi.object().keys({
    email: Joi.string()
      .email()
      .required(),
  }),
}

export interface IGetEmailRegistrationRequest extends Request {
  params: {
    email: string
  }
}
