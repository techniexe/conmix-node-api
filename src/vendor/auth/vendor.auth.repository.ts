import { inject, injectable } from "inversify"
import {
  AuthModel,
  IAuthDoc,
  //emailVerificationType,
} from "../../model/auth.model"
import { VendorUserModel } from "../../model/vendor.user.model"
// import { generateRandomNumber } from "../../utilities/generate-random-number"
import { InvalidInput } from "../../utilities/customError"
import { MailGunService } from "../../utilities/mailgun.service"
import { TestService } from "../../utilities/test.service"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { VendorTypes } from "../vendor.types"
//import { CommonService } from "../../utilities/common.service"
//import { MailService } from "../../utilities/mail-service"
//import { emailVerificationLinkURI } from "../../utilities/config"
//import { ObjectID } from "bson"
//import { logger } from "../../logger"
//import { VendorTypes } from "../vendor.types"
//import { TwilioService } from "../../utilities/twilio-service"
import { ObjectId } from "bson"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
@injectable()
export class VendorAuthRepository {
   constructor(
     @inject(VendorTypes.TestService) private testService: TestService,
  // //  @inject(VendorTypes.MailService) private mailService: MailService,
  // //  @inject(VendorTypes.TwilioService) private twilioService: TwilioService
  @inject(VendorTypes.mailgunService) private mailgunService: MailGunService 
  ) {}
  async getVendorAuthCode(
    identifier: string,
    user_id?: string
  ): Promise<IAuthDoc | null> {
    const code: any = generateRandomNumber()
    const $setOnInsert: { [k: string]: any } = {
      identifier,
      user_type: "vendor",
      code,
    }

    if (!isNullOrUndefined(user_id)) {
      $setOnInsert.user_id = user_id
    }
    const authData = await AuthModel.findOneAndUpdate(
      { identifier, user_type: "vendor" },
      {
        $set: { created_at: Date.now() },
        $setOnInsert,
      },
      { upsert: true, new: true }
    ).exec()

    console.log(authData)
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }
    let vendorUserDoc: any
    if (!isNullOrUndefined(user_id)) {
     vendorUserDoc = await VendorUserModel.findOne({
      _id: new ObjectId(user_id)
    })
    if(isNullOrUndefined(vendorUserDoc)){
      return Promise.reject(new InvalidInput(`No vendor doc found`, 400))
    }
  }
    //let message = `${authData.code} is an OTP for your Partner Account Registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    //let message = `${authData.code} is the OTP to Reset Password for Conmix Admin. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
    // let message = `${authData.code} is an OTP to Reset Password for your Partner Account at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`  
    // let templateId = "1707163731244954184"
    // await this.testService.sendSMS(identifier, message, templateId)

    // let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // // Registration OTP.
    // let templateId = "1707163645095255064"
    // await this.testService.sendSMS(identifier, message, templateId)

    let message = `${authData.code} is an OTP for your Partner Account Registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163731237397701"
    await this.testService.sendSMS(identifier, message, templateId)
  //   var data = {
  //     from: "no-reply@conmate.in",
  //     to: identifier,
  //     subject: 'Conmix - Forgot Password One Time Password',
  //     html : `Dear Vendor,<br>
  //     ${authData.code} is an OTP to Reset Password for your Partner Account at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others. 
  //     <br>  <br> <br>  
      
  //     Regards,<br>
  //     Conmate E-Commerce Pvt. Ltd.`
  //   }
  //  await this.mailgunService.sendEmail(data)

  let one_time_pwd_html = vendor_mail_template.one_time_pwd
  one_time_pwd_html = one_time_pwd_html.replace("{{code}}", code)
   var data = {
    from: "no-reply@conmate.in",
    to: identifier,
    subject: "Conmix - One Time Password",
    html: one_time_pwd_html,
  }
  await this.mailgunService.sendEmail(data)

    return Promise.resolve(authData)
  }

  async getVendorAuthCodeForResetPwd( identifier: string,
    user_id?: string): Promise<IAuthDoc | null> {
      const code = generateRandomNumber()
     
      let query: { [k: string]: any } = {}
      query.$or = [{
       mobile_number : identifier
      },
      {
       email : identifier
      }
     ]
     let vendorUserData = await VendorUserModel.findOne(query)
     if(!isNullOrUndefined(vendorUserData)){
        if(!isNullOrUndefined(vendorUserData.master_vendor_id)){
          return Promise.reject(
            new InvalidInput(`You are not authorized to reset password.`, 400)
          )
        }
     }
     
      const $setOnInsert: { [k: string]: any } = {
        identifier,
        user_type: "vendor",
        code,
      }
  
      if (!isNullOrUndefined(user_id)) {
        $setOnInsert.user_id = user_id
      }
      const authData = await AuthModel.findOneAndUpdate(
        { identifier, user_type: "vendor" },
        {
          $set: { created_at: Date.now() },
          $setOnInsert,
        },
        { upsert: true, new: true }
      ).exec()
  
      console.log(authData)
      if (authData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }
      let vendorUserDoc: any
      if (!isNullOrUndefined(user_id)) {
       vendorUserDoc = await VendorUserModel.findOne({
        _id: new ObjectId(user_id)
      })
      if(isNullOrUndefined(vendorUserDoc)){
        return Promise.reject(new InvalidInput(`No vendor doc found`, 400))
      }
    }

    console.log("vendorUserDoc", vendorUserDoc)
      //let message = `${authData.code} is an OTP for your Partner Account Registration at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      //let message = `${authData.code} is the OTP to Reset Password for Conmix Admin. It is valid upto 60 seconds. Kindly do not share the OTP with others.`
      let message = `${authData.code} is an OTP to Reset Password for your Partner Account at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.`  
      let templateId = "1707163731244954184"
      await this.testService.sendSMS(identifier, message, templateId)

      let forgot_pwd_otp_mail_html = vendor_mail_template.forgot_pwd_otp
      forgot_pwd_otp_mail_html = forgot_pwd_otp_mail_html.replace("{{authData.code}}", authData.code)
      var data = {
        from: "no-reply@conmate.in",
        to: identifier,
        subject: 'Conmix - Forgot Password One Time Password',
        html : forgot_pwd_otp_mail_html
      }
     await this.mailgunService.sendEmail(data)
     return Promise.resolve(authData)
    }
  async checkVerificationCodeWithDb(
    identifier: string,
    user_type: string,
    code: string
  ): Promise<IAuthDoc | null> {
    return AuthModel.findOne({ identifier, code, user_type }).exec()
  }

  async sendEmailforRegistration(identifier: string, user_id?: string) {
    const code = generateRandomNumber()
    const $setOnInsert: { [k: string]: any } = {
      identifier,
      user_type: "vendor",
      code,
    }
    if (!isNullOrUndefined(user_id)) {
      $setOnInsert.user_id = user_id
    }
    const authData = await AuthModel.findOneAndUpdate(
      { identifier, user_type: "vendor" },
      {
        $set: { created_at: Date.now() },
        $setOnInsert,
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let reg_otp_mail_html = vendor_mail_template.registration_otp
    reg_otp_mail_html = reg_otp_mail_html.replace("{{authData.code}}", authData.code)
    var data = {
      from: "no-reply@conmate.in",
      to: identifier,
      subject: 'Conmix - Registration One Time Password',
      html : reg_otp_mail_html
    }
   await this.mailgunService.sendEmail(data)

    return Promise.resolve()
  }
}
