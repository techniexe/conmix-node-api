import { controller, httpPost, httpGet } from "inversify-express-utils"
import { injectable, inject } from "inversify"
import * as Joi from "joi"
import * as request from "request"
import { VendorTypes } from "../vendor.types"
import { NextFunction, Response } from "express"
import { verifySessionToken } from "../../middleware/auth-token.middleware"
import {
  getFBCustomTokenSchema,
  getGoogleCustomTokenSchema,
  IGetAuthenticationCodeOnMobileRequest,
  verifyAuthenticationCodeSchema,
  IVerifyAuthenticationCodeForMobileRequest,
  loginUserSchema,
  ILoginUserRequest,
  EmailRegistrationSchema,
  IGetEmailRegistrationRequest,
} from "./vendor.auth.schema"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { createJWTToken } from "../../utilities/jwt.utilities"
import {
  assertThatMobileNumberNotRegistered,
  assertThatEmailNotRegistered,
} from "../user/vendor.user.middleware"
//import { TwilioService } from "../../utilities/twilio-service"
import { VendorAuthRepository } from "./vendor.auth.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  VendorUserRepository,
  getUserIdentifierType,
  //  UserIdentifiers,
} from "../user/vendor.user.repository"

@controller("/auth", verifySessionToken)
@injectable()
export class VendorAuthController {
  constructor(
    @inject(VendorTypes.VendorUserRepository)
    private vendorUserRepo: VendorUserRepository,
    //  @inject(VendorTypes.TwilioService) private twilioService: TwilioService,
    @inject(VendorTypes.VendorAuthRepository)
    private authRepo: VendorAuthRepository
  ) {}

  /**
   * Used to get auth code(OTP) on mobile number during Signup.
   * @param req
   * @param res
   * @param next
   */
  @httpGet("/mobiles/:mobileNumber", assertThatMobileNumberNotRegistered)
  async getAuthenticationCodeOnMobile(
    req: IGetAuthenticationCodeOnMobileRequest,
    res: Response,
    next: NextFunction
  ) {
    const { mobileNumber } = req.params
    // const { method = "sms" } = req.query
    try {
      const auth = await this.authRepo.getVendorAuthCode(mobileNumber)
      if (auth === null) {
        return res
          .status(400)
          .json({
            error: { message: "We are unable to send OTP to your mobile no." },
          })
      }
      // await this.twilioService.sendCode(mobileNumber, auth.code, method)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /** Verify mobile number to get registration token (User should not registered on our database) */
  @httpPost(
    "/mobiles/:mobileNumber/code/:code",
    validate(verifyAuthenticationCodeSchema)
  )
  async verifyAuthenticationCodeForMobile(
    req: IVerifyAuthenticationCodeForMobileRequest,
    res: Response,
    next: NextFunction
  ) {
    let { mobileNumber, code } = req.params
    const codeRow = await this.authRepo.checkVerificationCodeWithDb(
      mobileNumber,
      "vendor",
      code
    )
    if (isNullOrUndefined(codeRow)) {
      return res
        .status(400)
        .json({
          error: { message: "You entered a wrong OTP. Kindly try again." },
        })
    }
    const jwtObj = {
      user_type: "vendor",
      full_name: "",
      uid: codeRow._id,
    }
    const customToken = await createJWTToken(jwtObj, "30d")
    res.status(200).json({ data: { customToken } })
  }

  /**
   * Get auth code for a user that exists in the database.
   * This api will be called when a user with a mobile number
   * has forgotten his/her password.
   * @param req Express Authenticated Request
   * @param res Express Response
   * @param next Express Next Function
   */
  @httpGet("/users/:identifier")
  async getAuthCodeForUser(req: any, res: Response, next: NextFunction) {
    const { identifier } = req.params
    // const { method = "sms" } = req.query

    try {
      const identifierType = getUserIdentifierType(identifier)
      const user = await this.vendorUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (user === null) {
        return res.status(404).json({
          error: {
            message: `User with this ${identifier}  could not be found`,
          },
        })
      }

      if (isNullOrUndefined(user.mobile_number)) {
        return res.status(400).json({
          error: {
            message:
              "This User is not registered with entered mobile number. Password recovery request is Invalid",
          },
        })
      }

      const auth = await this.authRepo.getVendorAuthCodeForResetPwd(identifier)
      if (auth === null) {
        return next("We are unable to send OTP to your mobile no.")
      }
      // await this.twilioService.sendCode(user.mobile_number, auth.code, method)
      return res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  /**
   * This API is called when the user with mobile number
   * wants to verify the code sent to him/her. (Forgot password/ for registered user only)
   * @param req
   * @param res
   * @param next
   */
  @httpPost("/users/:identifier/codes/:code")
  async verifyAuthCodeForUser(req: any, res: Response, next: NextFunction) {
    const { identifier, code } = req.params

    try {
      const identifierType = getUserIdentifierType(identifier)

      const user = await this.vendorUserRepo.getUserFromDbUsing(
        identifierType,
        identifier
      )

      if (isNullOrUndefined(user)) {
        return res.status(400).json({
          error: {
            message: `User with this ${identifier}  could not be found`,
          },
        })
      }

      const codeRow = await this.authRepo.checkVerificationCodeWithDb(
        identifier,
        "vendor",
        code
      )

      if (isNullOrUndefined(codeRow)) {
        return res
          .status(400)
          .json({
            error: { message: "You entered the wrong OTP. Kindly try again" },
          })
      }
      const jwtObj = {
        user_type: "vendor",
        full_name: user.full_name,
        uid: user._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      next(err)
    }
  }
  /** User login with identifier */
  @httpPost("/:identifier", validate(loginUserSchema))
  async loginUserJwt(
    req: ILoginUserRequest,
    res: Response,
    next: NextFunction
  ) {
    const { identifier } = req.params
    const { password } = req.body

    try {
      const identifierType = getUserIdentifierType(identifier)
      const errorMsg = `Kindly enter valid email/mobile no. and password.`
      const user = await this.vendorUserRepo.getUserWithPassword(
        identifierType,
        identifier,
        password
      )

      if (user === null) {
        return res.status(400).json({ error: { message: errorMsg } })
      }

      if (!isNullOrUndefined(user)) {
        if (user.is_blocked === true) {
          return res
            .status(400)
            .json({
              error: {
                message: `Your Account has been blocked by Admin. Kindly contact Admin.`,
              },
            })
        } else {
          const jwtObj = {
            user_type: "vendor",
            full_name: user.full_name,
            uid: user._id,
          }
          const customToken = await createJWTToken(jwtObj, "30d")
          console.log("customToken", customToken)
          res.status(200).json({ data: { customToken } })
        }
      }
    } catch (err) {
      next(err)
    }
  }

  /**
   * Used to get email during Signup.
   * @param req
   * @param res
   * @param next
   */
  @httpGet(
    "/email/:email",
    validate(EmailRegistrationSchema),
    assertThatEmailNotRegistered
  )
  async sendEmailforRegistration(
    req: IGetEmailRegistrationRequest,
    res: Response,
    next: NextFunction
  ) {
    const { email } = req.params
    try {
      const message = await this.authRepo.sendEmailforRegistration(email)
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  /*************************************************************** */
  @httpPost("/facebook/tokens/:token")
  async facebookUserJwt(req: any, res: Response, next: NextFunction) {
    const { token } = req.params
    request.get(
      `https://graph.facebook.com/v2.11/me?access_token=${token}&fields=id,name,email&format=json&method=get`,
      undefined,
      async (error, resp, body) => {
        if (!isNullOrUndefined(error)) {
          return res.status(400).json({
            error: { message: "Error in Facebook Sign in." },
          })
        }
        let result = Joi.validate(body, getFBCustomTokenSchema)
        if (!isNullOrUndefined(result.error)) {
          return res.status(400).json({
            error: {
              message: "Error in Facebook Sign in.",
              details: [{ path: "token", message: "invalid token." }],
            },
          })
        }
        const bodyObj = JSON.parse(body)
        const { email, name: full_name } = bodyObj
        const userInfo = await this.vendorUserRepo.createAndGetUser({
          email,
          full_name,
          signup_type: "facebook",
        })
        const jwtObj = {
          user_type: "vendor",
          full_name,
          uid: userInfo._id,
        }
        const customToken = await createJWTToken(jwtObj, "30d")
        res.status(200).json({ data: { customToken } })
      }
    )
  }
  @httpPost("/google/tokens/:token")
  async googleUserJwt(req: any, res: Response, next: NextFunction) {
    const { token } = req.params
    request.get(
      `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`,
      undefined,
      async (error, resp, body) => {
        if (!isNullOrUndefined(error)) {
          return res.status(400).json({
            error: { message: "Error in Google Sign in." },
          })
        }
        let result = Joi.validate(body, getGoogleCustomTokenSchema, {
          abortEarly: false,
          allowUnknown: true,
        })
        if (!isNullOrUndefined(result.error)) {
          return res.status(400).json({
            error: {
              message: "Error in Google Sign in.",
              details: [{ path: "token", message: "Invalid token." }],
            },
          })
        }
        const { email, exp, name: full_name, picture: image } = JSON.parse(body)
        if (exp < Date.now() / 1000) {
          return res.status(400).json({
            error: {
              message: "Error in Google Sign in.",
              details: [{ path: "token", message: "Token expired." }],
            },
          })
        }
        const userInfo = await this.vendorUserRepo.createAndGetUser({
          email,
          full_name,
          image,
          signup_type: "google",
        })
        const jwtObj = {
          user_type: "vendor",
          full_name,
          uid: userInfo._id,
        }
        const customToken = await createJWTToken(jwtObj, "30d")
        res.status(200).json({ data: { customToken } })
      }
    )
  }
}
