import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import {
  IAdmixtureQuantity,
  IEditAdmixtureQuantity,
  IGetAdmixtureQuantityList,
} from "./admixture_quantity.req-schema"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { ObjectId } from "bson"
import { AddressModel } from "../../model/address.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { AdmixtureOSModel } from "../../model/admix_os.model"
import { AuthModel } from "../../model/auth.model"

@injectable()
export class AdmixtureQuantityRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addAdmixtureQuantity(
    vendor_id: string,
    AdmixtureQuantityData: IAdmixtureQuantity
  ) {
    const query: { [k: string]: any } = {
      address_id: AdmixtureQuantityData.address_id,
      brand_id: AdmixtureQuantityData.brand_id,
      category_id: AdmixtureQuantityData.category_id,
    }

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    let data = await AdmixtureQuantityModel.findOne(query)

    // const data = await AdmixtureQuantityModel.findOne({
    //   vendor_id,
    //   address_id: AdmixtureQuantityData.address_id,
    //   brand_id: AdmixtureQuantityData.brand_id,
    //   category_id: AdmixtureQuantityData.category_id,
    // })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(`Admix qunatity stock already exists for this brand and category at selected site address.`, 400)
      )
    }
    AdmixtureQuantityData.vendor_id = vendor_id

    let sub_vendor_id: any
    if (!isNullOrUndefined(AdmixtureQuantityData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(AdmixtureQuantityData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
    }
    AdmixtureQuantityData.sub_vendor_id = sub_vendor_id

    const newAdmixtureQuantity = new AdmixtureQuantityModel(
      AdmixtureQuantityData
    )
    const newadmixtureDoc = await newAdmixtureQuantity.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_ADMIXTURE_QUANTITY_BY_VENDOR,
      `Vendor added Admixture quantity of id ${newadmixtureDoc._id} .`
    )

    return Promise.resolve(newadmixtureDoc)
  }

  async editAdmixtureQuantity(
    vendor_id: string,
    AdmixtureQuantityId: string,
    editData: IEditAdmixtureQuantity
  ): Promise<any> {
    const editDoc: { [k: string]: any } = {}

    const result = await AuthModel.findOne({
      code: editData.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const query: { [k: string]: any } = {
      address_id: editData.address_id,
      brand_id: editData.brand_id,
      category_id: editData.category_id,
    }

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    let data = await AdmixtureQuantityModel.findOne(query)

    // const data = await AdmixtureQuantityModel.findOne({
    //   vendor_id,
    //   address_id: AdmixtureQuantityData.address_id,
    //   brand_id: AdmixtureQuantityData.brand_id,
    //   category_id: AdmixtureQuantityData.category_id,
    // })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(AdmixtureQuantityId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`Admix qunatity stock already exists for this brand and category at selected site address.`, 400)
      )
    }

    const edtFld = [
      "address_id",
      "brand_id",
      "category_id",
      "quantity",
      "master_vendor_id",
    ]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if (!isNullOrUndefined(editData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    if (!isNullOrUndefined(editData.quantity)) {
      let vendor_capacity: any
      let vendorData = await VendorUserModel.findOne({
        _id: new ObjectId(editDoc.sub_vendor_id),
      })
      if (!isNullOrUndefined(vendorData)) {
        vendor_capacity =
          vendorData.no_of_plants *
          vendorData.plant_capacity_per_hour *
          vendorData.no_of_operation_hour
      }

      let qty = editData.quantity * vendor_capacity
      if (qty > 0.01) {
        await AdmixtureOSModel.findOneAndRemove({
          address_id: editData.address_id,
          sub_vendor_id: editDoc.sub_vendor_id,
        })
      }
    }

    const edtRes = await AdmixtureQuantityModel.updateOne(
      {
        _id: new ObjectId(AdmixtureQuantityId),
        //  vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Invalid request as no Admixture quantity data found`, 400)
      )
    }

    await AuthModel.deleteOne({
      code: editData.authentication_code,
    })

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_ADMIXTURE_QUANTITY_BY_VENDOR,
      "aggreagte quantity information Updated."
    )

    return Promise.resolve()
  }
  async removeAdmixtureQuantity(
    AdmixtureQuantityId: string,
    authentication_code: string
  ) {
    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const status = await AdmixtureQuantityModel.deleteOne({
      _id: new ObjectId(AdmixtureQuantityId),
    })
    if (status.n !== 1) {
      throw new Error("Admixture quantity doesn't exists")
    }

    await AuthModel.deleteOne({
      code: authentication_code,
    })
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetAdmixtureQuantityList) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    // query.vendor_id = new ObjectId(vendor_id)

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.brand_id)) {
      query.brand_id = new ObjectId(searchParam.brand_id)
    }

    if (!isNullOrUndefined(searchParam.category_id)) {
      query.category_id = new ObjectId(searchParam.category_id)
    }


    if (!isNullOrUndefined(searchParam.quantity) &&
    !isNullOrUndefined(searchParam.quantity_value) &&
    searchParam.quantity_value === "less") {
      query.quantity = {$lte: parseInt(searchParam.quantity)}
    }

    if (!isNullOrUndefined(searchParam.quantity) &&
    !isNullOrUndefined(searchParam.quantity_value) &&
    searchParam.quantity_value === "more") {
      query.quantity = {$gte: parseInt(searchParam.quantity)}
    }
    
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: {
          path: "$admixture_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,

          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          "admixture_category.admixture_type": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await AdmixtureQuantityModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
