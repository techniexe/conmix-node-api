import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAdmixtureQuantity {
  address_id: string
  brand_id: string
  category_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddAdmixtureQuantityRequest extends IAuthenticatedRequest {
  body: IAdmixtureQuantity
}

export const addAdmixtureQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex).required(),
    category_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export interface IEditAdmixtureQuantity {
  address_id: string
  brand_id: string
  category_id: string
  quantity: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditAdmixtureQuantityRequest extends IAuthenticatedRequest {
  params: {
    AdmixtureQuantityId: string
  }
  body: IEditAdmixtureQuantity
}

export const editAdmixtureQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    category_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number().min(1),
    authentication_code: Joi.string().required(),
  }),

  params: Joi.object().keys({
    AdmixtureQuantityId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveAdmixtureQuantityRequest extends IAuthenticatedRequest {
  params: {
    AdmixtureQuantityId: string
  }
}

export const removeAdmixtureQuantitySchema: IRequestSchema = {
  params: Joi.object().keys({
    AdmixtureQuantityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetAdmixtureQuantityList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  category_id?: string
  quantity?: any
  quantity_value?: string
}

export interface IGetAdmixtureQuantityRequest extends IAuthenticatedRequest {
  query: IGetAdmixtureQuantityList
}

export const getAdmixtureQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    category_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
  }),
}
