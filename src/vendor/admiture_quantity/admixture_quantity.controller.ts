import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { AdmixtureQuantityRepository } from "./admixture_quantity.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addAdmixtureQuantitySchema,
  IAddAdmixtureQuantityRequest,
  editAdmixtureQuantitySchema,
  IEditAdmixtureQuantityRequest,
  removeAdmixtureQuantitySchema,
  IRemoveAdmixtureQuantityRequest,
  getAdmixtureQuantitySchema,
  IGetAdmixtureQuantityRequest,
} from "./admixture_quantity.req-schema"

@injectable()
@controller("/admixture_quantity", verifyCustomToken("vendor"))
export class AdmixtureQuantityController {
  constructor(
    @inject(VendorTypes.AdmixtureQuantityRepository)
    private admixtureQuantityRepo: AdmixtureQuantityRepository
  ) {}

  @httpPost("/", validate(addAdmixtureQuantitySchema))
  async addAdmixtureQuantity(
    req: IAddAdmixtureQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.admixtureQuantityRepo.addAdmixtureQuantity(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:AdmixtureQuantityId", validate(editAdmixtureQuantitySchema))
  async editAdmixtureQuantity(
    req: IEditAdmixtureQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.admixtureQuantityRepo.editAdmixtureQuantity(
      req.user.uid,
      req.params.AdmixtureQuantityId,
      req.body
    )
    res.sendStatus(204)
  }

  @httpDelete("/:AdmixtureQuantityId", validate(removeAdmixtureQuantitySchema))
  async removeAdmixtureQuantity(
    req: IRemoveAdmixtureQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.admixtureQuantityRepo.removeAdmixtureQuantity(
        req.params.AdmixtureQuantityId,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getAdmixtureQuantitySchema))
  async List(req: IGetAdmixtureQuantityRequest, res: Response) {
    const data = await this.admixtureQuantityRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
