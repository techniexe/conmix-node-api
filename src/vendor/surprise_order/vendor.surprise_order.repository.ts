import {
  SupplierOrderStatus,
  VendorOrderModel,
} from "./../../model/vendor.order.model"
import { injectable, inject } from "inversify"
import { InvalidInput } from "../../utilities/customError"
import { SurpriseOrderModel } from "../../model/surprise_order.model"
import { ObjectId } from "bson"
import { monthsCode, PaymentStatus } from "../../utilities/config"
import { VendorTypes } from "../vendor.types"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class VendorSurpriserOrderRepository {
  constructor(
    @inject(VendorTypes.UniqueidService)
    private uniqueIdService: UniqueidService
  ) {}
  async updateSurpriseOrder(
    order_status: string,
    surpriseOrderId: string,
    user_id: string,
    authentication_code: string
  ) {
    if (order_status === SupplierOrderStatus.REJECTED) {
      const result = await AuthModel.findOne({
        code: authentication_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
      }
    }
    const surpriseOrderCheck = await SurpriseOrderModel.findOne({
      _id: surpriseOrderId,
      user_id,
    })
    if (surpriseOrderCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    // If surprise order Accepted then remove it from surprise order and add into vendor order .
    if (
      !isNullOrUndefined(order_status) &&
      order_status === SupplierOrderStatus.ACCEPTED
    ) {
      const vendor_order: any[] = []
      vendor_order.push({
        _id: await this.uniqueIdService.getUniqueSupplierOrderId(),
        user_id,
        design_mix_id: surpriseOrderCheck.design_mix_id,
        buyer_id: surpriseOrderCheck.buyer_id,
        buyer_order_id: surpriseOrderCheck.buyer_order_id,
        buyer_order_display_id: surpriseOrderCheck.buyer_order_display_id,
        buyer_order_item_id: surpriseOrderCheck.buyer_order_item_id,
        buyer_order_display_item_id:
          surpriseOrderCheck.buyer_order_display_item_id,
        quantity: surpriseOrderCheck.quantity,
        concrete_grade_id: surpriseOrderCheck.concrete_grade_id,
        admix_brand_id: surpriseOrderCheck.admix_brand_id,
        admix_cat_id: surpriseOrderCheck.admix_cat_id,
        fly_ash_source_id: surpriseOrderCheck.fly_ash_source_id,
        aggregate2_sub_cat_id: surpriseOrderCheck.aggregate2_sub_cat_id,
        agg_source_id: surpriseOrderCheck.agg_source_id,
        aggregate1_sub_cat_id: surpriseOrderCheck.aggregate1_sub_cat_id,
        sand_source_id: surpriseOrderCheck.sand_source_id,
        cement_brand_id: surpriseOrderCheck.cement_brand_id,
        cement_grade_id: surpriseOrderCheck.cement_grade_id,
        cement_quantity: surpriseOrderCheck.cement_quantity,
        sand_quantity: surpriseOrderCheck.sand_quantity,
        aggregate1_quantity: surpriseOrderCheck.aggregate1_quantity,
        aggregate2_quantity: surpriseOrderCheck.aggregate2_quantity,
        fly_ash_quantity: surpriseOrderCheck.fly_ash_quantity,
        admix_quantity: surpriseOrderCheck.admix_quantity,
        water_quantity: surpriseOrderCheck.water_quantity,
        cement_per_kg_rate: surpriseOrderCheck.cement_per_kg_rate,
        sand_per_kg_rate: surpriseOrderCheck.sand_per_kg_rate,
        aggregate1_per_kg_rate: surpriseOrderCheck.aggregate1_per_kg_rate,
        aggregate2_per_kg_rate: surpriseOrderCheck.aggregate2_per_kg_rate,
        fly_ash_per_kg_rate: surpriseOrderCheck.fly_ash_per_kg_rate,
        admix_per_kg_rate: surpriseOrderCheck.admix_per_kg_rate,
        water_per_ltr_rate: surpriseOrderCheck.water_per_ltr_rate,
        cement_price: surpriseOrderCheck.cement_price,
        sand_price: surpriseOrderCheck.sand_price,
        aggreagte1_price: surpriseOrderCheck.aggreagte1_price,
        aggreagte2_price: surpriseOrderCheck.aggreagte2_price,
        fly_ash_price: surpriseOrderCheck.fly_ash_price,
        admix_price: surpriseOrderCheck.admix_price,
        water_price: surpriseOrderCheck.water_price,
        with_TM: surpriseOrderCheck.with_TM,
        with_CP: surpriseOrderCheck.with_CP,
        delivery_date: surpriseOrderCheck.delivery_date,
        end_date: surpriseOrderCheck.end_date,
        distance: surpriseOrderCheck.distance,
        cgst_rate: surpriseOrderCheck.cgst_rate,
        sgst_rate: surpriseOrderCheck.sgst_rate,
        sgst_price: surpriseOrderCheck.sgst_price,
        cgst_price: surpriseOrderCheck.cgst_price,
        gst_price: surpriseOrderCheck.gst_price,
        // item_status: oItem.item_status,
        unit_price: surpriseOrderCheck.unit_price,
        selling_price: surpriseOrderCheck.selling_price,
        margin_price: surpriseOrderCheck.margin_price,
        TM_price: surpriseOrderCheck.TM_price,
        CP_price: surpriseOrderCheck.CP_price,
        order_status: SupplierOrderStatus.RECEIVED,
        payment_status: PaymentStatus.UNPAID,
        address_id: surpriseOrderCheck.address_id,
        line1: surpriseOrderCheck.line1,
        line2: surpriseOrderCheck.line2,
        state_id: surpriseOrderCheck.state_id,
        city_id: surpriseOrderCheck.city_id,
        pincode: surpriseOrderCheck.pincode,
        location: surpriseOrderCheck.location,
        address_type: surpriseOrderCheck.address_type, //warehouse/factory/quarry/kapchi/home/office/
        business_name: surpriseOrderCheck.business_name,
        city_name: surpriseOrderCheck.city_name,
        state_name: surpriseOrderCheck.state_name,
        total_amount: surpriseOrderCheck.total_amount,
        total_amount_without_margin:
          surpriseOrderCheck.total_amount_without_margin,
        is_surprise_order: true  
      })

      console.log("surpriseOrderCheck", surpriseOrderCheck)

      await VendorOrderModel.create(vendor_order)

      //await new VendorOrderModel(surpriseOrderCheck).save()

      await SurpriseOrderModel.findOneAndDelete({
        _id: surpriseOrderId,
      })
    } else {
      await SurpriseOrderModel.findOneAndUpdate(
        {
          _id: surpriseOrderId,
        },
        {
          $set: {
            order_status,
          },
        }
      )
    }
    await AuthModel.deleteOne({
      user_id,
      code: authentication_code,
    })
    // await this.commonService.addActivityLogs(
    //   user_id,
    //   "logisctics",
    //   AccessTypes.UPDATE_QUOTE_BY_LOGISCTICS,
    //   `Logistics updated details quote of id ${quoteId} .`,
    //   res,
    //   editDoc
    // )

    return Promise.resolve()
  }

  async getVendorSurpriseOrder(
    user_id?: string,
    before?: string,
    after?: string,
    payment_status?: string,
    design_mix_id?: string,
    order_status?: string,
    buyer_id?: string,
    address_id?: string,
    month?: string,
    year?: number,
    date?: Date
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      query.user_id = new ObjectId(user_id)
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }
    if (!isNullOrUndefined(payment_status)) {
      query.payment_status = payment_status
    }
    if (!isNullOrUndefined(design_mix_id)) {
      query.design_mix_id = design_mix_id
    }

    if (!isNullOrUndefined(order_status)) {
      query.order_status = order_status
    }

    if (!isNullOrUndefined(buyer_id)) {
      query.buyer_id = buyer_id
    }

    if (!isNullOrUndefined(address_id)) {
      query.address_id = new ObjectId(address_id) 
    }

    console.log("query", query)
    // If month or year not passed, then check with current date.
   if (isNullOrUndefined(month) || isNullOrUndefined(year)) {
     var now = new Date()
     query.created_at = {
       $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
     }
   } else {
     // If month or year passed, then find first and last day of given month and year then check.
     let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
     query.created_at = {
       $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
       $lte: new Date(endDate.getTime() - 1),
     }
   }
   if(!isNullOrUndefined(date)){

     var d1 = new Date(date);
     console.log("d1", d1)
    let next_day =  new Date(d1.setDate(d1.getDate() + 1));
    console.log("date", date)
    console.log("next_day", next_day)
    query.created_at = {
     $gte: date,
     $lte: next_day,
   }
   }

   if (!isNullOrUndefined(year)) {
     query.created_at = {
       $gte: new Date(year, 0, 1),
       $lte: new Date(year, 11, 31),
     }
   }
   
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          max_accepted_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const surpriseOrderData = await SurpriseOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      surpriseOrderData.reverse()
    }
    return Promise.resolve(surpriseOrderData)
  }

  async getSurpriseOrderById(surpriseOrderId: string, user_id: string) {
    const orderQuery: { [k: string]: any } = {}
    orderQuery._id = surpriseOrderId
    orderQuery.user_id = new ObjectId(user_id)

    const aggregateArr = [
      { $match: orderQuery },
      { $sort: { created_at: -1 } },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "pickup_address",
        },
      },
      {
        $unwind: { path: "$pickup_address" },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_display_id",
          foreignField: "display_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "order.state_id",
          foreignField: "_id",
          as: "stateData",
        },
      },
      {
        $unwind: { path: "$stateData" },
      },
      {
        $lookup: {
          from: "city",
          localField: "order.city_id",
          foreignField: "_id",
          as: "cityData",
        },
      },
      {
        $unwind: { path: "$cityData" },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "user_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },
      // {
      //   $lookup: {
      //     from: "order_item",
      //     localField: "buyer_order_item_id",
      //     foreignField: "_id",
      //     as: "orderItem",
      //   },
      // },
      {
        $project: {
          _id: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          user_id: true,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          // address_id: true,
          // line1: true,
          // line2: true,
          // state_id: true,
          // city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          vendor_media: true,
          max_accepted_at: true,
          total_CP_price: true,
          "buyer._id": true,
          "buyer.full_name": true,
          "pickup_address._id": true,
          "pickup_address.state_name": "$state_name",
          "pickup_address.city_name": "$city_name",
          "pickup_address.line1": true,
          "pickup_address.line2": true,
          "pickup_address.pincode": true,
          "pickup_address.location": true,
          "delivery_address.site_id": "$order.site_id",
          "delivery_address.site_name": "$order.site_name",
          "delivery_address.address_line1": "$order.address_line1",
          "delivery_address.address_line2": "$order.address_line2",
          "delivery_address.state_id": "$order.state_id",
          "delivery_address.city_id": "$order.city_id",
          "delivery_address.pincode": "$order.pincode",
          "delivery_address.location": "$order.delivery_location",
          "delivery_address.created_at": "$order.created_at",
          "delivery_address.state_name": "$stateData.state_name",
          "delivery_address.city_name": "$cityData.city_name",
        },
      },
    ]

    const surpriseOrderData = await SurpriseOrderModel.aggregate(aggregateArr)
    if (surpriseOrderData.length < 0) {
      return Promise.reject(new InvalidInput(`No Surprise Order found`, 400))
    }

    return Promise.resolve(surpriseOrderData[0])
  }
}
