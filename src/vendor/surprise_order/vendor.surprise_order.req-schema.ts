import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode } from "../../utilities/config"

export interface IUpdateSurpriseOrderRequest extends IAuthenticatedRequest {
  body: {
    order_status: string
    authentication_code: string
  }
}

export const updatesurpriseOrderSchema: IRequestSchema = {
  body: Joi.object().keys({
    order_status: Joi.string().required(),
    authentication_code: Joi.string().required(),
  }),
  params: Joi.object().keys({
    surpriseOrderId: Joi.string().required(),
  }),
}

export interface IGetVendorSurpriseOrder {
  user_id?: string
  before?: string
  after?: string
  design_mix_id?: string
  payment_status?: string
  order_status?: string
  buyer_id?: string
  address_id?: string
  month?: string
  year?: number
  date: Date
}

export interface IGetVendorSurpriseOrderRequest extends IAuthenticatedRequest {
  query: IGetVendorSurpriseOrder
}

export const getVendorSurpriseOrder: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    design_mix_id: Joi.string().regex(objectIdRegex),
    payment_status: Joi.string(),
    order_status: Joi.string(),
    buyer_id: Joi.string().regex(objectIdRegex),
    address_id: Joi.string().regex(objectIdRegex),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
  }),
}

export interface IGetSurpriseOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    surpriseOrderId: string
  }
}

export const getSurpriseOrderById: IRequestSchema = {
  params: Joi.object().keys({
    surpriseOrderId: Joi.string(),
  }),
}
