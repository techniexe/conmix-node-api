import { injectable, inject } from "inversify"
import { controller, httpPut, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { VendorTypes } from "../vendor.types"
import { VendorSurpriserOrderRepository } from "./vendor.surprise_order.repository"
import {
  updatesurpriseOrderSchema,
  IUpdateSurpriseOrderRequest,
  getVendorSurpriseOrder,
  IGetVendorSurpriseOrderRequest,
  getSurpriseOrderById,
  IGetSurpriseOrderByIdRequest,
} from "./vendor.surprise_order.req-schema"

@injectable()
@controller("/surprise_order", verifyCustomToken("vendor"))
export class VendorSurpriseOrderController {
  constructor(
    @inject(VendorTypes.VendorSurpriserOrderRepository)
    private surpriserOrderRepo: VendorSurpriserOrderRepository
  ) {}

  @httpPut("/:surpriseOrderId", validate(updatesurpriseOrderSchema))
  async updateSurpriseOrder(
    req: IUpdateSurpriseOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.surpriserOrderRepo.updateSurpriseOrder(
        req.body.order_status,
        req.params.surpriseOrderId,
        req.user.uid,
        req.body.authentication_code
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  // Get vendor surprise order list.
  @httpGet("/", validate(getVendorSurpriseOrder))
  async getVendorSurpriseOrder(
    req: IGetVendorSurpriseOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    const {
      before,
      after,
      payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      month,
      year,
      date
    } = req.query

    const data = await this.surpriserOrderRepo.getVendorSurpriseOrder(
      req.user.uid,
      before,
      after,
      payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      month,
      year,
      date
    )
    res.json({ data })
  }

  // Get surprise order details.
  @httpGet("/:surpriseOrderId", validate(getSurpriseOrderById))
  async getSurpriseOrderById(
    req: IGetSurpriseOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.surpriserOrderRepo.getSurpriseOrderById(
        req.params.surpriseOrderId,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
