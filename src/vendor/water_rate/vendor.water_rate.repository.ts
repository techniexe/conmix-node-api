import {
  IWaterRate,
  IEditWaterRate,
  IGetWaterRateList,
} from "./vendor.water_rate.req.schema"
import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { WaterRateModel } from "../../model/water_rate.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { ObjectId } from "bson"
import { AddressModel } from "../../model/address.model"

@injectable()
export class WaterRateRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addWaterRate(vendor_id: string, reqBody: IWaterRate) {
    const data = await WaterRateModel.findOne({
      vendor_id,
      address_id: reqBody.address_id,
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(`Water rate already exists at selected site address.`, 400)
      )
    }
    reqBody.vendor_id = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(reqBody.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(reqBody.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      }
      reqBody.sub_vendor_id = sub_vendor_id

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_WATER_RATE,
      "Water Rate Added."
    )

    return new WaterRateModel(reqBody).save()
  }
  async editWaterRate(
    vendor_id: string,
    WaterRateId: string,
    editData: IEditWaterRate
  ): Promise<any> {

    const data = await WaterRateModel.findOne({
      vendor_id,
      address_id: editData.address_id,
    })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(WaterRateId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`Water rate already exists at selected site address.`, 400)
      )
    }

    const editDoc: { [k: string]: any } = {}
    const edtFld = ["address_id", "per_ltr_rate", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(editData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(editData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    const edtRes = await WaterRateModel.updateOne(
      {
        _id: new ObjectId(WaterRateId),
       // vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`No Water rate found to edit`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_WATER_RATE,
      "Water rate information Updated."
    )

    return Promise.resolve()
  }

  async removeWaterRate(WaterRateId: string) {
    const status = await WaterRateModel.deleteOne({
      _id: new ObjectId(WaterRateId),
    })
    if (status.n !== 1) {
      throw new Error("Water Rate doesn't exists")
    }
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetWaterRateList) {
    const query: { [k: string]: any } = {}

   // query.vendor_id = new ObjectId(vendor_id)

   query.$or = [
    {
      vendor_id: new ObjectId(vendor_id),
    },
    {
      sub_vendor_id: new ObjectId(vendor_id),
    },
    {
      master_vendor_id: new ObjectId(vendor_id),
    },
  ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.per_ltr_rate) &&
    !isNullOrUndefined(searchParam.per_ltr_rate_value) &&
    searchParam.per_ltr_rate_value === "less") {
      query.per_ltr_rate = {$lte: parseInt(searchParam.per_ltr_rate)}
    }

    if (!isNullOrUndefined(searchParam.per_ltr_rate) &&
    !isNullOrUndefined(searchParam.per_ltr_rate_value) &&
    searchParam.per_ltr_rate_value === "more") {
      query.per_ltr_rate = {$gte: parseInt(searchParam.per_ltr_rate)}
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
{
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          per_ltr_rate: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await WaterRateModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
