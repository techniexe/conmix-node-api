import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import {
  addWaterRateSchema,
  IAddWaterRateRequest,
  IEditWaterRateRequest,
  editWaterRateSchema,
  removeWaterRateSchema,
  IRemoveWaterRateRequest,
  getWaterRateSchema,
  IGetWaterRateRequest,
} from "./vendor.water_rate.req.schema"
import { WaterRateRepository } from "./vendor.water_rate.repository"
import { NextFunction, Response } from "express"

@injectable()
@controller("/water_rate", verifyCustomToken("vendor"))
export class WaterRateController {
  constructor(
    @inject(VendorTypes.WaterRateRepository)
    private waterRateRepo: WaterRateRepository
  ) {}

  @httpPost("/", validate(addWaterRateSchema))
  async addWaterRate(
    req: IAddWaterRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.waterRateRepo.addWaterRate(uid, req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:WaterRateId", validate(editWaterRateSchema))
  async editWaterRate(
    req: IEditWaterRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.waterRateRepo.editWaterRate(
      req.user.uid,
      req.params.WaterRateId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete("/:WaterRateId", validate(removeWaterRateSchema))
  async removeWaterRate(
    req: IRemoveWaterRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.waterRateRepo.removeWaterRate(req.params.WaterRateId)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getWaterRateSchema))
  async List(req: IGetWaterRateRequest, res: Response) {
    const data = await this.waterRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
