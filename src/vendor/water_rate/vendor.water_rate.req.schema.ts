import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IWaterRate {
  address_id: string
  per_ltr_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddWaterRateRequest extends IAuthenticatedRequest {
  body: IWaterRate
}

export const addWaterRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    per_ltr_rate: Joi.number().min(1).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),
}

export interface IEditWaterRate {
  address_id: string
  per_ltr_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditWaterRateRequest extends IAuthenticatedRequest {
  params: {
    WaterRateId: string
  }
  body: IEditWaterRate
}

export const editWaterRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    per_ltr_rate: Joi.number().min(1),
    master_vendor_id: Joi.string().regex(objectIdRegex),
  }),

  params: Joi.object().keys({
    WaterRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveWaterRateRequest extends IAuthenticatedRequest {
  params: {
    WaterRateId: string
  }
}

export const removeWaterRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    WaterRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetWaterRateList {
  before?: string
  after?: string
  address_id?: string
  per_ltr_rate?: any
  per_ltr_rate_value?: string
}

export interface IGetWaterRateRequest extends IAuthenticatedRequest {
  query: IGetWaterRateList
}

export const getWaterRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    per_ltr_rate: Joi.number(),
    per_ltr_rate_value: Joi.string(),
  }),
}
