import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import {
  // TMCategoryModel,
  // TMSubCategoryModel,
  TMModel,
} from "../../model/TM.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { ObjectId } from "bson"
import { DriverInfoModel } from "../../model/driver.model"
import { OperatorInfoModel } from "../../model/operator.model"
import { ITM, IEditTM, IGetTMDetails } from "./vendor.TM.req-schema"
import { UploadedFile } from "express-fileupload"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { AccessTypes } from "../../model/access_logs.model"
import { AddressModel } from "../../model/address.model"

@injectable()
export class VendorTMRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  // async listAllTMCategory() {
  //   return TMCategoryModel.find()
  // }

  // async listSubCategory(TMCategoryId?: string) {
  //   let query: { [k: string]: any } = {}
  //   if (!isNullOrUndefined(TMCategoryId)) {
  //     query.TM_category_id = new ObjectId(TMCategoryId)
  //   }
  //   return TMSubCategoryModel.aggregate([
  //     {
  //       $match: query,
  //     },
  //     {
  //       $sort: { created_at: -1 },
  //     },
  //     {
  //       $limit: 40,
  //     },
  //     {
  //       $lookup: {
  //         from: "TM_category",
  //         localField: "TM_category_id",
  //         foreignField: "_id",
  //         as: "TM_category",
  //       },
  //     },
  //     {
  //       $unwind: { path: "$TM_category" },
  //     },
  //     {
  //       $project: {
  //         _id: 1,
  //         sub_category_name: 1,
  //         load_capacity: 1,
  //         weight_unit_code: 1,
  //         weight_unit: 1,
  //         created_at: 1,
  //         "TM_category.category_name": 1,
  //       },
  //     },
  //   ])
  // }

  async listDriver(user_id: string) {
    return DriverInfoModel.find({ "user.user_id": user_id })
  }

  async listOperator(user_id: string) {
    return OperatorInfoModel.find({ "user.user_id": user_id })
  }

  async addTM(
    uid: string,
    TMCategoryId: string,
    TMSubCategoryId: string,
    reqBody: ITM,
    fileData: {
      rc_book_image: UploadedFile
      insurance_image: UploadedFile
      TM_image: UploadedFile
    }
  ) {
    reqBody.user_id = uid
    reqBody.TM_category_id = TMCategoryId
    reqBody.TM_sub_category_id = TMSubCategoryId
    reqBody._id = new ObjectId()

    let data = await TMModel.findOne({
      TM_rc_number: reqBody.TM_rc_number,
    })
    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `TM already exists with same RC number`,
          400
        )
      )
    }


    let sub_vendor_id: any
    if (!isNullOrUndefined(reqBody.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(reqBody.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
    }
    reqBody.sub_vendor_id = sub_vendor_id


    if (!isNullOrUndefined(reqBody.driver1_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(reqBody.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let driverData = await DriverInfoModel.findOne({
        _id: new ObjectId(reqBody.driver1_id),
      })

      if (!isNullOrUndefined(driverData) &&
      driverData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This driver is not belongs to selected address.`,
            400
          )
        )
      }
    }
      
  

    if (!isNullOrUndefined(reqBody.driver2_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(reqBody.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let driverData = await DriverInfoModel.findOne({
        _id: new ObjectId(reqBody.driver2_id),
      })

      if (!isNullOrUndefined(driverData) &&
      driverData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This driver is not belongs to selected address.`,
            400
          )
        )
      }
    }

    // if (
    //   !isNullOrUndefined(reqBody.min_trip_price) &&
    //   !isNullOrUndefined(reqBody.calculated_trip_price)
    // ) {
    //   if (reqBody.min_trip_price > reqBody.calculated_trip_price) {
    //     return Promise.reject(
    //       new InvalidInput(
    //         `Can't add value of trip price more than ${reqBody.calculated_trip_price}`,
    //         400
    //       )
    //     )
    //   }
    // }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.rc_book_image) &&
      !isNullOrUndefined(fileData.rc_book_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM_rc_book`
      let objectName = "" + reqBody._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.rc_book_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.rc_book_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.rc_book_image.data,
        fileData.rc_book_image.mimetype
      )
      reqBody.rc_book_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.insurance_image) &&
      !isNullOrUndefined(fileData.insurance_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM_insurance`
      let objectName = "" + reqBody._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.insurance_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.insurance_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.insurance_image.data,
        fileData.insurance_image.mimetype
      )
      reqBody.insurance_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.TM_image) &&
      !isNullOrUndefined(fileData.TM_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM`
      let objectName = "" + reqBody._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.TM_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.TM_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.TM_image.data,
        fileData.TM_image.mimetype
      )
      reqBody.TM_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.ADD_TM,
      "TM information Added."
    )

    console.log("Here")
    return new TMModel(reqBody).save()
  }
  async getTM(user_id: string, searchParam: IGetTMDetails) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    // query.user_id = new ObjectId(user_id)

    let limit: number = 20
    if (!isNullOrUndefined(searchParam.limit_count)) {
      limit = searchParam.limit_count
    }

    if (!isNullOrUndefined(searchParam.is_active)) {
      query.is_active = searchParam.is_active
    }

    if (!isNullOrUndefined(searchParam.TM_category_id)) {
      query.TM_category_id = new ObjectId(searchParam.TM_category_id)
    }
    if (!isNullOrUndefined(searchParam.TM_sub_category_id)) {
      query.TM_sub_category_id = new ObjectId(searchParam.TM_sub_category_id)
    }

    if (!isNullOrUndefined(searchParam.is_gps_enabled)) {
      query.is_gps_enabled = searchParam.is_gps_enabled
    }

    if (!isNullOrUndefined(searchParam.is_insurance_active)) {
      query.is_insurance_active = searchParam.is_insurance_active
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id) 
    }

    if (!isNullOrUndefined(searchParam.TM_rc_number)) {
      query.TM_rc_number = searchParam.TM_rc_number 
    }
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: limit,
      },

      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: {
          path: "$TM_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: {
          path: "$TM_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: "$address",
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: "$address.pincode",
          business_name: "$address.business_name",
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          pickup_location: 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.min_load_capacity": 1,
          "TM_sub_category.max_load_capacity": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)
    // for (let i = 0; i < data.length; i++) {
    //   let rateDoc = await VehicleRateModel.findOne({
    //     state_id: new ObjectId(data[i].state_id),
    //   })

    //   if (!isNullOrUndefined(rateDoc)) {
    //     data[i].per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
    //     data[i].calculated_trip_price =
    //       minOrder *
    //       deliveryRangeForVehicle.min *
    //       data[i].per_metric_ton_per_km_rate
    //   } else {
    //     return Promise.reject(
    //       new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
    //     )
    //   }
    // }
    return Promise.resolve(data)
  }

  async getTMDetails(user_id: string, TMId: string) {
    const query: { [k: string]: any } = {}
    query._id = new ObjectId(TMId)

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    // query.user_id = new ObjectId(user_id)

    console.log(query)
    const aggregateArr = [
      {
        $match: query,
      },
      {
        $lookup: {
          from: "TM_category",
          localField: "TM_category_id",
          foreignField: "_id",
          as: "TM_category",
        },
      },
      {
        $unwind: { path: "$TM_category" },
      },

      {
        $lookup: {
          from: "TM_sub_category",
          localField: "TM_sub_category_id",
          foreignField: "_id",
          as: "TM_sub_category",
        },
      },
      {
        $unwind: { path: "$TM_sub_category" },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver1_id",
          foreignField: "_id",
          as: "driver1_info",
        },
      },
      {
        $unwind: { path: "$driver1_info" },
      },

      {
        $lookup: {
          from: "driver_info",
          localField: "driver2_id",
          foreignField: "_id",
          as: "driver2_info",
        },
      },
      {
        $unwind: {
          path: "$driver2_info",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: "$address",
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: "$city",
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          TM_category_id: 1,
          TM_sub_category_id: 1,
          TM_rc_number: 1,
          per_Cu_mtr_km: 1,
          pickup_location: 1,
          line1: "$address.line1",
          line2: "$address.line2",
          state_id: "$address.state_id",
          city_id: "$address.city_id",
          city_name: "$city.city_name",
          state_name: "$city.state_name",
          address_id: 1,
          pincode: 1,
          delivery_range: 1,
          manufacturer_name: 1,
          manufacture_year: 1,
          TM_model: 1,
          is_active: 1,
          is_gps_enabled: 1,
          is_insurance_active: 1,
          rc_book_image_url: 1,
          insurance_image_url: 1,
          TM_image_url: 1,
          user_id: 1,
          min_trip_price: 1,
          created_at: 1,
          "TM_category.category_name": 1,
          "TM_sub_category.sub_category_name": 1,
          "TM_sub_category.load_capacity": 1,
          "driver1_info._id": 1,
          "driver1_info.driver_name": 1,
          "driver1_info.driver_mobile_number": 1,
          "driver2_info._id": 1,
          "driver2_info.driver_name": 1,
          "driver2_info.driver_mobile_number": 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]

    const data = await TMModel.aggregate(aggregateArr)

    // let rateDoc = await VehicleRateModel.findOne({
    //   state_id: new ObjectId(data[0].state_id),
    // })

    // if (!isNullOrUndefined(rateDoc)) {
    //   data[0].per_metric_ton_per_km_rate = rateDoc.per_metric_ton_per_km_rate
    //   data[0].calculated_trip_price =
    //     minOrder *
    //     deliveryRangeForVehicle.min *
    //     data[0].per_metric_ton_per_km_rate
    // } else {
    //   return Promise.reject(
    //     new InvalidInput(`Rate for this product is unvailable for requested location.`, 400)
    //   )
    // }
    return Promise.resolve(data[0])
  }

  async editTM(
    user_id: string,
    TM_id: string,
    editData: IEditTM,
    fileData: {
      rc_book_image: UploadedFile
      insurance_image: UploadedFile
      TM_image: UploadedFile
    }
  ): Promise<any> {
    console.log("fileData", fileData)

    const editObj: { $set: { [K: string]: any } } = { $set: {} }

    // if (!isNullOrUndefined(editData.TM_rc_number)) {
    //   let data = await TMModel.findOne({
    //     TM_rc_number: editData.TM_rc_number,
    //   })
    //   if (!isNullOrUndefined(data) &&
    //   data._id.equals(user_id) === false) {
    //     return Promise.reject(
    //       new InvalidInput(
    //         `TM already exists with same RC number`,
    //         400
    //       )
    //     )
    //   }
    // }

    if (!isNullOrUndefined(editData.TM_rc_number)) {
      const check = await TMModel.findOne({
        TM_rc_number: editData.TM_rc_number,
      })

      if (
        !isNullOrUndefined(check) &&
        check._id.equals(TM_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `TM already exists with same RC number`,
            400
          )
        )
      }
    }

    const edtFld = [
      "TM_rc_number",
      "per_Cu_mtr_km",
      // "per_metric_ton_per_km_rate",
      "delivery_city_ids",
      "delivery_sub_city_ids",
      "pickup_city_ids",
      "pickup_sub_city_ids",
      "manufacturer_name",
      "manufacture_year",
      "TM_model",
      "is_gps_enabled",
      "is_insurance_active",
      "is_active",
      "rc_book_image_url",
      "insurance_image_url",
      "TM_image_url",
      "driver_name",
      "driver_mobile_number",
      "mobile_number",
      "alt_mobile_number",
      "whatsapp_number",
      "driver_pic",
      "delivery_range",
      "min_trip_price",
      "pickup_location",
      //   "line1",
      //   "line2",
      //   "state_id",
      //   "city_id",
      //   "pincode",
      "address_id",
      "master_vendor_id",
      "driver1_id",
      "driver2_id",
    ]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editObj.$set[key] = val
      }
    }
    if (Object.keys(editObj.$set).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    let sub_vendor_id: any
    if (!isNullOrUndefined(editData.address_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      editObj.$set.sub_vendor_id = sub_vendor_id
    }


    if (!isNullOrUndefined(editData.driver1_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let driverData = await DriverInfoModel.findOne({
        _id: new ObjectId(editData.driver1_id),
      })

      if (!isNullOrUndefined(driverData) &&
      driverData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This driver is not belongs to selected address.`,
            400
          )
        )
      }
    }

    if (!isNullOrUndefined(editData.driver2_id)) {
      let addressData = await AddressModel.findOne({
        _id: new ObjectId(editData.address_id),
      })
      if (!isNullOrUndefined(addressData)) {
        sub_vendor_id = addressData.sub_vendor_id
      }
      let driverData = await DriverInfoModel.findOne({
        _id: new ObjectId(editData.driver2_id),
      })

      if (!isNullOrUndefined(driverData) &&
      driverData.sub_vendor_id.equals(sub_vendor_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This driver is not belongs to selected address.`,
            400
          )
        )
      }
    }

    // if (
    //   !isNullOrUndefined(editData.min_trip_price) &&
    //   !isNullOrUndefined(editData.calculated_trip_price)
    // ) {
    //   if (editData.min_trip_price > editData.calculated_trip_price) {
    //     return Promise.reject(
    //       new InvalidInput(
    //         `Can't add value of trip price more than ${editData.calculated_trip_price}`,
    //         400
    //       )
    //     )
    //   }
    // }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.rc_book_image) &&
      !isNullOrUndefined(fileData.rc_book_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM_rc_book`
      let objectName = "" + TM_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.rc_book_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.rc_book_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.rc_book_image.data,
        fileData.rc_book_image.mimetype
      )
      editObj.$set.rc_book_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.insurance_image) &&
      !isNullOrUndefined(fileData.insurance_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM_insurance`
      let objectName = "" + TM_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.insurance_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.insurance_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.insurance_image.data,
        fileData.insurance_image.mimetype
      )
      editObj.$set.insurance_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.TM_image) &&
      !isNullOrUndefined(fileData.TM_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/TM`
      let objectName = "" + TM_id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.TM_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.TM_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.TM_image.data,
        fileData.TM_image.mimetype
      )
      editObj.$set.TM_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    const edtRes = await TMModel.updateOne(
      {
        _id: TM_id,
        // user_id,
      },
      editObj
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(
          `Invalid request as Transit Mixer has not been added`,
          400
        )
      )
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_TM,
      "TM information updated."
    )

    return Promise.resolve()
  }
}
