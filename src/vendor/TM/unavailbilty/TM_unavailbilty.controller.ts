import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../../middleware/auth-token.middleware"
import { VendorTypes } from "../../vendor.types"
import { VendorTMunavailbiltyRepository } from "./TM_unavailbilty.repository"
import { NextFunction, Response } from "express"
import {
  addTMUnSchema,
  IAddTMUnRequest,
  editTMUnSchema,
  IEditTMUnRequest,
  listTMunSchema,
  IListUnRequest,
  IDeleteTMUnRequest,
} from "./TM_unavailbilty.req-schema"
import { validate } from "../../../middleware/joi.middleware"

@injectable()
@controller("/TM_unavailbilty", verifyCustomToken("vendor"))
export class VendorTMunavailbiltyController {
  constructor(
    @inject(VendorTypes.VendorTMunavailbiltyRepository)
    private TMunavailbiltyRepo: VendorTMunavailbiltyRepository
  ) {}

  @httpPost("/:TMId", validate(addTMUnSchema))
  async add(req: IAddTMUnRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    try {
      const { TMId } = req.params
      await this.TMunavailbiltyRepo.add(uid, TMId, req.body)
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:id", validate(editTMUnSchema))
  async edit(req: IEditTMUnRequest, res: Response, next: NextFunction) {
    await this.TMunavailbiltyRepo.edit(req.user.uid, req.params.id, req.body)
    res.sendStatus(204)
  }

  @httpGet("/:TMId", validate(listTMunSchema))
  async get(req: IListUnRequest, res: Response, next: NextFunction) {
    console.log(req.user.uid)
    const data = await this.TMunavailbiltyRepo.get(
      req.user.uid,
      req.params.TMId
    )
    res.json({ data })
  }

  @httpDelete("/:id")
  async delete(req: IDeleteTMUnRequest, res: Response, next: NextFunction) {
    await this.TMunavailbiltyRepo.delete(req.user.uid, req.params.id)
    res.sendStatus(204)
  }
}
