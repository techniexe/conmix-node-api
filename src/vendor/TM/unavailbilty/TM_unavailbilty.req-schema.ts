import * as Joi from "joi"
import { IAuthenticatedRequest } from "../../../middleware/auth-token.middleware"
import {
  IRequestSchema,
  objectIdRegex,
} from "../../../middleware/joi.middleware"

export interface ITMUn {
  TMId: string
  // unavailable_at: string
  start_time: string
  end_time: string
  master_vendor_id: string
  sub_vendor_id: string
  [k: string]: any
}

export interface IAddTMUnRequest extends IAuthenticatedRequest {
  body: ITMUn
  params: {
    TMId: string
  }
}

export const addTMUnSchema: IRequestSchema = {
  body: Joi.object().keys({
    // unavailable_at: Joi.date().required(),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    TMId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IEditTMUn {
 // unavailable_at: Date
  start_time: string
  end_time: string
  master_vendor_id: string
  sub_vendor_id: string
  [k: string]: any
}

export interface IEditTMUnRequest extends IAuthenticatedRequest {
  params: {
    id: string
  }
  body: IEditTMUn
}

export const editTMUnSchema: IRequestSchema = {
  body: Joi.object().keys({
    // unavailable_at: Joi.date(),
    start_time: Joi.date(),
    end_time: Joi.date(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    sub_vendor_id: Joi.string().regex(objectIdRegex),
  }),
  params: Joi.object().keys({
    id: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const listTMunSchema: IRequestSchema = {
  params: Joi.object().keys({
    TMId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IListUnRequest extends IAuthenticatedRequest {
  params: {
    TMId: string
  }
}

export interface IDeleteTMUnRequest extends IAuthenticatedRequest {
  params: {
    id: string
  }
}