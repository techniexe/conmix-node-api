import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { ITMUn, IEditTMUn } from "./TM_unavailbilty.req-schema"
import { AccessTypes } from "../../../model/access_logs.model"
import { TM_unModel } from "../../../model/TM_un.model"
import { VendorTypes } from "../../vendor.types"
import { CommonService } from "../../../utilities/common.service"
import { InvalidInput } from "../../../utilities/customError"

@injectable()
export class VendorTMunavailbiltyRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async add(uid: string, TMId: string, reqBody: ITMUn) {
    reqBody.vendor_id = uid
    reqBody.TMId = TMId

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.ADD_TM_UN,
      "TM Unavailablity information Added."
    )
    console.log("Here")
    return new TM_unModel(reqBody).save()
  }

  async edit(user_id: string, id: string, editData: IEditTMUn): Promise<any> {
    const editObj: { $set: { [K: string]: any } } = { $set: {} }
    const edtFld = ["start_time", "end_time", "sub_vendor_id", "master_vendor_id"]
    for (const [key, val] of Object.entries(editData)) {
      if (edtFld.includes(key)) {
        editObj.$set[key] = val
      }
    }
    if (Object.keys(editObj.$set).length === 0) {
      return Promise.reject(
        new InvalidInput(`At least one filed is required for edit`, 400)
      )
    }

    const edtRes = await TM_unModel.updateOne(
      {
        _id: new ObjectId(id),
      //  vendor_id: new ObjectId(user_id),
      },
      editObj
    )
    if (edtRes.n !== 1) {
      return Promise.reject(new InvalidInput(`Invalid request as Transit Mixer has not been added`, 400))
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_TM_UN,
      "TM Unavailablity information Updated."
    )

    return Promise.resolve()
  }

  async get(vendor_id: string, TMId: string) {
    // const data = await TM_unModel.find({
    //   TMId: new ObjectId(TMId),
    //   vendor_id: new ObjectId(vendor_id),
    // })

    const TMUNquery: { [k: string]: any } = { TMId: new ObjectId(TMId)}

      TMUNquery.$or = [
        {
          vendor_id: new ObjectId(vendor_id),
        },
        {
          sub_vendor_id: new ObjectId(vendor_id),
        },
        {
          master_vendor_id: new ObjectId(vendor_id),
        },
      ]

      let data = await TM_unModel.find(TMUNquery)

    return Promise.resolve(data)
  }

  async delete(user_id: string, id: string){
    return TM_unModel.findOneAndRemove({
      _id: new ObjectId(id)
    })
  }
}
