import { injectable, inject } from "inversify"
import {
  controller,
  httpGet,
  httpPost,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorTMRepository } from "./vendor.TM.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addTMSchema,
  IAddTMRequest,
  editTMRequestSchema,
  IEditTMRequest,
  getTMSchema,
  getTMDetailsRequest,
  IGetTMDetailsRequest,
  IGetTMRequest,
} from "./vendor.TM.req-schema"
import { InvalidInput } from "../../utilities/customError"

@injectable()
@controller("/TM", verifyCustomToken("vendor"))
export class VendorTMController {
  constructor(
    @inject(VendorTypes.VendorTMRepository)
    private TMRepo: VendorTMRepository
  ) {}

  // @httpGet("/categories")
  // async listAllTMCategory(
  //   req: IRequestSchema,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   const data = await this.TMRepo.listAllTMCategory()
  //   res.json({ data })
  // }

  // @httpGet("/subCategory")
  // @httpGet("/subCategory/:TMCategoryId", validate(listTMSubCategorySchema))
  // async listSubCategory(
  //   req: IListTMSubCategoryRequest,
  //   res: Response,
  //   next: NextFunction
  // ) {
  //   const { TMCategoryId } = req.params
  //   const data = await this.TMRepo.listSubCategory(TMCategoryId)
  //   res.json({ data })
  // }

  @httpGet("/driver")
  async listDriver(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.listDriver(req.user.uid)
    res.json({ data })
  }

  @httpGet("/operator")
  async listOperator(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.listOperator(req.user.uid)
    res.json({ data })
  }

  @httpPost("/:TMCategoryId/:TMSubCategoryId", validate(addTMSchema))
  async addTM(req: IAddTMRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    try {
      const { TMCategoryId, TMSubCategoryId } = req.params
      await this.TMRepo.addTM(
        uid,
        TMCategoryId,
        TMSubCategoryId,
        req.body,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `You have already added TM with this RC number `
      }
      const err1 = new InvalidInput(msg)
      err1.httpStatusCode = 400
      next(err1)
    }
  }

  @httpPatch("/:TMId", validate(editTMRequestSchema))
  async editTM(req: IEditTMRequest, res: Response, next: NextFunction) {
    await this.TMRepo.editTM(req.user.uid, req.params.TMId, req.body, req.files)
    res.sendStatus(202)
  }
  @httpGet("/", validate(getTMSchema))
  async getTM(req: IGetTMRequest, res: Response, next: NextFunction) {
    console.log(req.user.uid)
    const data = await this.TMRepo.getTM(req.user.uid, req.query)
    res.json({ data })
  }
  @httpGet("/:TMId", validate(getTMDetailsRequest))
  async getTMDetails(
    req: IGetTMDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const data = await this.TMRepo.getTMDetails(req.user.uid, req.params.TMId)
    res.json({ data })
  }
}
