import { ObjectId } from "bson"
import { inject, injectable } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import {
  ICementRate,
  IEditCementRate,
  IGetCementRateList,
} from "./vendor.cement_rate.req-schema"
import { CementRateModel } from "../../model/cement_rate.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { AddressModel } from "../../model/address.model"

@injectable()
export class CementRateRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addCementRate(vendor_id: string, cementRateData: ICementRate) {
    const data = await CementRateModel.findOne({
      vendor_id,
      address_id: cementRateData.address_id,
      brand_id: cementRateData.brand_id,
      grade_id: cementRateData.grade_id,
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(`Cement rate already exists for this brand at selected site address.`, 400)
      )
    }
    cementRateData.vendor_id = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(cementRateData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(cementRateData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      }
      cementRateData.sub_vendor_id = sub_vendor_id

    const newCementRate = new CementRateModel(cementRateData)
    const newcementRateDoc = await newCementRate.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_CEMENT_RATE_BY_VENDOR,
      `Vendor added cement rate of id ${newcementRateDoc._id} .`
    )

    return Promise.resolve(newcementRateDoc)
  }

  async editCementRate(
    vendor_id: string,
    CementRateId: string,
    editData: IEditCementRate
  ): Promise<any> {

    const data = await CementRateModel.findOne({
      vendor_id,
      address_id: editData.address_id,
      brand_id: editData.brand_id,
      grade_id: editData.grade_id,
    })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(CementRateId) === false
    ) {
      return Promise.reject(
        new InvalidInput(`Cement rate already exists for this brand at selected site address.`, 400)
      )
    }

    // if (!isNullOrUndefined(data)) {
    //   return Promise.reject(
    //     new InvalidInput(`Cement rate already exists for this brand at selected site address.`, 400)
    //   )
    // }
    const editDoc: { [k: string]: any } = {}
    const edtFld = ["address_id", "brand_id", "grade_id", "per_kg_rate", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(editData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(editData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    const edtRes = await CementRateModel.updateOne(
      {
        _id: new ObjectId(CementRateId),
       // vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Invalid request as Cement rate has not been added`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_CEMENT_RATE_BY_VENDOR,
      "cement rate information Updated."
    )

    return Promise.resolve()
  }
  async removeCementRate(CementRateId: string) {
    const status = await CementRateModel.deleteOne({
      _id: new ObjectId(CementRateId),
    })
    if (status.n !== 1) {
      throw new Error("Cement rate has not been added")
    }
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetCementRateList) {
    const query: { [k: string]: any } = {}

    //query.vendor_id = new ObjectId(vendor_id)

    query.$or = [
      {
        vendor_id: new ObjectId(vendor_id),
      },
      {
        sub_vendor_id: new ObjectId(vendor_id),
      },
      {
        master_vendor_id: new ObjectId(vendor_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.brand_id)) {
      query.brand_id = new ObjectId(searchParam.brand_id)
    }

    if (!isNullOrUndefined(searchParam.grade_id)) {
      query.grade_id = new ObjectId(searchParam.grade_id)
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "less") {
      query.per_kg_rate = {$lte: parseInt(searchParam.per_kg_rate)}
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "more") {
      query.per_kg_rate = {$gte: parseInt(searchParam.per_kg_rate)}
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "cement_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          per_kg_rate: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await CementRateModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
