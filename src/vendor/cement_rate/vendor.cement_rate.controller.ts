import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addCementRateSchema,
  IAddCementRateRequest,
  editCementRateSchema,
  IEditCementRateRequest,
  removeCementRateSchema,
  IRemoveCementRateRequest,
  getCementRateSchema,
  IGetCementRateRequest,
} from "./vendor.cement_rate.req-schema"
import { CementRateRepository } from "./vendor.cement_rate.repository"

@injectable()
@controller("/cement_rate", verifyCustomToken("vendor"))
export class CementRateController {
  constructor(
    @inject(VendorTypes.CementRateRepository)
    private cementRateRepo: CementRateRepository
  ) {}

  @httpPost("/", validate(addCementRateSchema))
  async addCementRate(
    req: IAddCementRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.cementRateRepo.addCementRate(uid, req.body)
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:CementRateId", validate(editCementRateSchema))
  async editCementRate(
    req: IEditCementRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.cementRateRepo.editCementRate(
      req.user.uid,
      req.params.CementRateId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete("/:CementRateId", validate(removeCementRateSchema))
  async removeCementRate(
    req: IRemoveCementRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.cementRateRepo.removeCementRate(req.params.CementRateId)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getCementRateSchema))
  async List(req: IGetCementRateRequest, res: Response) {
    const data = await this.cementRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
