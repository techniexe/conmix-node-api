import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ICementRate {
  address_id: string
  brand_id: string
  grade_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddCementRateRequest extends IAuthenticatedRequest {
  body: ICementRate
}

export const addCementRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex).required(),
    grade_id: Joi.string().regex(objectIdRegex).required(),
    per_kg_rate: Joi.number().min(1).required(),
  }),
}

export interface IEditCementRate {
  address_id: string
  brand_id: string
  grade_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditCementRateRequest extends IAuthenticatedRequest {
  params: {
    CementRateId: string
  }
  body: IEditCementRate
}

export const editCementRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number().min(1),
  }),

  params: Joi.object().keys({
    CementRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveCementRateRequest extends IAuthenticatedRequest {
  params: {
    CementRateId: string
  }
}

export const removeCementRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    CementRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetCementRateList {
  before?: string
  after?: string
  address_id?: string
  brand_id?: string
  grade_id?: string
  per_kg_rate?: any
  per_kg_rate_value?: string
}

export interface IGetCementRateRequest extends IAuthenticatedRequest {
  query: IGetCementRateList
}

export const getCementRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    brand_id: Joi.string().regex(objectIdRegex),
    grade_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number(),
    per_kg_rate_value: Joi.string(),
  }),
}
