import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ISandSourceRate {
  address_id: string
  source_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddSandSourceRateRequest extends IAuthenticatedRequest {
  body: ISandSourceRate
}

export const addSandSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex).required(),
    per_kg_rate: Joi.number().min(1).required(),
  }),
}

export interface IEditSandSourceRate {
  address_id: string
  source_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditSandSourceRateRequest extends IAuthenticatedRequest {
  params: {
    SandSourceRateId: string
  }
  body: IEditSandSourceRate
}

export const editSandSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number().min(1),
  }),

  params: Joi.object().keys({
    SandSourceRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveSandSourceRateRequest extends IAuthenticatedRequest {
  params: {
    SandSourceRateId: string
  }
}

export const removeSandSourceRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    SandSourceRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetSandSourceRateList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  per_kg_rate?: any
  per_kg_rate_value?: string
}

export interface IGetSandSourceRateRequest extends IAuthenticatedRequest {
  query: IGetSandSourceRateList
}

export const getSandSourceRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number(),
    per_kg_rate_value: Joi.string(),
  }),
}
