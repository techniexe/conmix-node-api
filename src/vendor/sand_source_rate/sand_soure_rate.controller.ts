import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { SandSourceRateRepository } from "./sand_soure_rate.repository"
import {
  addSandSourceRateSchema,
  IAddSandSourceRateRequest,
  editSandSourceRateSchema,
  IEditSandSourceRateRequest,
  removeSandSourceRateSchema,
  IRemoveSandSourceRateRequest,
  getSandSourceRateSchema,
  IGetSandSourceRateRequest,
} from "./sand_soure_rate.req-schema"

@injectable()
@controller("/sand_source_rate", verifyCustomToken("vendor"))
export class SandSourceRateController {
  constructor(
    @inject(VendorTypes.SandSourceRateRepository)
    private sandSourceRateRepo: SandSourceRateRepository
  ) {}

  @httpPost("/", validate(addSandSourceRateSchema))
  async addSandSourceRate(
    req: IAddSandSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.sandSourceRateRepo.addSandSourceRate(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:SandSourceRateId", validate(editSandSourceRateSchema))
  async editSandSourceRate(
    req: IEditSandSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.sandSourceRateRepo.editSandSourceRate(
      req.user.uid,
      req.params.SandSourceRateId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete("/:SandSourceRateId", validate(removeSandSourceRateSchema))
  async removeSandSourceRate(
    req: IRemoveSandSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.sandSourceRateRepo.removeSandSourceRate(
        req.params.SandSourceRateId
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getSandSourceRateSchema))
  async List(req: IGetSandSourceRateRequest, res: Response) {
    const data = await this.sandSourceRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
