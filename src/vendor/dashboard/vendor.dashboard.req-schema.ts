import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { monthsCode } from "../../utilities/config"

export interface IGetSupplierDashboardChartDataByDay {
  month?: string
  year?: number
  status?: string
}

export interface IGetSupplierDashboardChartDataByDayReq
  extends IAuthenticatedRequest {
  query: IGetSupplierDashboardChartDataByDay
}

export const GetSupplierDashboardChartDataByDay: IRequestSchema = {
  query: Joi.object().keys({
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    status: Joi.string(),
  }),
}

export interface IGetSupplierDashboardChartDataByMonth {
  year?: number
  status?: string
}

export interface IGetSupplierDashboardChartDataByMonthReq
  extends IAuthenticatedRequest {
  query: IGetSupplierDashboardChartDataByMonth
}

export const GetSupplierDashboardChartDataByMonth: IRequestSchema = {
  query: Joi.object().keys({
    year: Joi.number(),
    status: Joi.string(),
  }),
}

export interface IGetLatestVendorOrderDetails {
  before?: string
  after?: string
}
export interface IGetLatestVendorOrderRequest extends IAuthenticatedRequest {
  body: IGetLatestVendorOrderDetails
}

export interface IGetLatestDesignMix {
  before?: string
  after?: string
}
export interface IGetLatestDesignMixRequest extends IAuthenticatedRequest {
  body: IGetLatestDesignMix
}

export interface IGetSupplierDashboardChartBasedOnDesignMixDataByMonth {
  year?: number
  design_mix_id?: string
  grade_id?: string
  //region_id?: string
}

export interface IGetSupplierDashboardChartBasedOnDesignMixByMonthReq
  extends IAuthenticatedRequest {
  query: IGetSupplierDashboardChartBasedOnDesignMixDataByMonth
}

export const GetSupplierDashboardChartDataBasedOnDesignMixByMonth: IRequestSchema = {
  query: Joi.object().keys({
    year: Joi.number(),
    design_mix_id: Joi.string(),
    grade_id: Joi.string(),
    // region_id: Joi.string(),
  }),
}

export interface IGetSupplierDashboardChartBasedOnDesignMixDataByDay {
  month?: string
  year?: number
  design_mix_id?: string
  grade_id?: string
  //region_id?: string
}

export interface IGetSupplierDashboardChartBasedOnDesignMixByDayReq
  extends IAuthenticatedRequest {
  query: IGetSupplierDashboardChartBasedOnDesignMixDataByDay
}

export const GetSupplierDashboardChartDataBasedOnDesignMixByDay: IRequestSchema = {
  query: Joi.object().keys({
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    design_mix_id: Joi.string(),
    grade_id: Joi.string(),
    //region_id: Joi.string(),
  }),
}

export interface IGetInQueueVendorOrderDetails {
  before?: string
  after?: string
}
export interface IGetInQueueVendorOrderRequest extends IAuthenticatedRequest {
  body: IGetLatestVendorOrderDetails
}

export const GetTotalOrderDetails: IRequestSchema = {
  query: Joi.object().keys({
    date: Joi.date().iso().required(),
  }),
}

export interface IGetOrderDetailsRequest extends IAuthenticatedRequest {
  date: Date
}

export const GetDayCapacitySchema: IRequestSchema = {
  query: Joi.object().keys({
    delivery_date: Joi.date().iso().required(),
  }),
}

export interface IGetDayCapacityRequest extends IAuthenticatedRequest {
  delivery_date: Date
}

