import { SupplierOrderStatus } from "./../../model/surprise_order.model"
import { OrderItemModel } from "../../model/order_item.model"
import { injectable } from "inversify"
import { VendorOrderModel } from "../../model/vendor.order.model"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { monthsCode, OrderItemStatus } from "../../utilities/config"
import {
  IGetLatestVendorOrderDetails,
  IGetLatestDesignMix,
  IGetInQueueVendorOrderDetails,
} from "./vendor.dashboard.req-schema"
import { ProductModel } from "../../model/product.model"
import { DesignMixVariantModel } from "../../model/design_mixture_variant.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
import { CementOSModel } from "../../model/cement_os.model"
import { AdmixtureOSModel } from "../../model/admix_os.model"
import { AggregateOSModel } from "../../model/agg_os.model"
import { SandOSModel } from "../../model/sand_os.model"

@injectable()
export class DashboardRepository {
  async getDashboardDetails(user_id: string) {
    let data: { [k: string]: any } = {}
    const t_query: { [k: string]: any } = {}
    t_query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]
    //t_query.order_status  = { $ne: SupplierOrderStatus.REJECTED },
    data.totalVendorOrder = await VendorOrderModel.countDocuments(t_query)

    const query: { [k: string]: any } = {}
    query.$and = [
      {
        $or: [
          {
            sub_vendor_id: new ObjectId(user_id),
          },
          {
            master_vendor_id: new ObjectId(user_id),
          },
          {
            user_id: new ObjectId(user_id),
          },
        ]
      },
      {
        $or : [
        {
           order_status: { $ne: SupplierOrderStatus.RECEIVED },
        },
        {
           order_status: { $ne: SupplierOrderStatus.REJECTED },
        },
        {
          order_status: { $ne: SupplierOrderStatus.CANCELLED },
        },
        ]
      }
    ]
    // Total supplier order amount
    data.totalVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: {
          _id: null,
          total: {
            $sum: "$total_amount",
          },
        },
      },
    ])

    const to_query: { [k: string]: any } = {}
    let start = new Date()
    start.setHours(0, 0, 0, 0)
    to_query.created_at = { $gte: start }
    to_query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]
    //to_query.order_status  = { $ne: SupplierOrderStatus.REJECTED },
    data.todayVendorOrder = await VendorOrderModel.countDocuments(to_query)

    const today_amount_query: { [k: string]: any } = {}
    let start1 = new Date()
    start1.setHours(0, 0, 0, 0)
    today_amount_query.created_at = { $gte: start1 }
    today_amount_query.$and = [
      {
        $or: [
          {
            sub_vendor_id: new ObjectId(user_id),
          },
          {
            master_vendor_id: new ObjectId(user_id),
          },
          {
            user_id: new ObjectId(user_id),
          },
        ]
      },
      {
        $or : [
        {
           order_status: { $ne: SupplierOrderStatus.RECEIVED },
        },
        {
           order_status: { $ne: SupplierOrderStatus.REJECTED },
        },
        {
          order_status: { $ne: SupplierOrderStatus.CANCELLED },
        },
        ]
      }
    ]

    console.log("today_amount_query", today_amount_query)
    data.todayVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: today_amount_query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    //Get This week's order
    const week_query: { [k: string]: any } = {}
    let curr = new Date()
    curr.setHours(0, 0, 0, 0)
    var first = curr.getDate() - curr.getDay()

    var firstdayOfWeek = new Date(curr.setDate(first))
    week_query.created_at = { $gte: firstdayOfWeek }
    week_query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]
    //week_query.order_status  = { $ne: SupplierOrderStatus.REJECTED },
    data.vendorOrderOfThisWeek = await VendorOrderModel.countDocuments(
      week_query
    )

    const week_amount_query: { [k: string]: any } = {}
    week_amount_query.created_at = { $gte: firstdayOfWeek }
    week_amount_query.$and = [
      {
        $or: [
          {
            sub_vendor_id: new ObjectId(user_id),
          },
          {
            master_vendor_id: new ObjectId(user_id),
          },
          {
            user_id: new ObjectId(user_id),
          },
        ]
      },
      {
        $or : [
        {
           order_status: { $ne: SupplierOrderStatus.RECEIVED },
        },
        {
           order_status: { $ne: SupplierOrderStatus.REJECTED },
        },
        {
          order_status: { $ne: SupplierOrderStatus.CANCELLED },
        },
        ]
      }
    ]
    
    data.thisWeekVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: week_amount_query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    const month_query: { [k: string]: any } = {}
    var nowdate = new Date()
    var monthStartDay = new Date(nowdate.getFullYear(), nowdate.getMonth(), 1)
    month_query.created_at = { $gte: monthStartDay }
    month_query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]
    //month_query.order_status  = { $ne: SupplierOrderStatus.REJECTED },
    data.vendorOrderOfThisMonth = await VendorOrderModel.countDocuments(
      month_query
    )

    const month_amount_query: { [k: string]: any } = {}
    month_amount_query.created_at = { $gte: monthStartDay }
    month_amount_query.$and = [
      {
        $or: [
          {
            sub_vendor_id: new ObjectId(user_id),
          },
          {
            master_vendor_id: new ObjectId(user_id),
          },
          {
            user_id: new ObjectId(user_id),
          },
        ]
      },
      {
        $or : [
        {
           order_status: { $ne: SupplierOrderStatus.RECEIVED },
        },
        {
           order_status: { $ne: SupplierOrderStatus.REJECTED },
        },
        {
          order_status: { $ne: SupplierOrderStatus.CANCELLED },
        },
        ]
      }
    ]

    data.thisMonthVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: month_amount_query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    const cancelled_query: { [k: string]: any } = {}
    cancelled_query.order_status = SupplierOrderStatus.CANCELLED
    data.cancelOrderData = await VendorOrderModel.find(cancelled_query)

    const processing_query: { [k: string]: any } = {}
    query.order_status = SupplierOrderStatus.RECEIVED
    data.processingOrderData = await VendorOrderModel.find(processing_query)

    data.processingVendorOrderDataAmount = await VendorOrderModel.aggregate([
      { $match: query },
      {
        $group: { _id: null, total: { $sum: "$total_amount_without_margin" } },
      },
    ])

    return data
  }

  async getSupplierDashboardChartByDay(
    user_id: string,
    month: string | undefined,
    year: number | undefined,
    status: string | undefined
  ) {
    var now = new Date()
    let query: { [k: string]: any } = {}

    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]
    // query.user_id = new ObjectId(user_id)
    // If month or year not passed, then check with current date.
    if (isNullOrUndefined(month) || isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
      }
    } else {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }

    if (!isNullOrUndefined(status)) {
      if (status === SupplierOrderStatus.RECEIVED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.PICKUP) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.TM_ASSIGNED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.DELIVERED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.CANCELLED) {
        query.order_status = status
      }
      // if (status === SupplierOrderStatus.REJECT) {
      //   query.order_status = status
      // }
      // if (status === SupplierOrderStatus.REFUNDED) {
      //   query.order_status = status
      // }
    }
    console.log(query)
    return VendorOrderModel.aggregate([
      { $match: query },
      {
        $project: {
          _id: 1,
          year: { $year: "$created_at" },
          month: { $month: "$created_at" },
          day: { $dayOfMonth: "$created_at" },
          total_amount: 1,
          quantity: 1,
        },
      },
      {
        $group: {
          _id: { year: "$year", month: "$month", day: "$day" },
          total_amount: { $sum: "$total_amount" },
          quantity: { $sum: "$quantity" },
          count: { $sum: 1 },
        },
      },
    ])
  }

  async getSupplierDashboardChartByMonth(
    user_id: string,
    year: number | undefined,
    status: string | undefined
  ) {
    let query: { [k: string]: any } = {}

    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]

    // query.user_id = new ObjectId(user_id)
    // If year is not passed, then check with first date of current year.
    if (isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(new Date().getFullYear(), 0, 1),
      }
    } else {
      // If year is passed, then check with first and last date of given year.
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }

    if (!isNullOrUndefined(status)) {
      if (status === SupplierOrderStatus.RECEIVED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.PICKUP) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.TM_ASSIGNED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.DELIVERED) {
        query.order_status = status
      }
      if (status === SupplierOrderStatus.CANCELLED) {
        query.order_status = status
      }
      // if (status === SupplierOrderStatus.REJECT) {
      //   query.order_status = status
      // }
      // if (status === SupplierOrderStatus.REFUNDED) {
      //   query.order_status = status
      // }
    }

    return VendorOrderModel.aggregate([
      { $match: query },
      {
        $project: {
          month: { $substr: ["$created_at", 0, 7] },
          total_amount: 1,
          quantity: 1,
        },
      },
      {
        $group: {
          _id: "$month",
          total_amount: { $sum: "$total_amount" },
          quantity: { $sum: "$quantity" },
          count: { $sum: 1 },
        },
      },
    ])
  }

  async latestVendorOrder(
    user_id: string,
    searchParam: IGetLatestVendorOrderDetails
  ) {
    const query: { [k: string]: any } = {}

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    // Find new(PROCESSING) order by supplier .

    query.order_status = { $ne: SupplierOrderStatus.RECEIVED }
    console.log("latest_order")
    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const orderData = await VendorOrderModel.aggregate(aggregateArr)
    return Promise.resolve(orderData)
  }

  async getLatestDesignMixList(
    user_id: string,
    searchParam: IGetLatestDesignMix
  ) {
    const query: { [k: string]: any } = {}
    //  vendor_id: new ObjectId(user_id),

    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        vendor_id: new ObjectId(user_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          design_mix_id: true,
          grade_id: true,
          cement_brand_id: true,
          sand_source_id: true,
          aggregate_source_id: true,
          fly_ash_source_id: true,
          aggregate1_sub_category_id: true,
          aggregate2_sub_category_id: true,
          ad_mixture_brand_id: true,
          product_name: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          ad_mixture_quantity: true,
          water_quantity: true,
          selling_price: true,
          is_availble: true,
          description: true,
          is_custom: true,
          address_id: true,
          ad_mixture_category_id: true,
          cement_grade_id: true,
          updated_by_id: true,
          updated_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
        },
      },
    ]

    const designMixData = await DesignMixVariantModel.aggregate(aggregateArr)

    if (sort.created_at === 1) {
      designMixData.reverse()
    }
    return Promise.resolve(designMixData)
  }

  async getDesignMixBasedChartByMonth(
    user_id: string,
    year: number | undefined,
    design_mix_id: string | undefined,
    grade_id: string | undefined
    //region_id: string | undefined
  ) {
    let query: { [k: string]: any } = {}
    // query.user_id = new ObjectId(user_id)

    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]

    // If year is not passed, then check with first date of current year.
    if (isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(new Date().getFullYear(), 0, 1),
      }
    } else {
      // If year is passed, then check with first and last date of given year.
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }

    if (design_mix_id === "true") {
      query.design_mix_id = { $exists: true }
    }

    if (design_mix_id === "false") {
      query.design_mix_id = { $exists: false }
    }

    if (!isNullOrUndefined(grade_id)) {
      query.concrete_grade_id = new ObjectId(grade_id)

      // let products = await ProductModel.find({ grade_id })
      // let p_ids = products.map(({ _id }) => _id)
      // query.product_id = { $in: p_ids }
    }

    // if (!isNullOrUndefined(region_id)) {
    //   let productsData = await ProductModel.find({ region_id })
    //   let product_ids = productsData.map(({ _id }) => _id)
    //   query.product_id = { $in: product_ids }
    // }

    console.log(query)
    return VendorOrderModel.aggregate([
      { $match: query },
      {
        $project: {
          month: { $substr: ["$created_at", 0, 7] },
          total_amount: 1,
          quantity: 1,
        },
      },
      {
        $group: {
          _id: "$month",
          total_amount: { $sum: "$total_amount" },
          quantity: { $sum: "$quantity" },
          count: { $sum: 1 },
        },
      },
    ])
  }

  async getDesignMixBasedChartByDay(
    user_id: string,
    month: string | undefined,
    year: number | undefined,
    design_mix_id: string | undefined,
    grade_id: string | undefined
    //region_id: string | undefined
  ) {
    var now = new Date()
    let query: { [k: string]: any } = {}

    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]

    // query.user_id = new ObjectId(user_id)
    // If month or year not passed, then check with current date.
    if (isNullOrUndefined(month) || isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0),
      }
    } else {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }

    if (!isNullOrUndefined(design_mix_id)) {
      query.design_mix_id = new ObjectId(design_mix_id)
    }

    if (isNullOrUndefined(design_mix_id)) {
      query.design_mix_id = { $exists: false }
    }

    if (!isNullOrUndefined(grade_id)) {
      query.concrete_grade_id = new ObjectId(grade_id)

      // let products = await ProductModel.find({ grade_id })
      // let p_ids = products.map(({ _id }) => _id)
      // query.product_id = { $in: p_ids }
    }

    // if (!isNullOrUndefined(region_id)) {
    //   let productsData = await ProductModel.find({ region_id })
    //   let product_ids = productsData.map(({ _id }) => _id)
    //   query.product_id = { $in: product_ids }
    // }

    console.log(query)
    return VendorOrderModel.aggregate([
      { $match: query },
      {
        $project: {
          _id: 1,
          year: { $year: "$created_at" },
          month: { $month: "$created_at" },
          day: { $dayOfMonth: "$created_at" },
          total_amount: 1,
          quantity: 1,
        },
      },
      {
        $group: {
          _id: { year: "$year", month: "$month", day: "$day" },
          total_amount: { $sum: "$total_amount" },
          quantity: { $sum: "$quantity" },
          count: { $sum: 1 },
        },
      },
    ])
  }

  async checkProductStock(user_id: string) {
    const cementquery: { [k: string]: any } = {}

    cementquery.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    let vendorData = await VendorUserModel.findOne({
      _id: new ObjectId(user_id),
    })
    console.log("vendorData", vendorData)
    let vendor_capacity: any
    if (!isNullOrUndefined(vendorData)) {
      vendor_capacity =
        vendorData.no_of_plants *
        vendorData.plant_capacity_per_hour *
        vendorData.no_of_operation_hour
    }
    console.log("vendor_capacity", vendor_capacity)

    let cement_total_quantity = 130 * vendor_capacity
    const cementData = await CementOSModel.aggregate([
      {
        $match: cementquery,
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "cement_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ])
    const admixquery: { [k: string]: any } = {}
    admixquery.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    let admix_total_quantity = 0.01 * vendor_capacity
    const admixData = await AdmixtureOSModel.aggregate([
      {
        $match: admixquery,
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "brand_id",
          foreignField: "_id",
          as: "admixture_brand",
        },
      },
      {
        $unwind: {
          path: "$admixture_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "admixture_category",
          localField: "category_id",
          foreignField: "_id",
          as: "admixture_category",
        },
      },
      {
        $unwind: {
          path: "$admixture_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "admixture_brand._id": 1,
          "admixture_brand.name": 1,
          "admixture_category._id": 1,
          "admixture_category.category_name": 1,
          "admixture_category.admixture_type": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ])

    const aggquery: { [k: string]: any } = {}
    aggquery.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    const aggData = await AggregateOSModel.aggregate([
      {
        $match: aggquery,
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "sub_cat_id",
          foreignField: "_id",
          as: "aggregate_sand_sub_category",
        },
      },
      {
        $unwind: {
          path: "$aggregate_sand_sub_category",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate_sand_sub_category._id": 1,
          "aggregate_sand_sub_category.sub_category_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ])
    //let agg_total_quantity: any
    for (let i = 0; i < aggData.length; i++) {
      if (aggData[i].aggregate_sand_sub_category.sub_category_name === "10mm") {
        aggData[i].agg_total_quantity = 416 * vendor_capacity
      } else if (
        aggData[i].aggregate_sand_sub_category.sub_category_name === "20mm"
      ) {
        aggData[i].agg_total_quantity = 848 * vendor_capacity
      } else if (
        aggData[i].aggregate_sand_sub_category.sub_category_name === "40mm"
      ) {
        aggData[i].agg_total_quantity = 690 * vendor_capacity
      }
    }

    const sanduery: { [k: string]: any } = {}
    sanduery.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]
    let sand_total_quantity = 640 * vendor_capacity
    const sandData = await SandOSModel.aggregate([
      {
        $match: sanduery,
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "sand_source",
          localField: "source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: {
          path: "$fly_ash_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          quantity: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ])

    return Promise.resolve({
      cementData,
      cement_total_quantity,
      admixData,
      aggData,
      sandData,
      admix_total_quantity,
      sand_total_quantity,
    })
  }

  async getProductStock(user_id: string) {
    console.log(user_id)

    const query: { [k: string]: any } = {}
    query.$or = [
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
      {
        user_id: new ObjectId(user_id),
      },
    ]

    const productData = await ProductModel.aggregate([
      {
        $match: {
          query,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_category",
          localField: "category_id",
          foreignField: "_id",
          as: "productCategory",
        },
      },
      {
        $unwind: { path: "$productCategory" },
      },
      {
        $lookup: {
          from: "product_sub_category",
          localField: "sub_category_id",
          foreignField: "_id",
          as: "productSubCategory",
        },
      },
      {
        $unwind: { path: "$productSubCategory" },
      },
      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address" },
      },
      {
        $lookup: {
          from: "source",
          localField: "address.source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          name: 1,
          quantity: 1,
          stock_updated_at: 1,
          created_at: 1,
          "productCategory._id": 1,
          "productCategory.category_name": 1,
          "productCategory.image_url": 1,
          "productCategory.thumbnail_url": 1,
          "productSubCategory._id": 1,
          "productSubCategory.sub_category_name": 1,
          "productSubCategory.selling_unit": 1,
          "productSubCategory.image_url": 1,
          "productSubCategory.thumbnail_url": 1,
          "address.source_name": "$source.source_name",
          //media: 1,
          // unit_price: 1,
          // verified_by_admin: 1,
          // minimum_order: 1,
          // is_available: 1,
          // "supplier.full_name": 1,
          // "supplier.mobile_number": 1,
          // "address.line1": 1,
          // "address.line2": 1,
          // "address.state_name": "$state.state_name",
          // "address.city_name": "$city.city_name",
          // "address.pincode": 1,
          // "address.location": 1,
        },
      },
      // {
      //   $group: {
      //     _id: "$sub_category_id",
      //     // _id: {
      //     //   sub_category: "$sub_category_id",
      //     //   //region: "$region_id",
      //     // },
      //     totalQuantity: { $sum: "$quantity" },
      //     count: { $sum: 1 },
      //   },
      // },
      // {
      //   $lookup: {
      //     from: "product_sub_category",
      //     // localField: "_id.sub_category",
      //     localField: "_id",
      //     foreignField: "_id",
      //     as: "product_sub_category",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$product_sub_category",
      //   },
      // },
      // {
      //   $project: {
      //     total_quantity: "$totalQuantity",
      //     "product_sub_category._id": true,
      //     "product_sub_category.sub_category_name": true,
      //     "product_sub_category.image_url": true,
      //     "product_sub_category.thumbnail_url": true,
      //   },
      // },
    ])

    console.log(productData)
    if (!isNullOrUndefined(productData) && productData.length > 0) {
      for (let i = 0; i < productData.length; i++) {
        const data = await OrderItemModel.findOne({
          supplier_id: new ObjectId(user_id),
          product_id: productData[i]._id,
          item_status: OrderItemStatus.DELIVERED,
          created_at: { $gte: productData[i].stock_updated_at },
        })

        console.log(data)
        if (!isNullOrUndefined(data)) {
          productData[i].remaining_quantity =
            productData[i].quantity - data.quantity
        }
      }
    }

    return Promise.resolve(productData)
  }

  async inQueueVendorOrder(
    user_id: string,
    searchParam: IGetInQueueVendorOrderDetails
  ) {
    const query: { [k: string]: any } = {}

    //query.user_id = new ObjectId(user_id)

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    // Find new(PROCESSING) order by supplier .

    query.order_status = SupplierOrderStatus.RECEIVED

    console.log(query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 5 },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "vendor",
          localField: "user_id",
          foreignField: "_id",
          as: "vendor",
        },
      },
      {
        $unwind: {
          path: "$vendor",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          igst_amount: true,
          sgst_amount: true,
          cgst_amount: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          max_accepted_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
        },
      },
    ]
    const orderData = await VendorOrderModel.aggregate(aggregateArr)
    return Promise.resolve(orderData)
  }

  async getOrderDetails(user_id: string, date: Date) {
    const query: { [k: string]: any } = {}

    //query.user_id = new ObjectId(user_id)

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    query.delivery_date = new Date(date)
    // let req_date = new Date(date)
    // req_date.setHours(0, 0, 0, 0)
    // query.delivery_date = req_date
    console.log("queryyyyyyyyy", query)

    let totalVendorOrder = await VendorOrderModel.countDocuments(query)
    let totalVendorOrderAmount = await VendorOrderModel.aggregate([
      { $match: query },
      { $group: { _id: null, total: { $sum: "$total_amount" } } },
    ])
    let totalvendor_capacity
    let vendorData = await VendorUserModel.findOne({
      _id: new ObjectId(user_id),
    })
    if (!isNullOrUndefined(vendorData)) {
      totalvendor_capacity =
        vendorData.no_of_plants *
        vendorData.plant_capacity_per_hour *
        vendorData.no_of_operation_hour
    }

    return {
      totalVendorOrder,
      totalVendorOrderAmount,
      totalvendor_capacity,
    }
  }

  async getCapacity(user_id: string, delivery_date: Date) {
    console.log("user_id", user_id)
    let vendorData = await VendorUserModel.findOne({
      _id: new ObjectId(user_id),
      //  verified_by_admin: true,
    })
    console.log("vendorData", vendorData)
    let vendor_capacity: any
    if (!isNullOrUndefined(vendorData)) {
      vendor_capacity =
        vendorData.no_of_plants *
        vendorData.plant_capacity_per_hour *
        vendorData.no_of_operation_hour
      let orderPartData: any = await OrderItemPartModel.find({
        vendor_id: new ObjectId(user_id),
        assigned_date: new Date(delivery_date),
      })

      if (!isNullOrUndefined(orderPartData)) {
        var day_capacity = 0
        for (var k = 0; k < orderPartData.length; k++) {
          day_capacity += orderPartData[k].assigned_quantity
        }
        console.log("day_capacity", day_capacity)
        return Promise.resolve({ day_capacity, vendor_capacity })
      }
    }
  }
}
