import { injectable, inject } from "inversify"
import { controller, httpGet } from "inversify-express-utils"
import { NextFunction, Response } from "express-serve-static-core"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { DashboardRepository } from "./vendor.dashboard.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  GetSupplierDashboardChartDataByDay,
  IGetSupplierDashboardChartDataByDayReq,
  GetSupplierDashboardChartDataByMonth,
  IGetSupplierDashboardChartDataByMonthReq,
  IGetLatestVendorOrderRequest,
  IGetLatestDesignMixRequest,
  GetSupplierDashboardChartDataBasedOnDesignMixByMonth,
  IGetSupplierDashboardChartBasedOnDesignMixByMonthReq,
  GetSupplierDashboardChartDataBasedOnDesignMixByDay,
  IGetSupplierDashboardChartBasedOnDesignMixByDayReq,
  IGetInQueueVendorOrderRequest,
  GetTotalOrderDetails,
  IGetOrderDetailsRequest,
  GetDayCapacitySchema,
  IGetDayCapacityRequest,
} from "./vendor.dashboard.req-schema"

@injectable()
@controller("/dashboard", verifyCustomToken("vendor"))
export class DashboardController {
  constructor(
    @inject(VendorTypes.DashboardRepository)
    private dashboardRepo: DashboardRepository
  ) {}

  @httpGet("/")
  async getDashboardDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getDashboardDetails(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/daychart", validate(GetSupplierDashboardChartDataByDay))
  async getChartByDay(
    req: IGetSupplierDashboardChartDataByDayReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getSupplierDashboardChartByDay(
        req.user.uid,
        req.query.month,
        req.query.year,
        req.query.status
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/monthchart", validate(GetSupplierDashboardChartDataByMonth))
  async getChartByMonth(
    req: IGetSupplierDashboardChartDataByMonthReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getSupplierDashboardChartByMonth(
        req.user.uid,
        req.query.year,
        req.query.status
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/latestVendorOrder")
  async latestVendorOrder(
    req: IGetLatestVendorOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.latestVendorOrder(
        req.user.uid,
        req.query
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/inQueueVendorOrder")
  async inQueueVendorOrder(
    req: IGetInQueueVendorOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.inQueueVendorOrder(
        req.user.uid,
        req.query
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/latestDesignMix")
  async getLatestDesignMixList(
    req: IGetLatestDesignMixRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getLatestDesignMixList(
        req.user.uid,
        req.query
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/designMixBasedmonthchart",
    validate(GetSupplierDashboardChartDataBasedOnDesignMixByMonth)
  )
  async getDesignMixBasedChartByMonth(
    req: IGetSupplierDashboardChartBasedOnDesignMixByMonthReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getDesignMixBasedChartByMonth(
        req.user.uid,
        req.query.year,
        req.query.design_mix_id,
        req.query.grade_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet(
    "/designMixBasedDaychart",
    validate(GetSupplierDashboardChartDataBasedOnDesignMixByDay)
  )
  async getDesignMixBasedChartByDay(
    req: IGetSupplierDashboardChartBasedOnDesignMixByDayReq,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getDesignMixBasedChartByDay(
        req.user.uid,
        req.query.month,
        req.query.year,
        req.query.design_mix_id,
        req.query.grade_id
        // req.query.region_id
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/totalOrder", validate(GetTotalOrderDetails))
  async getOrderDetails(
    req: IGetOrderDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getOrderDetails(
        req.user.uid,
        req.query.date
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/checkProductStock")
  async checkProductStock(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.checkProductStock(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /************************************************************************* */

  @httpGet("/productStock")
  async getProductStock(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getProductStock(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/dayCapacity", validate(GetDayCapacitySchema))
  async getCapacity(
    req: IGetDayCapacityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.dashboardRepo.getCapacity(req.user.uid, req.query.delivery_date)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
