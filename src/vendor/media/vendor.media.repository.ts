import { ObjectId } from "bson"
import { injectable } from "inversify"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput } from "../../utilities/customError"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"
import { VendorMediaModel } from "../../model/vendor_media.model"

@injectable()
export class VendorMediaRepository {
  async addMedia(vendor_id: string, fileData: { media_url?: UploadedFile }) {
    const data = await VendorMediaModel.findOne({ vendor_id, type: "PRIMARY" })
    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Primary media already added, so please delete it and update.`,
          400
        )
      )
    }
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.media_url) &&
      !isNullOrUndefined(fileData.media_url.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor/media`
      let objectName = `${Date.now()}_${Math.random()}`

      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.media_url.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.media_url.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg and mp4 supported`,
            400
          )
        )
      }

      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.media_url.data,
        fileData.media_url.mimetype
      )
      const mediaObj: { [k: string]: any } = {
        media_url: getS3MediaURL(`${objectDir}/${objectName}`),
        media_type: "image",
        vendor_id,
        type: "PRIMARY",
      }

      const newMedia = new VendorMediaModel(mediaObj)
      const newMediaDoc = await newMedia.save()
      return Promise.resolve(newMediaDoc)
    } else {
      return Promise.reject(
        new InvalidInput(`Media not found in request payload.`, 400)
      )
    }
  }

  async addSecondaryMedia(
    vendor_id: string,
    fileData?: {
      media_url?: [UploadedFile]
    }
  ) {
    const objectDir = `${
      process.env.NODE_ENV !== "production" ? "d" : "p"
    }/vendor/media`

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.media_url)
    ) {
      if (!Array.isArray(fileData.media_url) || fileData.media_url.length < 1) {
        return Promise.reject(
          new InvalidInput(
            JSON.stringify({
              error: {
                message: `Invalid media_url payload. Array required.`,
                details: [
                  {
                    path: "media_url",
                    description: `Invalid media_url payload. Array required.`,
                  },
                ],
              },
            })
          )
        )
      }
      await Promise.all(
        fileData.media_url.map(async (media) => {
          let objectName = `${vendor_id}_${Math.random()}`
          let media_type = "image"
          if (
            ["image/jpg", "image/jpeg"].includes(media.mimetype) &&
            /\.jpe?g$/i.test(media.name)
          ) {
            objectName += ".jpg"
            media_type = "image"
          } else if (
            media.mimetype === "video/mp4" &&
            /\.mp4$/i.test(media.name)
          ) {
            objectName += ".mp4"
            media_type = "video"
          } else {
            return Promise.reject(
              new InvalidInput(
                JSON.stringify({
                  error: {
                    message: `Invalid media media format. Only jpg and jpeg supported`,
                    details: [
                      {
                        path: "media",
                        description: `Invalid media media format. Only jpg and jpeg supported`,
                      },
                    ],
                  },
                })
              )
            )
          }
          const objectPath = `${objectDir}/${objectName}`
          await putObject(
            awsConfig.s3MediaBucket,
            objectPath,
            media.data,
            media.mimetype
          )
          return new VendorMediaModel({
            vendor_id,
            media_url: getS3MediaURL(`${objectDir}/${objectName}`),
            media_type,
            type: "SECONDARY",
          }).save()
        })
      )
    } else {
      return Promise.reject(
        new InvalidInput(`Media not found in request payload.`, 400)
      )
    }
  }

  async getMedia(vendor_id: string) {
    const query: { [k: string]: any } = {
      vendor_id,
    }
    const media = await VendorMediaModel.find(query)
    return Promise.resolve(media)
  }

  async deleteVendorMedia(mediaId: string, vendor_id: string) {
    return VendorMediaModel.deleteOne({
      _id: new ObjectId(mediaId),
      vendor_id: new ObjectId(vendor_id),
    })
  }
}
