import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"
import * as Joi from "joi"
import { objectIdRegex, IRequestSchema } from "../../middleware/joi.middleware"

export interface IAddPrimaryMediaReq extends IAuthenticatedRequest {
  files: {
    media_url: UploadedFile
  }
}

export interface IAddSecondaryMediaRequest extends IAuthenticatedRequest {
  files: {
    media_url: [UploadedFile]
  }
}

export interface IDeleteVendorMedia extends IAuthenticatedRequest {
  body: {
    mediaId: string
  }
}

export const deleteVendorMediaSchema: IRequestSchema = {
  body: Joi.object().keys({
    mediaId: Joi.string().regex(objectIdRegex).required(),
  }),
}
