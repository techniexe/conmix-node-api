import { IAuthenticatedRequest } from "./../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpDelete,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import {
  IAddPrimaryMediaReq,
  IAddSecondaryMediaRequest,
  deleteVendorMediaSchema,
  IDeleteVendorMedia,
} from "./vendor.media.req-schema"
import { VendorTypes } from "../vendor.types"
import { VendorMediaRepository } from "./vendor.media.repository"
import { Response, NextFunction } from "express"
import { validate } from "../../middleware/joi.middleware"

@injectable()
@controller("/media", verifyCustomToken("vendor"))
export class VendorMediaController {
  constructor(
    @inject(VendorTypes.VendorMediaRepository)
    private MediaRepo: VendorMediaRepository
  ) {}
  /* add primary media in Vendor */
  @httpPost("/primary")
  async addMedia(req: IAddPrimaryMediaReq, res: Response) {
    await this.MediaRepo.addMedia(req.user.uid, req.files)
    res.sendStatus(204)
  }

  /* add primary media in Vendor */
  @httpPost("/secondary")
  async addSecondaryMedia(
    req: IAddSecondaryMediaRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.MediaRepo.addSecondaryMedia(req.user.uid, req.files)
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  /* get media of vendor */
  @httpGet("/")
  async getMedia(req: IAuthenticatedRequest, res: Response) {
    const media = await this.MediaRepo.getMedia(req.user.uid)
    res.json({ data: media })
  }

  /**
   * Delete Vendor media
   */
  @httpDelete("/", validate(deleteVendorMediaSchema))
  async deleteVendorMedia(req: IDeleteVendorMedia, res: Response) {
    await this.MediaRepo.deleteVendorMedia(req.body.mediaId, req.user.uid)
    res.sendStatus(204)
  }
}
