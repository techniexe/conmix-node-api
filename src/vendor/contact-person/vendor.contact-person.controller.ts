import { injectable, inject } from "inversify"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import { ContactpersonRepository } from "./vendor.contact-person.repository"
import { VendorTypes } from "../vendor.types"
import {
  addContactpersonSchema,
  getContactpersonSchema,
  editContactpersonSchema,
} from "./vendor.contact-person.request-schema"
import { NextFunction, Response } from "express"
import {
  IAddContactpersonRequest,
  IEditContactpersonRequest,
} from "./vendor.contat-person.request-definations"
import { validate } from "../../middleware/joi.middleware"

@injectable()
@controller("/contact-person", verifyCustomToken("supplier"))
export class ContactpersonController {
  constructor(
    @inject(VendorTypes.ContactpersonRepository)
    private contactpersonRepo: ContactpersonRepository
  ) {}

  @httpPost("/", validate(addContactpersonSchema))
  async addContactperson(
    req: IAddContactpersonRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { contact } = req.body
      const { _id } = await this.contactpersonRepo.addContactperson(
        uid,
        contact
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getContactperson(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { search, before, after } = req.query
      const data = await this.contactpersonRepo.getContactpersons(
        uid,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/:contactpersonId", validate(getContactpersonSchema))
  async getContactpersonById(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.contactpersonRepo.getContactpersonById(
        req.user.uid,
        req.params.contactpersonId
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:contactpersonId", validate(editContactpersonSchema))
  async editContactperson(
    req: IEditContactpersonRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.contactpersonRepo.editContactperson(
        req.user.uid,
        req.params.contactpersonId,
        req.body.contact
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }
}
