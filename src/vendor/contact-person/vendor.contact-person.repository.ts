import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import {
  IContactperson,
  IEditContactperson,
} from "./vendor.contat-person.request-definations"
import { ContactModel, IContactDoc } from "../../model/contact.model"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"

@injectable()
export class ContactpersonRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addContactperson(user_id: string, contactData: IContactperson) {
    contactData.user = { user_id }
    contactData.user.user_type = "supplier"
    const newContactData = new ContactModel(contactData)
    const newContactDataDoc = await newContactData.save()
    return Promise.resolve(newContactDataDoc)
  }

  async getContactpersons(
    user_id: string,
    search?: string,
    before?: string,
    after?: string
  ): Promise<IContactDoc[]> {
    let query: { [k: string]: any } = {
      "user.user_id": new ObjectId(user_id),
      "user.user_type": "supplier",
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    return ContactModel.find(query).limit(10)
  }

  async getContactpersonById(
    user_id: string,
    contactpersonId: string
  ): Promise<IContactDoc | null> {
    return ContactModel.findOne({
      "user.user_id": user_id,
      "user.user_type": "supplier",
      _id: contactpersonId,
    })
  }

  async editContactperson(
    user_id: string,
    contactpersonId: string,
    contactpersonData: IEditContactperson
  ) {
    const flds = ["person_name", "title", "profile_pic", "landline_number"]

    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(contactpersonData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const conatctPersonCheck = await ContactModel.findOne({
      _id: contactpersonId,
    })
    if (conatctPersonCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    if (
      !isNullOrUndefined(contactpersonData.email) &&
      conatctPersonCheck.email !== contactpersonData.email
    ) {
      editDoc.email = contactpersonData.email
      editDoc.email_verified = false
    }

    if (
      !isNullOrUndefined(contactpersonData.mobile_number) &&
      conatctPersonCheck.mobile_number !== contactpersonData.mobile_number
    ) {
      editDoc.mobile_number = contactpersonData.mobile_number
      editDoc.mobile_number_verified = false
    }

    if (
      !isNullOrUndefined(contactpersonData.alt_mobile_number) &&
      conatctPersonCheck.alt_mobile_number !==
        contactpersonData.alt_mobile_number
    ) {
      editDoc.alt_mobile_number = contactpersonData.alt_mobile_number
      editDoc.alt_mobile_number_verified = false
    }

    if (
      !isNullOrUndefined(contactpersonData.whatsapp_number) &&
      conatctPersonCheck.whatsapp_number !== contactpersonData.whatsapp_number
    ) {
      editDoc.whatsapp_number = contactpersonData.whatsapp_number
      editDoc.whatsapp_number_verified = false
    }

    const res = await ContactModel.findOneAndUpdate(
      {
        _id: contactpersonId,
        "user.user_id": user_id,
        "user.user_type": "supplier",
      },
      { $set: editDoc }
    )

    if (res === null) {
      const err = new UnexpectedInput(`This contactperson doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "supplier",
      AccessTypes.EDIT_CONTACTPERSON_BY_VENDOR,
      `Supplier updated details contact person of id ${contactpersonId} .`,
      res,
      editDoc
    )

    return Promise.resolve()
  }
}
