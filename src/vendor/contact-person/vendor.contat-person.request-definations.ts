import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IContactperson {
  user: {
    user_id?: string
    user_type?: string
  }
  person_name: string
  title: string
  email: string
  email_verified: boolean
  mobile_number: string
  mobile_number_verified: boolean
  landline_number: string
  alt_mobile_number: string
  alt_mobile_number_verified: boolean
  whatsapp_number: string
  whatsapp_number_verified: boolean
  // profile_pic: string
}

export interface IAddContactpersonRequest extends IAuthenticatedRequest {
  body: {
    contact: IContactperson
  }
}

export interface IEditContactperson {
  user: {
    user_id?: string
    user_type?: string
  }
  person_name?: string
  title?: string
  email?: string
  email_verified?: boolean
  mobile_number?: string
  mobile_number_verified?: boolean
  landline_number?: string
  alt_mobile_number?: string
  alt_mobile_number_verified?: boolean
  whatsapp_number?: string
  whatsapp_number_verified?: boolean
  profile_pic?: string
}

export interface IEditContactpersonRequest extends IAuthenticatedRequest {
  body: {
    contact: IEditContactperson
  }
}
