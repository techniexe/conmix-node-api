import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export const addContactpersonSchema: IRequestSchema = {
  body: Joi.object().keys({
    contact: Joi.object()
      .keys({
        person_name: Joi.string().required(),
        title: Joi.string().required(),
        email: Joi.string().required(),
        mobile_number: Joi.string().required(),
        landline_number: Joi.string(),
        alt_mobile_number: Joi.string(),
        whatsapp_number: Joi.string(),
        //profile_pic: Joi.string().required(),
      })
      .required(),
  }),
}

export const getContactpersonSchema: IRequestSchema = {
  params: Joi.object().keys({
    contactpersonId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const editContactpersonSchema: IRequestSchema = {
  body: Joi.object().keys({
    contact: Joi.object().keys({
      person_name: Joi.string(),
      title: Joi.string(),
      email: Joi.string(),
      mobile_number: Joi.string(),
      landline_number: Joi.string(),
      alt_mobile_number: Joi.string(),
      whatsapp_number: Joi.string(),
      profile_pic: Joi.string(),
    }),
    params: Joi.object().keys({
      contactpersonId: Joi.string().regex(objectIdRegex).required(),
    }),
  }),
}
