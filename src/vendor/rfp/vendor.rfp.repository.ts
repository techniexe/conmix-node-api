import { injectable, inject } from "inversify"
import { AccessTypes } from "../../model/access_logs.model"
import { CommonService } from "../../utilities/common.service"
import { VendorTypes } from "../vendor.types"
import { RfpModel } from "../../model/rfp.model"
import { IRfp } from "./vendor.rfp.req-schema"
import { ObjectId } from "bson"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { escapeRegExp } from "../../utilities/mongoose.utilities"

@injectable()
export class SupplierRfpRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addRfp(user_id: string, rfpData: IRfp) {
    rfpData.user_id = user_id
    const newRfp = new RfpModel(rfpData)

    const newrfpDoc = await newRfp.save()

    await this.commonService.addActivityLogs(
      user_id,
      "supplier",
      AccessTypes.ADD_RFP_BY_VENDOR,
      `Supplier requested for product category of id ${newrfpDoc._id} .`
    )

    return Promise.resolve(newrfpDoc)
  }

  async getRfp(
    user_id: string,
    contact_person_id?: string,
    before?: string,
    after?: string,
    category_name?: string,
    sub_category_name?: string,
    status?: string
  ) {
    const query: { [k: string]: any } = {
      user_id: new ObjectId(user_id),
    }

    if (!isNullOrUndefined(category_name)) {
      query.category_name = new RegExp(escapeRegExp(category_name), "i")
    }

    if (!isNullOrUndefined(sub_category_name)) {
      query.sub_category_name = new RegExp(escapeRegExp(sub_category_name), "i")
    }

    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }

    if (!isNullOrUndefined(contact_person_id)) {
      query.contact_person_id = new ObjectId(contact_person_id)
    }

    if (!isNullOrUndefined(status)) {
      query.status = status
    }

    return RfpModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { requested_at: 1 },
      },
      {
        $limit: 40,
      },
      {
        $lookup: {
          from: "contact",
          localField: "contact_person_id",
          foreignField: "_id",
          as: "contactDetails",
        },
      },
      {
        $unwind: { path: "$contactDetails", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "address",
          localField: "pickup_address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: { path: "$address", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "address.state_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: { path: "$city", preserveNullAndEmptyArrays: true },
      },

      {
        $project: {
          _id: 1,
          user_id: 1,
          category_name: 1,
          sub_category_name: 1,
          contact_person_id: 1,
          pickup_address_id: 1,
          description: 1,
          status: 1,
          requested_at: 1,
          quantity: 1,
          "contactDetails.person_name": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_name": "$state.state_name",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
        },
      },
    ])
  }
}
