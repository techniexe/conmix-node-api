import { controller, httpPost, httpGet } from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { VendorTypes } from "../vendor.types"
import { SupplierRfpRepository } from "./vendor.rfp.repository"
import {
  addRfpSchema,
  IAddRfpRequest,
  getRfpBySupplier,
  IGetRfpBySupplier,
} from "./vendor.rfp.req-schema"

@controller("/rfp", verifyCustomToken("supplier"))
@injectable()
export class SupplierRfpController {
  constructor(
    @inject(VendorTypes.SupplierRfpRepository)
    private rfpRepo: SupplierRfpRepository
  ) {}
  @httpPost("/", validate(addRfpSchema))
  async addRfp(req: IAddRfpRequest, res: Response, next: NextFunction) {
    try {
      await this.rfpRepo.addRfp(req.user.uid, req.body)
      res.sendStatus(201)
    } catch (error) {
      next(error)
    }
  }

  @httpGet("/", validate(getRfpBySupplier))
  async getRfp(req: IGetRfpBySupplier, res: Response, next: NextFunction) {
    try {
      const {
        contact_person_id,
        before,
        after,
        category_name,
        sub_category_name,
        status,
      } = req.query
      const data = await this.rfpRepo.getRfp(
        req.user.uid,
        contact_person_id,
        before,
        after,
        category_name,
        sub_category_name,
        status
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
