import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IRfp {
  user_id: string
  contact_person_id: string
  category_name: string
  sub_category_name: string
  quantity: number
  pickup_address_id: string
  description?: string
  status: string
  requested_at: Date
  requested_by_id: string
}

export interface IAddRfpRequest extends IAuthenticatedRequest {
  body: IRfp
}
export const addRfpSchema: IRequestSchema = {
  body: Joi.object().keys({
    contact_person_id: Joi.string().regex(objectIdRegex).required(),
    category_name: Joi.string().max(64).required(),
    sub_category_name: Joi.string().max(64),
    quantity: Joi.number().min(0).required(),
    pickup_address_id: Joi.string().regex(objectIdRegex).required(),
    description: Joi.string().max(5000),
  }),
}

export interface IGetRfpBySupplier extends IAuthenticatedRequest {
  query: {
    contact_person_id?: string
    before?: string
    after?: string
    category_name?: string
    sub_category_name?: string
    status?: string
  }
}

export const getRfpBySupplier: IRequestSchema = {
  query: Joi.object().keys({
    contact_person_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    category_name: Joi.string(),
    sub_category_name: Joi.string(),
    status: Joi.string().valid(["Unverified", "Verified"]),
  }),
}
