import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { ISetting, IEditSetting } from "./vendor.setting.req.schema"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorSettingModel } from "../../model/vendor.setting.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { AuthModel } from "../../model/auth.model"
import { isNullOrUndefined } from "../../utilities/type-guards"

@injectable()
export class VendorSettingRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addSetting(uid: string, reqBody: ISetting) {
    reqBody.vendor_id = uid

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.ADD_SETTING,
      "Setting Added."
    )

    return new VendorSettingModel(reqBody).save()
  }

  async editSetting(user_id: string, settingData: IEditSetting) {
    const flds = [
      "with_TM",
      "TM_price",
      "with_CP",
      "CP_price",
      "is_customize_design_mix",
      "new_order_taken",
    ]

    if (settingData.new_order_taken === false || 
      settingData.new_order_taken === true) {
      const result = await AuthModel.findOne({
        code: settingData.authentication_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
      }
    }

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(settingData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = user_id
    const res = await VendorSettingModel.findOneAndUpdate(
      { vendor_id: new ObjectId(user_id) },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This vendor's settings doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    await AuthModel.deleteOne({
      user_id,
      code: settingData.authentication_code,
    })
    await this.commonService.addActivityLogs(
      user_id,
      "admin",
      AccessTypes.UPDATE_SETTING,
      `Vendor settings updated.`,
      res,
      editDoc
    )

    return Promise.resolve()
  }

  async getSetting(user_id: string) {
    const data = await VendorSettingModel.findOne({
      vendor_id: new ObjectId(user_id),
    })
    return Promise.resolve(data)
  }
}
