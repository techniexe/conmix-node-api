import * as Joi from "joi"
import { IRequestSchema } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface ISetting {
  with_TM?: boolean
  TM_price: number
  with_CP?: boolean
  CP_price: number
  is_customize_design_mix?: boolean
  [k: string]: any
}

export interface IAddSettingRequest extends IAuthenticatedRequest {
  body: ISetting
}

export const addSettingSchema: IRequestSchema = {
  body: Joi.object().keys({
    with_TM: Joi.boolean().default(false),
    TM_price: Joi.number(),
    with_CP: Joi.boolean().default(false),
    CP_price: Joi.number(),
    is_customize_design_mix: Joi.boolean().default(true),
  }),
}

export interface IEditSetting {
  with_TM?: boolean
  TM_price: number
  with_CP?: boolean
  CP_price: number
  is_customize_design_mix?: boolean
  new_order_taken?: boolean
  authentication_code: string
  [k: string]: any
}

export interface IEditSettingRequest extends IAuthenticatedRequest {
  body: IEditSetting
}

export const editSettingSchema: IRequestSchema = {
  body: Joi.object().keys({
    with_TM: Joi.boolean(),
    TM_price: Joi.number(),
    with_CP: Joi.boolean(),
    CP_price: Joi.number(),
    is_customize_design_mix: Joi.boolean(),
    new_order_taken: Joi.boolean(),
    authentication_code: Joi.string(),
  }),
}
