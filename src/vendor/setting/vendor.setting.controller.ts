import { IAuthenticatedRequest } from "./../../middleware/auth-token.middleware"
import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { VendorSettingRepository } from "./vendor.setting.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addSettingSchema,
  IAddSettingRequest,
  editSettingSchema,
  IEditSettingRequest,
} from "./vendor.setting.req.schema"

@injectable()
@controller("/setting", verifyCustomToken("vendor"))
export class VendorSettingController {
  constructor(
    @inject(VendorTypes.VendorSettingRepository)
    private settingRepo: VendorSettingRepository
  ) {}

  @httpPost("/", validate(addSettingSchema))
  async addSetting(req: IAddSettingRequest, res: Response, next: NextFunction) {
    const { uid } = req.user
    try {
      await this.settingRepo.addSetting(uid, req.body)
      res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/", validate(editSettingSchema))
  async editSetting(
    req: IEditSettingRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.settingRepo.editSetting(req.user.uid, req.body)
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getSetting(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.settingRepo.getSetting(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
