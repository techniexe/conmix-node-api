import { ObjectId } from "bson"
import { ProductModel } from "../../model/product.model"
import { injectable, inject } from "inversify"
import { IAddress, IEditAddress, IOTPForAddress } from "./vendor.address.request-schema"
import { IAddressDoc, AddressModel } from "../../model/address.model"
import { escapeRegExp, isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { CommonService } from "../../utilities/common.service"
import { VendorTypes } from "../vendor.types"
import { VendorUserModel } from "../../model/vendor.user.model"
import * as bcrypt from "bcryptjs"
import { VendorSettingRepository } from "../setting/vendor.setting.repository"
import { AuthModel } from "../../model/auth.model"
import { TestService } from "../../utilities/test.service"
import { MailGunService } from "../../utilities/mailgun.service"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { AdminUserModel } from "../../model/admin.user.model"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
const SALT_WORK_FACTOR = 10

@injectable()
export class AddressRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService,
    @inject(VendorTypes.VendorSettingRepository)
    private vendorSettingRepo: VendorSettingRepository,
    @inject(VendorTypes.TestService) private testService: TestService,
    @inject(VendorTypes.mailgunService) private mailgunService: MailGunService 
  ) {}
  async addAddress(
    user_id: string,
    addressData: IAddress
  ): Promise<IAddressDoc> {

    let vendorUserDoc = await VendorUserModel.findOne({
      _id: new ObjectId(user_id)
    })

    if(isNullOrUndefined(vendorUserDoc)){
      return Promise.reject(
        new InvalidInput(
          `No Vendor user Doc Found.`,
          400
        )
      )
    }
    if(!isNullOrUndefined(addressData.email_code)){
        const res = await AuthModel.findOne({
        code: addressData.email_code,
      })

      if (isNullOrUndefined(res)) {
        return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    }
}
  

if(!isNullOrUndefined(addressData.mobile_code)){
  const res = await AuthModel.findOne({
  code: addressData.mobile_code,
})

if (isNullOrUndefined(res)) {
  return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
}
}

let existingVendorData = await VendorUserModel.findOne({mobile_number: addressData.mobile_number})
if(!isNullOrUndefined(existingVendorData)){
  return Promise.reject(new InvalidInput(`Vendor with same mobile number already exists. `, 400))
}

let existingVenData = await VendorUserModel.findOne({email: addressData.email})
if(!isNullOrUndefined(existingVenData)){
  return Promise.reject(new InvalidInput(`Vendor with same email already exists. `, 400))
}

    const newVendorData:any =  {
      full_name: addressData.full_name,
      mobile_number: addressData.mobile_number,
      mobile_verified: vendorUserDoc.mobile_verified,
      email: addressData.email,
      email_verified: vendorUserDoc.email_verified,
      landline_number: vendorUserDoc.landline_number,
      company_type: vendorUserDoc.company_type,
      signup_type: vendorUserDoc.signup_type, //facebook/google/mobile
      password: await bcrypt.hash(addressData.password, SALT_WORK_FACTOR),
      pan_number: vendorUserDoc.pan_number,
      pancard_image_url: vendorUserDoc.pancard_image_url,
      gst_number: vendorUserDoc.gst_number,
      gst_certification_image_url: vendorUserDoc.gst_certification_image_url,
      verified_by_admin: false,
      verified_at: vendorUserDoc.verified_at,
      cst: vendorUserDoc.cst,
      annual_turnover: vendorUserDoc.annual_turnover,
      trade_capacity: vendorUserDoc.trade_capacity,
      number_of_employee: vendorUserDoc.number_of_employee,
      company_name: vendorUserDoc.company_name,
      company_certification_number: vendorUserDoc.company_certification_number,
      company_certification_image_url: vendorUserDoc.company_certification_image_url,
      hse_number: vendorUserDoc.hse_number,
      ehs_as_per_iso: vendorUserDoc.ehs_as_per_iso,
      ohsas_as_per_iso: vendorUserDoc.ohsas_as_per_iso,
      working_with_other_ecommerce: vendorUserDoc.working_with_other_ecommerce,
      language: vendorUserDoc.language,
      region_served: vendorUserDoc.region_served,
      created_at: new Date(),
      rating_sum: vendorUserDoc.rating_sum,
      rating_count: vendorUserDoc.rating_count,
      rating: vendorUserDoc.rating,
      notification_count: vendorUserDoc.notification_count,
      notification_opted: vendorUserDoc.notification_opted,
      unseen_message_count: vendorUserDoc.unseen_message_count,
      current_year: vendorUserDoc.current_year,
      no_of_plants: addressData.no_of_plants,
      plant_capacity_per_hour: addressData.plant_capacity_per_hour,
      no_of_operation_hour: addressData.no_of_operation_hour,
      new_order_taken: vendorUserDoc.new_order_taken,
      master_vendor_id: user_id
    }

    const newVendor = new VendorUserModel(newVendorData)
    const newVendorDoc = await newVendor.save()
    const IAddSetting = {
      with_TM: true,
      TM_price: 0,
      with_CP: true,
      CP_price: 0,
      is_customize_design_mix: true,
      master_vendor_id: user_id
    }
    await this.vendorSettingRepo.addSetting(newVendorDoc._id, IAddSetting)
    const newDoc = {
      line1: addressData.line1,
      line2: addressData.line2,
      state_id: addressData.state_id,
      city_id: addressData.city_id,
      sub_city_id: addressData.sub_city_id,
      pincode: addressData.pincode,
      address_type: addressData.address_type,
      user: {
        user_type: "vendor",
        user_id,
      },
      location: {
        type: "Point",
        coordinates: addressData.location.coordinates,
      },
      business_name: addressData.business_name,
      region_id: addressData.region_id,
      source_id: addressData.source_id,
      sub_vendor_id: newVendorDoc._id,
      billing_address_id: addressData.billing_address_id
    }

    const newAddress = new AddressModel(newDoc)
    const newAddressDoc = await newAddress.save()

    await AuthModel.deleteOne({
      user_id,
      code: addressData.mobile_code
    })

    await AuthModel.deleteOne({
      user_id,
      code: addressData.email_code,
    })

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_ADDRESS_BY_VENDOR,
      `Vendor added address of id ${newAddressDoc._id} .`
    )

    return Promise.resolve(newAddressDoc)
  }

  async getAddress(
    user_id: string,
    search?: string,
    before?: string,
    after?: string,
    city_id?: string
  ): Promise<IAddressDoc[]> {
    let query: { [k: string]: any } = {}
    query.$or = [
      {
        "user.user_id": new ObjectId(user_id),
        "user.user_type": "vendor",
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
    ]

    // let query: { [k: string]: any } = {
    //   "user.user_id": new ObjectId(user_id),
    //   "user.user_type": "vendor",
    // }

    
    if (!isNullOrUndefined(search) && search !== "") {
      const $options = "i"
      const $regex = escapeRegExp(search)
      query.$or = [
        // {
        //   country_code: { $regex, $options },
        // },
        {
          business_name: { $regex, $options },
        },
      ]
    }
    if (!isNullOrUndefined(search) && search !== "") {
      query.$text = { $search: search }
    }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    if (!isNullOrUndefined(city_id) && city_id !== "") {
      query.city_id = new ObjectId(city_id)
    }

    console.log(query)
    return AddressModel.aggregate([
      {
        $match: query,
      },
      {
        $sort: { created_at: -1 },
      },
      {
        $limit: 10,
      },
      {
        $lookup: {
          from: "source",
          localField: "source_id",
          foreignField: "_id",
          as: "source",
        },
      },
      {
        $unwind: { path: "$source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "region_id",
          foreignField: "_id",
          as: "state",
        },
      },
      {
        $unwind: { path: "$state", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "state_id",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $unwind: { path: "$stateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "city_id",
          foreignField: "_id",
          as: "cityDetails",
        },
      },
      {
        $unwind: { path: "$cityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "billing_address",
          localField: "billing_address_id",
          foreignField: "_id",
          as: "billingAddressDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "state",
          localField: "billingAddressDetails.state_id",
          foreignField: "_id",
          as: "billingAddressStateDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressStateDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "city",
          localField: "billingAddressDetails.city_id",
          foreignField: "_id",
          as: "billingAddressCityDetails",
        },
      },
      {
        $unwind: { path: "$billingAddressCityDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $project: {
          _id: 1,
          location: 1,
          line1: 1,
          line2: 1,
          state_id: 1,
          city_id: 1,
          pincode: 1,
          address_type: 1,
          "user.user_type": 1,
          "user.user_id": 1,
          business_name: 1,
          region_id: 1,
          region_name: "$state.state_name",
          source_id: 1,
          source_name: "$source.source_name",
          created_at: 1,
          billing_address_id: 1,
          is_verified: 1,
          is_blocked: 1,
          is_accepted: 1,
          "stateDetails._id": "$stateDetails._id",
          "stateDetails.state_name": "$stateDetails.state_name",
          "stateDetails.country_id": "$stateDetails.country_id",
          "stateDetails.country_code": "$stateDetails.country_code",
          "stateDetails.country_name": "$stateDetails.country_name",
          "stateDetails.created_at": "$stateDetails.created_at",
          "cityDetails._id": "$cityDetails._id",
          "cityDetails.city_name": "$cityDetails.city_name",
          "cityDetails.location": "$cityDetails.location",
          "cityDetails.state_id": "$cityDetails.state_id",
          "cityDetails.created_at": "$cityDetails.created_at",
          "cityDetails.country_code": "$cityDetails.country_code",
          "cityDetails.country_id": "$cityDetails.country_id",
          "cityDetails.country_name": "$cityDetails.country_name",
          "cityDetails.state_name": "$cityDetails.state_name",
          "cityDetails.popularity": "$cityDetails.popularity",
          "vendorDetails._id": 1,
          "vendorDetails.full_name" : 1,
          "vendorDetails.mobile_number" : 1,
          "vendorDetails.email" : 1,
          "vendorDetails.password" : 1,
          "vendorDetails.no_of_plants":1,
          "vendorDetails.plant_capacity_per_hour":1,
          "vendorDetails.no_of_operation_hour":1,
          "billingAddressDetails._id": 1,
          "billingAddressDetails.company_name": 1,
          "billingAddressDetails.line1": 1,
          "billingAddressDetails.line2": 1,
          "billingAddressDetails.pincode": 1,
          "billingAddressDetails.gst_number": 1,
          "billingAddressDetails.gst_image_url": 1,
          "billingAddressDetails.created_at": 1,
          "billingAddressStateDetails._id": "$stateDetails._id",
          "billingAddressStateDetails.state_name": "$stateDetails.state_name",
          "billingAddressStateDetails.country_id": "$stateDetails.country_id",
          "billingAddressStateDetails.country_code": "$stateDetails.country_code",
          "billingAddressStateDetails.country_name": "$stateDetails.country_name",
          "billingAddressStateDetails.created_at": "$stateDetails.created_at",
          "billingAddressCityDetails._id": "$cityDetails._id",
          "billingAddressCityDetails.city_name": "$cityDetails.city_name",
          "billingAddressCityDetails.location": "$cityDetails.location",
          "billingAddressCityDetails.state_id": "$cityDetails.state_id",
          "billingAddressCityDetails.created_at": "$cityDetails.created_at",
          "billingAddressCityDetails.country_code": "$cityDetails.country_code",
          "billingAddressCityDetails.country_id": "$cityDetails.country_id",
          "billingAddressCityDetails.country_name": "$cityDetails.country_name",
          "billingAddressCityDetails.state_name": "$cityDetails.state_name",
          "billingAddressCityDetails.popularity": "$cityDetails.popularity",
        },
      },
    ])

    // return AddressModel.find(query).limit(40)
  }

  async editAddress(
    user_id: string,
    addressId: string,
    addressData: IEditAddress
  ): Promise<any> {

    if(!isNullOrUndefined(addressData.email_code)){
      const res = await AuthModel.findOne({
      code: addressData.email_code,
    })

    if (isNullOrUndefined(res)) {
      return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
  }
}


if(!isNullOrUndefined(addressData.mobile_code)){
const res = await AuthModel.findOne({
code: addressData.mobile_code,
})

if (isNullOrUndefined(res)) {
return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
}
}

    const fields = [
      "line1",
      "line2",
      "state_id",
      "city_id",
      "sub_city_id",
      "pincode",
      "address_type",
      "location",
      "business_name",
      "region_id",
      "source_id",
      "billing_address_id"
    ]
    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(addressData)) {
      if (fields.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }

    console.log("addressDataaaaaaaaaaaaaaaaaaaaaaaaa", addressData)

  if(!isNullOrUndefined(addressData.full_name) ||
  !isNullOrUndefined(addressData.mobile_number) ||
  !isNullOrUndefined(addressData.password) ||
  !isNullOrUndefined(addressData.email)){

    const addressDoc = await AddressModel.findOne(
      {
        _id: addressId,
        "user.user_id": user_id,
        "user.user_type": "vendor",
      }
    )
    if (addressDoc === null) {
      const err = new UnexpectedInput(`This address doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    const Newfields = [
      "full_name",
      "mobile_number",
      "password",
      "email",
      "no_of_plants",
      "plant_capacity_per_hour",
      "no_of_operation_hour"
    ]

    const editVendorDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(addressData)) {
      if (Newfields.indexOf(key) > -1) {
        editVendorDoc[key] = val
      }
    }

    if (!isNullOrUndefined(addressData.password)) {
      editVendorDoc.password = await bcrypt.hash(addressData.password, SALT_WORK_FACTOR)
    }

   const vendorDoc = await VendorUserModel.findOneAndUpdate(
      {
        _id: addressDoc.sub_vendor_id,
      },
      { $set: editVendorDoc }
    )
    if (vendorDoc === null) {
      const err = new UnexpectedInput(`This vendor doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
  }

    if (!isNullOrUndefined(addressData.location)) {
      await ProductModel.updateMany(
        {
          pickup_address_id: new ObjectId(addressId),
          user_id: new ObjectId(user_id),
        },
        {
          $set: {
            pickup_location: addressData.location,
          },
        }
      )
    }

    // if(!isNullOrUndefined(addressData.no_of_plants) || 
    // !isNullOrUndefined(addressData.plant_capacity_per_hour) || 
    // !isNullOrUndefined(addressData.no_of_operation_hour))
    // {
    //   await VendorUserModel.findOneAndUpdate(
    //     {
    //       pickup_address_id: new ObjectId(addressId),
    //       user_id: new ObjectId(user_id),
    //     },
    //     {
    //       $set: {
    //         pickup_location: addressData.location,
    //       },
    //     }
    //   )
    // }

    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }
    const res = await AddressModel.findOneAndUpdate(
      {
        _id: addressId,
        "user.user_id": user_id,
        "user.user_type": "vendor",
      },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This address doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await AuthModel.deleteOne({
      user_id,
      code: addressData.mobile_code
    })

    await AuthModel.deleteOne({
      user_id,
      code: addressData.email_code,
    })
    
    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.UPDATE_ADDRESS_BY_VENDOR,
      `Vendor updated details Address of id ${addressId} .`,
      res,
      editDoc
    )

    return Promise.resolve()
  }

  async requestOTPForAddress(user_id: string, plantData: IOTPForAddress){
      // let mobile_code: any
      // let email_code: any

      let vendorUserDoc: any
      if (!isNullOrUndefined(user_id)) {
       vendorUserDoc = await VendorUserModel.findOne({
        _id: new ObjectId(user_id)
      })
    }
      if(isNullOrUndefined(vendorUserDoc)){
        return Promise.reject(new InvalidInput(`No vendor doc found`, 400))
      }
      let mobileauthData: any
    if(!isNullOrUndefined(plantData.mobile_number)){

      const numberCheck = await VendorUserModel.findOne({
        mobile_number: plantData.mobile_number,
      })
      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered.`, 400)
        )
      }

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === true
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered.`, 400)
        )
      }

      const clientNumberCheck = await BuyerUserModel.findOne({
        mobile_number: plantData.mobile_number,
      })
      if (
        !isNullOrUndefined(clientNumberCheck)
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered with Client.`, 400)
        )
      }

      const adminNumberCheck = await AdminUserModel.findOne({
        mobile_number: plantData.mobile_number,
      })
      if (
        !isNullOrUndefined(adminNumberCheck)
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered with Admin.`, 400)
        )
      }


       const mobile_code = generateRandomNumber()
       mobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: plantData.mobile_number,
          user_id: user_id,
        },
        {
          $set: { code :mobile_code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (mobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }
      let message = `"${mobile_code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Registration OTP.
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(plantData.mobile_number, message, templateId)
    }

    let emailauthData: any
   if(!isNullOrUndefined(plantData.email)){
    console.log("here")
    const numberCheck = await VendorUserModel.findOne({
      email: plantData.email,
    })
    console.log("numberCheck", numberCheck)
    console.log("user_id", user_id)
    if (
      !isNullOrUndefined(numberCheck) &&
      numberCheck._id.equals(user_id) === false
    ) {
      return Promise.reject(
        new InvalidInput(`This email is already registered.`, 400)
      )
    }

    if (
      !isNullOrUndefined(numberCheck) &&
      numberCheck._id.equals(user_id) === true
    ) {

      return Promise.reject(
        new InvalidInput(`This email is already registered.`, 400)
      )
    }


    const clientEmailCheck = await BuyerUserModel.findOne({
      email: plantData.email,
    })
    if (
      !isNullOrUndefined(clientEmailCheck)
    ) {
      return Promise.reject(
        new InvalidInput(`This email is already registered with Client.`, 400)
      )
    }

    const adminEmailCheck = await AdminUserModel.findOne({
       email: plantData.email,
    })
    if (
      !isNullOrUndefined(adminEmailCheck)
    ) {
      return Promise.reject(
        new InvalidInput(`This email is already registered with Admin.`, 400)
      )
    }

    const email_code: any = generateRandomNumber()
    emailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: plantData.email,
          user_id: user_id,
        },
        {
          $set: { code :email_code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (emailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let one_time_pwd_html = vendor_mail_template.one_time_pwd
      one_time_pwd_html = one_time_pwd_html.replace("{{code}}", email_code)
       var data = {
        from: "no-reply@conmate.in",
        to: plantData.email,
        subject: "Conmix - One Time Password",
        html: one_time_pwd_html,
      }
      await this.mailgunService.sendEmail(data)
      // var data = {
      //   from: "no-reply@conmate.in",
      //   to: plantData.email,
      //   subject: "Conmix - One Time Password",
      //   html: `Dear ${vendorUserDoc.company_name},<br>
      //   "${email_code}" is your OTP at Conmix. It is valid for 60 seconds. Kindly do not share the OTP with others.
      //   <br><br> <br>  
        
      //   Regards,<br>
      //   Conmate E-Commerce Pvt. Ltd.`,
      // }
      // await this.mailgunService.sendEmail(data)
    
    }
  
    return Promise.resolve({
      mobileauthData,
      emailauthData,
    })
  }
  
}
