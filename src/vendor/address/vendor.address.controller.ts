import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { AddressRepository } from "./vendor.address.repository"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import {
  addressSchema,
  IAddressRequest,
  IEditAddressRequest,
  editAddressSchema,
  requestOTPForAddressSchema,
  IOTPForAddressRequest,
} from "./vendor.address.request-schema"
//import { InvalidInput } from "../../utilities/customError"

@injectable()
@controller("/address", verifyCustomToken("vendor"))
export class AddressController {
  constructor(
    @inject(VendorTypes.AddressRepository)
    private addressRepo: AddressRepository
  ) {}
  @httpPost("/", validate(addressSchema))
  async addAddress(req: IAddressRequest, res: Response, next: NextFunction) {
    try {
      const { _id } = await this.addressRepo.addAddress(req.user.uid, req.body)
      res.json({ data: { _id } })
    } catch (err) {
      // if (err.code === 11000) {
      //   return next(new InvalidInput(`Duplicate address not allowed`, 400))
      // }
      next(err)
    }
  }

  @httpGet("/")
  async getAddress(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { uid } = req.user
      const { search, before, after, city_id } = req.query
      const data = await this.addressRepo.getAddress(uid, search, before, after, city_id)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:addressId", validate(editAddressSchema))
  async editAddress(
    req: IEditAddressRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.addressRepo.editAddress(
        req.user.uid,
        req.params.addressId,
        req.body
      )
      res.sendStatus(202)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/requestOTPForAddress", validate(requestOTPForAddressSchema))
  async requestOTPForAddress(
    req: IOTPForAddressRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.addressRepo.requestOTPForAddress(req.user.uid, req.body)

      console.log("data", data)
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

}
