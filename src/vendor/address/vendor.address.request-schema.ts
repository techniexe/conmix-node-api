import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { AddressType } from "../../model/address.model"

export interface IAddress {
  line1: string
  line2?: string
  state_id: string
  city_id: string
  sub_city_id: string
  pincode: number
  address_type: AddressType
  business_name: string
  location: {
    type?: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
  region_id: string
  source_id: string
  full_name: string
  mobile_number: string
  email: string
  password: string
  billing_address_id: string
  no_of_plants: number
  plant_capacity_per_hour: number,
  no_of_operation_hour: number,
  mobile_code: string,
  email_code: string
}

export interface IEditAddress {
  line1?: string
  line2?: string
  state_id?: string
  city_id?: string
  sub_city_id?: string
  pincode?: number
  address_type?: string
  business_name?: string
  location?: {
    type: string //not required, we will override it
    coordinates: [number] //[lng,lat]
  }
  full_name: string
  mobile_number: string
  email: string
  password: string
  billing_address_id: string
  no_of_plants: number
  plant_capacity_per_hour: number,
  no_of_operation_hour: number,
  mobile_code: string,
  email_code: string
}

export interface IAddressRequest extends IAuthenticatedRequest {
  body: IAddress
}

export const addressSchema: IRequestSchema = {
  body: Joi.object().keys({
    line1: Joi.string().required(),
    line2: Joi.string(),
    state_id: Joi.string().required(),
    city_id: Joi.string().required(),
    sub_city_id: Joi.string(),
    pincode: Joi.number().min(6).required(),
    address_type: Joi.string().valid(Object.values(AddressType)).required(),
    business_name: Joi.string().required(),
    location: Joi.object()
      .keys({
        type: Joi.string().default("Point"),
        coordinates: Joi.array()
          .ordered([
            Joi.number().min(-180).max(180).required(),
            Joi.number().min(-90).max(90).required(),
          ])
          .length(2)
          .required(),
      })
      .required(),
    region_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    full_name: Joi.string().required(),
    mobile_number: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    billing_address_id: Joi.string().regex(objectIdRegex).required(),
    no_of_plants: Joi.number().required(),
    plant_capacity_per_hour: Joi.number().required(),
    no_of_operation_hour: Joi.number().required(),  
    mobile_code: Joi.string(),
    email_code: Joi.string(),
  }),
}

export const editAddressSchema: IRequestSchema = {
  params: Joi.object().keys({
    addressId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    line1: Joi.string(),
    line2: Joi.string(),
    state_id: Joi.string().regex(objectIdRegex),
    city_id: Joi.string().regex(objectIdRegex),
    sub_city_id: Joi.string().regex(objectIdRegex),
    pincode: Joi.number().min(6),
    address_type: Joi.string().valid(Object.values(AddressType)),
    business_name: Joi.string(),
    location: Joi.object().keys({
      type: Joi.string().default("Point"),
      coordinates: Joi.array()
        .ordered([
          Joi.number().min(-180).max(180).required(),
          Joi.number().min(-90).max(90).required(),
        ])
        .length(2)
        .required(),
    }),
    region_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    full_name: Joi.string(),
    mobile_number: Joi.string(),
    email: Joi.string(),
    password: Joi.string(),
    billing_address_id: Joi.string().regex(objectIdRegex),
    no_of_plants: Joi.number(),
    plant_capacity_per_hour: Joi.number(),
    no_of_operation_hour: Joi.number(),
    mobile_code: Joi.string(),
    email_code: Joi.string(),
  }),
}

export interface IEditAddressRequest extends IAuthenticatedRequest {
  body: IEditAddress
}

export interface IOTPForAddress {
  mobile_number: string
  email: string

}

export const requestOTPForAddressSchema: IRequestSchema = {
  body: Joi.object().keys({
    mobile_number: Joi.string(),
    email: Joi.string(),
  }),
}

export interface IOTPForAddressRequest extends IAuthenticatedRequest {
  body: IOTPForAddress
}