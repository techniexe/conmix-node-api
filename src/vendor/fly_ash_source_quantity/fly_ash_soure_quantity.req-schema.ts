import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IFlyAshSourceQuantity {
  address_id: string
  source_id: string
  quantity: string
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddFlyAshSourceQuantityRequest extends IAuthenticatedRequest {
  body: IFlyAshSourceQuantity
}

export const addFlyAshSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex).required(),
    quantity: Joi.number().min(1).required(),
  }),
}

export interface IEditFlyAshSourceQuantity {
  address_id: string
  source_id: string
  quantity: string
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditFlyAshSourceQuantityRequest extends IAuthenticatedRequest {
  params: {
    FlyAshSourceQuantityId: string
  }
  body: IEditFlyAshSourceQuantity
}

export const editFlyAshSourceQuantitySchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number().min(1),
    authentication_code: Joi.string().required(),
  }),

  params: Joi.object().keys({
    FlyAshSourceQuantityId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveFlyAshSourceQuantityRequest extends IAuthenticatedRequest {
  params: {
    FlyAshSourceQuantityId: string
  }
}

export const removeFlyAshSourceQuantitySchema: IRequestSchema = {
  params: Joi.object().keys({
    FlyAshSourceQuantityId: Joi.string().regex(objectIdRegex),
  }),
  body: Joi.object().keys({
    authentication_code: Joi.string().required(),
  }),
}

export interface IGetFlyAshSourceQuantityList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  quantity?: any
  quantity_value?: string
}

export interface IGetFlyAshSourceQuantityRequest extends IAuthenticatedRequest {
  query: IGetFlyAshSourceQuantityList
}

export const getFlyAshSourceQuantitySchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    quantity: Joi.number(),
    quantity_value: Joi.string(),
  }),
}
