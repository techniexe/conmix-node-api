import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { FlyAshSourceQuantityRepository } from "./fly_ash_soure_quantity.repository"
import {
  addFlyAshSourceQuantitySchema,
  IAddFlyAshSourceQuantityRequest,
  editFlyAshSourceQuantitySchema,
  IEditFlyAshSourceQuantityRequest,
  removeFlyAshSourceQuantitySchema,
  IRemoveFlyAshSourceQuantityRequest,
  getFlyAshSourceQuantitySchema,
  IGetFlyAshSourceQuantityRequest,
} from "./fly_ash_soure_quantity.req-schema"

@injectable()
@controller("/fly_ash_source_quantity", verifyCustomToken("vendor"))
export class FlyAshSourceQuantityController {
  constructor(
    @inject(VendorTypes.FlyAshSourceQuantityRepository)
    private flyAshSourceRateRepo: FlyAshSourceQuantityRepository
  ) {}

  @httpPost("/", validate(addFlyAshSourceQuantitySchema))
  async addFlyAshSourceQuantity(
    req: IAddFlyAshSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.flyAshSourceRateRepo.addFlyAshSourceQuantity(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch(
    "/:FlyAshSourceQuantityId",
    validate(editFlyAshSourceQuantitySchema)
  )
  async editFlyAshSourceQuantity(
    req: IEditFlyAshSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.flyAshSourceRateRepo.editFlyAshSourceQuantity(
      req.user.uid,
      req.params.FlyAshSourceQuantityId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete(
    "/:FlyAshSourceQuantityId",
    validate(removeFlyAshSourceQuantitySchema)
  )
  async removeFlyAshSourceQuantity(
    req: IRemoveFlyAshSourceQuantityRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.flyAshSourceRateRepo.removeFlyAshSourceQuantity(
        req.params.FlyAshSourceQuantityId,
        req.body.authentication_code
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getFlyAshSourceQuantitySchema))
  async List(req: IGetFlyAshSourceQuantityRequest, res: Response) {
    const data = await this.flyAshSourceRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
