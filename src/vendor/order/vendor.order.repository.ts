
import { ObjectId } from "bson"
import {
  OrderItemStatus,
  awsConfig,
  BillStatus,
  UserType,
  monthsCode,
} from "../../utilities/config"
import { injectable, inject } from "inversify"
import { isNullOrUndefined } from "../../utilities/type-guards"
import {
  VendorOrderModel,
  SupplierOrderStatus,
} from "../../model/vendor.order.model"
import { InvalidInput } from "../../utilities/customError"
import { OrderItemModel } from "../../model/order_item.model"
import { OrderItemPartModel } from "../../model/order_item_part.model"
// import { CPOrderTrackModel } from "../../model/CP_order_track.model"
import { UploadedFile } from "express-fileupload"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { OrderTrackModel } from "../../model/order_track.model"
import { TMModel, TMCategoryModel } from "../../model/TM.model"
import { DriverInfoModel } from "../../model/driver.model"
import { VendorTypes } from "../vendor.types"
import { NotificationUtility } from "../../utilities/notification.utility"
import {
  ClientNotificationType,
  AdminNotificationType,
} from "../../model/notification.model"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { AuthModel } from "../../model/auth.model"
import { TestService } from "../../utilities/test.service"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { OrderModel } from "../../model/order.model"
import { SiteModel } from "../../model/site.model"
import { VendorUserModel } from "../../model/vendor.user.model"
import { client_mail_template } from "../../utilities/client_email_tempalte"
import { MailGunService } from "../../utilities/mailgun.service"
import { admin_mail_template } from "../../utilities/admin_email_template"
import { vendor_mail_template } from "../../utilities/vendor_email_template"

@injectable()
export class OrderRepository {
  constructor(
    @inject(VendorTypes.NotificationUtility)
    private notUtil: NotificationUtility,
    @inject(VendorTypes.TestService) private testService: TestService,
    @inject(VendorTypes.mailgunService) private mailgunService: MailGunService
    ) {}
  async getVendorOrder(
    user_id?: string,
    before?: string,
    after?: string,
    payment_status?: string,
    design_mix_id?: string,
    order_status?: string,
    buyer_id?: string,
    address_id?: string,
    order_id?: string,
    month?: string,
    year?: number,
    date?: Date,
    buyer_order_id?: string,
    buyer_name?: string
  ) {
    const query: { [k: string]: any } = {}
    if (!isNullOrUndefined(user_id)) {
      // query.user_id = new ObjectId(user_id)
    }

    query.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: new Date(before) }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: new Date(after) }
      sort.created_at = 1
    }
    if (!isNullOrUndefined(payment_status)) {
      query.payment_status = payment_status
    }
    if (!isNullOrUndefined(design_mix_id)) {
      query.design_mix_id = design_mix_id
    }

    if (!isNullOrUndefined(order_status)) {
      query.order_status = order_status
    }

    if (!isNullOrUndefined(buyer_id)) {
      let buyerUserData = await BuyerUserModel.findOne({
        user_id: buyer_id
      })
      if(!isNullOrUndefined(buyerUserData)){
        query.buyer_id = buyerUserData._id
      }
    }

    if (!isNullOrUndefined(address_id)) {
      query.address_id = new ObjectId(address_id)
    }

    if (!isNullOrUndefined(order_id)) {
      query._id = order_id
    }

  
    if (!isNullOrUndefined(buyer_order_id)) {
      query.buyer_order_display_id = buyer_order_id
    }

 
    if (!isNullOrUndefined(buyer_name)) {
        let buyerData = await BuyerUserModel.findOne({
          full_name: buyer_name,
        })
        if (!isNullOrUndefined(buyerData)) {
          query.buyer_id = new ObjectId(buyerData._id)
        } else {
          query.buyer_id = ""
        }
      }
    

    if (!isNullOrUndefined(year)) {
      query.created_at = {
        $gte: new Date(year, 0, 1),
        $lte: new Date(year, 11, 31),
      }
    }

    if (!isNullOrUndefined(month) && !isNullOrUndefined(year)) {
      // If month or year passed, then find first and last day of given month and year then check.
      let endDate = new Date(year, monthsCode.indexOf(month) + 1, 0, 0, 0, 0)
      query.created_at = {
        $gte: new Date(year, monthsCode.indexOf(month), 0, 0, 0, 0),
        $lte: new Date(endDate.getTime() - 1),
      }
    }
    if(!isNullOrUndefined(date)){

      var d1 = new Date(date);
      console.log("d1", d1)
     let next_day =  new Date(d1.setDate(d1.getDate() + 1));
     console.log("date", date)
     console.log("next_day", next_day)
     query.created_at = {
      $gte: date,
      $lte: next_day,
    }
    }

    
    console.log("query", query)
    const aggregateArr = [
      { $match: query },
      { $sort: sort },
      { $limit: 10 },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: true,
          user_id: true,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          address_id: true,
          line1: true,
          line2: true,
          state_id: true,
          city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          max_accepted_at: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          "buyer._id": true,
          "buyer.full_name": true,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)
    if (sort.created_at === 1) {
      vendorOrderData.reverse()
    }
    return Promise.resolve(vendorOrderData)
  }

  async getOrderById(orderId: string, user_id: string) {
    console.log("user_id", user_id)
    const orderQuery: { [k: string]: any } = {}
    orderQuery._id = orderId
    // orderQuery.user_id = new ObjectId(user_id)

    orderQuery.$or = [
      {
        user_id: new ObjectId(user_id),
      },
      {
        sub_vendor_id: new ObjectId(user_id),
      },
      {
        master_vendor_id: new ObjectId(user_id),
      },
    ]

    const aggregateArr = [
      { $match: orderQuery },
      { $sort: { created_at: -1 } },

      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "concrete_grade",
          localField: "concrete_grade_id",
          foreignField: "_id",
          as: "concrete_grade",
        },
      },
      {
        $unwind: {
          path: "$concrete_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_brand",
          localField: "admix_brand_id",
          foreignField: "_id",
          as: "admix_brand",
        },
      },
      {
        $unwind: {
          path: "$admix_brand",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "admixture_category",
          localField: "admix_cat_id",
          foreignField: "_id",
          as: "admix_cat",
        },
      },
      {
        $unwind: { path: "$admix_cat", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },

      {
        $lookup: {
          from: "fly_ash_source",
          localField: "fly_ash_source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: { path: "$fly_ash_source", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate2_sub_cat_id",
          foreignField: "_id",
          as: "aggregate2_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate2_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_source",
          localField: "agg_source_id",
          foreignField: "_id",
          as: "aggregate_source",
        },
      },
      {
        $unwind: {
          path: "$aggregate_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "aggregate_sand_sub_category",
          localField: "aggregate1_sub_cat_id",
          foreignField: "_id",
          as: "aggregate1_sub_cat",
        },
      },
      {
        $unwind: {
          path: "$aggregate1_sub_cat",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "sand_source",
          localField: "sand_source_id",
          foreignField: "_id",
          as: "sand_source",
        },
      },
      {
        $unwind: {
          path: "$sand_source",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_brand",
          localField: "cement_brand_id",
          foreignField: "_id",
          as: "cement_brand",
        },
      },
      {
        $unwind: {
          path: "$cement_brand",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "cement_grade",
          localField: "cement_grade_id",
          foreignField: "_id",
          as: "cement_grade",
        },
      },
      {
        $unwind: {
          path: "$cement_grade",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "buyer",
          localField: "buyer_id",
          foreignField: "_id",
          as: "buyer",
        },
      },
      {
        $unwind: {
          path: "$buyer",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "pickup_address",
        },
      },
      {
        $unwind: { path: "$pickup_address" },
      },
      {
        $lookup: {
          from: "order",
          localField: "buyer_order_display_id",
          foreignField: "display_id",
          as: "order",
        },
      },
      {
        $unwind: {
          path: "$order",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "order_item",
          localField: "buyer_order_item_id",
          foreignField: "_id",
          as: "orderItem",
        },
      },
      {
        $unwind: {
          path: "$orderItem",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "state",
          localField: "order.state_id",
          foreignField: "_id",
          as: "stateData",
        },
      },
      {
        $unwind: {
          path: "$stateData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "city",
          localField: "order.city_id",
          foreignField: "_id",
          as: "cityData",
        },
      },
      {
        $unwind: {
          path: "$cityData",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor_media",
          localField: "user_id",
          foreignField: "vendor_id",
          as: "vendor_media",
        },
      },
      {
        $lookup: {
          from: "design_mix_variant",
          localField: "design_mix_id",
          foreignField: "_id",
          as: "design_mix",
        },
      },
      {
        $unwind: {
          path: "$design_mix",
          preserveNullAndEmptyArrays: true,
        },
      },

      // {
      //   $lookup: {
      //     from: "order_item_part",
      //     localField: "buyer_order_id",
      //     foreignField: "buyer_order_id",
      //     as: "orderItemPartData",
      //   },
      // },
      // {
      //   $unwind: {
      //     path: "$order_item_part",
      //     preserveNullAndEmptyArrays: true,
      //   },
      // },
      // {
      //   $lookup: {
      //     from: "order_item",
      //     localField: "buyer_order_item_id",
      //     foreignField: "_id",
      //     as: "orderItem",
      //   },
      // },
      {
        $project: {
          _id: true,
          "concrete_grade._id": 1,
          "concrete_grade.name": 1,
          "admix_brand._id": 1,
          "admix_brand.name": 1,
          "admix_cat._id": 1,
          "admix_cat.category_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          "aggregate2_sub_cat._id": 1,
          "aggregate2_sub_cat.sub_category_name": 1,
          "aggregate_source._id": 1,
          "aggregate_source.aggregate_source_name": 1,
          "aggregate1_sub_cat._id": 1,
          "aggregate1_sub_cat.sub_category_name": 1,
          "sand_source._id": 1,
          "sand_source.sand_source_name": 1,
          "cement_brand._id": 1,
          "cement_brand.name": 1,
          "cement_grade._id": 1,
          "cement_grade.name": 1,
          user_id: true,
          "design_mix._id": 1,
          "design_mix.product_name": 1,
          design_mix_id: true,
          buyer_id: true,
          buyer_order_id: true,
          buyer_order_display_id: true,
          buyer_order_item_id: true,
          buyer_order_display_item_id: true,
          quantity: true,
          revised_quantity: true,
          vehicle_id: true,
          logistics_user_id: true,
          vehicle_category_id: true,
          vehicle_sub_category_id: true,
          concrete_grade_id: true,
          admix_brand_id: true,
          admix_cat_id: true,
          fly_ash_source_id: true,
          aggregate2_sub_cat_id: true,
          agg_source_id: true,
          aggregate1_sub_cat_id: true,
          sand_source_id: true,
          cement_brand_id: true,
          cement_grade_id: true,
          cement_quantity: true,
          sand_quantity: true,
          aggregate1_quantity: true,
          aggregate2_quantity: true,
          fly_ash_quantity: true,
          admix_quantity: true,
          water_quantity: true,
          cement_per_kg_rate: true,
          sand_per_kg_rate: true,
          aggregate1_per_kg_rate: true,
          aggregate2_per_kg_rate: true,
          fly_ash_per_kg_rate: true,
          admix_per_kg_rate: true,
          water_per_ltr_rate: true,
          cement_price: true,
          sand_price: true,
          aggreagte1_price: true,
          aggreagte2_price: true,
          fly_ash_price: true,
          admix_price: true,
          water_price: true,
          with_TM: true,
          with_CP: true,
          distance: true,
          igst_rate: true,
          cgst_rate: true,
          sgst_rate: true,
          gst_type: true, // IGST/SGST,CGST
          cgst_price: true,
          sgst_price: true,
          gst_price: true,
          item_status: true,
          selling_price: true,
          margin_price: true,
          TM_price: true,
          CP_price: true,
          order_status: true,
          payment_status: true,
          // address_id: true,
          // line1: true,
          // line2: true,
          // state_id: true,
          // city_id: true,
          pincode: true,
          location: true,
          address_type: true, //warehouse/factory/quarry/kapchi/home/office/
          business_name: true,
          city_name: true,
          state_name: true,
          unit_price: true,
          base_amount: true,
          margin_rate: true,
          margin_amount: true,
          total_amount: true,
          total_amount_without_margin: true,
          created_at: true,
          bills: true,
          creditNotes: true,
          debitNotes: true,
          cancelled_at: true,
          dispatched_at: true,
          pickedup_at: true,
          delivered_at: true,
          vendor_media: true,
          max_accepted_at: true,
          total_CP_price: true,
          "buyer._id": true,
          "buyer.full_name": true,
          "pickup_address._id": true,
          "pickup_address.state_name": "$state_name",
          "pickup_address.city_name": "$city_name",
          "pickup_address.line1": true,
          "pickup_address.line2": true,
          "pickup_address.pincode": true,
          "pickup_address.location": true,
          "delivery_address.site_id": "$order.site_id",
          "delivery_address.site_name": "$order.site_name",
          "delivery_address.address_line1": "$order.address_line1",
          "delivery_address.address_line2": "$order.address_line2",
          "delivery_address.state_id": "$order.state_id",
          "delivery_address.city_id": "$order.city_id",
          "delivery_address.pincode": "$order.pincode",
          "delivery_address.location": "$order.delivery_location",
          "delivery_address.created_at": "$order.created_at",
          "delivery_address.state_name": "$stateData.state_name",
          "delivery_address.city_name": "$cityData.city_name",
          orderItem: true,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
          // orderItemPartData: true,
        },
      },
    ]

    const vendorOrderData = await VendorOrderModel.aggregate(aggregateArr)

    console.log("vendorOrderData", vendorOrderData)
    if (vendorOrderData.length <= 0) {
      return Promise.reject(new InvalidInput(`No Supplier Order found`, 400))
    }

    const orderTrackData = await OrderTrackModel.find({
      vendor_order_id: orderId,
    })
    vendorOrderData[0].orderTrackData = orderTrackData
    for (let i = 0; i < vendorOrderData[0].orderTrackData.length; i++) {
      const TMData = await TMModel.findById(
        vendorOrderData[0].orderTrackData[i].TM_id
      )

      if (!isNullOrUndefined(TMData)) {
        vendorOrderData[0].orderTrackData[i].TM_rc_number = TMData.TM_rc_number

        const TMCategoryData = await TMCategoryModel.findById(
          TMData.TM_category_id
        )

        if (!isNullOrUndefined(TMCategoryData)) {
          console.log("TMCategoryData", TMCategoryData)
          vendorOrderData[0].orderTrackData[i].category_name =
            TMCategoryData.category_name
        }
        const driverInfo = await DriverInfoModel.findById(TMData.driver1_id)
        if (!isNullOrUndefined(driverInfo)) {
          vendorOrderData[0].orderTrackData[i].driver_name =
            driverInfo.driver_name
          vendorOrderData[0].orderTrackData[i].driver_mobile_number =
            driverInfo.driver_mobile_number
        }
      }
      vendorOrderData[0].orderTrackData[i].TM_rc_number =
        orderTrackData[i].TM_rc_number
      vendorOrderData[0].orderTrackData[i].TM_category_name =
        orderTrackData[i].TM_category_name
      vendorOrderData[0].orderTrackData[i].TM_sub_category_name =
        orderTrackData[i].TM_sub_category_name
      vendorOrderData[0].orderTrackData[i].TM_driver1_name =
        orderTrackData[i].TM_driver1_name
      vendorOrderData[0].orderTrackData[i].TM_driver2_name =
        orderTrackData[i].TM_driver2_name
      vendorOrderData[0].orderTrackData[i].TM_driver1_mobile_number =
        orderTrackData[i].TM_driver1_mobile_number
      vendorOrderData[0].orderTrackData[i].TM_driver2_mobile_number =
        orderTrackData[i].TM_driver2_mobile_number
    }

    const orderItemPartQuery: { [k: string]: any } = {}
    // orderItemPartQuery.buyer_order_id = vendorOrderData[0].buyer_order_id
    orderItemPartQuery.order_item_id = vendorOrderData[0].buyer_order_item_id

    const orderItemPartaggregateArr = [
      { $match: orderItemPartQuery },

      {
        $lookup: {
          from: "order_track",
          localField: "_id",
          foreignField: "order_item_part_id",
          as: "TMtrack_details",
        },
      },
      {
        $lookup: {
          from: "CP_order_track",
          localField: "_id",
          foreignField: "order_item_part_id",
          as: "CPtrack_details",
        },
      },
      {
        $project: {
          _id: true,
          order_item_id: true,
          assigned_quantity: true,
          start_time: true,
          end_time: true,
          buyer_order_id: true,
          buyer_id: true,
          temp_quantity: true,
          created_at: true,
          TMtrack_details: true,
          CPtrack_details: true,
        },
      },
    ]

    const orderItemPartData = await OrderItemPartModel.aggregate(
      orderItemPartaggregateArr
    )

    // console.log("orderItemPartData", orderItemPartData)

    // const orderItemPartData = await OrderItemPartModel.find({
    //   buyer_order_id: vendorOrderData[0].buyer_order_id,
    // })
    vendorOrderData[0].orderItemPartData = orderItemPartData

    // for (let i = 0; i < vendorOrderData[0].orderItemPartData.length; i++) {
    //   const TMtrack_details = await OrderTrackModel.find({
    //     order_item_part_id: vendorOrderData[0].orderItemPartData[i]._id,
    //   })
    //   console.log("TMtrack_details")

    //   if (!isNullOrUndefined(TMtrack_details)) {
    //     console.log(TMtrack_details)
    //     vendorOrderData[0].orderItemPartData[
    //       i
    //     ].TMtrack_details = TMtrack_details
    //   }

    //   const CPtrack_details = await CPOrderTrackModel.find({
    //     order_item_part_id: vendorOrderData[0].orderItemPartData[i]._id,
    //   })
    //   console.log("CPtrack_details")

    //   if (!isNullOrUndefined(CPtrack_details)) {
    //     console.log(CPtrack_details)
    //     vendorOrderData[0].orderItemPartData[
    //       i
    //     ].CPtrack_details = CPtrack_details

    //     console.log("vendorOrderData[0].orderItemPartData[i]")
    //     console.log(vendorOrderData[0].orderItemPartData[i])
    //   }
    // }

    // console.log("vendorOrderData[0].orderItemPartData")
    // console.log(vendorOrderData[0].orderItemPartData)

    // const OrderItemPartData = await OrderItemPartModel.find({
    //   buyer_order_id: new ObjectId(orderId),
    // })
    // if (orderTrackData.length > 0) {
    //   vendorOrderData[0].OrderItemPartData = OrderItemPartData
    // }

    return Promise.resolve(vendorOrderData[0])
  }

  async cancelOrder(user_id: string, order_id: string) {
    //Find supplier order.
    const data = await VendorOrderModel.findOne({
      _id: order_id,
      user_id,
      order_status: { $ne: SupplierOrderStatus.DELIVERED },
    })

    if (!isNullOrUndefined(data)) {
      //update buyer order item status also.
      await VendorOrderModel.findOneAndUpdate(
        {
          _id: order_id,
        },
        {
          $set: {
            order_status: SupplierOrderStatus.CANCELLED,
            cancelled_at: Date.now(),
          },
        }
      )

      const orderItemData = OrderItemModel.findOne({
        display_item_id: data.buyer_order_display_item_id,
        supplier_id: user_id,
        item_status: { $ne: OrderItemStatus.DELIVERED },
      })

      if (!isNullOrUndefined(orderItemData)) {
        await OrderItemModel.findOneAndUpdate(
          {
            _id: new ObjectId(data.buyer_order_item_id),
          },
          {
            $set: {
              item_status: OrderItemStatus.CANCELLED,
              cancelled_at: Date.now(),
            },
          }
        )

        let buyerUserData = await BuyerUserModel.findOne({
          _id: new ObjectId(data.buyer_id),
        })
        // let client_name: any
        // let mobile_number: any
        // if (!isNullOrUndefined(buyerUserData)) {
        //   client_name = buyerUserData.full_name
        //   mobile_number = buyerUserData.mobile_number
        // }

        let client_name: any
        let mobile_number: any
        let email: any
        if (!isNullOrUndefined(buyerUserData)) {
          mobile_number = buyerUserData.mobile_number
          email =  buyerUserData.email
          if (buyerUserData.account_type === "Individual") {
            client_name = buyerUserData.full_name
          } else {
            client_name = buyerUserData.company_name
          }
        }

        // let vendorUserData = await VendorUserModel.findOne({
        //   _id: new ObjectId(user_id)
        // })
        // let vendor_name: any
        // if(!isNullOrUndefined(vendorUserData)){
        //   vendor_name = vendorUserData.full_name
        // }
        let buyerOrderData = await OrderModel.findOne({
          display_id: new ObjectId(data.buyer_order_display_id),
        })
        let site_id: any
        if (!isNullOrUndefined(buyerOrderData)) {
          site_id = buyerOrderData.site_id
        }

        let siteInfo = await SiteModel.findOne({
          _id: new ObjectId(site_id),
        })
        let site_mobile_number: any
        let site_person_name: any
        let site_person_email: any
        if (!isNullOrUndefined(siteInfo)) {
          site_mobile_number = siteInfo.mobile_number
          site_person_name = siteInfo.person_name
          site_person_email = siteInfo.email
        }

        // send sms and email to associated buyer/supplier and admin too.
        // let message = `Hi ${client_name}, Sorry to inform you that order with an Order ID ${data.buyer_order_display_id} has been cancelled by the supplier ${vendor_name}.`
        // // Order cancelled by supplier side
        // let templateId = "1707163577173066391"
        // await this.testService.sendSMS(mobile_number, message, templateId)

        let message = `Hi ${client_name}, sorry to inform you that your placed order with Product Id ${data.buyer_order_display_item_id} having an Order ID ${data.buyer_order_display_id} order has not been accepted by any RMC Supplier. Thus your placed order is cancelled with No Action. - conmix`
        // Order cancelled by supplier side
        let templateId = "1707163732917331188"
        await this.testService.sendSMS(mobile_number, message, templateId)

        let site_message = `Hi ${site_person_name}, sorry to inform you that your placed order with Product Id ${data.buyer_order_display_item_id} having an Order ID ${data.buyer_order_display_id} order has not been accepted by any RMC Supplier. Thus your placed order is cancelled with No Action. - conmix`
        // Order cancelled by supplier side
        let site_templateId = "1707163732917331188"
        await this.testService.sendSMS(
          site_mobile_number,
          site_message,
          site_templateId
        )

        let order_cancelled_by_supplier_mail_html = client_mail_template.order_cancelled_by_supplier
        order_cancelled_by_supplier_mail_html = order_cancelled_by_supplier_mail_html.replace("{{client_name}}", client_name)
        order_cancelled_by_supplier_mail_html = order_cancelled_by_supplier_mail_html.replace("{{data.buyer_order_display_item_id}}", data.buyer_order_display_item_id) 
        order_cancelled_by_supplier_mail_html = order_cancelled_by_supplier_mail_html.replace("{{data.buyer_order_display_id}}", data.buyer_order_display_id)
        var data1 = {
            from: "no-reply@conmate.in",
            to: email,
            subject: "Conmix - Order cancelled",
            html: order_cancelled_by_supplier_mail_html,
          }
          await this.mailgunService.sendEmail(data1)

          let order_cancelled_by_supplier_mail_to_site_html = client_mail_template.order_cancelled_by_supplier
          order_cancelled_by_supplier_mail_to_site_html = order_cancelled_by_supplier_mail_to_site_html.replace("{{client_name}}", site_person_name)
          order_cancelled_by_supplier_mail_to_site_html = order_cancelled_by_supplier_mail_to_site_html.replace("{{data.buyer_order_display_item_id}}", data.buyer_order_display_item_id) 
          order_cancelled_by_supplier_mail_to_site_html = order_cancelled_by_supplier_mail_to_site_html.replace("{{data.buyer_order_display_id}}", data.buyer_order_display_id)
          var data1 = {
              from: "no-reply@conmate.in",
              to: site_person_email,
              subject: "Conmix - Order cancelled",
              html: order_cancelled_by_supplier_mail_to_site_html,
            }
            await this.mailgunService.sendEmail(data1)  

         let order_cancelled_by_supplier_payment_mail_html = client_mail_template.order_cancelled_by_supplier_payment_refund
         order_cancelled_by_supplier_payment_mail_html = order_cancelled_by_supplier_payment_mail_html.replace("{{client_name}}", site_person_name)
         order_cancelled_by_supplier_payment_mail_html = order_cancelled_by_supplier_payment_mail_html.replace("{{data.buyer_order_display_item_id}}", data.buyer_order_display_item_id) 
         order_cancelled_by_supplier_payment_mail_html = order_cancelled_by_supplier_payment_mail_html.replace("{{data.buyer_order_display_id}}", data.buyer_order_display_id)
          var data1 = {
              from: "no-reply@conmate.in",
              to: site_person_email,
              subject: "Conmix - Cancelled order payment refund",
              html: order_cancelled_by_supplier_mail_to_site_html,
            }
            await this.mailgunService.sendEmail(data1)   
            
            const adminUserDoc = await AdminUserModel.findOne({
              admin_type: "superAdmin",
            })
      
            if (isNullOrUndefined(adminUserDoc)) {
              return Promise.reject(
                new InvalidInput(`No Admin data were found `, 400)
              )
            }
          
            let order_cancelled_by_supplier_admin_html = admin_mail_template.order_cancelled_by_supplier_admin
            order_cancelled_by_supplier_admin_html = order_cancelled_by_supplier_admin_html.replace("{{client_name}}", site_person_name)
            order_cancelled_by_supplier_admin_html = order_cancelled_by_supplier_admin_html.replace("{{data.buyer_order_display_item_id}}", data.buyer_order_display_item_id) 
            order_cancelled_by_supplier_admin_html = order_cancelled_by_supplier_admin_html.replace("{{data.buyer_order_display_id}}", data.buyer_order_display_id)
             var data11 = {
                 from: "no-reply@conmate.in",
                 to: adminUserDoc.email,
                 subject: "Conmix - Cancelled order",
                 html: order_cancelled_by_supplier_admin_html,
               }
               await this.mailgunService.sendEmail(data11) 
               
               let order_cancelled_by_supplier_payment_admin_html = admin_mail_template.order_cancelled_by_supplier_payment_refund_admin
               order_cancelled_by_supplier_payment_admin_html = order_cancelled_by_supplier_payment_admin_html.replace("{{client_name}}", site_person_name)
               order_cancelled_by_supplier_payment_admin_html = order_cancelled_by_supplier_payment_admin_html.replace("{{data.buyer_order_display_item_id}}", data.buyer_order_display_item_id) 
               order_cancelled_by_supplier_payment_admin_html = order_cancelled_by_supplier_payment_admin_html.replace("{{data.buyer_order_display_id}}", data.buyer_order_display_id)
                var admin_data = {
                    from: "no-reply@conmate.in",
                    to: adminUserDoc.email,
                    subject: "Conmix - Cancelled order payment refund",
                    html: order_cancelled_by_supplier_payment_admin_html,
                  }
                  await this.mailgunService.sendEmail(admin_data)            

      } else {
        return Promise.reject(
          new InvalidInput(
            `Sorry we could not find any item for the entered order Id`,
            400
          )
        )
      }
    } else {
      return Promise.reject(
        new InvalidInput(
          `No Supplier order found associated with this Id.`,
          400
        )
      )
    }
  }

  async uploadBill(
    user_id: string,
    order_id: string,
    fileData: {
      bill_image: UploadedFile
      creditNote_image: UploadedFile
      debitNote_image: UploadedFile
    }
  ) {
    const orderCheck = await VendorOrderModel.findOne({
      _id: order_id,
     // user_id: new ObjectId(user_id),
    })
    if (orderCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    let billArr: {
      url: string
      uploaded_at?: Date
      status?: BillStatus
    }[] = []

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.bill_image) &&
      !isNullOrUndefined(fileData.bill_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_order`
      let objectName = `${orderCheck._id}_${Math.random()}`
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.bill_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.bill_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(fileData.bill_image.mimetype) &&
        /\.doc$/i.test(fileData.bill_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.bill_image.mimetype) &&
        /\.docx$/i.test(fileData.bill_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.bill_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.bill_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.bill_image.data,
        fileData.bill_image.mimetype
      )
      let bill_image_url = getS3MediaURL(`${objectDir}/${objectName}`)

      billArr.push({
        url: bill_image_url,
        uploaded_at: new Date(),
        status: BillStatus.PENDING,
      })

      await VendorOrderModel.findOneAndUpdate(
        { _id: order_id, user_id: new ObjectId(user_id) },
        {
          $push: {
            bills: billArr,
          },
        }
      )
    }
    let creditNotesArr: {
      url: string
      uploaded_at?: Date
      status?: BillStatus
    }[] = []
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.creditNote_image) &&
      !isNullOrUndefined(fileData.creditNote_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_order`
      let objectName = `${orderCheck._id}_${Math.random()}`

      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.creditNote_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.creditNote_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(fileData.creditNote_image.mimetype) &&
        /\.doc$/i.test(fileData.creditNote_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.creditNote_image.mimetype) &&
        /\.docx$/i.test(fileData.creditNote_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.creditNote_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.creditNote_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.creditNote_image.data,
        fileData.creditNote_image.mimetype
      )
      let creditNote_image = getS3MediaURL(`${objectDir}/${objectName}`)

      creditNotesArr.push({
        url: creditNote_image,
        uploaded_at: new Date(),
        status: BillStatus.PENDING,
      })
      await VendorOrderModel.findOneAndUpdate(
        { _id: order_id, user_id: new ObjectId(user_id) },
        {
          $push: {
            creditNotes: creditNotesArr,
          },
        }
      )
    }

    let debitNotesArr: {
      url: string
      uploaded_at?: Date
      status?: BillStatus
    }[] = []
    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.debitNote_image) &&
      !isNullOrUndefined(fileData.debitNote_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_order`
      let objectName = `${orderCheck._id}_${Math.random()}`

      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.debitNote_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.debitNote_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(fileData.debitNote_image.mimetype) &&
        /\.doc$/i.test(fileData.debitNote_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.debitNote_image.mimetype) &&
        /\.docx$/i.test(fileData.debitNote_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.debitNote_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.debitNote_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.debitNote_image.data,
        fileData.debitNote_image.mimetype
      )
      let debitNote_image = getS3MediaURL(`${objectDir}/${objectName}`)

      debitNotesArr.push({
        url: debitNote_image,
        uploaded_at: new Date(),
        status: BillStatus.PENDING,
      })
      await VendorOrderModel.findOneAndUpdate(
        { _id: order_id },
        {
          $push: {
            debitNotes: debitNotesArr,
          },
        }
      )
      return Promise.resolve()
    }
  }

  async updateOrderStatus(
    vendor_id: string,
    order_id: string,
    order_status: string,
    authentication_code: string
  ) {
 
    // Check for status.
    if (order_status === SupplierOrderStatus.REJECTED) {
      const result = await AuthModel.findOne({
        code: authentication_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    // Find vendor order data.
    const vendorOrderData = await VendorOrderModel.findOne({
      _id: order_id,
    })

    if (isNullOrUndefined(vendorOrderData)) {
      return Promise.reject(
        new InvalidInput(
          `No Vendor order found associated with id ${order_id}`,
          400
        )
      )
    }

    // Check already "ACCEPTED".
    if(vendorOrderData.order_status === SupplierOrderStatus.ACCEPTED && 
      order_status === SupplierOrderStatus.ACCEPTED){
     return Promise.reject(
        new InvalidInput(
          `This vendor order status is already ACCEPTED.`,
          400
        )
      )
    }

      // Check already "REJECTED".
    if(vendorOrderData.order_status === SupplierOrderStatus.REJECTED && 
      order_status === SupplierOrderStatus.REJECTED){
     return Promise.reject(
        new InvalidInput(
          `This vendor order status is already REJECTED.`,
          400
        )
      )
    }

    // Update order status.
    await VendorOrderModel.findOneAndUpdate(
      {
        _id: order_id,
      },
      {
        $set: {
          order_status,
        },
      }
    )
     
    // Remove auth code.
    await AuthModel.deleteOne({
      user_id: new ObjectId(vendor_id),
      code: authentication_code,
    })

    // update order Item status.
    if (order_status === SupplierOrderStatus.ACCEPTED) {
      await OrderItemModel.findOneAndUpdate(
        { _id: vendorOrderData.buyer_order_item_id },
        {
          $set: {
            item_status: OrderItemStatus.CONFIRMED,
          },
        }
      )
    }

 

    let buyerUserData = await BuyerUserModel.findOne({
      _id: new ObjectId(vendorOrderData.buyer_id),
    })

    let client_name: any
    let mobile_number: any
    let email: any
    if (!isNullOrUndefined(buyerUserData)) {
      mobile_number = buyerUserData.mobile_number
      email = buyerUserData.email
      if (buyerUserData.account_type === "Individual") {
        client_name = buyerUserData.full_name
      } else {
        client_name = buyerUserData.company_name
      }
    }
    let buyerOrderData = await OrderModel.findOne({
      _id: new ObjectId(vendorOrderData.buyer_order_id),
    })
    let site_name: any
    let site_person_name: any
    let site_mobile_number: any
    let site_email: any
    if (!isNullOrUndefined(buyerOrderData)) {
      let site_id = buyerOrderData.site_id

      let siteData = await SiteModel.findOne({ _id: new ObjectId(site_id) })
      if (!isNullOrUndefined(siteData)) {
        site_name = siteData.site_name
        site_person_name = siteData.person_name
        site_mobile_number = siteData.mobile_number
        site_email = siteData.email
      }
    }

    let vendorUserData = await VendorUserModel.findOne({
      _id: new ObjectId(vendor_id),
    })
    let vendor_mobile_number: any
    let vendor_company_name: any
    let vendor_email: any
    if (!isNullOrUndefined(vendorUserData)) {
      vendor_mobile_number = vendorUserData.mobile_number
      vendor_company_name = vendorUserData.company_name
      vendor_email = vendorUserData.email
    }
    if (order_status === SupplierOrderStatus.ACCEPTED) {
     
      // send SMS And email to Client.
      let message = `Hi ${client_name}, your order for Product Id ${vendorOrderData.buyer_order_display_item_id} having an Order ID ${vendorOrderData.buyer_order_display_id} has been accepted and thus confirmed by the RMC Supplier ${vendor_company_name}. Kindly login to Conmix App to track your order online. - conmix`
      let templateId = "1707163792146437326"
      await this.testService.sendSMS(mobile_number, message, templateId)

      let accept_order_by_supplier_mail_html = client_mail_template.accept_order_by_supplier
      accept_order_by_supplier_mail_html = accept_order_by_supplier_mail_html.replace("{{client_name}}", client_name)
      accept_order_by_supplier_mail_html = accept_order_by_supplier_mail_html.replace("{{vendorOrderData.buyer_order_display_item_id}}", vendorOrderData.buyer_order_display_item_id) 
      accept_order_by_supplier_mail_html = accept_order_by_supplier_mail_html.replace("{{vendorOrderData.buyer_order_display_id}}", vendorOrderData.buyer_order_display_id)
      accept_order_by_supplier_mail_html = accept_order_by_supplier_mail_html.replace("{{vendor_company_name}}", vendor_company_name)
      var data1 = {
            from: "no-reply@conmate.in",
            to: email,
            subject: "Conmix - Order Accepted",
            html: accept_order_by_supplier_mail_html,
          }
          await this.mailgunService.sendEmail(data1)

      // send SMS And email to Site.
      let site_message = `Hi ${site_person_name}, your order for Product Id ${vendorOrderData.buyer_order_display_item_id} having an Order ID ${vendorOrderData.buyer_order_display_id} has been accepted and thus confirmed by the RMC Supplier ${vendor_company_name}. Kindly login to Conmix App to track your order online. - conmix`
      let site_templateId = "1707163792146437326"
      await this.testService.sendSMS(site_mobile_number, site_message, site_templateId)

      let accept_order_by_supplier_site_mail_html = client_mail_template.accept_order_by_supplier
      accept_order_by_supplier_site_mail_html = accept_order_by_supplier_site_mail_html.replace("{{client_name}}", site_person_name)
      accept_order_by_supplier_site_mail_html = accept_order_by_supplier_site_mail_html.replace("{{vendorOrderData.buyer_order_display_item_id}}", vendorOrderData.buyer_order_display_item_id) 
      accept_order_by_supplier_site_mail_html = accept_order_by_supplier_site_mail_html.replace("{{vendorOrderData.buyer_order_display_id}}", vendorOrderData.buyer_order_display_id)
      accept_order_by_supplier_site_mail_html = accept_order_by_supplier_site_mail_html.replace("{{vendor_company_name}}", vendor_company_name)
      var data1 = {
            from: "no-reply@conmate.in",
            to: site_email,
            subject: "Conmix - Order Accepted",
            html: accept_order_by_supplier_site_mail_html,
          }
          await this.mailgunService.sendEmail(data1)
    
      // send SMS And email to vendor.
      let vendor_message = `Hi ${vendor_company_name}, you have accepted and thus confirmed the order with Product Id ${vendorOrderData.buyer_order_display_item_id} having an Order ID ${vendorOrderData.buyer_order_display_id} Kindly login to your partner account to plan for the delivery. - conmix`
      let vendor_templateId = "1707163732945123188"
      await this.testService.sendSMS(vendor_mobile_number, vendor_message, vendor_templateId)

      let accept_order_supplier_html = vendor_mail_template.accept_order
      accept_order_supplier_html = accept_order_supplier_html.replace("{{vendor_company_name}}", vendor_company_name)
      accept_order_supplier_html = accept_order_supplier_html.replace("{{vendorOrderData.buyer_order_display_item_id}}", vendorOrderData.buyer_order_display_item_id) 
      accept_order_supplier_html = accept_order_supplier_html.replace("{{vendorOrderData.buyer_order_display_id}}", vendorOrderData.buyer_order_display_id)
      accept_order_supplier_html = accept_order_supplier_html.replace("{{client_name}}", client_name)
      accept_order_supplier_html = accept_order_supplier_html.replace("{{site_name}}", site_name)
      var v_data1 = {
            from: "no-reply@conmate.in",
            to: vendor_email,
            subject: "Conmix - Order Accepted",
            html: accept_order_supplier_html,
          }
          await this.mailgunService.sendEmail(v_data1)


      // Check Admin Type.
      const adminQuery: { [k: string]: any } = {}
      adminQuery.$or = [
        {
          admin_type : AdminRoles.superAdmin
        },
        {
          admin_type : AdminRoles.admin_manager
        },
        {
          admin_type : AdminRoles.admin_sales
        }
      ]
      const adminUserDocs = await AdminUserModel.find(adminQuery)
      if (adminUserDocs.length < 0) {
        return Promise.reject(
          new InvalidInput(`No Admin data were found `, 400)
        )
      }

      for(let i = 0 ; i< adminUserDocs.length; i ++){ 
        // send  email to Admin.
        let accept_order_admin_html = admin_mail_template.accept_order
        accept_order_admin_html = accept_order_admin_html.replace("{{adminUserDoc.full_name}}", adminUserDocs[i].full_name)
        accept_order_admin_html = accept_order_admin_html.replace("{{vendor_company_name}}", vendor_company_name)
        accept_order_admin_html = accept_order_admin_html.replace("{{vendorOrderData.buyer_order_display_item_id}}", vendorOrderData.buyer_order_display_item_id) 
        accept_order_admin_html = accept_order_admin_html.replace("{{vendorOrderData.buyer_order_display_id}}", vendorOrderData.buyer_order_display_id)
        accept_order_admin_html = accept_order_admin_html.replace("{{client_name}}", client_name)
        accept_order_admin_html = accept_order_admin_html.replace("{{site_name}}", site_name)
        var v_data11 = {
              from: "no-reply@conmate.in",
              to: adminUserDocs[i].email,
              subject: "Conmix - Order Accepted",
              html: accept_order_admin_html,
            }
        await this.mailgunService.sendEmail(v_data11)

        await this.notUtil.addNotificationForAdmin({
              to_user_type: UserType.ADMIN,
              to_user_id: adminUserDocs[i]._id,
              notification_type: AdminNotificationType.orderAccept,
              order_id: vendorOrderData.buyer_order_id,
              order_item_id: vendorOrderData.buyer_order_display_item_id,
              vendor_id: vendorOrderData.user_id,
              client_id: vendorOrderData.buyer_id,
            })
      }


      // send notification for order accept.        
      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: vendorOrderData.buyer_id,
        notification_type: ClientNotificationType.orderAccept,
        order_id: vendorOrderData.buyer_order_id,
        order_item_id: vendorOrderData.buyer_order_display_item_id,
        vendor_id: vendorOrderData.user_id,
      })

      // send notification for Assign qty.        
      await this.notUtil.addNotificationForBuyer({
        to_user_type: UserType.BUYER,
        to_user_id: vendorOrderData.buyer_id,
        notification_type: ClientNotificationType.assignQty,
        order_id: vendorOrderData.buyer_order_id,
        order_item_id: vendorOrderData.buyer_order_display_item_id,
        vendor_id: vendorOrderData.user_id,
      })

      // send SMS for Assign qty to client and site.        
      let ass_qty_message = `Hi ${client_name}, request you to kindly assign the qty for your order with product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData.buyer_order_display_id}. Your order will be lapsed if you fail to assign the qty before 24 hours of its end delivery date. - conmix`
      let ass_qty_templateId = "1707163843566876822"
      await this.testService.sendSMS(mobile_number, ass_qty_message, ass_qty_templateId)

      let ass_qty_site_message = `Hi ${site_person_name}, request you to kindly assign the qty for your order with product id ${vendorOrderData.buyer_order_display_item_id} having an order id ${vendorOrderData.buyer_order_display_id}. Your order will be lapsed if you fail to assign the qty before 24 hours of its end delivery date. - conmix`
      let ass_qty_site_templateId = "1707163843566876822"
      await this.testService.sendSMS(site_mobile_number, ass_qty_site_message, ass_qty_site_templateId)
    }

    return Promise.resolve()
  }
}
