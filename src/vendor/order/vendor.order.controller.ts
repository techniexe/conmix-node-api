import { injectable, inject } from "inversify"
import {
  controller,
  httpGet,
  httpPost,
  httpPatch,
} from "inversify-express-utils"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express-serve-static-core"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { OrderRepository } from "./vendor.order.repository"
import {
  getVendorOrder,
  IGetVendorOrderRequest,
  getOrderById,
  IGetOrderByIdRequest,
  cancelOrderSchema,
  ICancelOrderByIdRequest,
  IUploadBillRequest,
  updateOrderStatusSchema,
  IupdateOrderStatusRequest,
} from "./vendor.order.req-schema"

@injectable()
@controller("/order", verifyCustomToken("vendor"))
export class OrderController {
  constructor(
    @inject(VendorTypes.OrderRepository) private orderRepo: OrderRepository
  ) {}

  // Get vendor order list.
  @httpGet("/", validate(getVendorOrder))
  async getVendorOrder(
    req: IGetVendorOrderRequest,
    res: Response,
    next: NextFunction
  ) {
    const {
      before,
      after,
      payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      order_id,
      month,
      year,
      date,
      buyer_order_id,
      buyer_name
    } = req.query

    const data = await this.orderRepo.getVendorOrder(
      req.user.uid,
      before,
      after,
      payment_status,
      design_mix_id,
      order_status,
      buyer_id,
      address_id,
      order_id,
      month,
      year,
      date,
      buyer_order_id,
      buyer_name
    )
    res.json({ data })
  }

  // Get vendor order details.
  @httpGet("/:orderId", validate(getOrderById))
  async getOrderById(
    req: IGetOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.orderRepo.getOrderById(
        req.params.orderId,
        req.user.uid
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  // Cancel vendor order.
  @httpPost("/:order_id/cancel", validate(cancelOrderSchema))
  async cancelOrder(
    req: ICancelOrderByIdRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.cancelOrder(req.user.uid, req.params.order_id)
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  //Vendor bill upload.
  @httpPost("/:order_id/uploadBill")
  async uploadBill(req: IUploadBillRequest, res: Response, next: NextFunction) {
    try {
      await this.orderRepo.uploadBill(
        req.user.uid,
        req.params.order_id,
        req.files
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }

  //Update vendor order status.
  @httpPatch("/:orderId", validate(updateOrderStatusSchema))
  async updateOrderStatus(
    req: IupdateOrderStatusRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.orderRepo.updateOrderStatus(
        req.user.uid,
        req.params.orderId,
        req.body.order_status,
        req.body.authentication_code
      )
      res.sendStatus(201)
    } catch (err) {
      next(err)
    }
  }
}
