import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"
import { monthsCode } from "../../utilities/config"

export interface IGetVendorOrder {
  user_id?: string
  before?: string
  after?: string
  design_mix_id?: string
  payment_status?: string
  order_status?: string
  buyer_id?: string
  address_id?: string
  order_id?: string
  month?: string
  year?: number
  date: Date
  buyer_order_id: string
  buyer_name: string
}

export interface IGetVendorOrderRequest extends IAuthenticatedRequest {
  query: IGetVendorOrder
}

export const getVendorOrder: IRequestSchema = {
  query: Joi.object().keys({
    user_id: Joi.string().regex(objectIdRegex),
    before: Joi.string(),
    after: Joi.string(),
    design_mix_id: Joi.string().regex(objectIdRegex),
    payment_status: Joi.string(),
    order_status: Joi.string(),
    buyer_id: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    order_id: Joi.string(),
    month: Joi.string().valid(monthsCode),
    year: Joi.number(),
    date: Joi.date().iso(),
    buyer_order_id: Joi.string(),
    buyer_name: Joi.string(),
  }),
}

export interface IGetOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    orderId: string
  }
}

export const getOrderById: IRequestSchema = {
  params: Joi.object().keys({
    orderId: Joi.string(),
  }),
}

export interface ICancelOrderByIdRequest extends IAuthenticatedRequest {
  params: {
    order_id: string
  }
}

export const cancelOrderSchema: IRequestSchema = {
  params: Joi.object().keys({
    order_id: Joi.string().required(),
  }),
}

export interface IUploadBillRequest extends IAuthenticatedRequest {
  files: {
    bill_image: UploadedFile
    creditNote_image: UploadedFile
    debitNote_image: UploadedFile
  }
}

export interface IUpdateOrderStatus {
  order_status: string
  authentication_code: string
}

export interface IupdateOrderStatusRequest extends IAuthenticatedRequest {
  body: IUpdateOrderStatus
  params: {
    orderId: string
  }
}

export const updateOrderStatusSchema: IRequestSchema = {
  body: Joi.object().keys({
    order_status: Joi.string().required(),
    authentication_code: Joi.string(),
  }),
  params: Joi.object().keys({
    orderId: Joi.string().required(),
  }),
}
