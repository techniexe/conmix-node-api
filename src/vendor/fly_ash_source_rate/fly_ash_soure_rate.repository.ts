import { ObjectId } from "bson"
import { inject, injectable } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"

import { isNullOrUndefined } from "../../utilities/type-guards"
import { InvalidInput, UnexpectedInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import {
  IFlyAshSourceRate,
  IEditFlyAshSourceRate,
  IGetFlyAshSourceRateList,
} from "./fly_ash_soure_rate.req-schema"
import { FlyAshSourceRateModel } from "../../model/fly_ash_source_rate.model"
import { AddressModel } from "../../model/address.model"

@injectable()
export class FlyAshSourceRateRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addFlyAshSourceRate(
    vendor_id: string,
    flyAshSourceRateData: IFlyAshSourceRate
  ) {
    const data = await FlyAshSourceRateModel.findOne({
      vendor_id,
      address_id: flyAshSourceRateData.address_id,
      source_id: flyAshSourceRateData.source_id,
    })

    if (!isNullOrUndefined(data)) {
      return Promise.reject(
        new InvalidInput(
          `Fly Ash rate already exists for this source at selected site address.`,
          400
        )
      )
    }
    flyAshSourceRateData.vendor_id = vendor_id
  
    let sub_vendor_id: any
    if(!isNullOrUndefined(flyAshSourceRateData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(flyAshSourceRateData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      }
      flyAshSourceRateData.sub_vendor_id = sub_vendor_id

    const newflyAshSourceRate = new FlyAshSourceRateModel(flyAshSourceRateData)
    const newflyAshSourceRateDoc = await newflyAshSourceRate.save()

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_FLY_ASH_SOURCE_RATE_BY_VENDOR,
      `Vendor added fly ash source rate of id ${newflyAshSourceRateDoc._id} .`
    )

    return Promise.resolve(newflyAshSourceRateDoc)
  }

  async editFlyAshSourceRate(
    vendor_id: string,
    FlyAshSourceRateId: string,
    editData: IEditFlyAshSourceRate
  ): Promise<any> {
    const editDoc: { [k: string]: any } = {}

    const data = await FlyAshSourceRateModel.findOne({
      vendor_id,
      address_id: editData.address_id,
      source_id: editData.source_id,
    })

    if (
      !isNullOrUndefined(data) &&
      data._id.equals(FlyAshSourceRateId) === false
    ) {
      return Promise.reject(
        new InvalidInput(
          `Fly Ash rate already exists for this source at selected site address.`,
          400
        )
      )
    }

    const edtFld = ["address_id", "source_id", "per_kg_rate", "master_vendor_id"]
    for (const [key, value] of Object.entries(editData)) {
      if (edtFld.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id

    let sub_vendor_id: any
    if(!isNullOrUndefined(editData.address_id)){
      let addressData = await AddressModel.findOne({_id: new ObjectId(editData.address_id)})
      if(!isNullOrUndefined(addressData)){
       sub_vendor_id = addressData.sub_vendor_id
      }
      editDoc.sub_vendor_id = sub_vendor_id
    }

    const edtRes = await FlyAshSourceRateModel.updateOne(
      {
        _id: new ObjectId(FlyAshSourceRateId),
      //  vendor_id,
      },
      { $set: editDoc }
    )
    if (edtRes.n !== 1) {
      return Promise.reject(
        new InvalidInput(`Invalid request as no Sand source rate was added by Vendor.`, 400)
      )
    }

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.EDIT_FLY_ASH_SOURCE_RATE_BY_VENDOR,
      "Sand source rate information Updated."
    )

    return Promise.resolve()
  }
  async removeFlyAshSourceRate(FlyAshSourceRateId: string) {
    const status = await FlyAshSourceRateModel.deleteOne({
      _id: new ObjectId(FlyAshSourceRateId),
    })
    if (status.n !== 1) {
      throw new Error("Fly ash rate has not been added")
    }
    return Promise.resolve()
  }

  async List(vendor_id: string, searchParam: IGetFlyAshSourceRateList) {
    const query: { [k: string]: any } = {}

   // query.vendor_id = new ObjectId(vendor_id)

   query.$or = [
    {
      vendor_id: new ObjectId(vendor_id),
    },
    {
      sub_vendor_id: new ObjectId(vendor_id),
    },
    {
      master_vendor_id: new ObjectId(vendor_id),
    },
  ]

    const sort = { created_at: -1 }
    if (!isNullOrUndefined(searchParam.before)) {
      query.created_at = { $lt: new Date(searchParam.before) }
    }
    if (!isNullOrUndefined(searchParam.after)) {
      query.created_at = { $gt: new Date(searchParam.after) }
      sort.created_at = 1
    }

    if (!isNullOrUndefined(searchParam.address_id)) {
      query.address_id = new ObjectId(searchParam.address_id)
    }

    if (!isNullOrUndefined(searchParam.source_id)) {
      query.source_id = new ObjectId(searchParam.source_id)
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "less") {
      query.per_kg_rate = {$lte: parseInt(searchParam.per_kg_rate)}
    }

    if (!isNullOrUndefined(searchParam.per_kg_rate) &&
    !isNullOrUndefined(searchParam.per_kg_rate_value) &&
    searchParam.per_kg_rate_value === "more") {
      query.per_kg_rate = {$gte: parseInt(searchParam.per_kg_rate)}
    }

    const aggregateArr = [
      {
        $match: query,
      },
      {
        $sort: sort,
      },
      {
        $limit: 10,
      },

      {
        $lookup: {
          from: "address",
          localField: "address_id",
          foreignField: "_id",
          as: "address",
        },
      },
      {
        $unwind: {
          path: "$address",
          preserveNullAndEmptyArrays: true,
        },
      },

      {
        $lookup: {
          from: "city",
          localField: "address.city_id",
          foreignField: "_id",
          as: "city",
        },
      },
      {
        $unwind: {
          path: "$city",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "fly_ash_source",
          localField: "source_id",
          foreignField: "_id",
          as: "fly_ash_source",
        },
      },
      {
        $unwind: {
          path: "$fly_ash_source",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "sub_vendor_id",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      {
        $unwind: { path: "$vendorDetails", preserveNullAndEmptyArrays: true },
      },
      {
        $lookup: {
          from: "vendor",
          localField: "master_vendor_id",
          foreignField: "_id",
          as: "masterVendorDetails",
        },
      },
      {
        $unwind: {
          path: "$masterVendorDetails",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          _id: 1,
          "address._id": 1,
          "address.line1": 1,
          "address.line2": 1,
          "address.state_id": "$city.state_id",
          "address.state_name": "$city.state_name",
          "address.city_id": "$city._id",
          "address.city_name": "$city.city_name",
          "address.pincode": 1,
          "address.location": 1,
          "address.business_name": 1,
          "fly_ash_source._id": 1,
          "fly_ash_source.fly_ash_source_name": 1,
          per_kg_rate: 1,
          created_at: 1,
          updated_at: 1,
          "vendorDetails._id": 1,
          "vendorDetails.full_name": 1,
          "vendorDetails.mobile_number": 1,
          "vendorDetails.email": 1,
          "vendorDetails.password": 1,
          "masterVendorDetails._id": 1,
          "masterVendorDetails.full_name": 1,
          "masterVendorDetails.mobile_number": 1,
          "masterVendorDetails.email": 1,
          "masterVendorDetails.password": 1,
        },
      },
    ]
    const data = await FlyAshSourceRateModel.aggregate(aggregateArr)
    return Promise.resolve(data)
  }
}
