import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpDelete,
  httpGet,
} from "inversify-express-utils"
import { verifyCustomToken } from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { FlyAshSourceRateRepository } from "./fly_ash_soure_rate.repository"
import {
  addFlyAshSourceRateSchema,
  IAddFlyAshSourceRateRequest,
  editFlyAshSourceRateSchema,
  IEditFlyAshSourceRateRequest,
  removeFlyAshSourceRateSchema,
  IRemoveFlyAshSourceRateRequest,
  getFlyAshSourceRateSchema,
  IGetFlyAshSourceRateRequest,
} from "./fly_ash_soure_rate.req-schema"

@injectable()
@controller("/fly_ash_source_rate", verifyCustomToken("vendor"))
export class FlyAshSourceRateController {
  constructor(
    @inject(VendorTypes.FlyAshSourceRateRepository)
    private flyAshSourceRateRepo: FlyAshSourceRateRepository
  ) {}

  @httpPost("/", validate(addFlyAshSourceRateSchema))
  async addFlyAshSourceRate(
    req: IAddFlyAshSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      const { _id } = await this.flyAshSourceRateRepo.addFlyAshSourceRate(
        uid,
        req.body
      )
      return res.json({ data: { _id } })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:FlyAshSourceRateId", validate(editFlyAshSourceRateSchema))
  async editFlyAshSourceRate(
    req: IEditFlyAshSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.flyAshSourceRateRepo.editFlyAshSourceRate(
      req.user.uid,
      req.params.FlyAshSourceRateId,
      req.body
    )
    res.sendStatus(202)
  }

  @httpDelete("/:FlyAshSourceRateId", validate(removeFlyAshSourceRateSchema))
  async removeFlyAshSourceRate(
    req: IRemoveFlyAshSourceRateRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.flyAshSourceRateRepo.removeFlyAshSourceRate(
        req.params.FlyAshSourceRateId
      )
      res.sendStatus(204)
    } catch (err) {
      next(err)
    }
  }
  @httpGet("/", validate(getFlyAshSourceRateSchema))
  async List(req: IGetFlyAshSourceRateRequest, res: Response) {
    const data = await this.flyAshSourceRateRepo.List(req.user.uid, req.query)
    res.json({ data })
  }
}
