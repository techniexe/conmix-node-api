import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IFlyAshSourceRate {
  address_id: string
  source_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IAddFlyAshSourceRateRequest extends IAuthenticatedRequest {
  body: IFlyAshSourceRate
}

export const addFlyAshSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex).required(),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex).required(),
    per_kg_rate: Joi.number().min(1).required(),
  }),
}

export interface IEditFlyAshSourceRate {
  address_id: string
  source_id: string
  per_kg_rate: number
  vendor_id: string
  master_vendor_id: string
  [k: string]: any
}

export interface IEditFlyAshSourceRateRequest extends IAuthenticatedRequest {
  params: {
    FlyAshSourceRateId: string
  }
  body: IEditFlyAshSourceRate
}

export const editFlyAshSourceRateSchema: IRequestSchema = {
  body: Joi.object().keys({
    address_id: Joi.string().regex(objectIdRegex),
    master_vendor_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number().min(1),
  }),

  params: Joi.object().keys({
    FlyAshSourceRateId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export interface IRemoveFlyAshSourceRateRequest extends IAuthenticatedRequest {
  params: {
    FlyAshSourceRateId: string
  }
}

export const removeFlyAshSourceRateSchema: IRequestSchema = {
  params: Joi.object().keys({
    FlyAshSourceRateId: Joi.string().regex(objectIdRegex),
  }),
}

export interface IGetFlyAshSourceRateList {
  before?: string
  after?: string
  address_id?: string
  source_id?: string
  per_kg_rate?: any
  per_kg_rate_value?: string
}

export interface IGetFlyAshSourceRateRequest extends IAuthenticatedRequest {
  query: IGetFlyAshSourceRateList
}

export const getFlyAshSourceRateSchema: IRequestSchema = {
  query: Joi.object().keys({
    before: Joi.string(),
    after: Joi.string(),
    address_id: Joi.string().regex(objectIdRegex),
    source_id: Joi.string().regex(objectIdRegex),
    per_kg_rate: Joi.number(),
    per_kg_rate_value: Joi.string(),
  }),
}
