import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import { VendorTypes } from "../vendor.types"
import { VendorUserRepository } from "./vendor.user.repository"
import { validate } from "../../middleware/joi.middleware"
import {
  createVendorUserSchema,
  editVendorUserSchema,
  verifyMobilenumberSchema,
  resendEmailSchema,
  resendOTPSchema,
  changePasswordSchema,
  forgotPasswordSchema,
  requestOTPForUpdateProfileSchema,
} from "./vendor.user.req-schema"
import {
  ICreateSupplierProfileRequest,
  IEditSupplierProfileRequest as IEditVendorProfileRequest,
  IVerifyMobilenumberRequest,
  IResendEmailRequest,
  IResendOTPRequest,
  IChangePasswordRequest,
  IForgotPasswordRequest,
} from "./vendor.user.req-defination"
import { NextFunction, Response } from "express"
import { createJWTToken } from "../../utilities/jwt.utilities"
import {
  IAuthenticatedRequest,
  verifyCustomToken,
} from "../../middleware/auth-token.middleware"
import { VendorSettingRepository } from "../setting/vendor.setting.repository"
//import { InvalidInput } from "../../utilities/customError"

// export interface IAddSetting {
//   with_TM?: true
//   TM_price: 0
//   with_CP?: true
//   CP_price: 0
//   is_customize_design_mix?: true
// }

@injectable()
@controller("/users", verifyCustomToken("vendor"))
export class VendorUserController {
  constructor(
    @inject(VendorTypes.VendorUserRepository)
    private vendorUserRepo: VendorUserRepository,
    @inject(VendorTypes.VendorSettingRepository)
    private vendorSettingRepo: VendorSettingRepository
  ) {}



  @httpGet("/subvendorlisting")
  async getSubVendor(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { search, before, after } = req.query
      const data = await this.vendorUserRepo.getSubVendor(
        req.user.uid,
        search,
        before,
        after
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  /** If user is login then this service may be used to change his password */
  @httpPatch("/changePassword", validate(changePasswordSchema))
  async changePassword(
    req: IChangePasswordRequest,
    res: Response,
    next: NextFunction
  ) {
    await this.vendorUserRepo.changePassword(
      req.user.uid,
      req.body.old_password,
      req.body.password,
      req.body.authentication_code
    )
    res.sendStatus(201)
  }

    /** Forgot Password. */
    @httpPatch("/forgotPassword", validate(forgotPasswordSchema))
    async forgotPassword(
      req: IForgotPasswordRequest,
      res: Response,
      next: NextFunction
    ) {
      try {
        await this.vendorUserRepo.forgotPassword(req.user.uid, req.body.password)
        res.sendStatus(201)
      } catch (err) {
        return next(err)
      }
    }

  @httpPost("/", validate(createVendorUserSchema))
  async createProfile(
    req: ICreateSupplierProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const stat = await this.vendorUserRepo.createUser(
        req.user.uid,
        req.body,
        req.query.signup_type,
        req.files
      )
      const jwtObj = {
        user_type: "vendor",
        full_name: stat.full_name,
        uid: stat._id,
      }
      const IAddSetting = {
        with_TM: true,
        TM_price: 0,
        with_CP: true,
        CP_price: 0,
        is_customize_design_mix: true,
        master_vendor_id: stat._id
      }

      const customToken = await createJWTToken(jwtObj, "30d")
      await this.vendorSettingRepo.addSetting(stat._id, IAddSetting)

      res.status(200).json({ data: { customToken } })
    } catch (err) {
      //let msg = `Unable to add Vehicle Category`
      // if (err.code === 11000) {
      //   let msg = `User with pan_number "${req.body.pan_number}" already exists.`
      //   const err1 = new InvalidInput(msg)
      //   err1.httpStatusCode = 400
      //   return next(err1)
      // }
      if (err.message === "User already registered.") {
        return res.status(422).json({
          error: {
            message: "Invalid Request",
            details: [{ path: "token", message: err.message }],
          },
        })
      }
      err.httpStatusCode = 400
      return next(err)
    }
  }

  @httpPatch("/", validate(editVendorUserSchema))
  async editProfile(
    req: IEditVendorProfileRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const stat = await this.vendorUserRepo.editUser(
        req.user.uid,
        req.body,
        req.files
      )
      const jwtObj = {
        user_type: "vendor",
        full_name: stat.full_name,
        uid: stat._id,
      }
      const customToken = await createJWTToken(jwtObj, "30d")
      res.status(200).json({ data: { customToken } })
    } catch (err) {
      return next(err)
    }
  }

  @httpGet("/profile")
  async getUserDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const user = await this.vendorUserRepo.getUserProfile(req.user.uid)
      if (user === null) {
        return res
          .status(404)
          .json({ error: { message: `Sorry we did not find any user with this details` } })
      }
      return res.json({ data: user })
    } catch (err) {
      next(err)
    }
  }

  @httpPost(
    "/mobile/:mobileNumber/code/:code",
    validate(verifyMobilenumberSchema)
  )
  async verifyMobilenumber(
    req: IVerifyMobilenumberRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.vendorUserRepo.verifyUserWithMobilenumber(
        req.user.uid,
        req.params.mobileNumber,
        req.params.code
      )
      return res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendEmail", validate(resendEmailSchema))
  async resendEmail(
    req: IResendEmailRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const message = await this.vendorUserRepo.resendEmail(
        req.user.uid,
        req.body.email
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/resendOTP", validate(resendOTPSchema))
  async resendOTP(req: IResendOTPRequest, res: Response, next: NextFunction) {
    try {
      const message = await this.vendorUserRepo.resendOTP(
        req.user.uid,
        req.body.mobileNumber
      )
      return res.json({ data: { message } })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/requestOTP")
  async requestOTP(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.vendorUserRepo.requestOTP(req.user.uid, req.query)
      return res.send({ data: data.code })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/requestOTPForMaster")
  async requestOTPForMaster(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.vendorUserRepo.requestOTPForMaster(req.user.uid)
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPost("/requestOTPForUpdateProfile", validate(requestOTPForUpdateProfileSchema))
  async requestOTPForUpdateProfile(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.vendorUserRepo.requestOTPForUpdateProfile(req.user.uid, req.body)
      return res.send({ data })
    } catch (err) {
      next(err)
    }
  }
}
