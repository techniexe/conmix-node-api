import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface ICreateSupplierUserDetails {
  full_name: string
  email: string
  image?: string
  signup_type: string
}

export interface ICreateVendorProfile {
  full_name: string
  company_name: string
  mobile_number?: string
  email?: string
  landline_number?: string
  account_type: string // personal/company
  company_type: string
  password: string
  pan_number: string
  pancard_image_url?: string
  gst_number?: string
  cst?: string
  annual_turnover?: number
  trade_capacity?: string
  number_of_employee?: number
  company_certification_number: string
  company_certification_image_url?: string
  hse_number?: string
  ehs_as_per_iso?: string
  ohsas_as_per_iso?: string
  working_with_other_ecommerce?: boolean
  language?: string
  authentication_code: string
  region_served: [string]
}

export interface ICreateSupplierProfileRequest extends IAuthenticatedRequest {
  body: ICreateVendorProfile
  files: {
    pancard_image: UploadedFile
    company_certification_image: UploadedFile
    gst_certification_image: UploadedFile
  }
}

export interface IEditSupplierProfile {
  full_name?: string
  company_name?: string
  pan_number?: string
  pancard_image_url?: string
  gst_number?: string
  password?: string
  email?: string
  mobile_number?: string
  landline_number?: string
  cst?: string
  annual_turnover?: number
  trade_capacity?: string
  number_of_employee?: number
  company_certification_number?: string
  company_certification_image_url?: string
  hse_number?: string
  ehs_as_per_iso?: string
  ohsas_as_per_iso?: string
  working_with_other_ecommerce?: boolean
  language?: string
  company_type?: string
  region_served: [string]
  authentication_code: string
  new_mobile_code: string
  new_email_code: string
  old_mobile_code: string
  old_email_code: string
}
export interface IEditSupplierProfileRequest extends IAuthenticatedRequest {
  body: IEditSupplierProfile
  files: {
    pancard_image: UploadedFile
    company_certification_image: UploadedFile
    gst_certification_image: UploadedFile
  }
}

export interface IVerifyMobilenumberRequest extends IAuthenticatedRequest {
  params: {
    mobileNumber: string
    code: string
  }
}

export interface IResendEmailRequest extends IAuthenticatedRequest {
  body: {
    email: string
  }
}

export interface IResendOTPRequest extends IAuthenticatedRequest {
  body: {
    mobileNumber: string
  }
}

export interface IChangePasswordRequest extends IAuthenticatedRequest {
  body: {
    old_password: string
    password: string
    authentication_code: string
  }
}

export interface IForgotPasswordRequest extends IAuthenticatedRequest {
  body: {
    password: string
  }
}