import { ObjectId } from "bson"
import { injectable, inject } from "inversify"
import { IVendorUserDoc, VendorUserModel } from "../../model/vendor.user.model"
import {
  ICreateSupplierUserDetails,
  ICreateVendorProfile,
  IEditSupplierProfile,
} from "./vendor.user.req-defination"
import { isNullOrUndefined } from "../../utilities/type-guards"
import * as bcrypt from "bcryptjs"
import {
  AuthEmailModel,
  AuthModel,
  emailVerificationType,
} from "../../model/auth.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { emailVerificationLinkURI, awsConfig, UserType } from "../../utilities/config"
import { MailService } from "../../utilities/mail-service"
import { logger } from "../../logger"
import { VendorAuthRepository } from "../auth/vendor.auth.repository"
//import { TwilioService } from "../../utilities/twilio-service"
import { AccessTypes } from "../../model/access_logs.model"
import { UploadedFile } from "express-fileupload"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { UniqueidService } from "../../utilities/uniqueid-service"
import { CityModel, StateModel } from "../../model/region.model"
import { VendorSettingModel } from "../../model/vendor.setting.model"
import { AddressModel } from "../../model/address.model"
import { TestService } from "../../utilities/test.service"
import { MailGunService } from "../../utilities/mailgun.service"
import { generateRandomNumber } from "../../utilities/generate-random-number"
import { vendor_mail_template } from "../../utilities/vendor_email_template"
import { AdminRoles, AdminUserModel } from "../../model/admin.user.model"
import { AdminNotificationType } from "../../model/notification.model"
import { NotificationUtility } from "../../utilities/notification.utility"
import { BuyerUserModel } from "../../model/buyer.user.model"
import { AdmixtureQuantityModel } from "../../model/admixture_quantity.model"
import { SandSourceQuantityModel } from "../../model/sand_source_quantity.model"
import { AggregateSourceQuantityModel } from "../../model/aggregate_source_quantity.model"
import { CementQuantityModel } from "../../model/cement_quantity.model"
import { FlyAshSourceQuantityModel } from "../../model/fly_ash_source_quantity.model"
const SALT_WORK_FACTOR = 10
/**
 * Ways in which a user can be identified
 */
export enum UserIdentifiers {
  mobile_number,
  email,
}
export function getUserIdentifierType(identifier: string): UserIdentifiers {
  if (identifier.length === 0) {
    throw new Error("Identifier cannot be of length zero.")
  }
  if (identifier.charAt(0) === "+") {
    return UserIdentifiers.mobile_number
  }
  return UserIdentifiers.email
}

@injectable()
export class VendorUserRepository {
  /**
   * Create new User using (third party auth) gmail or facebook
   * @param user
   */

  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService,
    @inject(VendorTypes.MailService) private mailService: MailService,
    @inject(VendorTypes.VendorAuthRepository)
    private authRepo: VendorAuthRepository,
    //@inject(VendorTypes.TwilioService) private twilioService: TwilioService,
    @inject(VendorTypes.UniqueidService)
    private uniqueIdService: UniqueidService,
    @inject(VendorTypes.TestService) private testService: TestService,
    @inject(VendorTypes.mailgunService) private mailgunService: MailGunService,
    @inject(VendorTypes.NotificationUtility)
    private notUtil: NotificationUtility
    ) {}

  async getSubVendor(
    master_vendor_id: string,
    search?: string,
    before?: string,
    after?: string
  ) {
    let query: { [k: string]: any } = {}
    // if (!isNullOrUndefined(search) && search !== "") {
    //   const $options = "i"
    //   const $regex = escapeRegExp(search)
    //   query.$or = [
    //     {
    //       mobile_number: search,
    //     },
    //     {
    //       email: search,
    //     },
    //     {
    //       full_name: { $regex, $options },
    //     },
    //   ]
    // }

    query.master_vendor_id = new ObjectId(master_vendor_id)
    if (!isNullOrUndefined(before) && before !== "") {
      query.created_at = { $lt: before }
    }
    if (!isNullOrUndefined(after) && after !== "") {
      query.created_at = { $gt: after }
    }
    let vendorData = await VendorUserModel.find(query)
      .sort({ created_at: -1 })
      .limit(10)

    if (vendorData.length > 0) {
      for (let i = 0; i < vendorData.length; i++) {
        let addressData = await AddressModel.findOne({
          sub_vendor_id: new ObjectId(vendorData[i]._id),
        })
        console.log("addressData", addressData)
        if (!isNullOrUndefined(addressData)) {
          vendorData[i].business_name = addressData.business_name
          vendorData[i].plant_address_id = addressData._id
          vendorData[i].line1 = addressData.line1
          vendorData[i].line2 = addressData.line2
          vendorData[i].state_id = addressData.state_id
          vendorData[i].city_id = addressData.city_id
          vendorData[i].pincode = addressData.pincode
          vendorData[i].address_type = addressData.address_type
          vendorData[i].region_id = addressData.region_id
          vendorData[i].source_id = addressData.source_id
          vendorData[i].billing_address_id = addressData.billing_address_id
          vendorData[i].is_verified = addressData.is_verified
          // vendorData[i].is_accepted = addressData.is_accepted
          // vendorData[i].is_blocked = addressData.is_blocked

          if (!isNullOrUndefined(addressData.city_id)) {
            let cityData = await CityModel.findOne({
              _id: new ObjectId(addressData.cityDetails._id),
            })
            if (!isNullOrUndefined(cityData)) {
              vendorData[i].city_name = addressData.cityDetails.city_name
              vendorData[i].state_name = addressData.cityDetails.state_name
            }
          }
        }
      }
    }
    return vendorData
  }

  async createAndGetUser(
    userInfo: ICreateSupplierUserDetails
  ): Promise<IVendorUserDoc> {
    const userCheck = await VendorUserModel.findOne({ email: userInfo.email })
    if (userCheck !== null) {
      return Promise.resolve(userCheck)
    }
    const userDoc = {
      user_id: await this.uniqueIdService.getUniqueSupplierid(),
      email: userInfo.email,
      full_name: userInfo.full_name,
      image: userInfo.image,
      thumbnail: userInfo.image,
      signup_type: userInfo.signup_type,
    }
    const user = new VendorUserModel(userDoc)
    return user.save()
  }

  async getUserFromDbUsing(
    identifierType: UserIdentifiers,
    value: string
  ): Promise<IVendorUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = value
    return VendorUserModel.findOne(query).exec()
  }

  async getUserWithPassword(
    identifierType: UserIdentifiers,
    identifierValue: string,
    password: string
  ): Promise<IVendorUserDoc | null> {
    const query: { [key: string]: string } = {}
    query[UserIdentifiers[identifierType]] = identifierValue
    console.log("query:", query)
    const doc = await VendorUserModel.findOne(query).exec()
    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.resolve(null)
    }
    if (await bcrypt.compare(password, doc.password)) {
      return Promise.resolve(doc)
    }
    return Promise.resolve(null)
  }

  /**method used for creating fresh user with mobile number token */
  async createUser(
    uid: string,
    createBody: ICreateVendorProfile,
    signup_type: string,
    fileData: {
      pancard_image: UploadedFile
      company_certification_image: UploadedFile
      gst_certification_image: UploadedFile
    }
  ): Promise<IVendorUserDoc> {
    if (!isNullOrUndefined(createBody.company_name)) {
      let data = await VendorUserModel.findOne({
        company_name: createBody.company_name,
      })

      if (!isNullOrUndefined(data)) {
        return Promise.reject(
          new Error("Vendor already registered with same company name.")
        )
      }
    }

    if (!isNullOrUndefined(createBody.mobile_number)) {
      let data = await VendorUserModel.findOne({
        mobile_number: createBody.mobile_number,
      })

      if (!isNullOrUndefined(data)) {
        return Promise.reject(
          new Error("Vendor already registered with same mobile number.")
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        mobile_number: createBody.mobile_number,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same mobileNumber already registserd with client.")
        )
      }


      let adminDoc = await AdminUserModel.findOne({
        mobile_number: createBody.mobile_number,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same mobileNumber already registserd with admin.")
        )
      }
    }

    if (!isNullOrUndefined(createBody.email)) {
      let data = await VendorUserModel.findOne({
        email: createBody.email,
      })

      if (!isNullOrUndefined(data)) {
        return Promise.reject(
          new Error("Vendor already registered with same email.")
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        email: createBody.email,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with client.")
        )
      }


      let adminDoc = await AdminUserModel.findOne({
        email: createBody.email,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with admin.")
        )
      }

    }

    const result = await AuthModel.findOne({
      code: createBody.authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    let createDoc: { [k: string]: any } = {
      user_id: await this.uniqueIdService.getUniqueSupplierid(),
      _id: uid,
      full_name: createBody.full_name.trim(),
      password: await bcrypt.hash(createBody.password, SALT_WORK_FACTOR),
      signup_type,
    }
    let tokenData: any
    const oldData = await VendorUserModel.findOne({ _id: uid }).exec()
    if (oldData !== null) {
      return Promise.reject(new Error("User already registered."))
    }

    if (signup_type === "email") {
      tokenData = await AuthEmailModel.findOne({ _id: uid }).exec()
      if (tokenData === null) {
        return Promise.reject(
          new Error("Unable to validate session. Please verify email again")
        )
      }
    } else {
      tokenData = await AuthModel.findOne({ _id: uid }).exec()
      if (tokenData === null) {
        return Promise.reject(
          new Error("Unable to validate session. Please verify mobile again")
        )
      }
    }
    // Validate user's identifier
    if (
      (signup_type === "mobile" &&
        getUserIdentifierType(tokenData.identifier) !==
          UserIdentifiers.mobile_number) ||
      (signup_type === "email" &&
        getUserIdentifierType(tokenData.email) !== UserIdentifiers.email)
    ) {
      return Promise.reject(new Error("Invalid request"))
    }

    const arrKeys = [
      "email",
      "account_type",
      "pan_number",
      "pancard_image_url",
      "gst_number",
      "mobile_number",
      "landline_number",
      "cst",
      "annual_turnover",
      "trade_capacity",
      "number_of_employee",
      "company_certification_number",
      "company_certification_image_url",
      "hse_number",
      "ehs_as_per_iso",
      "ohsas_as_per_iso",
      "working_with_other_ecommerce",
      "company_type",
      "company_name",
      "region_served",
      // "no_of_plants",
      // "plant_capacity_per_hour",
      // "no_of_operation_hour",
    ]
    for (const [key, value] of Object.entries(createBody)) {
      if (arrKeys.indexOf(key) > -1) {
        createDoc[key] = value
      }
    }

    if (signup_type === "mobile") {
      createDoc.mobile_number = tokenData.identifier
      createDoc.mobile_verified = true
    } else if (signup_type === "email") {
      createDoc.email = tokenData.email
      createDoc.email_verified = true
    }
    createDoc.current_year = new Date().getFullYear()

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.pancard_image) &&
      !isNullOrUndefined(fileData.pancard_image.data)
    ) {
      console.log(
        "fileData.pancard_image.mimetype",
        fileData.pancard_image.mimetype
      )

      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + createDoc._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.pancard_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(fileData.pancard_image.mimetype) &&
        /\.doc$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.pancard_image.mimetype) &&
        /\.docx$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.pancard_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid pancard_image format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.pancard_image.data,
        fileData.pancard_image.mimetype
      )
      createDoc.pancard_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.company_certification_image) &&
      !isNullOrUndefined(fileData.company_certification_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + createDoc._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.company_certification_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(
          fileData.company_certification_image.mimetype
        ) &&
        /\.doc$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.company_certification_image.mimetype) &&
        /\.docx$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.company_certification_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid company_certification_image format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.company_certification_image.data,
        fileData.company_certification_image.mimetype
      )
      createDoc.company_certification_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.gst_certification_image) &&
      !isNullOrUndefined(fileData.gst_certification_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + createDoc._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.gst_certification_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(
          fileData.gst_certification_image.mimetype
        ) &&
        /\.doc$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.gst_certification_image.mimetype) &&
        /\.docx$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.gst_certification_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid gst_certification_image format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.gst_certification_image.data,
        fileData.gst_certification_image.mimetype
      )
      createDoc.gst_certification_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    const userDoc = await new VendorUserModel(createDoc).save()

    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.CREATE_VENDOR_PROFILE,
      `Vendor profile created with id ${userDoc._id} .`
    )

    let welcome_mail_html = vendor_mail_template.welcome
    welcome_mail_html = welcome_mail_html.replace("{{createBody.company_name}}", createBody.company_name)
    if (!isNullOrUndefined(createBody.email)) {
      var data = {
        from: "no-reply@conmate.in",
        to: createBody.email,
        subject: "Conmix - Welcome",
        html: welcome_mail_html,
      }
      await this.mailgunService.sendEmail(data)
    }

    if (!isNullOrUndefined(createBody.mobile_number)) {
      const authCode = await this.authRepo.getVendorAuthCode(
        createBody.mobile_number,
        uid
      )
      if (isNullOrUndefined(authCode)) {
        return Promise.reject(new UnexpectedInput(`No authCode found`, 400))
      }
      //mobile number has been changed. Send OTP verification to user
      // await this.twilioService.sendCode(
      //   createBody.mobile_number,
      //   authCode.code,
      //   "sms"
      // )

      // let message = `Hi ${createBody.full_name}, Welcome to Conmix - an online mobile and web platform to sell your RMC. You can now sell, manage RMC production and deliveries efficiently using Conmix. Login to your Partner Account for business.`
      let message = `Hi ${createBody.company_name}, Welcome to Conmix - an online mobile and web platform to sell your RMC. You can now sell, manage RMC production and deliveries efficiently using Conmix. Login to your Partner Account for business.`
      let templateId = "1707163731233668919"
      await this.testService.sendSMS(
        createBody.mobile_number,
        message,
        templateId
      )
    }

   // Check Admin Type.
   const adminQuery: { [k: string]: any } = {}
   adminQuery.$or = [
     {
       admin_type : AdminRoles.superAdmin
     },
     {
       admin_type : AdminRoles.admin_manager
     },
     {
       admin_type : AdminRoles.admin_customer_care
     }
   ]
   const adminUserDocs = await AdminUserModel.find(adminQuery)
   if (adminUserDocs.length < 0) {
     return Promise.reject(
       new InvalidInput(`No Admin data were found `, 400)
     )
   }
   for(let i = 0 ; i< adminUserDocs.length; i ++){ 
    await this.notUtil.addNotificationForAdmin({
      to_user_type: UserType.ADMIN,
      to_user_id: adminUserDocs[i]._id,
      notification_type: AdminNotificationType.vendorRegister,
      vendor_id: userDoc._id,
    })
   }
    await AuthModel.deleteOne({ _id: uid })
    return userDoc
  }

  async editUser(
    uid: string,
    editBody: IEditSupplierProfile,
    fileData: {
      pancard_image: UploadedFile
      company_certification_image: UploadedFile
      gst_certification_image: UploadedFile
    }
  ): Promise<IVendorUserDoc> {
    const userCheck = await VendorUserModel.findOne({ _id: uid })
    if (userCheck === null) {
      return Promise.reject(
        new InvalidInput(`Update failed as no record exists`, 400)
      )
    }

    // const result = await AuthModel.findOne({
    //   code: editBody.authentication_code,
    // })
    // if (isNullOrUndefined(result)) {
    //   return Promise.reject(new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400))
    // }

    if (!isNullOrUndefined(editBody.new_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.new_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.new_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_mobile_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_mobile_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.old_email_code)) {
      const result = await AuthModel.findOne({
        code: editBody.old_email_code,
      })
      if (isNullOrUndefined(result)) {
        return Promise.reject(
          new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
        )
      }
    }

    if (!isNullOrUndefined(editBody.company_name)) {
      const numberCheck = await VendorUserModel.findOne({
        company_name: editBody.company_name,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(uid) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This company name is already associated with other vendor.`,
            400
          )
        )
      }
    }

    if (!isNullOrUndefined(editBody.mobile_number)) {
      const numberCheck = await VendorUserModel.findOne({
        mobile_number: editBody.mobile_number,
      })
      console.log("numberCheck", numberCheck)
      console.log("uid", uid)
      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(uid) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This mobile_number is already associated with other vendor.`,
            400
          )
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        mobile_number: editBody.mobile_number,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same mobile_number already registserd with client.")
        )
      }


      let adminDoc = await AdminUserModel.findOne({
        mobile_number: editBody.mobile_number,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same mobile_number already registserd with admin.")
        )
      }
    }

    if (!isNullOrUndefined(editBody.email)) {
      const numberCheck = await VendorUserModel.findOne({
        email: editBody.email,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(uid) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `This email is already associated with other vendor.`,
            400
          )
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        email: editBody.email,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with client.")
        )
      }


      let adminDoc = await AdminUserModel.findOne({
        email: editBody.email,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with admin.")
        )
      }
    }

    let editDoc: { [k: string]: any } = {}
    const arrKeys = [
      "full_name",
      "company_name",
      "pan_number",
      "pancard_image_url",
      "gst_number",
      "landline_number",
      "cst",
      "annual_turnover",
      "trade_capacity",
      "number_of_employee",
      "company_certification_number",
      "company_certification_image_url",
      "hse_number",
      "ehs_as_per_iso",
      "ohsas_as_per_iso",
      "working_with_other_ecommerce",
      "language",
      "company_type",
      "region_served",
      // "no_of_plants",
      // "plant_capacity_per_hour",
      // "no_of_operation_hour",
    ]
    for (const [key, value] of Object.entries(editBody)) {
      if (arrKeys.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }

    let subVendorEditDoc:  { [k: string]: any } = {}
    const subVendorarrKeys = [
      "company_name",
      "pan_number",
      "pancard_image_url",
      "gst_number",
      "landline_number",
      "cst",
      "annual_turnover",
      "trade_capacity",
      "number_of_employee",
      "company_certification_number",
      "company_certification_image_url",
      "hse_number",
      "ehs_as_per_iso",
      "ohsas_as_per_iso",
      "working_with_other_ecommerce",
      "language",
      "company_type",
      "region_served",
    ]
    for (const [key, value] of Object.entries(editBody)) {
      if (subVendorarrKeys.indexOf(key) > -1) {
        subVendorEditDoc[key] = value
      }
    }
    console.log(editDoc)
    if (!isNullOrUndefined(editBody.password)) {
      editDoc.password = await bcrypt.hash(editBody.password, SALT_WORK_FACTOR)
    }
    let mobile_number: any
    if (!isNullOrUndefined(editBody.mobile_number)) {
      mobile_number = editBody.mobile_number
    } else {
      mobile_number = userCheck.mobile_number
    }
    let email: any
    if (!isNullOrUndefined(editBody.email)) {
      email = editBody.email
    } else {
      email = userCheck.email
    }
    console.log("editBody", editBody)
    console.log("userCheck", userCheck)
    if (!isNullOrUndefined(editBody.email)) {
      editDoc.email = editBody.email

      if (userCheck.email !== editBody.email) {
        editDoc.email_verified = false

        let message = `Hi ${userCheck.company_name}, you have successfully updated your email id to ${editBody.email}. Your earlier registered email id. was ${userCheck.email}. - conmix`
        // mobile no update
        let templateId = "1707163894403996215"
        await this.testService.sendSMS(mobile_number, message, templateId)
     
        var data1 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Email Update Successful",
          html: `Dear ${userCheck.company_name},<br>
          You have successfully updated your email id to ${editBody.email}. Your earlier registered email id. was ${userCheck.email}.
          <br>  <br> <br>
          Regards,<br>
          Conmate E-Commerce Pvt. Ltd.`,
        }
        await this.mailgunService.sendEmail(data1)
      }
    }

    if (!isNullOrUndefined(editBody.mobile_number)) {
      editDoc.mobile_number = editBody.mobile_number

      // let message = `Hi ${userCheck.company_name}, you have successfully updated your Mobile No. to ${editBody.mobile_number}. Your earlier registered Mobile no. was ${editBody.mobile_number}. - conmix`
      // // mobile no update
      // let templateId = "1707163894397944141"
      // await this.testService.sendSMS(editBody.mobile_number, message, templateId)

      if (userCheck.mobile_number !== editBody.mobile_number) {
        editDoc.mobile_verified = false

        let message = `Hi ${userCheck.company_name}, you have successfully updated your Mobile No. to ${editBody.mobile_number}. Your earlier registered Mobile no. was ${userCheck.mobile_number}. - conmix`
        // mobile no update
        let templateId = "1707163894397944141"
        await this.testService.sendSMS(mobile_number, message, templateId)
      
        var data11 = {
          from: "no-reply@conmate.in",
          to: email,
          subject: "Conmix - Mobile number Update Successful",
          html: `Dear ${userCheck.company_name},<br>
          You have successfully updated your Mobile No. to ${editBody.mobile_number}. Your earlier registered Mobile no. was ${userCheck.mobile_number}.
          <br>  <br> <br>

          Regards,<br>
          Conmate E-Commerce Pvt. Ltd.`,
        }
        await this.mailgunService.sendEmail(data11)
      }
    }

    if (Object.keys(editDoc).length < 1) {
      return Promise.reject(
        new UnexpectedInput(`Kindly enter a field for update`, 400)
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.pancard_image) &&
      !isNullOrUndefined(fileData.pancard_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + userCheck._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.pancard_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(fileData.pancard_image.mimetype) &&
        /\.doc$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.pancard_image.mimetype) &&
        /\.docx$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.pancard_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.pancard_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.pancard_image.data,
        fileData.pancard_image.mimetype
      )
      editDoc.pancard_image_url = getS3MediaURL(`${objectDir}/${objectName}`)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.company_certification_image) &&
      !isNullOrUndefined(fileData.company_certification_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + userCheck._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.company_certification_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(
          fileData.company_certification_image.mimetype
        ) &&
        /\.doc$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.company_certification_image.mimetype) &&
        /\.docx$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.company_certification_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.company_certification_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.company_certification_image.data,
        fileData.company_certification_image.mimetype
      )
      editDoc.company_certification_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.gst_certification_image) &&
      !isNullOrUndefined(fileData.gst_certification_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/vendor_user`
      let objectName = "" + userCheck._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.gst_certification_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".jpg"
      } else if (
        ["application/msword"].includes(
          fileData.gst_certification_image.mimetype
        ) &&
        /\.doc$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".doc"
      } else if (
        [
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        ].includes(fileData.gst_certification_image.mimetype) &&
        /\.docx$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".docx"
      } else if (
        fileData.gst_certification_image.mimetype === "application/pdf" &&
        /\.pdf$/i.test(fileData.gst_certification_image.name)
      ) {
        objectName += ".pdf"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg, jpeg, mp4, doc, docx & pdf supported.`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.gst_certification_image.data,
        fileData.gst_certification_image.mimetype
      )
      editDoc.gst_certification_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    console.log("editDoc" , editDoc)
    const userDoc = await VendorUserModel.findOneAndUpdate(
      { _id: uid },
      { $set: editDoc },
      {
        new: true,
      }
    )
    if (userDoc === null) {
      return Promise.reject(
        new UnexpectedInput(`Update failed as no record exists`, 400)
      )
    }
  
    const subvendorusers = await VendorUserModel.find({
      master_vendor_id: new ObjectId(userDoc._id),
    })
    console.log("subvendorusers" , subvendorusers)
        for (let i = 0; i < subvendorusers.length; i++) {
         await VendorUserModel.findOneAndUpdate(
            { _id: new ObjectId(subvendorusers[i]._id) },
            { $set: subVendorEditDoc },
          )
        }
    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.UPDATE_VENDOR_PROFILE,
      `Vendor Updated profile with id ${uid}.`,
      userDoc,
      editDoc
    )

    if (!isNullOrUndefined(editDoc.email)) {
      // SEND EMAIL.
      // //email has been changed. Send verification link to user
      // const emailToken = await this.commonService.getEmailAuthToken(
      //   editDoc.email,
      //   "vendor",
      //   uid,
      //   emailVerificationType.PROFILE
      // )
      // const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
      // try {
      //   await this.mailService.sendMailUsingTemplate(
      //     "email_verification",
      //     {
      //       subject: { name: userDoc.full_name },
      //       html: { name: userDoc.full_name, email: editDoc.email, link },
      //     },
      //     { full_name: userDoc.full_name, email: editDoc.email }
      //   )
      // } catch (e) {
      //   logger.fatal("Unable to send message:", e)
      //   return Promise.reject(
      //     new Error(`Unable to send email. Please try again later.`)
      //   )
      // }
    }
    // if (!isNullOrUndefined(editDoc.mobile_number)) {
    //   const authCode = await this.authRepo.getVendorAuthCode(
    //     editDoc.mobile_number,
    //     uid
    //   )

    //   if (isNullOrUndefined(authCode)) {
    //     return Promise.reject(new UnexpectedInput(`No authCode found`, 400))
    //   }
    //   //mobile number has been changed. Send OTP verification to user
    //   // await this.twilioService.sendCode(
    //   //   editDoc.mobile_number,
    //   //   authCode.code,
    //   //   "sms"
    //   // )
    // }

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_mobile_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.new_email_code,
    })

    await AuthModel.deleteOne({
      user_id: new ObjectId(uid),
      code: editBody.old_email_code,
    })

    return Promise.resolve(userDoc)
  }
  async getUserProfile(uid: string) {
    let vendorData = await VendorUserModel.findById(uid).exec()

    if (!isNullOrUndefined(vendorData) && vendorData.region_served.length > 0) {
      let region_name: any[] = []
      for (let i = 0; i < vendorData.region_served.length; i++) {
        let doc = await StateModel.findById(vendorData.region_served[i])
        if (!isNullOrUndefined(doc)) {
          region_name.push(doc.state_name)
        }
      }
      vendorData.region_name = region_name
    }
    let vendorSettingData = await VendorSettingModel.findOne({
      vendor_id: new ObjectId(uid),
    })
    if (
      !isNullOrUndefined(vendorData) &&
      !isNullOrUndefined(vendorSettingData)
    ) {
      vendorData.new_order_taken = vendorSettingData.new_order_taken
    }

    return Promise.resolve(vendorData)
  }

  async verifyUserWithMobilenumber(
    user_id: string,
    mobileNumber: string,
    code: string
  ) {
    const userCheck = await AuthModel.findOne({
      user_id,
      user_type: "vendor",
      identifier: mobileNumber,
      code,
    })

    if (userCheck === null) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again `, 400)
      )
    }

    return Promise.all([
      VendorUserModel.findOneAndUpdate(
        {
          _id: user_id,
          mobile_number: mobileNumber,
        },
        { $set: { mobile_verified: true } }
      ),
      AuthModel.deleteOne({
        user_id,
        user_type: "vendor",
        identifier: mobileNumber,
      }),
    ])
  }

  async resendEmail(user_id: string, email: string) {
    const userCheck = await VendorUserModel.findOne({
      _id: user_id,
      email,
    })

    if (userCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This Email is not registered.`, 400)
      )
    }

    if (userCheck.email_verified === true) {
      return Promise.resolve("Email already verified.")
    }
    const emailToken = await this.commonService.getEmailAuthToken(
      email,
      "vendor",
      user_id,
      emailVerificationType.PROFILE
    )
    const link = `${emailVerificationLinkURI}${emailToken.auth_token}`
    try {
      await this.mailService.sendMailUsingTemplate(
        "email_verification",
        {
          subject: { name: userCheck.full_name },
          html: { name: userCheck.full_name, email, link },
        },
        { full_name: userCheck.full_name, email }
      )
      return Promise.resolve("Email sent.")
    } catch (e) {
      logger.fatal("Unable to send message:", e)
      return Promise.reject(
        new Error(`Unable to send email. Please try again later.`)
      )
    }
  }

  async resendOTP(user_id: string, mobileNumber: string) {
    const userCheck = await VendorUserModel.findOne({
      _id: user_id,
      mobile_number: mobileNumber,
    })

    if (userCheck === null) {
      return Promise.reject(
        new UnexpectedInput(`This Mobile is not registered.`, 400)
      )
    }

    if (userCheck.mobile_verified === true) {
      return Promise.resolve("This mobile already verified.")
    }

    const authCode = await this.authRepo.getVendorAuthCode(
      mobileNumber,
      user_id
    )
    if (isNullOrUndefined(authCode)) {
      return Promise.reject(new UnexpectedInput(`No authCode found`, 400))
    }
    let message = `"${authCode.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(mobileNumber, message, templateId)
    return Promise.resolve("OTP sent.")
  }

  async changePassword(
    uid: string,
    old_password: string,
    password: string,
    authentication_code: string
  ): Promise<any> {
    console.log(uid)

    const result = await AuthModel.findOne({
      code: authentication_code,
    })
    if (isNullOrUndefined(result)) {
      return Promise.reject(
        new InvalidInput(`You entered a wrong OTP. Kindly try again`, 400)
      )
    }

    const doc = await VendorUserModel.findById(uid)

    if (doc === null || isNullOrUndefined(doc.password)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }

    if (await bcrypt.compare(old_password, doc.password)) {
      let message = `Hi ${doc.company_name}, you have successfully updated the login password for your Partner Account at Conmix.`
      // Password Update Successful.
      let templateId = "1707163731250172170"
      await this.testService.sendSMS(doc.mobile_number, message, templateId)

      let pwd_update_success_html = vendor_mail_template.password_updated_successfully
      pwd_update_success_html = pwd_update_success_html.replace("{{doc.company_name}}", doc.company_name)
      var data = {
        from: "no-reply@conmate.in",
        to: doc.email,
        subject: "Conmix - Password Update Successful",
        html: pwd_update_success_html,
      }
      await this.mailgunService.sendEmail(data)

      return VendorUserModel.updateOne(
        { _id: uid },
        { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
      )
    }

    await AuthModel.deleteOne({
      uid,
      code: authentication_code,
    })
    await this.commonService.addActivityLogs(
      uid,
      "vendor",
      AccessTypes.CHANGE_PASSWORD_BY_VENDOR,
      `Vendor Changed password with id ${uid}.`,
      old_password,
      password
    )

    return Promise.reject(
      new InvalidInput(
        `The New password and  Confirm password do not match `,
        400
      )
    )
  }

  async requestOTP(user_id: string, reqData: any) {
    const userDoc = await VendorUserModel.findById(user_id)

    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }

    if(reqData.event_type === "edit_admix_stock"){
      const query: { [k: string]: any } = {
        address_id: reqData.admix_address_id,
        brand_id: reqData.admix_brand_id,
        category_id: reqData.admix_category_id,
      }
  
      query.$or = [
        {
          vendor_id: new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
        {
          master_vendor_id: new ObjectId(user_id),
        },
      ]
  
      let data = await AdmixtureQuantityModel.findOne(query)
    
      if (
        !isNullOrUndefined(data) &&
        data._id.equals(reqData.AdmixtureQuantityId) === false
      ) {
        return Promise.reject(
          new InvalidInput(`Admix qunatity stock already exists for this brand and category at selected site address.`, 400)
        )
      }
    }

    if(reqData.event_type === "edit_sand_stock"){
      const query: { [k: string]: any } = {
        address_id: reqData.sand_address_id,
        source_id: reqData.sand_source_id,
      }
  
      query.$or = [
        {
          vendor_id: new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
        {
          master_vendor_id: new ObjectId(user_id),
        },
      ]
  
      let data = await SandSourceQuantityModel.findOne(query)

      if (
        !isNullOrUndefined(data) &&
        data._id.equals(reqData.SandSourceQuantityId) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `Sand stock already exists for this source at selected site address.`,
            400
          )
        )
      }
    }

    if(reqData.event_type === "edit_agg_stock"){
      const query: { [k: string]: any } = {
        address_id: reqData.agg_address_id,
        source_id: reqData.agg_source_id,
        sub_cat_id: reqData.agg_sub_cat_id,
      }
  
      query.$or = [
        {
          vendor_id: new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
        {
          master_vendor_id: new ObjectId(user_id),
        },
      ]
  
      let data = await AggregateSourceQuantityModel.findOne(query)

      if (
        !isNullOrUndefined(data) &&
        data._id.equals(reqData.AggregateSourceQuantityId) === false
      ) {
        return Promise.reject(
          new InvalidInput(`Aggregate stock already exists for this source at selected site address.`, 400)
        )
      }
    }

    if(reqData.event_type === "edit_cement_stock"){
      const query: { [k: string]: any } = {
        address_id: reqData.cement_address_id,
        brand_id: reqData.cement_brand_id,
        grade_id: reqData.cement_grade_id,
      }
  
      query.$or = [
        {
          vendor_id: new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
        {
          master_vendor_id: new ObjectId(user_id),
        },
      ]
  
      let data = await CementQuantityModel.findOne(query)

      if (
        !isNullOrUndefined(data) &&
        data._id.equals(reqData.CementQuantityId) === false
      ) {
        return Promise.reject(
          new InvalidInput(`Cement stock already exists for this brand at selected site address.`, 400)
        )
      }
    }

    if(reqData.event_type === "edit_flyAsh_stock"){
      const query: { [k: string]: any } = {
        address_id: reqData.flyAsh_address_id,
        source_id: reqData.flyAsh_source_id,
      }
  
      query.$or = [
        {
          vendor_id: new ObjectId(user_id),
        },
        {
          sub_vendor_id: new ObjectId(user_id),
        },
        {
          master_vendor_id: new ObjectId(user_id),
        },
      ]
  
      let data = await FlyAshSourceQuantityModel.findOne(query)

      if (
        !isNullOrUndefined(data) &&
        data._id.equals(reqData.FlyAshSourceQuantityId) === false
      ) {
        return Promise.reject(
          new InvalidInput(
            `Fly Ash stock already exists for this source at selected site address.`,
            400
          )
        )
      }
    }
    
    if(!isNullOrUndefined(reqData.old_password) && !isNullOrUndefined(reqData.password)){
      if (await bcrypt.compare(reqData.old_password, userDoc.password)) {
        const code = generateRandomNumber()

        const authData = await AuthModel.findOneAndUpdate(
          {
            identifier: userDoc.mobile_number,
            user_id,
          },
          {
            $set: { code, created_at: Date.now() },
          },
          { upsert: true, new: true }
        ).exec()
        if (authData === null) {
          return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
        }
       
        let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
        // Registration OTP.
        let templateId = "1707163645095255064"
        await this.testService.sendSMS(userDoc.mobile_number, message, templateId)
        return Promise.resolve(authData)
      } else {
        return Promise.reject(
          new InvalidInput(
            `The old password not matched with existing password. `,
            400
          )
        )
      }
    }

    const code = generateRandomNumber()

    const authData = await AuthModel.findOneAndUpdate(
      {
        identifier: userDoc.mobile_number,
        user_id,
      },
      {
        $set: { code, created_at: Date.now() },
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(userDoc.mobile_number, message, templateId)

    return Promise.resolve(authData)
  }

  async requestOTPForMaster(user_id: string) {
    const userDoc = await VendorUserModel.findById(user_id)

    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }
    let mastervendorDoc: any
    if (!isNullOrUndefined(userDoc.master_vendor_id)) {
      mastervendorDoc = await VendorUserModel.findById(userDoc.master_vendor_id)
      if (isNullOrUndefined(mastervendorDoc)) {
        return Promise.reject(
          new InvalidInput(`No master vendor document could be found`, 400)
        )
      }
    }
    const code = generateRandomNumber()
    const authData = await AuthModel.findOneAndUpdate(
      {
        identifier: mastervendorDoc.mobile_number,
        user_id: mastervendorDoc._id,
      },
      {
        $set: { code, created_at: Date.now() },
      },
      { upsert: true, new: true }
    ).exec()
    if (authData === null) {
      return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
    }

    let message = `"${authData.code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
    // Registration OTP.
    let templateId = "1707163645095255064"
    await this.testService.sendSMS(userDoc.mobile_number, message, templateId)
    return Promise.resolve({
      mobile_number: mastervendorDoc.mobile_number,
      authData,
    })
  }

  async forgotPassword(uid: string, password: string) {
    console.log(uid)

    const doc = await VendorUserModel.findById(uid)

    if (isNullOrUndefined(doc)) {
      return Promise.reject(new InvalidInput(`No record was found.`, 400))
    }

    await VendorUserModel.updateOne(
      { _id: uid },
      { $set: { password: await bcrypt.hash(password, SALT_WORK_FACTOR) } }
    )

    let message = `Hi ${doc.company_name}, you have successfully updated the login password for your Partner Account at Conmix.`
    // Password Update Successful.
    let templateId = "1707163731250172170"
    await this.testService.sendSMS(doc.mobile_number, message, templateId)

    let pwd_update_success_html = vendor_mail_template.password_updated_successfully
      pwd_update_success_html = pwd_update_success_html.replace("{{doc.company_name}}", doc.company_name)
    var data = {
      from: "no-reply@conmate.in",
      to: doc.email,
      subject: "Conmix - Password Update Successful",
      html: pwd_update_success_html,
    }
    await this.mailgunService.sendEmail(data)
    await this.commonService.addActivityLogs(
      uid,
      "admin",
      AccessTypes.FORGOT_PASSWORD_BY_VENDOR,
      `Admin Forgot password with id ${uid}.`,
      doc.password,
      password
    )
  }

  async requestOTPForUpdateProfile(user_id: string, reqData: any) {
    const userDoc = await VendorUserModel.findById(user_id)
    if (isNullOrUndefined(userDoc)) {
      return Promise.reject(new InvalidInput(`No document could be found`, 400))
    }

    let oldmobileauthData: any
    if (!isNullOrUndefined(reqData.old_mobile_no)) {

      const code = generateRandomNumber()

      oldmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Send OTP On Old mobile number
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(reqData.old_mobile_no, message, templateId)
    }

    let newmobileauthData: any
    if (!isNullOrUndefined(reqData.new_mobile_no)) {
      //const code = generateRandomNumber()

      const numberCheck = await VendorUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(`This mobile_number is already registered.`, 400)
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same mobile_number already registserd with client.")
        )
      }

      let adminDoc = await AdminUserModel.findOne({
        mobile_number: reqData.new_mobile_no,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same mobile_number already registserd with admin.")
        )
      }

      const code = generateRandomNumber()
     newmobileauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_mobile_no,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newmobileauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let message = `"${code}" is your OTP at Conmix . It is valid for 60 seconds. Kindly do not share the OTP with others.`
      // Send OTP On Old mobile number
      let templateId = "1707163645095255064"
      await this.testService.sendSMS(reqData.new_mobile_no, message, templateId)
    }

    let oldemailauthData: any
    if(!isNullOrUndefined(reqData.old_email)){
      const code: any = generateRandomNumber()
      oldemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.old_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (oldemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let one_time_pwd_with_vendorName = vendor_mail_template.one_time_pwd_with_vendorName
      one_time_pwd_with_vendorName = one_time_pwd_with_vendorName.replace("{{userDoc.company_name}}", userDoc.company_name)
      one_time_pwd_with_vendorName = one_time_pwd_with_vendorName.replace("{{code}}", code)
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.old_email,
        subject: "Conmix - One Time Password",
        html: one_time_pwd_with_vendorName,
      }
      await this.mailgunService.sendEmail(data)
    }

    let newemailauthData: any
    if (!isNullOrUndefined(reqData.new_email)) {
      const numberCheck = await VendorUserModel.findOne({
        email: reqData.new_email,
      })

      if (
        !isNullOrUndefined(numberCheck) &&
        numberCheck._id.equals(user_id) === false
      ) {
        return Promise.reject(
          new InvalidInput(`This email is already registered.`, 400)
        )
      }

      let clientDoc = await BuyerUserModel.findOne({
        email: reqData.new_email,
      })

      if (!isNullOrUndefined(clientDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with client.")
        )
      }


      let adminDoc = await AdminUserModel.findOne({
        email: reqData.new_email,
      })

      if (!isNullOrUndefined(adminDoc)) {
        return Promise.reject(
          new Error("Same email already registserd with admin.")
        )
      }

      const code: any = generateRandomNumber()
      newemailauthData = await AuthModel.findOneAndUpdate(
        {
          identifier: reqData.new_email,
          user_id,
        },
        {
          $set: { code, created_at: Date.now() },
        },
        { upsert: true, new: true }
      ).exec()
      if (newemailauthData === null) {
        return Promise.reject(new InvalidInput(`Unable to get Auth Code.`, 400))
      }

      let one_time_pwd_with_vendorName = vendor_mail_template.one_time_pwd_with_vendorName
      one_time_pwd_with_vendorName = one_time_pwd_with_vendorName.replace("{{userDoc.company_name}}", userDoc.company_name)
      one_time_pwd_with_vendorName = one_time_pwd_with_vendorName.replace("{{code}}", code)
      var data = {
        from: "no-reply@conmate.in",
        to: reqData.new_email,
        subject: "Conmix - One Time Password",
        html: one_time_pwd_with_vendorName,
      }
      await this.mailgunService.sendEmail(data)
    }

    return Promise.resolve({
      oldmobileauthData,
      newmobileauthData,
      oldemailauthData,
      newemailauthData,
    })
  }
}
