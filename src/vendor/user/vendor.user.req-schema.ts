import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
const FullNameRegex = /^[a-z. ]*$/i

export const createVendorUserSchema: IRequestSchema = {
  query: Joi.object().keys({
    signup_type: Joi.string().valid(["mobile", "email"]).required(),
  }),

  body: Joi.object().keys({
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      })
      .required(),
    company_name: Joi.string().min(3).max(64),
    email: Joi.string().email(),
    // account_type: Joi.string().valid(["personal", "company"]),
    company_type: Joi.string()
      .valid(["Partnership", "Proprietor", "LLP", "LTD", "PVT LTD", "One Person Company"])
      .required(),
    password: Joi.string().min(6).max(20).required(),
    pan_number: Joi.string().length(10).required(),
    //pancard_image_url: Joi.string().max(200),
    gst_number: Joi.string().max(100),
    mobile_number: Joi.string(),
    landline_number: Joi.string(),
    cst: Joi.string(),
    annual_turnover: Joi.number(),
    trade_capacity: Joi.string(),
    number_of_employee: Joi.number(),
    company_certification_number: Joi.string(),
    // company_certification_image_url: Joi.string(),
    hse_number: Joi.string(),
    ehs_as_per_iso: Joi.string(),
    ohsas_as_per_iso: Joi.string(),
    working_with_other_ecommerce: Joi.boolean(),
    language: Joi.string(),
    region_served: Joi.array().items(objectIdRegex),
    authentication_code: Joi.string().required(),
    // no_of_plants: Joi.number().required(),
    // plant_capacity_per_hour: Joi.number().required(),
    // no_of_operation_hour: Joi.number().required(),
  }),
}

export const editVendorUserSchema: IRequestSchema = {
  body: Joi.object().keys({
    full_name: Joi.string()
      .min(3)
      .max(64)
      .regex(FullNameRegex)
      .options({
        language: {
          any: { allowOnly: "Please use only letters(a-z) and periods" },
        },
      }),
    company_name: Joi.string().min(3).max(64),
    pan_number: Joi.string().length(10),
    company_type: Joi.string().valid([
      "Partnership",
      "Proprietor",
      "LLP",
      "LTD",
      "PVT LTD",
      "One Person Company"
    ]),
    // pancard_image_url: Joi.string().max(200),
    gst_number: Joi.string().max(100),
    password: Joi.string().min(6).max(20),
    email: Joi.string(),
    mobile_number: Joi.string(),
    landline_number: Joi.string(),
    cst: Joi.string(),
    annual_turnover: Joi.number(),
    trade_capacity: Joi.string(),
    number_of_employee: Joi.number(),
    company_certification_number: Joi.string(),
    // company_certification_image_url: Joi.string(),
    hse_number: Joi.string(),
    ehs_as_per_iso: Joi.string(),
    ohsas_as_per_iso: Joi.string(),
    working_with_other_ecommerce: Joi.boolean(),
    language: Joi.string(),
    region_served: Joi.array().items(objectIdRegex),
   // authentication_code: Joi.string().required(),
    new_mobile_code: Joi.string(),
    new_email_code: Joi.string(),
    // no_of_plants: Joi.number(),
    // plant_capacity_per_hour: Joi.number(),
    // no_of_operation_hour: Joi.number(),
  }),
}
export const verifyMobilenumberSchema: IRequestSchema = {
  params: Joi.object().keys({
    mobileNumber: Joi.string().required(),
    code: Joi.string().required(),
  }),
}

export const resendEmailSchema: IRequestSchema = {
  body: Joi.object().keys({
    email: Joi.string().required(),
  }),
}

export const resendOTPSchema: IRequestSchema = {
  body: Joi.object().keys({
    mobileNumber: Joi.string().required(),
  }),
}

export const changePasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    old_password: Joi.string().min(6).max(20).required(),
    password: Joi.string().min(6).max(20).required(),
    authentication_code: Joi.string().required(),
  }),
}

export const forgotPasswordSchema: IRequestSchema = {
  body: Joi.object().keys({
    password: Joi.string().min(6).max(20).required()
  }),
}

export const requestOTPForUpdateProfileSchema: IRequestSchema = {
  body: Joi.object().keys({
    new_mobile_no: Joi.string(),
    new_email: Joi.string(),
    old_mobile_no: Joi.string(),
    old_email: Joi.string(),
  }),
}