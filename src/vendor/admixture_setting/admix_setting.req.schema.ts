import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"
import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"

export interface IAddAdMixSetting {
  settings: [
    {
      min_km: number
      max_km: number
      ad_mixture1_brand_id: string
      ad_mixture1_category_id: string
      ad_mixture2_brand_id: string
      ad_mixture2_category_id: string
      price: number
    }
  ]
  [k: string]: any
}

export interface IAddAdMixSettingRequest extends IAuthenticatedRequest {
  body: IAddAdMixSetting
}

export const addAdMixSettingSchema: IRequestSchema = {
  body: Joi.object().keys({
    settings: Joi.array()
      .items({
        min_km: Joi.number(),
        max_km: Joi.number(),
        ad_mixture1_brand_id: Joi.string().regex(objectIdRegex),
        ad_mixture1_category_id: Joi.string().regex(objectIdRegex),
        ad_mixture2_brand_id: Joi.string().regex(objectIdRegex),
        ad_mixture2_category_id: Joi.string().regex(objectIdRegex),
        price: Joi.number(),
      })
      .required(),
  }),
}

export interface IEditAdMixSetting {
  settings: [
    {
      min_km: number
      max_km: number
      ad_mixture1_brand_id: string
      ad_mixture1_category_id: string
      ad_mixture2_brand_id: string
      ad_mixture2_category_id: string
      price: number
    }
  ]
  [k: string]: any
}

export interface IEditAdMixSettingRequest extends IAuthenticatedRequest {
  body: IEditAdMixSetting
}

export const editAdMixSettingSchema: IRequestSchema = {
  body: Joi.object().keys({
    settings: Joi.array().items({
      min_km: Joi.number(),
      max_km: Joi.number(),
      ad_mixture1_brand_id: Joi.string().regex(objectIdRegex),
      ad_mixture1_category_id: Joi.string().regex(objectIdRegex),
      ad_mixture2_brand_id: Joi.string().regex(objectIdRegex),
      ad_mixture2_category_id: Joi.string().regex(objectIdRegex),
      price: Joi.number(),
    }),
  }),
}
