import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpPatch,
  httpGet,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { validate } from "../../middleware/joi.middleware"
import { NextFunction, Response } from "express"
import { VendorAdMixSettingRepository } from "./admix_setting.repository"
import {
  IAddAdMixSettingRequest,
  addAdMixSettingSchema,
  editAdMixSettingSchema,
  IEditAdMixSettingRequest,
} from "./admix_setting.req.schema"

@injectable()
@controller("/ad_mix_setting", verifyCustomToken("vendor"))
export class VendorAdMixSettingController {
  constructor(
    @inject(VendorTypes.VendorAdMixSettingRepository)
    private settingRepo: VendorAdMixSettingRepository
  ) {}

  @httpPost("/", validate(addAdMixSettingSchema))
  async addAdMixSetting(
    req: IAddAdMixSettingRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      await this.settingRepo.addAdMixSetting(uid, req.body)
      res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:setting_id", validate(editAdMixSettingSchema))
  async editAdMixSetting(
    req: IEditAdMixSettingRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      await this.settingRepo.editAdMixSetting(uid, req.body)
      res.sendStatus(200)
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getAdMixSetting(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    try {
      let data = await this.settingRepo.getAdMixSetting(uid)
      if (data === null) {
        res.json({ data: {} })
      }
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }
}
