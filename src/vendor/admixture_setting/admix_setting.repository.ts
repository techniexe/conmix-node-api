import { isNullOrUndefined } from "util"
import { injectable, inject } from "inversify"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { IAddAdMixSetting, IEditAdMixSetting } from "./admix_setting.req.schema"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorAdMixtureSettingModel } from "../../model/vendor_ad_mix_setting.model"
import { UnexpectedInput } from "../../utilities/customError"
import { ObjectId } from "bson"
import { AdmixtureBrandModel } from "../../model/admixture_brand.model"
import { AdmixtureCategoryModel } from "../../model/admixture_category.model"

@injectable()
export class VendorAdMixSettingRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}

  async addAdMixSetting(vendor_id: string, settingData: IAddAdMixSetting) {
    settingData.vendor_id = vendor_id

    await this.commonService.addActivityLogs(
      vendor_id,
      "vendor",
      AccessTypes.ADD_ADMIXTURE_SETTING,
      "Ad mixture Setting Added."
    )

    return new VendorAdMixtureSettingModel(settingData).save()
  }

  async editAdMixSetting(vendor_id: string, settingData: IEditAdMixSetting) {
    const flds = ["settings"]

    const editDoc: { [k: string]: any } = {}
    for (const [key, value] of Object.entries(settingData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = value
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    editDoc.updated_at = Date.now()
    editDoc.updated_by = vendor_id
    const res = await VendorAdMixtureSettingModel.findOneAndUpdate(
      { vendor_id: new ObjectId(vendor_id) },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This vendor's settings doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      vendor_id,
      "admin",
      AccessTypes.UPDATE_ADMIXTURE_SETTING,
      `Ad mixture Setting Updated.`,
      res,
      editDoc
    )

    return Promise.resolve()
  }

  async getAdMixSetting(vendor_id: string) {
    let data = await VendorAdMixtureSettingModel.findOne({
      vendor_id,
    })
    if (!isNullOrUndefined(data)) {
      console.log("heree")
      for (let i = 0; i < data.settings.length; i++) {
        if (!isNullOrUndefined(data.settings[i].ad_mixture1_brand_id)) {
          const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
            _id: data.settings[i].ad_mixture1_brand_id,
          }).lean()

          console.log("adMixtureBrandDoc", adMixtureBrandDoc)
          if (!isNullOrUndefined(adMixtureBrandDoc)) {
            data.settings[i].ad_mixture1_brand_name = adMixtureBrandDoc.name
          }
        }

        if (!isNullOrUndefined(data.settings[i].ad_mixture2_brand_id)) {
          const adMixtureBrandDoc = await AdmixtureBrandModel.findById({
            _id: data.settings[i].ad_mixture2_brand_id,
          }).lean()

          console.log("adMixtureBrandDoc", adMixtureBrandDoc)
          if (!isNullOrUndefined(adMixtureBrandDoc)) {
            data.settings[i].ad_mixture2_brand_name = adMixtureBrandDoc.name
          }
        }

        if (!isNullOrUndefined(data.settings[i].ad_mixture1_category_id)) {
          const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
            _id: data.settings[i].ad_mixture1_category_id,
          }).lean()

          console.log("adMixtureCatDoc", adMixtureCatDoc)
          if (!isNullOrUndefined(adMixtureCatDoc)) {
            data.settings[i].ad_mixture1_category_name =
              adMixtureCatDoc.category_name
          }
        }

        if (!isNullOrUndefined(data.settings[i].ad_mixture2_category_id)) {
          const adMixtureCatDoc = await AdmixtureCategoryModel.findById({
            _id: data.settings[i].ad_mixture2_category_id,
          }).lean()

          console.log("adMixtureCatDoc", adMixtureCatDoc)
          if (!isNullOrUndefined(adMixtureCatDoc)) {
            data.settings[i].ad_mixture2_category_name =
              adMixtureCatDoc.category_name
          }
        }
      }
    }

    return Promise.resolve(data)
  }
}
