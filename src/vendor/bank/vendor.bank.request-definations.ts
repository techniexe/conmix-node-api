import { IAuthenticatedRequest } from "../../middleware/auth-token.middleware"
import { UploadedFile } from "express-fileupload"

export interface IBank {
  bank_name: string
  user: {
    user_id?: string
    user_type?: string
  }
  company_id?: string
  account_holder_name: string
  account_number: number
  ifsc: string
  account_type: string
  cancelled_cheque_image_url?: string
  is_default?: boolean
}

export interface IEditBank {
  user: {
    user_id?: string
    user_type?: string
  }
  bank_name: string
  account_holder_name: string
  account_number: number
  ifsc: string
  account_type: string
  cancelled_cheque_image_url?: string
  is_default?: boolean
}

export interface IAddBankDetailsRequest extends IAuthenticatedRequest {
  body: {
    bank: IBank
  }
  files: {
    cancelled_cheque_image: UploadedFile
  }
}

export interface IGetBankDetailsRequest extends IAuthenticatedRequest {
  body: {
    bankId: string
  }
}

export interface IEditBankDetailsRequest extends IAuthenticatedRequest {
  body: {
    bank: IEditBank
  }
  files: {
    cancelled_cheque_image: UploadedFile
  }
}
