import * as Joi from "joi"
import { IRequestSchema, objectIdRegex } from "../../middleware/joi.middleware"

export const addBankDetailsSchema: IRequestSchema = {
  body: Joi.object().keys({
    bank: Joi.object()
      .keys({
        bank_name: Joi.string(),
        company_id: Joi.string(),
        account_holder_name: Joi.string().required(),
        account_number: Joi.number().required(),
        ifsc: Joi.string().required(),
        account_type: Joi.string().required().valid(["current", "saving"]),
        //  cancelled_cheque_image_url: Joi.string(),
        is_default: Joi.boolean(),
      })
      .required(),
  }),
}

export const getBankDetailsSchema: IRequestSchema = {
  params: Joi.object().keys({
    bankId: Joi.string().regex(objectIdRegex).required(),
  }),
}

export const editBankDetailsSchema: IRequestSchema = {
  body: Joi.object().keys({
    bank: Joi.object().keys({
      bank_name: Joi.string(),
      account_holder_name: Joi.string(),
      account_number: Joi.number(),
      ifsc: Joi.string(),
      account_type: Joi.string().valid(["current", "saving"]),
      // cancelled_cheque_image_url: Joi.string(),
      is_default: Joi.boolean(),
    }),
  }),
}
