import { injectable, inject } from "inversify"
import { IBank, IEditBank } from "./vendor.bank.request-definations"
import { IBankInfoDoc, BankInfoModel } from "../../model/bank.model"
import { UnexpectedInput, InvalidInput } from "../../utilities/customError"
import { AccessTypes } from "../../model/access_logs.model"
import { VendorTypes } from "../vendor.types"
import { CommonService } from "../../utilities/common.service"
import { UploadedFile } from "express-fileupload"
import { isNullOrUndefined } from "../../utilities/type-guards"
import { putObject, getS3MediaURL } from "../../utilities/s3.utilities"
import { awsConfig } from "../../utilities/config"

@injectable()
export class BankRepository {
  constructor(
    @inject(VendorTypes.CommonService) private commonService: CommonService
  ) {}
  async addBankDetails(
    user_id: string,
    bankData: IBank,
    fileData: { cancelled_cheque_image: UploadedFile }
  ): Promise<IBankInfoDoc> {
    bankData.user = { user_id }
    bankData.user.user_type = "vendor"
    const newBank = new BankInfoModel(bankData)

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.cancelled_cheque_image) &&
      !isNullOrUndefined(fileData.cancelled_cheque_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/bank`
      let objectName = "" + newBank._id + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.cancelled_cheque_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.cancelled_cheque_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.cancelled_cheque_image.data,
        fileData.cancelled_cheque_image.mimetype
      )
      newBank.cancelled_cheque_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    const newbankDoc = await newBank.save()

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.ADD_BANKDETAILS_BY_VENDOR,
      `Vendor added bank details of id ${newbankDoc._id} .`
    )

    return Promise.resolve(newbankDoc)
  }

  async getBankDetailsById(
    user_id: string,
    bankId: string
  ): Promise<IBankInfoDoc | null> {
    return BankInfoModel.findOne({
      "user.user_id": user_id,
      _id: bankId,
    })
  }

  async getBankDetails(user_id: string): Promise<IBankInfoDoc[]> {
    return BankInfoModel.find({
      "user.user_id": user_id,
    })
  }

  async editBankDetails(
    user_id: string,
    bankId: string,
    bankData: IEditBank,
    fileData: { cancelled_cheque_image: UploadedFile }
  ): Promise<any> {
    const flds = [
      "bank_name",
      "account_holder_name",
      "account_number",
      "ifsc",
      "account_type",
      "is_default",
    ]
    const editDoc: { [k: string]: any } = {}
    for (const [key, val] of Object.entries(bankData)) {
      if (flds.indexOf(key) > -1) {
        editDoc[key] = val
      }
    }
    if (Object.keys(editDoc).length < 1) {
      const err = new UnexpectedInput(`Kindly enter a field for update`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    if (
      !isNullOrUndefined(fileData) &&
      !isNullOrUndefined(fileData.cancelled_cheque_image) &&
      !isNullOrUndefined(fileData.cancelled_cheque_image.data)
    ) {
      const objectDir = `${
        process.env.NODE_ENV !== "production" ? "d" : "p"
      }/bank`
      let objectName = "" + bankId + Date.now()
      if (
        ["image/jpg", "image/jpeg", "image/png"].includes(
          fileData.cancelled_cheque_image.mimetype
        ) &&
        /\.jpe?g$/i.test(fileData.cancelled_cheque_image.name)
      ) {
        objectName += ".jpg"
      } else {
        return Promise.reject(
          new InvalidInput(
            `Invalid media format. Only jpg and jpeg supported`,
            400
          )
        )
      }
      const objectPath = `${objectDir}/${objectName}`
      await putObject(
        awsConfig.s3MediaBucket,
        objectPath,
        fileData.cancelled_cheque_image.data,
        fileData.cancelled_cheque_image.mimetype
      )
      editDoc.cancelled_cheque_image_url = getS3MediaURL(
        `${objectDir}/${objectName}`
      )
    }

    if (
      !isNullOrUndefined(bankData.is_default) &&
      bankData.is_default === true
    ) {
      await BankInfoModel.updateMany(
        {
          "user.user_id": user_id,
          is_default: true,
        },
        {
          $set: {
            is_default: false,
          },
        }
      ).exec()
    }

    const res = await BankInfoModel.findOneAndUpdate(
      {
        _id: bankId,
        "user.user_id": user_id,
      },
      { $set: editDoc }
    )
    if (res === null) {
      const err = new UnexpectedInput(`This bank details doesn't exists`)
      err.httpStatusCode = 400
      return Promise.reject(err)
    }

    await this.commonService.addActivityLogs(
      user_id,
      "vendor",
      AccessTypes.EDIT_BANKDETAILS_BY_VENDOR,
      `Vendor updated details bank of id ${bankId} .`,
      res,
      editDoc
    )

    return Promise.resolve()
  }
}
