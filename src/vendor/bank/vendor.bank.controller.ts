import { injectable, inject } from "inversify"
import {
  controller,
  httpPost,
  httpGet,
  httpPatch,
} from "inversify-express-utils"
import {
  verifyCustomToken,
  IAuthenticatedRequest,
} from "../../middleware/auth-token.middleware"
import { VendorTypes } from "../vendor.types"
import { BankRepository } from "./vendor.bank.repository"
import { NextFunction, Response } from "express"
import { validate } from "../../middleware/joi.middleware"
import {
  addBankDetailsSchema,
  getBankDetailsSchema,
  editBankDetailsSchema,
} from "./vendor.bank.request-schema"
import {
  IAddBankDetailsRequest,
  IGetBankDetailsRequest,
  IEditBankDetailsRequest,
} from "./vendor.bank.request-definations"
import { UnexpectedInput } from "../../utilities/customError"

@injectable()
@controller("/bank", verifyCustomToken("vendor"))
export class BankController {
  constructor(
    @inject(VendorTypes.BankRepository) private bankRepo: BankRepository
  ) {}

  @httpPost("/", validate(addBankDetailsSchema))
  async addBankDetails(
    req: IAddBankDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    const { uid } = req.user
    const { bank } = req.body
    try {
      const { _id } = await this.bankRepo.addBankDetails(uid, bank, req.files)
      return res.json({ data: { _id } })
    } catch (err) {
      let msg = err.message
      if (err.code === 11000) {
        msg = `This bank details is already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }

  @httpGet("/:bankId", validate(getBankDetailsSchema))
  async getBankDetailsById(
    req: IGetBankDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.bankRepo.getBankDetailsById(
        req.user.uid,
        req.params.bankId
      )
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpGet("/")
  async getBankDetails(
    req: IAuthenticatedRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.bankRepo.getBankDetails(req.user.uid)
      res.json({ data })
    } catch (err) {
      next(err)
    }
  }

  @httpPatch("/:bankId", validate(editBankDetailsSchema))
  async editBankDetails(
    req: IEditBankDetailsRequest,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.bankRepo.editBankDetails(
        req.user.uid,
        req.params.bankId,
        req.body.bank,
        req.files
      )
      res.sendStatus(202)
    } catch (err) {
      let msg = `This bank details is already exists. `
      if (err.code === 11000) {
        msg = `This bank details is already exists. `
      }
      const err1 = new UnexpectedInput(msg)
      err1.httpStatusCode = 400
      return next(err1)
    }
  }
}
