// // let variants = {
// //     cement_grade: ["OPC", "PPC"],
// //     cement_brand: ["JK", "Ultratech"],
// //     send_source: ["rajpadi", "sevalia"],
// //     aggregate_subcat: ["10mm", "20mm"],
// //     aggregate_source: ["aggregate_source"],
// //     fly_ash_source: ["fly_source"],
// //     admix_cate: ["admix_cat1", "admix_cat2"],
// //     admix_brand: ["admix_brand1", "admix_brand2"]
// // }

// // function combinations(variants) {
// //     return (function recurse(keys) {
// //         if (!keys.length) return [{}];
// //         let result = recurse(keys.slice(1));
// //         return variants[keys[0]].reduce((acc, value) =>
// //             acc.concat(result.map(item =>
// //                 Object.assign({}, item, {
// //                     [keys[0]]: value
// //                 })
// //             )),
// //             []
// //         );
// //     })(Object.keys(variants));
// // }

// // // Result
// // console.log(combinations(variants));

// // var allData = [
// //   {
// //     name: "Cat",
// //     type: "Animal",
// //     key: "key1",
// //   },
// //   {
// //     name: "lion",
// //     type: "Animal",
// //     key: "key2",
// //   },
// //   {
// //     name: "Tiger",
// //     type: "Animal",
// //     key: "key1",
// //   },
// // ]
// // let obj = {}
// // var newArr = []
// // for (let i = 0; i < allData.length; i++) {
// //   var finalData = []
// //   let obj = allData[i]
// //   for (let KEY in obj) {
// //     //Pushing data to other array as object
// //     finalData.push({
// //       [KEY]: obj[KEY],
// //     })
// //   }
// //   obj.key = "key1"
// //   newArr.push({ obj: finalData })
// // }

// // console.log("finalData", finalData)
// // console.log("newArr", newArr)
// // console.log("newArr", newArr["key1"])
// let resp = []
// let addressData = [{
//     "_id": "60129bd8d5a4a27134f15952",
//     "location": {
//       "coordinates": [
//         72.56999953782,
//         23.019009453091
//       ],
//       "type": "Point"
//     },
//     "line1": "Ahmedabad site line 1",
//     "line2": "line 2",
//     "state_id": "5b27adcdba933e1f06a78560",
//     "city_id": "5b2a2d2bba933e2973a7e0aa",
//     "pincode": 380015,
//     "address_type": "quarry",
//     "user": {
//       "user_type": "vendor",
//       "user_id": "600e760b68077bbcb83fcd73"
//     },
//     "business_name": "Ahmedabad Quarry",
//     "region_id": "5b27adcdba933e1f06a78560",
//     "source_id": "5f1690619080e62a91dbfb08",
//     "created_at": "2021-01-28T11:11:20.948Z",
//     "calculated_distance": 58441.76153188989
//   },
//   {
//     "_id": "6035deadffa2891f89f8f295",
//     "location": {
//       "type": "Point",
//       "coordinates": [
//         72.56999953782,
//         23.019009453091
//       ]
//     },
//     "line1": "Ahmedabad site2",
//     "line2": "line 2",
//     "state_id": "5b27adcdba933e1f06a78560",
//     "city_id": "5b2a2d2bba933e2973a7e0aa",
//     "pincode": 380015,
//     "address_type": "quarry",
//     "user": {
//       "user_type": "vendor",
//       "user_id": "600e760b68077bbcb83fcd73"
//     },
//     "business_name": "Ahmedabad Quarry",
//     "region_id": "5b27adcdba933e1f06a78560",
//     "source_id": "5f1690619080e62a91dbfb08",
//     "created_at": "2021-02-24T05:05:49.059Z",
//     "calculated_distance": 58441.76153188989
//   },
//   {
//     "_id": "6033a811c809c5314ceb7dd6",
//     "location": {
//       "type": "Point",
//       "coordinates": [
//         72.5713621,
//         23.022505
//       ]
//     },
//     "line1": "4 shantanu banlows rajpath club ni same",
//     "line2": "Narolgam, Ellisbridge",
//     "state_id": "5b27adcdba933e1f06a78560",
//     "city_id": "5b2a2d2bba933e2973a7e0aa",
//     "pincode": 380006,
//     "address_type": "office",
//     "user": {
//       "user_type": "vendor",
//       "user_id": "603355f2646e197e55272045"
//     },
//     "business_name": "Ahmedabad Plant",
//     "created_at": "2021-02-22T12:48:17.059Z",
//     "calculated_distance": 58595.86971069187
//   }
// ]
// for (let i = 0; i < addressData.length; i++) {

//   if (resp[addressData[i].user.user_id] === undefined) {
//     resp[addressData[i].user.user_id] = [addressData[i]]
//   } else {
//     console.log("resppp", resp)
//     console.log("addressData[i].user.user_id", addressData[i].user.user_id)
//     resp[addressData[i].user.user_id].push(
//       addressData[i]
//     )
//   }
// }

// // resp["user.user_id"] = data
// // //resp.push(data)

// // console.log(resp["key1"])
// // if (resp["key1"] !== undefined) {
// //   resp["key1"].push({
// //     name: "sdsd"
// //   })
// // }
// // data = [{
// //   name: "dhwani"
// // }]
// // console.log(resp["key2"])
// // if (resp["key2"] === undefined) {
// //   console.log(data)
// //   resp["key2"] = data
// // }

// console.log(resp["603355f2646e197e55272045"])


let marginSlab = [{
    upto: 1000,
    rate: 5
  },
  {
    upto: 2000,
    rate: 3
  },
  {
    upto: 50000,
    rate: 2
  }
]

function getMarginRate(
  amount,
  marginSlab,
) {
  const slab = marginSlab.sort((a, b) => {
    if (a.upto > b.upto) {
      return -1
    }
    if (a.upto < b.upto) {
      return 1
    }
    return 0
  })
  let returnRate = slab[0].rate
  for (let i = 0; i < slab.length; i++) {
    if (amount < slab[i].upto) {
      returnRate = slab[i].rate
    } else {
      break
    }
  }
  console.log("returnRate", returnRate)
  return returnRate

}



getMarginRate(5000, marginSlab)